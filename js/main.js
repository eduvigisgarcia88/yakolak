  
 function statusChangeCallback(response) {
    //$(".custom-photo-pane").addClass("hide");
    //$(".default-photo-pane").removeClass("hide");
   // console.log('statusChangeCallback');
   // console.log(response);
    // The response object is returned with a status field that lets the
    // app know the current login status of the person.
    // Full docs on the response object can be found in the documentation
    // for FB.getLoginStatus().
    if (response.status === 'connected') {

     
      // Logged into your app and Facebook.
       $("#standard_photo").addClass("hide");
       $(".standard_photo_pane").addClass("hide");
       $("#standard_photo_for_user").addClass("hide");
       // $(".default-photo-pane").addClass("hide");
       $(".facebook-photo-pane").removeClass("hide");
       //console.log(response.authResponse.accessToken);
       ///$("#fb-connect").attr('title','Logout your facebook account');
       FB.api('/me', function(response) {
      
        $(".system-photo-pane").html("<img class='img-circle image-responsive' id='row-photo_row' name='fb_photo' height='100' width='100' style='margin: 5px;' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        //$(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        //$(".system-photo-pane").append("");
        $("#image_settings").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_pane_edit_photo").html("<img class='img-circle image-responsive' name='fb_photo' height='100' width='100' src='http://graph.facebook.com/"+response.id+"/picture?type=large'>");
        $("#image_fb_link").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_for_system_user").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
        $("#image_fb_link_edit_photo").val("http://graph.facebook.com/"+response.id+"/picture?type=large");
       
        $("#default_image_pane").addClass("hide");
        $("#image_pane_edit_photo").removeClass("hide");
        $("#image_pane").removeClass("hide");
        $("#user_photo_pane").addClass("hide");
        $("#image_settings").removeClass("hide");
        $("#row-photo").addClass("hide");
        $("#settings-photo-change").text('Change Photo');

        });
      $("#status").val(1);

    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
    
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");


    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
        $("#row-photo").removeClass("hide");
        $("#standard_photo").removeClass("hide");
        $(".standard_photo_pane").removeClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");
        $("#default_image_pane").removeClass("hide");
        $("#user_photo_pane").removeClass("hide");
        $(".facebook-photo-pane").addClass("hide");
        $("#image_settings").addClass("hide");
        //$(".system-photo-div").html("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-photo-change'>CHANGE</button>");
        //$(".system-photo-pane").html("<img id='row-photo_row' src='....' class='preview img-circle' height='100' width='100' style='margin:5px;'/>");
        $(".system-photo-pane").html("<img src='{{ url('uploads')."/".$settings->default_photo }}' class='preview img-circle' height='100' width='100' id = 'row-photo_row' style='margin:5px;'/>");
        //$(".system-photo-pane").append("<button type='button' class='btn btn-primary btn-sm borderzero' id='edit-change-photo'>CHANGE</button>");
        //$(".system-photo-pane").append("<div class='change'><input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'></div>");
        $("#status").val(0);
        // $(".custom-photo-pane").html("");
        // $(".default-photo-pane").html("");


        //$("#row-photo_row").removeClass("hide");
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '395487573951234',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response);

  });
  };
 
    // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
 $("#btn-custom-photo").click(function() {

        $(".uploader").removeClass("hide");
        $(".default-photo-pane").addClass("hide");
        $(".photo_view").addClass("hide");
        $("#image_pane").addClass("hide");
        $("#image_pane_edit_photo").addClass("hide");

});
//tab navigation
$(document).ready(function(){
    $(".nav-tabs a").click(function(){
        $(this).tab('show');
    });
   
});
//animate number
$('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 1200,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
});

$("input.file_photo").fileinput({
    maxFileCount: 1,
    maxFileSize: 1024,
    allowedFileTypes: ['image'],
    allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
  
});

$("input.file_csv").fileinput({
    maxFileCount: 1,
    maxFileSize: 20480,
    allowedFileExtensions: ['xls', 'csv', 'xlsx'],
});

$.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
    options.async = true;
});

//$(".btn-file").html("<i class='fa fa-folder'></i>");
// tooltip
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
// show dialog modal
function dialog(title, body, action, id) {
	$("#dialog-title").html(title);
	$("#dialog-body").html(body);
	$("#dialog-confirm").data('url', action)
						.data('id', id);

	// show confirmation
    $("#modal-dialog").modal('show');
}

// display status message
function status(title, body, type, box) {
	if(box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	} else {
		switch(type) {
			case 'alert-success':
				toastr.success(body);
				break;
			case 'alert-danger':
				toastr.error(body);
				break;
		}
	}
}

// get url parameter
function urlData(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for(var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if(sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

$(document).ready(function() {

	$(".table").addClass("tablesorter");

	// do action after confirmation
	$("#dialog-confirm").click(function() {
		var url = $(this).data('url');
		var id = $(this).data('id');
      	token = $("#token").val();

		$.post(url, { id: id, _token: token }, function(response) {
			$("#modal-dialog").modal('hide');

			if(response.error) {
				status("Error", response.error, 'alert-danger');
			} else {
				status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
				refresh();
			}
		}, 'json');
	});

	// reset form notice
	$("#modal-form").on('show.bs.modal', function(e) {
		$("#form-notice").html("");
	});

	// submit form
    $("#modal-form").find("form").submit(function(e) {
   
  		var form = $("#modal-form").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
      		processData: false,
      		contentType: false,
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#modal-form").modal('hide');
					status(response.title, response.body, 'alert-success');
					
					// refresh the list
					
					refresh();
          $(".facebook-photo-pane").addClass("hide");
          $(".default-photo-pane").addClass("hide");
				}

				loading.addClass('hide');

			}
  		});
    });
   

	// submit form
    $("#modal-profile").find("form").submit(function(e) {
  		var form = $("#modal-profile").find("form");
  		var loading = $("#load-profile");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			//data: form.serialize(),
			data: new FormData($("#modal-save_profile")[0]),
			dataType: "json",
			async:true,
      		processData: false,
      		contentType: false,
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#profile-notice');
				} else {
					$("#modal-profile").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list 
					// error on  refresh
					refresh_profile();

				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-designate").find("form").submit(function(e) {
   
  		var form = $("#modal-designate").find("form");
  		var loading = $("#load-designate");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#designate-notice');
				} else {
					$("#modal-designate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-team").find("form").submit(function(e) {
   
  		var form = $("#modal-team").find("form");
  		var loading = $("#load-team");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#team-notice');
				} else {
					$("#modal-team").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-bonding").find("form").submit(function(e) {
   
  		var form = $("#modal-bonding").find("form");
  		var loading = $("#load-bonding");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');

  		console.log(form.serialize());

  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#bonding-notice');
				} else {
					$("#modal-bonding").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-provider").find("form").submit(function(e) {
   
  		var form = $("#modal-provider").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#provider-notice');
				} else {
					$("#modal-provider").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				
				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-product").find("form").submit(function(e) {
   
  		var form = $("#modal-product").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#product-notice');
				} else {
					$("#modal-product").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				
				loading.addClass('hide');

			}
  		});
    });

    // submit form
    $("#modal-rate").find("form").submit(function(e) {
   
  		var form = $("#modal-rate").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#rate-notice');
				} else {
					$("#modal-rate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });
     // submit form
   //  $("#modal-export").find("form").submit(function(e) {
   
  	// 	var $_token = $("input[name=_token]").val();
  	// 	var form = $("#modal-export").find("form");
  	// 	var loading = $("#load-form");
  	// 	var desigation_id = $("row-designation_id").val();
  	// 	var from = $("row-from").val();
  	// 	var to = $("row-to").val();
  	// 	// stop form from submitting
  	// 	e.preventDefault();

  	// 	loading.removeClass('hide');
  	// 	console.log(form.serialize());
  	// 	// push form data
  	// 	$.ajax({
			// type: "post",
			// url: form.attr('action'),
			// data: form.serialize(),
			// dataType: "json",
			// success: function(response) {
			// 	if(response.error) {
			// 		 var errors = '<ul>';

			// 		 $.each(response.error, function(index, value) {
			// 			errors += '<li>' + value + '</li>';
			// 		});

			// 		 errors += '</ul>';

			// 		status('Please correct the following:',error, 'alert-danger', '#export-notice');
			// 	} else {
			// 		$("#modal-export").modal('hide');
			// 		status(response.title, response.body, 'alert-success');

					
			// 	}

			// 	loading.addClass('hide');

			// }
  	// 	});
   //  });
    $("#modal-export").find("form").submit(function(e) {
    	var $_token = $("input[name=_token]").val();
  		var form = $("#modal-export").find("form");
  		var loading = $("#load-form");
  		var desigation_id = $("row-designation_id").val();
  		var from = $("row-from").val();
  		var to = $("row-to").val();

          console.log($_token);


          $.post(form.attr('action'), { _token: $_token }, function(response) {
           // var blob=new Blob([response]);
           // var link=document.createElement('a');
            //link.href=window.URL.createObjectURL(blob);
            //link.download="myfile.xls";
            //link.click();
          });

          $("#modal-export").modal('hide');

      });

  //   // submit form
  //   $("#modal-export").find("form").submit(function(e) {

  // 		var form = $("#modal-export").find("form");
  // 		var loading = $("#load-form");
  //    	var $_token = $("input[name=_token]").val();
     	
  //       $.post(form.attr('action'), { _token: $_token }, function(response) {
		// 	var blob=new Blob([response]);
		//     var link=document.createElement('a');
		//     link.href=window.URL.createObjectURL(blob);
		//     link.download="myfile.csv";
		//     link.click();
		// });

  // 		// stop form from submitting
  // 		e.preventDefault();

  // 		loading.removeClass('hide');

  // 		// push form data
  // 		$.ajax({
		// 	type: "post",
		// 	url: form.attr('action'),
		// 	data: form.serialize(),
		// 	dataType: "json",
		// 	success: function(response) {
		// 		if(response.error) {
		// 			var errors = '<ul>';

		// 			$.each(response.error, function(index, value) {
		// 				errors += '<li>' + value + '</li>';
		// 			});

		// 			errors += '</ul>';

		// 			status('Please correct the following:', errors, 'alert-danger', '#export-notice');
		// 		} else {


		// 			$("#modal-export").modal('hide');
		// 			status(response.title, response.body, 'alert-success');

		// 			// // refresh the list
		// 			// refresh();
		// 		}

		// 		loading.addClass('hide');

		// 	}
  // 		});
  //   });
   $("#modal-reject").find("form").submit(function(e) {
      // console.log("sdsds");
      var form = $("#modal-reject").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#reject-notice');
        } else {
          $("#modal-reject").modal('hide');
          status(response.title, response.body, 'alert-success');

          // // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

    // submit form
    $("#modal-production").find("form").submit(function(e) {
   
  		var form = $("#modal-production").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
      console.log($("#row-id").val());
      console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#production-notice');
				} else {
					$("#modal-production").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });

	// submit form
    $("#modal-allocate").find("form").submit(function(e) {
   
  		var form = $("#modal-allocate").find("form");
  		var loading = $("#load-allocate");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
          var errors = '';
					$.each(response.error, function(index, value) {
						errors += value;
					});

          $("#modal-allocate").modal('hide');
					status('', errors, 'alert-danger');

          refresh();
				} else {
					$("#modal-allocate").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


	// submit form
    $("#form-parse").find("form").submit(function(e) {

    	var form = $("#form-parse").find("form");
  		var loading = $("#load-form");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#production-notice');
				} else {
					//$("#modal-production").modal('hide');
					status(response.title, response.body, 'alert-success');

					// // refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


    // submit form
   //  $("#modal-banding-edit").find("form").submit(function(e) {
   
  	// 	var form = $("#modal-banding-edit").find("form");
  	// 	var loading = $("#load-form");
  	// 	// stop form from submitting
  	// 	e.preventDefault();

  	// 	loading.removeClass('hide');
  	// 	console.log(form.serialize());
  	// 	// push form data
  	// 	$.ajax({
			// type: "post",
			// url: form.attr('action'),
			// data: form.serialize(),
			// dataType: "json",
			// success: function(response) {
			// 	if(response.error) {
			// 		var errors = '<ul>';

			// 		$.each(response.error, function(index, value) {
			// 			errors += '<li>' + value + '</li>';
			// 		});

			// 		errors += '</ul>';

			// 		status('Please correct the following:', errors, 'alert-danger', '#banding-notice');
			// 	} else {
			// 		$("#modal-banding-edit").modal('hide');
			// 		status(response.title, response.body, 'alert-success');

			// 		// refresh the list
			// 		refresh();
			// 	}

			// 	loading.addClass('hide');

			// }
  	// 	});
   //  });

    // submit form
    $("#modal-assign").find("form").submit(function(e) {
   
  		var form = $("#modal-assign").find("form");
  		var loading = $("#load-form");
      var loading_assign = $("#load-assign");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
      loading_assign.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#assign-notice');
				} else {
					$("#modal-assign").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');
        loading_assign.addClass('hide');

			}
  		});
    });
    // submit form
    $("#modal-release").find("form").submit(function(e) {
   
  		var form = $("#modal-release").find("form");
  		var loading = $("#load-release");
  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: form.attr('action'),
			data: form.serialize(),
			dataType: "json",
			success: function(response) {
				console.log("uy");
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Batch Released Failed:', errors, 'alert-danger', '#release-notice');
				} else {
					$("#modal-release").modal('hide');
					status(response.title, response.body, 'alert-success');

					// refresh the list
					refresh();
				}

				loading.addClass('hide');

			}
  		});
    });


   $("#modal-remove-feed").find("form").submit(function(e) {
      // console.log("sdsds");
      var form = $("#modal-remove-feed").find("form");
      var loading = $("#load-remove-feed");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#remove-feed-notice');
        } else {
          $("#modal-remove-feed").modal('hide');
          status(response.title, response.body, 'alert-success');

          // // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
});
