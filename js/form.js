
$("#modal-sub-category-two").find("form").submit(function(e) {
      var form = $("#modal-sub-category-two").find("form");
      var loading = $("#modal-sub-category-two").find("#load-form");
      $("#modal-sub-category-two").find("#load-form").removeClass("hide");
      var notice = $("#modal-sub-category-two").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-sub-category-two").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
  });
  });

$("#modal-sub-category").find("form").submit(function(e) {
      var form = $("#modal-sub-category").find("form");
      var loading = $("#modal-sub-category").find("#load-form");
      $("#modal-sub-category").find("#load-form").removeClass("hide");
      var notice = $("#modal-sub-category").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-sub-category").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-add-category").find("form").submit(function(e) {
      var form = $("#modal-add-category").find("form");
      var loading = $("#modal-add-category").find("#load-form");
      $("#modal-add-category").find("#load-form").removeClass("hide");
      var notice = $("#modal-add-category").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-add-category").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-add-city").find("form").submit(function(e) {
      var form = $("#modal-add-city").find("form");
      var loading = $("#modal-add-city").find("#load-form");
      $("#modal-add-city").find("#load-form").removeClass("hide");
      var notice = $("#modal-add-city").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-add-city").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-add-localization").find("form").submit(function(e) {
      var form = $("#modal-add-localization").find("form");
      var loading = $("#modal-add-localization").find("#load-form");
      $("#modal-add-localization").find("#load-form").removeClass("hide");
      var notice = $("#modal-add-localization").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-add-localization").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-add-phrase").find("form").submit(function(e) {
      var form = $("#modal-add-phrase").find("form");
      var loading = $("#modal-add-phrase").find("#load-form");
      $("#modal-add-phrase").find("#load-form").removeClass("hide");
      var notice = $("#modal-add-phrase").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-add-phrase").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-recaptcha-settings").find("form").submit(function(e) {
      var form = $("#modal-recaptcha-settings").find("form");
      var loading = $("#modal-recaptcha-settings").find("#load-form");
      $("#modal-recaptcha-settings").find("#load-form").removeClass("hide");
      var notice = $("#modal-recaptcha-settings").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-recaptcha-settings").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          location.reload();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-spam-and-bot").find("form").submit(function(e) {
      var form = $("#modal-spam-and-bot").find("form");
      var loading = $("#modal-spam-and-bot").find("#load-form");
      $("#modal-spam-and-bot").find("#load-form").removeClass("hide");
      var notice = $("#modal-spam-and-bot").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-spam-and-bot").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
$("#modal-form-featured-settings").find("form").submit(function(e) {
      var form = $("#modal-form-featured-settings").find("form");
      var loading = $("#modal-form-featured-settings").find("#load-form");
      $("#modal-form-featured-settings").find("#load-form").removeClass("hide");
      var notice = $("#modal-form-featured-settings").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-form-featured-settings").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
         location.reload();
        }

        loading.addClass('hide');

      }
      });
  });
  $("#modal-add-country").find("form").submit(function(e) {

      var form = $("#modal-add-country").find("form");
      var loading = $("#load-form");
      // stop form from submitting
       e.preventDefault();

        loading.removeClass('hide');
        // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        }  else {

         $("#modal-add-country").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
 $("#modal-email-template").find("form").submit(function(e) {
  		var form = $("#modal-email-template").find("form");
  		var loading = $("#load-form");
      console.log(loading);
  		// stop form from submitting
    	 e.preventDefault();

        loading.removeClass('hide');
        // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        }  else {

         $("#modal-email-template").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
  });
  $("#modal-feature-ads").find("#feature_ad_points").find("form").submit(function(e) {
      var form = $("#modal-feature-ads").find("#feature_ad_points").find("form");
     
  });
  $("#points-pane").find('form').submit(function(e) {
      var form = $("#points-pane").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
        // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else if(response.status == 2){
            $("#error-insufficient-points").removeClass("hide");
        }else {
         $("#modal-feature-ads").modal('hide');
         $("#error-insufficient-points").addClass("hide");
         window.location.href = response.url;
         // status(response.title, response.body, 'alert-success');

          // refresh the list
        }

        loading.addClass('hide');

      }
      });
  });

  $("#modal-form").find("form").submit(function(e) {
   
      var form = $("#modal-form").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-form").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
  $(".login-form").find("form").submit(function(e) {
   
      var form = $(".login-form").find("form");
      var loading = $("#load-form-login");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
     $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $(".login-form").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
   // $("#inviteUser").find("form").submit(function(e) {
   
   //    var form = $("inviteUser").find("form");
   //    var loading = $(this).find("#load-form");

   //    // stop form from submitting
   //    e.preventDefault();

   //    loading.removeClass('hide');
   //    // console.log(new FormData($("#modal-register_form")[0]));
   //    // push form data
   //     $.ajax({
   //          type: "post",
   //          url: form.attr('action'),
   //          //data: form.serialize(),
   //          data: new FormData($("#modal-send-referral")[0]),
   //          dataType: "json",   
   //          async: true,
   //          processData: false,
   //          contentType: false,
   //    success: function(response) {
   //      if(response.error) {
   //        var errors = '<ul>';

   //        $.each(response.error, function(index, value) {
   //          errors += '<li>' + value + '</li>';
            
   //        });
   //        errors += '</ul>';
   //        status('Please correct the following:', errors, 'alert-danger', '#form-notice');
   //      } else {
   //        $("#modal-promotion").find("form")[0].reset();
   //        $("#modal-promotion").modal('hide');
   //        status(response.title, response.body, 'alert-success');
   //        // refresh the list
   //        refresh();
   //      }

   //      loading.addClass('hide');

   //    }
   //    });
   //  });
 // submit form
    $("#modal-register").find("form").submit(function(e) {

      var form = $("#modal-register").find("form");
      var loading = $("#modal-register").find("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-register_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {

            if (response.unauthorized) {
              window.location.href = response.unauthorized;
            }
        else if(response.error) {
          $(".sup-errors").html("");
          $(".form-control").removeClass("required");
          $.each(response.error, function(index, value) {
            var errors = value.split("|");
            $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
            $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
          });
        } else {
          $("#modal-register").modal('hide');
          status(response.title, response.body, 'alert-success');
          
          // refresh the list
          
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

  $("#modal-add-banner").find("form").submit(function(e) {

      var form = $("#modal-add-banner").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-banner_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-add-banner").find("form")[0].reset();
          $("#modal-add-banner").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
 $("#modal-promotion").find("form").submit(function(e) {

      var form = $("#modal-promotion").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-form-promotion")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-promotion").find("form")[0].reset();
          $("#modal-promotion").modal('hide');
          status(response.title, response.body, 'alert-success');
          location.reload();
          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

 $("#modal-moderation-banner").find("form").submit(function(e) {

      var form = $("#modal-moderation-banner").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-moderation-banner")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-moderation-banner").modal('hide');
          status(response.title, response.body, 'alert-success');
          // refresh the list
          // refresh();
        }

        loading.addClass('hide');

      }
      });
    });
 $("#modal-promotion-message").find("form").submit(function(e) {
   
      var form = $("#modal-promotion-message").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
     $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-promotion-message").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
  $("#modal-promotion-banner").find("form").submit(function(e) {

      var form = $("#modal-promotion-banner").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-form-promotion")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-promotion-banner").find("form")[0].reset();
          $("#modal-promotion-banner").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
   $("#modal-send-msg").find("form").submit(function(e) {
     
        var form = $("#modal-send-msg").find("form");
        var loading = $("#load-message-form");
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        // push form data
       $.ajax({
        type: "post",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json",
        success: function(response) {
          if(response.error) {
            var errors = '<ul>';

            $.each(response.error, function(index, value) {
              errors += '<li>' + value + '</li>';
              
            });

            errors += '</ul>';

            status('Please correct the following:', errors, 'alert-danger', '#form-notice');
          } else {
            
            status(response.title, response.body, 'alert-success');
            $("#modal-send-msg").modal("hide");
            // refresh the list
            refresh();
          }

          loading.addClass('hide');

        }
        });
      });


	$("#modal-direct-msg").find("form").submit(function(e) {
     
        var form = $("#modal-direct-msg").find("form");
        var loading = $("#modal-direct-msg").find("#load-message-form");
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());
        // push form data
       $.ajax({
        type: "post",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json",
        success: function(response) {
          if(response.error) {
            var errors = '<ul>';

            $.each(response.error, function(index, value) {
              errors += '<li>' + value + '</li>';
              
            });

            errors += '</ul>';

            status('Please correct the following:', errors, 'alert-danger', '#form-notice');
          } else {
            
            status(response.title, response.body, 'alert-success');
            $("#modal-direct-msg").modal("hide");
            // refresh the list
            refresh();
          }

          loading.addClass('hide');

        }
        });
      });


   $("#profileSettings").find("form").submit(function(e) {
     
        var form = $("#profileSettings").find("form");
        console.log(form);
        var loading = $("#load-settings-form");
        // stop form from submitting
        e.preventDefault();

        loading.removeClass('hide');
        console.log(form.serialize());

        // push form data
       $.ajax({
        type: "post",
        url: form.attr('action'),
        data: form.serialize(),
        dataType: "json",
        success: function(response) {
          if(response.error) {
            var errors = '<ul>';

            $.each(response.error, function(index, value) {
              errors += '<li>' + value + '</li>';
              
            });

            errors += '</ul>';

            status('Please correct the following:', errors, 'alert-danger', '#settings-form-notice');
          } else {

            status(response.title, response.body, 'alert-success');
            $("#settings-form-notice").html("");
            $("#profileSettings").find("form")[0].reset();
            // refresh the list
            refresh();
            refreshSettings();
          }

          loading.addClass('hide');

        }
        });
      });
   $("#message-pane").find("form").submit(function(e) {
       
          var form = $("#message-pane").find("form");
          $('#message-pane').find(".loading-pane").removeClass("hide");
          // stop form from submitting
          e.preventDefault();

          //loading.removeClass('hide');
          console.log(form.serialize());
          // push form data
         $.ajax({
          type: "post",
          url: form.attr('action'),
          data: form.serialize(),
          dataType: "json",
          success: function(response) {
            if(response.error) {
              var errors = '<ul>';

              $.each(response.error, function(index, value) {
                errors += '<li>' + value + '</li>';
                
              });

              errors += '</ul>';

              status('Please correct the following:', errors, 'alert-danger', '#form-notice');
              
            } else {
              $("#message-pane").modal('hide');
              //status(response.title, response.body, 'alert-success');
         
              // refresh the list
             refreshMessages();
             $("#message-pane").find("#row-message").val("");
            }
             $('#message-pane').find(".loading-pane").addClass("hide");
            //loading.addClass('hide');

          }
          });
      });
  $("#panel-account-profile").find("form").submit(function(e) {

      var form = $("#panel-account-profile").find("form");
      var loading = $("#load-account-form");
      
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(new FormData($("#modal-register_form")[0]));
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form-profile_save")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#account-profile-notice');
        } else {

          status(response.title, response.body, 'alert-success');
          if(response.body){
             $("#account").find('#account-profile-notice').addClass("hide");
          }
          // refresh the list
          refresh();
        }
       
        loading.addClass('hide');

      }
      });
    });
  // / submit form
    $("#panel-profile").find("form").submit(function(e) {
      var form = $("#panel-profile").find("form");

      // stop form from submitting
      e.preventDefault();
      $("#panel-profile").find(".imageUpload").html("");
      $("#panel-profile").find(".imageUpload").html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
      //console.log(form.serialize());
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_photo")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('', errors, 'alert-danger', '#change-photo-notice');
        } else {
          status(response.title, response.body, 'alert-success');
          $("#change-photo-notice").find(".alert").remove();
          $("#panel-profile").find("#file_name").val("");
          // refresh the list
          refresh();
        }

        $("#panel-profile").find(".imageUpload").html("");
        $("#panel-profile").find(".imageUpload").html("<i class='fa fa-picture-o'></i> Upload Photo");

      }
      });
    });
$("#editAccount").find("form").submit(function(e) {
   
      var form = $("#editAccount").find("form");
      var loading = $("#load-profile-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      //console.log(form.serialize());
      // push form data
      $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#profile-form-notice');
        } else if (response.status) {
          var errors = '<ul>';

          $.each(response.status, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('', errors, 'alert-success', '#profile-form-notice');
        } else if (response.relogin) {
          location.reload();
        } else {
          $("#modal-form").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

// save ads
  $("#ads-save").find("form").submit(function(e) {
   
        var form  = $("#ads-save").find("form");
        var loading = $("#load-form");
        // stop form from submitting
       e.preventDefault();
      loading.removeClass('hide');
      //console.log(form.serialize());
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#save_ads")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
     success: function(response) {
			 console.log(response);			 
				if (response.unauthorized) {
					window.location.href = response.unauthorized;
				}
        else if(response.error) {
						$(".sup-errors").html("");
						$(".form-control").removeClass("required");
						$.each(response.error, function(index, value) {
							if(value == 'LIMIT_REACHED') {
								status('Limit Reached', 'Limit reached for the selected Ad Type', 'alert-danger');
								loading.addClass('hide');
								return false;
							} else {
								var errors = value.split("|");
								$("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
								$("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
								$("." + errors[0].replace(/ /g,"_")).next('.sup-errors').html(errors[1]);
							}
							
						});
          
        } else {
          console.log(response);
          $("#modal-form").modal('hide');
          status(response.title, response.body, 'alert-success');
          if(response.code == 1 || response.code == 2)
          {
            if(response.ad_type == 1)
            {
              setTimeout(function()
              {
                window.location = response.url+'#listings';
              },1500);
            }
            else
            {
              setTimeout(function()
              {
                window.location = response.url+'#auction';
              },1500);
            }
          }
          else
          {
            refresh();
          }
        }

        loading.addClass('hide');

      }
      });
    });
 
  $("#comment-pane").find("form").submit(function(e) {
   
      var form = $("#comment-pane").find("form");
      var loading = $("#load-comment-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
     $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          status(response.title, response.body, 'alert-success');
           $("#comment-pane").find("form")[0].reset();
           location.reload();
        }

        loading.addClass('hide');

      }
      });
    });
  $("#ad-comment-pane").find("form").submit(function(e) {
      var form = $("#ad-comment-pane").find("form");
      var loading = $("#load-comment-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
     $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          status(response.title, response.body, 'alert-success');
           $("#ad-comment-pane").find("form")[0].reset();

        }

        loading.addClass('hide');

      }
      });
     
    });


  $(".comments-container").find("form").submit(function(e) {
      var form = $(this).parent().parent().parent().parent().find('form');
      var loading = $("#load-comment-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
     $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          status(response.title, response.body, 'alert-success');
           $("#reply-pane").find("form")[0].reset();
           location.reload();
        }

        loading.addClass('hide');

      }
      });
    });

  // $("#replies-container").find("form").submit(function(e) {
  //   alert("replies");
  //     var form = $(this).parent().parent().parent().find('form');
  //     var loading = $("#load-comment-form");
  //     // stop form from submitting
  //     e.preventDefault();

  //     loading.removeClass('hide');
  //     console.log(form.serialize());
  //     // push form data
  //    $.ajax({
  //     type: "post",
  //     url: form.attr('action'),
  //     data: form.serialize(),
  //     dataType: "json",
  //     success: function(response) {
  //       if(response.error) {
  //         var errors = '<ul>';

  //         $.each(response.error, function(index, value) {
  //           errors += '<li>' + value + '</li>';
            
  //         });

  //         errors += '</ul>';

  //         status('Please correct the following:', errors, 'alert-danger', '#form-notice');
  //       } else {
  //         status(response.title, response.body, 'alert-success');
  //          $("#reply-pane").find("form")[0].reset();
  //          location.reload();
  //       }

  //       loading.addClass('hide');

  //     }
  //     });
  //   });


   // $("#ad-replies-container").find("form").submit(function(e) {
   //    alert("sdds");
   //    var form = $(this).parent().parent().parent().find('#form-sub-reply');
   //    var loading = $("#load-comment-form");
   //    // stop form from submitting
   //    e.preventDefault();

   //    loading.removeClass('hide');
   //    console.log(form.serialize());
   //    // push form data
   //   $.ajax({
   //    type: "post",
   //    url: form.attr('action'),
   //    data: form.serialize(),
   //    dataType: "json",
   //    success: function(response) {
   //      if(response.error) {
   //        var errors = '<ul>';

   //        $.each(response.error, function(index, value) {
   //          errors += '<li>' + value + '</li>';
            
   //        });

   //        errors += '</ul>';

   //        status('Please correct the following:', errors, 'alert-danger', '#form-notice');
   //      } else {
   //        status(response.title, response.body, 'alert-success');
   //         $("#reply-pane").find("form")[0].reset();
   //         location.reload();
   //      }

   //      loading.addClass('hide');

   //    }
   //    });
   //  });



// $("#modal-login").find("form").submit(function(e) {
   
//         var form = $("#modal-login").find("form");
//         var loading = $("#load-form");
//         // stop form from submitting
//        e.preventDefault();
//       loading.removeClass('hide');
//       //console.log(form.serialize());
//       // push form data
//        $.ajax({
//             type: "post",
//             url: form.attr('action'),
//             //data: form.serialize(),
//             data: new FormData($("#modal-login-notice")[0]),
//             dataType: "json",   
//             async: true,
//             processData: false,
//             contentType: false,
//       success: function(response) {
//         if(response.error) {
//           var errors = '<ul>';

//           $.each(response.error, function(index, value) {
//             errors += '<li>' + value + '</li>';
            
//           });

//           errors += '</ul>';

//           status('', errors, 'alert-danger', '#ads-notice');
//         } else {
//           status(response.title, response.body, 'alert-success');
//           $("#ads-save").find("form")[0].reset();
//             refresh();
//         }
//         loading.addClass('hide');
//       }
//       });
//     });




// display status message
function status(title, body, type, box) {
	if(box) {
		var html = '<' + 'div class="alert alert-dismissible ' + type + '" role="alert">' +
				   '<' + 'button type="button" class="close" data-dismiss="alert"><' + 'span aria-hidden="true">&times;</' + 'span><' + 'span class="sr-only">Close</' + 'span></' + 'button>' +
				   (title ? '<strong>' + title + '</strong> ' : '') + body +
				   '</div>';

		$(box).removeClass('hide').html(html);
	} else {
		switch(type) {
			case 'alert-success':
				toastr.success(body);
				break;
			case 'alert-danger':
				toastr.error(body);
				break;
		}
	}
}

 $(function() {
  toastr.options.positionClass = 'toast-bottom-right';
  toastr.options.closeButton = true;
  // toastr.options.progressBar = false; // new
  // toastr.options.positionClass = "toast-bottom-full-width"; // new
  toastr.options.showMethod = 'slideDown';
 });

    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2
      });
    });



    function refresh() {
        var loading = $("#load-client");
        $('#modal-form').find('form').trigger('reset');
        loading.addClass('hide');
    }
 // show dialog modal
function dialog(title, body, action, id ,rate,review,vendor_id,ad_id) {
  $("#dialog-title").html(title);
  $("#dialog-body").html(body);
  $("#dialog-confirm").data('url', action)
            .data('id', id).data('rate',rate).data('vendor_id',vendor_id).data('review',review).data('ad_id',ad_id);

  $("#dialog-confirm-backend").data('url', action)
            .data('id', id).data('rate',rate).data('vendor_id',vendor_id).data('ad_id',ad_id);
  // show confirmation
  $("#modal-dialog").modal('show');
}
function dialogMessage(title, body, action, id ,rate,vendor_id,ad_id) {
  $("#dialog-message-title").html(title);
  $("#dialog-message-body").html(body);
  $("#dialog-message-confirm").data('url', action)
            .data('id', id).data('rate',rate).data('vendor_id',vendor_id).data('ad_id',ad_id);
  // show confirmation
  $("#modal-dialog-message").modal('show');
}
$(document).ready(function() {
  $("#dialog-confirm").click(function() {
      var url = $(this).data('url');
      var id = $(this).data('id');
      var rate = $(this).data('rate');
      var ad_id = $(this).data('ad_id');
      var review = $(this).data('review');
      var vendor_id = $(this).data('vendor_id');
      var loading = $("#load-dialog");
      token = $("#token").val();
      loading.removeClass('hide');
      $.post(url, { id: id,rate : rate,review: review,vendor_id: vendor_id,ad_id: ad_id, _token: token }, function(response) {
        $("#modal-dialog").modal('hide');

        if(response.error) {
          status("Error", response.error, 'alert-danger');
        } else {
          status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);
          location.reload();
          refresh();
          refreshMessageContainer(); 
          refreshUserBanner();
        }
        loading.addClass('hide');
      }, 'json');   
    });
});
 $("#dialog-confirm-backend").click(function() {
      var url = $(this).data('url');
      var id = $(this).data('id');
      var rate = $(this).data('rate');
      var ad_id = $(this).data('ad_id');
      var vendor_id = $(this).data('vendor_id');
      token = $("#token").val();
      var loading = $("#load-dialog");
      loading.removeClass('hide');
      $.post(url, { id: id,rate : rate,vendor_id: vendor_id,ad_id: ad_id, _token: token }, function(response) {
        $("#modal-dialog").modal('hide');

        if(response.error) {
          status("Error", response.error, 'alert-danger');
        } else {
          status(response.title ? response.title : false, response.body, 'alert-success', response.box ? response.box : false);        }
          refresh();
        loading.addClass('hide');
      }, 'json');   
    });

 $("#modal-ads-delete").find("form").submit(function(e) {
      var form = $("#modal-ads-delete").find("form");
      var loading = $("#load-basic-listing-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(new FormData($("#modal-save_form")[0]));
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-ads-delete").modal('hide');
          status(response.title, response.body, 'alert-success');

              // refresh the list
          refreshVendorAdsActiveList();
        
        }

        loading.addClass('hide');

      }
      });
    });

 $("#modal-ads-disable").find("form").submit(function(e) {
      var form = $("#modal-ads-disable").find("form");
      var loading_basic = $("#load-basic-listing-form");
      var loading_auction = $("#load-auction-listing-form");
      // stop form from submitting
      e.preventDefault();

      loading_basic.removeClass('hide');
      loading_auction.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else if(response.erroruser){
          $("#modal-ads-disable").find("form")[0].reset();
          $("#modal-ads-disable").modal('hide');
          status(response.title, response.erroruser, 'alert-danger');
          // refresh the list
          refreshVendorAdsActiveList();
        } else {
          $("#modal-ads-disable").find("form")[0].reset();
          $("#modal-ads-disable").modal('hide');
          status(response.title, response.body, 'alert-success');

        // refresh the list
          refreshVendorAdsActiveList();
      
        }

        loading_basic.addClass('hide');
        loading_auction.addClass('hide');

      }
      });
    });




 $("#modal-auction-disable").find("form").submit(function(e) {
      var form = $("#modal-auction-disable").find("form");
      var loading_basic = $("#load-basic-listing-form");
      var loading_auction = $("#load-auction-listing-form");
      // stop form from submitting
      e.preventDefault();

      loading_basic.removeClass('hide');
      loading_auction.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else if(response.erroruser){
          $("#modal-auction-disable").find("form")[0].reset();
          $("#modal-auction-disable").modal('hide');
          status(response.title, response.erroruser, 'alert-danger');
          // refresh the list
          refreshVendorAuctionActive();
        } else {
          $("#modal-auction-disable").find("form")[0].reset();
          $("#modal-auction-disable").modal('hide');
          status(response.title, response.body, 'alert-success');

        // refresh the list
          refreshVendorAuctionActive();
      
        }

        loading_basic.addClass('hide');
        loading_auction.addClass('hide');

      }
      });
    });


  $("#modal-ads-enable").find("form").submit(function(e) {
      var form = $("#modal-ads-enable").find("form");
      var loading_basic = $("#load-basic-listing-form");
      var loading_auction = $("#load-auction-listing-form");
      // stop form from submitting
      e.preventDefault();

      loading_basic.removeClass('hide');
      loading_auction.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-ads-enable").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading_auction.addClass('hide');
        loading_basic.addClass('hide');

      }
      });
    });



   $("#modal-ads-enable-alert").find("form").submit(function(e) {
      var form = $("#modal-ads-enable-alert").find("form");
      var loading_basic = $("#load-basic-listing-form");
      var loading_auction = $("#load-auction-listing-form");
     
      // stop form from submitting
      e.preventDefault();

      loading_basic.removeClass('hide');
      loading_auction.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-ads-enable-alert").modal('hide');
          status(response.title, response.body, 'alert-success');
          // refresh the list
          refreshVendorAdsActiveList();  
        }
        loading_auction.addClass('hide');
        loading_basic.addClass('hide');
      }
      });
    });

$("#modal-ads-disable-list").find("form").submit(function(e) {
      var form = $("#modal-ads-disable-list").find("form");
      var loading_basic = $("#load-basic-listing-form");
      var loading_auction = $("#load-auction-listing-form");
     
      // stop form from submitting
      e.preventDefault();

      loading_basic.removeClass('hide');
      loading_auction.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_ads_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-ads-disable-list").modal('hide');
          status(response.title, response.body, 'alert-success');
          // refresh the list
          refreshVendorAdsActiveList();  
        }
        loading_auction.addClass('hide');
        loading_basic.addClass('hide');
      }
      });
    });

  $("#modal-watchlist-remove").find("form").submit(function(e) {
      var form = $("#modal-watchlist-remove").find("form");
      var loading = $("#load-watchlist-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-remove_watchlist_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';       
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-watchlist-remove").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');
        location.reload();
      }
      });
    });



   $("#modal-watchlist").find("form").submit(function(e) {
      var form = $("#modal-watchlist").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_watchlist_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';
          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-watchlist").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');
        location.reload();
      }
      });
    });


// Upgrade subscription
 $("#modal-plan-subscription").find("form").submit(function(e) {
      var form = $("#modal-plan-subscription").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_subscription_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $("#modal-plan-subscription").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
            location.reload();
             
        }

        loading.addClass('hide');

      }
      });
    });


// edit email template
  $("#save-email-template").find("form").submit(function(e) {
        var form = $("#save-email-template").find("form");
        var loading = $("#load-form");

        // stop form from submitting
       e.preventDefault();
      loading.removeClass('hide');
      //console.log(form.serialize());
      // push form data
       $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form_email-template")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';     
          });
          errors += '</ul>';
          status('', errors, 'alert-danger', '#email-template-notice');
        } else {
          status(response.title, response.body, 'alert-success');
            location.reload();
        }
        loading.addClass('hide');
      }
      });
    });




// PLAN 
 $("#modal-form-add-plan").find("form").submit(function(e) {
      var form = $("#modal-form-add-plan").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-user_plan_save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-user_plan_notice');
        } else {
          $("#modal-form-add-plan").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

//PLAN RATES
  $("#modal-form-plan-rate").find("form").submit(function(e) {
      var form = $("#modal-form-plan-rate").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form-user_plan_rate_save")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#plan-rate-notice');
        } else {
          $("#modal-form-plan-rate").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
            location.reload();
                  }

        loading.addClass('hide');

      }
      });
    });

 //ADVERTISEMENT MANAGEMENT
  $("#modal-form_ad_management").find("form").submit(function(e) {
      var form = $("#modal-form_ad_management").find("form");
      var loading = $("#ad-management-load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form-ad_management_save")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-ad_management_notice');
        } else if(response.errorbidlimit){
          status(response.title, response.errorbidlimit, 'alert-danger');
          // refresh the list
         refresh();
        } else {
          $("#modal-form_ad_management").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });
  
 // PLAN ADD PRICE
 $("#modal-form-plan-price").find("form").submit(function(e) {
      var form = $("#modal-form-plan-price").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-user_plan_price_save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-plan_price_notice');
        } else {
          $("#modal-form-plan-price").find("form")[0].reset();
          $("#modal-form-plan-price").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });

  // BANNER PLACEMENT PURCHASE
$("#modal-form-purchase-banner").find("form").submit(function(e) {
      var form = $("#modal-form-purchase-banner").find("form");
      var loading = $("#banner-load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      // console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_form_banner")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-banner_notice');
          
        }else if(response.errorVerify){
                status('sample','Action not allowed', 'alert-danger');
        }else if(response.body){

        status(response.title, response.body, 'alert-success');
        $("#modal-form-purchase-banner").modal("hide");

        refreshUserBanner();
        
        }
      
        loading.addClass('hide');
      }

      });
    });


  // ADD BIDDER
 $("#modal-bidding").find("form").submit(function(e) {
      var form = $("#modal-bidding").find("form");
      var loading = $("#load-form_bidder");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      $('.validation-messages').remove();
      // console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_bidder_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          // var errors = '<ul>';

          // $.each(response.error, function(index, value) {
          //   errors += '<li>' + value + '</li>';
            
          // });

          // errors += '</ul>';

          // status("Can't add bid your the latest bidder", errors, 'alert-danger', '#form-bidder_notice');
          $('#validation-errors').prepend("<div class='validation-messages alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a><strong>Error!</strong> Can't add bid because you're the latest bidder.</div>");

        } else if(response.errormsg) {
          $("#modal-bidding").find("form")[0].reset();
          // $("#modal-bidding").modal('hide');
          // status(response.title, response.errormsg, 'alert-danger');
          $('#validation-errors').prepend('<div class="validation-messages alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> '+response.errormsg+'</div>');
          // refresh the list
          // refreshBidders();
        } else if(response.errormsgpoints) {
          $("#modal-bidding").find("form")[0].reset();
          // $("#modal-bidding").modal('hide');
          // status(response.title, response.errormsgpoints, 'alert-danger');
          $('#validation-errors').prepend('<div class="validation-messages alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> '+response.errormsgpoints+'</div>');
          // refresh the list
          // refreshBidders();
        } else if(response.errormsgowner){
          $("#modal-bidding").find("form")[0].reset();
          // $("#modal-bidding").modal('hide');
          // status(response.title, response.errormsgowner, 'alert-danger');
          $('#validation-errors').prepend('<div class="validation-messages alert alert-danger alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> '+response.errormsgowner+'</div>');

          // refresh the list
          // refreshBidders();
        } else {
          $("#modal-bidding").find("form")[0].reset();
          $("#modal-bidding").modal('hide');
          status(response.title, response.body, 'alert-success');
          setTimeout(function(){location.reload();},1500)

          // refresh the list
          // refreshBidders();
        }

        loading.addClass('hide');

      }
      });
    });



   // AD MANAGEMENT SETTINGS SAVE
  $("#modal-form-ad-settings").find("form").submit(function(e) {
      var form = $("#modal-form-ad-settings").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-ad_settings_save_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';

          });
          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-ad_settings_notice');
        } else {
          $("#modal-form-ad-settings").modal('hide');
          $("#modal-form-ad-settings").modal('hide');

          status(response.title, response.body, 'alert-success');

          // refresh the list
          refresh();
        }

        loading.addClass('hide');

      }
      });
    });


    //DASHBOARD AD MAKE AUCTION
     $("#modal-make-auction").find("form").submit(function(e) {
      var form = $("#modal-make-auction").find("form");
      var loading = $("#load-basic-listing-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_make_auction_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-make_auction_notice');
        } else if(response.erroruser){
          $("#modal-make-auction").find("form")[0].reset();
          $("#modal-make-auction").modal('hide');
          status(response.title, response.erroruser, 'alert-danger');
          // refresh the list
          refreshVendorAdsActiveList();
        } else if(response.errorbidlimit){
          status(response.title, response.errorbidlimit, 'alert-danger');
          // refresh the list
         refreshVendorAdsActiveList();
        } else {
          $("#modal-make-auction").find("form")[0].reset();
          $("#modal-make-auction").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refreshVendorAdsActiveList();
        }

        loading.addClass('hide');

      }
      });
    });//DASHBOARD AD START AUCTION
     $("#modal-start-auction").find("form").submit(function(e) {
      var form = $("#modal-start-auction").find("form");
      var loading = $("#load-basic-listing-form");
      // stop form from submitting
      e.preventDefault();

      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_start_auction_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-start_auction_notice');
        } else if(response.erroruser){
          $("#modal-start-auction").find("form")[0].reset();
          $("#modal-start-auction").modal('hide');
          status(response.title, response.erroruser, 'alert-danger');
          // refresh the list
          refreshVendorAdsActiveList();
        } else if(response.errorbidlimit){
          status(response.title, response.errorbidlimit, 'alert-danger');
          // refresh the list
         refreshVendorAdsActiveList();
        } else {
          $("#modal-start-auction").find("form")[0].reset();
          $("#modal-start-auction").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refreshVendorAuctionActive();
        }

        loading.addClass('hide');

      }
      });
    });
 

 $("#modal-favorite-profile").find("form").submit(function(e) {
      var form = $("#modal-favorite-profile").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-save_favorite_profile_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';
          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-favorite_prof_notice');
        } else {
          $("#modal-favorite-profile").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');
        refresh();
      }
      });
    });

     $("#modal-remove-favorite-profile").find("form").submit(function(e) {
      var form = $("#modal-remove-favorite-profile").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#modal-remove_favorite_profile_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';
          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-remove-favorite_prof_notice');
        } else {
          $("#modal-remove-favorite-profile").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');
        refresh();
        
      }
      });
    });


      $("#form-import_contact").find("form").submit(function(e) {
      var form = $("#form-import_contact").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form-save_import")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';
          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-import_contact_notice');
        } else {
          $("#modal-remove-favorite-profile").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');  
      }
      });
    });




//SendReferral Invirtation
       $("#modal-send_referral").find("form").submit(function(e) {
      var form = $("#modal-send_referral").find("form");
      var loading = $("#load-form");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      console.log(form.serialize());
      // push form data
      $.ajax({
            type: "post",
            url: form.attr('action'),
            //data: form.serialize(),
            data: new FormData($("#form-fereral_invitation_form")[0]),
            dataType: "json",   
            async: true,
            processData: false,
            contentType: false,
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';
          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });
          errors += '</ul>';
          status('Please correct the following:', errors, 'alert-danger', '#form-send_referral_invitation_notice');
        } else {
          $("#modal-send_referral").find("form")[0].reset();
          $("#modal-send_referral").modal('hide');
          status(response.title, response.body, 'alert-success');
        }
        loading.addClass('hide');  
      }
      });
    });

// /*!
//  * jQuery JavaScript Library v2.1.4
//  * http://jquery.com/
//  *
//  * Includes Sizzle.js
//  * http://sizzlejs.com/
//  *
//  * Copyright 2005, 2014 jQuery Foundation, Inc. and other contributors
//  * Released under the MIT license
//  * http://jquery.org/license
//  *
//  * Date: 2015-04-28T16:01Z
//  */
