<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yakolak</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" >

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
<div class="container">
<div class="form-register">
<div class="panel panel-default borderzero">
<div class="panel-heading form-signin-heading">LogIN</div>
<div class="panel-body">
 {!! Form::open(array('url' => 'login', 'role' => 'form')) !!}
                @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
                @endif   
<div class="form-group">
<label class="col-xs-12" for="register4-email">Email</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="email" id="register4-email" name="email" placeholder="Enter your email..">
<span class="input-group-addon borderzero"><i class="fa fa-envelope-o"></i></span>
</div>
</div>
</div>
<div class="form-group">
<label class="col-xs-12" for="register4-password">Password</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="password" id="register4-password" name="password" placeholder="Enter your password..">
<span class="input-group-addon borderzero"><i class="fa fa-asterisk"></i></span>
</div>
</div>
</div>
<div class="form-group">
<div class="col-xs-12">
<div class="input-group">
<input type="submit" class="btn btn-sm btn-success borderzero" value="Login">
</div>
</div> 
</div>
     {!! Form::close() !!}
</div>
</div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>