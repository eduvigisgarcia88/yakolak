@extends('layout.frontend')
@section('scripts')
<script>
  function selectContactUs(){
    $(".btn-contact-us").click();
  }
  function selectCallUs(){
    $(".btn-call-us").click();
  }
  function selectAboutUs(){
    $(".btn-about-us").click();
  }
  function selectFaqs(){
    $(".btn-faqs").click();
  }
  function selectAdvertising(){
    $(".btn-advertising").click();
  }
</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 noPadding">
			<div class="col-sm-3">
				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SUPPORT</span>
					</div>
					<div class="panel-body normalText">
						<ul class="nav nav-pills nav-stacked noPadding" role="tablist">
							<li role="presentation"><a href="#contactUs" aria-controls="contactUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i>  Contact us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#helpCenter" aria-controls="helpCenter" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question rightPadding"></i>  Help Center <i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#aboutUs" aria-controls="aboutUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-info rightPadding"></i>  About us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#callUs" aria-controls="callUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-phone rightPadding"> </i> Call us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#termsandCondition" aria-controls="termsandCondition" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-file-text-o rightPadding"></i> Terms and Conditions<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#faqs" aria-controls="faqs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question-circle rightPadding"></i> FAQ's<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#advertising" aria-controls="advertising" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-volume-up rightPadding"></i> Advertising<i class="fa fa-angle-right pull-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-9 noPadding">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="contactUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Contact Us</span>
							</div>
							<div class="panel-body">
								1
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="helpCenter">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Help Center</span>
							</div>
							<div class="panel-body">
								2
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="aboutUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">About us</span>
							</div>
							<div class="panel-body">
								3
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="callUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Call us</span>
							</div>
							<div class="panel-body">
								4
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="termsandCondition">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Terms and Conditions</span>
							</div>
							<div class="panel-body">
								5
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="faqs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">FAQ's</span>
							</div>
							<div class="panel-body">
								6
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="advertising">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Advertising</span>
							</div>
							<div class="panel-body">
								7
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
