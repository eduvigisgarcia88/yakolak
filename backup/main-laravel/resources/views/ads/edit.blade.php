@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
             window.location.href = "";   
        loading.addClass('hide');
    } 

   $("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $("#btn-change_photo").addClass("hide");

    });
    $("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide");
        $(".photo-pane").removeClass("hide");
         $("#btn-change_photo").removeClass("hide");

    });    
</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 noPadding">
      <div class="col-lg-9 col-md-9 col-sm-9 noPadding">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Create Ad</span>
          </div>
          <div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
           
           {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
            <div id="ads-notice"></div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="row-ad_type"  name="ad_type">
                  <option value="basic">Basic Ads</option>
                  <option value="auction">Auction</option>
                </select> 
              </div>
            </div>
                @foreach ($ads as $rows)
                  @endforeach
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title</h5>
              <div class="col-sm-9">
                <input type="text" name="title" class="borderZero form-control borderzero" id="row-title" maxlength="30" value="{{ $rows->title }}">

              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="row-price" name="price" placeholder="" value="{{ $rows->price }}">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
                  <select class="form-control borderZero" id="rows-category"  name="category" >
                   <option value="{{ $rows->category }}">{{ $rows->category }}</option>
                 <option value="Buy and Sell">BUY AND SELL</option>
                @foreach($buy_and_sell as $row)
                  <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                @endforeach
                  <option value="Bidding">BIDDING</option>
                @foreach($biddable as $row)
                  <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                @endforeach   
                  <option value="Cars and Vehicles">CARS AND VEHICLES</option>
                  <option value="Clothings">CLOTHINGS</option>
                  <option value="Events">EVENTS</option>
                  <option value="Jobs">JOBS</option>
                  <option value="Real Estate">REAL ESTATES</option>
                  <option value="Services">SERVICES</option>
                  <option value="All Categories">ALL CATEGORIES</option>
                @foreach($categories as $row)
                 <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
               @endforeach
               </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description</h5>
              <div class="col-sm-9">
                <textarea  class="form-control borderZero" id="row-description" rows="10"  name="description">{{ $rows->description }}</textarea>
              </div>
            </div>

            @foreach ($ads_photos as $ads)
            
           <div class="form-group">     
             <h5 class="col-sm-3 normalText" for="register4-email">Ad Photos (Max 4)</h5>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploadPhoto">
                  <div class="panel panel-default borderzero photo-pane">
                    <div class="panel-body clearfix">
                      <div>
                        <img class="img-responsive" src="{{url('uploads/ads/').'/'.$ads->photo}}">
                        
                      </div>
                    </div>
                  
                  </div>
                </div>
                  <div class="uploader-pane hide">
                    <div class="row">
                        <div class='change col-sm-12'>
                          <input type="hidden" name="photo_id[]" value='<?php echo($ads->id) ?>'>
                           <input name="photo[]" id='ads-photo[]' type='file' class='file borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'  accept="image/*">
                        </div>
                    
                    </div>
                  </div>

              </div>
            </div>

            @endforeach
             <div class="text-right">
                  <button type="button" class="btn btn-primary btn-md borderzero" id="btn-change_photo"><i class="fa fa-camera"></i> CHANGE PHOTOS</button>
                   
               <button type="button" class="btn btn-primary btn-md borderzero" id="btn-cancel"><i class="fa fa-reply"></i></button>
                   </div>
<!-- 
          <div class="form-group">     
              <label for="row-photo" class="col-sm-3 col-xs-5 font-color"></label>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploadPhoto">
                  <div class="panel panel-default borderzero photo-pane">
                    <div class="panel-body clearfix">
                      <div>
                        <img class="img-responsive" src="{{url('uploads/ads/').'/'.$rows->photo[1]}}">
                        
                      </div>
                    </div>
                    <div class="panel-footer text-right">
                      <button type="button" class="btn btn-primary btn-md borderzero" id="btn-change_photo"><i class="fa fa-camera"></i> CHANGE</button>
                    </div>
                  </div>
                </div>
                  <div class="uploader-pane hide">
                    <div class="row">
                        <div class='change col-sm-10'>
                           <input name="photo[1]" id='rows-photo[1]' type='file' class='file borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>
                        </div>
                        <div class="col-sm-1">
                         <button type="button" class="btn btn-primary btn-md borderzero pull-left" id="btn-cancel"><i class="fa fa-reply"></i></button>
                        </div>
                    </div>
                  </div>
              </div>
            </div> 


         <div class="form-group">     
              <label for="row-photo" class="col-sm-3 col-xs-5 font-color"></label>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploadPhoto">
                  <div class="panel panel-default borderzero photo-pane">
                    <div class="panel-body clearfix">
                      <div>
                        <img class="img-responsive" src="{{url('uploads/ads/').'/'.$rows->photo[2]}}">
                        
                      </div>
                    </div>
                    <div class="panel-footer text-right">
                      <button type="button" class="btn btn-primary btn-md borderzero" id="btn-change_photo"><i class="fa fa-camera"></i> CHANGE</button>
                    </div>
                  </div>
                </div>
                  <div class="uploader-pane hide">
                    <div class="row">
                        <div class='change col-sm-10'>
                           <input name="photo[2]" id='rows-photo[2]' type='file' class='file borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>
                        </div>
                        <div class="col-sm-1">
                         <button type="button" class="btn btn-primary btn-md borderzero pull-left" id="btn-cancel"><i class="fa fa-reply"></i></button>
                        </div>
                    </div>
                  </div>
              </div>
            </div> 
         



           <div class="form-group">     
              <label for="row-photo" class="col-sm-3 col-xs-5 font-color"></label>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploadPhoto">
                  <div class="panel panel-default borderzero photo-pane">
                    <div class="panel-body clearfix">
                      <div>
                        <img class="img-responsive" src="{{url('uploads/ads/').'/'.$rows->photo[3]}}">
                        
                      </div>
                    </div>
                    <div class="panel-footer text-right">
                      <button type="button" class="btn btn-primary btn-md borderzero" id="btn-change_photo"><i class="fa fa-camera"></i> CHANGE</button>
                    </div>
                  </div>
                </div>
                  <div class="uploader-pane hide">
                    <div class="row">
                        <div class='change col-sm-10'>
                           <input name="photo[3]" id='rows-photo[3]' type='file' class='file borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>
                        </div>
                        <div class="col-sm-1">
                         <button type="button" class="btn btn-primary btn-md borderzero pull-left" id="btn-cancel"><i class="fa fa-reply"></i></button>
                        </div>
                    </div>
                  </div>
              </div>
            </div> 
 -->




           <!--  <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                <div class="col-sm-9">
                 <input name="photo[3]"  type='file' id="row-photo[3]" class='form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;  
                </div>
            </div> -->
            <h5 class="inputTitle borderbottomLight bottomPadding">Auction Information</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
              <div class="col-sm-9">
                <select type="text" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">
                  <option value="10">Free</option>
                  <option value="30">5$</option>
                  <option value="60">10$</option>
                  <option value="120">20$</option>
                  <option value="150">25$</option>
                  <option value="180">30$</option>
                </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
              <div class="col-sm-9">
                 <select type="text" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount">
                   <option value="10">Free</option>
                   <option value="30">5$</option>
                   <option value="60">10$</option>
                   <option value="120">20$</option>
                   <option value="150">25$</option>
                   <option value="180">30$</option>
                </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="row-minimum_allowed_amount" name="minimum_allowed_amount" placeholder="" value="{{ $rows->minimum_allowed_amount }}">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Duration</h5>
              <div class="col-sm-9">
                <div class="col-sm-6 noleftPadding">
                <div class="form-group">
                  <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                  <div class="col-sm-8">         
                    <input class="form-control borderZero" type="text" id="row-bid_duration_start" name="bid_duration_start" placeholder="" value="{{ $rows->bid_duration_start }}">
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 norightPadding">
                  <div class="form-group">
                    <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                    <div class="col-sm-8">         
                    <select type="text" class="form-control borderZero" id="row-bid_duration_end"  name="bid_duration_end">
                      <option value="hours">Hours</option>
                      <option value="day">Days</option>
                      <option value="week">Weeks</option>
                      <option value="month">Months</option>
                      <option value="year">Years</option>
                    </select> 
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h5 class="inputTitle borderbottomLight bottomPadding">Custom Attribute</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 1</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 2</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 3</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 4</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>

            <div class="bordertopLight topPaddingB">
              <input type="hidden" name="id" value='<?php echo($rows->id) ?>'>
              <input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Update">
            </div>
           
          {!! Form::close() !!}
          </div>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-3">
        
        <div class="col-lg-12 noPadding">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span class="pull-right">10</span></div>
            <hr>
            <div class=""><i class="fa fa-th"></i> Auction Ads Available <span class="pull-right">1</span></div>
          </div>
        </div>
          
        <div class="panel panel-default removeBorder borderZero ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
          <div class="panel-body noPadding">
            <div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
            </div>
          </div>
        </div>
        <div class="panel panel-default removeBorder borderZero ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">FEATURED ADS</span>
          </div>
          <div class="panel-body noPadding">
            <div class="col-lg-12 cornerPadding">
            <div class="col-lg-5 col-xs-4 col-sm-12 left noPadding" >
              <div class="thumbnailSize " style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
              </div>
            </div>
            <div class-"col-lg-7 col-xs-8 col-sm-12 right">
              <div class="mediumText noPadding">Listing Title Here</div>
              <div class="smallText grayText noPadding">By Distrubuter</div>
              <div class="mediumText blueText bottomPadding"><strong>1,000,000 USD</strong></div>
              <div class="normalText  bottomPadding">
                <i class="fa fa-map-marker"></i> Jeddah, UAE
              </div>
            </div>
          </div>
          </div>
        </div>
          
        </div>


      </div>
    </div>
  </div>
</div>

@stop
