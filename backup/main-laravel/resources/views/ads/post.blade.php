@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
             window.location.href = "";   
        loading.addClass('hide');
    } 
</script>
@stop
@section('content')
<div class="container-fluid bgGray noPadding borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
			<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">Create Ad</span>
					</div>
					<div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
            <div id="ads-notice"></div>
				   {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
						<div class="form-group">
               <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-ad_type"  name="ad_type">
                  <option value="basic">Basic Ads</option>
                  <option value="auction">Auction</option>
                </select> 
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title</h5>
              <div class="col-sm-9">
                <input type="text" name="title" class="borderZero form-control borderzero" id="ads-title" maxlength="30">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="ads-price" name="price" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-category"  name="category">
                   <option value="">Select a Category</option>
                 <option value="Buy and Sell">BUY AND SELL</option>
                @foreach($buy_and_sell as $row)
                  <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                @endforeach
                  <option value="Bidding">BIDDING</option>
                @foreach($biddable as $row)
                  <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                @endforeach   
                  <option value="Cars and Vehicles">CARS AND VEHICLES</option>
                  <option value="Clothings">CLOTHINGS</option>
                  <option value="Events">EVENTS</option>
                  <option value="Jobs">JOBS</option>
                  <option value="Real Estate">REAL ESTATES</option>
                  <option value="Services">SERVICES</option>
                  <option value="All Categories">ALL CATEGORIES</option>
                @foreach($categories as $row)
                 <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
               @endforeach
               </select>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description</h5>
              <div class="col-sm-9">
                <textarea  class="form-control borderZero" id="ads-description" rows="10"  name="description"></textarea>
              </div>
            </div>
                  <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email">Ad Photos (Max 4)</h5>
               <div class="col-sm-9">
                <!-- <img src ="" id ="row-photo" height="150" width="150" class="img-responsive"> -->
                <!-- <input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text"> -->
                <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
               <input name="photo[0]" type='file' id="row-photo[0]" class='file form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...' accept="image/*">&nbsp;
                      </div>
                </div>
              </div>
              </div>
                 <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
               <div class="col-sm-9">
                <!-- <img src ="" id ="row-photo" height="150" width="150" class="img-responsive"> -->
                <!-- <input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text"> -->
                <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
               <input name="photo[1]" type='file' id="row-photo[1]" class='file form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...' accept="image/*">&nbsp;
                      </div>
                </div>
              </div>
              </div>
                 <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
               <div class="col-sm-9">
                <!-- <img src ="" id ="row-photo" height="150" width="150" class="img-responsive"> -->
                <!-- <input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text"> -->
                <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
               <input name="photo[2]" type='file' id="row-photo[2]" class='file form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...' accept="image/*">&nbsp;
                      </div>
                </div>
              </div>
              </div>
                 <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
               <div class="col-sm-9">
                <!-- <img src ="" id ="row-photo" height="150" width="150" class="img-responsive"> -->
                <!-- <input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text"> -->
                <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
               <input name="photo[3]" type='file' id="row-photo[3]" class='file form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...' accept="image/*">&nbsp;
                      </div>
                </div>
              </div>
              </div>
<!--      
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Ad Photos (Max 4)</h5>
                 <div class="col-sm-9">
              <input name="photo[0]"  type='file' id="ads-photo[0]" class='form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;  
              </div>
            </div>
            <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                 <div class="col-sm-9">
                   <input name="photo[1]"  type='file' id="ads-photo[1]" class='form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;  
                 </div>
            </div>
            <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                <div class="col-sm-9">
                 <input name="photo[2]"  type='file' id="ads-photo[2]" class='form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;  
                </div>
            </div>
            <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                <div class="col-sm-9">
                 <input name="photo[3]"  type='file' id="ads-photo[3]" class='form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;  
                </div>
            </div> -->
            <h5 class="inputTitle borderbottomLight bottomPadding">Auction Information</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
              <div class="col-sm-9">
                <select type="text" class="form-control borderZero" id="ads-bid_limit_amount"  name="bid_limit_amount">
                  <option value="10">Free</option>
                  <option value="30">5$</option>
                  <option value="60">10$</option>
                  <option value="120">20$</option>
                  <option value="150">25$</option>
                  <option value="180">30$</option>
                </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
              <div class="col-sm-9">
                 <select type="text" class="form-control borderZero" id="bid_start_amount"  name="bid_start_amount">
                   <option value="10">Free</option>
                   <option value="30">5$</option>
                   <option value="60">10$</option>
                   <option value="120">20$</option>
                   <option value="150">25$</option>
                   <option value="180">30$</option>
                </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="minimum_allowed_amount" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Bid Duration</h5>
              <div class="col-sm-9">
                <div class="col-sm-6 noleftPadding">
                <div class="form-group">
                  <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                  <div class="col-sm-8">         
                    <input class="form-control borderZero" type="text" id="ads-bid_duration_start" name="bid_duration_start" placeholder="">
                    </div>
                  </div>
              	</div>
                <div class="col-sm-6 norightPadding">
                	<div class="form-group">
               			<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                    <div class="col-sm-8">         
                    <select type="text" class="form-control borderZero" id="ads-bid_duration_end"  name="bid_duration_end">
                      <option value="hours">Hours</option>
                      <option value="day">Days</option>
                      <option value="week">Weeks</option>
                      <option value="month">Months</option>
                      <option value="year">Years</option>
                    </select> 
                    </div>
                	</div>
              	</div>
              </div>
            </div>
            <h5 class="inputTitle borderbottomLight bottomPadding">Custom Attribute</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 1</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 2</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 3</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Attribute 4</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="register4-email" name="text" placeholder="">
              </div>
            </div>
            <div class="bordertopLight topPaddingB">
            	<input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Post">
          	</div>
          {!! Form::close() !!}
					</div>
        </div>
        </div>
				</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span class="pull-right">10</span></div>
            <hr>
            <div class=""><i class="fa fa-th"></i> Auction Ads Available <span class="pull-right">1</span></div>
          </div>
        </div>
					
					
					
					
					
					
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
					<div class="panel panel-default bottomMarginLight removeBorder borderZero">
						<div class="panel-heading panelTitleBarLightB">
							<span class="panelRedTitle">ADVERTISEMENT</span>
						</div>

						<div class="panel-body noPadding">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
								<div class="">
									<div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
								<div class="rightPadding nobottomPadding">
									<div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
									<div class="normalText redText bottomPadding">
										by Dell Distributor
									</div>
									<div class="mediumText blueText bottomPadding">
										<strong>1,000,000 USD</strong>
									</div>
									<div class="mediumText bottomPadding">
									<i class="fa fa-map-marker"></i> 
										Jeddah, UAE
									</div>
									<div class="mediumText blueText">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-empty"></i>
										<span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
									</div>
								</div>
							</div>
						</div>
						</div>

					</div>
					<div class="panel panel-default marginbottomHeavy removeBorder borderZero">
						<div class="panel-body noPadding">
							<div class="col-sm-12 col-xs-12 noPadding">
								<div class="">
									<div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
								<div class="rightPadding nobottomPadding">
									<div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
									<div class="normalText redText bottomPadding">
										by Dell Distributor
									</div>
									<div class="mediumText blueText bottomPadding">
										<strong>1,000,000 USD</strong>
									</div>
									<div class="mediumText bottomPadding">
									<i class="fa fa-map-marker"></i> 
										Jeddah, UAE
									</div>
									<div class="mediumText blueText">
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-empty"></i>
										<span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
									</div>
								</div>
							</div>
						</div>
						</div>
					</div>
				</div>
					

          
        </div>


      </div>
		</div>
	</div>
</div>

@stop
