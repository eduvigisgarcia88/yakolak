<!DOCTYPE html>
<html lang="en-US">
  <head>
		<meta charset="utf-8">
	
  </head>
  <body style="font-family: Helvetica, Arial, sans-serif;">
  <h2><mark style="background: #ffe599 !important;">Yakolak</mark></h2>
  	<div style="margin-top: 20px;">
	  	@yield('content')
		</div>
		<div style="margin-top: 20px; border-top: 1px solid #eee; padding-top: 10px; font-size: 0.9em;">
			<p>&copy; {{ date('Y') }} <mark style="background: #ffe599 !important;">Yakolak<mark></p>
			<p>(<u style="color:red;">Yakolak</u> ONLY)</p>
		</div>
  </body>
</html>	