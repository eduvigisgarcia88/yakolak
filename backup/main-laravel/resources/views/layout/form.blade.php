<!--modal login -->
<div class="modal fade" id="modal-login" role="dialog">
  <div class="modal-dialog modal-sm" id="modal-login_form">
    <div class="modal-content removeBorder loginSize borderZero">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
          <h4 class="modal-title panelTitle">Login</h4>
        </div>
        <div class="modal-body">
             @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
             @endif
            {!! Form::open(array('url' => 'login', 'role' => 'form', 'class' => 'form-horizontal')) !!} 
              <div class="form-group">
                <h5 class="col-sm-3 normalText" for="register4-email">Email</h5>
                <div class="col-sm-9">
                  <input class="form-control borderZero inputBox" type="email" id="register4-email" name="email" placeholder="Enter your email..">
                </div>
              </div>
              <div class="form-group">
                <h5 class="col-sm-3 normalText" for="register4-password">Password</h5>
                <div class="col-sm-9">          
                   <input class="form-control borderZero inputBox" type="password" id="register4-password" name="password" placeholder="Enter your password..">
                </div>
              </div>
          <!--     <div class="form-group">
                <h5 class="col-sm-3 normalText" for="">Security</h5>
                <div class="col-sm-9">
                  <input class="form-control borderZero inputBox" type="email" id="register4-" name="captcha" placeholder="Captcha..">
                </div>
              </div> -->
                       <div class="form-group">
                      <h5 class="col-xs-3 normalText" for="register4-password">Security</h5>
                      <div class="col-xs-9 noPadding">           
              // <div id="RecaptchaField1" class="col-md-12"></div>

                </div>
                  </div>
              <div class="row">
              <div class="bordertopLight modalSocialMedia">
                <a class="btn btn-block btn-social facebookButton borderZero">
                  <i class="fa fa-facebook"></i> Log in with Facebook
                </a>
                <a class="btn btn-block btn-social twitterButton borderZero">
                  <i class="fa fa-twitter"></i> Log in with Twitter
                </a>
                <a class="btn btn-block btn-social googleButton borderZero">
                  <i class="fa fa-google-plus"></i> Log in with Google
                </a>
              </div>
            </div>
        </div>
        <div class="modal-footer bordertopLight borderBottom">
          <span class="pull-left normalText redText topPadding">Not yet a member? 
            <a href="#" class="normalText redText topPadding" data-dismiss="modal" data-toggle="modal" data-target="#modal-register">Register Now!</a>
          </span>
          <span class="pull-right">
            <button type="submit" class="btn blueButton borderZero">Login</button>
          </span>
        </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
<!--modal register -->
<div class="modal fade" id="modal-register" role="dialog" data-backdrop="static">
  <div class="modal-dialog modal-md">
    <div class="modal-content removeBorder borderZero">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
          <h4 class="modal-title panelTitle">Register</h4>
        </div>
        <div class="modal-body">
          <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
          </div>
          <div id="register-notice"></div>
          <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
          {!! Form::open(array('url' => 'register/save', 'role' => 'form', 'class' => 'form-horizontal','files'=>'true','id'=>'modal-register_form')) !!} 
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email Address</h5>
              <div class="col-sm-9">
                <input type="email" class="form-control borderZero inputBox" name="email" id="client-email" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Password</h5>
              <div class="col-sm-9">
                <input type="password" class="form-control borderZero inputBox" name="password" id="client-password" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Confirm Password</h5>
              <div class="col-sm-9">
                <input type="password" class="form-control borderZero inputBox" name="password_confirmation" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">User Type</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-usertype" name="usertype">
                  @foreach($usertypes as $row)
                    <option value="{{$row->id}}">{{$row->type_name}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3 common-name-pane" for="email">Vendor Name</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="name" id="client-name" placeholder="">
              </div>
            </div>
            <br>
            <h5 class="borderbottomLight bottomPadding">
              <a href="#collapseOne" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="inputTitle">Additional Information<span class="notrequiredText"> ( Not Required )</span><span class="pull-right"><i class="fa fa-angle-double-down"></i></span></a>
            </h5>
            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
               <div class="form-group">
                <h5 class="normalText col-sm-3" for="email">Account Photo</h5>
                <div class="col-sm-9">
                  <div class="col-sm-4 photo-pane">
                    <img src="{{ url('uploads/default').'/'}}default_image.png" id="client-photo" class="img-responsive borderZero" height="100" width="100" >
                  </div>
                  <div class="col-sm-4 button-pane">
                    <button type="button" class="btn btn-primary fullSize btn-sm borderZero paddingBottom" id="btn-change_photo" style="">CHANGE</button>
                  </div>
                  <div class="col-sm-12 nosidePadding topPadding">
                  <div class="uploader-pane hide">
                      <div class='change'>
                        <input name='photo' id='row-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo...'>
                        <button type="button" class="borderZero btn btn-primary topMargin borderzero fullSize" id="btn-cancel">Cancel</button>
                      </div>
                  </div>
              </div>
                </div>
            </div>
           <!--  <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="email" id="client-email" placeholder="">
              </div>
            </div> -->
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Telephone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="telephone_no" id="client-tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Cellphone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="mobile" id="client-mobile" placeholder="">
              </div>
            </div>
            <div id="gender" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Gender</h5>
              <div class="col-sm-9">
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-male" value="m">Male</label>
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-female" value="f">Female</label>
               <input type="hidden" name="gender" id="client-gender">
              </div>
            </div>
            <div id="startDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Company Start Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-company_start_date">
                   <input type="text" name="company_start_date" class="form-control borderZero inputBox" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-country" name="country">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-city" name="city">
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="address" id="client-address" placeholder="">
              </div>
            </div>
            <div id="birthDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Birth Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-date_of_birth">
                   <input type="text" name="date_of_birth" class="form-control borderZero inputBox" id="client-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div id="officeHours" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Office Hours</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" namee="office_hours" id="client-office_hours" placeholder="">
              </div>
            </div>
            <div id="officeHours" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Youtube</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" namee="youtube_account" id="client-youtube" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Website</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="website" id="client-website" placeholder="">
              </div>
            </div>
            <div id="branchContainer">
            <section id="addBranch" class="container-fluid">
            <h5 class="grayText borderbottomLight bottomPadding">
              BRANCH<span class="notrequiredText"> ( Not Required )</span><button type="button" value="" class="hide-view btn-add-branch pull-right borderZero inputBox"><i class="fa fa-plus redText"></i></button>
            </h5>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Telephone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_tel_no[]" id="client-company_tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Cellphone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_mobile[]" id="client-company_mobile" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox company_country" id="client-company_country" name="company_country[]">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                 <select class="form-control borderZero inputBox" id="client-company_city" name="company_city[]">
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_address[]" id="client-company_address" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_email[]" id="client-company_email" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Office Hours</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_office_hours[]" id="client-company_office_hours" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Website</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_website[]" id="company-website" placeholder="">
              </div>
            </div>
            <input type="hidden" name="company_branch_id[]">
            </section>
            </div>
            </div>
            <h5 class="borderbottomLight bottomPadding">
              <a href="#collapseTwo" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo" class="inputTitle">Social Media<span class="notrequiredText"> ( Not Required )</span><span class="pull-right"><i class="fa fa-angle-double-down"></i></span></a>
            </h5>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Facebook</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="facebook_account" id="client-facebook" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Twitter</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="twitter_account" id="client-twitter" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Instagram</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="instagram_account" id="client-instagram" placeholder="">
              </div>
            </div>
            </div>
        </div>
        <div class="modal-footer bordertopLight borderBottom">
          <span class="pull-left normalText redText topPadding">Already a member? 
            <a href="#" class="normalText redText topPadding" data-dismiss="modal" data-toggle="modal" data-target="#modal-login">Login Now!</a>
          </span>
          <span class="pull-right">
            <button type="submit" class="btn blueButton borderZero">Register</button>
          </span>
        </div>
      </div>
    </div>
      {!! Form::close() !!}

  </div>

  