<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yakolak</title>

    <!-- Bootstrap -->
     {!! HTML::style('css/bootstrap/bootstrap.min.css') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- Font Awesome -->
      {!! HTML::style('font-awesome/css/font-awesome.min.css') !!}
      <!-- Glyphicons -->
     <!--  {!! HTML::style('font/font-awesome.min.css') !!} -->


      <!-- Plugins -->
       <!-- Editor -->
      <!-- Xerolabs -->
      {!! HTML::style('/css/xerolabs/style.css') !!}
      {!! HTML::style('/css/xerolabs/common.css') !!}
      {!! HTML::style('/css/xerolabs/frontend/custom.css') !!}
      {!! HTML::style('/plugins/css/toastr.min.css') !!}
      {!! HTML::style('/plugins/css//fileinput.min.css') !!}
      {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
      <!-- captcha -->
    <!--  <script src="//www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script> -->

</head>
<body>

@if (Auth::guest())
<div class="modal fade" id="modal-ads" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content borderZero">
      <div class="modal-header modal-warning">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Warning</h4>
        </div>
        <div class="modal-body">
        <h5>Your not logged in!</h5>
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default borderZero" data-dismiss="modal">Close</button>
          <a href="{{ url('auth/login') }}"><button type="submit" class="btn btn-warning borderZero" data-dismiss="modal">Login</button></a>

        </div>
      </div>
    </div>
  </div>
</div>
 @else
  <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderZero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn btn-default borderZero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

 @endif
<!-- header start -->
<nav class="navbar navbar-default navbar-fixed-top bgWhite borderBottom">
  <div class="container noPadding">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 leftPadding">
        <div class="col-xs-3 col-sm-3 minPadding hidden-md hidden-lg noleftPadding">
          <button class="btn noBGButton menuToggle borderZero sidebar-btn" type="button"> <i class="fa fa-bars"></i> </button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-9 col-xs-9 leftPadding ">
             <a class="navbar-brand noleftPadding logo" href="#">
               <img src="{{ url('img').'/'}}yakolaklogo.png" height="" class="img-responsive">
             </a>
        </div>
      </div>
      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-7 pull-right;" >
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" style="padding-top:3px" >
                <div class="input-group minPadding noleftPadding norightPadding text-right" >           
                  <input type="text" class="form-control borderZero inputBox inputSearch" placeholder="Search for...">

                  <span class="input-group-btn">
                    <a href="{{ url('search') }}"><button class="btn blueButton borderZero hidden-xs" type="button">Search</button></a>
                    <button class="btn blueButton borderZero leftMargin hidden-sm hidden-md hidden-lg" type="button"><i class="fa fa-search"></i></button>
                    <a href="{{ url('ads/post') }}" class="hidden-xs hidden-sm"><button class="btn mediumText redButton borderZero postAds" type="button">Post Ads</button></a>
                  </span>
                </div>
        </div>
        <div class="collapse navbar-collapse col-lg-12 col-md-12 col-sm-12 col-xs-12 bottomMargin pull-right">
                <ul class="nav navbar-nav hidden-sm">
                  <li><a href="{{ url('buy') }}">Buy</a></li>
                  <li><a href="{{ url('ads/post') }}">Sell</a></li>
                  <li><a href="{{ url('bid') }}">Bid</a></li>
                  <li><a href="{{url('plans')}}">Plans</a></li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categories
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('category/list')}}">Buy and Sell</a></li>
                      <li><a href="#">Live Auctions</a></li>
                      <li><a href="#">Cars and Vehicles</a></li> 
                      <li><a href="#">Clothing</a></li>
                      <li><a href="#">Events</a></li>
                      <li><a href="#">Jobs</a></li>
                      <li><a href="#">Real Estate</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">All Categories</a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Support
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('support')}}" onclick="selectContactUs()">Contact us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAboutUs()">About us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectCallUs()">Call us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectFaqs()">FAQ's</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAdvertising()">Advertising</a></li>
                    </ul>
                  </li>
                  @if(!Auth::check())
                  <li class="borderleftLight"><a href="#" class="btn-login" data-toggle="modal" data-target="#modal-login">Login</a></li>
                   <li class="borderleftLight"><a href="#" class="btn-login" data-toggle="modal" data-target="#modal-register">Register</a></li>
                  @else
                  <li class="borderleftLight"><a href="#" data-toggle="modal" class="button-user">{{Auth::user()->name}}</a></li>

                  <li class="borderleftLight"><a class="redText" href="{{ url('messages') }}">3 <i class="fa fa-envelope"></i></a></li>
                  <li><a href="#">7 <i class="fa fa-flag"></i></a></li>
                   @endif
                  <li class="borderleftLight dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">EN
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">English</a></li>
                      <li><a href="#">Chinese</a></li>
                      <li><a href="#">Arabic</a></li>
                    </ul>
                  </li>
                </ul>
        </div>
      </div>

    </div>
  </div>
</nav>
  @if (!(Auth::guest()))
  <div id="" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding userPanel">
  <div class="container">
          <ul class="normalText nav navbar-nav navbar-left">
            <li style="padding: 11px;"><strong><a class="whiteText" href="#">Account Navigation:</a></strong></li>
            <li><a class="whiteText" href="{{url('dashboard')}}">Dashboard</a></li>
            <li><a class="whiteText" href="{{url('dashboard')}}">Public Page</a></li>
            <li><a class="whiteText" href="{{url('dashboard/messages')}}">Messages</a></li>
            <li><a class="whiteText" href="{{url('dashboard/listings')}}">Listings</a></li>
            <li><a class="whiteText" href="{{url('dashboard/bids')}}">Bids</a></li>
            <li><a class="whiteText" href="{{url('dashboard/watchlist')}}">Watchlist</a></li>
            <li><a class="whiteText" href="{{url('dashboard/settings')}}">Settings</a></li>
          </ul>
           <ul class="normalText nav navbar-nav navbar-left pull-right">
            <li class="pull-right"><a class="whiteText" href="{{ url('logout') }}">Logout</a></li>
          </ul>
        </div>
        </div>
@endif
<div id="mobilesidebarAccount" class="visible-sm visible-xs">
  <h4 class="whiteText sidebarTitle">Welcome @if (!(Auth::guest())) {{Auth::user()->name}} @endif</h4>
 
  <li><a class="whiteText normalText" href="{{url('')}}">Home</a></li>
   @if (Auth::guest())
  <li><a class="whiteText normalText" href="#" class="btn-login" data-toggle="modal" data-target="#btn-login">Login</a></li>
  <li><a class="whiteText normalText" href="#" class="btn-login" data-toggle="modal" data-target="#btn-register">Register</a></li>
  @else
  <li><a class="whiteText normalText normalText" href="{{url('dashboard')}}">Dashboard</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/messages')}}">Public Page</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/messages')}}">Messages</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/listings')}}">Listings</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/bids')}}">Bids</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/watchlist')}}">Watchlist</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/settings')}}">Settings</a></li>
  <li><a class="whiteText normalText" href="{{url('logout')}}">Logout</a></li>
<!--   <li class="borderbottomLight"></li> -->
  @endif
  <li><a class="whiteText normalText" href="">Post Ads</a></li>
  <li><a class="whiteText normalText" href="">Buy</a></li>
  <li><a class="whiteText normalText" href="">Sell</a></li>
  <li><a class="whiteText normalText" href="">Bid</a></li>
  <li><a class="whiteText normalText" href="">Categories</a></li>
  <li><a class="whiteText normalText" href="">Support</a></li>
  <li><a class="whiteText normalText" href="">Language</a></li>
</div>
<!-- <div id="mobilesidebarCategory" class="visible-sm visible-xs">
  <h4 class="whiteText leftMarginB">Categories</h4>
  <div class="form-group" id="accordion">
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" class="whiteText" data-toggle="collapse" data-parent="#accordion" href="#buyandsell">Buy and Sell</a>
      <div id="buyandsell" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#bidding">Bidding</a>
      <div id="bidding" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#carsandvehicles">Cars and Vehicles</a>
      <div id="carsandvehicles" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#clothings">Clothings</a>
      <div id="clothings" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#events">Events</a>
      <div id="events" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#jobs">Jobs</a>
      <div id="jobs" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#realestate">Real Estate</a>
      <div id="realestate" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#services">Services</a>
      <div id="services" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#all">All Categories</a>
      <div id="all" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
  </div>
  </div> -->
<!-- header end -->
<div id="wrapper">
 <div id="content">
     @yield('content')
 </div>
</div>
@if(Route::current()->getPath() == 'ads/post' || Route::current()->getPath() == 'profile' || Route::current()->getPath() == 'search' || Route::current()->getPath() == 'plans' || Route::current()->getPath() == 'support' || Route::current()->getPath() == 'dashboard' || Route::current()->getPath() == 'buy' || Route::current()->getPath() == 'bid' || Route::current()->getPath() == 'sell' || Route::current()->getPath() == 'support/contact-us' || Route::current()->getPath() == 'support/call-us' || Route::current()->getPath() == 'support/about-us' || Route::current()->getPath() == 'support/faqs' || Route::current()->getPath() == 'support/advertising' || Route::current()->getPath() == 'support/help-us' || Route::current()->getPath() == 'ads/view/{id}' ||  Route::current()->getPath() == 'dashboard/messages' || Route::current()->getPath() == 'dashboard/listing' || Route::current()->getPath() == 'dashboard/bids' || Route::current()->getPath() == 'dashboard/watchlist' || Route::current()->getPath() == 'dashboard/settings' || Route::current()->getPath() == 'user/vendor' | Route::current()->getPath() == 'ads/edit/{id}')


@else
<!-- main page start -->
<div class="container-fluid noPadding bgWhite borderBottom">
<div class="container bannerJumbutron">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
  <div class="col-lg-3 col-md-3 col-md-3 noPadding hidden-xs hidden-sm">
    <h4 class="listTitle leftPaddingB">CATEGORIES</h4>
<div id='flyout_menu'>
<ul class="main">
  <li class=""><a href="#" class="grayText">Buy and Sell <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Live Auctions <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Cars and Vehicles <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Clothing <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Events <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Jobs <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Real Estate <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="grayText">Services <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="#" class="redText">All Categories <i class="fa fa-angle-right pull-right"></i></a>
</ul>
</div>
<!--     <div id="sidebar">
    <ul class="nav nav-pills nav-stacked">
      <li class=""><a href="#" class="listCategory">Buy and Sell <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="subMenu">
        <li><a href="#" class="">Page 1-1</a></li>
        <li><a href="#" class="listCategory whiteText">Page 1-2</a></li>
        <li><a href="#" class="listCategory whiteText">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Bidding <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Cars and Vehicles <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Clothings <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Events <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Jobs <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Real Estate <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Services <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategoryAll textRed">All Categories <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
    </ul>
  </div> -->
  </div>
  
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 noPadding">
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
      <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainA.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainB.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    <div class="item">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainC.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        advertising
      </div> -->
    </div>
  </div>

  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
     <!--  <a>
        <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; height: 400px; width: auto;">
        </div>
      </a> -->
    </div>

    
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 noPadding topPaddingSm">
      <div class="col-sm-6  col-md-12 col-sm-6 col-xs-12 bottomPadding ">
<div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniA-1.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniA-2.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
  </div>

  <!-- Controls -->
 <!--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
        <!-- <div class="" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;"> -->
        </div>
      <div class="col-sm-6 col-md-12 col-sm-6 col-xs-12 notoppaddingSm topPadding">
        <div id="carousel-example-generic" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniB-1.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniB-2.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
  </div>

  <!-- Controls -->
<!--   <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
       <!--  <div class="" style="background: url({{ url('img').'/'}}6.jpg); background-repeat: no-repeat; background-size: cover; height: 200px; width: auto;"> -->
        </div>
      </div>
      <div class="clearfix visible-md-block visible-lg-block"></div>
    </div>
  </div>
</div>
</div>
</div>
<!-- main page end -->

<!-- latest bids start -->
<div class="container-fluid bgGray">
<div class="container blockMargin noPadding">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Ads</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
      <span class="redText pull-right">view more</span>
    </div>

@foreach ($adslatestbidsxs as $row)
    <div class="panel-body noPadding">
      <div class="col-sm-6 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            D: 00 - H: 00 - M: 00
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
@endforeach

  </div>
<!--   <div class="panel panel-default marginbottomHeavy removeBorder borderZero">
    <div class="panel-body noPadding">
      <div class="col-sm-6 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding topmarginveryLight">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            D: 00 - H: 00 - M: 00
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div> -->
</div>

<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs" >
    <div class="panel panel-default  removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
        <span class="redText pull-right">view more</span>
      </div>
      <div class="panel-body">
        <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">


          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
            <div class="col-lg-12 col-md-12">


  @foreach ($adslatestbids as $row)
              <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                 <!--   <img src="{{url('uploads/ads/thumbnail').'/'.$row->photo}}" class="img-responsive" style="width:100%; height: 200px;" alt="Image">
                   --></div>
                  <div class="minPadding nobottomPadding">
                    <a href="{{ url('/ads/view') . '/' . '1' }}"><div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}dddddd</div></a>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div>
  @endforeach
             <!--  <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div> -->
           <!--    <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div> -->
    <!--           <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div> -->
              
            </div>
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; left:-10px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; right:-10px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
  </div>
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default bottomMarginLight removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
      <span class="redText pull-right">view more</span>
    </div>
     
    <div class="panel-body noPadding">
      <div class="col-sm-12 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            by Dell Distributor
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>

  </div>
  <div class="panel panel-default marginbottomHeavy removeBorder borderZero">
    <div class="panel-body noPadding">
      <div class="col-sm-12 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            by Dell Distributor
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
    <div class="panel panel-default removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our members latest ads now!</span>
        <span class="redText pull-right">view more</span>
      </div>
      <div class="panel-body">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <!-- Indicators -->


          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            <div class="item active">
              <div class="item active">
              <div class="col-lg-12">
                @foreach ($adslatestbuysell as $row)
              <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
<!--               <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div> -->
     <!--          <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div> -->
<!--               <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div> -->  
            </div>
          </div>
<!--         <div class="item active">
          <div class="col-lg-12">
            <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Titdddddle Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>  
             <div class="col-lg-3 col-md-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by Dell Distributor
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i>Jeddah, UAE
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>    
            </div>
          </div> -->
        </div>
      </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:230px; left:-10px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:230px; right:-10px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
      </div>
    </div>
    </div>
  </div>
<!-- latest end -->

<!-- main information start -->
<div class="container-fluid bgGray ">
  <div class="container noPadding blockBottom">
    <div class="col-lg-12 col-md-12 noPadding">

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
      <div class="panel panel-default removeBorder borderZero borderBottom">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Panel Heading</span>
        </div>
        <div class="panel-body">
          <p>New Listing in last 24 hours</p>
          <h4 class="blueText">48,670</h4>
          <p>No. of comments</p>
          <h4 class="blueText">10,001</h4>
          <p>Premium listings</p>
          <h4 class="blueText">300</h4>
          <p>Total Ad Listings</p>
          <h4 class="blueText">48,670</h4>
        </div>
      </div>  
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
      <div class="panel panel-default removeBorder borderZero borderBottom">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Panel Heading</span>
        </div>
        <div class="panel-body">
          <p>New Listing in last 24 hours</p>
          <h4 class="blueText">48,670</h4>
          <p>No. of comments</p>
          <h4 class="blueText">10,001</h4>
          <p>Premium listings</p>
          <h4 class="blueText">300</h4>
          <p>Total Ad Listings</p>
          <h4 class="blueText">48,670</h4>
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
      <div class="panel panel-default removeBorder borderZero borderBottom">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Panel Heading</span>
        </div>
        <div class="panel-body">
          <p>New Listing in last 24 hours</p>
          <h4 class="blueText">48,670</h4>
          <p>No. of comments</p>
          <h4 class="blueText">10,001</h4>
          <p>Premium listings</p>
          <h4 class="blueText">300</h4>
          <p>Total Ad Listings</p>
          <h4 class="blueText">48,670</h4>
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
      <div class="panel panel-default removeBorder borderZero borderBottom">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Panel Heading</span>
        </div>
        <div class="panel-body">
          <p>New Listing in last 24 hours</p>
          <h4 class="blueText">48,670</h4>
          <p>No. of comments</p>
          <h4 class="blueText">10,001</h4>
          <p>Premium listings</p>
          <h4 class="blueText">300</h4>
          <p>Total Ad Listings</p>
          <h4 class="blueText">48,670</h4>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>
@endif
<!-- main information end -->
              
<!-- main footer start -->
<div class="container-fluid bgWhite borderBottom ">
  <div class="container noPadding ">
    <div class="col-lg-12 col-md-12 col-sm-12 noPadding">

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Customer Service</span>
        </div>
        <div class="panel-body normalText">
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectFaqs()">Help Center</a></p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectContactUs()">Contact Us</a></p>
          <p>Terms and Conditions</p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectFaqs()">Frequently Asked Questions</a></p>
          <p>&nbsp;</p>
          <p>Facebook Page</p>
          <p>Twitter</p>
          <p>Google Plus</p>
        </div>
      </div>  
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">About Yakolak</span>
        </div>
        <div class="panel-body normalText">
          <p>Company Information</p>
          <p>Mission and Vision</p>
          <p>&nbsp;</p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()">Advertising</a></p>
          <p>Sitemap</p>
        </div>
      </div>
      </div>

      <div class="clearfix visible-xs-block visible-sm-block"></div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Sell on Yakolak</span>
        </div>
        <div class="panel-body normalText">
          <p><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#btn-register">Create an account</a></p>
          <p><a href="{{url('ads/post')}}">Publish Ads</a></p>
          <button class="btn redButton borderZero noMargin fullSize" type="button">LIST AD FOR FREE</button>
          <p>&nbsp;</p>
          <p>Account Upgrade</p>
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="panelTitle">Subscribe</span>
        </div>
        <div class="panel-body normalText">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Email Address">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="First Name">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Last Name">
          <button class="btn blueButton borderZero noMargin fullSize" type="button">SUBSCRIBE NOW</button>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>

@include('layout.form')
<div class="container-fluid bgGray">
<div class="container noPadding">
<div class="col-lg-12 ">
  <div class="col-lg-12 minPadding noleftPadding">© 2016 Yakolak.com. All rights reserved.</div>
</div>
</div>
</div>

<!-- main footer end -->

<!-- search start -->

<!-- sea    
        
      </div>
    </div>
  </div>
</div>
<!-- account end -->


<!-- NAVIGATOR -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
   <!-- jQuery -->
{!! HTML::script('/js/jquery.js') !!}
{!! HTML::script('/js/form.js') !!}
{!! HTML::script('/plugins/js//toastr.min.js') !!}
{!! HTML::script('/plugins/js/fileinput.min.js') !!}
{!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
{!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
 
    <!-- Bootstrap -->
{!! HTML::script('/js/bootstrap.min.js') !!}
    <!-- Editor -->
{!! HTML::script('/plugins/js/summernote.js') !!}
 @yield('scripts')
<script type="text/javascript">



 var CaptchaCallback = function(){
  grecaptcha.render('RecaptchaField1', {'sitekey' : '6LfVrhgTAAAAANeObYO4iJuGHYZByzN3xhDdEePp'});
  grecaptcha.render('RecaptchaField2', {'sitekey' : '6LfVrhgTAAAAANeObYO4iJuGHYZByzN3xhDdEePp'});
};

$(document).ready(function() {
$(".sidebar-btn").click(function(){
    $("#mobilesidebarAccount").toggleClass("active");
    $("#mobilesidebarCategory").removeClass("active");
});
$(".searchToggle").click(function(){
  //$(this).find('i').fadeToggle()('fa-search ', 1000).fadeToggle()('fa-close', 1000);

  $(this).find('i').toggleClass('fa-search fa-close')
  $(this).find('i').fadeOut(150);
  $(this).find('i').fadeIn(150);
    $("#mobileSearchpad").toggleClass("active");
});
  $('[data-toggle=offcanvas]').click(function() {
    $('#sidebar-wrapper').toggleClass('active');
  });
   $(function () {
        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
      });

$(".button-user").click(function(){
    $(".userPanel").toggleClass("active");
});
$("#client-usertype").change(function(){
    $(this).find("option:selected").each(function(){
        if($(this).attr("value")=="2"){
            $("#addBranch").show();
            $("#startDate").show();
            $("#officeHours").show();
            $("#gender").hide();
            $("#birthDate").hide();
            $(".common-name-pane").text("Company Name");
            // $(".common-name-pane").html('<h5 class="normalText col-sm-3" for="email">Company Name</h5><div class="col-sm-9"><input type="text" class="form-control borderZero inputBox" name="name" id="client-vendor_name" placeholder=""></div>');
        }
        else{
            $("#addBranch").hide();
            $("#startDate").hide();
            $("#officeHours").hide();
            $(".common-name-pane").text("Vendor Name");
            // $(".common-name-pane").html('<h5 class="normalText col-sm-3" for="email">Vendor Name</h5><div class="col-sm-9">
            //     <input type="text" class="form-control borderZero inputBox" name="vendor_name" id="client-vendor_name" placeholder="">
            //   </div>');
            $("#gender").show();
            $("#birthDate").show();
        }
    });
}).change();
$(".btn-nav").click(function(){
    $("#navULxs").toggleClass("active");
    $("#loginULxs").removeClass("active");
});
$(".btn-prof").click(function(){
    $("#loginULxs").toggleClass("active");
    $("#navULxs").removeClass("active");
});
$("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $(".button-pane").addClass('hide');

  });
$("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide");
        $(".photo-pane").removeClass("hide");
        $(".button-pane").removeClass('hide');

});
@if(Session::has('notice'))
  $("#modal-login").modal("show");
@endif

});


$("#modal-login").on("hidden.bs.modal", function (event) {
  $("#modal-login .alert").remove();
});

$("#modal-register").on("hidden.bs.modal", function (event) {
  $("#modal-register .alert").remove();
  $("#modal-register").find("form")[0].reset();
});

$("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
 $(".modal").on("change", ".company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().find("#client-company_city");
        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
var branch = $("#branchContainer").html();

 $(".btn-add-branch").click(function() {
        $("#branchContainer").append(branch);
        var country = $("#branchContainer").find('section:first').find("#client-company_country").val();
        var city = $("#branchContainer").find('section:first').find("#client-company_city").html();

        $("#branchContainer").find('section:last').find("#client-company_country").val(country);
        $("#branchContainer").find('section:last').find("#client-company_city").html(city);
        $("#branchContainer").find('section:last').find(".btn-add-branch").removeClass("btn-add-branch").addClass("btn-del-branch").html("<i class='fa fa-minus redText'></i>");
        //$("#branchContainer").find('section:last').find(".btn-add-branch").html("<button type='button' value='' class='hide-view btn-add-branch pull-right borderZero inputBox'></button>");
 });
 $(".modal").on("click", ".btn-del-branch", function() { 
    $(this).parent().parent().remove();
 });
  $('#client-gender-male').change(function() {
      if($(this).is(":checked")) {
        $("#client-gender").val("m");
      }
  });
  $('#client-gender-female').change(function() {
      if($(this).is(":checked")) {
         $("#client-gender").val("f");
      }
  });
  @if(session('status'))
      status("Error", "Account Validated", 'alert-success');
  @endif
</script>

</body>
</html>