@extends('layout.frontend')
@section('scripts')
<script>
 
</script>
@stop

@section('content')

<section id="publicProfile">
<div class="container">
  <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-5 col-xs-5">
        <div class="panel panel-default borderzero">
          <div class="panel-heading pnl-heading">
              <div class="">
              <img src="http://192.168.254.14/yakolak/public/img/avatar.jpg" height="" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    <div class="col-lg-9 col-md-9 col-sm-7 col-xs-7">
      <div class="panel panel-default borderzero">
        <div class="panel-heading">Vendor Information</div>
        <div class="panel-body">
            <p>Name:<span id="customer-name">{{$rows->name}}</span></p>
            <p class="email"><i class="ion-ios-email fa-lg"></i><span id="customer-email">Email: {{$rows->email}}</span></p>
            <p>Address: Concepcion, Tarlac City</p>
            <p>Country: Philippines</p>
            <p>Gender: Male</p>
            <p id="customer-mobile">Mobile: {{$rows->mobile}}</p>
            <div class="clearfix"></div></div>
        </div>
    </div>
</div>
</div>
</section>
  <div id="vendorAdsListing" class="col-lg-12">
    <div class="container">
  <div class="row">
  <div class="col-lg-9 col-md-9 col-sm-9">
    <div class="panel panel-default borderzero">
      <div class="panel-heading pnl-heading">Vendor ads listing</div>
          <div class="panel-body">
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
            <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
              <div class="item clearfix">
                <div class="image">
                  <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
                    <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
                  </a>
                </div>
                <div class="content">
                  <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
                    <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
                  <br>
                  <br>
                  <div class="short-desc">
                  Location : Concepcion, Tarlac City
                  </div>
                  <span class="price">54353543.00 PHP</span>
                </div>
                <div class="metas admin-option">
                  <span class="location hidden-sm">Tarlac City, Philippines</span>
                   
                  <span class="location visible-sm">Tarlac City</span>
                </div>
              </div>
            </div>
          </div>
        </div>
         <div class="panel panel-default borderzero">
          <div class="panel-body"></div>
        </div>
  </div>
  <div class="col-lg-3 col-md-3 col-sm-3">
    <div class="panel panel-default borderzero">
        <div class="panel-heading pnl-heading"><i class="fa fa-envelope"></i> Contact</div>
        <div class="panel-body">
          <div class="form-group">
            <input type="text" class="form-control borderzero" id="email" placeholder="Your Name">
          </div>
          <div class="form-group">
            <input type="text" class="form-control borderzero" id="email" placeholder="Your Email Address">
          </div>
          <div class="form-group">
            <input type="text" class="form-control borderzero" id="email" placeholder="Phone Number (Optional)">
          </div>
          <div class="form-group">
            <input type="text" class="form-control borderzero" id="email" rows="5" placeholder="Message">
          </div>
          <div class="form-group text-center">
            <button type="button" class="btn btn-danger borderzero fullSize">Submit</button>
          </div>
        </div>
      </div>
      <div id="listingpanel" class="col-xs-12 thumbnail right-caption span4 borderzero">
      <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 listingImg">
        <a>
          <div class="img-ads" style="background: url('uploads/ads/thumnail/');">
          </div>
        </a>
      </div>
      <div class="caption">
        <p>iPhone</p>
          <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
      </div>
    </div>
    <div id="listingpanel" class="col-xs-12 thumbnail right-caption span4 borderzero">
      <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 listingImg">
        <a>
          <div class="img-ads" style="background: url('uploads/ads/thumnail/');">
          </div>
        </a>
      </div>
      <div class="caption">
        <p>iPhone</p>
          <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
      </div>
    </div>
    <div id="listingpanel" class="col-xs-12 thumbnail right-caption span4 borderzero">
      <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12 listingImg">
        <a>
          <div class="img-ads" style="background: url('uploads/ads/thumnail/');">
          </div>
        </a>
      </div>
      <div class="caption">
        <p>iPhone</p>
          <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
      </div>
    </div>
  </div>
</div>
</div>
</div>
<div id="vendorReviews" class="col-lg-12">
  <div class="container">
    <div class="row">
        <h3 class="commentHeading">Comments and Review</h3>
          <p><strong>USERNAME</strong></p>
          <p>Quisque suscipit, nunc et posuere suscipit, risus lacus iaculis ligula, vitae iaculis orci neque nec lorem. Suspendisse potenti. Curabitur vitae mi purus. In sit amet arcu euismod, posuere dolor lacinia, facilisis neque. Phasellus augue quam, commodo eget dapibus sed, ullamcorper ut diam. Integer nec neque vitae ante laoreet lacinia. Donec consequat sollicitudin lectus nec congue. Integer mollis quam ac vulputate ultricies. Quisque tempus faucibus ante rhoncus molestie. Nunc vestibulum lacus ut tincidunt scelerisque. Integer maximus commodo justo in accumsan. Nam pulvinar risus at leo pretium sagittis. Aliquam ac elit enim.</p>
          <hr>
          <p><strong>USERNAME</strong><span class="userRating"> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p>Quisque suscipit, nunc et posuere suscipit, risus lacus iaculis ligula, vitae iaculis orci neque nec lorem. Suspendisse potenti. Curabitur vitae mi purus. In sit amet arcu euismod, posuere dolor lacinia, facilisis neque. Phasellus augue quam, commodo eget dapibus sed, ullamcorper ut diam. Integer nec neque vitae ante laoreet lacinia. Donec consequat sollicitudin lectus nec congue. Integer mollis quam ac vulputate ultricies. Quisque tempus faucibus ante rhoncus molestie. Nunc vestibulum lacus ut tincidunt scelerisque. Integer maximus commodo justo in accumsan. Nam pulvinar risus at leo pretium sagittis. Aliquam ac elit enim.</p>
      </div>
    </div>
  </div>
  <div id="vendorLatest" class="col-lg-12">
  <div class="container">
    <div class="row">
        <div class="hidden-xs panel panel-default borderzero">
  <div class="panel-heading pnl-heading"><strong class="lbl">LATEST BIDS</strong><span> | </span><span> Check out our members latest bids and start bidding now!</span><span class="pull-right"><a href="#">view more</a></span>
  </div>
  <div class="panel-body" style="height: 100%;">
    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: 100%;">
    <!-- Indicators -->
    <ol class="hidden carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class=""></li>
      <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item">
                <div class="col-sm-12 col-md-3 col-sm-6">
          <div class="panel panel-default borderzero">
            <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
            <div class="panel-body">
          <a href="http://192.168.254.14/yakolak/public/ads/17"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.images (1).jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
            <div class="caption">
            <a href="http://192.168.254.14/yakolak/public/ads/17">
            <p>sample</p>
            </a>
            <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
            <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
            </div>
          </div>
          </div>
        </div>
                <div class="col-sm-12 col-md-3 col-sm-6">
          <div class="panel panel-default borderzero">
            <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
            <div class="panel-body">
          <a href="http://192.168.254.14/yakolak/public/ads/17"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.images.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
            <div class="caption">
            <a href="http://192.168.254.14/yakolak/public/ads/17">
            <p>sample</p>
            </a>
            <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
            <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
            </div>
          </div>
          </div>
        </div>
                <div class="col-sm-12 col-md-3 col-sm-6">
          <div class="panel panel-default borderzero">
            <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
            <div class="panel-body">
          <a href="http://192.168.254.14/yakolak/public/ads/17"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.large.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
            <div class="caption">
            <a href="http://192.168.254.14/yakolak/public/ads/17">
            <p>sample</p>
            </a>
            <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
            <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
            </div>
          </div>
          </div>
        </div>
                <div class="col-sm-12 col-md-3 col-sm-6">
          <div class="panel panel-default borderzero">
            <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
            <div class="panel-body">
          <a href="http://192.168.254.14/yakolak/public/ads/20"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/20.Mushroom2.PNG" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
            <div class="caption">
            <a href="http://192.168.254.14/yakolak/public/ads/20">
            <p>32323223</p>
            </a>
            <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
            <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
            </div>
          </div>
          </div>
        </div>
        
<!--       <div class="col-sm-12 col-md-4">
        <div class="thumbnail borderzero">
        <img src="img/4.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image">
          <div class="caption">
          <p>iPhone</p>
          <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
          </div>
        </div>
      </div>
      <div class="col-sm-12 col-md-4">
        <div class="thumbnail borderzero">
        <img src="img/4.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image">
          <div class="caption">
          <p>iPhone</p>
          <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
          <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
          </div>
        </div>
      </div> -->
    </div>

    <div class="item active">
          <div class="col-sm-12 col-md-3 col-sm-6">
      <div class="panel panel-default borderzero">
        <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
        <div class="panel-body">
      <a href="http://192.168.254.14/yakolak/public/ads/14"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.images (1).jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
        <div class="caption">
        <p>sample</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
      </div>
    </div>
        <div class="col-sm-12 col-md-3 col-sm-6">
      <div class="panel panel-default borderzero">
        <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
        <div class="panel-body">
      <a href="http://192.168.254.14/yakolak/public/ads/15"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.images.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
        <div class="caption">
        <p>sample</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
      </div>
    </div>
        <div class="col-sm-12 col-md-3 col-sm-6">
      <div class="panel panel-default borderzero">
        <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
        <div class="panel-body">
      <a href="http://192.168.254.14/yakolak/public/ads/16"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/17.large.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
        <div class="caption">
        <p>sample</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
      </div>
    </div>
        <div class="col-sm-12 col-md-3 col-sm-6">
      <div class="panel panel-default borderzero">
        <div class="panel-heading pnl-heading text-center"> D: 00 - H: 00 - M: 00</div>
        <div class="panel-body">
      <a href="http://192.168.254.14/yakolak/public/ads/17"><img src="http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/20.Mushroom2.PNG" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
        <div class="caption">
        <p>32323223</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
      </div>
    </div>
    <!--     <div class="col-sm-12 col-md-4">
      <div class="thumbnail borderzero">
      <img src="img/4.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image">
        <div class="caption">
        <p>iPhone</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-md-4">
      <div class="thumbnail borderzero">
      <img src="img/4.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image">
        <div class="caption">
        <p>iPhone</p>
        <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
        <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
        </div>
      </div>
    </div> -->
    </div>
  </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="carousel-btn-left glyphicon glyphicon-menu-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="carousel-btn-right glyphicon glyphicon-menu-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
    </a>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>
      
<!--   <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
  <div class="user-public-head clearfix">
<div class="user-public-picture">
<img src="{{ url('img').'/'}}avatar.jpg" class="seller-image" width="200" height="200px"> </div>
<div class="user-public-detail">
<h3><span id="customer-name">{{$rows->name}}</span></h3>
<p class="email"><i class="ion-ios-email fa-lg"></i><span id="customer-email">{{$rows->email}}</span></p>
  <p>Tarlac</p>
  <p id="customer-mobile">{{$rows->mobile}}</p>
</div>
<div class="clearfix"></div>
</div>
<h3>{{$rows->name}} Listing</h3>
<div class="row">
    <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
    <div class="item clearfix">
      <div class="image">
        <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
          <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
        </a>
      </div>
      <div class="content">
        <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
          <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
        <br>
        <br>
        <div class="short-desc">
        Location : Concepcion, Tarlac City
        </div>
        <span class="price">54353543.00 PHP</span>
      </div>
      <div class="metas admin-option">
        <span class="location hidden-sm">Tarlac City, Philippines</span>
         
        <span class="location visible-sm">Tarlac City</span>
      </div>
    </div>
  </div>
  <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
    <div class="item clearfix">
      <div class="image">
        <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
          <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
        </a>
      </div>
      <div class="content">
        <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
          <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
        <br>
        <br>
        <div class="short-desc">
        Location : Concepcion, Tarlac City
        </div>
        <span class="price">54353543.00 PHP</span>
      </div>
      <div class="metas admin-option">
        <span class="location hidden-sm">Tarlac City, Philippines</span>
         
        <span class="location visible-sm">Tarlac City</span>
      </div>
    </div>
  </div>
  <div class="list col-md-4 col-sm-4 col-xs-12 panelBottomPadding">
    <div class="item clearfix">
      <div class="image">
        <a href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">
          <img src="{{ url('uploads/ads').'/'}}58855.jpg" class="img-responsive">
        </a>
      </div>
      <div class="content">
        <h4><a class="" href="" title="Marigondon Mactan House &amp; Lot 4 FOR SALE Callia PGV">Doodle Doods House</a></h4>
          <span class="category gray small"><i class="fa fa-tags"></i> <span class="label label-primary">Cars</span> <span class="label label-success">Sports</span></span>
        <br>
        <br>
        <div class="short-desc">
        Location : Concepcion, Tarlac City
        </div>
        <span class="price">54353543.00 PHP</span>
      </div>
      <div class="metas admin-option">
        <span class="location hidden-sm">Tarlac City, Philippines</span>
         
        <span class="location visible-sm">Tarlac City</span>
      </div>
    </div>
  </div>
</div>
</div>
@if(!Auth::check())
<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
  <div class="panel panel-default borderzero">
  <div class="panel-heading pnl-heading"><i class="fa fa-envelope"></i> Contact</div>
  <div class="panel-body">
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="email" placeholder="Your Name">
    </div>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="email" placeholder="Your Email Address">
    </div>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="email" placeholder="Phone Number (Optional)">
    </div>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="email" rows="5" placeholder="Message">
    </div>
    <div class="form-group text-center">
      <button type="button" class="btn btn-danger borderzero fullSize">Submit</button>
    </div>
  </div>
</div>
</div> 
@endif-->
@stop