@extends('layout.frontend')
@section('scripts')

@stop
@section('content')
view view view
<div class="container-fluid bgWhite borderbottomLight">
	<div class="container bannerJumbutron">
		<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
					  <div class="panel-body noPadding">
					  	<div class="col-md-12 col-sm-12 noPadding">
					  	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body topPaddingB bgGray">
							  	<div class="row">
							  		<div class="col-md-4 col-sm-5 col-xs-12">
							  			<div class="adImage" style="height:85px; width:100%; background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/flower_macro_petals_blur_107395_3840x2160.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
								  		</div>
								  	</div>
							  		<div class="col-md-8 col-sm-7 col-xs-12">
							  			<div class="mediumText grayText"><strong>SHANGRI-LA HOTELS</strong></div>
							  			<div class="normalText grayText"><i class="fa fa-envelope rightMargin"></i> sales@shangrila.com</div>
							  			<div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> <span class="skype_c2c_print_container notranslate">+639178238577</span><span id="skype_c2c_container" class="skype_c2c_container notranslate" dir="ltr" tabindex="-1" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" data-numbertocall="+639178238577" data-numbertype="paid" data-isfreecall="false" data-isrtl="false" data-ismobile="false"><span class="skype_c2c_highlighting_inactive_common" dir="ltr" skypeaction="skype_dropdown"><span class="skype_c2c_textarea_span" id="non_free_num_ui"><img width="0" height="0" class="skype_c2c_logo_img" src="chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/call_skype_logo.png"><span class="skype_c2c_text_span">+639178238577</span><span class="skype_c2c_free_text_span"></span></span></span></span></div>
							  			<div class="normalText grayText"><i class="fa fa-map-marker rightMargin"></i> Jeddah, UAE</div>
							  		</div>
							  	</div>
							  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding normalText">Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.
							  		</div>
							  </div>
							</div>
							<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body bgGray">
					  			<div class="normalText grayText"><i class="fa fa-globe rightMargin"></i> shangrila.com</div>
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-facebook-square rightMargin"></i></span> facebook.com/shangrila</div>
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-twitter rightMargin"></i></span> twitter.com/shangrila</div>
							  </div>
							</div>
						</div>
						<div class="col-md-12 noPadding"><button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-paper-plane"></i> Message Shangrila</button></div>
					  </div>
				</div>
			</div>
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body bgGray">		
		  			<div class="mediumText">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ac nisi venenatis, scelerisque mi id, consequat justo. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer vehicula, velit quis pharetra elementum, leo erat fringilla orci, ut ullamcorper enim libero id elit. Curabitur sagittis pharetra consectetur. Sed vulputate purus sed semper faucibus. Sed rhoncus efficitur eleifend. Pellentesque condimentum erat id turpis porttitor, maximus fringilla ligula rutrum. Ut enim metus, porta ac molestie vestibulum, faucibus quis nisl. Donec a neque lobortis, malesuada nulla nec, hendrerit libero. Integer risus purus, mollis sit amet augue ut, consequat gravida ante. Donec pulvinar elit quis condimentum aliquet. Sed quis justo in elit varius sodales quis quis risus. Nullam viverra mauris sit amet nisl facilisis euismod. Interdum et malesuada fames ac ante ipsum primis in faucibus.</div>
		  			<br>
		  			<div class="mediumText">Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.</div>
				  </div>
				</div>
			</div>
    </div>
 </div>
 </div>
 <div class="container-fluid bgWhite">
	<div class="container topMarginB">
		<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
		 	<div class="col-md-12 col-sm-12 col-xs-12 panelTitle">HONGKONG BRANCH INFORMATION<a class="btn pull-right grayText normalText" data-toggle="collapse" data-target="#vendorAd"><i class="fa fa-angle-down"></i></a></div>
		 	<div id="vendorAd" class="col-md-12 col-sm-12 col-xs-12 noPadding collapse">
		    <div class="col-md-3 col-sm-5 col-xs-12">
		    	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3850.9779215686144!2d120.58926331539807!3d15.159557167196674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3396f273d427eb37%3A0xb9f5907c1809acdb!2sOceana+Commercial+Complex!5e0!3m2!1sen!2sph!4v1455856335967" width="100%" height="206px;" frameborder="0" style="border:0" allowfullscreen></iframe>
		    </div>
		    <div class="col-md-9 col-sm-7 col-xs-12">
			  	<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText"> Unit 202 2nd Floor Oceana, Balibago, Angeles, Pampanga 2009 Phillipines</span></div>
			  	<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText"> (045) 436-4083 / (045) 322-7427</span></div>
			  	<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText"> info@email.com</span></div>
			  	<div class="mediumText grayText"><strong>Office Hours:</strong><span> Mon - Sat: 9:00 am - 6:00 pm</span></div>
		    </div>
  		</div>
   </div>
	</div>
</div>
<div class="container-fluid bgGray borderBottom">
	<div class="container blockMargin">
		<div class="col-md-12 noPadding">
			<div class="col-md-9">
		<div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding">
	   	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelTitle">VENDOR BIDS</span>
        </div>
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText topPadding bottomPadding">Featured Ads 2</div>
        		<div class="normalText redText bottomPadding">by Dell Distributor</div>
        		<div class="bottomPadding mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        </div>
        </div>
	     </div>
	   </div>
	     <div class="panel panel-default removeBorder borderZero">
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText">Featured Ads 1</div>
        		<div class="normalText redText">by Dell Distributor</div>
        		<div class="topMarginB mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
	     </div>
		</div>
				<div class="col-md-12 col-sm-12 hidden-xs hidden-sm noPadding">
				<div class="panel panel-default  removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">Vendor Bids</span>  <span class="hidden-xs"></span>
						<span class="redText pull-right">view more</span>
					</div>
						<div class="panel-body">
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="col-lg-3 col-md-3 col-sm-3 noPadding ">
											<div class="sideMargin borderbottomLight">
												<a href="http://192.168.254.14/yakolak/public/ads/view/29"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/2.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
												</div></a>
												<div class="minPadding nobottomPadding">
													<div class="mediumText noPadding topPadding bottomPadding">Phones for Sale</div>
														<div class="normalText grayText bottomPadding bottomMargin">
														by sample222
														<span class="pull-right"><strong class="blueText">2000 USD</strong>
														</span>
														</div>
													<div class="normalText bottomPadding">
														<i class="fa fa-map-marker rightMargin"></i>
															<span class="pull-right ">
																	<span class="blueText rightMargin">
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star"></i>
																	<i class="fa fa-star-half-empty"></i>
																</span>
															<span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
														</span>
													</div>
												</div>
											</div>
											<div class="sideMargin minPadding text-center nobottomPadding redText normalText">
											D: 00 - H: 00 - M: 00
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3 noPadding ">
												<div class="sideMargin borderbottomLight">
													<a href="http://192.168.254.14/yakolak/public/ads/view/29"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/2.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
													</div></a>
													<div class="minPadding nobottomPadding">
														<div class="mediumText noPadding topPadding bottomPadding">Phones for Sale</div>
															<div class="normalText grayText bottomPadding bottomMargin">
															by sample222
															<span class="pull-right"><strong class="blueText">2000 USD</strong>
															</span>
															</div>
														<div class="normalText bottomPadding">
															<i class="fa fa-map-marker rightMargin"></i>
																<span class="pull-right ">
																		<span class="blueText rightMargin">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-half-empty"></i>
																	</span>
																<span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
															</span>
														</div>
													</div>
												</div>
												<div class="sideMargin minPadding text-center nobottomPadding redText normalText">
												D: 00 - H: 00 - M: 00
												</div>
											</div>
										<div class="col-lg-3 col-md-3 col-sm-3 noPadding">
												<div class="sideMargin borderbottomLight">
													<a href="http://192.168.254.14/yakolak/public/ads/view/29"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/2.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
													</div></a>
													<div class="minPadding nobottomPadding">
														<div class="mediumText noPadding topPadding bottomPadding">Phones for Sale</div>
															<div class="normalText grayText bottomPadding bottomMargin">
															by sample222
															<span class="pull-right"><strong class="blueText">2000 USD</strong>
															</span>
															</div>
														<div class="normalText bottomPadding">
															<i class="fa fa-map-marker rightMargin"></i>
																<span class="pull-right ">
																		<span class="blueText rightMargin">
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-half-empty"></i>
																	</span>
																<span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
															</span>
														</div>
													</div>
												</div>
												<div class="sideMargin minPadding text-center nobottomPadding redText normalText">
												D: 00 - H: 00 - M: 00
												</div>
											</div>
											<div class="col-lg-3 col-md-3 col-sm-3 noPadding ">
													<div class="sideMargin borderbottomLight">
														<a href="http://192.168.254.14/yakolak/public/ads/view/29"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/2.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
														</div></a>
														<div class="minPadding nobottomPadding">
															<div class="mediumText noPadding topPadding bottomPadding">Phones for Sale</div>
																<div class="normalText grayText bottomPadding bottomMargin">
																by sample222
																<span class="pull-right"><strong class="blueText">2000 USD</strong>
																</span>
																</div>
															<div class="normalText bottomPadding">
																<i class="fa fa-map-marker rightMargin"></i>
																	<span class="pull-right ">
																			<span class="blueText rightMargin">
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star"></i>
																			<i class="fa fa-star-half-empty"></i>
																		</span>
																	<span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
																</span>
															</div>
														</div>
													</div>
													<div class="sideMargin minPadding text-center nobottomPadding redText normalText">
													D: 00 - H: 00 - M: 00
													</div>
												</div>
											</div>
									</div>
							</div>
						</div>
								<div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding">
	   	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelTitle">LATEST ADS</span>
        </div>
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText topPadding bottomPadding">Featured Ads 2</div>
        		<div class="normalText redText bottomPadding">by Dell Distributor</div>
        		<div class="bottomPadding mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        </div>
        </div>
	     </div>
	   </div>
	     <div class="panel panel-default removeBorder borderZero">
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText">Featured Ads 1</div>
        		<div class="normalText redText">by Dell Distributor</div>
        		<div class="topMarginB mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
	     </div>
		</div>
	<div class="col-md-12 col-lg-12 hidden-xs hidden-sm noPadding">
		<div class="panel panel-default removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our members latest ads now!</span>
        <span class="redText pull-right">view more</span>
      </div>
      	<div class="panel-body">
           <div class="col-lg-12">
           	<div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/27"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/download.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Samsung Galaxy S6</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/27"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/download.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Samsung Galaxy S6</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/21"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/hoverboard.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">321awddwa</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/19"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/flower_macro_petals_blur_107395_3840x2160.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Flower</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/17"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/hahaha.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">Mighty Bond</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/13"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/fghg.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">dfvfvfvdv</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/12"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/f.jpeg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">wqeqwfffffffff</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
             <div class="col-lg-3 col-md-3 col-sm-3 bottommarginveryLight noPadding ">
                <div class="sideMargin borderbottomLight">
                  <a href="http://192.168.254.14/yakolak/public/ads/view/11"><div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/ewfd.jpeg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">awdaw</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                      by 
                      <span class="pull-right"><strong class="blueText">1,000,000 USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                      <i class="fa fa-map-marker rightMargin"></i> 
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    		</div>
			</div>
		</div>
<div class="col-md-3 noPadding">
<div class="col-md-12 col-sm-12 col-xs-12">
	   	<div class="panel panel-default removeBorder borderZero">
	        <div class="panel-heading panelTitleBarLightB">
	          <span class="panelRedTitle">ADVERTISEMENT</span>
	        </div>
	      <div class="panel-body noPadding">
          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
	     </div>
	   	</div>
 		</div>
 		<div class="col-md-12 col-sm-12 col-xs-12">
	   	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText topPadding bottomPadding">Featured Ads 2</div>
        		<div class="normalText redText bottomPadding">by Dell Distributor</div>
        		<div class="bottomPadding mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        </div>
        </div>
	     </div>
	   </div>
	     <div class="panel panel-default removeBorder borderZero">
    		<div class="panel-body noPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
        		<div class="mediumText grayText">Featured Ads 1</div>
        		<div class="normalText redText">by Dell Distributor</div>
        		<div class="topMarginB mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText bottomPadding">
	          <i class="fa fa-map-marker"></i> 
	            Jeddah, UAE
	          </div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
	     </div>
	   	</div>
</div>
		</div>
	</div>
</div>
 <div class="container-fluid bgWhite borderBottom">
	<div class="container blockMargin">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 noPadding">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panelTitle bottomPadding">Comments and reviews</div>
			</div>
			<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">
				<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 50px; width: auto;">
        </div>
			</div>
			<div class="col-lg-11 col-md-11 col-sm-10 col-xs-10">
					<textarea class="form-control borderZero fullSize" rows="2" id="comment"></textarea>
   		</div>
   		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPadding text-right">
   			<span class="mediumText adRating">
	  			<i class="fa fa-star"></i>
          <i class="fa fa-star"></i>
          <i class="fa fa-star"></i>
          <i class="fa fa-star"></i>
          <i class="fa fa-star-half-empty"></i>
	  		</span>
				<button class="btn normalText rateButton borderZero"><i class="fa fa-star"></i> Rate Product</button>
				<button class="btn normalText blueButton borderZero"><i class="fa fa-eye"></i> Leave Comment</button>
   		</div>
  </div>
</div>
@stop