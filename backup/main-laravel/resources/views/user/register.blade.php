@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
        loading.addClass('hide');
    } 

    $("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $(".photo-pane").addClass("hide");

    });
    $("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide");
        $(".photo-pane").removeClass("hide");

    });    
</script>
@stop

@section('content')
<section id="register" class="col-lg-12">
  <div class="row">
    <div class="container">
      <div id="register-save">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
        </div>
      <div class="form-register">
    
      <div class="panel panel-default borderzero">
        <div class="panel-body">    
        <h4 class="modal-title"> REGISTER USER</h4>
        <hr>
            <div id="register-notice"></div>
         {!! Form::open(array('url' => 'user/register/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}           
            <div class="form-group">     
              <label for="row-photo" class="col-sm-3 col-xs-5 font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploadPhoto">
                  <div class="panel panel-default borderzero photo-pane">
                    <div class="panel-body">
                      <div>
                        <img class="img-responsive" src="{{url('img').'/default_image.png'}}">
                        
                      </div>
                    </div>
                    <div class="panel-footer text-center">
                      <button type="button" class="btn btn-primary btn-md borderzero" id="btn-change_photo"><i class="fa fa-camera"></i> CHANGE</button>
                    </div>
                  </div>
                </div>
                  <div class="uploader-pane hide">
                    <div class="row">
                        <div class='change col-sm-11'>
                           <input name="user_photo" id='client-photo_upload' type='file' class='file borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>
                        </div>
                        <div class="col-sm-1">
                         <button type="button" class="btn btn-primary btn-md borderzero" id="btn-cancel"><i class="fa fa-reply"></i></button>
                        </div>
                    </div>
                  </div>
              </div>
            </div> 
            <div class="form-group">
              <label for="row-name" class="col-sm-3 col-xs-5 font-color">Name</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="name" class="form-control borderzero" id="row-name" maxlength="30" placeholder="Enter your last name">
              </div>
            </div>
            <div class="form-group">
              <label for="row-email" class="col-sm-3 col-xs-5 font-color">Email</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="email" class="form-control borderzero" id="row-email" maxlength="30" placeholder="Enter your email address...">
              </div>
            </div>
            <div class="form-group hide-view">
              <label for="row-password" class="col-sm-3 col-xs-5 font-color">Password</label>
              <div class="col-sm-9 col-xs-7">
              <input type="password" name="password" class="form-control borderzero" id="row-password" maxlength="30" placeholder="Enter your password...">
              </div>
            </div>
            <div class="form-group hide-view">
              <label for="row-password_confirmation" class="col-sm-3 col-xs-5 font-color">Confirm Password</label>
              <div class="col-sm-9 col-xs-7">
              <input type="password" name="password_confirmation" class="form-control borderzero" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Contact Number</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-usertype_id" class="col-sm-3 col-xs-5 font-color">User Type</label>
              <div class="col-sm-9 col-xs-7">
              <select class="form-control borderzero" id="row-usertype_id" name="usertype_id">
              <option class="hide">Select</option>
              <option value="1">Vendor</option>
              <option value="1">Company</option>
              <option value="1">Customer or Bidder</option>
              </select>
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Company</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="company" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Mobile</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="mobile" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Country</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="country" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Region</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="region" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">City</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="city" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">City Area</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="city_area" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
             <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Address</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="address" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Website</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="website" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
            <div class="form-group">
              <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Descripion</label>
              <div class="col-sm-9 col-xs-7">
              <input type="text" name="description" class="form-control borderzero" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
              </div>
            </div>
        <hr>
        <div class="text-right">
        <input type="hidden" name="id" id="row-id">
        <button type="submit" class="btn btn-success hide-view borderzero" id="button-save"><i class="fa fa-save"></i> Register</button>
        </div>
      {!! Form::close() !!}
        </div>
      </div>
      </div>
      </div>
    </div>
  </div>
</section>


@stop
