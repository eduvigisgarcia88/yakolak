@extends('layout.frontend')
@section('scripts')
<script>
function refresh(){

	$_token = "{{ csrf_token() }}";
	 var id = "{{ $user_id }}";

	 var loading = $(".loading-pane");

	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
	 $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
	 	
	 	$.each(response.rows, function(index, value) {
	 		//console.log(index+"="+value);
	 		var field = $("#customer-" + index);

 			if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	console.log(value);
             	 $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }
             
          }); 
      }, 'json');

	 loading.addClass("hide");
	}

	$(document).ready(function() {
	});

	 $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });
     $('.btn-remove').on('click', function() {
      var id = {{ $rows->id }};

      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      dialog('Remove Photo', 'Are you sure you want to remove your photo</strong>?', "{{ url('profile/photo/remove') }}", id);

     });
  $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
 $("#accountBranchContainer").on("change", "#account-company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().parent().parent().find("#account-company_city");

        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });

 var branchAccount = $("#accountBranch").html();
 $(".btn-add-account-branch").click(function() {
        $("#accountBranchContainer").append("<div id='accountBranch'>"+branchAccount+"</div>");
         

        var country = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_country").val();
        var city = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_city").html();

        $("#branchContainer").find('#accountBranch:last-child').find("#account-company_country").val(country);
        $("#branchContainer").find('#accountBranch:last-child').find("#account-company_city").html(city);
        // $("#branchContainer").find('section:last').find(".btn-add-branch").removeClass("btn-add-branch").addClass("btn-del-branch").html("<i class='fa fa-minus redText'></i>");
       $("#accountBranchContainer").find('#accountBranch:last-child').find(".btn-add-account-branch").removeClass("btn-add-account-branch").addClass("btn-del-account-branch").html("<i class='fa fa-minus redText'></i>");
 	   var branch_id = $("#accountBranchContainer").find('#accountBranch:last-child').find('#branch_id').val();
 	   $("#accountBranchContainer").find('#accountBranch:last-child input:text').val("");
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#branch_id").prop("value","0");
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_country option:eq(0)").attr('selected','selected');
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_city option:eq(0)").attr('selected','selected');
 	     	
 });

 $(".btn-del-account-branch").click(function(){
    $(this).parent().parent().parent().remove();
 });
</script>
@stop
@section('content')
<div class="container-fluid noPadding bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 noPadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        			<div id="change-photo-notice"></div>
					<div class="panel panel-default removeBorder borderZero borderBottom" id="panel-profile">
							<div class="panel-body normalText">	
							<div id="load-form" class="loading-pane hide">
						             <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
						     </div>
								<img src ="{{url('uploads').'/'.$rows->photo}}" class="img-responsive" id="profile-photo" height="224" width="224">
					                <div class="minPadding noleftPadding norightPadding">
				            {!! Form::open(array('url' => 'profile/photo/save', 'role' => 'form', 'class' => 'form-horizontal','files'=>'true','id'=>'modal-save_photo')) !!} 
				                  	  <div class="uploader-pane">
					                      <div class='change'>
					                        <input name='photo' id='row-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo'>
					                      </div>
					                  </div>
				                    <!-- <button class="btn redButton borderZero" type="button">Browse</button> -->
				                </div>
				                <input type="hidden" name="id" value="{{$rows->id}}">
								<button class="btn blueButton borderZero noMargin fullSize" type="submit">Upload Photo</button>
						   </div>
						   {!! Form::close() !!}
					 </div>
					
					<div class="panel panel-default removeBorder borderZero borderBottom">
							<div class="panel-body normalText">
								<ul class="nav nav-pills nav-stacked noPadding" role="tablist">
									<li role="presentation"><a href="#account" aria-controls="account" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i> Account<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-diamond  rightPadding"></i> Subscription <span class="redText">(Upgrade Plan)</span><i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#listings" aria-controls="listings" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-th rightPadding"></i> Listings<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#alerts" aria-controls="alerts" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-flag rightPadding"></i> Alerts<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-envelope rightPadding"></i> Messages<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#commentsandReviews" aria-controls="commentsandReviews" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-comments rightPadding"></i> Comments and Reviews<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#watchlist" aria-controls="watchlist" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-eye rightPadding"></i> Watchlist<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#rewardsandreferrals" aria-controls="rewardsandreferals" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-ticket  rightPadding"></i> Rewards and Referrals<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#advertisingplans" aria-controls="advertisingplans" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-barcode  rightPadding"></i> Advertising Plans<i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-area-chart rightPadding"></i> Reports<i class="fa fa-angle-right pull-right"></i></a></li>
								</ul>
							</div>
						</div>

        </div>
      </div>			
     <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 noPadding ">
			<div class="tab-content">
		    <div role="tabpanel" class="tab-pane active" id="account">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
		    		<div id="account-profile-notice"></div>
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Account Settings</span>
						</div>
						<div class="panel-body" id="panel-account-profile">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomPadding redText ">
							BASIC INFORMATION
							</div>
						 	 <div id="load-account-form" class="loading-pane hide">
						          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
						     </div>	
						{!! Form::open(array('url' => 'register/save', 'role' => 'form', 'class' => '','files'=>'true','id'=>'form-profile_save')) !!} 
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
								<div class="col-lg-6 col-md-6 col-xs-6">
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
													<input name="username" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Profile Name</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
													<input name="name" placeholder="Input" value="{{$rows->name}}" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
													<input name="email" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Usertype</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
													  <select class="form-control inputBox" id="sel1" name="usertype">
													  	@foreach($usertypes as $row)
													  		<option value="{{$row->id}}"{{($rows->usertype_id == $row->id ? 'selected' : '')}}>{{$row->type_name}}</option>
													  	@endforeach  
													  </select>
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="password" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="password_confirmation" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
												</div>
											</div>
										</div>

								</div>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<div class="form-group bottomMargin">
											<label class="col-lg-12 col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Description</label>
											<div class="inputGroupContainer">
												<div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
														<textarea class="form-control inputBox" rows="5" id="comment"></textarea>
												</div>
											</div>
										</div>
								</div>
						</div>
					<div id="accountBranchContainer">	
						<?php echo($branches) ?>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomPadding topMarginB bottomMargin redText">
								SOCIAL MEDIA
							</span>
							<div class="form-group bottomMargin">
								<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Facebook</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
												<input name="facebook_account" placeholder="" value="{{$rows->facebook_account}}" class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Twitter</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
												<input name="twitter_account" placeholder="" value="{{$rows->twitter_account}}" class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Instagram</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
												<input name="instagram_account" placeholder="" value="{{$rows->instagram_account}}" class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Linkedin</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
												<input name="linkedin_account" placeholder="" value="" class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>

						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomPadding topMarginB bottomMargin redText">
								NEWSLETTER
							</span>
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Monthly</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Weekly</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Promotions</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
						</div>
					</div>
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordertopLight noPadding topPaddingB ">
						<input type="hidden" name="id" id="client-id" value="{{$rows->id}}">
							<button class="btn blueButton borderZero noMargin pull-right " type="submit">Update</button>
						</div>
					{!! Form::close() !!}
						</div>
					</div>
		   </div>
		   <div role="tabpanel" class="tab-pane" id="subscription">
		   		<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Subcription</span>
						</div>
						<div class="panel-body">
							2
						</div>
					</div>
		   </div>
		    <div role="tabpanel" class="tab-pane" id="listings">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Listing</span>
						</div>
						<div class="panel-body">
							3
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="alerts">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Alerts</span>
						</div>
						<div class="panel-body">
							4
						</div>
					</div>
		    </div>
		 <div role="tabpanel" class="tab-pane" id="messages">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Messages</span>
						</div>
						<div class="panel-body">
							5
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="commentsandReviews">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Comments and Reviews</span>
						</div>
						<div class="panel-body">
							<div class="col-lg-12 bgWhite noPadding bottomMargin">
								<div class="col-md-12">
								<span class="panelTitle pull-left bottomMargin">Ad Title Comment or Review</span>
								</div>
								<div class="col-md-2">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
								</div>
								<div class="col-md-10">
									<div class="panelTitleB">Username</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a ipsum ligula. Quisque diam dolor, blandit ut venenatis ac, mollis et libero. Ut consequat nunc in massa ornare dignissim. Vestibulum ac porttitor turpis. Donec at velit erat. In condimentum justo in elit cursus, ultricies hendrerit purus sodales. Maecenas elementum consequat orci, at tempor justo ultrices nec.</p>
									<br>
									<p>Vestibulum laoreet interdum libero. Maecenas tincidunt, sem sed maximus scelerisque, dolor enim convallis enim, vitae dignissim sem nulla vel lacus. Nam lacinia lacus mi, quis porta quam mollis a. Curabitur vel arcu libero.</p>
								</div>
							</div>
							<div class="col-lg-12 bgGray noPadding bottomMargin">
								<div class="col-md-12">
								<span class="panelTitle pull-left bottomMargin">Ad Title Comment or Review</span>
								</div>
								<div class="col-md-2">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
								</div>
								<div class="col-md-10">
									<div class="panelTitleB">Username</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a ipsum ligula. Quisque diam dolor, blandit ut venenatis ac, mollis et libero. Ut consequat nunc in massa ornare dignissim. Vestibulum ac porttitor turpis. Donec at velit erat. In condimentum justo in elit cursus, ultricies hendrerit purus sodales. Maecenas elementum consequat orci, at tempor justo ultrices nec.</p>
									<br>
									<p>Vestibulum laoreet interdum libero. Maecenas tincidunt, sem sed maximus scelerisque, dolor enim convallis enim, vitae dignissim sem nulla vel lacus. Nam lacinia lacus mi, quis porta quam mollis a. Curabitur vel arcu libero.</p>
								</div>
							</div>
							<div class="col-lg-12 bgWhite noPadding bottomMargin">
								<div class="col-md-12">
								<span class="panelTitle pull-left bottomMargin">Ad Title Comment or Review</span>
								</div>
								<div class="col-md-2">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
								</div>
								<div class="col-md-10">
									<div class="panelTitleB">Username</div>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis a ipsum ligula. Quisque diam dolor, blandit ut venenatis ac, mollis et libero. Ut consequat nunc in massa ornare dignissim. Vestibulum ac porttitor turpis. Donec at velit erat. In condimentum justo in elit cursus, ultricies hendrerit purus sodales. Maecenas elementum consequat orci, at tempor justo ultrices nec.</p>
									<br>
									<p>Vestibulum laoreet interdum libero. Maecenas tincidunt, sem sed maximus scelerisque, dolor enim convallis enim, vitae dignissim sem nulla vel lacus. Nam lacinia lacus mi, quis porta quam mollis a. Curabitur vel arcu libero.</p>
								</div>
							</div>
						</div>
					</div>
		    </div>
		 <div role="tabpanel" class="tab-pane" id="watchlist">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Watchlist</span>
						</div>
						<div class="panel-body">
							7
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="rewardsandreferrals">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Rewards and Referrals</span>
						</div>
						<div class="panel-body">
							8
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="advertisingplans">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Advertising Plans</span>
						</div>
						<div class="panel-body">
							9
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="reports">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Reports</span>
						</div>
						<div class="panel-body">
							10
						</div>
					</div>
		    </div>
		 </div>
		</div>

				
				
      </div>
    </div>
  </div>
		
<!-- Remove Modal -->
  <div id="modal-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-remove-header" class="modal-header modal-yeah">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-remove-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-remove-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-remove-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-remove-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
@stop