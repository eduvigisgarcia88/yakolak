@extends('layout.frontend')
@section('scripts')
<script>

</script>
@stop
<section id="subCategory">
	<div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="panel panel-default borderzero">
          <div class="panel-body">
            <h4><i class="fa fa-bars"></i> Categories</h4>
            <ul class="list-group">
              <li class="list-group-item">All Categories</li>
              <li class="list-group-item">Vehicles</li>
              <li class="list-group-item">Mobile Phones</li>
              <li class="list-group-item">Computers</li>
              <li class="list-group-item">Clothing</li>
              <li class="list-group-item">Real Estate</li>
              <li class="list-group-item">Appliances</li>
              <li class="list-group-item">Electronics</li>
              <li class="list-group-item">Furniture</li>
              <li class="list-group-item">Jobs</li>
              <li class="list-group-item">Services</li>
              <li class="list-group-item">Events</li>
              <li class="list-group-item">Other Deals</li>
            </ul>
            <hr>
            <h4><i class="fa fa-map-marker"></i> Location</h4>
            <ul class="list-group">
              <li class="list-group-item">All locations</li>
              <li class="list-group-item">Abra</li>
              <li class="list-group-item">Agusan del Norte</li>
              <li class="list-group-item">Aklan</li>
              <li class="list-group-item">Albay </li>
              <li class="list-group-item">Antipolo</li>
              <li class="list-group-item">Antique</li>
              <li class="list-group-item">Aurora</li>
              <li class="list-group-item">Bataan</li>
              <li class="list-group-item">Batanes</li>
              <li class="list-group-item">Batangas</li>
              <li class="list-group-item">Benguet</li>
              <li class="list-group-item">Bohol</li>
              <li class="list-group-item">Bulacan</li>
              <li class="list-group-item">Cagayan</li>
              <li class="list-group-item">Camarines Sur</li>
              <li class="list-group-item">Cavite</li>
              <li class="list-group-item">Cebu</li>
              <li class="list-group-item">Davao del Norte</li>
              <li class="list-group-item">Davao del Sur</li>
              <li class="list-group-item">Ifugao</li>
              <li class="list-group-item">Ilocos Sur</li>
              <li class="list-group-item">Isabela</li>
              <li class="list-group-item">La Union</li>
              <li class="list-group-item">Laguna</li>
              <li class="list-group-item">Leyte</li>
              <li class="list-group-item">Maguindanao</li>
              <li class="list-group-item">Marikina</li>
              <li class="list-group-item">Masbate</li>
              <li class="list-group-item">Metro Manila</li>
              <li class="list-group-item">Mindoro Oriental</li>
              <li class="list-group-item">Misamis Oriental</li>
              <li class="list-group-item">Negros Occidental</li>
              <li class="list-group-item">Negros Oriental</li>
              <li class="list-group-item">Northern Samar</li>
              <li class="list-group-item">Palawan</li>
              <li class="list-group-item">Pampanga</li>
              <li class="list-group-item">Pangasinan</li>
              <li class="list-group-item">Quezon</li>
              <li class="list-group-item">Rizal</li>
              <li class="list-group-item">Sorsogon</li>
              <li class="list-group-item">South Cotabato</li>
              <li class="list-group-item">Tarlac</li>
              <li class="list-group-item">Zambales</li>
              </ul>
              <hr> 
              <div class="row">
                <label for="email" class="col-sm-12">Price Range:</label>
                <span class="col-sm-6">
                <input type="email" class="form-control borderzero" id="email">
                </span>
                <span class="col-sm-6">
                <input type="email" class="form-control borderzero" id="email">
                </span>
              </div>
              <hr>
              <button type="button" class="btn btn-danger fullSize borderzero">Apply</button>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-9">
        <div id="adsSection">
        </div>
        <div class="panel panel-default borderzero">
          <div class="panel-body"></div>
        </div>
          <div class="panel-body">
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="panel panel-default borderzero">
              <div class="panel-body">
              <a href="http://192.168.254.14/yakolak/public/ads/53"><img src="http://192.168.254.14/yakolak/public/uploads/ads/53.jpg" class="img-responsive" style="width:100%; height: 200px;" alt="Image"></a>
              <div class="caption">
              <a href="#">
              <p>Vancouver Luxury Homes</p>
              </a>
              <p class="lbl">1,000,000 USD<span class="pull-right"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-empty"></i></span></p>
              <p><i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right">27 <i class="fa fa-comment"></i></span></p>
              </div>
            </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default borderzero">
          <div class="panel-body">
          </div>
        </div>
        <div id="adsSection">
        </div>
    </div>
	</div>
</section>

@section('content')


@stop