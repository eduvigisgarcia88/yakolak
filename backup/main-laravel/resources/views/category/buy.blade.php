@extends('layout.frontend')
@section('scripts')

@stop
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 noPadding">
      <div class="col-lg-3 col-md-3">
				
        <div class="col-lg-12 noPadding">
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SEARCH OPTIONS</span>
					</div>
					<div class="panel-body normalText">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Search Value">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Ad Type (Ads / Bids)">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Category">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Country">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="City / State">
					</div>
					<div class="panel-heading panelTitleBarLight bordertopLight">
						<span class="panelTitleSub">(CATEGORY) ATTRIBUTES</span>
					</div>
					<div class="panel-body normalText">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
					</div>
					<div class="panel-heading panelTitleBarLight bordertopLight">
						<span class="panelTitleSub">PRICE RANGE</span>
					</div>
					<div class="panel-body normalText">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin " placeholder="ie. 100">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding leftPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin " placeholder="ie. 10000">
							</div>
						</div>
						<button class="btn blueButton borderZero noMargin fullSize" type="button">Update Search</button>
					</div>
				</div>
					
				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelRedTitle">Featured Ads</span>
					</div>
					<div class="panel-body">
						<div class="col-lg-5 left noPadding">
							<div class="thumbnailSize " style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
							</div>
						</div>
						<div class-"col-lg-7="" right"="">
							<h5 class="mediumText noPadding">Listing Title Here</h5>
							<h5 class="smallText grayText noPadding">By Distrubuter</h5>
							<div class="mediumText blueText bottomPadding"><strong>1,000,000 USD</strong></div>
							<div class="normalText  bottomPadding">
								<i class="fa fa-map-marker"></i> Jeddah, UAE
							</div>
						</div>
					</div>
				</div>
					
        </div>


      </div>
      <div class="col-lg-9 col-md-9 ">
				
				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-body">
					</div>
					<div class="panel-heading removeBorder panelTitleBarLightB">
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>

				</div>
				
				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelRedTitle">Featured Ads</span>
					</div>
					<div class="panel-body">

              <div class="col-lg-4 col-md-4 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
              <div class="col-lg-4 col-md-4 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
              <div class="col-lg-4 col-md-4 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
					</div>
				</div>
				
				<nav class="noPadding">
					<ul class="pagination noPadding noMargin blockBottom">
						<li>
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">«</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">»</span>
							</a>
						</li>
					</ul>
				</nav>
				
				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>
					<div class="panel-body">
					</div>
				</div>
				
        </div>
				
				
      </div>
    </div>
  </div>


@section('content')

@stop