<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ $title }}</title>

    <!-- Bootstrap -->
     {!! HTML::style('/css/bootstrap.min.css') !!}
     {!! HTML::style('/font-awesome/css/font-awesome.min.css') !!}
     {!! HTML::style('/css/styles.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
   <div class="container-fluid">
      <div class="page-header no-top-margin">
        <img alt="Brand" src="{{ url()}}/img/yakolaklogo.png" height="100" width="150" class="img-responsive">
      </div>
   </div>
     <div class="container">
      <div class="well well-xs">
      <h2 style="text-align:center;"><i class="fa fa-key"></i> Login</h2>
       {!! Form::open(array('url' => 'admin/login', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form')) !!} 
                {!! csrf_field() !!}
                @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
                @endif
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="pwd">Password:</label>
            <div class="col-sm-10">          
              <input type="password" class="form-control" name="password" id="pwd" placeholder="Enter password">
            </div>
          </div>
          <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
              </div>
            </div>
          </div>
          <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
      <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
  </body>
</html>