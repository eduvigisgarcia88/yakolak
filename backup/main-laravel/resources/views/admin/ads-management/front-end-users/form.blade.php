@section('scripts')
<script type="text/javascript">
  
     $('#row-photo').on('change', function() {
    var tmppath = URL.createObjectURL(event.target.files[0]);
      $("img#preview").attr('src',tmppath);
    }); 
$("input.file").fileinput({
        maxFileCount: 1,
        maxFileSize: 1024,
        allowedFileTypes: ['image'],
    allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
    });
</script>
@stop
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Add Advertisement</h4>
        </div>
          <div class="modal-body"> 
            <div id="form-notice"></div>      
              {!! Form::open(array('url' => 'admin/ads/save', 'role' => 'form', 'id' => 'modal-save_form', 'files' => true)) !!}
              <div class="form-group">
             <label for="photo">Ad Type</label>
                <select name="ad_type" class="form-control" id="row-ad_type">
                  <option>Bid</option>
                  <option>Auction</option>
                </select>
            </div>
             <div class="form-group">
                <label for="photo">Photo</label>
                <!-- img src ="" id ="row-photo" height="150" width="150" class="img-responsive"> -->
                <!-- <input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text"> -->
                <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
               <input name="photo" type='file' id="row-photo" class='file form-control borderZero file_photo'  multiple="true" data-show-upload='false' placeholder='Upload a photo...'>&nbsp;
                      </div>
                </div>
              </div>
              <div class="form-group">
                <label for="category" >Category</label>
               <select type="text" class="form-control borderzero" id="row-category"  name="category">
                   <option value="">Select a Category</option>
                 <option value="Buy and Sell">BUY AND SELL</option>
                     @foreach($buy_and_sell as $row)
                        <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                     @endforeach
                        <option value="Bidding">BIDDING</option>
                     @foreach($biddable as $row)
                        <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
                   @endforeach   
                      <option value="Cars and Vehicles">CARS AND VEHICLES</option>
                        <option value="Clothings">CLOTHINGS</option>
                    <option value="Events">EVENTS</option>
                    <option value="Jobs">JOBS</option>
                  <option value="Real Estate">REAL ESTATES</option>
                    <option value="Services">SERVICES</option>
                  <option value="All Categories">ALL CATEGORIES</option>
              @foreach($categories as $row)
                 <option value="{{$row->name}}"> &nbsp;&nbsp;{{$row->name}}</option>
               @endforeach
               </select>
              </div>
              <div class="form-group">
                <label for="title">Title</label>
                <input type="text" class="form-control" id="row-title" name="title">
              </div>
              <div class="form-group">
                <label for="email">Description</label>
                 <textarea  class="form-control" name="description" id="row-description" cols="20" rows="10"></textarea>
              </div>
         
       
        <div class="modal-footer">
         <input type="hidden" name="id" id="row-id" value="">
          <button type="submit" class="btn btn-default btn-success"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
      </div>
      </div>
    </div>
  </div>
</div>


    