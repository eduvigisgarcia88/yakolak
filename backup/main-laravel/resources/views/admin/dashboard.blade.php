@extends('layout.master')


@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-sm-12">
				<h3><i class="fa fa-tachometer"></i> {{$title}}</h3>
			</div>
			<!-- <div class="col-lg-6 col-sm-12">
			 	<button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>	
			 	<button type="button" class="btn btn-warning btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-shield"></i> Block</button>
			 	<button type="button" class="btn btn-info btn-md btn-attr pull-right" data-toggle="modal" data-target="#myModal"><i class="fa fa-user-plus"></i> Add</button>		
			</div> -->
		</div>
		<hr></hr>
		<!-- <div class="table-responsive">
			<table class="table table-hover">
			    <thead>
			      <tr>
			        <th>Firstname</th>
			        <th>Lastname</th>
			        <th>Email</th>
			        <th>Tools</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>John</td>
			        <td>Doe</td>
			        <td>john@example.com</td>
			        <td>
			        	<button type="button" class="btn btn-danger btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
			       		<button type="button" class="btn btn-primary btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
			       		<button type="button" class="btn btn-primary btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
			   		</td>		
			      </tr>
		 	 </table>
		 </div> -->
	</div>
</div>
@stop