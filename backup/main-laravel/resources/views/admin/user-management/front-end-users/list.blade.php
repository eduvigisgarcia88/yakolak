@extends('layout.master')
@section('scripts')

<script type="text/javascript">
   $_token = '{{ csrf_token() }}';
function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        console.log(response);
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.email + '</td>' +
              '<td class="rightalign">'+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
      			     '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
      				   '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
			  '</td></tr>';
            

        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);

        // $(".tablesorter").trigger('update');

        // // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

        loading.addClass("hide");

      }, 'json');
    } 

    function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";
      console.log(response);

        $.each(response.rows.data, function(index, row) {

           body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.email + '</td>' +
              //'<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td class="rightalign">';
                  
              body += '</td>'+
            '</tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

      }, 'json');
    
    }

    $(".content-wrapper").on("click", ".btn-add", function() {
           $("#modal-form").find('form').trigger("reset");
    	     $("#row-name").removeAttr('disabled','disabled');
           $("#row-email").removeAttr('disabled','disabled');
           $("#row-password").removeAttr('disabled','disabled');
           $("#row-password_confirmation").removeAttr('disabled','disabled');
           $("#row-mobile").removeAttr('disabled','disabled');
           $("#row-usertype_id").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".modal-title").html('<i class="fa fa-user-plus"></i><strong>Add User</strong>');
           $("#modal-form").modal('show');
       	
   	});
     $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/user/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-email").removeAttr('disabled','disabled');
                      $("#row-password").removeAttr('disabled','disabled');
                      $("#row-password_confirmation").removeAttr('disabled','disabled');
                      $("#row-mobile").removeAttr('disabled','disabled');
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
 	 $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/user/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-name").attr('disabled','disabled');
                      $("#row-email").attr('disabled','disabled');
                      $("#row-password").attr('disabled','disabled');
                      $("#row-password_confirmation").attr('disabled','disabled');
                      $("#row-mobile").attr('disabled','disabled');
                      $("#row-usertype_id").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
</script>
@stop
@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-sm-12 col-xs-12">
				<h3><i class="fa fa-users"></i> {{$title}}</h3>
			</div>
			<div class="col-lg-6 col-sm-12 col-xs-12">
				<button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>		
			 	<button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>	
			 	<button type="button" class="btn btn-warning btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-shield"></i> Block</button>
			 	<button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>		
			</div>
		</div>
		<hr></hr>
		<div class="table-responsive col-xs-12">
			<div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
			<table class="table">
			    <thead>
			      <tr>
			        <th>Name</th>
			        <th>Email</th>
			        <th class="text-right">Tools</th>
			      </tr>
			    </thead>
			    <tbody id="rows">
			    	@foreach($rows as $row)
			    		<tr data-id="{{$row->id}}">
			    			<td>{{$row->name}}</td>
			    			<td>{{$row->email}}</td>
			    			 <td>
					        	<button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
					       		<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
					   			<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
					   		</td>	
			    		</tr>
			    	@endforeach
			    <!--   <tr>
			        <td>John</td>
			        <td>Doe</td>
			        <td>john@example.com</td>
			       	
			      </tr> -->
		 	 </table>
		 </div>
	</div>
</div>
@stop
@include('admin.user-management.front-end-users.form')