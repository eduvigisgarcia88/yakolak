<!-- modal add user -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/user/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD USER</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
               <div class="form-group">
                <label for="row-photo" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                  <div class="uploader-pane">
                      <div class='change'>
                        <input type="hidden" id="row-photo">
                        <input name='photo' id='row-photo_upload' type='file' class='file borderzero file_photo' data-show-upload='false' placeholder='Upload a photo...'>
                        <button type="button" class="btn btn-primary borderzero" id="btn-cancel-photos">Cancel</button>
                      </div>
                </div>
                </div>
                </div>

                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="name" class="form-control" id="row-name" maxlength="30" placeholder="Enter your last name">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-usertype_id" class="col-sm-3 col-xs-5 control-label font-color">User Type</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control" id="row-usertype_id" name="usertype_id">
                      <option class="hide">Select</option>
                      @foreach($usertypes as $row)
                      <option value="{{ $row->id}}">{{ strtoupper($row->type_name)}}</option>
                      @endforeach
                  </select>
                </div>
                </div>
              </div>
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>