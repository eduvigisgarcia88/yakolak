@extends('layout.email')

@section('content')
    <p>Hello</p>
    <p style="margin: 20px 0;">You are receiving this e-mail because you need to validate your email.Just by clicking the button below:</p>
    <div>
    
      <![if !mso]>
       <table cellspacing="0" cellpadding="0">
        <tr> 
          <td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="{{ URL::to('register/verify/' . $confirmation_code) }}" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Verify your email address
              </a>
          </td> 
        </tr>
      </table> 
      <![endif]>
    </div>
    <p style="margin: 20px 0;">If the button doesn't work, copy and paste this link in your browser window:
    <p>This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes for security purposes.</p>
    <p style="margin-top: 20px;">Thank you!</p>
@stop
