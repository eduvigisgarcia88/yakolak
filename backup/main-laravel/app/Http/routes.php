<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', 'HomeController@index');
Route::get('ads/post', 'HomeController@postAds');
Route::post('get-city','HomeController@getCity');

Route::group(array('before' => 'ajax'), function() {
	
});
//Route::get('dashboard', 'FrontEndDashboardController@index');

Route::get('support', 'SupportController@index');
// Route::get('support/about-us', 'SupportController@showAboutUs');
// Route::get('support/contact-us', 'SupportController@showContactUs');
// Route::get('support/call-us', 'SupportController@showCallUs');
// Route::get('support/advertising', 'SupportController@showAdvertising');
// Route::get('support/faqs', 'SupportController@showFAQS');
// Route::get('support/help-us', 'SupportController@showHelpUs');



Route::get('dashboard/messages', 'FrontEndDashboardController@getMessages');
Route::get('dashboard/bids', 'FrontEndDashboardController@getBids');
Route::get('dashboard/listings', 'FrontEndDashboardController@getListings');
Route::get('dashboard/watchlist', 'FrontEndDashboardController@getWatchlist');
Route::get('dashboard/settings', 'FrontEndDashboardController@getSetttings');



Route::get('plans', 'PlanController@index');
Route::get('search','SearchController@index');
Route::get('buy','BuyController@index');
Route::get('sell','SellController@index');
Route::get('bid','BidController@index');
Route::get('login',	'AuthController@login');

Route::post('login', 'AuthController@doLogin');
Route::get('logout', 'AuthController@frontendLogout');

Route::get('dashboard', 'FrontEndUserController@fetchInfo');
Route::get('register', 'FrontEndUserController@register');
Route::get('register/verify/{confirmation_code}', 'FrontEndUserController@validateAccount');
Route::post('register/save', 'FrontEndUserController@registerSave');
Route::get('user/vendor', 'FrontEndUserController@getAds');

  Route::get('category/list', 'FrontEndCategoryController@getCategoryList');
  Route::get('buy', 'FrontEndCategoryController@getBuyCategory');
  Route::get('sell', 'FrontEndCategoryController@getSellCategory');
  Route::get('bid', 'FrontEndCategoryController@getBidCategory');

  
  Route::get('profiler', 'FrontEndUserController@index');
  //Route::get('dashboard', 'FrontEndUserController@fetchInfo');
  Route::get('profile/{id}/ads', 'FrontEndUserController@viewUserAds');
  Route::get('profile/view/{id}', 'FrontEndUserController@fetchInfoPublic');


Route::group(array('before' => 'ajax'), function() {
	Route::post('profile/fetch-info/{id}', 'FrontEndUserController@doFetchInfo');
	Route::post('profile/save', 'FrontEndUserController@registerSave');
	Route::post('profile/photo/save', 'FrontEndUserController@changePhoto');
	Route::post('profile/photo/remove', 'FrontEndUserController@removePhoto');
});


	Route::post('admin/login','AdminController@doLogin');
	Route::get('admin', 'AdminController@login');
Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/logout','AdminController@logout');
	Route::get('admin/dashboard','DashboardController@index');		
});

	 Route::get('ads/view/{id}', 'FrontEndAdvertisementController@fetchInfoPublicAds');
Route::group(array('before' => 'ajax'), function() {
	Route::post('ads/fetch-info/{id}', 'FrontEndUserController@doFetchInfoPublicAds');
	Route::post('ads/refresh', 'FrontEndAdvertisementController@doList');
	Route::post('ads/save', 'FrontEndAdvertisementController@saveAds');
	Route::get('ads/edit/{id}', 'FrontEndAdvertisementController@editAds');
});

Route::get('admin/ads', 'AdvertisementController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/ads/manipulate', 'AdvertisementController@edit');
	Route::post('admin/ads/save', 'AdvertisementController@save');
	Route::post('admin/ads/refresh', 'AdvertisementController@doList');
	Route::post('admin/ads/edit', 'AdvertisementController@edit');
	Route::get('admin/ads/{id}/view', 'AdvertisementController@viewAds');
});

Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/users', 'UserController@index');
	Route::post('admin/users/refresh', 'UserController@doList');
	Route::post('admin/user/edit', 'UserController@edit');
});

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/user/save', 'SystemController@save');

});

	Route::get('admin/category', 'CategoryController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/category/manipulate', 'CategoryController@edit');
	Route::post('admin/category/save', 'CategoryController@save');
	Route::post('admin/category/refresh', 'CategoryController@doList');
	Route::post('admin/category/get-subcat-sequence','CategoryController@checkSequenceSubCat');
	Route::post('admin/category/get-cat-sequence','CategoryController@checkSequenceCat');
	Route::post('admin/subcategory/manipulate','CategoryController@subEdit');
	

});
	Route::get('admin/category/custom-attributes', 'CustomAttributeController@index');
Route::group(array('before' => 'ajax'), function() {

	Route::post('admin/category/custom-attribute/save','CustomAttributeController@save');
	Route::post('admin/category/custom-attribute/refresh','CustomAttributeController@doList');
	Route::post('admin/category/custom-attribute/manipulate','CustomAttributeController@manipulateMainAttri');
	
});

