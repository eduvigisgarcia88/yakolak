<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;

use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
use App\Country;


class FrontEndAdvertisementController extends Controller
{
   public function index(){

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
     // $this->data['categories'] = Category::join('sub_categories', 'sub_categories.cat_id', '=', 'categories.id')
     //  ->select('sub_categories.*','categories.*')
     //  ->where(function($query) {
     //    $query->where('categories.status', '=', '1');
     // })->get();

      // $this->data['categories'] = Category::all();
// dd($this->data['categories']);
      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

   public function doList() {
        
      
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;


      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Advertisement::select('advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')
                         ->leftjoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('advertisements_photo.primary', '=', '1')
                                          ->where('advertisements.status', '=', '1')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        //dd($rows);
      // dd($rows);

      } else {
        $count = Advertisement::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

    }


    public function editAds($id){
     $row = Advertisement::find($id);
     $ads_id = $row->id;

     if($row) {    
         $this->data['ads'] = Advertisement::join('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                        ->select('advertisements_photo.*','advertisements.*')
                                      ->where(function($query) use($ads_id) {
                                            $query->where('advertisements_photo.ads_id','=',$ads_id);
                                     
                                        })->get();

    $this->data['ads_photos'] = AdvertisementPhoto::where('advertisements_photo.ads_id','=',$ads_id)->where('advertisements_photo.status','=',1)       
                                         ->latest()->take(4)->get();
// dd(count($this->data['ads_photos']));


// dd( $this->data['ads_photos']);

         // dd($this->data['ads']);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

      $this->data['title'] = "Edit My Ads"; 
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all(); 
    //dd($this->data['ads']);
      return view('ads.edit', $this->data);
   }


    public function saveAds(){
        $new = true;

        $input = Input::all();
      
        $photo_id = array_get($input, 'photo_id');
        // dd($photo_id );

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

     // getting all of the post data
      $files = Input::file('photo');

            $rules = [
             'photo'                  => 'max:3000',
             'title'                  => 'required|min:4|max:100',
             'description'            => 'required',
             'category'               => 'required',
             'minimum_allowed_amount' => 'required|min:1|numeric',
             'price'                  => 'required|min:5|numeric',
             'bid_duration_start'     => 'required|min:1|max:99|numeric',
        ];
        // field name overrides
        $names = [
            
            'title'              => 'title',
            'description'        => 'description',
            'price'              => 'price',
            'bid_duration_start' => 'bid duration',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

  
     // create model if new
      if($new) {
          $row = new Advertisement;
      }
          $row->category                  = array_get($input, 'category');
          $row->user_id                   = Auth::user()->id;
          // $row->photo                  = 'no_photo.jpg';
          $row->title                     = array_get($input, 'title');
          $row->price                     = array_get($input, 'price');
          $row->description               = array_get($input, 'description');
          $row->bid_limit_amount          = array_get($input, 'bid_limit_amount');
          $row->bid_start_amount          = array_get($input, 'bid_start_amount');
          $row->minimum_allowed_amount    = array_get($input, 'minimum_allowed_amount');
          $row->bid_duration_start        = array_get($input, 'bid_duration_start');
          $row->bid_duration_end          = array_get($input, 'bid_duration_end');
     
            // save model
          $row->save();


    
    if(!($new))
      {foreach($photo_id as $photo_ids) {
     $statusphoto = AdvertisementPhoto::find($photo_ids);
     $statusphoto->status = '2';
    $statusphoto->save(); 
    }
     
}


    // Making counting of uploaded images
    $file_count = count($files);
    // start count how many uploaded
    $uploadcount = 0;


    foreach($files as $file) {
      $uploadName =$row->id;
      // dd($file);
       $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
       $validator = Validator::make(array('file'=> $file), $rules);

     if ($validator->passes()){
        $destinationPath = 'uploads/ads';

        $files   = Advertisement::find($row->id);
        $filename = $file->getClientOriginalName();
        $uploadedFile =  $filename;

        // move photo
        $upload_success = $file->move($destinationPath, $uploadedFile);
        $img = Image::make('uploads/ads/' . $uploadedFile);
        $img->fit(900, 900);
        $img->save('uploads/ads/thumbnail/' . $uploadedFile);
        $uploadcount ++;
 
 // dd($uploadedFile);
        //insert photo
 

// dd($savephoto);

// dd($savephoto);
      if($new){ 
        $savephoto = new AdvertisementPhoto;
    if ($uploadcount == 1) {
        $savephoto->primary = 1; }
            
        $savephoto->ads_id = $row->id;
        $savephoto->photo = $uploadedFile;
      }
      else{        
        $savephoto = new AdvertisementPhoto;
        if ($uploadcount == 1) {
        $savephoto->primary = 1; }
        $savephoto->ads_id = $row->id;
        $savephoto->photo = $uploadedFile;

      }
         $savephoto->save();  
    } 
  }



    if($new){
     return Response::json(['body' => "Advertisement Created"]);
    } 
    else {
      //return Redirect::to('/')->withInput()->withErrors($validator);
      // return Response::json(['error' => array_unique($validator->errors()->all())]);
         return Response::json(['body' => "Advertisement Successfully updated"]);
    }
  }



    public function fetchInfoPublicAds ($id){

   $result = $this->doFetchInfoPublicAds($id);

   $this->data['ads_id'] = $id;

   $this->data['rows'] = $result['rows'];
   $this->data['thumbnails'] = $result['thumbnails'];
   $this->data['vendor_ads'] = $result['vendor_ads'];
   $this->data['featured_ads'] = $result['vendor_ads'];

   
 //dd($this->data['vendor_ads'] );
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();

      return view('ads.view', $this->data);
    }



  
public function doFetchInfoPublicAds($id){
  //get user
        $row = Advertisement::find($id);
        $user_id = $row->user_id;
      
        $rows = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.office_hours')                          
                                      ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.id', '=', $id);                                       
                                        })->first(); 

       $thumbnails = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                 ->where('advertisements_photo.ads_id', '=', $id);                                
                                        })->get(); 

       $vendor_ads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) use ($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 

       $featured_ads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) use ($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get();                                       




    if (Request::ajax()) {
        $result['rows'] = $rows->toArray();
        $result['thumbnails'] = $thumbnails->toArray();
        $result['vendor_ads'] = $thumbnails->toArray();
        $result['featured_ads'] = $featured_ads->toArray();
        return Response::json($result);
    } else {
        $result['rows'] = $rows;
        $result['thumbnails'] = $thumbnails;
        $result['vendor_ads'] = $vendor_ads;
        $result['featured_ads'] = $featured_ads;
        // $result['branches'] = $branch;
          return $result;
     }                                      

}











public function editor ()
{
   return View::make('editor.index');
}

public function sub2editor ()
{
   return View::make('editor.sub1.sub2.index');
}

public function formeditor ()
{
   return View::make('admin.ads-management.front-end-users.form');
}

}