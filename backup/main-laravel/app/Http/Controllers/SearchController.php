<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;

use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
use App\Country;
class SearchController extends Controller
{


	public function index(){
		$this->data['title'] = "Search";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('category.search', $this->data);
	}

}