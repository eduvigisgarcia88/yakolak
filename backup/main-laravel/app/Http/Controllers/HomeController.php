<?php 
namespace App\Http\Controllers;


use App\Http\Requests;
use App\AdvertisementPhoto;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Response;
use App\Advertisement;
use Request;
use App\Category;
use App\Country;
use App\City;
use App\Usertypes;
use Excel;
use Response;
use Auth;
use Redirect;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
     $row  = Advertisement::orderBy('id', 'desc')->first();

		  // $this->data['adslatestbidsxs'] = Advertisement::latest()->take(2)->get();
      $this->data['adslatestbidsxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                      'users.name', 'countries.countryName')                        
                                      ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
      // dd($this->data['adslatestbidsxs']);
     
  		$this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) use {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 
      // dd($this->data['adslatestbids']);
      $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                      ->where(function($query) {
                                            $query->where('advertisements_photo.primary', '=', '1')
                                                 ->where('advertisements.status', '=', '1');                                
                                        })->skip(4)->take(4)->get(); 
      $this->data['adslatestbuysellxs'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                           
                                      ->where(function($query) {
                                            $query->where('advertisements.category', '=', 'Buy and Sell')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

 // $this->data['adslatestbuysell'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
 //                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
 //                                       ->leftJoin('countries','countries.id', '=', 'users.country')
 //                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
 //                                        'users.name', 'countries.countryName')                           
 //                                      ->where(function($query) {
 //                                            $query->where('advertisements.category', '=', 'Buy and Sell')
 //                                                  ->where('advertisements_photo.primary', '=', '1') 
 //                                                  ->where('advertisements.status', '=', '1');                              
 //                                        })->orderBy('advertisements.created_at', 'desc')->take(8)->get(); 

   $this->data['adslatestbuysell'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category', '=', 'Buy and Sell')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category', '=', 'Buy and Sell')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 

		  $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
		  $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
		  $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();

      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
// Excel::load(storage_path('city.xlsx'), function($reader) {
//   $results = $reader->all();
//   // dd($results->take(10));
//   foreach ($results as $key => $field) {

//     $get_code = Country::where('countryCode', '=', $field->code)->first();
    
//     $row = new City;
//     $row->name = $field->name;

//     $row->country_id = $get_code->id;
//     $row->save();

//   }

// });

// dd("qa");

     return view('home', $this->data);
	}

public function editor()
{
  return View::make('admin.ads-management.front-end-users.ckfinder.index');	
  return view('plugins/ckfinder');	
}

public function viewAds($id)
{
      if (is_null(Advertisement::where('id', '=', $id)->first())) {
            return abort(404);
        } 
      $this->data['ads'] = Advertisement::find($id);

         $this->data['adslatestbids'] = Advertisement::latest()->take(4)->get(); 
         $this->data['adslatestbids2'] = Advertisement::latest()->skip(4)->take(4)->get(); 
         $this->data['ads2'] = Advertisement::where(function($query) {
                              $query->where('category', '=', 'Buy and Sell');    
                              })->take(8)->get();
         $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
         $this->data['countries'] = Country::all();
         $this->data['usertypes'] = Usertypes::all();
        if ($id) {
             $this->data['row'] = Advertisement::find($id);
            // if ($row) {
            //     return Response::json($row);
            // }
        }
      return view('ads.view', $this->data);
}


public function postAds()
{

        if (!Auth::check()) {
        return Redirect::to('/');
        }

         $this->data['adslatestbids'] = Advertisement::latest()->take(4)->get(); 
         $this->data['adslatestbids2'] = Advertisement::latest()->skip(4)->take(4)->get(); 
         $this->data['ads2'] = Advertisement::where(function($query) {
                              $query->where('category', '=', 'Buy and Sell');    
                              })->take(8)->get();
         $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
         $this->data['countries'] = Country::all();
         $this->data['usertypes'] = Usertypes::all();
        // if ($id) {
        //      $this->data['row'] = Advertisement::find($id);
        //     // if ($row) {
        //     //     return Response::json($row);
        //     // }
        // }
      return view('ads.post', $this->data);
}
public function getCity() {

      $get_city = City::where('country_id', '=', Request::input('id'))->orderBy('name','asc')->get();

      $rows = '<option class="hide" value="">Select:</option>';

      foreach ($get_city as $key => $field) {
        $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
      }
      return Response::json($rows);

}

}
