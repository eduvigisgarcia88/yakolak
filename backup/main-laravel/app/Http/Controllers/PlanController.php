<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;

class PlanController extends Controller
{

	public function index(){
		$this->data['title'] = "Plans";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('plans.list', $this->data);
	}

}
   