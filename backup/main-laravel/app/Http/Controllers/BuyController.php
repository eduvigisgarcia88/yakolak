<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;

class BuyController extends Controller
{

	public function index(){
		$this->data['title'] = "Buy";
		return View::make('buy.list', $this->data);
	}

}