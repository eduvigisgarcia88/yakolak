<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Validator;
use Response;
use Hash;
use Image;
class SystemController extends Controller
{
   

	public function save(){

		$new = true;

        $input = Input::all();
        dd($input);
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            //'photo' => $new ? 'image|max:3000' : 'image|max:3000',
            //'system_id' => $new ? 'required|min:2|max:100' : 'min:2|max:100',
            //'code' => 'required|min:2|max:100',
            //'username' => 'required|min:2|max:15|alpha_dash|unique:users,username' . (!$new ? ',' . $row->id : ''),
            'password' => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
            'name' => 'required|min:2|max:100',
            'email'  => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'mobile' => 'required|min:2|max:100',
        ];

        //do not require type if user is editing self
        if($new || ($new = false && $row->id != Auth::user()->id)) {
            $rules['usertype_id'] = 'required|exists:usertypes,id';
        }

        // field name overrides
        $names = [
            'name' => 'real name',
            'usertype_id' => 'designation',
            'system_id' => 'user id',
            'code' => 'user code',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

      

        // create model if new
        if($new) {
          $row = new User;
          $row->name      = strip_tags(array_get($input, 'name'));
          $row->email     = array_get($input, 'email');
          $row->mobile    = array_get($input, 'mobile');
          $row->username  = array_get($input, 'email');
        } else {
          // $log_code         = $row->code;
          // $log_name         = $row->name;
          // $log_email        = $row->email;
          // $log_mobile       = $row->mobile;
          // $log_username     = $row->username;
          $row->name      = strip_tags(array_get($input, 'name'));
          $row->email     = array_get($input, 'email');
          $row->mobile    = array_get($input, 'mobile');
          $row->username  = array_get($input, 'email');
          $row->save();
        }   

        // set type only if this isn't me
        if($new || ($new = false && $row->id != Auth::user()->id)) {
          $log_usertype_id = $row->usertype_id;
          $row->usertype_id = array_get($input, 'usertype_id');
        }

        // do not change password if old user and field is empty
        if($new || ($new = false && array_get($input, 'password'))) {
          $log_password = $row->password;
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        // if($new) {
        //     // auto generate id
        //     $row->system_id = 'USER-' . sprintf("%04d", $row->id);
        //     $row->save();

        // }

        // if (Request::hasFile('photo')) {
        //     // Save the photo
        //     Image::make(Request::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
        //     $row->photo = $row->id . '.jpg';

        // } else {

        //   if(array_get($input, 'image_link') || array_get($input,'image_fb_link_edit_photo'))
        //   {
        //       //dd(array_get($input, 'image_link'));
        //       $img = file_get_contents(array_get($input, 'image_link'));
        //       Image::make($img)->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
        //       $row->photo = $row->id . '.jpg';
        //       $row->save();

        //   }else{

        //     $photo = Settings::all()->first();
        //     $row->photo = $photo->default_photo;
        //     $row->save();

        //   }
           
        // }
        //    $row->save();
 		 return Response::json(['body' => 'System user created successfully.']);

	} 
}