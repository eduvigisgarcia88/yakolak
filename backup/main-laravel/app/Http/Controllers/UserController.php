<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;


class UserController extends Controller
{
   public function index(){

      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/users/refresh');
   	  $this->data['title'] = "User Management";
      $this->data['usertypes']  = Usertypes::all();
      return View::make('admin.user-management.front-end-users.list', $this->data);
   }
   public function edit(){
    $row = User::find(Request::input('id'));

    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }


   }
   public function doList() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        //dd($rows);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*', 'usertypes.type_name')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
   public function register() {

        if (!Auth::check()) {
           
            return view('auth/register');
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    public function doRegister(Request $request) {
    	
		$registerUser = new User;
		$registerUser->firstname = $request->input('firstname');
		$registerUser->lastname = $request->input('lastname');
		$registerUser->email = $request->input('email');
		$registerUser->username = $request->input('email');
		$registerUser->password =  Hash::make($request->password);
		$registerUser->remember_token = $request->input('_token');

		$registerUser->save();

    Session::flash('success', 'uploaded file is not valid');
    return Redirect::to('auth/register');
       
    }


}