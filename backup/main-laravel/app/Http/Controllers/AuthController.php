<?php

namespace App\Http\Controllers;

use Response;
use Input;
use View;
use Validator;
use Carbon;
use Auth;
use Paginator;
use DB;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\Category;
use App\AdvertisementPhoto;
use Redirect;
use App\Country;
use App\Usertypes;





class AuthController extends Controller
{
	public function index(){

		$this->data['title'] = "Admin";
        return View::make('admin.form', $this->data);
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Displays the login form
     *
     */
    public function frontendLogin() {

        if (!Auth::check()) {
            $this->data['title'] = "Login";
            $this->data['adslatestbidsxs'] = Advertisement::latest()->take(2)->get();
      
            $row  = Advertisement::orderBy('id', 'desc')->first();
            $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                          // ->select('advertisements_photo.*','advertisements.*')
                                          ->where(function($query) use ($row){
                                                $query->where('advertisements_photo.primary', '=', '1')
                                                ->where('advertisements.status', '=', '1');
                                                
                                            
                                            })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

            // dd($this->data['adslatestbids']);                            
             $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                          // ->select('advertisements_photo.*','advertisements.*')
                                          ->where(function($query) {
                                                $query->where('advertisements_photo.primary', '=', '1')
                                                ->where('advertisements.status', '=', '1');                                
                                            })->skip(4)->take(4)->get(); 
            // $this->data['ads2'] = Advertisement::where(function($query) {
      //                           $query->where('category', '=', 'Buy and Sell');    
      //                       })->take(8)->get();
            $this->data['adslatestbuysell'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                          
                                                                            ->where(function($query) {
                                                $query->where('advertisements.category', '=', 'Buy and Sell')
                                                ->where('advertisements_photo.primary', '=', '1') 
                                                ->where('advertisements.status', '=', '1');                              
                                            })->take(8)->get(); 
                                                 
            $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
            $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
            $this->data['ads2'] = Advertisement::where(function($query) {
                            $query->where('category', '=', 'Buy and Sell');    
                        })->take(8)->get();
            $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
            return view('/home', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }
     
    /**
     * Attempt to do login
     *
     */
    public function doFrontendLogin(Request $request) {
      //      $request = Input::all();
        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');

        if($honeypot) {
            // robot detected
            $error = trans('validation.blank');
        } 
        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
            // login successful
            return redirect()->intended();
        }
        else if(Auth::validate(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();

            if($user->status == 0) {
                // user is not active
                $error = "This account is currently inactive.";
            }
        }
        else {
            // invalid login
            $error = 'Invalid email or password.';
        }
        // return error
        return redirect()->action('AuthController@login')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
      // $rules = [
      //       'email' => 'required',
      //       'password' =>  'required',
      //       // 'g-recaptcha-response' => 'required|captcha'
      //   ];

      //   // field name overrides
      //   $names = [
      //       'email' => 'username|email',
      //       'password' => 'password',
      //       // 'g-recaptcha-response' => 'invalid captcha',
      //   ];

      //   // do validation
      //   $validator = Validator::make(Input::all(), $rules);
      //   $validator->setAttributeNames($names);

      //   // return errors
      //   if($validator->fails()) {
      //       return Response::json(['error' => array_unique($validator->errors()->all())]);
      //   }

      //   // $username = $request->input('email');
      //   // $password = $request->input('password');
      //   // $remember = (bool) $request->input('remember');
      //   // $honeypot = $request->input('honeypot');


      //   $username = array_get($request, 'email');
      //   $password = array_get($request, 'password');
      //   $remember = (bool) array_get($request, 'remember');
      //   // $honeypot = $request->input('honeypot');

      //   // if($honeypot) {
      //   //     // robot detected
      //   //     $error = trans('validation.blank');
      //   // } 
      //   if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
      //       // login successful
      //       return redirect()->intended();
      //   }
      //   else if(Auth::validate(['username' => $username, 'password' => $password])) {
      //       $user = User::where('username', $username)->first();

      //       if($user->status == 0) {
      //           // user is not active
      //           // $error = "This account is currently inactive.";
      //            return Response::json(['error' => "invalid username or password"]);
      //       }
      //   }
      //   else {
      //       // invalid login
      //         // return Response::json(['error' => "invalid username or password"]);
      //   }
      //   // return error
      // return Response::json(['error' => "invalid username or password"]);
    }


    public function login() {

        if (!Auth::check()) {
            $this->data['title'] = "Login";
             $row  = Advertisement::orderBy('id', 'desc')->first();
          $this->data['adslatestbidsxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->select('advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')                          
                                      ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
           $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->select('advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')                          
                                      ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 
          $this->data['adslatestbuysellxs'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                          
                                      ->where(function($query) {
                                            $query->where('advertisements.category', '=', 'Buy and Sell')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->take(2)->get(); 
           $this->data['adslatestbuysell'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                          
                                      ->where(function($query) {
                                            $query->where('advertisements.category', '=', 'Buy and Sell')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->take(8)->get(); 

            $this->data['countries'] = Country::all();
            $this->data['usertypes'] = Usertypes::all();
            return view('layout.frontend', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
            //return Redirect::to('/');
        }

    }
     
    /**
     * Attempt to do login
     *
     */
    public function doLogin(Request $request) {
      

        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        $captcha = $request->input('g-recaptcha-response');
 
        if($captcha == ""){
              $error = "Captcha is required";
        }else if($honeypot) {
            // robot detected
            $error = trans('validation.blank');
        } 
        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
            // login successful
            return redirect()->intended();
        }
        else if(Auth::validate(['username' => $username, 'password' => $password])) {

            $user = User::where('username', $username)->first();

            if($user->status == 2) {
                // user is not active
                $error = "This account is currently inactive.";
            }
        }
        else {
            // invalid login
            $error = 'Invalid email or password.';
        }

        // return error
        return redirect()->action('AuthController@login')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }
    /**
     * Displays the forgot password form
     *
     */
    public function forgotPassword() {
        $this->data['title'] = "Password Recovery";
        return View::make('users.forgotpass', $this->data);
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function doForgotPassword() {
        $user = User::where('email', Input::get('email'))
                    ->first();

        View::composer('emails.reminder', function($view) use ($user) {
            $view->with([
                'firstname' => $user->firstname
            ]);
        });

        $response = Password::remind(Input::only('email'), function($message) {
            $message->subject("Reset Password Request");
        });

        // (test) always say success
        $response = Password::REMINDER_SENT;

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'danger'
                ));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'success'
                ));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function resetPassword($token = null) {
        if(is_null($token)) App::abort(404);

        if(Auth::guest()) {
            $this->data['token'] = $token;
            $this->data['title'] = "Reset Password";

            return View::make('users.resetpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt change password of the user
     *
     */
    public function doResetPassword() {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 6 && $credentials['password'] != $credentials['email'];
        });

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()
                               ->withInput(Input::except('password'))
                               ->with('notice', array(
                                    'msg' => Lang::get($response),
                                    'type' => 'danger'
                ));
            case Password::PASSWORD_RESET:
                return Redirect::to('login')
                               ->with('notice', array(
                                    'msg' => Lang::get('reminders.complete'),
                                    'type' => 'success'
                ));
        }
    }

    public function frontendLogout() {
        Auth::logout();
        return redirect()->action('AuthController@login');
    }

    public function logout() {
        Auth::logout();
        return redirect()->action('AuthController@login');
    }

}
