<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\Usertypes;

class FrontEndDashboardController extends Controller
{
  
	public function index(){

		$this->data['title'] = "Dashboard";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.index', $this->data);
	}
	public function getMessages(){

		$this->data['title'] = "Dashboard-Messages";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.messages', $this->data);
	}
	public function getBids(){

		$this->data['title'] = "Dashboard-Bids";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.bids', $this->data);
	}
	public function getListings(){

		$this->data['title'] = "Dashboard-Listings";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.listings', $this->data);
	}
	public function getWatchlist(){

		$this->data['title'] = "Dashboard-WatchList";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.watchlist', $this->data);
	}
	public function getSetttings(){

		$this->data['title'] = "Dashboard-Settings";
		$this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.dashboard.settings', $this->data);
	}
	
 } 