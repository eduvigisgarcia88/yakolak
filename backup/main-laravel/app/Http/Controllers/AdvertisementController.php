<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;

use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
class AdvertisementController extends Controller
{
   public function index(){

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
     // $this->data['categories'] = Category::join('sub_categories', 'sub_categories.cat_id', '=', 'categories.id')
     //  ->select('sub_categories.*','categories.*')
     //  ->where(function($query) {
     //    $query->where('categories.status', '=', '1');
     // })->get();

      // $this->data['categories'] = Category::all();
// dd($this->data['categories']);
      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

   public function doList() {
        
      
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;


      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Advertisement::select('advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')
                         ->leftjoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                ->where(function($query) use ($status) {
                                    $query->where('advertisements_photo.primary', '=', '1')
                                          ->where('advertisements.status', '=', '1');
                                          
                                  })
                                // ->where(function($query) use ($search) {
                                //   $query->where('status', 'LIKE', '%' . $search . '%');
                                        
                                //   })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        //dd($rows);
      // dd($rows);

      } else {
        $count = Advertisement::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->join('usertypes', 'users.usertype_id', '=', 'usertypes.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

    }


    public function edit(){
     $row = Advertisement::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
   }


    public function save(){

 //      $this->data['title'] = "Ads Management";

 //      $new = true;

 //        $input = Request::all();

 //        $row = new Advertisement;
 //        // $row->user_id = Auth::user()->id;
 //        $row->category     = array_get($input, 'category');
 //        // $row->photo         = 'no_photo.jpg';
 //        $row->title      = array_get($input, 'title');
 //        $row->price      = array_get($input, 'price');
 //        $row->description  = array_get($input, 'description');
 //        $row->status        = 1;
 //        // save model
 //        $row->save();
 //        // $file               = array_get($input, 'photo');
 //        // $destinationPath    = 'uploads/ads';
 //        // $extension          = $file->getClientOriginalExtension();
 //        // $fileName           = $row->id . '.' . $extension;
 //        // $upload_success     = $file->move($destinationPath, $fileName);

 //      $photo = Input::file('photo');

 // // getting all of the post data
 //    $files = Input::file('photo');

 //            $rules = [
 //             // 'photo' => 'max:4000',
 //            'title' => 'required|min:2|max:100',
 //            'description' => 'required',
 //            'category' => 'required',
 //            // 'price' => 'required',

 //            // 'name' => 'required|min:2|max:100',
 //            // 'email' => 'required|email|unique:users,email,' . $row->id,
 //            // 'mobile' => 'required|min:2|max:100',
 //        ];
 //        // field name overrides
 //        $names = [
 //            'photo' => 'photo',
 //            'title' => 'title',
 //            // 'price' => 'price',
 //            'description' => 'description',
 //            // 'code' => 'user code',
 //        ];

 //        // do validation
 //        $validator = Validator::make(Input::all(), $rules);
 //        $validator->setAttributeNames($names); 

 //        // return errors
 //        if($validator->fails()) {
 //            return Response::json(['error' => array_unique($validator->errors()->all())]);
 //        }

 //    // Making counting of uploaded images
 //    $file_count = count($files);
 //    // start count how many uploaded
 //    $uploadcount = 0;


 //    foreach($files as $file) {
 //      $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
 //      $validator = Validator::make(array('file'=> $file), $rules);
 //      if($validator->passes()){
 //        $destinationPath = 'uploads/ads';

 //        $photo   = Advertisement::find($row->id);
 //        $filename = $file->getClientOriginalName();
 //        $uploadedFile =  $filename;

 //        // move photo
 //        $upload_success = $file->move($destinationPath, $uploadedFile);
 //        $img = Image::make('uploads/ads/' . $uploadedFile);
 //        $img->fit(300, 300);
 //        $img->save('uploads/ads/thumbnail/' . $uploadedFile);
 //        $uploadcount ++;
 
 //        //insert photo
 //        $rows = new AdvertisementPhoto;
 //        //for primary
 //        if ($uploadcount == 1) {
 //          $rows->primary = 1;
 //          }
 //        $rows->ads_id = $row->id;
 //        $rows->photo = $uploadedFile;
 //        $rows->save();  
 //      }
 
 //    } 
 //    if($new){
 //     return Response::json(['body' => "Advertisement Created"]);
 //    } 
 //    else {
 //      //return Redirect::to('/')->withInput()->withErrors($validator);
 //      return Response::json(['error' => array_unique($validator->errors()->all())]);
 //    }


         $this->data['title'] = "Ads Management";
        $new = true;

        $input = Request::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

     // getting all of the post data
      $files = Input::file('photo');

            $rules = [
             // 'photo' => 'max: 4000',
             // 'title' => 'required|min:2|max:100',
             // 'description' => 'required',
             // 'category' => 'required',
             // 'minimum_allowed_amount' => 'required|numeric',
             // 'price' => 'required|numeric',
             // 'bid_duration_start' => 'required|numeric',
        ];
        // field name overrides
        $names = [
            // 'photo' => 'photo',
            // 'title' => 'title',
            // 'description' => 'description',
            // // 'photo' => 'photo',
            // 'price' => 'price',
            // 'bid_duration_start' => 'bid duration',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

  
     // create model if new
      if($new) {
          $row = new Advertisement;
      }

          $row->category           = array_get($input, 'category');
          // $row->photo            = 'no_photo.jpg';
          $row->title               = array_get($input, 'title');
          $row->price               = array_get($input, 'price');
          $row->description         = array_get($input, 'description');
        if(array_get($input, 'bid_limit_amount')){
               $row->bid_limit_amount  = array_get($input, 'bid_limit_amount');
          }else{
               $row->bid_limit_amount = "null";
         }
         if(array_get($input, 'bid_start_amount')){
               $row->bid_start_amount  = array_get($input, 'bid_start_amount');
          }else{
               $row->bid_start_amount = "null";
         }
          
          if(array_get($input, 'minimum_allowed_amount')){
               $row->minimum_allowed_amount  = array_get($input, 'minimum_allowed_amount');
          }else{
               $row->minimum_allowed_amount = "null";
         }
         if(array_get($input, 'bid_duration_start')){
               $row->bid_duration_start  = array_get($input, 'bid_duration_start');
          }else{
               $row->bid_duration_start = "null";
         }
          if(array_get($input, 'bid_duration_end')){
               $row->bid_duration_end  = array_get($input, 'bid_duration_end');
          }else{
               $row->bid_duration_end = "null";
         }
      
            // save model
          $row->save();



    // Making counting of uploaded images
    $file_count = count($files);
    // start count how many uploaded
    $uploadcount = 0;


    // foreach($files as $file) {
    //   // dd($file);
    //    $rules = array('file' => 'required'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
    //    $validator = Validator::make(array('file'=> $file), $rules);
    //  if ($validator->passes()){
    //     $destinationPath = 'uploads/ads';

    //     $files   = Advertisement::find($row->id);
    //     $filename = $file->getClientOriginalName();
    //     $uploadedFile =  $filename;

    //     // move photo
    //     $upload_success = $file->move($destinationPath, $uploadedFile);
    //     $img = Image::make('uploads/ads/' . $uploadedFile);
    //     $img->fit(300, 300);
    //     $img->save('uploads/ads/thumbnail/' . $uploadedFile);
    //     $uploadcount ++;
 
    //     //insert photo
    //     if($new){
    //     $rows = new AdvertisementPhoto;
     
    //     //for primary
    //      if ($uploadcount == 1) {
    //           $rows->primary = 1;
    //         }

    //     $rows->ads_id = $row->id;
    //     $rows->photo = $uploadedFile;
    //     $rows->save();  
    //      }
    //     }
    // } 

    if($new){
     return Response::json(['body' => "Advertisement Created"]);
    } 
    else {
      //return Redirect::to('/')->withInput()->withErrors($validator);
      // return Response::json(['error' => array_unique($validator->errors()->all())]);
         return Response::json(['body' => "Advertisement Successfully updated"]);
    }
  }



    public function viewAds (){
      $id = Request::input('id');
        if ($id) {
            $row = News::find($id);
            if ($row) {
                return Response::json($row);
            }
        }
        return Response::json(['error' => "The requested item was not found in the database."]);
    }


public function editor ()
{
   return View::make('editor.index');
}

public function sub2editor ()
{
   return View::make('editor.sub1.sub2.index');
}

public function formeditor ()
{
   return View::make('admin.ads-management.front-end-users.form');
}

}