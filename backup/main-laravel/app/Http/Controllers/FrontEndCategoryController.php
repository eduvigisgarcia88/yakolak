<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\CustomAttributes;
use App\Advertisement;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Country;



class FrontEndCategoryController extends Controller
{


	public function getCategoryList(){

     $this->data['title'] = "Category";
     $this->data['ads'] = Advertisement::latest()->get();
  	 $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
  	 $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
  	 $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
     $this->data['countries'] = Country::all();
     $this->data['usertypes'] = Usertypes::all();
     return View::make('category.list', $this->data);
  }

  public function getBuyCategory(){
  		$this->data['title'] = "Buy";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.buy', $this->data);
  }
  public function getSellCategory(){
  		$this->data['title'] = "Sell";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.sell', $this->data);
  }
  public function getBidCategory(){
  	  $this->data['title'] = "Bid";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.bid', $this->data);
  }
}