<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\UserPlanTypes;
use App\Http\Controllers\Controller;
use Form;
use Hash;
use Validator;
use Redirect;
use Url;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Advertisement;
use App\Category;
use App\Country;
use App\City;
use App\AdvertisementPhoto;
use App\AdvertisementWatchlists;
use Image;
use Mail;
use App\CompanyBranch;
use Carbon\Carbon;
use App\UserRating;
use App\AdRating;
use App\UserComments;
use App\UserCommentReplies;
use App\UserAdComments;
use App\UserNotifications;
use App\UserPlanRates;
use App\UserAdCommentReplies;
use App\UserMessages;
use App\UserMessagesHeader;
use App\Newsletters;
use App\NewslettersUserCounters;
use App\Languages;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use App\BidPoints;
use App\AuctionBidders;
use App\UserNotificationsHeader;
use App\UserPlanPrices;
use App\UserAdViews;
use App\UserFavoriteProfile;
use App\UserBidList;
use App\UserImportContacts;
use App\UserRegisterReferral;
use App\CountryCode;


class FrontEndUserController extends Controller
{
  public function index(){
      $this->data['title'] = "Dashboard"; 
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                  
      return view('user.view', $this->data);
  
  }

  public function doFetchInfo($id){
    $rows = User::with("getUserCompanyBranch")->find($id);
    $get_all_rate = UserRating::where('vendor_id','=',$id)->avg('rate');

    $get_average_rate = Advertisement::select('id')->where('user_id','=',$id)->count();
    
    $getalladswithrating = AdRating::select('advertisements.id','ad_ratings.*')
                                      ->where('advertisements.user_id',$id)
                                      ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                      ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                      ->avg('rate');
      // dd($getalladswithrating);
      if(!is_null($getalladswithrating)){
        $get_average_rate = $getalladswithrating;
      }else{
        $get_average_rate = 0;
      }
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();



      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

    $get_countries = Country::where('status',1)->get();

    $user_country = "";

    // $user_country .= '<select name="country" id="client-country" class="form-control borderZero inputBox fullSize">';
      foreach ($get_countries as $key => $field) {
        $user_country .= '<option value ="'.$field->id.'" '.($rows->country == $field->id ? 'selected':'').'>'.$field->countryName.'</option>';
      }
    // $user_country .=   '</select>';



    $get_city = City::where('status',1)->get();

    $user_city  = "";
    // $user_city .= '<select name="city" id="client-city" class="form-control borderZero inputBox fullSize">';
       foreach ($get_city as $key => $field) {
        $user_city .= '<option value ="'.$field->id.'" '.($rows->city == $field->id ? 'selected':'').'>'.$field->name.'</option>';
      }
    // $user_city .= '</select>';

    $branch = "";
    if (count($rows->getUserCompanyBranch) > 0) {
      $ctr = 0;      
      foreach ($rows->getUserCompanyBranch as $key => $field) {
         if($field->status == 1){
         $branch .= '<div id ="accountBranch"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 borderbottomLight normalText noPadding bottomPadding topMarginB bottomMarginB redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText">' . ($ctr == 0 ? '<button type="button" value="" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground readonly-view"><i class="fa fa-plus redText"></i> ADD BRANCH</button>' : '<button type="button" value="" class="hide-view btn-del-account-branch pull-right borderZero inputBox noBackground readonly-view"><i class="fa fa-minus redText"></i> REMOVE</button>') . '</span>' .
            '</div>' .
            '<div class="row">'.
            '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="'.$field->company_tel_no.'" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize readonly-view">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as $gckey => $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '" ' . ($field->company_country == $gcfield->id ? 'selected' : '') . '>' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="'.$field->company_address.'" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="'.$field->company_office_hours.'" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="'.$field->company_email.'" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="'.$field->company_mobile.'" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize readonly-view">' .
                    '<option class="hide">Select</option>';

                    $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get()->take(50);
                      // dd($get_cities);
                    foreach ($get_cities as $gkey => $gfield) {
                      
                      $branch .= '<option value="' . $gfield->id . '" ' . ($field->company_city == $gfield->id ? 'selected' : '') .'>' . $gfield->name . '</option>';

                    }

                 $branch .= '</select></div>'.
                 '</div>'.
                '</div>'.
              '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id" value="'.$field->id.'">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status" value="'.$field->status.'">'.
          '</div></div>';

          $ctr += 1;
      }
    }
    }
    else {
      $branch .= '<div id ="accountBranch"><div class="col-lg-12  borderbottomLight normalText noPadding bottomPadding topMarginB bottomMargin redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText"><button type="button" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground readonly-view"><i class="fa fa-plus redText"></i> ADD BRANCH</button></span>' .
            '</div>' .
            '<div class="row">'.
            '<div class="col-lg-6 ">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize readonly-view">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as  $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '">' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 ">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize readonly-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
               '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize readonly-view">' .
                    '<option class="hide">Select</option>';

                    // $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get();
                    //   // dd($get_cities);
                    // foreach ($get_cities as $gkey => $gfield) {
                      
                    //   $branch .= '<option value="' . $gfield->id . '">' . $gfield->name . '</option>';

                    // }

                 $branch .= '</select></div>'.
                '</div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status">' .
          '</div></div>';
    }
    $branches_raw = "";
      if($rows->usertype_id == 2){

      if (count($rows->getUserCompanyBranch) > 0) {
        $ctr = 0;      
        foreach ($rows->getUserCompanyBranch as $key => $field) {
           if($field->status == 1){
              $get_public_country = Country::where('id','=',$field->company_country)->first(); 
              $get_public_city = City::where('id','=',$field->company_city)->first();
              //dd($get_public_city);
              $branches_raw .= '<div class="container-fluid topMarginB borderbottomLight"><div class="container"><div class="col-md-12 col-sm-12 col-xs-12 noPadding">'.
                          '<div class="col-md-12 col-sm-12 col-xs-12 panelTitle">'.$get_public_country->countryName.' BRANCH INFORMATION<a class="btn pull-right grayText normalText" data-toggle="collapse" data-target="#'.$field->id.'"><i class="fa fa-angle-down"></i></a></div>'.
                          '<div id="'.$field->id.'" class="col-md-12 col-sm-12 col-xs-12 noPadding collapse">'.
                            '<div class="col-md-3 col-sm-5 col-xs-12">'.
                            '<iframe class="bottomPaddingC" width="100%" height="206px"frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDuGKZMGH7Fo4H8SWyvNJWYelH5brT4bTg&q='.$get_public_city->name.'+'.$get_public_country->countryName.'" allowfullscreen></iframe>'.
                            '</div>'.
                            '<div class="col-md-9 col-sm-7 col-xs-12">'.
                              '<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText">'.$field->company_address.'('.$get_public_city->name.','.$get_public_country->countryName.')</span></div>'.
                              '<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText">'.$field->company_mobile.'/'.$field->company_tel_no.'</span></div>'.
                              '<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText">'.$field->company_email.'</span></div>'.
                              '<div class="mediumText grayText"><strong>Office Hours:</strong><span>'.$field->company_office_hours.'</span></div>'.
                            '</div>'.
                            '</div>'.
                            '</div>'.
                          '</div></div>';

            $ctr += 1;
           }
      }
    }else{
         $branches_raw .= '<div class="container topMarginB"><div class="col-md-12 col-sm-12 col-xs-12 noPadding">'.
                          '</div>';
    }
  }else{
    $branches_raw = "";
  }
       $form="";

   if(Auth::check()){

          $user_id = Auth::user()->id;

          $check_rates = UserRating::where('vendor_id','=',$id)->where('user_id','=',$user_id)->first();

           $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body borderBottom bottomPaddingB noPadding">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.url('uploads').'/'.Auth::user()->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'user/comment', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="ad-comment" name="comment"></textarea></p>'.
                              '<input type="hidden" name="user_id" value="'.$id.'">'.
                              '<input type="hidden" name="vendor_id" value="'.Auth::user()->id.'">'.
                          '</div>'.
                          '<span class="pull-right" id="review-product-pane">'.
                            '<span id="rateYo" class="mediumText adRating pull-left '.( Auth::user()->id  == $id ? "hide" : "").''.( is_null($check_rates) ? '' : 'hide').'"></span>'.
                            '<button type="button" class="btn btn-sm rateButton borderZero btn-rate_product '.( Auth::user()->id  == $id ? 'hide' : '').''.( $check_rates  != null ? 'hide' : '').'"><i class="fa fa-star"></i> Rate Product</button>'.
                            '<button type="submit" class="btn btn-sm blueButton borderZero btn-save_comment leftMargin"><i class="fa fa-comments"></i> Leave Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
      }else{
          $user_id = null;
          
              $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body borderBottom noPadding">'.
                   '<p><div class="alert alert-info borderZero">You must be logged in to post a comment. Login, Register or Login with Facebook</div>
                   </p>'.
                   '</div>'.
                   '</div>'.
                   '</div>';
      }

   
    $stars ="";
    // $stars_reply = "";
    $comment_info= User::with('getUserComments')->find($id);
    $comments ="";
    $ctr = 1;

    if (count($comment_info->getUserComments) > 0) {
      foreach ($comment_info->getUserComments as $key => $field) {

                    $get_vendor_info = User::find($field->vendor_id);  
                    $get_user_rating = UserRating::where('user_id','=',$field->vendor_id)->where('vendor_id','=',$id)->first();
            
                        if(!is_null($get_user_rating)){
                            if($get_user_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
                            }

                            if($get_user_rating->rate == 4){
                              $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i></span>';   
                            }

                            if($get_user_rating->rate == 3){
                             $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_user_rating->rate == 2){
                             $stars = '<span class="mediumText"><i class="fa fa-sta rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_user_rating->rate == 1){
                             $stars = '<span class="mediumText "><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_user_rating->rate == null){
                              $stars = "";
                            }
                          }else{
                            $stars = "";
                          }
                    
       $comments .= '<div id="reply-pane" class="'.($ctr % 2  == 0 ? 'panel panel-default bgGray nobottomMargin borderZero borderBottom removeBorder' : 'panel panel-default nobottomMargin bgWhite borderZero removeBorder').' ">'.
          '<div class="panel-body topPaddingD nobottomPadding" id="replies-container">'.
            '<div class="commentBlock">'.
              '<div class="commenterImage">'.
                 '<img src="'.url('uploads').'/'.$get_vendor_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                '</div>'.
                '<div class="commentText bottomPaddingC">'.
                '<div class="panelTitle nobottomPadding">'.$get_vendor_info->name.' '.$stars.'<span class="lightgrayText text-capitalize topPadding normalWeight normalText pull-right">'.date("F j, Y", strtotime($field->created_at)).'</span></div>'.
                '<p class="normalText nobottomMargin nobottomPadding">'.$field->comment.'</p>'.
                '<div class="subText">'.
                '<a '.($get_vendor_info->id == $user_id ? '': 'data-name='.$get_vendor_info->name ).' id="show" role="button" class="blueText normalText '.($field->vendor_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-comments"></i> Reply</a>'.
                  // '<span class="blueText normalText "><i class="fa fa-comments"></i> Reply</span>'.
                  '<span class="leftMarginB redText normalText '.($field->vendor_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>'.
                  
                '</div>';
                 $get_replies = UserCommentReplies::where('comment_id','=',$field->id)->orderBy('created_at','desc')->get();

                foreach ($get_replies  as $key => $reply_info) {
                    $get_user_info = User::where('id','=',$reply_info->user_id)->first();
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' borderZero removeBorder replyBlock bordertopLight nobottomMargin topMarginB">
                      <div class="panel-body topPaddingD  norightPadding nobottomPadding bordertopLight">
                        <div class="commentBlock">
                          <div class="commenterImage">
                             <img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                            </div>
                            <div class="commentText">
                            <div class="panelTitle nobottomPadding">'.$get_user_info->name.'<span class="lightgrayText topPadding text-capitalize normalText normalWeight pull-right">'.date("F j, Y", strtotime($reply_info->created_at)).'</span></div>
                            <p class="normalText nobottomPadding nobottomMargin">'.$reply_info->content.'</p>
                            <div class="subText">
                              <a '.($get_user_info->id == $user_id ? '': 'data-name='.$get_user_info->name ).' id="show_reply_box" role="button" class="blueText normalText '.($user_id == null ? 'hide':'').'"><i class="fa fa-comments"></i> Reply</a>
                              <span class="leftMarginB redText normalText '.($reply_info->user_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>
                              
                              </div>
                              <div>
                            </div>';
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' borderZero removeBorder nobottomMargin replyBlock bordertopLight inputReplyBox hidden">'.
                                  '<div class="panel-body norightPadding nobottomPadding">'.
                                    '<div class="commentBlock">'.
                                    Form::open(array('url' => 'user/comment/reply', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment_reply')).
                                    '<textarea class="form-control borderZero fullSize" rows="2" id="reply-message" name="reply_content"></textarea>'.
                                    '<input type="hidden" name="comment_id" value="'.$field->id.'">'.
                                    '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                                    '</div>'.
                                    '<span class="pull-right">'.
                                      '<button class="btn normalText blueButton submitReply borderZero topMargin"> Submit</button>'.
                                    '</span>'.
                                    Form::close().
                                  '</div>'.
                              '</div>                       
                            </div>
                        </div>
                      </div>
                  </div>';                          

                }
     
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomMargin borderZero removeBorder replyBlock bordertopLight inputReply hidden">'.
                  '<div class="panel-body norightPadding nobottomPadding '.($field->vendor_id == $user_id  ? 'hide':'').'">'.
                    '<div class="commentBlock">'.
                    Form::open(array('url' => 'user/comment/reply', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment_reply')).
                    '<textarea class="form-control borderZero fullSize" rows="2" id="main_comment_reply" name="reply_content"></textarea>'.
                    '<input type="hidden" name="comment_id" value="'.$field->id.'">'.
                    '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                    '</div>'.
                    '<span class="pull-right">'.
                      '<button class="btn normalText blueButton submitReply borderZero topMargin parentSubmit"> Submit</button>'.
                    '</span>'.
                    Form::close().
                  '</div>'.
              '</div>'.
                '</div>'.
            '</div>'.
          '</div>'.
        '</div>';
          $ctr++;

      }

    }else{
      $comments .= '';
    }
     
    $get_user_messages = UserMessagesHeader::where(function($query) use($user_id){
                                                    $query->where('reciever_id','=',$user_id)
                                                          ->orWhere('sender_id',$user_id);
                                              })
                                              ->where('status',1)
                                              ->orderBy('updated_at','desc')
                                              ->get();
   
      //dd($get_user_messages );
      if(count($get_user_messages)>0){
              $messages_header ='';
              foreach ($get_user_messages as $key => $field){

                 $messages_header .=  '<tr><td data-header-id="'.$field->id.'" class="view-message"><i class="'.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'fa fa-envelope grayText':'fa fa-envelope-o grayTextB').' rightPadding"></i><span class="normalText '.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'grayText':'grayTextB').'">'.$field->title.'</span></td>
                                       <td class="text-right smallText"><span class="rightPadding '.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'grayText':'grayTextB').' normalWeight">'.date("m/d/y g:i A", strtotime($field->created_at)).'</span><span class="redText"><button class="borderZero removeborder noBackground" id="delete-message" data-id= "'.$field->id.'"><i class="fa fa-trash"></i></button></span></td></tr>';
              }
      }else{
          $messages_header = '';
      }
     


   if(Auth::check() != false){   
            $get_advertisement_info = User::with('getUserAdvertisements')
                                          ->with('getUserAdvertisements.getAdvertisementComments')
                                          ->find($user_id);
                                         
                  $ctr=1;                          
                    $review_summary = "";
                      if(count($get_advertisement_info->getUserAdvertisements) > 0){

                          foreach ($get_advertisement_info->getUserAdvertisements as $key => $field) {

                             if(count($field->getAdvertisementComments) > 0){
                       foreach ($field->getAdvertisementComments as $key => $value) {

                         if($value->vendor_id != $user_id){   
                            $review_summary .= '<table id="myTableCommentsReview" class="table '.($ctr % 2  == 0 ? 'bottomPaddingB':'bottomPaddingB').'">
                                              <thead><tr data-id=""><th class="panelTitle bottomPaddingB"><a class="blueText" href='.url('ads/view')."/".$field->id.'>'.$field->title.'</a></th></tr></thead>';
                                            
                            $get_advertisement_comments =  UserAdComments::where('ad_id','=',$field->id)->get();

                              foreach ($get_advertisement_comments as $key => $field_info) {
                                 $get_user_info = User::where('id','=',$field_info->vendor_id)->first();
                                $get_ad_rating = AdRating::where('user_id','=',$field_info->vendor_id)->where('ad_id','=',$field->id)->first();
                        if(!is_null($get_ad_rating)){
                            if($get_ad_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars"> <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
                            }
                            if($get_ad_rating->rate == 4){
                              $stars = '<span class="mediumText"> <i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i></span>';   
                            }
                            if($get_ad_rating->rate == 3){
                             $stars = '<span class="mediumText"> <i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_ad_rating->rate == 2){
                             $stars = '<span class="mediumText"> <i class="fa fa-sta rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_ad_rating->rate == 1){
                             $stars = '<span class="mediumText "> <i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                          }else{
                            $stars = "";
                          }                   
                          $review_summary .= '<tbody class="topPadding norightPadding nobottomPadding bordertopLight"><tr><td class=" nobottomPadding">
                                 <div class="commentBlock">
                                                    <div class="commenterImage">
                                                       <img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                                                      </div>
                                                      <div class="commentText bottomPaddingC">
                                                      <div class="panelTitleB nobottomPadding"><a class="blueText" href='.url('/')."/".$get_user_info->alias.'>'.$get_user_info->name.'</a>'.$stars.'<span style="font-weight:normal !important" class="lightgrayText topPadding text-capitalize normalText normalWeight pull-right">'.date("F j, Y", strtotime($field->created_at)).'</span></div>
                                                      <p class="normalText nobottomPadding nobottomMargin">'.$field_info->comment.'</p>
                                                      <div class="subText">
                                                        <a id="show_reply_box" href= "'.url('ads/view').'/'.$field->id.'" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>
                                                        <span class="normalText"><a class="leftMarginB redText normalText" target="_blank" href='.url($get_user_info->alias).'#'.'direct'.'|'.str_slug($field->title,'|').' class="leftMarginB redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</a></span>
                                                        
                                                      </div>
                                                      </div>
                                                  </div></td></tr></tbody>';
                           }           
                           $review_summary .= '</table>';

                           $ctr++;
                          }else{
                            $review_summary .= '';
                          }  
                          }
                          }
                          else{
                            $review_summary .= '';
                          } 
                          
                          }

               }else{
                  $review_summary = "";
                }
          }else{
            $review_summary = "";
          }

 // dd($get_user_info);


//SUBSCRIPTION
if(Auth::check()){
    $token = Session::token();

    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;

    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->where('status', '=', 1)->get();         
    $get_free_plan= UserPlanTypes::where('usertype_id', '=', $usertype_id)
                                  ->where(function($query){
                                             $query->where('id', '=',1)
                                                   ->orWhere('id', '=',5);
                                  })  
                                  ->where('plan_status', '=', 1)
                                  ->where('status', '=', 1)
                                  ->get();
 
    $get_current_plan = UserPlanTypes::find($user_plan_types);
    $created = new Carbon(Auth::user()->created_at);
    $now = Carbon::now();
    $difference = ($created->diff($now)->days < 1)
    ? 'today'
    : $created->diffForHumans($now);
    //check if days not months
   
    $arr = explode(' ',trim($difference));
    
    if ($arr == 'today') {
      $diff = '0';
    } else {
           if ($arr == 'days') {
           $diff = '0';
           $test = 'true';
          } elseif ($arr == 'years' || $arr == 'year') {
           $diff = '12';
          }
          else {
           $diff = substr($difference, 0, 1); 
          }
    }


$get_plan_rate = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->first();

$duration = $get_plan_rate->duration;
$discount = $get_plan_rate->discount;

$ads_posted = Advertisement::where(function($query) use ($user_id){
                                             $query->where('ads_type_id','=',1)
                                                   ->where('user_id','=',$user_id)
                                                   ->where('status','!=',2);
                                  })->get();
$total_ads_rem = $get_current_plan->ads - count($ads_posted);

$auction_posted = Advertisement::where('ads_type_id','=',2)->where('user_id','=',$user_id)->where('status','!=',2)->get();
$total_auction_rem = $get_current_plan->ads - count($auction_posted);

$all_posted = Advertisement::where('user_id','=',$user_id)->where('status','!=',2)->get();
$total_ads_allowed_rem = $get_current_plan->total_ads_allowed - count($all_posted);

$post_with_video = Advertisement::where('user_id','=',$user_id)->where('status','!=',2)->where('youtube','!=','null')->get();
$total_video_rem = $get_current_plan->video_total - count($post_with_video);

$get_user_bid_rem = User::where('id','=',$user_id)->first();
$total_user_bid_rem = $get_current_plan->bid + $get_user_bid_rem->bid;


$plan = "";
$plan .=' <div id="" class="col-md-12 noPadding">
                          <div><div class=" xxlargeText"><strong><span class="blueText">'.strtoupper($get_current_plan->plan_name).'</span> <span class="lightgrayText"> ZERO COST FOREVER</span></strong> </div>
                           <div>
                           <div class="normalText grayText bottomPaddingC">
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue leo eu erat elementum placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam at dui dui. Nunc rhoncus tellus eu neque ultrices molestie. Vivamus eget rutrum leo. Nulla dapibus ligula quis massa faucibus sollicitudin sed ac purus. Maecenas egestas rhoncus nunc, sit amet vestibulum lectus iaculis nec. Pellentesque at ipsum id odio suscipit commodo a a dui. Nam turpis nibh, imperdiet sed urna quis, auctor maximus quam. Cras scelerisque metus tempus ligula gravida blandit. Praesent dictum tempor quam varius laoreet. Morbi quis libero ut erat egestas lacinia laoreet nec mauris. Ut nec imperdiet lectus, at ullamcorper orci. Aenean ultrices iaculis augue at tempor.
                            </div>
                            <div class="col-lg-12 noPadding">
                            <div class="col-lg-4 noleftPadding">
                            <div class="normalText normalText grayText">
                              <span class=""> No. of Ads Remaining<span class="pull-right blueText bold">'.abs($total_ads_rem).'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> No. of Auctions Remaining<span class="pull-right blueText bold">'.abs($total_auction_rem).'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Total Allowed Posting Remaining<span class="pull-right blueText bold">'.abs($total_ads_allowed_rem).'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class="">  No. of Image per Ad Remaining<span class="pull-right blueText bold">'.abs($get_current_plan->img_per_ad).'</span></span>
                            </div>
                            
                            </div>
                            <div class="col-lg-4 noleftPadding">
                            <div class="normalText normalText grayText">
                              <span class="">  No. of Videos per Ad Remaining<span class="pull-right blueText bold">'.abs($total_video_rem).'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Total Free Points Remaining<span class="pull-right blueText bold">20 </span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Total Bids Remaining<span class="pull-right blueText bold">'.abs($total_user_bid_rem).'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Free SMS Notification Remaining<span class="pull-right blueText bold">200 </span></span>
                            </div>
                            </div>
                            </div>
                          </div>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" id="subscription-upgrade">
                      <hr class="nobottomMargin">
                     '; 

 //COMPUTE POSTED ADS FOR REPORTS TAB
        $total_ad_posted = Advertisement::where(function($query) use($user_id){
                                             $query->where('status', '!=', '2')
                                                   ->where('user_id', '=', $user_id);                                      
                                        })->orderBy('created_at', 'desc')->get();

        $total_auction_posted = Advertisement::where(function($query) use($user_id){
                                             $query->where('status', '!=', '2')
                                                   ->where('ads_type_id', '=', '2')
                                                   ->where('user_id', '=', $user_id);                                      
                                        })->orderBy('created_at', 'desc')->get(); 
        $total_uploaded_photos = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                         ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.photo', '!=', 'null')
                                                    ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->get();


        $total_embed_video = Advertisement::where('user_id','=', $user_id)->where('youtube', '!=', 'null' )->get();
        $get_user_plan_types = UserPlanTypes::find($user_plan_types);
        $total_ads_allowed = $get_user_plan_types->total_ads_allowed;
        $auction = $get_user_plan_types->auction;
        $img_total =  $get_user_plan_types->img_total;
        $video_total = $get_user_plan_types->video_total;
  
          //compute listing available
                  $listing_available = $total_ads_allowed - count($total_ad_posted);
                 if ($listing_available <= 0) {
                   $get_listing_available = '0';
                 } else{
                  $get_listing_available = $listing_available;

                 }
         //compute photos available
               $upload_photos_available = $img_total - count($total_uploaded_photos);
               if ($upload_photos_available <= 0) {
                 $get_upload_photos_available = '0';
               } else{
                $get_upload_photos_available = $upload_photos_available;
               }

          //compute videos available
               $embed_videos_available = $video_total - count($total_embed_video);
               if ($embed_videos_available <= 0) {
                 $get_embed_videos_available = '0';
               } else{
                $get_embed_videos_available = $embed_videos_available;
               }
foreach ($get_free_plan as $value) {
     $plan .='<div class="col-md-3 blockTop">
                            <div class="row panel panel-default borderZero nobottomMargin" style="'.($get_current_plan->id == $value->id ? 'border:2px solid #ff6d6d;':'border:none').'">
                              <div class="panel-heading planTitleBar borderbottomPlan" style="'.($get_current_plan->id == $value->id ? 'border-bottom:none !important; background-color: #ff6d6d !important;margin-left:0px;margin-right:0px;':'background-color: #ffffff;margin-left:5px;margin-right:5px;').'"><div class="mediumText"  style="'.($get_current_plan->id == $value->id ? 'color:#ffffff':'color:#5da4ec').'"><strong>'.strtoupper($value->plan_name).  '</strong></div></div>
                              <div class="panel-body" style="'.($get_current_plan->id == $value->id ? 'padding-bottom: 30px;background-color: #ffffff':'padding-bottom: 0px; background-color: #ffffff').'">
                                <div class="text-center borderbottomLight" style="'.($get_current_plan->id == $value->id ? 'padding-bottom: 63px; border-bottom:1px solid #b3b3b3 !important; ':'border:none; padding-bottom: 63px;').'">
                                '.($user_plan_types == $value->id ? '<div class="normalText" style="color:#2C9245; "></div>' : '<div class="normalText lightgrayText"></div> ').'                                              
                                    <div class="'.(count($get_plan) == 4 ? 'largeText':'xlargeText').' xxlargeText '.($get_current_plan->id == $value->id ? 'redText':'blueText').'"><strong>'.($value->id == 1 || $value->id == 5? 'FREE':'').'</strong></div>                          
                                </div>
                                <div class="blockTop lineHeightC">
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> '.($user_plan_types == $value->id ? 'No. of Ads  <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_listing_available.'</span>':'No. of Ads <span class="pull-right blueText bold">'.$value->ads.'').'</span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> '.($user_plan_types == $value->id ? 'No. of Auction  <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_listing_available.'</span>':'No. of Auctions <span class="pull-right blueText bold">'.$value->auction.'').'</span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> '.($user_plan_types  == $value->id ? 'No. of Image per Ad  <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_upload_photos_available.'</span>':' No. of Image per Ad <span class="pull-right blueText bold">'.$value->img_per_ad .'').'</span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> '.($user_plan_types  == $value->id ? 'No. of Video per Ad  <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_embed_videos_available.'</span>':' No. of Videos per Ad <span class="pull-right blueText bold">'.$value->video_total .'').'</span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> Total Free Points <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->point.' </span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> Total Bids <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->bid.' </span></span>
                                </div>
                                <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                                  <span class=""> Free SMS Notification <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->point_exchange.' </span></span>
                                </div>
                                <div class="blockTop text-center">';
            if (Auth::check()) {
         $plan .='<form action="plan/payment" id="upgrade" method="post">
                  <input type="hidden" name="_token" id="csrf-token" value="'.$token.'" />
                  <input type="hidden" name="plan_id" value="'.$value->id.'" />';
      }  
        if (Auth::check()) {
                $plan .=($user_plan_types  == $value->id ? '<div class="normalText" style="color:#2C9245; "></div>' : '<a href='.url('/').'/plans'.'><button data-user_id="'.Auth::user()->id.'" data-plan="'.$value->id.'" data-id="'.$rows->id.'" data-title="'.$value->plan_name.'" class="btn btn-sm blueButton fullSize borderZero noMargin" type="submit">GET STARTED</button>');
        } else {
               $plan .='<button class="btn btn-sm blueButton fullSize borderZero noMargin btn-login" data-toggle="modal" data-target="#modal-login" type="submit">GET STARTED</button>';
        }

        $plan .='</form>
                     </div>
                         </div>
                              </div>
                              <div class="panel-footer bgWhite rightMargin borderZero borderBottom hide"><a href="#'.$value->plan_name.'"><span class="normalText redText">View Details<span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span></span></a></div>
                            </div>
                          </div> ';
}
foreach ($get_plan as  $value) {
 $total_price =0;
 $get_plan_rate2 = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->orderBy('id', 'desc')->first();
 $get_plan_price = UserPlanPrices::where('usertype_id', '=', $usertype_id)->where('user_plan_id', '=', $value->id)->where('status', '=', 1)->orderBy('id', 'asc')->first();
if(Auth::check()){
        if ($diff <= $duration || $diff == 0) {
           $inti_discount  = 100 - $get_plan_rate2->discount;
           $total_discount = '.'.$inti_discount;
           $price_with_discount = $get_plan_price->price * $total_discount;
           $total_price_no_discount = $get_plan_price->price / $get_plan_price->term;
           $total_price_with_discount = $price_with_discount / $get_plan_price->term;
           $test = 'test1';
         } else {
          $total_price_no_discount = $get_plan_price->price;
          $total_price_with_discount = $get_plan_price->price;
         }

} else{ 
  $total_price = $get_plan_price->price;

}

$plan .='<div class="col-md-3 blockTop">
                        <div class="row panel panel-default borderZero nobottomMargin" style="'.($get_current_plan->id == $value->id ? ' border:2px solid #ff6d6d;':'border:none').'">
                          <div class="panel-heading planTitleBar borderbottomPlan " style="'.($get_current_plan->id == $value->id ? 'border-bottom:none !important; background-color: #ff6d6d;margin-left:0px;margin-right:0px;':'background-color: #ffffff;margin-left:5px;margin-right:5px;').'"><span class="panelTitle '.($get_current_plan->id == $value->id ? ' whiteText':'blueText').'">'.strtoupper($value->plan_name).  '</span></div>
                          <div class="panel-body" style="'.($get_current_plan->id == $value->id ? 'padding-bottom: 30px;background-color: #ffffff':'background-color: #ffffff').'">
                            <div class="bottomPaddingC text-center borderbottomLight" style="'.($get_current_plan->id == $value->id ? 'padding-bottom: 63px !important; border-bottom:1px solid #b3b3b3 !important; ':'border:none').'">
                            '.($user_plan_types == $value->id ? '<div class="normalText" style="color:#2C9245; "></div>' : '<div class="normalText lightgrayText">'.$get_plan_rate2->discount.'% OFF THE REGULAR $'.round($total_price_no_discount, 3).' MO.</div> ').'                       
                                <div class="'.(count($get_plan) == 4 ? 'largeText':'xlargeText').' xxlargeText '.($get_current_plan->id == $value->id ? 'redText':'blueText').'"><strong>'.($value->id == 1 || $value->id == 5? 'FREE' : '$'.round($total_price_with_discount, 3).'/mo.').'</strong></div>                          
                            </div>
                            <div class="blockTop lineHeightC">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Ads <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_listing_available.'</span>':'No. of Ads <span class="pull-right blueText bold">'.$value->ads.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Auction <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_listing_available.'</span>':'No. of Auctions <span class="pull-right blueText bold">'.$value->auction.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Image per Ad <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_upload_photos_available.'</span>':' No. of Image per Ad <span class="pull-right blueText bold">'.$value->img_per_ad .'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Video per Ad <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$get_embed_videos_available.'</span>':' No. of Videos per Ad <span class="pull-right blueText bold">'.$value->video_total .'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Free Points <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->point.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Bids <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->bid.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Free SMS Notification <span class="pull-right '.($get_current_plan->id == $value->id ? 'redText':'blueText').' bold">'.$value->point_exchange.' </span></span>
                            </div>
                            <div class="blockTop text-center">';
        if (Auth::check()) {
     $plan .='<form action="plan/payment" id="upgrade" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="'.$token.'" />
              <input type="hidden" name="plan_id" value="'.$value->id.'" />';
  }  
    if (Auth::check()) {
            $plan .=($user_plan_types  == $value->id ? '<div class="normalText" style="color:#2C9245; "></div>' : '<a href='.url('/').'/plans'.'><button data-user_id="'.Auth::user()->id.'" data-plan="'.$value->id.'" data-id="'.$rows->id.'" data-title="'.$value->plan_name.'" class="btn btn-sm blueButton fullSize borderZero noMargin" type="submit">GET STARTED</button>');
    } else {
           $plan .='<button class="btn btn-sm blueButton fullSize borderZero noMargin btn-login" data-toggle="modal" data-target="#modal-login" type="submit">GET STARTED</button>';
    }

    $plan .='</form>
                 </div>
                     </div>
                          </div>
                          <div class="panel-footer bgWhite rightMargin borderZero borderBottom hide"><a href="#'.$value->plan_name.'"><span class="normalText redText">View Details<span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span></span></a></div>
                        </div>
                      </div> ';

 
 
}

      } else {
        $user_id = null;
        $usertype_id = null;
        $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->get();       
        $get_plan_rate = UserPlanRates::where('usertype_id', '=', 1)->where('rate_status', '=', 1)->first();
        $duration = null;
        $user_plan_types = '0';
        $plan = null;
        $total_ad_posted = null;
        $total_auction_posted = null;
        $total_uploaded_photos = null;
      }

  $row  = Advertisement::orderBy('id', 'desc')->first();

  $vendoradsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->select('advertisements.title', 'advertisements.status', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'users.alias','countries.countryName')                          
                                         ->where(function($query)use($id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $id)    
                                                     ->where('advertisements.status', '!=', 2);                                    
                                          })->orderBy('advertisements_photo.created_at', 'desc')->take(6)->get(); 

   $featuredads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name','users.alias', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $id)                                                                           
                                                     ->where('advertisements.feature', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();                                        



  $vendorbidsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name','users.alias', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                        ->selectRaw('avg(rate) as average_rate')
                                        ->selectRaw('count(rate) as comments_total')
                                        ->where(function($query) use($id){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1')
                                                     ->where('advertisements.ads_type_id', '=', '2')
                                                     ->where('advertisements.user_id', '=', $id);                                       
                                          })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

    $vendorbids = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                      ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $id);                                       
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements.created_at', 'desc')->take(3)->get();   

   $featured_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.alias','countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')
                                       ->take(4)->get(); 
    
   $latestadsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.alias', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->selectRaw('count(rate) as comments_total')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $id)
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
                                      
    $lastestads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.category_id', 'advertisements.sub_category_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.alias', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $id) 
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements_photo.created_at', 'desc')->take(3)->get(); 
                                  
    $watchlistads = AdvertisementWatchlists::join('advertisements','advertisements.id', '=', 'advertisements_watchlists.ads_id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftJoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->select('advertisements_watchlists.id as watchlist_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.alias', 'countries.countryName', 'country_cities.name as city', 'advertisements_watchlists.id as watchlist_id','advertisements.status')                          
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements_watchlists.user_id', '=', $id) 
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements_watchlists.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();


     $related = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->select('advertisements.category_id', 'advertisements.sub_category_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        }) ->groupBy('advertisements.id')
                                           ->orderBy('advertisements_photo.created_at', 'desc')->first(); 

     $category_id = $related->category_id;

     $related_ads_mobile  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.alias','countries.countryName',  'country_cities.name as city')                          
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                            
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(1)->take(4)->get();

      $related_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                         
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

      $related_adset2  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                         
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 


      $user_banner  = BannerSubscriptions::join('users', 'banner_subscriptions.user_id', '=', 'users.id') 
                                       ->join('banner_placement_plans','banner_placement_plans.id', '=', 'banner_subscriptions.placement_plan')
                                       ->join('languages','languages.id', '=', 'banner_subscriptions.language_id')
                                       ->select('users.*', 'languages.*', 'banner_placement_plans.*', 'banner_subscriptions.*')                          
                                       ->where(function($query) use ($user_id) {
                                             $query->where('banner_subscriptions.status', '=', '1')
                                                   ->where('banner_subscriptions.user_id', '=', $user_id);                                                                                                               
                                        })->orderBy('banner_subscriptions.created_at', 'desc')->get(); 

      $favorite_profiles  = UserFavoriteProfile::join('users', 'user_favorite_profiles.profile_id', '=', 'users.id')
                                        ->leftJoin('countries','countries.id', '=', 'users.country')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                        ->select('user_favorite_profiles.id as watchlist_id', 'users.name','users.alias','users.photo', 'countries.countryName',  'country_cities.name as city')                          
                                        ->where(function($query) use ($user_id) {
                                               $query->where('user_favorite_profiles.status', '=', '1')
                                                     ->where('user_favorite_profiles.user_id', '=', $user_id);                                                                                                               
                                         })->orderBy('user_favorite_profiles.created_at', 'desc')->groupBy('user_favorite_profiles.profile_id')->get();
      
      $auction_biddeds  = UserBidList::join('advertisements', 'user_bid_list.ads_id', '=', 'advertisements.id')
                                         ->leftJoin('users', 'user_bid_list.user_id', '=', 'users.id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->leftJoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                         ->select('user_bid_list.id as watchlist_id','users.alias','advertisements.price', 'advertisements_photo.photo', 'advertisements.id','advertisements.title', 'countries.countryName',  'country_cities.name as city', 
                                           'users.name','advertisements.status')                          
                                        ->where(function($query) use ($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('user_bid_list.status', '=', '1')
                                                     ->where('advertisements.status', '=', '1')
                                                     ->where('user_bid_list.user_id', '=', $user_id);                                                                                                               
                                         })->orderBy('user_bid_list.created_at', 'desc')->get();

    if (Request::ajax()) {
        $result['vendoradsmobile']      = $vendoradsmobile->toArray();
        $result['featuredads']          = $featuredads->toArray();
        $result['vendorbidsmobile']     = $vendorbidsmobile->toArray();
        $result['vendorbids']           = $vendorbids->toArray();
        $result['featured_ads']         = $featured_ads->toArray();
        $result['latestadsmobile']      = $latestadsmobile->toArray();
        $result['lastestads']           = $lastestads->toArray();
        $result['watchlistads']         = $watchlistads->toArray();
        $result['rows']                 = $rows->toArray();
        $result['related_ads_mobile']   = $related_ads_mobile->toArray();
        $result['related_ads']          = $related_ads->toArray();
        $result['related_adset2']       = $related_adset2->toArray();
        $result['total_ad_posted']      = $total_ad_posted->toArray();
        $result['total_auction_posted'] = $total_auction_posted->toArray();
        $result['user_country']         = $user_country;
        $result['user_city']            = $user_city;
        $result['favorite_profiles']    = $favorite_profiles;
        $result['auction_biddeds']       = $auction_biddeds;
        return Response::json($result);
    } else {

        $result['vendoradsmobile']     = $vendoradsmobile;
        $result['featuredads']         = $featuredads;
        $result['vendorbids']          = $vendorbids;
        $result['featured_ads']        = $featured_ads;
        $result['latestadsmobile']     = $latestadsmobile;
        $result['vendorbidsmobile']    = $vendorbidsmobile;
        $result['lastestads']          = $lastestads;
        $result['watchlistads']        = $watchlistads;
        $result['rows']                = $rows;
        $result['branches']            = $branch;
        $result['branches_all']        = $branches_raw;
        $result['plan']                = $plan;
        $result['comments']            = $comments;
        $result['form']                = $form;
        $result['messages_header']     = $messages_header;
        $result['get_average_rate']    = $get_average_rate;
        $result['related_ads_mobile']  = $related_ads_mobile;
        $result['related_ads']         = $related_ads;
        $result['related_adset2']      = $related_adset2;
        $result['review_summary']      = $review_summary;
        $result['total_ad_posted']       = $total_ad_posted;
        $result['total_auction_posted']  = $total_auction_posted;
        $result['total_uploaded_photos'] = $total_uploaded_photos;
        $result['user_banner']         = $user_banner;
        $result['user_city']           = $user_city;
        $result['user_country']        = $user_country;
        $result['favorite_profiles']   = $favorite_profiles;
        $result['auction_biddeds']     = $auction_biddeds;
        return $result;
     }
    

    }
    
    public function fetchInfo() {

      if (Auth::check()) {
        $id = Auth::user()->id;
      } else {
         return Redirect::to('/');     
        
      }
      $result = $this->doFetchInfo($id);
      // $this->data['top_members'] = User::leftjoin('advertisements','advertisements.user_id','=','users.id')
      //                                   ->select('users.*','advertisements.id','advertisements.user_id')
      //                                   ->where('advertisements.status',1)
      //                                   ->groupBy('advertisements.user_id')
      //                                   ->orderBy('advertisements.created_at','desc')
      //                                   ->get();


 
      $this->data['user_id']           = $id;
      $this->data['rows']              = $result['rows'];
      $this->data['watchlistads']      = $result['watchlistads'];
      $this->data['form']              = $result['form'];
      if(Auth::check()){
         $this->data['review_summary'] = $result['review_summary'];
       }else {
         $this->data['review_summary'] = "";
       } 
      $this->data['vendorbidsmobile']  = $result['vendorbidsmobile'];
      $this->data['vendoradsmobile']   = $result['vendoradsmobile'];
      $this->data['featuredads']       = $result['featuredads'];
      $this->data['branches']          = $result['branches'];
      $this->data['messages_header']   = $result['messages_header'];
      $this->data['comments']          = $result['comments'];
      $this->data['plan']              = $result['plan'];
      $this->data['user_banner']       = $result['user_banner'];
      $this->data['user_country']      = $result['user_country'];
      $this->data['user_city']         = $result['user_city'];
      $this->data['favorite_profiles'] = $result['favorite_profiles'];
      $this->data['auction_biddeds']    = $result['auction_biddeds'];
      $this->data['total_ad_posted']   = $result['total_ad_posted'];
      $this->data['total_auction_posted']   = $result['total_auction_posted'];
      $this->data['total_uploaded_photos'] = $result['total_uploaded_photos'];
      $this->data['buy_and_sell']      = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable']          = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories']        = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get(); 
      $this->data['category']          = Category::with('subcategoryinfo')->get();
      $this->data['countries']         = Country::where('status',1)->get();
      $this->data['bid_points'] = BidPoints::all();
      $get_city = User::where('id',$id)->first();
      $this->data['cities']            = City::where('country_id',$get_city->country)->where('status',1)->get();

      $this->data['languages']         = Languages::all();
      $this->data['usertypes']         = Usertypes::all();
      $this->data['banner_placement']  = BannerPlacement::all();
      $this->data['no_of_reviews'] = AdRating::where('user_id',$id)->count();
      $this->data['no_of_comments_for_ads'] = UserAdComments::with('getUserAdCommentReplies')->where('user_ad_comments.vendor_id',$id)->count();
      $this->data['no_of_comments_for_user'] = UserComments::with('getUserCommentReplies')->where('user_comments.vendor_id',$id)->count();
      $this->data['no_of_page_and_ad_visits'] =UserAdViews::where('user_id',$id)->count();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      // $this->data['notifications'] = UserNotifications::where(function($query) use ($user_id){
      //                                               $query->where('id','=',$user_id)
      //                                               ->select('user_notifications.*','users.photo');
      //                                               })->where(function($status){
      //                                               $status->where('status','=',1);                                      
      //                                   })->orderBy('user_notifications.created_at', 'desc')->get();
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();   
      $this->data['notificationsList'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id') 
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')  
                                       ->select('user_notifications.*', 'user_notifications_header.*','user_notifications.created_at','users.photo') 
                                       ->where(function($query) use ($id){
                                             $query->where('user_notifications_header.reciever_id', '=', $id);                                     
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      $this->data['contact_lists'] = UserImportContacts::where('user_id','=',$id)->where('status','=',1)->get();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                                               

      return view('user.dashboard.index', $this->data);
    }

    public function fetchInfoPublic($username) {
        $get_id = User::where('alias',$username)->first();
        if (!(is_null($get_id))) {
        $id = $get_id->id;
        } else {
         $id = null; 
        }
        $result = $this->doFetchInfo($id);
        
        if(Auth::check()){
          $user_id = Auth::user()->id;
        }else{
          $user_id=null;
        }
        $this->data['user_id'] = $id;
        $this->data['vendorbids']         = $result['vendorbids'];
        $this->data['featured_ads']       = $result['featured_ads'];
        $this->data['latestadsmobile']    = $result['latestadsmobile'];
        $this->data['lastestads']         = $result['lastestads'];
        $this->data['rows']               = $result['rows'];
        $this->data['comments']           = $result['comments'];
        $this->data['form']               = $result['form'];
        $this->data['get_average_rate']   = $result['get_average_rate'];
        $this->data['related_ads_mobile'] = $result['related_ads_mobile'];
        $this->data['vendorbidsmobile']   = $result['vendorbidsmobile'];
        $this->data['related_ads']        = $result['related_ads'];
        $this->data['related_adset2']     = $result['related_adset2'];
        $this->data['messages_header']    = $result['messages_header'];
        $this->data['favorite_profiles']  = $result['favorite_profiles'];
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
        $this->data['banner_placement_bottom'] = BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);                                 
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies); 
        $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
        $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
        $this->data['branches_public'] = $result['branches_all'];
        $this->data['buy_and_sell']    = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable']        = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories']      = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get(); 
        $this->data['countries']       = Country::all();
        $this->data['rating']          = UserRating::where('user_id','=',$user_id)->where('vendor_id','=',$id)->first();
        $this->data['usertypes']       = Usertypes::all();
        if(Auth::check()){
          $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                         
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
       if ($user_id) {
        $this->data['get_favorite_profiles']  = UserFavoriteProfile::join('users', 'user_favorite_profiles.profile_id', '=', 'users.id')
                                      ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                      ->select('user_favorite_profiles.id', 'users.name','users.photo', 'countries.countryName',  'country_cities.name as city')                          
                                      ->where(function($query) use ($user_id, $id) {
                                             $query->where('user_favorite_profiles.status', '=', '1')
                                                    ->where('user_favorite_profiles.profile_id','=',$id)
                                                   ->where('user_favorite_profiles.user_id', '=', $user_id);                                                                                                               
                                       })->orderBy('user_favorite_profiles.created_at', 'desc')->first();

        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                  
     
       }
        return view('user.vendor.list', $this->data);
 
       
    
    }

     public function getUserNotificationCount(){
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

     $result['message'] = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();


    $result['bidding']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                               
      return Response::json($result);                                              
     }


     public function registerSave() {
        $get_referral_code = Session::get('referral_code');

        $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'              => $new ? 'image|max:3000' : 'image|max:3000',
            'password'           => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
            'name'               => 'required|min:2|unique:users,name' . (!$new ? ',' . $row->id : ''),
            'email'              => 'email|required|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'username'           => 'required|min:2|unique:users,username' . (!$new ? ',' . $row->id : ''),
            'mobile'             => 'numeric|unique:users,mobile'. (!$new ? ',' . $row->id : ''),
            'telephone'          => 'numeric',
            'date_of_birth'      => 'date',
            'company_start_date' => 'date',
        ];

        // field name overrides
        $names = [
            'email'           => 'email address',
            'username'        => 'username',
            'telephone'       => 'telephone',
            'country'         => 'country',
            'city'            => 'city',
            'address'         => 'address',
            'office_hours'    => 'office hours',
            'website'         => 'website',
            'gender'          => 'gender',
            'date_of_birth'   => 'date of birth',
        ];

        $branch_array            = array();
        $company_tel_no          = Input::get('company_tel_no');
        $company_mobile          = Input::get('company_mobile');
        $company_country         = Input::get('company_country');
        $company_address         = Input::get('company_address');
        $company_city            = Input::get('company_city');
        $company_email           = Input::get('company_email');
        $company_office_hours    = Input::get('company_office_hours');
        $company_branch_id       = Input::get('company_branch_id');
        // $company_website         = Input::get('company_website');
        $company_branch_status   = Input::get('company_branch_status');
        // validate child rows
        if($company_tel_no) {
            foreach($company_tel_no as $key => $val) {
                if($company_tel_no[$key]) {
                    $subrules = array(
                        "company_tel_no.$key"         => 'required|numeric',
                        "company_mobile.$key"         => 'required|numeric',
                        "company_country.$key"        => 'required',
                        "company_city.$key"           => 'required',
                        "company_email.$key"          => 'required|email',
                        "company_address.$key"        => 'required',
                        "company_office_hours.$key"   => 'required',
                       
                    );
                    $subfn = array(
                        "company_tel_no.$key"       => 'company telephone no',
                        "company_mobile.$key"       => 'company mobile no',
                        "company_country.$key"      => 'company country',
                        "company_city.$key"         => 'company city',
                        "company_email.$key"        => 'company email address',
                        "company_address.$key"      => 'company address',
                        "company_office_hours.$key" => 'company office hours',
                        
                    );
                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $branch_array[] = $company_tel_no[$key];
                } else {
                    unset($company_branch_id[$key]);
                    unset($company_tel_no[$key]);
                }
            }
        }

        $messages = [
          'photo.required' => 'error-photo|* Fill the required fields',
          'username.required' => 'error-username|* Fill the required fields',
          'email.required' => 'error-email|* Fill the required fields',
          'password.required' => 'error-password|* Fill the required fields',
          'facebook_account.required' => 'error-facebook_account|* Fill the required fields',
          // 'password_confirmation.required' => 'error-password_confirmation|* Fill the required fields',
          // 'usertype.required' => 'error-usertype|* Fill the required fields',
          'name.required' => 'error-name|* Fill the required fields',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules, $messages);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $confirmation_code = str_random(30);
        // create model if new
        if($new) {
            $row = new User;
            $str           = Input::get('email');
            $referral_code = str_random(35);
            $availab_ref_code = User::where('referral_code','=',$referral_code)->first();
            if (is_null($availab_ref_code)) {
              $row->referral_code = $referral_code; 
            } else {
              $row->referral_code = str_random(35);
            }
            $row->name      = strip_tags(array_get($input, 'name'));
            $row->email     = array_get($input, 'email');
            $row->usertype_id     = array_get($input, 'usertype');
            $row->alias =  array_get($input, 'username');
            if (array_get($input, 'usertype') == '2') {
              $row->user_plan_types_id = '5';
            } else {
              $row->user_plan_types_id = '1';
            }
            // $row->mobile    = array_get($input, 'mobile');
            $row->username  = array_get($input, 'username');

            // if(array_get($input, 'date_of_birth')){
            //    $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
            //  }else{
            //    $row->date_of_birth = null;
            //  }
            // if(array_get($input, 'company_start_date')){
            //    $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
            //  }else{
            //    $row->company_start_date = null;
            //  }
            // if(array_get($input, 'telephone')){
            //    $row->telephone  = array_get($input, 'telephone');
            //  }else{
            //    $row->telephone = null;
            //  }
            //  if(array_get($input, 'country')){
            //    $row->country  = array_get($input, 'country');
            //  }
            //  if(array_get($input, 'city')){
            //    $row->city  = array_get($input, 'city');
            //  }
            //  if(array_get($input, 'address')){
            //    $row->address  = array_get($input, 'address');
            //  }else{
            //    $row->address = null;
            //  }
            //  if(array_get($input, 'office_hours')){
            //    $row->office_hours  = array_get($input, 'office_hours');
            //  }else{
            //    $row->office_hours = null;
            //  }
            //  if(array_get($input, 'website')){
            //    $row->website  = array_get($input, 'website');
            //  }else{
            //    $row->website = null;
            //  }
            //  if(array_get($input, 'gender')){
            //      $row->gender  = array_get($input, 'gender');
            //   }else{
            //      $row->gender = null;
            //   }
            //   if(array_get($input, 'facebook_account')){
            //      $row->facebook_account  = array_get($input, 'facebook_account');
            //   }else{
            //      $row->facebook_account = null;
            //   }
            //    if(array_get($input, 'twitter_account')){
            //      $row->twitter_account  = array_get($input, 'twitter_account');
            //   }else{
            //      $row->twitter_account    = null;
            //   }
            //   if(array_get($input, 'instagram_account')){
            //      $row->instagram_account  = array_get($input, 'instagram_account');
            //   }else{
            //      $row->instagram_account = null;
            //   }
            //   if(array_get($input, 'youtube_account')){
            //      $row->youtube_account  = array_get($input, 'youtube_account');
            //   }else{
            //      $row->youtube_account   = null;
            //   }
            $row->confirmation_code = $confirmation_code;
            $row->status = 2;
        } else {

            $row->name        = strip_tags(array_get($input, 'name'));
            $row->email       = array_get($input, 'email');
            $row->alias       = array_get($input, 'alias');
            $row->usertype_id = array_get($input, 'usertype');
            $row->mobile      = array_get($input, 'mobile');
            $row->username    = array_get($input, 'username');
            $row->country  = array_get($input, 'country');
            $row->city  = array_get($input, 'city');
            $row->telephone  = array_get($input, 'telephone_no');
            $row->gender            = array_get($input, 'gender');

            // $row->alias    = array_get($input, 'username');
            if(array_get($input, 'date_of_birth')){
               $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
             }else{
               $row->date_of_birth = "null";
             }
            if(array_get($input, 'company_start_date')){
               $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
             }
             if(array_get($input, 'description')){
               $row->description  = array_get($input, 'description');
             }else{
               $row->description         = null;
             }
             if(array_get($input, 'address')){
               $row->address  = array_get($input, 'address');
             }else{
               $row->address             = null;
             }
             if(array_get($input, 'office_hours')){
               $row->office_hours  = array_get($input, 'office_hours');
             }else{
               $row->office_hours        = null;
             }
             if(array_get($input, 'website')){
               $row->website  = array_get($input, 'website');
             }else{
               $row->website             = null;
             }
             if(array_get($input, 'facebook_account')){
                 $row->facebook_account  = array_get($input, 'facebook_account');
              }else{
                 $row->facebook_account  = null;
              }
               if(array_get($input, 'twitter_account')){
                 $row->twitter_account   = array_get($input, 'twitter_account');
              }else{
                 $row->twitter_account   = null;
              }
               if(array_get($input, 'instagram_account')){
                 $row->instagram_account = array_get($input, 'instagram_account');
              }else{
                 $row->instagram_account = null;
              }
              if(array_get($input, 'youtube_account')){
                 $row->youtube_account   = array_get($input, 'youtube_account');
              }else{
                 $row->youtube_account   = null;
              }
              if(array_get($input, 'linkedin_account')){
                 $row->linkedin_account  = array_get($input, 'linkedin_account');
              }else{
                 $row->linkedin_account  = null;
              }
              $row->save();
        }   

        // set type only if this isn't me
        if($new || ($new = false && $row->id = Auth::user()->id)) {
          $log_usertype_id = $row->usertype_id;
          $row->usertype_id = array_get($input, 'usertype');
        }
        // do not change password if old user and field is empty
        if(array_get($input, 'password')) {
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        if($branch_array) {
          foreach($branch_array as $key => $child) {
              if($company_branch_id[$key]) {
                  // this is an existing visa
                  $cinfo = CompanyBranch::find($company_branch_id[$key]);
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = $company_branch_status[$key];
            
              } else {
                  // this is a new visa
                  $cinfo = new CompanyBranch;
                  $cinfo->user_id = $row->id;
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = "1";
                 
              }
                  $cinfo->save();
            

              // add to visa ID array
              if(!$company_branch_id[$key]) {
                  $company_branch_id[] = $cinfo->id;
              }
          }
      }

        if (Input::file('photo')) {
            // Save the photo
            Image::make(Input::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        }else{
            if($new){
              $row->photo ='default_image.png';
              $row->save();
            }
        }



        $validate = User::find($row->id);

         //tagging
              $contact_name = $row->name ; 
              $user_name = $row->username;
              $user_email = $row->email;
              if ($row->telephone) {
                  $user_phone =  $row->telephone;
              } else {
                  $user_phone =  $row->mobile;
              }  
           $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="'.url('/').'/register/verify/'.$confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Verify your email address
              </a>
          </td>';

    //save referral code
   if ($get_referral_code) {
     $referral_code = new UserRegisterReferral;
     $referral_code->user_id = $row->id;
     $referral_code->user_referral_code = $get_referral_code;
     $referral_code->save();
     //add reward points
     $get_user = User::where('referral_code','=',$get_referral_code)->first();

     $get_user_plan = UserPlanTypes::where('id','=',$get_user->user_plan_types_id)->first();
     $add_reward = $get_user->points + $get_user_plan->reward_point;
     $get_user->points = $add_reward;
     $get_user->save(); 
    } 

    //register user email validation sent
        // if($new){
        //      $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
        //     if ($newsletter) {
        //               Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'confirmation_code' => $confirmation_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $validate) {
        //                    $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

        //                });
        //         }
        // }
      if($new){
          return Response::json(['body' => 'Account Created<br>Please check your email for verification']);
      }else{
          return Response::json(['body' => ['Account Updated']]);
      }
    }

    public function register() {

      $this->data['title'] = "Register";
      $this->data['adslatestbids'] = Advertisement::latest()->take(4)->get(); 
      $this->data['adslatestbids2'] = Advertisement::latest()->skip(4)->take(4)->get();             
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['ads2'] = Advertisement::where(function($query) {
                            $query->where('ads_type_id', '=', '1');    
                        })->take(8)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                  

      return view('user.register', $this->data);
       
    }
    public function validateAccount($confirmation_code){

      $row = User::where('confirmation_code','=',$confirmation_code)->first();


      if(!is_null($row)){
        $row->confirmation_code = null;
        $row->status = 1;
        $row->save();

           //tagging
              $contact_name = $row->name ; 
              $user_name = $row->username;
              $user_email = $row->email;
              if ($row->telephone) {
                  $user_phone =  $row->telephone;
              } else {
                  $user_phone =  $row->mobile;
              }  
    //email welcome user
        if(!is_null($row)){
             $newsletter = Newsletters::where('id', '=', '11')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                      Mail::send('email.welcome_email', ['contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                           $message->to($row->email)->subject(''.$newsletter->subject.'');
                       });
                }
        }

       $counter = new NewslettersUserCounters;
       $counter->user_id = $row->id;
       $counter->newsletter_id = '14';
       $counter->save();

       $counter = new NewslettersUserCounters;
       $counter->user_id = $row->id;
       $counter->newsletter_id = '15';
       $counter->save();

        return redirect('/')->with('status', 'Account Validated');
      }else{
        return redirect('/')->with('error', 'Account is already validated');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function viewUserAds(){

      $this->data['title'] = "My Ads"; 
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    
      return view('user.ads.list', $this->data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function changePhoto(){
      $input = Input::all();

      $row = User::find(array_get($input, 'id'));

      if(!is_null($row)){

          $rules = [
            'photo' => 'required|image|max:3000',
        ];

        // field name overrides
        $names = [
            'photo' => 'photo',
        ];

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        Image::make(Input::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
        $row->photo = $row->id . '.jpg';
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
      
    }
       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    public function removePhoto(){

      $row = User::find(Request::input('id'));
      if(!is_null($row)){
        $row->photo = "default_image.png";
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

    public function purchaseBid(){

    $get_bid_points = User::find(Auth::user()->id);
    $get_add_points = BidPoints::find(Request::input('id'));
    $total_bid = 0;
    $row = User::find(Auth::user()->id);
      if(!is_null($row)){
        $total_bid = floatval($get_bid_points->bid) + floatval($get_add_points->points);
        $row->bid  = floatval($total_bid);
        $row->save();
        return Response::json(['body' => 'Bid points has been successfully purchased']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    // $get_user = User::where('referral_code','=',$get_referral_code)->first();

    //  $get_user_plan = UserPlanTypes::where('id','=',$get_user->user_plan_types_id)->first();
    //  $add_reward = $get_user->points + $get_user_plan->reward_point;
    //  $get_user->points = $add_reward;
    //  $get_user->save(); 
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function rateUser(){
    $row = UserRating::where('vendor_id','=',Request::input('vendor_id'))->where('user_id','=',Auth::user()->id)->first();
      if(is_null($row)){
        $row = new UserRating;
        $row->rate      = Request::input('rate');
        $row->vendor_id = Request::input('vendor_id');
        $row->user_id   = Auth::user()->id;
        $row->save();
        return Response::json(['body' => 'Rate has been saved']);      
      }else{
        return Response::json(['error' => ['error']]); 
      }
    }
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function saveComment(){
      $input = Input::all();
      $row = new UserComments;
      $row->user_id     = array_get($input, 'user_id');
      $row->vendor_id     = array_get($input, 'vendor_id');
      $row->comment    = array_get($input, 'comment');
      $row->save();
      return Response::json(['body' => 'posted']);      
    }
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function saveCommentReply(){
        $input = Input::all();
        $row = new UserCommentReplies;
        $row->comment_id = array_get($input, 'comment_id');
        $row->content = array_get($input, 'reply_content');
        $row->user_id = array_get($input, 'user_id');
        $row->save();
        return Response::json(['body' => 'Replied']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function getAds(){
        $this->data['title'] = "Dashboard-Settings";
        $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                  
        return View::make('user.vendor.list', $this->data);
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  }
  public function deleteBranch(){

        $row = CompanyBranch::find(Request::input('id'));
        if(!is_null($row)){
        $row->status="2";
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
      public function message(){

        $input = Input::all();
       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories']   = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
    $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['countries']    = Country::all();
    $this->data['usertypes']    = Usertypes::all();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                  
    
         return View::make('user.dashboard.messages', $this->data);
  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
  public function getMessages(){
    $get_message_title =  UserMessagesHeader::where('id','=',Request::input('id'))->first(); 
    $get_messages      = UserMessages::where('message_id','=',Request::input('id'))->get();
 
      $message_content ='';
      $ctr=1;
    if(count($get_messages)>0){
      foreach ($get_messages as $key => $field) {
        $get_user_info = User::where('id','=',$field->sender_id)->first();
        $message_body = $field->message;
        $header_name = ucwords($get_user_info->name, " ");
        if($get_user_info->name !== 'admin yakolak'){
          $message_body = htmlentities($field->message);
          $header_name = $get_user_info->name;
        }
        $message_content .= '<tr><td class="'.($ctr % 2 ==0 ? 'bgWhite':'bglightGray bottomBorder').'"><div class="col-sm-12">'.
                            '<div class="commentBlock topPaddingB">'.
                              '<div class="commenterImage">'.
                                '<img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                              '</div>'.
                              '<div class="commentText">'.
                              '<div class="panelTitleB bottomPadding">'.$header_name.'<span class="smallText lightgrayText pull-right normalWeight">'.date("F j, Y g:i A", strtotime($field->created_at)).'</div>'.
                              '<p class="normalText grayText">'.$message_body.'</p>'.
                            '</div>'.
                            '</div>'.
                            '</div></td>'.
                          '</tr>';
      $ctr++;
      }
   }else{
      $message_content ='';
   }
    $result['message_title']   = $get_message_title->title;
    $result['message_content'] = $message_content;
    return Response::json($result);
  }

  public function saveUserMessage(){
    
        $input = Input::all();
       
        // create model if new
        if(array_get($input,'message_id')){
            $header = UserMessagesHeader::where('id','=',array_get($input,'message_id'))->first();
            $header->read_status = 1;
            $header->read_status_sender = 1;
            $header->save();
            $row = new UserMessages;
            $row->message_id = strip_tags(array_get($input, 'message_id'));
            $row->message    = array_get($input, 'message');
            $row->sender_id  = Auth::user()->id;
            $row->save();
         
        }
      
      return Response::json(['body' => ['Message Sent']]);
  }

  public function sendMessage(){

        $input = Input::all();
       
        // create model if new
        if(array_get($input,'reciever_id')){
            $row = new UserMessagesHeader;
            $row->title       = array_get($input, 'title');
            $row->sender_id   = Auth::user()->id;
            $row->read_status = 1;
            $row->read_status_sender = 1;
            $row->reciever_id = array_get($input, 'reciever_id');
            $row->save();

            $rows = new UserMessages;
            $rows->message_id  = $row->id;
            $rows->message     = array_get($input, 'message');
            $rows->sender_id   = Auth::user()->id;
            $rows->save();

        }


            $get_user_email = User::find($row->reciever_id);
            $useremail      = $get_user_email->email;
         //tagging
            $contact_name = Auth::user()->name; 
            $user_name    = Auth::user()->username;
            $user_email   = Auth::user()->email;
            if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
            } else {
                  $user_phone = Auth::user()->mobile;
            } 
            if ($user_phone == null) {
              $user_phone = 'N/A';
            }
            $title = $row->title;
            $messages = $rows->message;
               //email vendor inquiry
             $newsletter = Newsletters::where('id', '=', '13')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.vendor_inquiry', ['contact_name' => $contact_name, 'user_name' => $user_name, 'user_email' => $user_email,'user_phone' => $user_phone, 'messages' => $messages, 'title' => $title, 'newsletter' => $newsletter], function($message) use ($newsletter, $useremail) {
                                    $message->to($useremail)->subject(''.$newsletter->subject.'');
                                });
                     }
                     
      return Response::json(['body' => ['Message Sent']]);

  }
  public function changeMessageStatus(){
    
      $row = UserMessagesHeader::find(Request::input('id'));
          if(!is_null($row)){
            if($row->read_status != 2){
              $row->read_status="2";
              $row->read_status_sender="2";
              $row->save();
            }
          }
    $user_id = Auth::user()->id;

    $updated_count = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

    $result['updated_count'] = $updated_count;                                             
     return Response::json($result);                                               
  }
    public function changeNotificationsStatus(){
      $row = UserNotificationsHeader::find(Request::input('id'));
          if(!is_null($row)){
              $row->read_status="0";
              $row->save();
          }
     
  }
  public function deleteMessage(){

      $row = UserMessagesHeader::find(Request::input('id'));
          if(!is_null($row)){
            if($row->status != 2){
              $row->status="2";
              $row->save();

               return Response::json(['body' => 'Message Deleted']);      
            }
          }
  }
  public function doListMessage(){
      $field = Request::input('field') ? :'created_at';
      $order = Request::input('order') ? :'desc'; 
      $get_user_messages = UserMessagesHeader::where(function($query){
                                                    $query->where('reciever_id','=',Auth::user()->id)
                                                          ->orWhere('sender_id',Auth::user()->id);
                                              })
                                              ->where('status',1)
                                              ->orderBy($field,$order)
                                              ->orderBy('created_at','desc')
                                              ->get();
    
      // dd($get_user_messages );
      if(count($get_user_messages)>0){
              $messages_header ='';
              foreach ($get_user_messages as $key => $field) 
                  {    
                 // $messages_header .=  '<tr><button><td data-header-id="'.$field->id.'" class="view-message"><i class="'.($field->read_status == 1 ? 'fa fa-envelope':'fa fa-envelope-o').' rightPadding"></i> <span class="normalText grayTextB">'.$field->title.'</span></td></button>
                 //                       <td class="text-right smallText"><span class="rightPadding lightgrayText normalWeight">'.date("m/d/y g:i A", strtotime($field->created_at)).'</span><span class="lightgrayText"><button class="redText borderZero removeborder noBackground" id="delete-message" data-id= "'.$field->id.'"><i class="fa fa-trash"></i></button></span></td></tr>';
                 $messages_header .=  '<tr><td data-header-id="'.$field->id.'" class="view-message" onclick="refreshCounter()"><i class="'.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'fa fa-envelope grayText':'fa fa-envelope-o grayTextB').' rightPadding"></i><span class="normalText '.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'grayText':'grayTextB').'">'.$field->title.'</span></td>
                                       <td class="text-right smallText"><span class="rightPadding '.(($field->read_status == 1 && $field->read_status_sender == 1) ? 'grayText':'grayTextB').' normalWeight">'.date("m/d/y g:i A", strtotime($field->created_at)).'</span><span class="redText"><button class="borderZero removeborder noBackground" id="delete-message" data-id= "'.$field->id.'"><i class="fa fa-trash"></i></button></span></td></tr>';
              }
      }else{
          $messages_header = '';
      }
      $result['messages_header'] = $messages_header;

      return Response::json($result);
  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
public function upgradeUserPlan(){
       $input = Input::all();
       $plan_id = Request::input('plan_id');
       $months_subscribed = Request::input('months_subscribed');
       $row = User::find(Request::input('user_id'));
        if(!is_null($row)){
        $row->user_plan_types_id = $plan_id;
        //Plan Expiration
        $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
        $date = date_create($current_time);
        $row->plan_expiration = date_add($date,date_interval_create_from_date_string($months_subscribed." months"));                   
        $row->save();

        $plan = UserPlanTypes::where('id','=', $plan_id)->first();
     //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
              if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;
                  //email user new upgrade subscription
              $newsletter = Newsletters::where('id', '=', '12')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.purchased_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
              } 
                       ;
        return Response::json(['body' => 'User Plan Upgraded']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function userPlanExpiring(){
      $get_users = User::select('users.id', 'users.user_plan_types_id', 'users.name', 'users.email', 'users.created_at', 'users.plan_expiration')
              ->where(function($query){
               $query ->where('user_plan_types_id', '!=', '1') 
                      ->where('status', '!=', '2')   
                      ->where('user_plan_types_id', '!=', '5');                                    
              })->get();

      foreach ($get_users as $value) {
      $date_trigger = '1 week after';

      $now = Carbon::now();
      $startTime = Carbon::parse($now);
      $finishTime = Carbon::parse($value->plan_expiration);
      $totalDuration = $finishTime->diffForHumans($startTime);

      if ($totalDuration == $date_trigger) {
      $plan = UserPlanTypes::where('id','=', $value->user_plan_types_id)->first();

             //tagging
              $contact_name = $value->name; 
              $user_name = $value->username;
              $user_email = $value->email;
              if ($value->phone) {
                  $user_phone = $value->telephone;
               } else {
                  $user_phone = $value->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;

                  $counter = NewslettersUserCounters::where('newsletter_id','=','14')->where('user_id','=',$value->id)->first();
                  if ($counter->counter_status == '1') {
                        $counter->counter_status = '2';
                        $counter->save();
                          //email user new upgrade subscription
                        $newsletter = Newsletters::where('id', '=', '14')->where('newsletter_status', '!=', '2')->first();
                        if ($newsletter) {
                                  Mail::send('email.expiration_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $value) {
                                      $message->to(''.$value->email.'')->subject(''.$newsletter->subject.'');
                                  });
                        } 
                  }
      }
 }         
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function userPlanExpired(){
$get_users = User::select('users.id', 'users.user_plan_types_id', 'users.name', 'users.email', 'users.created_at', 'users.plan_expiration')
              ->where(function($query){
               $query ->where('user_plan_types_id', '!=', '1') 
                      ->where('status', '!=', '2')    
                      ->where('user_plan_types_id', '!=', '5');                                    
              })->get();

      foreach ($get_users as $value) {

      $now           = Carbon::now();
      $startTime     = Carbon::parse($now);
      $finishTime    = Carbon::parse($value->plan_expiration);
      $totalDuration = $finishTime->diffForHumans($startTime);

       $diff = substr($totalDuration, -6); 

          if ($diff == 'before') {
          $plan = UserPlanTypes::where('id','=', $value->user_plan_types_id)->first();
          $test = 'test';
             //tagging
              $contact_name = $value->name; 
              $user_name    = $value->username;
              $user_email   = $value->email;
              if ($value->phone) {
                  $user_phone = $value->telephone;
               } else {
                  $user_phone = $value->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;

                  $counter = NewslettersUserCounters::where('newsletter_id','=','15')->where('user_id','=',$value->id)->first();
                  if ($counter->counter_status == '1') {
                        $counter->counter_status = '2';
                        $counter->save();
                          //email user new upgrade subscription
                        $newsletter = Newsletters::where('id', '=', '15')->where('newsletter_status', '!=', '2')->first();
                        if ($newsletter) {
                                  Mail::send('email.expiration_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $value) {
                                      $message->to(''.$value->email.'')->subject(''.$newsletter->subject.'');
                                  });
                        } 
                  }
     // $disable_account = User::find($value->id);
     // $disable_account->status = '2';
     // $disable_account->save(); 
      }
   
     
 }         
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function settingSave(){
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = User::find(array_get($input, 'id'));
            
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'password'           => 'min:6|confirmed',
            'alias'              => 'unique:users,alias'.(!$new ? ',' . $row->id : ''),


        ];

        // field name overrides
        $names = [
            'password'       => 'password',
            'alias' => 'alias',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // dd(Hash::make(array_get($input,'password')));
        // set type only if this isn't me


       if(array_get($input,'password')){
          $row->password = Hash::make(array_get($input, 'password'));
          $row->save();
        }
       if(array_get($input,'alias')){
          $row->alias = array_get($input,'alias');
          $row->save();
       }
        if(array_get($input,'country') && array_get($input,'city')  ){
          $row->country = array_get($input,'country');
          $row->city = array_get($input,'city');
          $row->save();
       }

        // do not change password if old user and field is empty
        // save model
      

    //     $validate = User::find($row->id);

    //      //tagging
    //           $contact_name = $row->name ; 
    //           $user_name = $row->username;
    //           $user_email = $row->email;
    //           if ($row->telephone) {
    //               $user_phone =  $row->telephone;
    //           } else {
    //               $user_phone =  $row->mobile;
    //           }  
    //        $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
    //           <a href="http://yakolak.xerolabs.co/register/verify/'.$confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
    //              Verify your email address
    //           </a>
    //       </td>';
    // //register user email validation sent
    //     if($new){
    //          $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
    //         if ($newsletter) {
    //                   Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'confirmation_code' => $confirmation_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $validate) {
    //                        $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

    //                    });
    //             }
    //     }
  
          return Response::json(['body' => ['Account Settings Updated']]);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    
    public function refreshVendorAdsActive(){
      $user_id = Auth::user()->id;
      $featuredads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.feature','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.feature','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '1')                                      
                                                     ->where('advertisements.feature', '=', '1')
                                                     ->where('advertisements.status', '=', '1');                                    
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $activeads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.feature','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                         
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.feature', '=', '0')                                     
                                                     // ->where('advertisements.ads_type_id', '!=', '2')
                                                     ->where('advertisements.ads_type_id', '=', '1')                                      
                                                     ->where('advertisements.status', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $disabledads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')    
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                       
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '1')                                      
                                                     ->where('advertisements.status', '=', '3');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
     $spamdads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)  
                                                     ->where('advertisements.ads_type_id', '=', '1')                                                                         
                                                     ->where('advertisements.status', '=', '4');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();

   
   $rows = '';
   $rows .= '
        <div class="panel panel-default removeBorder borderZero">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Listings</span>
              </div>
                 <div id="load-basic-listing-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 

          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">';
            $i = 4;
             for ($x = 1; $x <= $i; $x++) {
              if ($x == 1) {
                   $ads =  $featuredads;
              } elseif ($x == 2) {
                   $ads =  $activeads;
              } elseif ($x == 3) {
                   $ads =  $disabledads;  
              } elseif ($x == 4) {
                   $ads =  $spamdads;  
              }
          $rows .=' <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding" >
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div  class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="list" class="grayText" data-toggle="collapse"  href="#collapse_ads_'.$x.'">
                '.($x == 1?'<strong>Featured Ads </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($featuredads).'</span> Featured':'').
                  ($x == 2?'<strong>Active Enable Ads</strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($activeads).'</span> Active':'').
                  ($x == 3?'<strong>Disabled Ads</strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($disabledads).'</span> Disabled':'').
                  ($x == 4?'<strong>Flagged as Spam </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($spamdads).'</span> Flagged as Spam':'').
                '<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse_ads_'.$x.'" class="panel-collapse collapse '.(count($ads) == 0 ? '':'in').'">
              <div class="panel-body borderBottom row removeBorder '.(count($ads) == 0 ? 'bgGray':'').'">';
               if(count($ads) == 0){
                  $rows .= '<p class="text-center redText topPadding bottomPadding">Empty</p>';
              }else{
         foreach ($ads as $row)  { 
                  $rows .='<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="ads-list">
                            
                    <div class="panel panel-default borderZero bottomMarginB  removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="'. url('/ads/view') . '/' . $row->id.'"><div class="adImage" style="background: url('. url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding" >
                              <div class="visible-lg">'.substr($row->title, 0, 26). 
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                                <div class="visible-md">'.substr($row->title, 0, 20).
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by '.$row->name .'<span class="pull-right"><strong class="blueText">
                                   '.number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">'
                                    .($row->city != null?'
                                <i class="fa fa-map-marker rightMargin"></i>
                                '.$row->city.'':'').($row->city != null && $row->countryName != null ? ', ':'').
                                    ($row->countryName != null ? ''.$row->countryName.'': '') .
                                    ($row->city == null && $row->countryName == null ? '<i class="fa fa-map-marker rightMargin"></i> not available':'').
                             '</div>
                                <div>
                                    <span class="blueText rightMargin">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-empty"></i>
                                    </span>
                                    <span class="normalText lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                  </div>
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                        <span class="dropdown">
                        <a class="dropdown-toggle noBackground noPadding normalText grayText cursor" type="" id="menu1" data-toggle="dropdown"><i class="fa fa-angle-down"></i> Tools
                        </a>
                        <ul class="dropdown-menu borderZero" role="menu" aria-labelledby="menu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="'.url('ads/edit').'/'.$row->id.'">Edit</a></li>
                          <li role="presentation"><a role="menuitem" data-ads_id="'.$row->id.'" data-id="'.$row->id.'" data-title="'. $row->title .'" '.($x == 3 ? 'class="btn-start-auction hide" tabindex="-1" href="#">Make Auction</a>':'class="btn-disable-auction-alert hide" tabindex="-1" href="#">End Auction</a>').' </li>
                          <li role="presentation"><a role="menuitem" data-id="'.$row->id.'" data-title="'. $row->title .'" class="btn-delete-ads" tabindex="-1" href="#">Delete</a></li>
                          <li role="presentation"><a class="'.($x == 3 ? 'lightgrayText unclickable':'grayText').'"role="menuitem" tabindex="-1" href="#" id="'.($row->feature == 0 ? 'btn-feature':'btn-unfeature').'" data-feature-id="'.$row->id.'">'.($row->feature == 0 ? 'Feature':'UnFeature').'</a></li>
                          <li role="presentation"><a role="menuitem" data-ads_id="'.$row->id.'" data-id="'.$row->id.'" data-title="'. $row->title .'" '.($x == 3 ? 'class="btn-enable-ads-alert" tabindex="-1" href="#">Enable</a>':'class="btn-disable-ads-alert-list" tabindex="-1" href="#">Disable</a>').' </li>
                        </ul>
                      </span>
                      <span class="normalText redText pull-right">'.($row->feature == 1 ? 'Featured':'').'</span>
                      </div>
                    </div>
                 </div>';
                }
              }
             $rows .= '</div>
            </div>
          </div>
        </div>
        </div>
        </div>';
}
       $rows .='</div>
          <!-- Mobile Listing -->
    </div>
  </div>
';
               
      return Response::json($rows);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  

    public function refreshVendorAuctionActive(){
     $user_id = Auth::user()->id;
     $activeauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.feature','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')     
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.id as get_ads_id', 'advertisements.feature','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                      
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                                                                  
                                                     ->where('advertisements.ads_type_id', '=', '2')
                                                     ->where('advertisements.feature', '=', '0')                                     
                                                     ->where('advertisements.status', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $endedauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')  
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                           'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                         
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '2')
                                                     ->where('advertisements.status', '=', '3');
                                                 })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $featureauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.feature','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')  
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.id as get_ads_id', 'advertisements.feature','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                         
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)
                                                     ->where('advertisements.ads_type_id', '=', '2') 
                                                     ->where('advertisements.status', '=', '1')                                                                                                                
                                                     ->where('advertisements.feature', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();

                                
      $disableauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')   
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.id as get_ads_id','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                        
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '2')                                      
                                                     ->where('advertisements.status', '=', '4');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
     $spamauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         // ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         // ->leftJoin('countries','countries.id', '=', 'users.country')
                                         // ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         // ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         //  'users.name', 'advertisements.status', 'countries.countryName')
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.id as get_ads_id','advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                           
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)
                                                     ->where('advertisements.ads_type_id', '=', '2')                                                                           
                                                     ->where('advertisements.status', '=', '5');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();

   $rows = '';
   $rows .= '
        <div class="panel panel-default removeBorder borderZero">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Auction Listings</span>
              </div>
                 <div id="load-auction-listing-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 

          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">';
            $i = 5;
            for ($x = 1; $x <= $i; $x++) {
              if ($x == 1) {
                   $ads =  $featureauction;  
              } elseif ($x == 2) {
                   $ads =  $activeauction;
              } elseif ($x == 3) {
                   $ads =  $endedauction;
              } elseif ($x == 4) {
                   $ads =  $disableauction;  
              } elseif ($x == 5) {
                   $ads =  $spamauction;  
              }
            
             // for ($x = 1; $x <= $i; $x++) {
             //  if ($x == 1) {
             //       $ads =  $activeauction;
             //  } else {
             //       $ads =  $endedauction;
             //  }

          $rows .=' <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding" >
                  <div class="panel-group nobottomMargin" id="accordion">
              <div class="panel panel-default removeBorder">
                <div  class="row notopPadding panel-heading removeBorder bgWhite">
                    <a id="list" class="grayText" data-toggle="collapse"  href="#collapse'.$x.'">
                    '.($x == 1?'<strong>Featured Auctions </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($featureauction).'</span> Featured':'').
                      ($x == 2?'<strong>Active Auctions </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($activeauction).'</span> Active':'').
                      ($x == 3?'<strong>Ended Auctions </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($endedauction).'</span> Ended':'').
                      ($x == 4?'<strong>Disabled Auctions </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($disableauction).'</span> Disabled':'').
                      ($x == 5?'<strong>Flagged as Spam </strong><span class="'.(count($ads) == 0 ? 'lightgrayText':'redText').' pull-right"><span class="'.(count($ads) == 0 ? 'bgGrayC':'bgRed').' badge">'.count($spamauction).'</span> Flagged as Spam':'').
                    '<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
                <div class="row borderbottomLight"></div>
                <div id="collapse'.$x.'" class="panel-collapse collapse '.(count($ads) == 0 ? '':'in').'">
              <div class="panel-body borderBottom row removeBorder '.(count($ads) == 0 ? 'bgGray':'').'" id="auction-container">';
              if(count($ads) == 0){
                  $rows .= '<p class="text-center redText topPadding bottomPadding">Empty</p>';
              }else{
         foreach ($ads as $row)  {
                  
                  $rows .='<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="ads-list">
                    <div class="panel panel-default borderZero removeBorder bottomMarginB">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="'. url('/ads/view') . '/' . $row->id.'"><div class="adImage" style="background: url('. url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding" >
                              <div class="visible-lg">'.substr($row->title, 0, 26). 
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                                <div class="visible-md">'.substr($row->title, 0, 20).
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by '.$row->name .'<span class="pull-right"><strong class="blueText">
                                   '.number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">'
                                    .($row->city != null?'
                                <i class="fa fa-map-marker rightMargin"></i>
                                '.$row->city.'':'').($row->city != null && $row->countryName != null ? ', ':'').
                                    ($row->countryName != null ? ''.$row->countryName.'': '') .
                                    ($row->city == null && $row->countryName == null ? '<i class="fa fa-map-marker rightMargin"></i> not available':'').
                             '</div>
                                <div>
                                    <span class="blueText rightMargin">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-empty"></i>
                                    </span>
                                    <span class="normalText lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                  </div>
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                        <span class="dropdown">
                        <a class="dropdown-toggle noBackground noPadding normalText grayText cursor" type="" id="menu1" data-toggle="dropdown"><i class="fa fa-angle-down"></i> Tools
                        </a>
                        <ul class="dropdown-menu borderZero" role="menu" aria-labelledby="menu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="'.url('ads/edit').'/'.$row->id.'">Edit</a></li>
                          <li role="presentation"><a role="menuitem" data-ads_id="'.$row->id.'" data-id="'.$row->id.'" data-title="'. $row->title .'" '.($x == 3 ? 'class="btn-start-auction" tabindex="-1" href="#">Start Auction</a>':'class="btn-disable-auction-alert" tabindex="-1" href="#">End Auction</a>').' </li>
                          <li role="presentation"><a role="menuitem" data-id="'.$row->id.'" data-title="'. $row->title .'" class="btn-delete-ads" tabindex="-1" href="#">Delete</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#" id="'.($row->feature == 0 ? 'btn-feature':'btn-unfeature').'" data-feature-id="'.$row->id.'">'.($row->feature == 0 ? 'Feature':'UnFeature').'</a></li>
                        </ul>
                      </span>
                      <span class="normalText redText pull-right">'.($row->feature == 1 ? 'Featured':'').'</span>
                      </div>
                    </div>
                 </div>';
                }
              }
             $rows .= '</div>
            </div>

          </div>
        </div>
        </div>
        </div>';
}
       $rows .='</div>
          <!-- Mobile Listing -->
    </div>
  </div>
';           
      return Response::json($rows);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function formMakeAuction() {
     $input = Input::all();  
    $row = Advertisement::find(Request::input('ads_id'));

      $result['row'] = $row;                      

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function adMakeAuction(){
  $new = true;
  $input = Input::all();
  $bid_limit_amount = Input::get('bid_limit_amount');
  $bid_start_amount = Input::get('bid_start_amount');
  $minimum_allowed_bid = Input::get('minimum_allowed_bid');
  $bid_duration_start = Input::get('bid_duration_start');
  $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
  $ads_id = Input::get('ads_id');
  $type_id = Input::get('type_id');
 
  if(array_get($input, 'ads_id')) {
            $row = Advertisement::find(array_get($input, 'ads_id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
       }
     $new = false;
  }

  if($ads_id) {
     $row = Advertisement::find($ads_id);
       if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
       }
       $new = false;
   }

  if (Auth::check()) {
     $user_id = Auth::user()->id;
     $user_plan_types_id = Auth::user()->user_plan_types_id;

        if ($user_id != $row->user_id) {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);      
        }
  } else  {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);    
  } 
  if ($type_id != '2') {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);     
  }
  $get_plan = UserPlanTypes::find($user_plan_types_id);
  $get_posted_auctions = Advertisement::where('ads_type_id','=','2')->where('user_id','=', $user_id)->where('status','!=','2')->get();
  if ($row->ads_type_id != '2') {
     if (count($get_posted_auctions) >= $get_plan->auction) {
         return Response::json(['erroruser' => "You have reached the alowed auction limit"]);     
    }
  }
   
  $remove_existing_bidders = AuctionBidders::where('ads_id','=',$ads_id)->get();
  if ($remove_existing_bidders) {
     foreach ($remove_existing_bidders as $value) {
       $value->bid_limit_amount_status = 0;
       $value->bidder_status = 0;
       $value->status = 0;
       $value->save();
     }
  }
  
 

        $rules = [
             'bid_limit_amount'               => 'required|int|min:1|max:999999999',
             'bid_start_amount'               => 'required|int|min:1|max:999999999999',
             'minimum_allowed_bid'            => 'required|int|min:1|max:999999999',
             'bid_duration_start'             => 'required|int|min:1|max:999',
             'bid_duration_dropdown_id'       => 'required|int|min:1',
             'type_id'                        => 'required|int|min:1',
        ];
        // field name overrides
        $names = [  
             'bid_limit_amount'               => 'Bid Limit Amount',
             'bid_start_amount'               => 'Bid Start Amount',
             'minimum_allowed_bid'            => 'Minimum Allowed Amount',
             'bid_duration_start'             => 'Bid Duration',
             'bid_duration_dropdown_id'       => 'Bid Duration',
             'ads_id'                         => 'required',
             'type_id'                        => 'photo',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($bid_limit_amount <= $bid_start_amount) {
            return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start']); 
        }

       $row->ads_type_id = $type_id;
       $row->bid_limit_amount = $bid_limit_amount;
       $row->bid_start_amount = $bid_start_amount;
       $row->minimum_allowed_bid = $minimum_allowed_bid;
       $row->bid_duration_start = $bid_duration_start;
       $row->bid_duration_dropdown_id = $bid_duration_dropdown_id;

        //ad expiration auction
          if ($bid_duration_start) {
           $bid_duration_start = Input::get('bid_duration_start');
           $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                } else {
                  return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);     
                }
          $row->bid_duration_start         = $bid_duration_start;
          } else {
            $row->bid_duration_start       = null;
          }
        $row->ad_exp_email_trigger = '1';
        $row->auction_expiring_noti_trigger = '1';
        $row->status = '1';
        $row->save();

        if ($new == false) {
           return Response::json(['body' => "Advertisement Successfully updated"]); 
        }
}

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function saveFavoriteProfile() {
      $input = Input::all();
      $profile_id = Request::input('profile_id');
      $user_id = Auth::user()->id;
      $get_user = User::find($user_id);
      if(!is_null($get_user)){
        $row = new UserFavoriteProfile;
        $row->user_id = $user_id;
        $row->profile_id = $profile_id;
        $row->save();

        return Response::json(['body' => 'Added Successfully']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

}

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function formWatchlist() {
      $input = Input::all();  
      $watchlist_id = Request::input('watchlist_id');
      $watchlist_type = Request::input('watchlist_type');
      if ($watchlist_type == '1') {
         $row = AdvertisementWatchlists::find($watchlist_id);;
      } else if ($watchlist_type == '2') {
        $row = UserFavoriteProfile::find($watchlist_id);
      } else if ($watchlist_type == '3') {
        $row = UserBidList::find($watchlist_id);
      }
      $result['row'] = $row;                      
     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

   /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


public function removeWatchlist() {
      $input = Input::all();
      $watchlist_id = Request::input('watchlist_id');
      $watchlist_type = Request::input('watchlist_type');
      if ($watchlist_type == '1') {
         $row = AdvertisementWatchlists::find($watchlist_id);
         $row->status = '2';
         $row->save();
      } else if ($watchlist_type == '2') {
         $row = UserFavoriteProfile::find($watchlist_id);
         $row->status = '2';
         $row->save();
      } else if ($watchlist_type == '3') {
         $row = UserBidList::find($watchlist_id);
         $row->status = '2';
         $row->save();
      }
      if($row) {
        return Response::json(['body' => 'Removed Successfully']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

public function refreshWatchlist() {
   $user_id = Auth::user()->id;
       $watchlistads = AdvertisementWatchlists::join('advertisements','advertisements.id', '=', 'advertisements_watchlists.ads_id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftJoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->select('advertisements_watchlists.id as watchlist_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'advertisements_watchlists.id as watchlist_id')                          
                                       ->where(function($query) use ($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements_watchlists.user_id', '=', $user_id) 
                                                   ->where('advertisements_watchlists.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $favorite_profiles  = UserFavoriteProfile::join('users', 'user_favorite_profiles.profile_id', '=', 'users.id')
                                      ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                      ->select('user_favorite_profiles.id as watchlist_id', 'users.name','users.alias','users.photo', 'countries.countryName',  'country_cities.name as city')                          
                                      ->where(function($query) use ($user_id) {
                                             $query->where('user_favorite_profiles.status', '=', '1')
                                                   ->where('user_favorite_profiles.user_id', '=', $user_id);                                                                                                               
                                       })->orderBy('user_favorite_profiles.created_at', 'desc')->get();
 
      $auction_biddeds  = UserBidList::join('advertisements', 'user_bid_list.ads_id', '=', 'advertisements.id')
                                       ->leftJoin('users', 'user_bid_list.user_id', '=', 'users.id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftJoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                       ->select('user_bid_list.id as watchlist_id','advertisements.price', 'advertisements_photo.photo', 'advertisements.id','advertisements.title', 'countries.countryName',  'country_cities.name as city', 
                                         'users.name')                          
                                      ->where(function($query) use ($user_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('user_bid_list.status', '=', '1')
                                                   ->where('user_bid_list.user_id', '=', $user_id);                                                                                                               
                                       })->orderBy('user_bid_list.created_at', 'desc')->get(); 
  
   $rows ="";
   $rows .='<div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
        <!-- Mobile View -->
        <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
            <div class="panel-heading panelTitleBarLightB">
                <span class="panelTitle">Listings</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
                  <span class="pull-right"><a href="http://192.168.254.14/yakolak/public/category/list" class="redText">view more</a></span>
               </div>
            <div class="col-sm-12 col-xs-12 blockBottom noPadding">
             <div class="panel-body noPadding">
            </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="panel panel-default removeBorder borderZero hidden-xs hidden-sm">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Watchlist</span>
              </div><div id="load-watchlist-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 
          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 noPadding"> 
              <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                <div class="panel-body nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse7">
                <strong>Favorite Advertisements </strong><span class="leftMargin lightgrayText">| You can upgrade "Active" listings to Featured.</span> <span class="redText pull-right">'.count($watchlistads).' Favorite Ads<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse7" class="panel-collapse collapse ">
              <div class="panel-body borderBottom row removeBorder">';


         foreach ($watchlistads as $row) {            
               $rows .= '<div class="col-lg-3 col-md-3" id="ads-list" data-watchlist_id="'.$row->watchlist_id.'" data-watchlist_type="1" data-title="'.$row->title.'">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="'.url('/').'/ads/view/'.$row->id.'"><div class="adImage" style="background: url('.url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg">'.substr($row->title, 0, 35).''; 
                              if(strlen($row->title) >= 37){
                    $rows .='..'; }
                    $rows .='</div>
                          <div class="visible-md">'.substr($row->title, 0, 25).'';
                              if(strlen($row->title) >= 37){
                    $rows .='..'; } 
                    $rows .='</div></div><div class="normalText grayText bottomPadding bottomMargin">
                            by '.$row->name.'<span class="pull-right"><strong class="blueText">
                                   '. number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">';
                        if($row->city || $row->countryName) {
                    $rows .='<i class="fa fa-map-marker rightMargin"></i>
                                   '. $row->city.'';
                        if ($row->city){
                    $rows .=', '; }  
                                   if($row->countryName) { 
                    $rows .=''.$row->countryName.''; } else { 
                    $rows .='None'; 
                                   } } else {
                    $rows .='<i class="fa fa-map-marker rightMargin"></i> not available';
                                   }
                    $rows .='<span class="pull-right">';
                                       if($row->average_rate != null) {
                     $rows .='<span id="ads_latest_bids_'.$row->id.'" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>';
                                       } else {
                    $rows .='<span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>';
                                       } 
                    $rows .='</span>
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText blueText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>';
              }

             $rows .='</div>
                </div>
                <div class="row borderBottom"></div>
              </div>
            </div>
            </div>
            </div> <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                    <div class="panel-body nobottomPadding">
                      <div class="panel-group nobottomMargin" id="accordion">
              <div class="panel panel-default removeBorder">
                <div class="row notopPadding panel-heading removeBorder bgWhite">
                    <a id="watch" class="grayText" data-toggle="collapse" href="#collapse8">
                    <strong>Favorite Profile </strong><span class="redText pull-right">'.count($favorite_profiles).' Favorite Users<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
                <div class="row borderbottomLight"></div>
                <div id="collapse8" class="panel-collapse collapse in">
                  <div class="panel-body borderBottom row removeBorder">';

          foreach ($favorite_profiles as $row)  {

            $rows .='<div class="col-lg-3 col-md-3" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="2">
                        <div class="panel panel-default borderZero removeBorder">
                          <div class="panel-body noPadding">
                              <div class="">
                              <a href="'.url('/').'/ads/view/'.$row->id.'"><div class="adImage" style="background: url('.url('uploads').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                            </div></a>
                              <div class="minPadding nobottomPadding"> 
                                    <div class="normalText grayText bottomPadding bottomMargin">
                                      <i class="fa fa-user" aria-hidden="true"></i> '.$row->name.'<span class="pull-right">
                                      </span></div><div class="normalText bottomPadding">';
                                    if($row->city || $row->countryName) {
            $rows .='<i class="fa fa-map-marker rightMargin"></i>'. $row->city.'';if ($row->city){
            $rows .=', '; }  if($row->countryName) { 
            $rows .=''.$row->countryName.''; } else { 
            $rows .='None';  } } else {
                    $rows .='<i class="fa fa-map-marker rightMargin"></i> not available';
                                   }
            $rows .='</div>
                       </div>  
                        </div>
                          </div>
                          <div class="panel-footer">
                           <a href="#" class="btn-remove-watchlist"><span class="normalText blueText"> <i class="fa fa-trash"></i> Remove</span></a>
                          </div>
                        </div>
                   </div>';
            }
             $rows .='</div>
                </div><div class="row borderBottom"></div>
                    </div>
                  </div>
                  </div>
                  </div> <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                          <div class="panel-body nobottomPadding">
                            <div class="panel-group nobottomMargin" id="accordion">
                    <div class="panel panel-default removeBorder">
                      <div class="row notopPadding panel-heading removeBorder bgWhite">
                          <a id="watch" class="grayText" data-toggle="collapse" href="#collapse9">
                          <strong>Auction Bidded </strong><span class="redText pull-right">'.count($auction_biddeds).' Bidded<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
                      <div class="row borderbottomLight"></div>
                      <div id="collapse9" class="panel-collapse collapse ">
                        <div class="panel-body borderBottom row removeBorder">';
         
      foreach ($auction_biddeds as $row) {             
           $rows .= '<div class="col-lg-3 col-md-3" id="ads-list" data-watchlist_id="'.$row->watchlist_id.'" data-watchlist_type="1" data-title="'.$row->title.'">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="'.url('/').'/ads/view/'.$row->id.'"><div class="adImage" style="background: url('.url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg">'.substr($row->title, 0, 35).''; 
                              if(strlen($row->title) >= 37){
            $rows .='..'; }
            $rows .='</div>
                          <div class="visible-md">'.substr($row->title, 0, 25).'';
                              if(strlen($row->title) >= 37){
            $rows .='..'; } 
            $rows .='</div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by '.$row->name.'<span class="pull-right"><strong class="blueText">
                                   '. number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">';
                        if($row->city || $row->countryName) {
            $rows .='<i class="fa fa-map-marker rightMargin"></i>
                                   '. $row->city.'';
                        if ($row->city){
            $rows .=', '; }  
                                   if($row->countryName) { 
            $rows .=''.$row->countryName.''; } else { 
            $rows .='None'; 
                                   } } else {
            $rows .='<i class="fa fa-map-marker rightMargin"></i> not available';
                                   }
            $rows .='<span class="pull-right">';
                                       if($row->average_rate != null) {
            $rows .='<span id="ads_latest_bids_'.$row->id.'" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>';
                                       } else {
            $rows .='<span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>';
                                       } 
            $rows .='</span>
                        </div>
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText blueText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>';
               }
          $rows .='</div>
                </div>
                <div class="row borderBottom"></div>
              </div>
            </div>
            </div>
            </div></div>
              <!-- Mobile Listing -->
        </div>
      </div>';

 

   return Response::json($rows);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function ReferralInvitation() {

     $input = Input::all();  
     $row = User::find(Request::input('user_id'));
      $result['row'] = $row;                      
     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


public function sendReferralInvitation() {
  $input = Input::all();  
  $user_id = Auth::user()->id;
  $get_user = User::find($user_id);

  $referral_code = $get_user->referral_code;
  $contact_name = $get_user->name; 
  $user_name = $get_user->username;
  $user_email = $get_user->email;
  $email_id  = array_get($input,'email');
  $filtered_email = (array_filter($email_id));  
    if ($get_user->telephone) {
        $user_phone = $get_user->telephone;
    } else {
        $user_phone = $get_user->mobile;
    } 
    if ($user_phone == null) {
        $user_phone = 'N/A';
    }  
    $referral_code = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="'.url('/').'/register/'.$get_user->referral_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Register Account
              </a>
          </td>';
            $newsletter = Newsletters::where('id', '=', '16')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                if(array_get($input,'contact_type') != 2){
                  foreach (array_filter($email_id) as $key) {
                  $get_email_id = UserImportContacts::where('id',$key)->get()->pluck('email')->toArray();
                    $check = Mail::send('email.referral_invitation', ['referral_code' => $referral_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $get_email_id) {
                           $message->to($get_email_id)->subject($newsletter->subject);
                       });

                    } 
                }else{
                   $check = Mail::send('email.referral_invitation', ['referral_code' => $referral_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $filtered_email) {
                           $message->to($filtered_email)->subject($newsletter->subject);
                       });

                }
               
              }   
     if($check){
       return redirect('dashboard')->with('status_referral', 'Invitation Sent');
      }else{
        return redirect('dashboard')->with('error_referral', 'Invitation Failed');
      }
}
 public function bidsUserPagination(){
    
    $skip  = Request::input('skip');
    $take  = Request::input('take');
    $last_id  = Request::input('last_id');
    $user_id = Request::input('user_id');
    $no_of_results  = Request::input('no_of_results');

    $check = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                      ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements.created_at', 'asc')->first();

    $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                      ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                       ->where('advertisements.id','<',$last_id)
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(3)->get();


                                       
    $result['last_id'] = $check->id;
    $result['rows'] = $rows;              
    $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s'); 

    return Response::json($result);
  }
   public function adsUserPagination(){
    $user_id = Request::input('user_id');
    $last_id  = Request::input('last_id');
    $check = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) use ($user_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'asc')->first();
    $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) use ($user_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                        ->where('advertisements.id','<',$last_id)
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(3)->get();
    $result['last_id'] = $check->id;                                    
    $result['rows'] = $rows;       
    return Response::json($result);
  }
  public function getCityForDashboard() {
        $get_city = City::where('country_id', '=', Auth::user()->country)->orderBy('name','asc')->get();

        $rows = '<option class="hide" value="">Select:</option>';
        foreach ($get_city as $key => $field) {
          $rows .= '<option value="' . $field->id . '" '.($field->id == Auth::user()->city ? 'selected':'').'>' . $field->name . '</option>';
        }

        
        $result['rows'] = $rows;
        return Response::json($result);

  }

  
}