<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomAttributes extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'category_custom_attributes';

	 public function subAttriInfo(){

	  		return $this->hasMany('App\SubAttributes','attri_id');
	    
	  }



}