<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomAttributeValues extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'custom_attribute_values';

   	public function getAttriValues(){
 		return $this->belongsToMany('App\CustomAttributes','sub_attri_id');
 	}

}