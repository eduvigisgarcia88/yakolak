<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubAttributes extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_attributes';

 	public function attriInfo(){
 		return $this->belongsToMany('App\CustomAttributes','cat_id');
 	}
	 public function attriValues(){

	  		return $this->hasMany('App\CustomAttributeValues','sub_attri_id');
	    
	  }
}