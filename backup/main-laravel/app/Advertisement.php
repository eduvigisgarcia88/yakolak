<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model {

	protected $fillable = [
		'title',
		'body',
		'published_at',
		'user_id',
		'photo'
		];

		protected $date = ['published_at'];


 		protected $table = 'advertisements';

		// public function scopePublished($query)
		// {
		// 	$query->where('published_at', '<=', Carbon::now());
		// }

		// public function setPublishedAtAttribute($date)
		// {
		// 	$this->attributes['published_at'] = Carbon::parse($date);
		// }

		// /** An articles is owned by a user. **/
		public function user()
		{
			return $this->belongsTo('App\User', 'user_id');
		}
			public function advertisementphoto()
		{
			return $this->belongsToMany('App\Advertisement', 'user_id');
		}
}
