<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'countries';

    public function getCity(){

    	return $this->hasOne('App\City','country_id');
    }
    
}