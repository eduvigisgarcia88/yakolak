<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryMain extends Model
{
  /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories_main';

    
}