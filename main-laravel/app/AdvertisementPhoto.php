<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementPhoto extends Model {

	protected $fillable = [
		'ads_id',
		'photo'
		];

		protected $date = ['published_at'];


 		protected $table = 'advertisements_photo';

		// public function scopePublished($query)
		// {
		// 	$query->where('published_at', '<=', Carbon::now());
		// }

		// public function setPublishedAtAttribute($date)
		// {
		// 	$this->attributes['published_at'] = Carbon::parse($date);
		// }

		// /** An articles is owned by a user. **/
		public function user()
		{
			return $this->belongsTo('App\Advertisement', 'ads_id');
		}
}
