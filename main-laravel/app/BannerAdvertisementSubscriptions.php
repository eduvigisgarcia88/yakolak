<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerAdvertisementSubscriptions extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner_advertisement_subscriptions';

  public function getBannerPlacement(){
         return $this->belongsTo('App\BannerPlacement','placement');
  }

  public function getBannerPlacementPlan(){
         return $this->belongsTo('App\BannerPlacementPlans','placement_plan_id');
  }
    
}