<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerSubscriptions extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner_subscriptions';

    public function getBannerPlacement(){
    	return $this->hasOne('App\BannerPlacement','id');
    }
  	public function getAllBannerPlacementPlans(){
  		return $this->hasOne('App\BannerPlacementPlans');
  	}
}