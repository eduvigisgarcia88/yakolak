<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model {

	protected $fillable = [
		'title',
		'body',
		'published_at',
		'user_id',
		'photo'
		];

		protected $date = ['published_at'];

 		protected $table = 'advertisements';

		// /** An articles is owned by a user. **/
		public function user(){
			return $this->belongsToMany('App\User', 'user_id');
		}
		 public function advertisementphoto(){
			return $this->belongsToMany('App\Advertisement', 'user_id');
		}
		public function getAdvertisementComments(){
			return $this->hasMany('App\UserAdComments','ad_id')->where('status', 1);
		}
		public function getAdvertisementPhoto(){
			return $this->hasMany('App\AdvertisementPhoto','ads_id')->orderby('photo','desc');
		}
		public function getDropdownCustomAttributes(){
			return $this->hasMany('App\CustomAttributesForDropdown','ad_id')->where('status',1);
		}
		public function getAdvertisementType(){
		   return $this->hasOne('App\AdvertisementType','id','ads_type_id');
		}
		public function getAdvertisementCategory(){
		   return $this->hasOne('App\Category','unique_id','parent_category');
		}
		public function getAdvertisementBidder(){
		   return $this->hasOne('App\AuctionBidders','ads_id')->orderby('bid','desc');
		}
		public function getAdRatings(){
			return $this->hasMany('App\AdRating','ad_id')->where('status', 1);
		}
	
}
