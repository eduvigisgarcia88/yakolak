<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Repositories\ShareGlobalVariablesRepository;

class ShareGlobalVariables {

    protected $menu;

    public function __construct(ShareGlobalVariablesRepository $menu)
    {
        $this->menu = $menu;
    }

    public function compose(View $view)
    {
        // // $global_countries_list = $this->menu->CountrySwitch();
        
        // // $global_country_list = '';
        
        // // foreach ($global_countries_list as $key => $field) {
            
        // //     $global_country_list .= '<div><b>' . chr($key) . '</b></div>';
        // //     foreach ($field as $fkey => $row) {
        // //        $global_country_list .= '<div>' . $row->countryName . '</div>';
        // //     }

        // // }

        // // // foreach ($variable as $key => $value) {
        // // //     # code...
        // // // }
        // // // $chr = 65;
        // // // foreach ($variable as $key => $value) {
        // // //     # code...
        // // // }
        // $global_country_list = $global_country_list['array'][0]
        //                         ->merge($global_country_list['array'][1])
        //                         ->merge($global_country_list['array'][2])
        //                         ->sort()
        //                         ->chunk($global_country_list['chunk_per'], TRUE);


        // // $global_country_list = '<div class="col-md-4"></div>';

        $global_country_list = $this->menu->CountrySwitch();

        $global_country_lists = $global_country_list['array'][0]
                                ->merge(isset($global_country_list['array'][1]) ? $global_country_list['array'][1] : [])
                                ->merge(isset($global_country_list['array'][2]) ? $global_country_list['array'][2] : [])
                                ->merge(isset($global_country_list['array'][3]) ? $global_country_list['array'][3] : [])
                                ->sort()->chunk($global_country_list['chunk_per'], TRUE);
                                
        $latest_comments_per = $this->menu->LatestComments();
        $latest_reviews_per = $this->menu->LatestReviews();
        $top_mem_per = $this->menu->TopMembers();
        $featured_ads_per = $this->menu->FeaturedAds();
        $total_ads = $this->menu->TotalAds();
        $total_auctions = $this->menu->TotalAuctions();
        $total_vendors = $this->menu->TotalVendors();
        $total_companies = $this->menu->TotalCompanies();
        $user_reg_count_today = $this->menu->UserRegistration()['user_count_today'];
        $user_reg_count_three = $this->menu->UserRegistration()['user_count_three'];
        $user_reg_count_two = $this->menu->UserRegistration()['user_count_two'];
        $user_reg_count_one = $this->menu->UserRegistration()['user_count_one'];
        $user_reg_max = $this->menu->UserRegistration()['highest_count_user_reg'];
        $user_reg_min = $this->menu->UserRegistration()['lowest_count_user_reg'];
        $ads_count_today = $this->menu->AdvertisementCount()['ads_count_today'];
        $ads_count_three = $this->menu->AdvertisementCount()['ads_count_three'];
        $ads_count_two = $this->menu->AdvertisementCount()['ads_count_two'];
        $ads_count_one = $this->menu->AdvertisementCount()['ads_count_one'];
        $ads_max = $this->menu->AdvertisementCount()['highest_count_ads'];
        $ads_min = $this->menu->AdvertisementCount()['lowest_count_ads'];
        $price_matrix_global = $this->menu->PriceMatrix();
        $languages_global = $this->menu->Languages();
        $banner_placement_global = $this->menu->BannerPlacement();
        $category_global = $this->menu->Categories();
        $recent_search = $this->menu->RecentSearch();
        $top_ads = $this->menu->TopAdsList();
        $top_bid = $this->menu->TopBidList();
        $featured_auctions_global = $this->menu->FeaturedAuctions();
        $verification_interval = $this->menu->GetVerificationInterval();
        $paypal_credentials = $this->menu->PaypalAPI();
        $categories_per = $this->menu->CategoriesPer();
        $message_count = $this->menu->messages();
        $notifications_global = $this->menu->notifications();
        $user_types_global = $this->menu->usertypes();
        $view->with(compact('global_country_lists','latest_comments_per','latest_reviews_per','top_mem_per','featured_ads_per','total_ads','total_auctions','total_vendors','total_companies','getSiteKey','user_reg_count_today','user_reg_count_three','user_reg_count_two','user_reg_count_one','user_reg_max','user_reg_min','ads_count_today','ads_count_three','ads_count_two','ads_count_one','ads_max','ads_min','price_matrix_global','languages_global','banner_placement_global','category_global','recent_search','top_ads','top_bid','featured_auctions_global','verification_interval','paypal_credentials','categories_per','message_count','notifications_global', 'user_types_global'));
    }
}