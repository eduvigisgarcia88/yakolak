<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\City;
use Paginator;
use Response;



class CityController extends Controller
{
	public function index(){
		$not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
	    $result = $this->doList();
	    $this->data['rows'] = $result['rows'];
	   	$this->data['title'] = "City Management";
	   	$this->data['refresh_route'] = url("admin/city/refresh");
	   	$this->data['country'] = Country::where('status',1)->get();
	  	return View::make('admin.country-management.city-management.list', $this->data);
	}
	public function doList(){
		 $result['sort'] = Request::input('sort') ?: 'countryName';
	      $result['order'] = Request::input('order') ?: 'asc';
	      $search = Request::input('search');
	      $status = Request::input('status');
	      $per = Request::input('per') ?: 20;

	      if (Request::input('page') != '»') {
	        Paginator::currentPageResolver(function () {
	            return Request::input('page'); 
	        });

	        $rows = City::where(function($query) use ($search) {
	                                  $query->where('countryName', 'LIKE', '%' . $search . '%');
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);             
	      } else {
	         $count = City::where(function($query) use ($search) {
	                                  $query->where('countryName', 'LIKE', '%' . $search . '%');
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);

	        Paginator::currentPageResolver(function () use ($count, $per) {
	            return ceil($count->total() / $per);
	        });

	      $rows = City::where(function($query) use ($search) {
	                                  $query->where('countryName', 'LIKE', '%' . $search . '%');
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);
	      }

	      // return response (format accordingly)
	      if(Request::ajax()) {
	          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows->toArray();
	          return Response::json($result);
	      } else {
	          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows;
	          return $result;
	      }	
	}
	public function changeStatus(){

	}
}