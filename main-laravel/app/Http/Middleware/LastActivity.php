<?php

namespace App\Http\Middleware;

use Closure;

use Carbon\Carbon;

use Auth;

class LastActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $unix_expiration = strtotime(Carbon::now('Asia/Manila')->addMinutes(5));
            $user = Auth::user();
            $user->online_status = 1;
            $user->last_activity = $unix_expiration;
            $user->save();
        }
      return $next($request);
    }
}
