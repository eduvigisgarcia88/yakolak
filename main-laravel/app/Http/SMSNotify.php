<?php
 function SMSNotify($message, $to)
  {
    $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
        [
          'api_key' =>  env('NEXMO_KEY'),
          'api_secret' => env('NEXMO_SECRET'),
          'to' => $to,
          'from' => 'YAKOLAK',
          'text' => $message
        ]
    );

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $nexmo_response = curl_exec($ch);
    curl_close($ch);
    return $nexmo_response;
  }