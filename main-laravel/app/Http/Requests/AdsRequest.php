<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class AdsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'body' => 'required',
            'photo' => 'required|image|max:3000',
        ];
    }

    /**
     * Get the validation attributes that apply to the request.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' => 'news title (english)', 
            'body' => 'description (english)', 
            'photo' => 'news title (arabic)', 
            
        ];
    }
}
