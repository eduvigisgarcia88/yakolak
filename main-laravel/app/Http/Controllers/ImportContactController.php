<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Category;
use App\Newsletters;
use Request;


class ImportContactController extends Controller
{
  public function loginWithGoogle(Request $request)
  {
      // get data from request
      $code = $request->get('code');

      // get google service
      $googleService = \OAuth::consumer('Google');

      // check if code is valid

      // if code is provided get user data and sign in
      if ( ! is_null($code))
      {
          // This was a callback request from google, get the token
          $token = $googleService->requestAccessToken($code);

          // Send a request with it
          $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

          $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
          echo $message. "<br/>";

          //Var_dump
          //display whole array.
          dd($result);
      }
      // if not ask for permission first
      else
      {
          // get googleService authorization
          $url = $googleService->getAuthorizationUri();

          // return to google login url
          return redirect((string)$url);
      }
  }
  
  public function microsoft() {
    
    $client_id = env('HOTMAIL_CLIENT_ID');
    $client_secret = env('HOTMAIL_SECRET_KEY');
    $redirect_uri = url('hotmail/import');
    $auth_code = request()->get('code');

    $fields = array(
      'code'=> urlencode($auth_code),
      'client_id'=> urlencode($client_id),
      'client_secret'=> urlencode($client_secret),
      'redirect_uri'=> urlencode($redirect_uri),
      'grant_type'=> urlencode('authorization_code')
    );

    $post = '';

    foreach ($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }

    $post = rtrim($post, '&');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://login.live.com/oauth20_token.srf');
    curl_setopt($ch, CURLOPT_POST, 5);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $access = json_decode($result);
    
    if (isset($access->access_token)) {
      $url = 'https://apis.live.net/v5.0/me/contacts?access_token='.$access->access_token;
      $response = file_get_contents($url);
      $response = json_decode($response, true);
      $this->data['result'] = [];
//       dd($response);
      foreach ($response['data'] as $key => $val) {
        $email = $val['emails']['preferred'];
        if ($email) {
          $this->data['result'] = $email;
        }
      }
      
      return view('contact.hotmail', $this->data);
      
    } else {
      return 'Something went wrong! Try again.';
    }
    
  }
  
  
  public function yahoo() {
    
    $client_id = env('YAHOO_CLIENT_ID');
    $client_secret = env('YAHOO_SECRET_KEY');
    $redirect_uri = url('yahoo/import');
    $auth_code = request()->get('code');
    
    $fields = array(
      'code'=> urlencode($auth_code),
      'client_id'=> urlencode($client_id),
      'client_secret'=> urlencode($client_secret),
      'redirect_uri'=> urlencode('oob'),
      'grant_type'=> urlencode('authorization_code')
    );

    $post = '';

    foreach ($fields as $key=>$value) { $post .= $key.'='.$value.'&'; }

    $post = rtrim($post, '&');

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://api.login.yahoo.com/oauth2/get_token');
    curl_setopt($ch, CURLOPT_POST, 5);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    $result = curl_exec($ch);
    curl_close($ch);
    $access = json_decode($result, true);
    if (isset($access['access_token'])) {
//       $url = 'https://social.yahooapis.com?access_token='.$access['access_token'];
      
      $crl = curl_init();

      $headr = array();
      $headr[] = 'Content-length: 0';
      $headr[] = 'Authorization: Bearer '.$access['access_token'];
      curl_setopt($crl, CURLOPT_URL, 'https://social.yahooapis.com/v1/user/'.$access['xoauth_yahoo_guid'].'/contacts?format=json&count=max');
      curl_setopt($crl, CURLOPT_HTTPHEADER, $headr);
      curl_setopt($crl, CURLOPT_HEADER, false);
      curl_setopt($crl, CURLOPT_POST, false);
      curl_setopt($crl, CURLOPT_RETURNTRANSFER, TRUE);
      $rest = curl_exec($crl);
      $contacts = json_decode($rest, true);

      curl_close($crl);
      
      $ctr = 0;
      $emails = [];
      $contact_list = isset($contacts['contacts']) ? $contacts['contacts'] : array();
      foreach ($contact_list as $key => $value) {
        
        $search = isset($value[$ctr]['fields']) ? $value[$ctr]['fields'] : array();
        
        for($x = 0; $x < count($search); $x++ ) {
          if (isset($search[$x]['type']) && $search[$x]['type'] == 'email') {
            $emails[] = $search[$x]['value'];
          }
        }
        
        $ctr++;
      }
//       dd($emails);
      $this->data['result'] = $emails;
      return view('contact.yahoo', $this->data);
      
    } else {
      return 'Something went wrong! Try again.';
    }
    
  }
  
}