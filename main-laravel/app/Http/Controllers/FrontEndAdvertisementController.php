<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\UserPlanTypes;
use App\UserAdComments;
use App\UserAdCommentReplies;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use App\SubCategoriesThree;
use App\AdvertisementType;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\AdvertisementPhoto;
use App\AdRating;
use App\AdvertisementBidDurationDropdown;
use App\SubAttributes;
use App\CompanyBranch;
use App\Country;
use App\City;
use App\CustomAttributes;
use App\CustomAttributeValues;
use App\CustomAttributesPerAd;
use App\CustomAttributesForTextbox;
use App\CustomAttributesForRadio;
use App\CustomAttributesForDropdown;
use App\CustomAttributesForCheckbox;
use App\AdvertisementWatchlists;
use App\Newsletters;
use App\PromotionPopups;
use App\PromotionPopupStatus;
use App\BannerSubscriptions;
use App\AuctionBidders;
use App\AdManagementSettings;
use App\BannerAdvertisementSubscriptions;
use App\BannerDefault;
use App\BannerAdvertisementMerge;
use App\BannerPlacementPlans;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Mail;
use Form;
use Route;
use Carbon\Carbon;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\UserMessages;
use App\UserNotifications;
use App\UserAdViews;
use App\UserBidList;
use App\AdvertisementSpamAndBots;
use URL;
use App\SMSLog;
use App\SettingForExchangePoints;
use App\AdFeatureTransactionHistory;
use App\AdVisit;
use App\APIKeys;

class FrontEndAdvertisementController extends Controller
{


    public function citySlug(){
      $cities = City::get();
      foreach($cities as $slug)
      {
        $slug->slug = strtolower(str_slug($slug->name, '-'));
        $slug->save();
      }
    }

    public function SMSNotify($message, $to){
      $api_key = APIKeys::where('id', 1)->first();
      $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
          [
            'api_key' =>  $api_key->api_key,
            'api_secret' => $api_key->api_secret,
            'to' => $to,
            'from' => 'YAKOLAK',
            'text' => $message
          ]
      );
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $nexmo_response = curl_exec($ch);
      curl_close($ch);
      $nexmo_result = json_decode($nexmo_response);
      $nexmo_status = $nexmo_result->messages[0]->status;
      if($nexmo_status == 0)
      {
        $sms_log = new SMSLog;
        $sms_log->user_id = Auth::user()->id;
        $sms_log->message_id = $nexmo_result->messages[0]->{'message-id'};
        $sms_log->message_price = $nexmo_result->messages[0]->{'message-price'};
        $sms_log->message_body = $message;
        $sms_log->user_mobile = $to;
        $sms_log->sms_status = $nexmo_status;
        $sms_log->delivery_status = 'sent';
        $sms_log->remaining_balance = $nexmo_result->messages[0]->{'remaining-balance'};
        $sms_log->sms_network = $nexmo_result->messages[0]->network;
        $sms_log->sms_type = 'bid';
        $sms_log->error_text = NULL;
        $sms_log->save();
      }
      
      else
      {
        $sms_log = new SMSLog;
        $sms_log->user_id = Auth::user()->id;
        $sms_log->message_id = NULL;
        $sms_log->message_price = NULL;
        $sms_log->message_body = $message;
        $sms_log->user_mobile = $to;
        $sms_log->sms_status = $nexmo_status;
        $sms_log->delivery_status = 'failed';
        $sms_log->remaining_balance = NULL;
        $sms_log->sms_network = NULL;
        $sms_log->sms_type = 'bid';
        $sms_log->error_text = $nexmo_result->messages[0]->{'error-text'};
        $sms_log->save();
      }

      return $nexmo_response;
    }

    public function getSMSBalance(){
      $api_key = APIKeys::where('id', 1)->first();
      $url = 'https://rest.nexmo.com/account/get-balance/'.$api_key->api_key.'/'.$api_key->api_secret;
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $nexmo_balance = curl_exec($ch);
      curl_close($ch);
      $nexmo_result = json_decode($nexmo_balance);
      $nextmo_remaining_balance = $nexmo_result->value;
      return $nextmo_remaining_balance;
    }

   public function index(){

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      // $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
      //                                               $query->where('reciever_id','=',$user_id);
      //                                                 // ->OrWhere('sender_id','=',$user_id);
      //                                               })->where(function($status){
      //                                                  $status->where('read_status','=',1);
      //                                               })->count();
       $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

   public function doList() {
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Advertisement::select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')
                         ->leftjoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('advertisements_photo.primary', '=', '1')
                                          ->where('advertisements.status', '=', '1')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

      } else {
        $count = Advertisement::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

    }


    public function editAds($id){

     $row = Advertisement::find($id);
     // check ad owner
     if(Auth::user()->id != $row->user_id){
      return Redirect::to('/');
    }
     $ads_id = $row->id;
     $listing = intval($row->listing);

     if($row) {    
        $this->data['ads'] = Advertisement::join('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                       ->leftJoin('categories','advertisements.category_id', '=', 'categories.id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.bid_duration_dropdown_id','advertisements.bid_duration_start','advertisements.minimum_allowed_bid','advertisements.bid_start_amount','advertisements.bid_limit_amount', 
                                        'advertisements.status','advertisements.updated_at','advertisements.sub_category_second_id','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.user_id',
                                         'advertisements.category_id','advertisements.sub_category_id',  'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube', 'advertisements.listing')
                                        ->where(function($query) use($ads_id) {
                                            $query->where('advertisements_photo.ads_id','=',$ads_id);
                                              })->first();



        $this->data['ads_photos'] = AdvertisementPhoto::where('advertisements_photo.ads_id','=',$ads_id)->where('advertisements_photo.status','=',1)->get();
        $this->data['featured_auctions']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.ads_type_id','advertisements.ad_expiration','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','users.usertype_id','countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1')
                                                   ->where('advertisements.ads_type_id','=',2);
                                        })->orderBy('advertisements.ad_expiration', 'desc')->take(4)->get();

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $custom_attributes = "";
          foreach ($get_dropdown as $key => $field) {

          $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
          $i = 1;
             foreach ($get_attri_info as $key => $field_attri_info) {
                $custom_attributes .= '<div class="form-group" id="ads-custom_attribute'.$i++.'">'.
                                      '<h5 class="col-sm-3 normalText" for="register4-email">'.$field_attri_info->name.'</h5>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero" name="dropdown_field[]">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                  $custom_attributes .= '<option class="hide">Select</option>';          
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $custom_attributes .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $custom_attributes .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
             
               foreach ($get_attri_info as $key => $field_attri_info) {
                $custom_attributes .= '<div class="form-group">'.
                                        '<h5 class="col-sm-3 normalText" for="register4-email">'.$field_attri_info->name.'</h5>'.
                                          '<div class="col-sm-9">';                  
                $custom_attributes .= '<input type="text" class="form-control borderZero" name="text_field[]" value="'.$field->attri_value.'">'; 
                 
                                 
                $custom_attributes .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'">'.
                                  '</div>'; 
               }           
          }
//        dd($dd);
          $parent_category_html = '';
          $category_html = '';
          $sub_category_html = '';
          $sub_category_two_html = '';
          if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry = Session::get('selected_location');
          }
          if($row->ads_type_id == 1){
            $get_ad_category = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                          ->where(function($query) use ($id) {
                                            $query->where('status',1);
                                           })->where(function($query) use ($listing){
                                            if ($listing == 0) {
                                              $query->where('buy_and_sell_status',2);
                                            }
                                           })->get();
            foreach ($get_ad_category as $key => $field) {
              $parent_category_html .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->parent_category ? ' selected':'').'>'.$field->name.'</option>';
            }

          }else{
           $get_ad_category = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                          ->where(function($query) {
                                            $query->where('status',1)
                                                  ->where('biddable_status', 2);
                                           })->get();
            foreach ($get_ad_category as $key => $field) {
               $parent_category_html .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->parent_category ? ' selected':'').'>'.$field->name.'</option>';
            }
          }

          if($row->category_id != null){
            $get_category = SubCategory::where('cat_id',$row->parent_category)->where('status',1)->get();
            foreach ($get_category as $key => $field) {
               $category_html .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->category_id ? ' selected':'').'>'.$field->name.'</option>';
            }
          }else{
              $category_html .='';
          }
          if($row->sub_category_id != null){
            $get_sub_category = SubCategoriesTwo::where('subcat_main_id',$row->category_id)->where('status',1)->get();
            foreach ($get_sub_category as $key => $field) {
               $sub_category_html .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->sub_category_id ? ' selected':'').'>'.$field->name.'</option>';
            }
          }else{
            $sub_category_html .= '';
          }
          if($row->sub_category_second_id != null){
            $get_sub_category_two = SubCategoriesThree::where('subcat_tier_id',$row->sub_category_id)->where('status',1)->get();
            foreach ($get_sub_category_two as $key => $field) {
               $sub_category_two_html .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->sub_category_second_id ? ' selected':'').'>'.$field->name.'</option>';
            }
          }else{
              $sub_category_two_html .='';
          }

          $this->data['parent_category_html'] =  $parent_category_html;
          $this->data['category_html'] = $category_html;
          $this->data['subcategory_html'] = $sub_category_html;
          $this->data['subcategorytwo_html'] =  $sub_category_two_html;
          $getCountryCode = Country::where('countryCode',$row->country_code)->where('status',1)->first();
          $this->data['countries'] = Country::where('status',1)->get();
          $this->data['city'] = City::where('status',1)->where('country_id',$getCountryCode->id)->get();
          $this->data['get_ad_types'] = AdvertisementType::all(); 
          $this->data['country'] = Country::all();            
          $this->data['custom_attributes_checkbox'] = CustomAttributesForCheckbox::where('ad_id','=',$ads_id)->get();
          $this->data['custom_attributes_radio'] = CustomAttributesForRadio::where('ad_id','=',$ads_id)->get();
          $this->data['bid_duration_dropwdown'] = AdvertisementBidDurationDropdown::orderBy('id', 'desc')->get(); 
    
     if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }

         $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
         // $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
         //                                            $query->where('reciever_id','=',$user_id);
         //                                            })->where(function($status){
         //                                               $status->where('read_status','=',1);
         //                                            })->count();
         $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

         

         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['category'] = Category::with('subcategoryinfo.getOtherSubcategories')->get();
         // $this->data['countries'] = Country::all();
         // $this->data['city'] = City::all();
         $this->data['usertypes'] = Usertypes::all();
         $this->data['ads_types'] = AdvertisementType::all();
         $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get(); 
         $this->data['sub_categories'] = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', $this->data['ads']->category_id)->get(); 

      }else{
          return Response::json(['error' => "Invalid row specified"]);
        }
        $this->data['title'] = "Edit My Ads"; 
        $this->data['category'] = Category::with('subcategoryinfo.getOtherSubcategories')->get();
        // $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['ads_types'] = AdvertisementType::all();
        $this->data['custom_attributes'] = $custom_attributes;
        
        $this->data['sub_categories'] = SubCategory::where('status',1)->where('cat_id','=', $this->data['ads']->category_id)->get(); 
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
       $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

        $remaining_listings = Auth::user()->total_ads;
        $remaining_auctions = Auth::user()->total_bids;

        $total_listings = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 1)->count();
        $total_auctions = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 2)->count();

        $listing_available = $remaining_listings - $total_listings;
         if ($listing_available <= 0) {
           $this->data['get_listing_available'] = '0';
         } else{
          $this->data['get_listing_available'] = $listing_available;
         }

         // compute auction ad available
         $auction_ads_available = $remaining_auctions - $total_auctions;
         if ($auction_ads_available <= 0) {
           $this->data['get_auction_ads_available'] = '0';
         } else{
          $this->data['get_auction_ads_available'] = $auction_ads_available;
         }
      
        $get_banner_placement_right = BannerAdvertisementSubscriptions::where('page',3)->where('banner_status','=',4)->where('placement','=',2)->get();
        if (count($get_banner_placement_right) > 0) {
           $this->data['banner_placement_right_status'] = 1;
           $this->data['banner_placement_right'] = $get_banner_placement_right->random(1);
        }else{
           $this->data['banner_placement_right_status'] = 0;
        }

                                
        return view('ads.edit', $this->data);
   }


public function postAds()
{
        if (!Auth::check()) {
          return Redirect::to('/');
        }

        if(Auth::user()->status != 1) {
          return Redirect::to('/');
        }

        $user_id = Auth::user()->id;
        $user_plan_type = Auth::user()->user_plan_types_id;
        $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
                                       
        $this->data['featured_auctions']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.ads_type_id','advertisements.ad_expiration','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1')
                                                   ->where('advertisements.ads_type_id','=',2);
                                        })->orderBy('advertisements.ad_expiration', 'desc')->take(4)->get();
                                       // dd($this->data['featured_auctions']);

        $total_ad_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();

         $get_user_plan_types = UserPlanTypes::find($user_plan_type);
    
         $post_ad_btn = "";
        //   if (count($total_ad_posted) >= $get_user_plan_types->total_ads_allowed) {
        // $post_ad_btn .= '<input type="button" class="btn blueButton borderZero noMargin pull-right btn-post_not_allowed" value="Post Ad" style="width:200px;">';   
        //   } else {
        //    $post_ad_btn .= '<input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Post Ad" style="width:200px;">';    
        //   }
          //compute listing available


        $remaining_listings = Auth::user()->total_ads;
        $remaining_auctions = Auth::user()->total_bids;

        $total_listings = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 1)->count();
        $total_auctions = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 2)->count();

        $listing_available = $remaining_listings - $total_listings;
         if ($listing_available <= 0) {
           $this->data['get_listing_available'] = '0';
         } else{
          $this->data['get_listing_available'] = $listing_available;
         }

         // compute auction ad available
         $auction_ads_available = $remaining_auctions - $total_auctions;
         if ($auction_ads_available <= 0) {
           $this->data['get_auction_ads_available'] = '0';
         } else{
          $this->data['get_auction_ads_available'] = $auction_ads_available;
         }


         $post_ad_btn .= '<input type="hidden" id="remaining_listings" value="'.max((int)$listing_available, 0).'"><input type="hidden" id="remaining_auction" value="'.max((int)$auction_ads_available, 0).'"><input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Post Ad" style="width:200px;">';

         if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }

          $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
          
          // $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
          //                                           $query->where('reciever_id','=',$user_id);
          //                                           })->where(function($status){
          //                                              $status->where('read_status','=',1);
          //                                           })->count();

           $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
                                                    
         $this->data['get_user_country'] = Auth::user()->country;
         $this->data['get_user_city'] = Auth::user()->city;
         $this->data['get_user_address'] = Auth::user()->address;
         $this->data['get_user_plan_types'] = $get_user_plan_types;
         $this->data['post_ad_btn'] = $post_ad_btn;
         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['category'] = Category::with('subcategoryinfo.getOtherSubcategories')->get();
         
         $this->data['usertypes'] = Usertypes::all();
         $this->data['ads_types'] = AdvertisementType::all();
        if(Session::has('selected_location') == null){
          $selectedCountry = Session::get('default_location');
        }else{
          $selectedCountry = Session::get('selected_location');
        }
         $this->data['countries'] = Country::where('status',1)->get();
         $this->data['city'] = [];
     
   
        $this->data['categories'] = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                           ->where('home',2)
                                           ->where('status',1)
                                           ->get();
         // $this->data['categories'] = Category::where()=>where('status',1)->get(); 
         $this->data['bid_duration_dropwdown'] = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get();
         $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        $this->data['preloaded_categories'] = Category::where(function($query){
                                                      $query->where('status','=',1)
                                                             ->where('biddable_status','=',1);
                                                      })->get();
        $get_banner_placement_right = BannerAdvertisementSubscriptions::where('page',3)->where('banner_status','=',4)->where('placement','=',2)->get();
        if (count($get_banner_placement_right) > 0) {
           $this->data['banner_placement_right_status'] = 1;
           $this->data['banner_placement_right'] = $get_banner_placement_right->random(1);
        }else{
           $this->data['banner_placement_right_status'] = 0;
        }

        if(request()->route()->parameters())
        {
          if(request()->route('slug') == 'auctions')
          {
            $this->data['change_to_auction'] = 1;
          }
        }
        // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
     
      // YAH IT WORKS TILL THE END!!!!
                                                                                               
      return view('ads.post', $this->data);
}

    public function search(){
        $input = Input::all();
        $row = Advertisement::where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get(); 
        

}

    public function saveAds(){
        $input = Input::all();
        $input['price'] = str_replace(',', '', array_get($input, 'price'));
        $input['bid_start_amount'] = str_replace(',', '', array_get($input, 'bid_start_amount'));
        $input['minimum_allowed_bid'] = str_replace(',', '', array_get($input, 'minimum_allowed_bid'));
        $images_loop = UserPlanTypes::where('id', Auth::user()->user_plan_types_id)->pluck('img_per_ad');
//         $price = str_replace(',', '', array_get($input, 'price'));
//         $input->merge(array('price' => $price));
        $new = true;
        $user_id = Auth::user()->id;
        $total_ad_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();

        $user_plan_type = Auth::user()->user_plan_types_id;
        $id = Input::get('id');
        $category = Input::get('category');
        $photo = array_get($input, 'photo');
        $rem_img_upload = Input::get('rem_img_upload');
        $delete_photo = Input::get('delete_photo[]');
        $text_field = Input::get('text_field');
        $dropdown_field = Input::get('dropdown_field');
        $attribute_textbox_id = Input::get('attribute_textbox_id');
        $attribute_dropdown_id = Input::get('attribute_dropdown_id');

        $text_field_id = Input::get('text_field_id');
        $dropdown_field_id = Input::get('dropdown_field_id');
        $country = Input::get('country');
        $photo_id1 = Input::get('photo_id1');
        $photo_id2 = Input::get('photo_id2');
        $photo_id3 = Input::get('photo_id3');
        $photo_id4 = Input::get('photo_id4');
//         $bid_limit_amount = Input::get('bid_limit_amount');
        $bid_start_amount = Input::get('bid_start_amount');
        $bid_duration_start = Input::get('bid_duration_start');
        $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
        $listing = Input::get('ad_type');
        $ads_type_id = (Input::get('ad_type') == 2 ? 2 : 1);
        
        if (Input::get('city2')) {
         $city = 'city2';
        } else if (Input::get('city')) {
         $city = 'city';

        }
        $contact_info = Auth::user()->telephone;
        if (is_null($contact_info)) {
          $contact_info = Auth::user()->mobile; 
        }

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }


        if ($new) {
           $remaining_listings = Auth::user()->total_ads;
           $remaining_auctions = Auth::user()->total_bids;

           $total_listings = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 1)->count();
           $total_auctions = Advertisement::where('status', 1)->where('user_id', $user_id)->where('ads_type_id', 2)->count();

           if ($ads_type_id == 2) {

            $auction_ads_available = $remaining_auctions - $total_auctions;
             if ($auction_ads_available <= 0) {
               return Response::json(['error' => ['LIMIT_REACHED']]);
             }
             
           } else {

            $listing_available = $remaining_listings - $total_listings;
             if ($listing_available <= 0) {
               return Response::json(['error' => ['LIMIT_REACHED']]);
             }

           }
           

           
        }
      
      $files = Input::file('photo');

        $i = $rem_img_upload;
        $rules = [
             'title'                  => 'required|min:4|max:100',
             'description'            => 'required',
             'category'               => 'required',
             'youtube'                => 'url',
             'price'                  => 'min:1|max:9999999999|numeric'.($ads_type_id==1?'|required':''),
             'bid_start_amount'       => ($ads_type_id==2?'min:1|max:9999999999|numeric|required':''),
             'minimum_allowed_bid' => ($ads_type_id==2?'min:1|max:9999999999|numeric|required':''),
             'bid_duration_start'     => ($ads_type_id==2?'min:1|max:99|numeric|required':''),
             'bid_duration_dropdown_id' => ($ads_type_id==2?'min:1|max:4|numeric|required':''),
             'country'                => 'min:1|required',
             (!is_null(Input::get('city2'))?'city2':'city')  => 'required',
        ];
      
      
        if (array_get($input, 'photo_id_edit')) {
          $validate_photo = [];
          foreach (array_get($input, 'photo_id_edit') as $photo_ids) {
            $validate_photo['photo'.$photo_ids] = 'image|max:3000|mimes:jpeg,jpg';
          }
          $rules = array_merge($rules, $validate_photo);
        }
      
      
        $dyn_photo_rules = [];
        for ($x = 1; $x <= $images_loop; $x++) {
          $dyn_photo_rules['photo'.$x] = 'image|max:3000|mimes:jpeg,jpg';
        }
      
        $rules = array_merge($rules, $dyn_photo_rules);
      
      
        // field name overrides
        $names = [
            'title'               => 'title',
            'category'            => 'category',
            'subcategory'         => 'sub category',
            'description'         => 'description',
            'price'               => 'price',
            'bid_start_amount'    => 'bid start amount',
            'minimum_allowed_bid' => 'minimum allowed amount',
            'bid_duration_start'  => 'bid duration',
            'bid_duration_dropdown_id'  => 'bid duration',
            'country'             => 'Country',
            (!is_null(Input::get('city2'))?'city2':'city') => 'City',
        ];

        $messages = [
          'title.required' => 'error-title|* Fill the required fields',
          'description.required' => 'error-description|* Fill the required fields',
          'category.required' => 'error-category|* Fill the required fields',
          'price.required' => 'error-price|* Fill the required fields',
          'price.max' => 'error-price|* Field may not be greater than '.number_format('9999999999'),
          'bid_start_amount.required' => 'error-bid_start_amount|* Fill the required fields',
          'bid_start_amount.min' => 'error-bid_start_amount|* Field must be greater than 1.',
          'bid_start_amount.max' => 'error-bid_start_amount|* Field may not be greater than '.number_format('9999999999'),
          'minimum_allowed_bid.required' => 'error-minimum_allowed_bid|* Fill the required fields',
          'minimum_allowed_bid.max' => 'error-minimum_allowed_bid|* Field may not be greater than '.number_format('9999999999'),
          'minimum_allowed_bid.min' => 'error-minimum_allowed_bid|* Field must be greater than 1.',
          'bid_duration_start.required' => 'error-bid_duration_start|* Fill the required fields',
          'bid_duration_dropdown_id.required' => 'error-bid_duration_dropdown_id|* Fill the required fields',
          'country.required' => 'error-country|* Fill the required fields',
          'city.required' => 'error-city|* Fill the required fields',
          'city2.required' => 'error-city|* Fill the required fields',
          'youtube.url' => 'error-youtube|* Invalid URL format.',
        ];
      
        if (array_get($input, 'photo_id_edit')) {
          $mes = [];
          foreach (array_get($input, 'photo_id_edit') as $photo_ids) {
            $mes['photo'.$photo_ids.'.max'] = 'error-photo-type-'.$photo_ids.'|* The photo may not be greater than 3MB';
            $mes['photo'.$photo_ids.'.mimes'] = 'error-photo-type-'.$photo_ids.'|* Photo must be a jpeg / jpg';
          }
          $messages = array_merge($messages, $mes);
        }
      
        
      
        $dyn_photo_message = [];
        for ($x = 1; $x <= $images_loop; $x++) {
          $dyn_photo_message['photo'.$x.'.max'] = 'error-photo'.$x.'|* The photo may not be greater than 3MB';
          $dyn_photo_message['photo'.$x.'.mimes'] = 'error-photo'.$x.'|* Photo must be a jpeg / jpg';
        }
      
        $messages = array_merge($messages, $dyn_photo_message);
        
        
        

        // do validation
        $validator = Validator::make($input, $rules, $messages);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        // if ($ads_type_id==2) {
        //    if ($bid_limit_amount <= $bid_start_amount) {
        //     return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start amount']); 
        //     }
        // }
       
       //check post limit depends on user plan
        $get_plan = UserPlanTypes::find($user_plan_type);
          $get_posted_auctions = Advertisement::where('ads_type_id','=','2')->where('user_id','=', $user_id)->where('status','!=','2')->get();
          $get_posted_basic_ad = Advertisement::where('ads_type_id','=','1')->where('user_id','=', $user_id)->where('status','!=','2')->get();
          $get_all_post = Advertisement::where('user_id','=', $user_id)->where('status','!=','2')->get();
        // if (count($get_all_post) >= $get_plan->total_ads_allowed && $new == true) {
        //              return Response::json(['erroruser' => "You have reached the allowed Advertisement post"]);     
        //   } 
          //check limit on edit ad page
          if ($new == false && $row->ads_type_id != $ads_type_id) {
            // if ($ads_type_id == '2') {
            //     if (count($get_posted_auctions) >= $get_plan->auction ) {
            //          return Response::json(['erroruser' => "You have reached the allowed auction limit"]);     
            //     } 
            // } else {
            //     if (count($get_posted_basic_ad) >= $get_plan->ads) {
            //          return Response::json(['erroruser' => "You have reached the allowed basic ad limit"]);     
            //     }
            // }

              $remove_existing_bidders = AuctionBidders::where('ads_id','=',$row->id)->get();
            if ($remove_existing_bidders) {
               foreach ($remove_existing_bidders as $value) {
                 $value->bid_limit_amount_status = 0;
                 $value->bidder_status = 0;
                 $value->status = 0;
                 $value->save();
               }
            }
  
        }

          // create model if new
          if($new) {
              $row = new Advertisement;
          }

          $array_for_title = [];
          $strPar = array_get($input, 'title');
          $get_keyword = AdvertisementSpamAndBots::get();
          foreach($get_keyword as $keyword => $field){
                   $exists = strpos($strPar, $field->restricted_word);
                    if ($exists !== false) {
                       $array_for_title[] = $field->id ;
                    }
          }
          $array_for_description = [];
          $strPar = array_get($input, 'description');
          $get_keyword = AdvertisementSpamAndBots::get();
          foreach($get_keyword as $keyword => $field){
                   $exists = strpos($strPar, $field->restricted_word);
                    if ($exists !== false) {
                       $array_for_description[] = $field->id ;
                    }
          }
          if(count($array_for_title) > 1){
            $row->status=6;
          }
          if(count($array_for_description) > 1){
            $row->status=6;
          }
          // $get_unique_id                   = Category::find($category);
          $row->ads_type_id                = $ads_type_id;
          if($ads_type_id == 1){
             $row->parent_category            = array_get($input, 'main_category');
          }else{
            $row->parent_category            = array_get($input, 'main_category_auction');
          }
          $row->cat_unique_id              = array_get($input, 'category');
          $row->sub_category_id            = array_get($input, 'subcategory_one');
          $row->sub_category_second_id     = array_get($input, 'subcategory_two');
          $row->category_id                = array_get($input,'category');
          $row->user_plan_id               = $user_plan_type;
          $row->user_id                    = Auth::user()->id;
          $row->title                      = array_get($input, 'title');
          $row->price                      = array_get($input, 'price');
          $row->currency                   = Country::where('id', array_get($input, 'country'))->pluck('currency');
      
          if ($listing == 4) {
            $row->listing = 1;
          } else {
            $row->listing = 0;
          }
      
          if ($contact_info) {
           $row->contact_info              = $contact_info;
          }else {
           $row->contact_info              = null;
          }

          if (array_get($input, 'description')) {
          $row->description                = array_get($input, 'description');
          } else {
           $row->description               = null;
          }

          //ad expiration auction
         if ($row->bid_duration_start != $bid_duration_start || $row->bid_duration_dropdown_id != $bid_duration_dropdown_id) {
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                } elseif ($bid_duration_dropdown_id == '4'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." years"));
                   $row->ad_expiration = $expiration_date;
                }
          } 
          if ($ads_type_id == 2) {
//             $row->bid_limit_amount         = array_get($input, 'bid_limit_amount');
            $row->bid_start_amount         = array_get($input, 'bid_start_amount');
            $row->minimum_allowed_bid      = array_get($input, 'minimum_allowed_bid');
            $row->minimum_bid_fixed        = array_get($input, 'minimum_allowed_bid');
            $row->price                    = array_get($input, 'bid_start_amount');
            if (!$new) {
              $reset_bidder = AuctionBidders::where('ads_id','=',$row->id)->where('bid_limit_amount_status','=',1)->first();
              if ($reset_bidder) {
                $reset_bidder->bid_limit_amount_status = '0';
                $reset_bidder->save();
              }
            }
            $row->bid_duration_start       = array_get($input, 'bid_duration_start');
            $row->bid_duration_dropdown_id = array_get($input, 'bid_duration_dropdown_id');
          } else {
//             $row->bid_limit_amount          = null;
            $row->bid_start_amount          = null; 
            $row->minimum_allowed_bid       = null;
            $row->minimum_bid_fixed         = null;
            $row->bid_duration_start        = null;
            $row->bid_duration_dropdown_id  = null;
          }

          //ad expiration basic ad
          $get_ad_mgmt_expire = AdManagementSettings::where('id','=','1')->first();
          if ($ads_type_id == '1' && $get_ad_mgmt_expire->basic_ad_expiration_status == '1') {
             $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($get_ad_mgmt_expire->basic_ad_expiration_end == 'hours') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." hours"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'days'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'months'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'years'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." years"));                 
                   $row->ad_expiration = $expiration_date;
                }
            } else if ($ads_type_id == '1') {
             $row->ad_expiration = '0000-00-00 00:00:00';
            }

          if (array_get($input, 'youtube')) {
            $row->youtube                  = array_get($input, 'youtube');
          } else {
            $row->youtube                  = null;
          }
            if (array_get($input, 'country')) {
            $row->country_id               = array_get($input, 'country');
          } else {
            $row->country_id               = null;
          }
            if (array_get($input, 'city2')) {
            $row->city_id                  = array_get($input, 'city2');
          } else {
            $row->city_id                 = array_get($input, 'city');
          }
            if (array_get($input, 'address')) {
            $row->address                  = array_get($input, 'address');
          } else {
            $row->address                  = null;
          }

          //for auction exp email trigger
          if ($row->ads_type_id == 2) {
           $row->ad_exp_email_trigger = 1;
           $row->auction_expiring_noti_trigger = 1;

          }
            // save model
          $row->save();
          $row->slug = str_slug($row->title,'-').'-'.$row->id;
          $save_parent_category_slug = Category::where('unique_id',$row->parent_category)->first();
          if(is_null($save_parent_category_slug)){
            $pool = '123456789';
            $random_code = substr(str_shuffle(str_repeat($pool, rand(3,9))), 0, 5);
            $row->parent_category_slug = $random_code;
          }else{
            $row->parent_category_slug = str_slug($save_parent_category_slug->name,'-');
          }
          $save_category_slug = SubCategory::where('unique_id',$row->category_id)->first();
          if(is_null($save_category_slug)){
            $pool = '123456789';
            $random_code = substr(str_shuffle(str_repeat($pool, rand(3,9))), 0, 5);
            $row->category_slug = $random_code;
          }else{
            $row->category_slug = str_slug($save_category_slug->name,'-');
          }
          if($row->ads_type_id == 1){
            $row->ad_type_slug = 'ads';
          }else{
            $row->ad_type_slug = 'auctions';
          }
          $get_country_code = Country::where('id',$row->country_id)->first();
          $row->country_code = strtolower($get_country_code->countryCode);
          if($row->category_id == 'Select'){
            $row->category_id == NULL;
          }
          if($row->sub_category_id == 'Select'){
            $row->sub_category_id = NULL;
          }
          $row->save();

     if ($new) {
      $i = $rem_img_upload;

      for ($x = 1; $x <= $i; $x++) {
        if(Input::file('photo'.$x.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$x.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$x.''))->fit(260, 160)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = new AdvertisementPhoto;
             $savephoto->ads_id = $row->id;
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->photo_no = $x;
             $savephoto->save();
        }else{
            $savephoto = new AdvertisementPhoto;
            $savephoto->ads_id = $row->id;
            $savephoto->photo = null;
            $savephoto->save();          
        }
      }

      // $primary_first = AdvertisementPhoto::where('ads_id',$row->id)->whereNotNull('photo')->orderBy('created_at','desc')->first();
      // if($primary_first){
      // $primary_first->primary = 1;
      // $primary_first->save();
      // }

      if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
      if(!is_null($dropdown_field)){
        foreach ($dropdown_field_id as $key => $xvalue) {
           $ad_attri = new CustomAttributesForDropdown;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $dropdown_field_id[$key];
           $ad_attri->attri_value_id  = $dropdown_field[$key];
           $ad_attri->save();
        }
      } 
   }
   //FOR EDITING ADS
  else{
    // Save the photo
   $i = $rem_img_upload;
     $get_photos = AdvertisementPhoto::where('ads_id','=',$id)->get();
     $get_primary_photos = AdvertisementPhoto::where('ads_id','=',$id)->where('primary','=','1')->first();
     $primary_photo = array_get($input, 'delete_photo'.$get_primary_photos->id.'');

  foreach($get_photos as $key => $value) {     
// if ($primary_photo) {
//    $rules = [
//              'photo'.$primary_photo.'' => 'required',
//             ];
//    $names = [           
//             'photo'.$primary_photo.''  => 'Primary Photo',
//         ];

//         $validator = Validator::make(Input::all(), $rules);
//         $validator->setAttributeNames($names); 
//         if($validator->fails()) {
//             return Response::json(['error' => array_unique($validator->errors()->all())]);
//         }
// }
         
  //save primary
        // if(Input::file('photo'.$primary_photo.'')){
        //     $FileName = $row->id.rand(10,9999);
        //     Image::make(Input::file('photo'.$primary_photo.''))->save('uploads/ads/' . $FileName .'.jpg');
        //     Image::make(Input::file('photo'.$primary_photo.''))->fit(260, 160)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
        //      $savephoto = AdvertisementPhoto::find($primary_photo);
        //      $savephoto->ads_id = $row->id;
        //      $savephoto->photo = $FileName . '.jpg';
        //      $savephoto->save();
        // }
  //save other photos
       if (Input::file('photo'.$value->id.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$value->id.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$value->id.''))->fit(260, 160)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = AdvertisementPhoto::find($value->id);
             $savephoto->ads_id = $row->id;
             $savephoto->photo_no = ($key+1);
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }

  
   //delete photo    
         $delete = array_get($input, 'delete_photo'.$value->id);
         if ($delete) {
        $get_delete_photo = AdvertisementPhoto::find($delete);
        $get_delete_photo->photo = null;
        $get_delete_photo->primary = 0;
        $get_delete_photo->save();
         }
        }

      $edit_check_photo = AdvertisementPhoto::where('ads_id','=',$row->id)->where('primary','=',1)->whereNotNull('photo')->first();
      if(is_null($edit_check_photo))
      {
        $savephoto_edit = new AdvertisementPhoto;
        $savephoto_edit->ads_id = $row->id;
        $savephoto_edit->primary = 1;
        $savephoto_edit->photo = 'nopreview.png';
        $savephoto_edit->save();
      }

      $delete_no_preview = AdvertisementPhoto::where('ads_id','=',$row->id)->whereNotNull('photo')->whereNotIn('photo',['nopreview.png'])->get();
      $getter = AdvertisementPhoto::where('ads_id','=',$row->id)->whereNotNull('photo')->get();
      if($delete_no_preview){
        foreach($getter as $del)
        {
          if($del->photo == 'nopreview.png')
          {
            $del->delete();
          }
        }
      }

      // //ONE PRIMARY ONLY - FIX
      // $fix_primary = AdvertisementPhoto::where('ads_id','=',$row->id)->whereNotNull('photo')->where('primary','=',1)->get();
      // if(count($fix_primary) > 1)
      // {
      //   foreach($fix_primary as $fix_this)
      //   {
      //     $fix_this->primary = 0;
      //     $fix_this->save();
      //   }
      // }

      // $edit_primary_first = AdvertisementPhoto::where('ads_id', $row->id)->whereNotNull('photo')->orderBy('photo_no', 'asc')->first();
      // if($edit_primary_first)
      // {
      //   $edit_primary_first->primary = 1;
      //   $edit_primary_first->save();
      // }
      

$get_exist_dropdown = CustomAttributesForDropdown::where('ad_id','=',$id)->get();
if ($get_exist_dropdown) {
     foreach ($get_exist_dropdown as $values) {
        $values->status = '2';
        $values->save();    
     }

      if(!is_null($dropdown_field)){
                foreach ($dropdown_field_id as $key => $xvalue) {     
                   $ad_attri = new CustomAttributesForDropdown;
                   $ad_attri->ad_id  = $row->id;
                   $ad_attri->attri_id = $dropdown_field_id[$key];
                   $ad_attri->attri_value_id  = $dropdown_field[$key];
                   $ad_attri->save();
                }
          } 
}
$get_existing_textbox = CustomAttributesForTextbox::where('ad_id','=',$id)->get();
if ($get_existing_textbox) {
     foreach ($get_existing_textbox as $values) {
        $values->status = '2';
        $values->save();    
     }

   if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
}
   
  }

       if($new){
             if (Auth::user()->name) {
                 $name = Auth::user()->name;
             }  else {
              $name = null;
             }

         //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
              if (Auth::user()->telephone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               }
               if ($user_phone == null) {
              $user_phone = 'N/A';
               }  
              $item_title = $row->title;
              $item_url =  url('view').'/'.$row->slug;
              $item_link = url('view').'/'.$row->slug;
              // $image_link = url('uploads/ads/thumbnail').'/'.$savephoto->photo;
              // dd($image_link);
              $comment = 'http://www.yakolak.com/ads/view/'.$row->id;

                  //email user new ad created
             if ($row->ads_type_id == '1') {
                 $newsletter = Newsletters::where('id', '=', '2')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.ad_creation', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
                     }
                 
             }else{
                  //email user auction created
                 $newsletter = Newsletters::where('id', '=', '5')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.ad_creation', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
                     }
             }

        }

        $fix_primary = AdvertisementPhoto::where('ads_id', $row->id)->get();
        if($fix_primary)
        {
          foreach ($fix_primary as $fix_this) {
            $fix_this->primary = 0;
            $fix_this->save();
          }
        }

        $edit_primary_first = AdvertisementPhoto::where('ads_id', $row->id)->whereNotNull('photo')->orderBy('photo_no', 'asc')->first();
        if ($edit_primary_first) {
          $edit_primary_first->primary = 1;
          $edit_primary_first->save();
        }

        $check_photo = AdvertisementPhoto::where('ads_id', $row->id)->where('primary', 1)->whereNotNull('photo')->first();
        if (is_null($check_photo)) {
          $savephoto = new AdvertisementPhoto;
          $savephoto->ads_id = $row->id;
          $savephoto->primary = 1;
          $savephoto->photo = 'nopreview.png';
          $savephoto->save();
        }


        // $redirect_url ='ads/view/'.$row->id;
        if($new){
              return Response::json(['body' => "Advertisement Created",'ad_id' => $row->id, 'code' => 1, 'ad_type' => $row->ads_type_id, 'url' => url('dashboard'), 'type' => 'alert-success']);

        } else {
             return Response::json(['body' => "Advertisement Successfully Updated",'code' => 2, 'ad_type' => $row->ads_type_id, 'url' => url('dashboard'), 'type' => 'alert-success']);
        }

    }

  public function redirectToSlug($id){
    $row = Advertisement::find($id);
    if($row == null){
      return "404";
    }else{
      return redirect()->route('SEO_URL',['country_code'=> $row->country_code,'ad_type_slug'=> $row->ad_type_slug,'parent_category_slug'=> $row->parent_category_slug,'category_slug'=> $row->category_slug,'slug'=> $row->slug]);
    }
  }

  // public function slugToSEO($slug){
  //   $row = Advertisement::where('slug',$slug)->first();
  //   if($row == null){
  //     return "404";
  //   }else{
  //     return redirect()->route('SEO_URL',['country_code'=> $row->country_code,'ad_type_slug'=> $row->ad_type_slug,'parent_category_slug'=> $row->parent_category_slug,'category_slug'=> $row->category_slug,'slug'=> $row->slug]);
  //   }
  // }

   public function fetchInfoPublicAds ($country_code,$ad_type_slug,$parent_category_slug,$category_slug,$slug){
    if($slug == 'q')
    {
      return app('App\Http\Controllers\FrontEndCategoryController')->fetchSubCategoryList($country_code,$ad_type_slug,$parent_category_slug,$category_slug);
    }
    else
    {
      $result = $this->doFetchInfoPublicAds($country_code,$ad_type_slug,$parent_category_slug,$category_slug,$slug);
    }
      
      $client_ip = Request::ip();
      $client_location = geoip()->getLocation($client_ip)->iso_code;
      $list_country = Country::where('status',1)->lists('countryCode')->toArray();
        if(!Auth::check())
        {
          if(Session::has('default_location') == FALSE)
          {
            if(in_array($client_location, $list_country))
            {
              Session::set('default_location',strtolower($client_location));
            }
            else
            {
              Session::set('default_location','ALL');
            }
          }
        }
        else // authenticated
        {
          if(Auth::user()->country == 0)
          {
            if(Session::has('selected_location'))
            {
              Session::set('default_location',Session::get('selected_location'));
            }
            else
            {
              if(in_array($client_location, $list_country))
              {
                Session::set('default_location',strtolower($client_location));
                Session::set('selected_location',strtolower($client_location));
              }
              else
              {
                Session::set('default_location','ALL');
                Session::set('selected_location','ALL');
              }
            }
          }
          else
          { //if country found
            $get_user_country = Country::where('id',Auth::user()->country)->where('status',1)->first();
            if($get_user_country)
            {
              if(Session::has('selected_location'))
              {
                Session::set('default_location',Session::get('selected_location'));
              }
              else
              {
                if(in_array($get_user_country->countryCode, $list_country))
                {
                  Session::set('default_location',strtolower($get_user_country->countryCode));
                  Session::set('selected_location',strtolower($get_user_country->countryCode));
                }
                else
                {
                  Session::set('default_location','ALL');
                  Session::set('selected_location','ALL');
                }
              }
            }
            else
            {
              if(Session::has('selected_location'))
              {
                Session::set('default_location',Session::get('selected_location'));
              }
              else
              {
                if(in_array($client_location, $list_country))
                {
                  Session::set('default_location',strtolower($client_location));
                  Session::set('selected_location',strtolower($client_location));
                }
                else
                {
                  Session::set('default_location','ALL');
                  Session::set('selected_location','ALL');
                }
              }
            }
          }
        }

      if($result == 404){
        return abort(404, '404: Ad not found.');
      }else if($result == 500){
        return abort(404, '404: This ad is either blocked / spam / auction ended or something is wrong.');
      }
      // else if($result == 4){
      //   return abort(404,'404: Auction Ended');
      // }else if($result == 6)


      $this->data['ads_id'] = $result['ads_id'];
      $this->data['getads_info'] = $result['getads_info'];
      // dd($this->data['getads_info']);


      // dd($result['getads_info']);
      $this->data['check_rated_ad']       = $result['check_rated_ad'];
      $this->data['ads_photos']           = $result['ads_photos'];
      $this->data['breadcrumb_container'] = $result['breadcrumb_container'];
      $this->data['primary_photos']       = $result['primary_photos'];
      $this->data['thumbnails']           = $result['thumbnails'];
      $this->data['thumbnails_set2']      = $result['thumbnails_set2'];
      $this->data['thumbnails_set3']      = $result['thumbnails_set3'];
      $this->data['vendor_ads']           = $result['vendor_ads'];
      $this->data['featured_ads']         = $result['featured_ads'];
      $this->data['related_ads_mobile']   = $result['related_ads_mobile'];
      $this->data['related_ads']          = $result['related_ads'];
      $this->data['related_adset2']       = $result['related_adset2'];
      $this->data['custom_attributes']    = $result['custom_attributes'];
      $this->data['comments']             = $result['comments'];
      $this->data['get_average_rate']     = $result['get_average_rate'];
      $this->data['watch_item']           = $result['watch_item'];
      $this->data['form']                 = $result['form'];
      $this->data['modal_related_popups'] = $result['modal_related_popups'];
      $this->data['modal_related_popups_duration'] = $result['modal_related_popups_duration'];
      $this->data['get_bidders']          = $result['get_bidders'];
      $this->data['get_all_bidders']      = $result['get_all_bidders'];
      $this->data['user_id']              = $result['user_id'];
      $this->data['get_ratings_count']    = $result['get_ratings_count'];
      $this->data['prev_url']    = $result['prev_url'];
      // $this->data['seo_img_title'] = 
        
     
   

      if (Auth::check()) {
        $user_id = Auth::user()->id;       
      } else {
        $user_id = null;
      }

      $this->data['get_winner_bidder'] = AuctionBidders::where('ads_id','=',$result['ads_id'])->where('bidder_status','=','1')->first();
       if ($this->data['getads_info']->status == '3') {
            if ($this->data['getads_info']->user_id != $user_id) {
                if ($this->data['get_winner_bidder']->user_id != $user_id) {
                   return Redirect::to('/');
                }
            }
       }
        if(Auth::check()){
           $check = UserAdViews::where('user_id',$user_id)->where('ad_id',$result['ads_id'])->first();
           if(is_null($check)){
            $row = new UserAdViews;
            $row->user_id = $user_id;
            $row->ad_id= $result['ads_id'];
            $row->save();
           }
        }
          

       $count_ads =  Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_bids=  Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);  
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      // $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s'); 
      $this->data['ads']          = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories']   = Category::where('status',1)->get();  
      $this->data['countries']    = Country::all();
      $this->data['usertypes']    = Usertypes::all();
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })
                                        ->orderBy('user_notifications_header.created_at', 'desc')->get();
     // $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
     // $this->data['banner_placement_bottom'] = BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);
     // $get_banner_placement_right = BannerAdvertisementSubscriptions::where('page',6)->where('banner_status','=',4)->where('placement','=',2)->get();

     //  if (count($get_banner_placement_right) > 0) {
     //     $this->data['banner_placement_right_status'] = 1;
     //     $this->data['banner_placement_right'] = $get_banner_placement_right->random(1);
     //  }else{
     //     $this->data['banner_placement_right_status'] = 0;
     //  }


      if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 

      $listingBanners = BannerAdvertisementSubscriptions::where('page',3)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               // ->where('category','=',$id)
                                                              ->where('status',1)
                                                              ->lists('placement_id')
                                                              ->toArray();
    
     $listingBannerIds = BannerAdvertisementSubscriptions::where('page',3)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              // ->where('category','=',$id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',3)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($listingBanners))
                                                               ->lists('id')
                                                               ->toArray();
                                                           

    
      $merge_array = array_merge(array_filter($listingBanners),array_filter($getBannerDefaultsPerCountry));

      if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
          
        

      if(!is_null($listingBannerIds)){

              foreach ($listingBannerIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 4;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 4;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
      

      $this->data['banner_placement_right'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',4)
                                                                    ->orderBy('order_no','asc')
                                                                    // ->orderByRaw('RAND()')
                                                                    ->first();


      $this->data['banner_placement_middle'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',4)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();


      if($result['getads_info']->id) {
        $this->data['feature_status'] = AdFeatureTransactionHistory::where('ad_id', $result['getads_info']->id)->pluck('status');
      }
                                                            
      return view('ads.view',$this->data);
  }

 
public function doFetchInfoPublicAds($country_code,$ad_type_slug,$parent_category_slug,$category_slug,$slug){

  //get user
      $row = Advertisement::where('country_code','=',$country_code)
                          ->where('ad_type_slug','=',$ad_type_slug)
                          ->where('parent_category_slug','=',$parent_category_slug)
                          ->where('category_slug','=',$category_slug)
                          ->where('slug','=',$slug)
                          ->first();
                          
      if(Auth::check())
      {
        $check_pending_comments = UserAdComments::where('vendor_id',Auth::user()->id)->where('status',2)->first();
        if($check_pending_comments)
        {
          $this->data['has_pending_comments'] = 1;
        }
        else
        {
          $this->data['has_pending_comments'] = 0;
        }
        $check_pending_comments_replies = UserAdCommentReplies::where('user_id',Auth::user()->id)->where('status',2)->first();

        if($check_pending_comments_replies)
        {
          $this->data['has_pending_comments_replies'] = 1;
        }
        else
        {
          $this->data['has_pending_comments_replies'] = 0;
        }
      }                    
      


      if(is_null($row)){
        return 404; //not found
      }else if($row->status != 1){
        if($row->status == 3 && $row->ads_type_id == 1){
          return 500;  
        }
         //disabled ads
      }
      $find_visit = AdVisit::where('ad_id',$row->id)->where('ip',Request::ip())->first();
      if(is_null($find_visit))
      {
        $page_visit = new AdVisit;
        $page_visit->ad_id = $row->id;
        $page_visit->ip = Request::ip();
        $page_visit->save();
      }
      
      $cat = $row->category_id;
      $id = $row->id;
      // dd($row->user_id);

      $get_all_rate = AdRating::where('ad_id','=',$id)->avg('rate');
      if(!is_null($get_all_rate)){
        $get_average_rate = $get_all_rate;
      }else{
        $get_average_rate = 0;
      }

      $user_id = $row->user_id;

       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

     $this->data['comments_count'] = UserAdComments::where('status', 1)->where('ad_id', $id)->count();


     $this->data['promotion_popups'] = PromotionPopups::where(function($query) use ($row){
                                               $query->where('cat_id','=',$row->category_id)
                                                   ->orWhere('cat_id','=',$row->sub_category_id)
                                                   ->orWhere('cat_id','=',$row->parent_category);
                                       })
                                    ->where('status',1)
                                    ->get();
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      // $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
      //                                               $query->where('reciever_id','=',$user_id);
      //                                               })->where(function($status){
      //                                                  $status->where('read_status','=',1);
      //                                               })->count();
       $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
                                                    
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      
      $getads_info = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.feature','advertisements.bid_start_amount','advertisements.minimum_allowed_bid','advertisements.status','advertisements.contact_info','advertisements.parent_category','advertisements.category_id','advertisements.sub_category_id', 'advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 'advertisements.city_id', 'advertisements.listing',
                                        'users.name','users.online_status', 'countries.countryName', 'users.alias','country_cities.name as city', 'users.email', 'users.mobile', 'advertisements.address', 'users.telephone', 'users.website','users.description as user_description', 'users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo', 'users.usertype_id',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $id);                                       
                                        })->first();
                                      // dd($getads_info);

      $primary_photos = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.ads_id', '=', $id) 
                                                  ->where('advertisements_photo.primary', '=', 1);                                
                                        })->orderBy('advertisements_photo.id', 'asc')->first(); 

      $ads_photos = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.ads_id', '=', $id) 
                                                  ->where('advertisements_photo.primary', '=', 0)                                
                                            ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->orderBy('advertisements_photo.id', 'asc')->get(); 

      $thumbnails = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->orderBy('advertisements_photo.id', 'asc')->get(); 

      $thumbnails_set2 = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->orderBy('advertisements_photo.id', 'asc')->skip(4)->take(4)->get(); 

      $thumbnails_set3 = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->skip(8)->take(4)->get(); 

      $ads_id = $getads_info->id;
      $user_id = $row->user_id;
      $vendor_ads = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','usertype_id','countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($user_id, $ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.id', '!=', $ads_id)
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(3)->get(); 
          // dd($vendor_ads);                          
        $featured_ads  = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.usertype_id', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($cat){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id',$cat)
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(4)->get();

        $related_popups = PromotionPopups::where('cat_id','=',$row->category_id)->first();
        $modal_related_popups = '';
        if(!is_null($related_popups)){
           $modal_related_popups .= '<div class="modal" id="modal-pop_up_message" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
                      <div class="modal-dialog modal-md">
                        <div class="modal-content borderZero">
                            <div id="load-form" class="loading-pane hide">
                              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                            </div>
                          <div class="modal-header modal-warning">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-info-circle"></i> Great Deals</h4>
                          </div>
                          <div class="modal-body">
                             <img src='.URL::route('uploads', array(), false).'/'.$related_popups->photo.' height="100%" width="100%">
                          </div> 
                        </div>
                      </div>
                    </div>';
                    $modal_related_popups_duration = $related_popups->duration;
        }else{
          $modal_related_popups .= '';
          $modal_related_popups_duration = 0;
        }

        $cat_unique_id = $getads_info->cat_unique_id;
        // $sub_category_id = $getads_info->sub_category_id;
        $related_ads_mobile  = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements.feature','advertisements_photo.photo', 
                                         'users.name','users.usertype_id','users.alias', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                       
                                       ->where(function($query) use ($cat_unique_id, $ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                
                                                   ->where('advertisements.id', '!=', $ads_id)                                                                        
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
        $related_ads  = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements.feature','advertisements_photo.photo', 
                                         'users.name','users.usertype_id','users.alias','advertisements.feature', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')
                                       // ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ( $ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.id', '!=', $ads_id)                                                                  
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
                                   
        $related_adset2  = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description','advertisements.feature', 'advertisements_photo.photo', 
                                         'users.name','users.usertype_id', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                       
                                       ->where(function($query) use ($ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.id', '!=', $ads_id)                                     
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get();

            //Check ad if already on watch list
            if(Auth::check()){
                $get_user_id = Auth::user()->id;
            } else {
                $get_user_id = null;
            }
            $watch_item  = AdvertisementWatchlists::where('user_id', '=', $get_user_id)->where('ads_id', '=', $getads_info->id)->where('status', '=', 1)->first(); 

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$id)->where('status','=',1)->get();
           $custom_attributes = "";
               foreach ($get_dropdown as $key => $field) {
                  $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();

                  foreach ($get_attri_info as $key => $field_attri_info) {
                     $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->where('id','=',$field->attri_value_id)->first();
                   if (!is_null($get_attri_values)) {
                     $custom_attributes .=  '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 noleftPadding">'.
                                              '<div class="form-group nobottomMargin">'.
                                                '<p class="col-sm-12 normalText grayText nobottomMargin topPadding" for="title">'.$field_attri_info->name.': <span>'.$get_attri_values->name.'</span></p>'.
                                                // '<div class="col-sm-8">'.
                                                //   '<h5 class="normalText grayText" for="title">'.$get_attri_values->name.'</h5>'.
                                                // '</div>'.
                                              '</div>'.
                                                '</div>';   
                   }else{
                      $custom_attributes .= '';
                   }
                                     
                }           
              }
  
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$id)->where('status','=',1)->get();
           
             if ($get_textbox) {
              foreach ($get_textbox as $key => $field) {
                $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
                 foreach ($get_attri_info as $key => $field_attri_info) {
                  if($field->attri_value != ""){
                   if (!is_null($field)) {
                      $custom_attributes .=  '<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 noleftPadding">'.
                                              '<div class="form-group nobottomMargin">'.
                                                '<p class="col-sm-12 normalText grayText nobottomMargin topPadding" for="title">'.$field_attri_info->name.':<span>'.$field->attri_value.'</span></p>'.
                                                // '<div class="col-sm-8">'.
                                                //   '<h5 class="normalText grayText" for="title">'.$get_attri_values->name.'</h5>'.
                                                // '</div>'.
                                              '</div>'.
                                          '</div>';      
                       }else{
                          $custom_attributes .= '';
                       }
                   }else{
                        $custom_attributes .= '';
                   }
                 
                 }           
               }
           }

           $form="";

      if(Auth::check()){
         $user_ad_id = Auth::user()->id;
        if(Auth::user()->status==1 ){
         
          $check_rates = AdRating::where('ad_id','=',$ads_id)->where('user_id','=',$user_ad_id)->first();
          $check_ad = Advertisement::where('user_id','=',$user_ad_id)->Where('id','=',$id)->first();
            $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body borderBottom bottomPaddingB noPadding" id="notice-pane">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.URL::route('uploads', array(), false).'/'.Auth::user()->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'ad/comment/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="user-ad-comment" name="comment"></textarea></p>'.
                              '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                              '<input type="hidden" name="vendor_id" value="'.Auth::user()->id.'">'.
                          '</div>'.
                          '<span class="pull-right topPadding commentXs" id="review-product-pane">'.
                            '<input type="hidden" name="ad_id" value="'.$ads_id.'">'.
                            '<span id="rateYo" class="mediumText adRating pull-left '.( Auth::user()->id  == $id ? "hide" : "").''.( !is_null($check_ad) ? 'hide' : '').'"></span>'.
                            '<button type="button" id="rate_product" class="btn btn-sm rateButton borderZero btn-rate_product '.( Auth::user()->id  == $id ? 'hide' : '').' '.( !is_null($check_ad) ? 'hide' : '').'"><i class="fa fa-star"></i> Rate <span class="hidden-xs">Product</span></button>'.
                            '<button type="submit" class="btn btn-sm blueButton borderZero btn-save_comment leftMarginB"><i class="fa fa-comments"></i> <span class="hidden-xs">Leave</span> Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
        }else if(Auth::user()->status == 2){

          $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body borderBottom bottomPaddingB noPadding" id="notice-pane">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.URL::route('uploads', array(), false).'/default_image.png" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'ad/comment/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="user-ad-comment" name="comment"></textarea></p>'.
                          '</div>'.
                          '<span class="pull-right topPadding commentXs" id="review-product-pane">'.
                            '<span id="rateYo" class="mediumText adRating pull-left"></span>'.
                            '<button type="button" href="#" class="btn btn-sm rateButton borderZero" disabled><i class="fa fa-star"></i> Rate <span class="hidden-xs">Product</span></button>'.
                            '<button type="button" href="#" disabled class="btn btn-sm blueButton borderZero leftMarginB"><i class="fa fa-comments"></i> <span class="hidden-xs">Leave</span> Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
        }else if(Auth::user()->status == 3){

          $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body borderBottom bottomPaddingB noPadding" id="notice-pane">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.URL::route('uploads', array(), false).'/default_image.png" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'ad/comment/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="user-ad-comment" name="comment"></textarea></p>'.
                          '</div>'.
                          '<span class="pull-right topPadding commentXs" id="review-product-pane">'.
                            '<span id="rateYo" class="mediumText adRating pull-left"></span>'.
                            '<button type="button" href="#" class="btn btn-sm rateButton borderZero" disabled><i class="fa fa-star"></i> Rate <span class="hidden-xs">Product</span></button>'.
                            '<button type="button" href="#" disabled class="btn btn-sm blueButton borderZero leftMarginB"><i class="fa fa-comments"></i> <span class="hidden-xs">Leave</span> Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
        }
      }else{
          $user_ad_id = null;

$form .= '<div class="col-md-12 noPadding">'.
        '<div class="panel panel-default borderZero removeBorder noMargin">'.
         '<div class="panel-body borderBottom bottomPaddingB noPadding" id="notice-pane">'.
            '<div class="commentBlock">'.
              '<div class="commenterImage">'.
                 '<img src="'.URL::route('uploads', array(), false).'/default_image.png" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                '</div>'.
                '<div class="commentText">'.
                Form::open(array('url' => 'ad/comment/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="user-ad-comment" name="comment"></textarea></p>'.
                '</div>'.
                '<span class="pull-right topPadding commentXs" id="review-product-pane">'.
                  '<span id="rateYo" class="mediumText adRating pull-left"></span>'.
                  '<button type="button" href="#" data-toggle="modal" data-target="#modal-login"  id="rate_product" class="btn btn-sm rateButton borderZero btn-rate_product"><i class="fa fa-star"></i> Rate <span class="hidden-xs">Product</span></button>'.
                  '<button type="button" href="#" data-toggle="modal" data-target="#modal-login" class="btn btn-sm blueButton borderZero btn-save_comment leftMarginB"><i class="fa fa-comments"></i> <span class="hidden-xs">Leave</span> Comment</button>'.
                '</span>'.
                 Form::close().
            '</div>'.
          '</div>'.
        '</div>'.
     '</div>';
      }

        $stars = "";
        $get_comments = UserAdComments::with('getUserAdCommentReplies')->where('user_ad_comments.ad_id','=',$id)->orderBy('user_ad_comments.created_at','desc')->where('status',1)->get();

           $comments = "";
        $ctr=1;
        foreach ($get_comments as $key => $comment_info) {
                  $get_vendor_info = User::find($comment_info->vendor_id);
                   $get_ad_rating = AdRating::where('user_id','=',$comment_info->vendor_id)->where('ad_id','=',$ads_id)->where('id',$comment_info->rating_id)->first();
                        if($comment_info->listing_type == 2){
                           if(!is_null($get_ad_rating)){
                            if($get_ad_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></span>';
                            }
                            if($get_ad_rating->rate == 4){
                              $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i></span>';   
                            }
                            if($get_ad_rating->rate == 3){
                             $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_ad_rating->rate == 2){
                             $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                            if($get_ad_rating->rate == 1){
                             $stars = '<span class="mediumText"><i class="fa fa-star rateStars"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i><i class="fa fa-star rateStarsB"></i></span>';  
                            }
                          }else{
                            $stars = "";
                          }
                        }else{
                             $stars = "";

                        }
                       
          $comments .= '<div id="reply-pane" class="'.($ctr % 2  == 0 ? 'panel panel-default bgGray borderZero nobottomMargin borderBottom removeBorder' : 'panel panel-default bgWhite nobottomMargin  borderZero removeBorder').' ">'.
              '<div class="panel-body topPaddingD"  id="ad-replies-container">'.
                '<div class="commentBlock">'.
                  '<div class="commenterImage">'.
                     '<img src="'.URL::route('uploads', array(), false).'/'.$get_vendor_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                    '</div>'.
                    '<div class="commentText nobottomPadding">'.
                    '<div class="panelTitle nobottomPadding">'.$get_vendor_info->name.' '.$stars.'<span class="lightgrayText text-capitalize topPadding normalWeight normalText pull-right">'.date("F j, Y", strtotime($comment_info->created_at)).'</span></div>'.
                    '<p class="normalText nobottomMargin nobottomPadding">'.htmlentities($comment_info->comment).'</p>'.
                    '<div class="subText">';

      if(Auth::check()){

        $comments .= '<a '.($get_vendor_info->id == $user_ad_id ? '': 'data-name='.$get_vendor_info->name ).' id="'.($user_ad_id == null ? 'button-reply':'show').'" role="button" class="blueText normalText '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-comments"></i> Reply</a>'.
                      '<span style="cursor: pointer;" data-receiver="'.$get_vendor_info->id.'" data-message="Hello '.$get_vendor_info->name.'" class="direct-msg leftMarginB redText normalText '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>'.
                      //'<span class="leftMarginB redText normalText '.($user_ad_id == null ? 'hide':'').'"></span>'.
                      
                    '</div>';
                    
      }else{

         $comments .= '<a role="button" data-toggle="modal" data-target="#modal-login" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>'.
                      '<a role="button" data-toggle="modal" data-target="#modal-login" class="blueText normalText"><span class="leftMarginB redText normalText><i class="fa fa-paper-plane"></i> Direct Message</span></a>'.
                      //'<span class="leftMarginB redText normalText '.($user_ad_id == null ? 'hide':'').'"></span>'.
                      
                    '</div>';

      }
              
                
                    // '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'
        if(count($comment_info->getUserAdCommentReplies) > 0){
           foreach ($comment_info->getUserAdCommentReplies as $key => $reply_info) {
                if($reply_info->status == 1){
                          $get_user_info = User::find($reply_info->user_id);

        $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' borderZero removeBorder replyBlock bordertopLight nobottomMargin topMarginB">
                      <div class="panel-body topPaddingB noPaddingXs norightPadding nobottomPadding bordertopLight">
                        <div class="commentBlock">
                          <div class="commenterImage">
                             <img src="'.URL::route('uploads', array(), false).'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                            </div>
                            <div class="commentText">
                            <div class="panelTitle nobottomPadding">'.$get_user_info->name.'<span class="lightgrayText topPadding text-capitalize normalText normalWeight pull-right">'.date("F j, Y", strtotime($reply_info->created_at)).'</span></div>
                            <p class="normalText nobottomMargin nobottomPadding">'.htmlentities($reply_info->content).'</p>
                            <div class="subText">';

      $comments .= '<a'.($get_user_info->id == $user_ad_id ? '': 'data-name='.$get_user_info->name ).' id="'.($user_ad_id == null ? 'button-reply' : 'show_reply_box').'" role="button" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>
                              <span class="leftMarginB redText normalText '.($reply_info->user_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>
                              
                            </div>';


       $comments .= '<div class="panel panel-default borderZero removeBorder nobottomMargin replyBlock bordertopLight nobottomMargin inputReplyBox hidden">'.
                                  '<div class="panel-body norightPadding nobottomPadding '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').'">'.
                                    '<div class="commentBlock">'.
                                    Form::open(array('url' => 'ad/comment/reply/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'form-sub-reply')).
                                    '<textarea class="form-control borderZero fullSize" rows="2" id="reply-message" name="reply_content"></textarea>'.
                                    '<input type="hidden" name="comment_id" value="'.$comment_info->id.'">'.
                                    '<input type="hidden" name="user_id" value="'.$user_ad_id.'">'.
                                    '</div>'.
                                    '<span class="pull-right">'.
                                      '<button class="btn normalText blueButton submitReply borderZero topMargin"> Submit</button>'.
                                    '</span>'.
                                    Form::close().
                                  '</div>'.
                              '</div>                       
                            </div>
                        </div>
                      </div>
                  </div>';                
                 
              }
            }
           
              
            }

           $comments .= '<div class="panel panel-default borderZero removeBorder replyBlock bordertopLight nobottomMargin inputReply hidden">'.
                  '<div class="panel-body norightPadding '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomPadding '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'">'.
                    '<div class="commentBlock">'.
                    Form::open(array('url' => 'ad/comment/reply/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'form-main-reply')).
                    '<textarea class="form-control borderZero fullSize" rows="2" id="main_comment_reply" name="reply_content"></textarea>'.
                    '<input type="hidden" name="comment_id" value="'.$comment_info->id.'">'.
                    '<input type="hidden" name="user_id" value="'.$user_ad_id.'">'.
                    '</div>'.
                    '<span class="pull-right">'.
                      '<button class="btn normalText blueButton submitReply borderZero topMargin parentSubmit"> Submit</button>'.
                    '</span>'.
                    Form::close().
                  '</div>'.
              '</div>'.
                '</div>'.
            '</div>'.
          '</div>'.
        '</div>';
        $ctr++;
         } 
    if(Auth::check()){
      $user_ad_id = Auth::user()->id;
      $check_rated_ad = AdRating::where('ad_id','=',$ads_id)->where('user_id','=',$user_ad_id)->first();
    }else{
      $check_rated_ad = null;
    }     
    $get_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.bid')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                  })->orderBy('auction_bidders.created_at', 'desc')->take(3)->get();

    $get_all_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.alias', 'users.username', 'users.name', 'users.usertype_id', 'auction_bidders.bid', 'auction_bidders.created_at')
                                 ->where(function($query) use ($ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                  })->orderBy('auction_bidders.created_at', 'desc')->take(3)->get();

                                  
    $get_ratings_count = AdRating::where('ad_id','=',$id)->count();
    $city_slug = City::where('id', $getads_info->city_id)->pluck('slug');
    $get_param = ($getads_info->listing == 1 ? '' : ($getads_info->ads_type_id == 1 ? '?listing=buy-and-sell' : '?listing=auctions'));
         
    $breadcrumb_container = "";
        $breadcrumb_container .= '<li class="active"><a href="'.url('/').'" class="grayTextB">Home</a></li>';
      if($getads_info->ads_type_id == 1){
        $breadcrumb_container.= '<li class="active"><a href="'.url($getads_info->country_code.'/'.$city_slug.'/buy-and-sell').'" class="grayTextB">Ads</a></li>';
      }else{
        $breadcrumb_container.= '<li class="active"><a href="'.url($getads_info->country_code.'/'.$city_slug.'/auctions').'" class="grayTextB">Auction</a></li>';
      }
//   dd($getads_info);
  
//   dd($city_slug);
      if(!is_null($getads_info->parent_category)){
        $get_parent_category = Category::where('unique_id',$getads_info->parent_category)->first();
        $breadcrumb_container.= '<li class="active"><a href="'.url($getads_info->country_code.'/'.$city_slug.'/'.$get_parent_category->slug.$get_param).'" class="grayTextB">'.$get_parent_category->name.'</a></li>';
      }else{
        $breadcrumb_container.= '';
      }

     if(!is_null($getads_info->category_id)){
        $get_category = SubCategory::where('unique_id',$getads_info->category_id)->first();
        // dd($get_category);
        if($get_category){
        $breadcrumb_container.= '<li class="active"><a href="'.url($getads_info->country_code.'/'.$city_slug.'/'.$get_parent_category->slug.'/'.$get_category->slug.$get_param).'" class="grayTextB">'.$get_category->name.'</a></li>';
          }else{
          $breadcrumb_container.= '';
          }
      }else{
        $breadcrumb_container.= '';
      }

//       if(!is_null($getads_info->sub_category_id)){
//         $get_sub_cat = SubCategoriesTwo::where('unique_id',$getads_info->sub_category_id)->first();
//         if($get_sub_cat){
//         $breadcrumb_container.= '<li class="active"><a href="'.url('category/list/'.$getads_info->ads_type_id.'/'.$get_sub_cat->slug).'" class="grayTextB">'.$get_sub_cat->name.'</a></li>';
//         }else{
//         $breadcrumb_container.= '';
//       }
//       }else{
//         $breadcrumb_container.= '';
//       }
      // if(!is_null($getads_info->title)){
      //  $breadcrumb_container.= '<li class="active"><a href="" class="grayTextB">'.$getads_info->title.'</a></li>';
      // }
       if (Request::ajax()) {
          $result['getads_info']         = $getads_info->toArray();
          $result['ads_photos']          = $ads_photos->toArray();
          $result['primary_photos']      = $primary_photos->toArray();
          $result['thumbnails']          = $thumbnails->toArray();
          $result['thumbnails_set2']     = $thumbnails_set2->toArray();
          $result['thumbnails_set3']     = $thumbnails_set3->toArray();
          $result['vendor_ads']          = $vendor_ads->toArray();
          $result['related_ads_mobile']  = $related_ads_mobile->toArray();
          $result['related_ads']         = $related_ads->toArray();
          $result['related_adset2']      = $related_adset2->toArray();
          $result['featured_ads']        = $featured_ads->toArray();
          // $result['watch_item']        = $watch_item->toArray();
          return Response::json($result);
       } else {
          $result['getads_info']         = $getads_info;
          $result['ads_photos']          = $ads_photos;
          $result['primary_photos']      = $primary_photos;
          $result['thumbnails']          = $thumbnails;
          $result['thumbnails_set2']     = $thumbnails_set2;
          $result['thumbnails_set3']     = $thumbnails_set3;
          $result['vendor_ads']          = $vendor_ads;
          $result['featured_ads']        = $featured_ads;
          $result['related_ads_mobile']  = $related_ads_mobile;
          $result['related_ads']         = $related_ads;
          $result['related_adset2']      = $related_adset2;
          $result['custom_attributes']   = $custom_attributes;
          $result['watch_item']          = $watch_item;
          $result['modal_related_popups']  = $modal_related_popups;
          $result['modal_related_popups_duration']  = $modal_related_popups_duration;        
          //$result['get_related_banners'] =  $get_related_banners;
          $result['form']   = $form;
          $result['comments']   = $comments;
          $result['breadcrumb_container'] =  $breadcrumb_container;
          $result['check_rated_ad'] =  $check_rated_ad;
          $result['user_id']   = $user_id;
          $result['get_average_rate'] = $get_average_rate;
          $result['get_bidders'] = $get_bidders;
          $result['get_all_bidders'] = $get_all_bidders;
          $result['get_ratings_count'] = $get_ratings_count;
          $result['ads_id'] = $row->id;
          $result['prev_url'] = URL::previous();
//          dd($getads_info);
          return $result;
      }                                      

}

 public function removeAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }


    public function disableadsList() {
     $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Disabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    
    //  public function disableAdsList() {
    //   $row = Advertisement::find(Request::input('id'));
    //   if(!is_null($row)){
    //     $row->status = "2";
    //     $row->save();
    //     return Response::json(['body' => 'Ad Successfully Disabled']);      
    //   }else{
    //     return Response::json(['error' => ['error on finding the user']]); 
    //   }
    // }
 // public function changePopupStatus(){
 //     $row = PromotionPopupStatus::where('popup_id','=',Request::input('popup_id'))->where('user_id','=',Request::input('user_id'))->first();
 //    if(is_null($row)){
 //       $row = new PromotionPopupStatus;
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = Request::input('popup_status');
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }
 //    else if($row->status == 1){
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = 2;
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }
 //    else if($row->status == 2){
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = 1;
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }

 // }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

 public function disableAds() {
      $id = Request::input('id');
      $toast = '';

        if(Request::input('id')) {
            $row = Advertisement::find($id);
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            $new = false;
        }
        if (Auth::check()) {
           $user_id = Auth::user()->id;
              if ($user_id != $row->user_id) {
               return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);      
              }
        }
   
          $href = 'http://yakolak.xerolabs.co/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug;
     if ($row->ads_type_id == '2') {
         $current_time = Carbon::now()->format('Y-m-d H:i:s');    
         $date = date_create($current_time);               
         $sub_ad_expiration = date_sub($date, date_interval_create_from_date_string('5 mins'));
         $row->ad_expiration = $sub_ad_expiration;
         $row->status = "3";
         $row->save();
         $get_bidder = AuctionBidders::where('ads_id','=',$id)->orderBy('created_at', 'desc')->where('status', 1)->first();
         
         if (!(is_null($get_bidder))) {
         $get_bidder->bidder_status = '1';
         $get_bidder->save();

         //notify bidder on bid won  
          $notiheader = new UserNotificationsHeader;
          $notiheader->title       ='Bidding';
          $notiheader->read_status = 1;
          $notiheader->reciever_id = $get_bidder->user_id;
          $notiheader->photo_id = $get_bidder->user_id;
          $notiheader->save();

          $noti = new UserNotifications;
          $noti->notification_id  = $notiheader->id;
          $noti->type ="Bidding";
          $noti->status = 1;
          $noti->save();
          $noti->message     = '<a class="grayText btn-change-noti-status" data-id="'.$noti->id.'" href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'">You won the bidding on the auction '.$row->title.'
                                <div class="normalText grayText topPadding"></div>
                                </a>'; 
          $noti->save();
          
        $bidder_info = User::find($get_bidder->user_id);

        } else { 
          $get_bidder_id = null;

        }
          //auction owner message
          $msgheaderowner = new UserMessagesHeader;
          $msgheaderowner->title       ='[System Message]';
          $msgheaderowner->sender_id   = 2;
          $msgheaderowner->read_status = 1;
          $msgheaderowner->reciever_id = $row->user_id;
          $msgheaderowner->save();

          $msgowner = new UserMessages;
          $msgowner->message_id  = $msgheaderowner->id;
          $msgowner->message = '<a href="'.$href.'">Your auction ad '.$row->title.' has ended. '.($get_bidder != null?''.$bidder_info->name.' won the bidding!':'No one placed a bid.</a>');
          $msgowner->sender_id   = 2;
          $msgowner->save();
          $toast = 'Auction Successfully Ended';
     } else {  
         $row->status = "3";
         $row->save();
         $toast = 'Ad Successfully Disabled';
    }

        return Response::json(['body' => $toast]);      

  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

 public function enableAds() {
      $row = Advertisement::find(Request::input('id'));
      //dd($row);
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Enabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function editor()
    {
       return View::make('editor.index');
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function sub2editor()
    {
       return View::make('editor.sub1.sub2.index');
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function formeditor()
    {
       return View::make('admin.ads-management.front-end-users.form');
    }
    
     /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function getCustomAttributes(){
       $cat_id = Request::input('id');

       // $id = explode(" ",$cat_id);
       // $get_cust_id = CustomAttributes::find($id[0]);
         $get_sub_attri_id = SubAttributes::where('attri_id','=',$cat_id)->get();
                     $rows = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'">
                      <option value="" class="hide">Selefgfafd</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9">'; 
                      $rows .= '<input type="text" name="text_field[]" class="form-control borderZero">';
                      $rows .='</div></div>';
                      $rows .='<input type="hidden" name="text_field_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .='</select></div>';
                      $rows .='<input type="hidden" name="dropdown_field_id[]" value="'.$field->id.'"></div>';
                 }           
          }
      // $result['category_id'] = $get_category_id;
      $result['rows'] = $rows;
      return Response::json($result);
    }
    
    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */    

  // public function getAuctionTypes(){
  //     $ads_type_id = Request::input('id');

  //     $rows = '';
  //     if ($ads_type_id == 2) {

  //      $rows .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
  //                   '<div class="form-group">'.
  //                     '<h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>'.
  //                        '<div class="col-sm-9">'.
  //                          '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>'.
  //                       '<div class="col-sm-9">'.
  //                             '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>'.
  //                       '<div class="col-sm-9">'.
  //                         '<input type="number" class="form-control borderZero" id="register4-email" name="minimum_allowed_amount" placeholder="" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
  //                       '<div class="col-sm-9">'.
  //                         '<div class="col-sm-6 noleftPadding">'.
  //                         '<div class="form-group">'.
  //                           '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
  //                           '<div class="col-sm-8">'.
  //                             '<input type="number" class="form-control borderZero" id="ads-bid_duration_start" name="bid_duration_start" placeholder="" required="required">'.
  //                             '</div>'.
  //                           '</div>'.
  //                         '</div>'.
  //                         '<div class="col-sm-6 norightPadding">'.
  //                           '<div class="form-group">'.
  //                             '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
  //                             '<div class="col-sm-8"> '.        
                  
  //                          '<select id="account-company_city" name="bid_duration_dropdown_id" class="form-control borderZero inputBox fullSize" required="required">' .
  //                   '<option class="hide">Select</option>';

  //                   $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'desc')->get(); 
  //                   // dd($get_bid_duration_dropwdown);
  //                   foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
  //                             $rows .= '<option value="' . $bid_field->id . '" ' . ($bid_field->id ? 'selected' : '') .'>'. $bid_field->value . '</option>';
  //                            }

  //                $rows .= '</select></div>'.
  //                           '</div>
  //                         </div>
  //                       </div>
  //                     </div>';
  //     }
          
  //     return Response::json($rows);
  //   }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function rateAd(){

    $row = AdRating::where('ad_id','=',Request::input('id'))->where('user_id','=',Auth::user()->id)->first();
      if(is_null($row)){
        $row = new AdRating;
        $row->rate = Request::input('rate');
        $row->ad_id = Request::input('id');
        $row->user_id = Auth::user()->id;
        $row->status = 2;
        $row->save();
        $get_vendor_info = Advertisement::find(Request::input('id'));
          $check_ad_comment_status = UserAdComments::where('vendor_id',Auth::user()->id)->where('status',1)->first();
       
          if(!is_null($get_vendor_info)){
              $save_review = new UserAdComments;
              $save_review->comment = Request::input('review');
              $save_review->user_id =$get_vendor_info->user_id;
              $save_review->rating_id =  $row->id;
              $save_review->vendor_id =  Auth::user()->id;
              $save_review->listing_type =  2;
              if(count($check_ad_comment_status) >0){
                $save_review->status = 1;
                $row->status = 1;
                $row->save();
              }
              $save_review->ad_id = Request::input('id');
              $save_review->save();
                $get_advertisement_type = Advertisement::find($save_review->ad_id);
                if(!is_null($get_advertisement_type)){
                  $save_review->ad_type = $get_advertisement_type->ads_type_id;
                  $save_review->save();
                }
          }
       
        if(Request::input('rate') >= 3){
          $get_ad_owner = Advertisement::find($row->ad_id);
          $update_ad_owner_point = User::find($get_ad_owner->user_id);
          $update_ad_owner_point->points = $update_ad_owner_point->points + 1;
          $update_ad_owner_point->save();
        }  
        $get_rater = User::where('id', '=', $row->user_id)->first();
        $rater     = $get_rater->name;
        $ads_id    = Request::input('id');
        $get_ad    = Advertisement::join('users','users.id', '=', 'advertisements.user_id')
                              ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description',  
                                'users.name', 'users.email')                          
                              ->where(function($query) use ($ads_id) {
                                      $query->where('advertisements.id', '=', $ads_id)                                                                        
                                            ->where('advertisements.status', '=', '1');                                     
                              })->first();
        $name = $get_ad->name;

        $get_user_info = User::find($get_ad->user_id);

        //Email Trigger review ads
             if (Auth::user()->name) {
                 $name = Auth::user()->name;
             }  else {
              $name = null;
             }

         //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
               if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               }  
              $item_title = $row->title;
              $item_url = url('view').'/'.$get_ad->slug;
              $item_link = url('view').'/'.$get_ad->slug;
              $comment = 'http://www.yakolak.com/ads/view/'. $row->ad_id;
              $rate = $row->rate;

      if ($get_ad->ads_type_id == '1') {
               //email user basic ad rate
                $newsletter = Newsletters::where('id', '=', '4')->where('newsletter_status','=','1')->first();
                  if ($newsletter) {
                                      Mail::send('email.ad_review', ['rate' => $rate,'contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $get_user_info) {
                                          $message->to($get_user_info->email)->subject(''.$newsletter->subject.'');
                                      });
                           }
      }else {
               //email user auction rate
                $newsletter = Newsletters::where('id', '=', '7')->where('newsletter_status','=','1')->first();
                  if ($newsletter) {
                                      Mail::send('email.ad_review', ['rate' => $rate, 'contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $get_user_info) {
                                          $message->to($get_user_info->email)->subject(''.$newsletter->subject.'');
                                      });
                           }

      }
        if(count($check_ad_comment_status) >0){
           return Response::json(['body' => 'Review has been saved']);   
        }else{
           return Response::json(['body' => 'Your comment will be reviewed by the admin']);  
        }    
      }else{
        return Response::json(['error' => ['error']]); 
      }

    }
 
     /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function saveReview(){

      $row = Advertisement::find(Request::input('id'));
      //dd($row);
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Rate has been saved']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

    }
    public function featureAd(){
      $input = Input::all();
      $duration = array_get($input,'duration');
      $payment_method = array_get($input,'payment_method');
      $duration_metric = array_get($input,'duration_metric');
      $cost_points = array_get($input,'cost_points');
      $row = Advertisement::find(array_get($input,'id'));
      $date_now = Carbon::now();
      $featured_expiration = date("Y-m-d H:i:s", strtotime($date_now . "+ $duration  days"));
      if(!is_null($row)){
        $get_user = User::find(Auth::user()->id);
        if(!is_null($get_user)){

          if($cost_points > $get_user->points) {
            return Response::json(['error' => ['Insufficient Points']]); 
          } else {
            $get_user->points =  $get_user->points - $cost_points;
            $get_user->save();
            $row->feature = "2";
            $row->featured_expiration = $featured_expiration;
            $row->save();
          }
        }
        $save_transaction_history = new AdFeatureTransactionHistory;
        $save_transaction_history->ad_id = $row->id;
        $save_transaction_history->user_id = Auth::user()->id;
        $save_transaction_history->cost = $cost_points;
        $save_transaction_history->duration = $duration;
        $save_transaction_history->duration_metric = $duration_metric;
        $save_transaction_history->payment_method = $payment_method;
        $save_transaction_history->save();
        $transaction_no = base64_encode('transaction-'.$save_transaction_history->id);
        $save_transaction_history->transaction_no = $transaction_no;
        $save_transaction_history->save();


        return Response::json(['body' => 'Ad has been featured']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    public function exhangePointChecker(){
      $duration = Request::input('duration');
      $duration_metric = Request::input('duration_metric');
      $payment_method = Request::input('payment_method');
      $get_exchange_point = SettingForExchangePoints::where('id',$duration_metric)->first();
      if(!is_null($get_exchange_point)){
        if($payment_method == 1){
            $result['computed_cost'] = $get_exchange_point->points * $duration;
        }else{
            $result['computed_cost'] = $get_exchange_point->price * $duration;
        }
        if($result['computed_cost'] > Auth::user()->points){
           $result['status'] = 2;
        }else{
           $result['status'] = 1;
        }
          return Response::json($result);
      }else{
        return Response::json(['error' => ['error on finding the id']]); 
      }

    }
     public function unfeatureAd(){
      $row = Advertisement::find(Request::input('id'));
      //dd($row);
      if(!is_null($row)){
        $row->feature = "0";
        $row->save();
        $update_feature_ad_trasact = AdFeatureTransactionHistory::where('ad_id',Request::input('id'))->where('status',1)->first();
        if(!is_null($update_feature_ad_trasact)){
          $update_feature_ad_trasact->status = 2;
          $update_feature_ad_trasact->save();
        }
        return Response::json(['body' => 'Ad has been unfeatured']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function saveAdComment(){
      $input = Input::all();
      $check_ad_comment_status = UserAdComments::where('vendor_id',array_get($input, 'vendor_id'))->where('status',1)->first();
      // dd(count($check_ad_comment_status));
      $row = new UserAdComments;
      $row->user_id   = array_get($input, 'user_id');
      $row->ad_id     = array_get($input, 'ad_id');
      $row->vendor_id = array_get($input, 'vendor_id');
      $row->comment   = array_get($input, 'comment');
      if(count($check_ad_comment_status)>0){
       $row->status = 1;
      }
      $row->save();

      $get_advertisement_type = Advertisement::find($row->ad_id);
      if(!is_null($get_advertisement_type)){
        $row->ad_type = $get_advertisement_type->ads_type_id;
        $row->save();
      }
      if(count($check_ad_comment_status) >0){
         return Response::json(['body' => 'Comment has been saved']);      
      }else{
         return Response::json(['body' => 'Comment will be reviewed by the admin']);        
      }
      
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

     public function saveAdCommentReply(){
      $input = Input::all();
      $change_comment_status  = UserAdCommentReplies::where('user_id',Auth::user()->id)->where('status',1)->first();
      $row = new UserAdCommentReplies;
      $row->comment_id     = array_get($input, 'comment_id');
      $row->user_id     = array_get($input, 'user_id');
      if(count($change_comment_status) > 0){
         $row->status = 1;
      }
      $row->content     = array_get($input, 'reply_content');
      $row->save();

      return Response::json(['body' => 'replied']);      

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function addWatchlist() {
     $row = AdvertisementWatchlists::find(Request::input('id'));
     $id = Request::input('id');
      if(is_null($row)){
        $row = new AdvertisementWatchlists;
        $row->user_id = Auth::user()->id;
        $row->ads_id = $id;
        $row->save();
        return Response::json(['body' => 'Ad Successfully added to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function removeWatchlist() {

     $row = AdvertisementWatchlists::find(Request::input('watchitem_id'));
    // dd($row);
      
      if(!(is_null($row))){
        $row->status = 2;
        $row->save();
        return Response::json(['body' => 'Ad Successfully removed to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function auctionExpired() {
    $input = Input::all();
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','2')->where('status','=', '1')->get();

foreach ($row as $value) {
     if($value->ad_expiration <= $current_time){
        $value->ads_type_id = 1;
        $value->status = 3;
        $value->ad_exp_email_trigger = 0;
        $value->save();

    $get_bidder = AuctionBidders::where('ads_id','=',$value->id)->orderBy('created_at', 'desc')->first();
        if (!(is_null($get_bidder))) {
         $get_bidder->bidder_status = '1';
         $get_bidder->save();

          //bidder message  
          $msgheader = new UserMessagesHeader;
          $msgheader->title       ='Bid Winner';
          $msgheader->sender_id   = 2;
          $msgheader->read_status = 1;
          $msgheader->reciever_id = $get_bidder->user_id;
          $msgheader->save();

          $msg = new UserMessages;
          $msg->message_id  = $msgheader->id;
          $msg->message     = '<a href="' .url('/').'/'.'view/'.$value->slug.'">You have won the bidding on the ad '.$value->title.'.</a>';
          $msg->sender_id   = 2;
          $msg->save(); 

          //notify bidder on bid won  
          $notiheader = new UserNotificationsHeader;
          $notiheader->title       ='Bidding';
          $notiheader->read_status = 1;
          $notiheader->reciever_id = $get_bidder->user_id;
          $notiheader->photo_id = $get_bidder->user_id;
          $notiheader->save();

          $noti = new UserNotifications;
          $noti->notification_id  = $notiheader->id;
          $noti->message     = '<a class="grayText" href=' .url('ads').'/'.'view/'.$value->id.'>You have won the bidding on the ad '.$value->title.'.</a>';
          $noti->type ="Bidding";
          $noti->save(); 

          $bidder_info = User::find($get_bidder->user_id);
        } else { 
          $get_bidder_id = null;

        }

          //auction owner message
          $msgheaderowner = new UserMessagesHeader;
          $msgheaderowner->title       ='[System Message]';
          $msgheaderowner->sender_id   = 2;
          $msgheaderowner->read_status = 1;
          $msgheaderowner->reciever_id = $value->user_id;
          $msgheaderowner->save();

          $msgowner = new UserMessages;
          $msgowner->message_id  = $msgheaderowner->id;
          $msgowner->message     = 'Your '.$value->title.' Auction has been ended. '.($get_bidder != null?'<a href=' .url('/').'/'.($bidder_info->usertype_id == 1 ? 'vendor' : 'company').'/'.$bidder_info->alias.'>'.$bidder_info->name.'</a> won the bidding!':'No one won your auction.');
          $msgowner->sender_id   = 2;
          $msgowner->save(); 

              $user = User::where('id','=',$value->user_id)->first();
              $name = $user->name;

              $contact_name = $user->name; 
              $user_name    = $user->username;
              $user_email   = $user->email;
              if ($user->telephone) {
                  $user_phone = $user->telephone;
              } else {
                  $user_phone = $user->mobile;
              }  
              $item_title = $value->title;
              $item_url = url('/').'/'.'view/'. $value->slug;
              $item_link = url('/').'/'.'/user/vendor/'.$value->user_id;
              $comment = url('/').'/'.'view/'. $value->slug;

           $newsletter = Newsletters::where('id', '=', '9')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.auction_expire', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $user) {
                                    $message->to($user->email)->subject(''.$newsletter->subject.'');
                                });
                     }
       }

    }

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function auctionExpiring() {
    $input = Input::all();
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','2')->where('status','=', '1')->get();

    $get_ad_settings = AdManagementSettings::where('expiring_auction_noti_time_status','=','1')->where('id','=','1')->first();
    $noti_time = $get_ad_settings->expiring_auction_noti_time_start.' '.$get_ad_settings->expiring_auction_noti_time_end;

foreach ($row as  $value) {
    $date = date_create($value->ad_expiration);  
    //subtract ad_expiration to noti time settings             
    $sub_ad_expiration = date_sub($date, date_interval_create_from_date_string($noti_time));

      if ($current_time > $sub_ad_expiration->format('Y-m-d H:i:s') && $value->auction_expiring_noti_trigger == '1') {
         
         $value->auction_expiring_noti_trigger = '0';
         $value->save();

          //bidder notification  
          $msgheader = new UserNotificationsHeader;
          $msgheader->title       ='[System Message]';
          // $msgheader->read_status = ;
          $msgheader->reciever_id = $value->user_id;
          $msgheader->save();

          $msg = new UserNotifications;
          $msg->notification_id  = $msgheader->id;
          $msg->message     = '<a class="grayText btn-change-noti-status" data-id="'.$noti->id.'" href=' .url('/').'/ads/edit/'.$value->id.'>Your Auction ad '.$value->title.' will expire within '.$noti_time.'.</a>';
          $msg->type ="Auction Expiration";
          $msg->status ="1";
          $msg->save(); 


          $user = User::find($value->user_id);
          $name = $user->name;

              $contact_name = $user->name; 
              $user_name    = $user->username;
              $user_email   = $user->email;
              if ($user->telephone) {
                  $user_phone = $user->telephone;
              } else {
                  $user_phone = $user->mobile;
              }  
              $item_title = $value->title;
              $item_url = url('/').'/ads/view/'. $value->id;
              $item_link = url('/').'/user/vendor/'.$value->user_id;
              $comment = url('/').'/ads/view/'. $value->id;

           $newsletter = Newsletters::where('id', '=', '8')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.auction_expire', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $user) {
                                    $message->to($user->email)->subject(''.$newsletter->subject.'');
                                });
                     }

      } 
}


    }

   /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function basicAdExpired() {
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','1')->where('status','=', '1')->get();
    foreach ($row as $value) {
         if($value->ad_expiration <= $current_time && $value->ad_expiration != '0000-00-00 00:00:00'){
            $value->status = 3;
            $value->ad_exp_email_trigger = 0;
            $value->save();
     
          //auction owner message
          $msgheaderowner = new UserMessagesHeader;
          $msgheaderowner->title       ='[System Message]';
          $msgheaderowner->sender_id   = 2;
          $msgheaderowner->read_status = 1;
          $msgheaderowner->reciever_id = $value->user_id;
          $msgheaderowner->save();
          $msgowner = new UserMessages;
          $msgowner->message_id  = $msgheaderowner->id;
          $msgowner->message     = 'Your '.$value->title.' Ad has been expired. <a href="' .url('/').'/'.'view/'.$value->slug.'">Click here</a> to view the Ad! ';
          $msgowner->sender_id   = 2;
          $msgowner->save(); 
        }
    }

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function getSubcategory(){
      $id = Request::input('id') ? : Session::get('subcategory');

      $get_subcat = SubCategoriesTwo::where('subcat_main_id',$id)->get();

      $subcat="";
      if(count($get_subcat)>0){
         $subcat.= "<option value='all'>All</option>";
        foreach ($get_subcat as $key => $field) {
        $subcat .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';

        }
      }else{
        $subcat .= "undefined";
      }

      return Response::json($subcat);
    }
    public function getMainSubCategory(){
      $id = Request::input('id') ? : Session::get('main_category');
      $get_subcat = Subcategory::where('cat_id','=',$id)->get();
      $subcat="";
      if(count($get_subcat)>0){
        $subcat.= "<option data-slug='all' value='all'>All Sub Categories</option>";
        foreach ($get_subcat as $key => $field) {
        $subcat .= '<option data-slug="'.$field->slug.'" value="'.$field->unique_id.'">'.$field->name.'</option>';
        }
      }else{
        $subcat .= 'undefined';
      }


      return Response::json($subcat);
    }
    public function getMainSubCategoryForPost(){
      $id = Request::input('id') ? : Session::get('main_category');
      $get_subcat = Subcategory::where('cat_id','=',$id)->get();
      $subcat="";
      if(count($get_subcat)>0){
        $subcat .= "<option class='hide'>Select</option>";
        foreach ($get_subcat as $key => $field) {
        $subcat .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
        }
      }else{
        $subcat .= 'undefined';
      }


      return Response::json($subcat);
    }
    public function getSubcategoryForPost(){
      $id = Request::input('id') ? : Session::get('subcategory');

      $get_subcat = SubCategoriesTwo::where('subcat_main_id',$id)->get();

      $subcat="";
      if(count($get_subcat)>0){
        $subcat .= '<option class="hide">Select</option>';
        foreach ($get_subcat as $key => $field) {
        $subcat .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';

        }
      }else{
        $subcat .= "undefined";
      }
      return Response::json($subcat);
    }
    public function getSubcategoryThreeForPost(){
        $id = Request::input('id') ? : Session::get('subcategory');

        $get_subcat = SubCategoriesThree::where('subcat_tier_id',$id)->get();

        $subcat="";
        if(count($get_subcat)>0){
          $subcat .= '<option class="hide">Select</option>';
          foreach ($get_subcat as $key => $field) {
          $subcat .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';

          }
        }else{
          $subcat .= "undefined";
        }
        return Response::json($subcat);
      }
    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function postAdRedirect()
    {

    $id = Session::get('ad_redirect');
    
    return Redirect::to(url('/').'/ads/view/'.$id);
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

public function addBidder(){
    $SMSBalance = $this->getSMSBalance();
    $ads_id = Request::input('ads_id');
    $bid_inc_value = str_replace(',', '', Request::input('bidvalue'));
    $latest_bidx = AuctionBidders::where('status','=','1')->where('ads_id','=',$ads_id)->orderBy('created_at', 'desc')->first();
    if(Auth::check()) {
      $user_id = Auth::user()->id;
    }
    $row = Advertisement::find($ads_id);
    $get_user = User::where('id','=',$user_id)->first();
    $user_bid = $get_user->bid;
    //Checker, if user bids is greater than the minimum bid - START
//       if(is_null($latest_bidx)){
//         $minbid = $row->bid_start_amount;
//           if($bid_inc_value < $minbid){
//           return Response::json(['errormsg' => 'Your bid amount must be equal or greater than the minimum allowed bid']);
//         }
//       }else{
//         $minbid = $row->minimum_allowed_bid;
//           if($bid_inc_value < $minbid){
//           return Response::json(['errormsg' => 'Your bid amount must be equal or greater than the minimum allowed bid']);
//         }
//       }
      $minbid = $row->minimum_allowed_bid;
      if($bid_inc_value < $minbid){
        return Response::json(['errormsg' => 'Your bid amount must be equal or greater than the minimum allowed bid']);
      }
  
    //Checker, if user bids is greater than the minimum bid - END

    //Check if the user has bid points - START
      if($get_user->bid <= 0){
        return Response::json(['errormsg' => 'Bid points not enough. You have '.$get_user->bid.' bid points remaining.']);
      }
    //Check if the user has bid points - END

    if ($row->user_id != $user_id) {
          $get_ad_settings = AdManagementSettings::where('bid_inc_value_status','=','1')->where('id','=','1')->first();
          $get_latest_bidder = AuctionBidders::where('status','=','1')->where('ads_id','=',$ads_id)->orderBy('created_at', 'desc')->first();
            if (is_null($get_latest_bidder)) {
              $latest_bidder = null;
              // $total_bid  = $row->minimum_allowed_bid + $bid_inc_value;
              $total_bid  = $bid_inc_value;

            } else {
              $total_bid  = $bid_inc_value;
              $latest_bidder = $get_latest_bidder->user_id;
            }
          if ($latest_bidder != $user_id) {
                if(!(is_null($row)) && $row->status == "1" && $row->user_id != $user_id){
                   $bidder = new AuctionBidders;
                   $bidder->ads_id  = $row->id;
                   $bidder->user_id = $user_id;
                   $bidder->status  = '1';
                   $bidder->bid  = $total_bid;
                   $bidder->save();

                   // Bid points will be deducted by 1 if successfully placed bid - START
                   $get_user->bid = $get_user->bid - 1;
                   $get_user->save();
                   // Bid points will be deducted by 1 if successfully placed bid - END

                   // Setting minimum allowed bid to the last placed bid amount - START
                   $row->price = $row->price + $bid_inc_value;
                   $row->save();
                   // Setting minimum allowed bid to the last placed bid amount - END


                //compute for bid limit notify vendor
                $get_bidders = AuctionBidders::where('ads_id','=',$ads_id)->orderBy('id', 'desc')->first();
                $get_bid_limit_amount_status = AuctionBidders::where('ads_id', $ads_id)->where('bid_limit_amount_status', 1)->where('status', 1)->get();
                $get_ad_poster = User::where('id','=',$row->user_id)->first();
                // wtf
                  $bidder_info = User::find($get_bidders->user_id);
                      $subject = $row->title;
                      //Notify bidder about how many placed bid - START
                      $notiheaderuser = new UserNotificationsHeader;
                      $notiheaderuser->title       ='Bidding';
                      $notiheaderuser->read_status = 1;
                      $notiheaderuser->reciever_id = $get_bidders->user_id;
                      $notiheaderuser->photo_id = $get_bidders->user_id;
                      $notiheaderuser->save();
                      $notiuser = new UserNotifications;
                      $notiuser->notification_id  = $notiheaderuser->id;
                      $notiuser->message = '<a href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'"> You placed '.$bid_inc_value.' USD to the '.$row->title.' auction </a>';
                      $notiuser->type= "Bid Placed";
                      $notiuser->status= "1";
                      $notiuser->save();
                      Mail::send('email.placedbid', ['bid_placed' => $bid_inc_value, 'ad_name' => $row->title], function ($m) use ($get_ad_poster,$subject,$get_user) {
                          $m->from('noreply@yakolak.com')->to($get_user->email)->subject('Yakolak - Bid Placed');
                      });
                      //Notify bidder about how many placed bid - END

                      //Notify vendor that someone placed bid - START
                      $notiheader = new UserNotificationsHeader;
                      $notiheader->title       ='Bidding';
                      $notiheader->read_status = 1;
                      $notiheader->reciever_id = $row->user_id;
                      $notiheader->photo_id = $bidder_info->id;
                      $notiheader->save();
                      $noti = new UserNotifications;
                      $noti->notification_id  = $notiheader->id;
                      $noti->message     = '<a href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'" class="grayText btn-change-noti-status">'.$bidder_info->name.'</a> placed '.$row->minimum_allowed_bid.' USD on your <a>'.$row->title.'</a>.';
                      $noti->type= "Bid Placed";
                      $noti->status= "1";
                      $noti->save();
                      Mail::send('email.vendor_notify_placed_bid', ['bid_placed' => $bid_inc_value, 'ad_name' => $row->title, 'bid_placer' => $get_user->name], function ($m) use ($get_ad_poster,$subject,$get_user) {
                          $m->from('noreply@yakolak.com')->to($get_ad_poster->email)->subject('Yakolak - Someone placed a bid');
                      });
                      if($get_ad_poster->sms_verification_status == 1){
                        if($get_ad_poster->sms > 0){
                          if($SMSBalance > 1){
                            $to = $get_ad_poster->mobile;
                            $message = $bidder_info->name.' placed '.$bid_inc_value.' USD on your '.$row->title;
                            $send = $this->SMSNotify($message, $to);
                            $nexmo_result = json_decode($send);
                            $nexmo_status = $nexmo_result->messages[0]->status;
                            if($nexmo_status == 0){
                              $get_ad_poster->sms = $get_ad_poster->sms - 1;
                              $get_ad_poster->save();
                            }
                          }
                        }
                      }
                // wtf
                      
                      //Notify bidder about how many placed bid - END
                    

                //add time on ad expiration 
                if ($get_ad_settings->bid_adding_time_status == '1') {
                    if ($get_ad_settings->bid_adding_time_end == 'minutes') {
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." minutes"));                 
              
                    } elseif ($get_ad_settings->bid_adding_time_end == 'hours'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." hours"));                 
                 
                    } elseif ($get_ad_settings->bid_adding_time_end == 'days'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." days"));                 
                      
                    } elseif ($get_ad_settings->bid_adding_time_end == 'months'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." months"));                 
                    }
                    $row->ad_expiration = $expiration_date;
                    $row->save();
                }                
                   //auction add to watchlist
                  $get_watchlist_bid = UserBidList::where('ads_id','=',$ads_id)->where('user_id','=',$user_id)->where('status','=',1)->first();
                  if (is_null($get_watchlist_bid)) {
                    $watchlist_bid = new UserBidList;
                    $watchlist_bid->ads_id = $ads_id;
                    $watchlist_bid->user_id = $user_id;
                    $watchlist_bid->status = '1';
                    $watchlist_bid->save();
                  }
                  return Response::json(['body'  => 'Bid added']);
                }else{
                  return Response::json(['error' => ['Error on adding bid']]); 
               }
          } else {
                  return Response::json(['errormsg' => 'Adding bid failed, you are the latest bidder']); 
          }
    
      } else {
            return Response::json(['errormsgowner' => 'Error adding bid! You are the owner of this Ad']); 
    }
 


    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

   public function refreshBidders(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
     $get_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.points')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->take(3)->get();

     $getads_info = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','countries.countryName', 'country_cities.name as city', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.website', 'users.description as information','users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $ads_id);                                       
                                        })->first(); 
                               
        $rows = '';
        $rows .= '<div id="load-form_bidder" class="loading-pane hide">
                          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div></div>
                            <div class="panel panel-default bottomMarginLight removeBorder borderZero bgGray">';
               $i = 1;
          foreach ($get_bidders as $bidders) {
                if ($i == '1') {
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                    <div class="mediumText grayText"><strong><span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.
                        '</div>'.'<img src="'.url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'.url('/user/vendor ') . '/' . $getads_info->user_id .'/'. $bidders->name.'">'.$bidders->name.'</a> </span><span class="noPadding pull-right">'.number_format($bidders->points).' pts.</span>
                          </strong>
                        </div>
                      </div>';
                        if($getads_info->ad_expiration <= $current_time) {
                          if($getads_info->ads_type_id == '2' && $getads_info->status == '3'){
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                              <div class="blueText mediumText noPadding noMargin" style="text-align: center;">Bid Winner</div>
                            </div>';  
                            }
                        }
                } else {
                      if($getads_info->status != '3') {
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                        <div class="normalText '.($getads_info->status == 3?'lightgrayText':'grayText').'">
                            <span class="col-lg-1 noPadding"><div style="display:none;">'.$i++.
                            '</div> <img src="'. url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText '.($getads_info->status == 3?'lightgrayText':'grayText').'" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="noPadding pull-right">'.number_format($bidders->points).' pts.</span>
                        </div></div>';
                      }
                }
          }
             
        $rows .='<a href="#" class="news-expand pull-left normalText" data-toggle="modal" data-target="#modal-list-bidders" aria-controls="rmjs-1">view&nbsp;more</a>';
                      
      
        return Response::json($rows);
    }
      /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

   public function refreshBidderList(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
       $get_all_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.bid', 'auction_bidders.created_at')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->get();
      $getads_info = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.status','advertisements.contact_info', 'advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.website', 'users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $ads_id);                                       
                                        })->first(); 
      $i = 1;
      $rows ='';
      $rows .='<div class="modal fade" id="modal-list-bidders" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
              <div class="modal-dialog">
                <div class="modal-content borderZero">
                    <div id="load-form_bidder" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div></div>
                  <div class="modal-header modal-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-money"></i> Bidders</h4>
                  </div>';
          foreach ($get_all_bidders as $bidders) {
      $rows .='<div class="modal-body">
              <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                <div class="mediumText grayText">';
                  if ($i == '1') {
      $rows .='<strong><span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.'</div><img src="'.url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="25" height="25"/> </span><span class="col-lg-5 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="col-lg-3 noPadding">'.number_format($bidders->bid).' points</span> <span class="pull-right"><time class="timeago" datetime="'.$bidders->created_at.'" title=""></time></span></strong>';
               }else {
      $rows .='<span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.'</div><img src="'. url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="25" height="25"/> </span><span class="col-lg-5 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="col-lg-3 noPadding">'.number_format($bidders->bid).' points</span> <span class="pull-right"><time class="timeago" datetime="'.$bidders->created_at.'" title=""></time></span>';
                  }
      $rows .='</div>
              </div>
            </div>';
         }
      $rows .='<div class="modal-body"></div>
                <div class="modal-footer">
                  <input type="hidden" name="ads_id" id="row-ads_id" value="">
                  <button type="button" class="btn redButton borderZero" data-dismiss="modal"> Close</button>
                </div>
              </div>
            </div>
          </div>';               
      return Response::json($rows);
    }
    
    public function getParentCategory(){

       $check_sub_category_two = SubCategoriesTwo::where('unique_id',Request::input('id'))->first();
       if(!is_null($check_sub_category_two)){
                $check_sub_category = Subcategory::where('unique_id',$check_sub_category_two->subcat_main_id)->first();
                   $result['parent_cat_id'] = $check_sub_category->cat_id;
          }else{
              $check_sub_category_null = Subcategory::where('unique_id',Request::input('id'))->first();
              if(!is_null($check_sub_category_null)){
                   $result['parent_cat_id'] = $check_sub_category_null->cat_id;
              }else{
                  $check_category_null = Category::where('unique_id',Request::input('id'))->first();
                    if(!is_null($check_category_null)){
                     $result['parent_cat_id'] = $check_category_null->unique_id;

                    }
             }
  
        }
       return Response::json($result);
       
    }

}