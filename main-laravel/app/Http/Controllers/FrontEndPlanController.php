<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\Advertisement;
use App\AdvertisementPhoto;
use App\UserPlanTypes;
use App\UserPlanRates;
use App\PlanPrice;
use Carbon\Carbon;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\UserPlanPrices;
use Session;
use App\AdRating;
use App\UserAdComments;


class FrontEndPlanController extends Controller  {

public function index(){
$token = Session::token();

$this->data['title'] = "Plans";
$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
$this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
$this->data['countries'] = Country::all();
$this->data['usertypes'] = Usertypes::all();

if(Auth::check()){
     $user_id = Auth::user()->id;
}else{
     $user_id = null;
     $usertype_id = 1;
}

$this->data['messages_counter'] = UserMessagesHeader::where(function($query) use ($user_id){
                          $query->where('reciever_id','=',$user_id);
                                // ->OrWhere('sender_id','=',$user_id);
                              })->where(function($status){
                                    $status->where('read_status','=',1);
                              })->count();
$this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

 $current_time = Carbon::now()->format('Y-m-d H:i:s'); 




if(Auth::check()){
    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;

    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->get();         

      
      $get_current_plan = UserPlanTypes::find($user_plan_types);         
      $get_plan_rate = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->first();

      $duration = $get_plan_rate->duration;
      $discount = $get_plan_rate->discount;

      $ads_posted = Advertisement::where('ads_type_id','=',1)->where('user_id','=',$user_id)->where('status','!=',2)->get();
      $this->data['total_ads_rem'] = $get_current_plan->ads - count($ads_posted);
      $auction_posted = Advertisement::where('ads_type_id','=',2)->where('user_id','=',$user_id)->where('status','!=',2)->get();
      $this->data['total_auction_rem'] = $get_current_plan->ads - count($auction_posted);
      $all_posted = Advertisement::where('user_id','=',$user_id)->where('status','!=',2)->get();
      $this->data['total_ads_allowed_rem'] = $get_current_plan->total_ads_allowed - count($all_posted);
      $post_with_video = Advertisement::where('user_id','=',$user_id)->where('status','!=',2)->where('youtube','!=','null')->get();
      $this->data['total_video_rem'] = $get_current_plan->video_total - count($post_with_video);
      $get_user_bid_rem = User::where('id','=',$user_id)->first();
      $this->data['total_user_bid_rem'] = $get_current_plan->bid - $get_user_bid_rem->points;

    } else {

    $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '1')->where('status', '=', 1)->get();         
    $user_plan_types = null;
    }


$plan = "";
$plan .='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="section1" class="panel panel-default removeBorder borderZero nobottomMargin">
    <div class="panel-body">
    <div class="table-responsive">
    <table class="table nobottomMargin normalText">
    <thead><tr><th class="col-xs-3 text-center tablerightBorder"><h3 class="blueText">Upgrade your Plan Now!</th>';
        foreach ($get_plan as $value) {
    
      $get_plan_price = UserPlanPrices::where('usertype_id', '=', $usertype_id)->where('user_plan_id', '=', $value->id)->where('status', '=', 1)->orderBy('id', 'asc')->first();
   if (Auth::check()) {
             if ($usertype_id == 1) {
                $rate_id_1st = '1';
                $rate_id_2nd = '2';
                $rate_id_guest = '3';

             } else if ($usertype_id == 2) {
                $rate_id_1st = '4';
                $rate_id_2nd = '5';
                $rate_id_guest = '6';
             }
        //Price Rate Duration computation
        $get_plan_rate = UserPlanRates::join('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                                        ->select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                                        ->where(function($query)use($usertype_id, $rate_id_1st) {
                                               $query->where('user_plan_rates.usertype_id', '=', $usertype_id)
                                                     ->where('user_plan_rates.id', '=', $rate_id_1st)    
                                                     ->where('user_plan_rates.status', '=', 1)                                   
                                                     ->where('user_plan_rates.rate_status', '!=', 2);                                   
                                         })->first();
        if (is_null($get_plan_rate)) {
            $get_discount = 0;
            $get_duration = 0;
        } else {
            $get_discount = $get_plan_rate->discount;
            $get_duration = $get_plan_rate->value;
        } 
          $user_plan_availed = Auth::user()->plan_availed;
          $current_time = Carbon::now()->format('Y-m-d H:i:s'); 
          $date = date_create($current_time);   

         //1st rate            
          $discount_duration = date_sub($date,date_interval_create_from_date_string($get_duration." months"));                 
           // dd(array($discount_duration->format('Y-m-d H:i:s'), $user_plan_availed))  ;
           
              if (!(is_null($get_plan_rate)) && $discount_duration->format('Y-m-d H:i:s') < $user_plan_availed)  {
                  //1st rate condition 
                  $right_discount = $get_discount;
                  $test = '1st rate';
              } else {
                   $test = 'aw';
                   $get_plan_rate2 = UserPlanRates::join('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                        ->select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                        ->where(function($query)use($usertype_id, $rate_id_2nd) {
                                               $query->where('user_plan_rates.usertype_id', '=', $usertype_id)
                                                     ->where('user_plan_rates.id', '=', $rate_id_2nd)    
                                                     ->where('user_plan_rates.status', '=', 1)                                   
                                                     ->where('user_plan_rates.rate_status', '!=', 2);                                   
                                          })->first();
                   if (is_null($get_plan_rate2)) {
                        $get_discount = 0;
                        $get_duration = 0;
                    } else {
                        $get_discount2 = $get_plan_rate2->discount;
                        $get_duration2 = $get_plan_rate2->value;
                    } 
                  $current_time = Carbon::now()->format('Y-m-d H:i:s'); 
                  $date = date_create($current_time);  
                  $discount_duration2 = date_sub($date,date_interval_create_from_date_string($get_duration2." months"));                 
                    if (!(is_null($get_plan_rate2)) && $discount_duration2->format('Y-m-d H:i:s') < $user_plan_availed) {
                        //2nd rate condition
                        $right_discount = $get_discount2;
                        $test = '2nd rate';

                    } else {
                      //3rd rate no discount
                        $right_discount = 'no discount';
                    }

              }
   } else {
    //Rate for guest 
      $get_plan_rate = UserPlanRates::join('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                        ->select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                        ->where(function($query) {
                                               $query->where('user_plan_rates.usertype_id', '=', 1)
                                                     ->where('user_plan_rates.id', '=', 3)    
                                                     ->where('user_plan_rates.status', '=', 1)                                   
                                                     ->where('user_plan_rates.rate_status', '!=', 2);                                   
                                          })->first();
            if (is_null($get_plan_rate)) {
                 $right_discount = 0;
            } else {
                 $right_discount = $get_plan_rate->discount;
            }  
   }  
    //compute the discount
    if ($right_discount == 'no discount') {
           $final_price = $get_plan_price->price / $get_plan_price->term;
    } else {
          $inti_discount  = 100 - $right_discount;
          $total_discount = '.'.$inti_discount;
          $price_with_discount = $get_plan_price->price * $total_discount;
          $final_price = $price_with_discount / $get_plan_price->term;
    }

    $get_no_discount_price = $get_plan_price->price / $get_plan_price->term;
            if ($value->id == '1' || $value->id == '5') {
                 $plan .='<th class="col-xs-2 text-center mediumText bgGray tablerightBorder">'.$value->plan_name.'<br><h3 class="noMargin lightgrayText"><br> </h3><button class="btn btn-sm redButton fullSize borderZero noMargin '.(Auth::check() ?'':'btn-login" data-toggle="modal" data-target="#modal-login"').' type="submit">'.($value->id == '1' || $value->id == '5' ?'FREE': '').'</button></th>';
            } else {
                 $plan .='<th class="col-xs-2 text-center mediumText bgGray tablerightBorder">'.$value->plan_name.'<br>'.($right_discount != 0?'<p class="noMargin lightgrayText">'.$right_discount.'% OFF REGULAR $'.round($get_no_discount_price, 2).' MO</p>':'').'<h3 class="noMargin lightgrayText">'.round($final_price, 2). '/mo*'.'</h3><button class="btn btn-sm redButton fullSize borderZero noMargin '.(Auth::check() ?'':'btn-login" data-toggle="modal" data-target="#modal-login"').' type="submit">'.($value->id == $user_plan_types ?'MY PLAN': 'DISCOUNT').'</button></th>';
            }
        }
        
$plan .='</tr>
    </thead>
            <tbody>
              <tr>
                <td class="bold blueText tablerightBorder">No. of Ads</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->ads.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">No. of Auctions</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->auction.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">No. of Image per Ad</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->img_per_ad.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">No. of Videos per Ad</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->video_total.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">Free SMS Notification</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->sms.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">No. of Bids</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->bid.'</td>';   
                }
        $plan .='</tr>
                <tr>
                <td class="bold blueText tablerightBorder">Total Free Points</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->point.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td class="bold blueText tablerightBorder">Total Points Exchange</td>';
                foreach ($get_plan as $value) {
                   $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->point_exchange.'</td>';   
                }
        $plan .='</tr>
              <tr>
                <td></td>';
                foreach ($get_plan as $value) {
                  if (Auth::check()) {
                    if ($value->id=='1' || $value->id =='5') {
                $plan .='<td class="tablerightBorder bgGray"><button class="btn btn-sm blueButton fullSize borderZero noMargin" id="btn-get-started">FREE</button></td>';           
                         } else {
                $plan .='<td class="tablerightBorder bgGray">'.($user_plan_types  == $value->id ? '<span><a name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton fullSize borderZero noMargin" id="btn-get-started" href="#'.$value->plan_name.'">MY PLAN</a></span>' : '<span><a name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton fullSize borderZero noMargin" href="#'.$value->plan_name.'">GET STARTED</a></span>').'</td>'; 
                         }    
                  } else {
                $plan .='<td class="tablerightBorder bgGray"><span><a class="btn btn-sm blueButton fullSize borderZero noMargin btn-login" href="#'.$value->plan_name.'">GET STARTED</a></span></td>';  
                  }

                }
        $plan .=' </tr>
            </tbody>
          </table>
          </div>
          </div>
        </div> 
      <div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        </div>        
        </div>   
      </div>';




//SUBSCRIPTION
$plan_description ="";
if(Auth::check()){
    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;
    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->where('status', '=', 1)->get();          
$get_plan_rate = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->first();

$duration = $get_plan_rate->duration;
$discount = $get_plan_rate->discount;

      } else {
        $user_id = null;
        $usertype_id = null;
        $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('plan_status', '!=', '5')->get();       
      
        $get_plan_rate = UserPlanRates::where('usertype_id', '=', 1)->where('rate_status', '=', 1)->first();
        $duration = null;
        $user_plan_types = '0';
      }
$rows = User::with("getUserCompanyBranch")->find($user_id);



//SECTION
foreach ($get_plan as  $value) {



 $plan_description .='<div id="'.$value->plan_name.'" class="col-md-12">
                        <div class="panel panel-default removeBorder borderZero borderBottom">
                          <div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>'.strtoupper($value->plan_name).'</strong></span>';
 if (Auth::check()) {
  $plan_description .='<form action="plan/payment" id="upgrade" method="post"><input type="hidden" name="_token" id="csrf-token" value="'.$token.'" />';
 }
  $plan_description .='</div><div class="panel-body"><div class="bottomPaddingB xxlargeText"><strong><span class="blueText">'.strtoupper($value->plan_name).'</span> <span class="lightgrayText"> ZERO COST FOREVER</span></strong>';

if (Auth::check()) {
    $plan_description .=($user_plan_types  == $value->id ? '<span class="pull-right"><button type="submit" name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton borderZero noMargin" type="submit" id="btn-get-started-below">MY PLAN</button></span>' : '<span class="pull-right"><button type="submit" name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton borderZero noMargin" type="submit">GET STARTED</button></span>');
     $plan_description .='</form>';

  } else {
$plan_description .='<span class="pull-right"><button class="btn btn-sm blueButton borderZero noMargin btn-login" data-toggle="modal" data-target="#modal-login" type="submit">GET STARTED</button></span>';
}
              $plan_description .=' </div>
              <div class="row borderbottomLight">                  
                          </div>
                           <div class="blockTop">
                           <div class="lineHeightC  normalText grayText bottomPaddingC">
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue leo eu erat elementum placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam at dui dui. Nunc rhoncus tellus eu neque ultrices molestie. Vivamus eget rutrum leo. Nulla dapibus ligula quis massa faucibus sollicitudin sed ac purus. Maecenas egestas rhoncus nunc, sit amet vestibulum lectus iaculis nec. Pellentesque at ipsum id odio suscipit commodo a a dui. Nam turpis nibh, imperdiet sed urna quis, auctor maximus quam. Cras scelerisque metus tempus ligula gravida blandit. Praesent dictum tempor quam varius laoreet. Morbi quis libero ut erat egestas lacinia laoreet nec mauris. Ut nec imperdiet lectus, at ullamcorper orci. Aenean ultrices iaculis augue at tempor.
                            </div>
                            <div class="col-lg-12 lineHeightC  noPadding">
                            <div class="col-lg-4 noleftPadding">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class="">No. of Ads Remaining <span class="pull-right blueText bold">'.$value->ads.'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class="">No. of Auctions <span class="pull-right blueText bold">'.$value->auction.'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class="">No. of Image per Ad <span class="pull-right blueText bold">'.$value->img_per_ad.'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class="">No. of Videos per Ad <span class="pull-right blueText bold">'.$value->video_total .'</span></span>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Free Points <span class="pull-right blueText bold">'.$value->point.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Bids <span class="pull-right blueText bold">'.$value->bid.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Free SMS Notification <span class="pull-right blueText bold">'.$value->point_exchange.' </span></span>
                            </div>
                            </div>
                            </div>
                          </div>
                          </div>
                      </div>
                    </div>
';
}
    if ($usertype_id == '1'||$usertype_id == null) {
       $id = '1';
    }  else {
       $id = '5';
    }
    $get_free_plan = UserPlanTypes::where('id','=',$id)->first();
    $this->data['get_free_plan'] = $get_free_plan;
    $this->data['plan'] = $plan;
    $this->data['plan_description'] = $plan_description;
    $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
    
      // YAH IT WORKS TILL THE END!!!!                       
    return View::make('plans.list', $this->data);
  }

}
