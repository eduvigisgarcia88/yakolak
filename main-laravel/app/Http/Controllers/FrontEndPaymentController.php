<?php 
namespace App\Http\Controllers;

use View;
use App\Http\Requests;
use App\AdvertisementPhoto;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Response;
use App\Advertisement;
use App\CustomAttributes;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\PromotionPopups;
use Request;
use App\Category;
// use App\AdvertisementWatchlist;
use App\AdvertisementWatchlists;
use App\Country;
use App\City;
use App\Usertypes;
use Excel;
use Response;
use Auth;
use Redirect;
use Input;
use Carbon\Carbon;
use App\UserMessages;
use App\UserMessagesHeader;
use App\UserPlanTypes;
use App\PlanPrice;
use App\UserPlanPrices;
use Session;
use App\User;
use App\PaypalPayments;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\UserAdComments;
use App\AdRating;
use App\UserReferrals;
use App\ReferralTypes;
use App\BidPoints;
use App\PaypalPaymentHistory;
use App\UserNotificationsHeader;
use App\PaypalPaymentHistoryForBidPoints;
use App\PaypalPaymentHistoryForBannerPlan;
use App\PaypalPaymentForBidPoints;
use App\PaypalPaymentForBannerPlan;
use App\BannerAdvertisementSubscriptions;
use App\Newsletters;
use App\AdFeatureTransactionHistory;
use App\PromotionPlanDiscounts;
use App\PromotionBidPlanDiscounts;
use Paypalpayment;
use Validator;
use Mail;
use App\Invoice;
use App\BannerPlacementLocation;




class FrontEndPaymentController extends Controller {


 // code
private $_apiContext;

     function __construct()
    {

        $this->_apiContext = Paypalpayment::apiContext(config('paypal_payment.Account.ClientId'), config('paypal_payment.Account.ClientSecret'));

    }

 public function index()
    {
        echo "<pre>";

        $payments = Paypalpayment::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

        // dd($addr);
    }

    public function create()
    {

        return View('payment.test');
    }
  
  
    public function createInvoice($user_id, $invoice_number, $transaction_type, $details, $extended_details, $cost, $currency)
    {
      $save = new Invoice;
      $save->user_id = $user_id;
      $save->invoice_no = $invoice_number;
      $save->transaction_type = $transaction_type;
      $save->details = $details;
      $save->extended_details = $extended_details;
      $save->cost = $cost;
      $save->currency = $currency;
      $save->save();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    public function verifyPayment($in){

       $get_paypal_invoice_number = Input::get('tx');
       $get_paypal_payment_status = Input::get('st');
       $get_paypal_payment_amount = Input::get('amt');
       $get_paypal_payment_currency = Input::get('cc');
       if($get_paypal_payment_status == "Completed"){
            $row = Paypalpayments::where('invoice_number',$in)->first();
            $get_plan_info = UserPlanTypes::find($row->plan_id);

            if(!is_null($row)){
                $row->status = 1;
                $row->save();
                if(!is_null($row->user_id)){
                  $update_user = User::where('id',$row->user_id)->where('status',1)->first();
                    if(!is_null($update_user)){
                      $update_user->user_plan_types_id = $row->plan_id;
                      $update_user->sms = $update_user->sms + $get_plan_info->sms;
                      $update_user->points = $update_user->points + $get_plan_info->point;
                      $update_user->bid = $update_user->bid + $get_plan_info->bid;
                      $update_user->total_ads = $get_plan_info->ads;
                      $update_user->total_bids = $get_plan_info->auction;
                      $update_user->save();

                      $plan_listings_allowed = $update_user->total_ads;
                      $plan_auctions_allowed = $update_user->total_bids;

                      $user_listing_count = Advertisement::where('status', 1)->where('ads_type_id', 1)->where('user_id', $row->user_id)->count();
                      $user_auction_count = Advertisement::where('status', 1)->where('ads_type_id', 2)->where('user_id', $row->user_id)->count();

                      $raw_disabled_listings = Advertisement::whereIn('status', [3,4])->where('ads_type_id', 1)->where('user_id', $row->user_id)->latest('created_at');
                      $raw_disabled_auctions = Advertisement::whereIn('status', [3,4])->where('ads_type_id', 2)->where('user_id', $row->user_id)->latest('created_at');

                      //enable or disable for listings - start
                      if($plan_listings_allowed > $user_listing_count) {
                        $listings_to_enable_count = $plan_listings_allowed - $user_listing_count;
                        $enable_listings = $raw_disabled_listings->take($listings_to_enable_count)->get();
                        foreach($enable_listings as $enable_listing) {
                          $enable_listing->status = 1;
                          $enable_listing->save();
                        }
                      } else {
                        if($plan_listings_allowed < $user_listing_count) {
                          $raw_listing = Advertisement::where('status', 1)->where('ads_type_id', 1)->where('user_id', $row->user_id)->latest('created_at');
                          if($user_listing_count > $plan_listings_allowed) {
                            $user_listings = $raw_listing->skip($plan_listings_allowed)->take($user_listing_count - $plan_listings_allowed)->get();
                          } else {
                            $user_listings = $raw_listing->get();
                          }
                          foreach ($user_listings as $disable_listing) {
                            $disable_listing->status = 3;
                            $disable_listing->save();
                          }
                        }
                      }
                      //enable or disable for listings - end

                      //enable or disable for auctions - start
                      if($plan_auctions_allowed > $user_auction_count) {
                        $auctions_to_enable_count = $plan_auctions_allowed - $user_auction_count;
                        $enable_auctions = $raw_disabled_auctions->take($auctions_to_enable_count)->get();
                        foreach($enable_auctions as $enable_auction) {
                          $enable_auction->status = 1;
                          $enable_auction->save();
                        }
                      } else {
                        if($plan_auctions_allowed < $user_auction_count) {
                          $raw_auction = Advertisement::where('status', 1)->where('ads_type_id', 1)->where('user_id', $row->user_id)->latest('created_at');
                          if($user_auction_count > $plan_auctions_allowed) {
                            $user_auctions = $raw_auction->skip($plan_auctions_allowed)->take($user_auction_count - $plan_auctions_allowed)->get();
                          } else {
                            $user_auctions = $raw_auction->get();
                          }
                          foreach ($user_auctions as $disable_auction) {
                            $disable_auction->status = 4;
                            $disable_auction->save();
                          }
                        }
                      }
                      //enable or disable for auctions - end

                      $get_plan_term = PlanPrice::where('user_plan_id',$update_user->user_plan_types_id)->where('usertype_id',$update_user->usertype_id)->first();
                      $new_plan_expiration = date("Y-m-d H:i:s", strtotime($update_user->updated_at . "+ $get_plan_term->term months"));
                      $update_user->plan_expiration = $new_plan_expiration;
                      $update_user->save();
                      $save_payment_history = new PaypalPaymentHistory;
                      $save_payment_history->user_id = $update_user->id;
                      $save_payment_history->transaction_no = $get_paypal_invoice_number;
                      $save_payment_history->payment_status = $get_paypal_payment_status;
                      $save_payment_history->payment_amount = $get_paypal_payment_amount;
                      $save_payment_history->payment_currency = $get_paypal_payment_currency;
                      $save_payment_history->save();
                      $this->createInvoice($update_user->id, $get_paypal_invoice_number, 'Plan Upgrade', $get_plan_info->plan_name, 'Plan Upgraded to '.$get_plan_info->plan_name, $get_paypal_payment_amount, $get_paypal_payment_currency);
                     
                      $update_referral = UserReferrals::where('user_id',$update_user->id)->where('referral_type',1)->where('status',1)->first();
                      $total_referral_points = UserReferrals::where('referrer_id',$update_user->id)->sum('referral_points');

                      $get_plan_info = UserPlanTypes::where('id',$row->plan_id)->where('status',1)->first();
                      if(!is_null($update_referral)){
                          $get_referral_type = ReferralTypes::where('id',2)->where('status',1)->first();
                          $save_referral = new UserReferrals;
                          $save_referral->referral_type = $get_referral_type->id;
                          $save_referral->referrer_id = $update_referral->referrer_id;
                          $save_referral->referral_points = $get_plan_info->reward_point;
                          $save_referral->user_id = $update_referral->user_id;
                          $save_referral->status = 1;
                          $save_referral->save();
                          $newsletter = Newsletters::where('id', '=', '19')->where('newsletter_status','=','1')->first();
                          $get_referrer_info = User::where('id',$save_referral->referrer_id)->first();
                          $get_referred_user = User::where('id',$save_referral->user_id)->first();
                          $referral_points = $save_referral->referral_points;
                          $referred_user = $get_referred_user->name;
                          $current_plan =  $get_plan_info->plan_name;
                         
                            if ($newsletter) {
                                      Mail::send('email.referral_upgrade_plan', ['referral_points'=> $referral_points,'current_plan'=>$current_plan,'referred_user' => $referred_user,'newsletter' => $newsletter], function($message) use ($newsletter,$get_referrer_info) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to($get_referrer_info->email)->subject(''.$newsletter->subject.'');
                                       });
                             }
                            $newsletter_for_purchased_plan = Newsletters::where('id', '=', '12')->where('newsletter_status', '!=', '2')->first();
                            $total_referral_points = "200";

                           if (!is_null($newsletter_for_purchased_plan)) {
                                  Mail::send('email.purchased_plan', ['sms' => $get_plan_info->sms,'current_plan' => $get_plan_info->plan_name,'point_exchange' => $get_plan_info->point_exchange,'bid' => $get_plan_info->bid,'point' => $get_plan_info->point,'video_total' => $get_plan_info->video_total,'total_ads_allowed' => $get_plan_info->total_ads_allowed,'img_total' => $get_plan_info->img_total,'img_per_ad' => $get_plan_info->img_per_ad,'auction' => $get_plan_info->auction,'ads' => $get_plan_info->ads,'months12_price' => $get_plan_info->months12_price,'months6_price' => $get_plan_info->months6_price,'plan_name' => $get_plan_info->plan_name,'contact_name' => $get_plan_info->contact_name,'user_name' => $update_user->name,'user_email' => $update_user->email,'user_phone' => $update_user->mobile,'referral_points' => $total_referral_points,'newsletter' => $newsletter_for_purchased_plan], function($message) use ($newsletter_for_purchased_plan,$update_user) {
                                      $message->from('noreply@yakolak.com', 'Yakolak');
                                      $message->to($update_user->email)->subject(''.$newsletter_for_purchased_plan->subject.'');
                                  });
                          }  
                      }else{

                         $total_referral_points = UserReferrals::where('referrer_id',34)->where('status',1)->sum('referral_points');
                         $newsletter = Newsletters::where('id', '=', '12')->where('newsletter_status', '!=', '2')->first();
                         if (!is_null($newsletter)) {
                                Mail::send('email.purchased_plan', ['sms' => $get_plan_info->sms,'current_plan' => $get_plan_info->plan_name,'point_exchange' => $get_plan_info->point_exchange,'bid' => $get_plan_info->bid,'point' => $get_plan_info->point,'video_total' => $get_plan_info->video_total,'total_ads_allowed' => $get_plan_info->total_ads_allowed,'img_total' => $get_plan_info->img_total,'img_per_ad' => $get_plan_info->img_per_ad,'auction' => $get_plan_info->auction,'ads' => $get_plan_info->ads,'months12_price' => $get_plan_info->months12_price,'months6_price' => $get_plan_info->months6_price,'plan_name' => $get_plan_info->plan_name,'contact_name' => $get_plan_info->contact_name,'user_name' => $update_user->name,'user_email' => $update_user->email,'user_phone' => $update_user->mobile,'referral_points' => $total_referral_points, 'newsletter' => $newsletter], function($message) use ($newsletter,$update_user) {
                                    $message->from('noreply@yakolak.com', 'Yakolak');
                                    $message->to($update_user->email)->subject(''.$newsletter->subject.'');
                                });
                        } 
                      }                    
                      Session::flash('payment_success',1);
                      return redirect()->action('FrontEndUserController@fetchInfo');
                    }else{
                      Session::forget('payment_success');
                      return Response::json(['error' => "Error finding the user's id"]);
                    }
                  
                }else{
                    return Response::json(['error' => "Error: null user id"]);
                }
          }else{
               return Response::json(['error' => "Error: finding the id"]);
          }
       }
       
    }
  
  
    public function verifyPaymentForBidPoints($in)
    {
       $get_paypal_invoice_number = Input::get('tx');
       $get_paypal_payment_status = Input::get('st');
       $get_paypal_payment_amount = Input::get('amt');
       $get_paypal_payment_currency = Input::get('cc');
       if($get_paypal_payment_status == "Completed"){
          $row = PaypalPaymentForBidPoints::where('invoice_number',$in)->first();
          $get_new_bid_points = BidPoints::where('id',$row->bid_plan_id)->where('status',1)->first();
          if(!is_null($row)){
            $update_user = User::where('id',$row->user_id)->where('status',1)->first();
            if(!is_null($update_user)){
                $update_user->bid = $update_user->bid + $get_new_bid_points->points;
                $update_user->save();
                $save_payment_history = new PaypalPaymentHistoryForBidPoints;
                $save_payment_history->user_id = $update_user->id;
                $save_payment_history->transaction_no = $get_paypal_invoice_number;
                $save_payment_history->payment_status = $get_paypal_payment_status;
                $save_payment_history->payment_amount = $get_paypal_payment_amount;
                $save_payment_history->payment_currency = $get_paypal_payment_currency;
                $save_payment_history->save();
              
                $this->createInvoice($update_user->id, $get_paypal_invoice_number, 'Bid Points', $get_new_bid_points->points.' points', $get_new_bid_points->points.' bid points has been added to the account', $get_paypal_payment_amount, $get_paypal_payment_currency);
              
              
                $newsletter = Newsletters::where('id', '=', '25')->where('newsletter_status','=','1')->first();
                            if ($newsletter) {
                                      Mail::send('email.purchased_bid_points', ['newsletter' => $newsletter], function($message) use ($newsletter) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                       });
                             }     
                Session::flash('payment_success_for_bid_points',1);
                return redirect()->action('FrontEndUserController@fetchInfo');
            }else{
               return Response::json(['error' => "Error finding the user's id"]);
            }
          }else{
              return Response::json(['error' => "Error finding the user's id"]);
          }
       }else{
          return Response::json(['error' => "Transaction not completed"]);
       }


    }
  
    public function PromotionBidPlanDiscounts($in){

       $get_paypal_invoice_number = Input::get('tx');
       $get_paypal_payment_status = Input::get('st');
       $get_paypal_payment_amount = Input::get('amt');
       $get_paypal_payment_currency = Input::get('cc');

       if($get_paypal_payment_status == "Completed"){
          $row = PaypalPaymentForBidPoints::where('invoice_number',$in)->first();
          $get_new_bid_points = BidPoints::where('id',$row->bid_plan_id)->where('status',1)->first();
          if(!is_null($row)){
            $update_user = User::where('id',$row->user_id)->where('status',1)->first();
            if(!is_null($update_user)){
                $update_user->bid = $update_user->bid + $get_new_bid_points->points;
                $update_user->save();
                $save_payment_history = new PaypalPaymentHistoryForBidPoints;
                $save_payment_history->user_id = $update_user->id;
                $save_payment_history->transaction_no = $get_paypal_invoice_number;
                $save_payment_history->payment_status = $get_paypal_payment_status;
                $save_payment_history->payment_amount = $get_paypal_payment_amount;
                $save_payment_history->payment_currency = $get_paypal_payment_currency;
                $save_payment_history->save();
                $newsletter = Newsletters::where('id', '=', '25')->where('newsletter_status','=','1')->first();
                            if ($newsletter) {
                                      Mail::send('email.purchased_bid_points', ['newsletter' => $newsletter], function($message) use ($newsletter) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                       });
                             }     
                Session::flash('payment_success_for_bid_points',1);
                return redirect()->action('FrontEndUserController@fetchInfo');
            }else{
               return Response::json(['error' => "Error finding the user's id"]);
            }
          }else{
              return Response::json(['error' => "Error finding the user's id"]);
          }
       }else{
          return Response::json(['error' => "Transaction not completed"]);
       }
      
    }

    public function verifyPaymentForBannerPlan($id){

       $get_paypal_invoice_number = Input::get('tx');
       $get_paypal_payment_status = Input::get('st');
       $get_paypal_payment_amount = Input::get('amt');
       $get_paypal_payment_currency = Input::get('cc');
       if($get_paypal_payment_status == "Completed"){
          $row = PaypalPaymentForBannerPlan::where('subscription_id',$id)->orderBy('created_at', 'desc')->first();
          if(!is_null($row)){
            $update_user = User::where('id',$row->user_id)->where('status',1)->first();
            if(!is_null($update_user)){
                $save_payment_history = new PaypalPaymentHistoryForBannerPlan;
                $save_payment_history->user_id = $update_user->id;
                $save_payment_history->transaction_no = $get_paypal_invoice_number;
                $save_payment_history->payment_status = $get_paypal_payment_status;
                $save_payment_history->payment_amount = $get_paypal_payment_amount;
                $save_payment_history->payment_currency = $get_paypal_payment_currency;
                $save_payment_history->save();
              
                $placement = '';
                $extended_details = '';
                
                
              
                $change_banner_status = BannerAdvertisementSubscriptions::where('id',$id)->where('banner_status',2)->first();
                if(!is_null($change_banner_status)){
                  $change_banner_status->banner_status = 4;
                  $metric = $change_banner_status->metric_duration == 'y' ? ' years' : ($change_banner_status->metric_duration == 'm' ? ' months' : ' days');
                  $change_banner_status->start_date = strtotime(new Carbon('now', 'Asia/Manila'));
                  $change_banner_status->expiration = strtotime(new Carbon('now + '.$change_banner_status->duration.$metric, 'Asia/Manila'));
                  $change_banner_status->save();
                  $placement .= BannerPlacementLocation::where('id', $change_banner_status->page)->pluck('name').' > '.BannerPlacementPlans::where('id', $change_banner_status->placement_id)->pluck('banner_placement_name');
                  $extended_details .= 'Banner Ad placed to '.$placement.' for the country '.Country::where('id', $change_banner_status->country_id)->pluck('countryName').' for '.$change_banner_status->duration.$change_banner_status->metric_duration;
                }
              
                $this->createInvoice($update_user->id, $get_paypal_invoice_number, 'Banner Ad', $placement, $extended_details, $get_paypal_payment_amount, $get_paypal_payment_currency);
              
              
                $update_referral = UserReferrals::where('user_id',$update_user->id)->where('referral_type',3)->where('status',2)->first();
                if(!is_null($update_referral)){
                    $update_referral->status = 1;
                    $update_referral->save();
                    $get_referrer_info = User::where('id',$update_referral->referrer_id)->first();
                    $referred_user = User::where('id',$update_referral->user_id)->first();
                    $newsletter = Newsletters::where('id', '=', '24')->where('newsletter_status','=','1')->first();
                            if ($newsletter) {
                                      Mail::send('email.referral_banner', ['referral_points'=> $update_referral->referral_points,'referred_user' => $referred_user->name,'newsletter' => $newsletter], function($message) use ($newsletter,$get_referrer_info) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to($get_referrer_info->email)->subject(''.$newsletter->subject.'');
                                       });
                             }
                  $newsletter_banner_payment = Newsletters::where('id', '=', '23')->where('newsletter_status','=','1')->first();
                            if ($newsletter_banner_payment) {
                                      Mail::send('email.purchased_banner', ['newsletter' => $newsletter_banner_payment], function($message) use ($newsletter_banner_payment) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to(Auth::user()->email)->subject(''.$newsletter_banner_payment->subject.'');
                                       });
                             }         
                }else{
                    $newsletter = Newsletters::where('id', '=', '23')->where('newsletter_status','=','1')->first();
                            if ($newsletter) {
                                      Mail::send('email.purchased_banner', ['newsletter' => $newsletter], function($message) use ($newsletter) {
                                           $message->from('noreply@yakolak.com', 'Yakolak');
                                           $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                       });
                             }
                }
                Session::flash('payment_success_for_banner_plan',1);
                return redirect()->action('FrontEndUserController@fetchInfo');
            }else{
               return Response::json(['error' => "Error finding the user's id"]);
            }
          }else{
              return Response::json(['error' => "Error finding the user's id"]);
          }
       }else{
          return Response::json(['error' => "Transaction not completed"]);
       }


    }

    public function verifyPaymentForFeaturingAd($id,$duration,$duration_metric,$callback = null){
       $get_paypal_invoice_number = Input::get('tx');
      /* $get_paypal_invoice_paypal_duration = Input::get('duration');
       $get_paypal_invoice_paypal_duration_metric = Input::get('duration_metric');*/
       $get_paypal_payment_status = Input::get('st');
       $get_paypal_payment_amount = Input::get('amt');
       $get_paypal_payment_currency = Input::get('cc');
       $row = Advertisement::find($id);
       if(!is_null($row)){
          $row->feature = 2;
          $row->save();
          $save_transaction_history = new AdFeatureTransactionHistory;
          $save_transaction_history->ad_id = $id;
          $save_transaction_history->duration = $duration;
          $save_transaction_history->duration_metric = $duration_metric;
          $save_transaction_history->payment_method = 2;
          $save_transaction_history->user_id = $row->user_id;
          $save_transaction_history->cost = $get_paypal_payment_amount;
          $save_transaction_history->save();
          $transaction_no = base64_encode('transaction-'.$save_transaction_history->id);
          $save_transaction_history->transaction_no = $transaction_no;
          $save_transaction_history->save();
         Session::flash('payment_success_for_feature_ad',1);
         
         if(!is_null($callback)) {
           return redirect()->to(\Crypt::decrypt($callback));
         } else {
           return redirect()->action('FrontEndUserController@fetchInfo');
         }
         
         
       }
    }
    public function purchasePlanThruPoints(){ //thru points

      $input = Input::all();
      $price_info = UserPlanPrices::leftjoin('plan_term','plan_term.id','=','user_plan_price.term')
                                   ->select('plan_term.name','user_plan_price.*')
                                   ->where('user_plan_price.id', '=', array_get($input,'price_id'))
                                   ->orderBy('user_plan_price.price','desc')->first();
      $term = '+'.$price_info->duration.' '.$price_info->name;
      $get_plan = UserPlanTypes::find($price_info->user_plan_id);
      $expiration_date = Carbon::now()->modify($term);
      if(!is_null($get_plan)){
        $get_user = User::find(Auth::user()->id);
        if(!is_null($get_user)){
          if($get_user->points >= $price_info->point_exchange){
              $get_user->points = $get_user->points - $price_info->point_exchange;
              $get_user->bid = $get_user->bid + $get_plan->bid;
              $get_user->total_ads = $get_user->total_ads + $get_plan->ads;
              $get_user->total_bids = $get_user->total_bids + $get_plan->auction;
              $get_user->sms = $get_user->sms + $get_plan->sms;
              $get_user->points = $get_user->points + $get_plan->point;
              $get_user->user_plan_types_id = $get_plan->id;
              $get_user->plan_expiration = $expiration_date;
              $get_user->save();
              Session::flash('payment_success',1);
              $url = url('dashboard');
              return Response::json(['url' => $url]);
          }else{
              Session::flash('payment_failed', 1);
              $url = url('dashboard');
              return Response::json(['url' => $url]);
          }
        }


     }




    }

    public function purchasePlan() //thru paypal
    {

      $input = Input::all();
      $usertype_id = Auth::user()->usertype_id;
      $plan_id = Input::get('plan_id');
      $price_id = Input::get('price_id');

      $price_chosen = Input::get('price_chosen');
      $get_plan = UserPlanTypes::where('id','=',$plan_id)->first();

      $this->data['plan_name'] = $get_plan->plan_name;
      $this->data['point_exchange'] = $get_plan->point_exchange;
      $this->data['plan_id'] = $get_plan->id;

      $created = new Carbon(Auth::user()->created_at);
      $diff =  Carbon::now()->diffInDays($created);
      $get_plan_rate = PromotionPlanDiscounts::where('status',1)->get();
      $discount = 0;
      $discount_one = 0;
      $discount_two = 0;
      $discount_three = 0;

      foreach ($get_plan_rate as $key => $field) {
          if($field->id == 1){
            if($diff >= 0 && $diff <= $field->period){
              $discount = $field->discount;
              break;
              $discount_one = $field->period + 1;
            }
          }
          else if($field->id == 2){
            if($diff >= $discount_one && $diff <= $field->period  ){
              $discount = $field->discount;
              break;
              $discount_two = $field->period + 1;
            }
          }
          else if($field->id == 3){
            if($diff >= $discount_two && $diff <= $field->period  ){
              $discount = $field->discount;
              break;
            }
          }
      }
      $get_plan_price = UserPlanPrices::leftjoin('plan_term','plan_term.id','=','user_plan_price.term')
                                   ->select('plan_term.name','user_plan_price.*')
                                   // ->where('user_plan_price.usertype_id', '=', $value->usertype_id)
                                   ->where('user_plan_price.user_plan_id', '=', $get_plan->id)
                                   ->where('user_plan_price.status', '=', 1)
                                   ->orderBy('user_plan_price.price','desc')->get();

      $user_id = Auth::user()->id;
      $get_user = User::find($user_id)->first(); 

      $transaction_code = str_random(35);
      $new = true;
      $row = Paypalpayments::where('user_id','=', Auth::user()->id)->where('plan_id','=',$plan_id)->where('status','=','1')->first();
      if (is_null($row)) {
        $row = new Paypalpayments;
        $row->user_id = Auth::user()->id;
        $row->plan_id = $plan_id;
        $row->invoice_number = $transaction_code;
        $row->status = '1';
        $row->save();
         Session::put('invoice_number', $transaction_code);
      $new = false;
      } else {
         Session::put('invoice_number', $row->invoice_number);
      }    
      $this->data['get_user'] = $get_user;
      $this->data['payment'] = "123123";
      $form ="";
      $form .='<div class="panel-heading text-center"><span class="mediumText grayText"><strong>UPGRADE TO '.strtoupper($get_plan->plan_name).' PLAN </strong></span><span class="redText"><strong>'.$discount.'% OFF</strong></span></div>
          <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
          <input type="hidden" name="cmd" value="_xclick">
          <input type="hidden" name="hosted_button_id" value="W3QQ6C546WHF4">
          <INPUT TYPE="hidden" NAME="return" value="'.url('/').'/payment/plan/'.$row->invoice_number.'">
          <input type="hidden" name="business" id="element-to-hide" value="yakolakseller@gmail.com">
          <input type="hidden" name="item_name" value="'.strtoupper($get_plan->plan_name).' PLAN">
          <input type="hidden" name="item_number" value="">
          <input type="hidden" name="cm" value="'.strtoupper($get_plan->plan_name).'">
          <input type="hidden" name="currency_code" value="USD">
          <div class="form-group bottomMargin">
          <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">
          <input type="hidden" name="on0" value="Period">Period</label>
          <div class="inputGroupContainer">
          <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
          <select name="amount" class="form-control borderZero inputBox fullSize" >';
        foreach ($get_plan_price as $key => $field) {
           $parseDiscount =  $discount / 100;
           $total_price_no_discount = $field->price;
           $priceDiscount = $parseDiscount * $field->price ;
           $total_price_with_discount = $field->price - $priceDiscount;
           $plan_duration =$field->duration;
           $regular_price = $field->price;
           if($plan_duration == 1){
              $term_name =$field->name;   
           }else{
              $term_name = $field->name.'s';   
           }
            $form .='<option '.($field->id == $price_id  ? 'selected':'').' value="'.$total_price_with_discount.'">$'.$total_price_with_discount.' / '.$plan_duration.' '.$term_name.'</option>';
        }                              
      $form .='</select></div></div></div>
          <div class="form-group bottomMargin">
          <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label>
          <div class="inputGroupContainer">
          <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
          <input type="hidden" name="currency_code" value="USD">
          <input type="submit" id="btn-paypal" value="Pay with Paypal"  style="width:100%;padding: 6px 12px;" name="submit" title="PayPal - The safer, easier way to pay online!" class="paypal_btn btn blueButton borderZero noMargin">
          </div></div></div></form>';
      
        $form_points ="";
        $form_points .='<div class="panel-heading text-center"><span class="mediumText grayText"><strong>'.strtoupper($get_plan->plan_name).' PLAN </strong></span><span class="redText"><strong>(Available points: '.Auth::user()->points.')</strong></span></div>
            <form id="redeem_by_points" action="'.url('plan/payment/points').'" method="POST">
            '.csrf_field().'
            <div class="form-group bottomMargin">
            <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">
            <input type="hidden" name="on0" value="Period">Period</label>
            <div class="inputGroupContainer">
            <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
            <select name="price_id" class="form-control borderZero inputBox fullSize" >';
          foreach ($get_plan_price as $key => $field) {
             $total_price_with_discount = $field->points_exchange.' points';
             $price_id = $field->id;
             $plan_duration =$field->duration;
             $regular_price = $field->price;
             if($plan_duration == 1){
                $term_name =$field->name;
             }else{
                $term_name = $field->name.'s';   
             }
              $form_points .='<option '.($field->id == $price_id  ? 'selected':'').' value="'.$price_id.'">'.$total_price_with_discount.' / '.$plan_duration.' '.$term_name.'</option>';
          }                              
        $form_points .='</select></div></div></div>
            <div class="form-group bottomMargin">
            <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label>
            <div class="inputGroupContainer">
            <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
            <input id="points_submitter" type="button" style="width:100%;padding: 6px 12px;" name="submit" class="btn blueButton borderZero noMargin" value="Redeem using points">
            </div></div></div></form>';


          $this->data['form'] = $form;
          $this->data['form_points'] = $form_points;
          $value = Session::token();
          $row  = Advertisement::orderBy('id', 'desc')->first();
          $this->data['trans_id_number'] = $transaction_code;
          $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
          $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
          $this->data['countries'] = Country::all();
          $this->data['usertypes'] = Usertypes::all();
          $this->data['categories'] = Category::where('status',1)->get();

          if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }

         $this->data['get_userinfo']   = User::Join('countries','countries.id', '=', 'users.country')
                                             ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                             ->select('users.mobile','users.telephone','users.email','users.address','users.name', 'countries.countryName',  'country_cities.name as city')                          
                                             ->where(function($query) use ($user_id) {
                                                   $query->where('users.id', '=', $user_id);
                                        
                                            })->first();
          if (Auth::user()->country != null) {
             $this->data['get_usercountry'] = Country::find(Auth::user()->country);
            
          } else {

          $this->data['get_usercountry'] = null;
            
      }

     $this->data['get_usercity'] = city::find(Auth::user()->city);
     $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
     $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
     $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();

      
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
  
      // YAH IT WORKS TILL THE END!!!!
                           
      return View('payment.plan.form', $this->data);
    } 

       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    public function getPaymentBidPointsButton(){
      $id =Request::input('id');
      $created = new Carbon(Auth::user()->created_at);
      $diff =  Carbon::now()->diffInDays($created);
      $bid_discount = 0;
      $bid_discount_one = 0;
      $bid_discount_two = 0;
      $bid_discount_three = 0;
      $get_bid_discount = PromotionBidPlanDiscounts::where('status',1)->get();
      foreach ($get_bid_discount as $key => $field) {
          if($field->id == 1){
            if($diff >= 0 && $diff <= $field->period){
              $bid_discount = $field->discount;
              break;
              $bid_discount_one = $field->period + 1;
            }
          }
          else if($field->id == 2){
            if($diff >= $bid_discount_one && $diff <= $field->period  ){
              $bid_discount = $field->discount;
              break;
              $bid_discount_two = $field->period + 1;
            }
          }
          else if($field->id == 3){
            if($diff >= $bid_discount_two && $diff <= $field->period  ){
              $bid_discount = $field->discount;
              break;
            }
          }
      }


    $get_bid_points = BidPoints::find($id);
    $discountValue = $bid_discount / 100;
    $discountActualValue = $discountValue * $get_bid_points->price;
    $discountedPrice = $get_bid_points->price - $discountActualValue;



         $form = '';
        if($id){
           $transaction_code = str_random(35);

           $row = new PaypalPaymentForBidPoints;
           $row->user_id = Auth::user()->id;
           $row->bid_plan_id = $id;
           $row->save();
           $row->invoice_number = base64_encode($row->id.$transaction_code);
           $row->save();

           $get_details = BidPoints::where('id',$id)->where('status',1)->first();
           $form .='<span class="mediumText grayText"><strong></strong></span></div>
            <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="hosted_button_id" value="W3QQ6C546WHF4">
            <INPUT TYPE="hidden" NAME="return" value="'.url('/').'/payment/bid/'.$row->invoice_number.'">
            <input type="hidden" name="business" id="element-to-hide" value="yakolakseller@gmail.com">
            <input type="hidden" name="item_name" value="'.strtoupper($get_details->points).' Bid Points for '.$discountedPrice.' $'.'">
            <input type="hidden" name="item_number" value="">
            <input type="hidden" name="cm" value="'.strtoupper($get_details->points).'Bid Points for '.$discountedPrice.'$'.'">
            <input type="hidden" name="currency_code" value="USD">
            <div class="form-group bottomMargin">
            <div class="inputGroupContainer">
                 <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
            <input type="hidden" name="amount" value="'.$discountedPrice.'">
            <div class="form-group bottomMargin noTopMargin">
            <div class="inputGroupContainer">
            <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
            <input type="hidden" name="currency_code" value="USD">
            <input type="submit" value="Redeem Now"  style=""width:100%" name="submit" title="PayPal - The safer, easier way to pay online!" class="paypal_btn">
            <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
            </div></div></form>';
        $this->data['form'] = $form;

           
      }
      return Response::json($this->data);
    }
    public function ipnHandler(){
       return View('payment.plan.ipn');
    }
    public function success() 
    {

     $this->data['invoincecode'] = Session::get('invoice_number');




      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
       return View('payment.success', $this->data);
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

   
       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
 public function purchaseBannerPlacement()
    {
     $title = Session::get('title');
     $start_date = Session::get('start_date');
     $country = Session::get('country');
     $language = Session::get('language');
     $page = Session::get('page');
     $category = Session::get('category');
     $banner_placement_id = Session::get('banner_placement_id');
     $duration = Session::get('duration');
     $end_date = Session::get('end_date');
     $banner_placement_plan_id = Session::get('banner_placement_plan_id');
     $link = Session::get('link');
     $photo = Session::get('photo');

     $get_banner_placement    = BannerPlacement::where('id','=', Session::get('banner_placement_id'))->first();
        
     $this->data['title']        = Session::get('title');
     $this->data['placement']    = $get_banner_placement->name;
     $this->data['start_date']   = Session::get('start_date');
     $this->data['end_date']     = Session::get('end_date');
     $this->data['banner_transaction_number']    = Session::get('banner_transaction_number');

      $form ="";
      $form .='
          <div class="panel-heading"><span class="mediumText grayText"><strong>PURCHASE '.strtoupper($get_banner_placement->name).'</strong></span></div>
          <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
          <input type="hidden" name="cmd" value="_xclick">
          <input type="hidden" name="business" id="element-to-hide" value="tejerogerald123@gmail.com">
          <input type="hidden" name="item_name" value="'.strtoupper($get_banner_placement->name).' ">
          <input type="hidden" name="currency_code" value="USD">
          <input id="discount_amount" name="discount_amount" type="hidden" value="4.00">
          <div class="form-group bottomMargin">
          <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">
          <input type="hidden" name="on0" value="Period">Period</label>
          <div class="inputGroupContainer">
               <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
          <p>60% OFF THE REGULAR $9.95 MO</p>
          <select name="amount" class="form-control borderZero inputBox fullSize" >';
      $banner_prices = BannerPlacementPlans::where('banner_placement_id','=',$get_banner_placement->id)->where('status','=','1')->get();
           foreach ($banner_prices as $banner_price) {
             $form .='<option value="'.$banner_price->price.'" '.($banner_price->id==Session::get('banner_placement_plan_id') ? 'selected':'' ).'>'.'$'.$banner_price->price.' USD '.$banner_price->duration.' days'.'</option>';
           }        
      $form .='</select></div></div></div>
        <div class="form-group bottomMargin">
        <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label>
        <div class="inputGroupContainer">
             <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
        <input type="hidden" name="currency_code" value="USD">
        <input type="image" src="'.url('/').'/'.'img/paypal-checkout-button3.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </div></div></div></form>';
        
      $this->data['form'] = $form;

      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['get_userinfo'] = User::find($user_id);
       if (Auth::user()->country != null) {
           $this->data['get_usercountry'] = Country::find(Auth::user()->country);
        } else {
           $this->data['get_usercountry'] = null;  
       }

      $this->data['get_usercity'] = city::find(Auth::user()->city);
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return view('payment.banner.form', $this->data);

    }

}