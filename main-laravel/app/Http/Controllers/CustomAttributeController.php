<?php
namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use App\CustomAttributes;
use App\CustomAttributeValues;
use App\SubAttributes;
use App\Country;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;


class CustomAttributeController extends Controller
{

  public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/category/custom-attribute/refresh');
      $this->data['title'] = "Custom Attributes Management";
      $this->data['category'] = Category::with('subcategoryinfo.getOtherSubcategories')->get();
      $this->data['other'] =SubCategoriesTwo::with('getSubCatTwoCustomAttributes')->get();
      $this->data['custom_attribute'] = CustomAttributes::where('status','!=',2)->get();
      $this->data['countries'] = Country::where('status',1)->get();
      return View::make('admin.category-management.custom-attribute-management.list', $this->data);
  }
  public function getAttributeValues(){

      $rows = SubAttributes::with('attriValues')->get();
      
  }

  public function getCustomAttributes(){
    $id = Request::input('id');
    $get_unique_id = Category::where('id',$id)->first();
    if(is_null($get_unique_id)){
          $get_unique_id = SubCategory::where('id',$id)->first();
    }

    $row = SubAttributes::where('attri_id',$get_unique_id->unique_id)->where('status','!=',3)->get();
          
        $rows = '<div class="table-responsive" id="table-sub-categories"><table class="table" style="margin-bottom:0px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Attribute Name</small></th>' .
                      '<th><small>Type</small></th>' .
                      '<th><small>Status</small></th>' .
                      '<th class="text-right"><small>Tools</small></th>' .
                      '<tbody></tr>';
        if(count($row) > 0){
             foreach ($row as $key => $field) {
              // <a class="btn btn-xs btn-table btn-expand-subcat" data-id="'.$field->id.'"><i class="fa fa-plus"></i></a>
                    $rows .= '<tr data-id='.$field->id.' class="'.($field->status == 2 ? 'disabledColor':'').'">' .
                             '<td><small>'.$field->name.'</small></td>' .
                             '<td><small>'.($field->attribute_type == 2 ? 'Textbox':'Dropdown').'</small></td>' .
                             '<td><small>'.($field->status == 1 ? 'Enabled':'Disabled').'</small></td>' .
                             '<td>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>';
                 if($field->status == 1){
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>';
                 }else{
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>';
                 }
                 // $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat-two"><i class="fa fa-plus"></i></button>';

                $rows .=   '</td>'.
                         '</tr>';
              }       
        }else{
              $rows .= '<tr>' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td colspan ="5"><small><center>No Available Attributes</center></small></td>' .
                     '</tr>';

        }
                     // <td><small>'.($row->status == 1 ? 'Enabled':'Disabled').'</small></td>
                   //      <td>
                    //        <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                    //        <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>
                    //        <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>';
                // if($row->status == 1){
            //   $rows .= '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>';
            // }else{
            //   $rows .= '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>';
            // }
            // $rows .= '</td></tr>'; 
     $get_subcategories = SubCategory::where('cat_id',$get_unique_id->unique_id)->get();
       if(!is_null($get_subcategories)){
        foreach($get_subcategories as $row){
         $rows .='<tr style="text-indent:20px;background-color:white;padding:0px !important;" data-id="'.$row->id.'" class="'.($row->status == 2 ? 'disabledColor':'').'">'.
                '<td colspan="2"><small><button class="btn btn-xs btn-table btn-expand-sub" style="background-color:transparent;color:#337ab7;" data-id="'.$row->id.'"><i class="fa fa-plus"></i></button>'.$row->name.'</small></td>'.
                '<td colspan="2" class="text-right"><small>'.strtoupper($get_unique_id->country_code).'</small></td></tr>';
   
         
    
             // $rows .= ''; 
        }
      }
   
      $rows .= '</tbody></table></div>';

    
      


    return Response::json($rows);
  }
  public function getSubCustomAttributes(){

    $id = Request::input('id');
    $get_unique_id = Category::where('id',$id)->first();
    if(is_null($get_unique_id)){
          $get_unique_id = SubCategory::where('id',$id)->first();
    }

    $row = SubAttributes::where('attri_id',$get_unique_id->unique_id)->where('status','!=',3)->get();
          
        $rows = '<div class="table-responsive" id="table-sub-categories"><table class="table" style="margin-bottom:0px !important;text-indent:40px;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Attribute Name</small></th>' .
                      '<th><small>Type</small></th>' .
                      '<th><small>Status</small></th>' .
                      '<tbody></tr>';
        if(count($row) > 0){
             foreach ($row as $key => $field) {
              // <a class="btn btn-xs btn-table btn-expand-subcat" data-id="'.$field->id.'"><i class="fa fa-plus"></i></a>
                $rows .= '<tr data-parent-id = "'.$id.'" data-id='.$field->id.' class="'.($field->status == 2 ? 'disabledColor':'').'">' .
                             '<td><small>'.$field->name.'</small></td>' .
                             '<td><small>'.($field->attribute_type == 2 ? 'Textbox':'Dropdown').'</small></td>' .
                             '<td><small>'.($field->status == 1 ? 'Enabled':'Disabled').'</small></td>' .
                             '<td>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>';
                 if($field->status == 1){
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>';
                 }else{
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>';
                 }
                 // $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat-two"><i class="fa fa-plus"></i></button>';

                $rows .=   '</td>'.
                         '</tr>';
              }       
        }else{
              $rows .= '<tr>' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td colspan ="5"><small><center>No Available Attributes</center></small></td>' .
                     '</tr>';

        }

      $rows .= '</tbody></table></div>';

    
      


    return Response::json($rows);
  }
  public function doList(){

      $result['sort'] = Request::input('sort') ?: 'categories.created_at';
      $result['sort_second'] = 'created_at';
      if($result['sort'] == 'categories.name'){
        $result['sort_second'] = 'sub_categories.name';
      }
      if($result['sort'] == 'catCountry.countryCode'){
        $result['sort_second'] = 'subCatCountry.countryCode';
      }
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $country = Request::input('country');
      $per = Request::input('per') ?: 10;
      $active_countries = Country::where('status',1)->lists('countryCode')->toArray();

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

          $rows = Category::with('subcategoryinfo')
                            ->leftjoin('countries','countries.countryCode','=','categories.country_code')
                            ->select('categories.*','countries.countryCode')
                            ->where(function($query) use ($search) {
                                     $query->where('categories.name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('categories.country_code', 'LIKE', '%' . $search . '%');
                            })
                            ->where(function($query) use ($country, $active_countries) {
                              if($country)
                              {
                                $query->where('categories.country_code', 'LIKE', '%' . $country . '%');
                              }
                              else
                              {
                                $query->whereIn('categories.country_code', $active_countries);
                              }
                            })
                            ->orderBy($result['sort'], $result['order'])
                            ->groupBy('categories.id')
                            ->paginate($per);


      } else {
         $count = Category::with('subcategoryinfo')
                            ->leftjoin('countries','countries.countryCode','=','categories.country_code')
                            ->select('categories.*','countries.countryCode')
                            ->where(function($query) use ($search) {
                                    $query->where('categories.name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('categories.country_code', 'LIKE', '%' . $search . '%');
                            })
                            ->where(function($query) use ($country, $active_countries) {
                              if($country)
                              {
                                $query->where('categories.country_code', 'LIKE', '%' . $country . '%');
                              }
                              else
                              {
                                $query->whereIn('categories.country_code', $active_countries);
                              }
                            })
                            ->orderBy($result['sort'], $result['order'])
                            ->groupBy('categories.id')
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = Category::with('subcategoryinfo')
                            ->leftjoin('countries','countries.countryCode','=','categories.country_code')
                            ->select('categories.*','countries.countryCode')
                            ->where(function($query) use ($search) {
                                 $query->where('categories.name', 'LIKE', '%' . $search . '%')
                                       ->orWhere('categories.country_code', 'LIKE', '%' . $search . '%');
                            })
                            ->where(function($query) use ($country, $active_countries) {
                              if($country)
                              {
                                $query->where('categories.country_code', 'LIKE', '%' . $country . '%');
                              }
                              else
                              {
                                $query->whereIn('categories.country_code', $active_countries);
                              }
                            })
                            ->orderBy($result['sort'], $result['order'])
                            ->groupBy('categories.id')
                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
   public function save(){
        $new = true;
        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
             
             $row = SubAttributes::find(array_get($input, 'id'));

              if(is_null($row)) {
                  return Response::json(['error' => "The requested item was not found in the database."]);
              }
              // this is an existing row
              $new = false;
        }
        $rules = [
            'country_code' => 'required',
            'name' => 'required',
            'sequence_no' => 'required',
            'attri_id' => 'required',
            'attri_type' => 'required',

        ];
        // field name overrides
        $names = [
            'country_code' => 'Country',
            'name' => 'Attribute name',
            'sequence_no' => 'Order No',
            'attri_id' => 'Category',
            'attri_type' => 'Attribute type',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        $sub_attri_id_array = [];
        $custom_attri = array();
        $sub_attri_id = Input::get('sub_attri_id');
        $attri_id = Input::get('attri_id');
        $attribute_value_id = Input::get('attribute_value_id');
        $attribute_value_dropdown = Input::get('attribute_value_dropdown');
        if($custom_attri) {
          foreach($custom_attri as $key => $val) {
              if($custom_attri[$key]) {
                  $subrules = array(
                      "attribute_value_dropdown.$key" => 'required',
                  );

                  $subfn = array(
                      "attribute_value_dropdown.$key" => 'contact',
                  );

                  if($attribute_value_id[$key]) {
                      $subrules["attribute_value_id.$key"] = 'exists:production_case_contacts,id';
                  }

                  $rules = array_merge($rules, $subrules);
                  $names = array_merge($names, $subfn);
                  $custom_attri[] = $attribute_value_dropdown[$key];
              } else {
                  unset($attribute_value_id[$key]);
                  unset($attribute_value_dropdown[$key]);
              }
          }
      }
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if($new){
          //for new attribute
          if(count($sub_attri_id) > 0){
            foreach ($sub_attri_id as $key => $field) {
             $row_new = new SubAttributes;
             $row_new->attri_id     =  $field;
             $row_new->name     =  array_get($input, 'name');
             $row_new->attribute_type     =  array_get($input, 'attri_type');
             $row_new->save();
              if($attribute_value_dropdown) {
                foreach($attribute_value_dropdown as $key => $child) {
                    if($attribute_value_id[$key]) {
                        $cinfo = CustomAttributeValues::find($attribute_value_id[$key]);
                    } else {
                        $cinfo = new CustomAttributeValues;
                        $cinfo->sub_attri_id = $row_new->id;
                    }
                    $cinfo->name = $attribute_value_dropdown[$key];
                    $cinfo->save();
                }

              }
            }
          }else{
             $row_new = new SubAttributes;
             $row_new->attri_id     =  array_get($input,'attri_id');
             $row_new->name     =  array_get($input, 'name');
             $row_new->attribute_type     =  array_get($input, 'attri_type');
             $row_new->save();

             if($attribute_value_dropdown) {
                foreach($attribute_value_dropdown as $key => $child) {
                    if($attribute_value_id[$key]) {
                        $cinfo = CustomAttributeValues::find($attribute_value_id[$key]);
                    } else {
                        $cinfo = new CustomAttributeValues;
                        $cinfo->sub_attri_id = $row_new->id;
                    }
                    $cinfo->name = $attribute_value_dropdown[$key];
                    $cinfo->save();
                }


              }
          }
        }else{
        ///for existing attributes
          if(count($sub_attri_id) > 0){
           
            foreach ($sub_attri_id as $key => $field) {
             $row_old = SubAttributes::where('attri_id',$field)->first();

             if(count($row) < 0){
               $row_old = new SubAttributes;
             }else{
               $row_old->attri_id     =  $field;
               $row_old->name     =  array_get($input, 'name');
               $row_old->attribute_type     =  array_get($input, 'attri_type');
               $row_old->save();
             }
              
              if($attribute_value_dropdown) {
                foreach($attribute_value_dropdown as $key => $child) {
                    if($attribute_value_id[$key]) {
                        // this is an existing visa
                        $cinfo = CustomAttributeValues::find($attribute_value_id[$key]);
                    } else {
                        // this is a new visa
                        $cinfo = new CustomAttributeValues;
                        $cinfo->sub_attri_id = $row_old->id;
                    }
                    $cinfo->name = $attribute_value_dropdown[$key];
                    $cinfo->save();
                }
              }
            }
          }else{
             $row_old = SubAttributes::find(array_get($input, 'id'));
             $row_old->attri_id     =  array_get($input,'attri_id');
             $row_old->name     =  array_get($input, 'name');
             $row_old->attribute_type     =  array_get($input, 'attri_type');
             $row_old->save();

             if($attribute_value_dropdown) {
                foreach($attribute_value_dropdown as $key => $child) {
                    if($attribute_value_id[$key]) {
                        // this is an existing visa
                        $cinfo = CustomAttributeValues::find($attribute_value_id[$key]);
                    } else {
                        $cinfo = new CustomAttributeValues;
                        $cinfo->sub_attri_id = $row_old->id;
                    }
                    $cinfo->name = $attribute_value_dropdown[$key];
                    $cinfo->save();
                }
              }
          }

        }
        if($new){
          return Response::json(['body' => 'Attribute created successfully.']);
        }else{
          return Response::json(['body' => 'Attribute updated successfully.']);
        }
      
   }
   public function manipulateMainAttri(){
    $row = SubAttributes::find(Request::input('id'));

    if(!is_null($row)){
       $get_country_code = Category::where('unique_id',$row->attri_id)->first();
       if(!is_null($get_country_code)){
           $result['country_code'] = strtoupper($get_country_code->country_code);
           $get_country_code_categories=Category::where('country_code',$get_country_code->country_code)->get();
            $category ='';
           if(!is_null($get_country_code_categories)){
            foreach ($get_country_code_categories as $key => $field) {
             $category .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $row->attri_id ? 'selected':'').'>'.$field->name.'</option>';
            }
           }
       }else{
          $subcat_info = SubCategory::where('unique_id',$row->attri_id)->first();
          $result['country_code'] = strtoupper($subcat_info->country_code);
          $get_categories = Category::where('unique_id',$subcat_info->cat_id)->first();
          $get_country_code_categories=Category::where('country_code',$subcat_info->country_code)->get();
          $category = '';
          if(!is_null($get_country_code_categories)){
            foreach ($get_country_code_categories as $key => $field) {
             $category .= '<option value="'.$field->unique_id.'"'.($field->unique_id == $subcat_info->cat_id ? 'selected':'').'>'.$field->name.'</option>';
            }
          } 
          $get_subcategories = SubCategory::where('cat_id',$get_categories->unique_id)->get();
           if(!is_null($get_subcategories)){
            $rows = '<select class="form-control" multiple="multiple" id="row-sub_attri_id" name="sub_attri_id[]">';
            foreach ($get_subcategories as $key => $field) {
              $rows .= '<option value="'.$field->unique_id.'" '.($field->unique_id == $row->attri_id ? 'selected':'').'>'.$field->name.'</option>';
            }
            $rows .= '</select>';
           $result['rows'] = $rows;
          }
       }

    }
    $get_attribute_values = CustomAttributeValues::where('sub_attri_id',Request::input('id'))->get();
    $attribute_values='';
    if(count($get_attribute_values) > 0){
       foreach ($get_attribute_values as $key => $field) {
        $attribute_values .= '<tr><td class="col-xs-12"><div class="col-xs-12 input-group"><input type="text" name="attribute_value_dropdown[]" class="form-control borderzero col-lg-12 disable-view" maxlength="30" value="'.$field->name.'"></div></td><td class="hide-sta text-right"><input type="hidden" name="attribute_value_id[]" value="'.$field->id.'"><input type="hidden" name="attribute_status[]" id="attribute_status" value="'.$field->status.'"><button type="button" value="" class="hide-view btn-del-attribute-value disable-view" title="Remove" style="background-color:transparent;border:0;color:red;"> <i class="fa fa-close"> </i></button></td></tr>';
      }
    }else{
       $attribute_values .= '<tr><td class="col-xs-12"><div class="col-xs-12 input-group"><input type="text" name="attribute_value_dropdown[]" class="form-control borderzero col-lg-12 disable-view" maxlength="30"></div></td><td class="hide-sta text-right"><input type="hidden" name="attribute_value_id[]"><input type="hidden" name="attribute_status[]" id="attribute_status"><button type="button" value="" class="hide-view btn-del-attribute-value disable-view" style="background-color:transparent;border:0;color:red;" title="Remove"> <i class="fa fa-close"> </i></button></td></tr>';
    }

    $result['attribute_values'] = $attribute_values;
    $result['categories'] = $category;

    $result['data'] = $row;

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }
   public function enableAttri(){
    $row = SubAttributes::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status = 1;
      $row->save();
      return Response::json(['body'=>'Attribute enabled']);
    }else{
     return Response::json(['error'=>'Invalid row specified']);
    }
   }
   public function disableAttri(){
   $row = SubAttributes::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status = 2;
      $row->save();
      return Response::json(['body'=>'Attribute disabled']);
    }else{
     return Response::json(['error'=>'Invalid row specified']);
    }
   }
   public function delete(){
    $row = SubAttributes::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = 3;
        $row->save();
        return Response::json(['body' => 'Attribute deleted.']);
    }else{
      return Response::json(['error' => "Invalid row specified"]);
    }
   }
  public function getCategories(){
    $row = Category::where('country_code',Request::input('id'))->get();
    if(!is_null($row)){
      $rows = '';
      foreach ($row as $key => $field) {
        $rows .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
      }
      return Response::json($rows);
    }else{
      return Response::json(['body' => 'Invalid row specified']);
    }
  }
  public function getSubCategories(){
    $row = SubCategory::where('cat_id',Request::input('id'))->get();

    if(!is_null($row)){
      $rows = '<select class="form-control disable-view" multiple="multiple" id="row-sub_attri_id" name="sub_attri_id[]">';
      foreach ($row as $key => $field) {
        $rows .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
      }
      $rows .= '</select>';
      return Response::json($rows);
    }else{
      return Response::json(['body' => 'Invalid row specified']);
    }
  }  
}