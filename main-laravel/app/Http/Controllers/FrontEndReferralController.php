<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use Carbon\Carbon;
use App\UserImportContacts;
use Input;
use Validator;
use Redirect;
use Session;
use Paginator;
use Response;
use App\AdvertisementPhoto;
use App\Advertisement;
use App\UserAdComments;
use App\AdRating;

class FrontEndReferralController extends Controller
{


 public function index() {
      $row  = Advertisement::orderBy('id', 'desc')->first();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();     
 	    $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
   	  $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      
      // YAH IT WORKS TILL THE END!!!!                         
      return view('contact.import', $this->data);
 }
  public function callback(Request $request) {
      $row  = Advertisement::orderBy('id', 'desc')->first();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();     
 	    $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
   	  $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
     
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
    
      // YAH IT WORKS TILL THE END!!!!

      return view('contact.callback', $this->data);
 }


	public function saveImport()
{
        $input = Input::all();
        $new = true;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }

        foreach (Input::get('email') as $emails)
        {
          $row = UserImportContacts::where('user_id','=',$user_id)->where('email','=',$emails)->first();
          if (is_null($row)) {
              $row = new UserImportContacts;          
              $row->email = $emails;
              $row->user_id = $user_id;
              $row->Save();
          }

        }
          if($new){
            return Response::json(['body' => "Contacts Successfully Imported"]);
          } 
          else {
               return Response::json(['body' => "Advertisement Successfully updated"]);
        }
}

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    public function referralRegister($referral_code){
      if (Auth::check()) {
        return redirect('/');
        Session::forget($referral_code);
      } else {
         Session::flash('referral_code', $referral_code);
      }
      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')                                       
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 
      $this->data['adslatestbids_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')  
                                                   ->where('advertisements.status', '!=', '4')                                                                           
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 

      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                           
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['latestads_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      //$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get(); 
                                                 
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      $master_poster_total = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                          ->where('ads_type_id',1)
                                          ->where('status',1)
                                          ->groupBy('user_id')
                                          ->orderBy('count', 'desc')
                                          ->take(4)
                                          ->get();
      $master_poster_ads = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                        ->where('ads_type_id',1)
                                        ->where('status',1)
                                        ->groupBy('user_id')
                                        ->orderBy('count', 'desc')
                                        ->take(4)
                                        ->get();
      $master_poster_auctions = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                              ->where('ads_type_id',2)
                                              ->where('status',1)
                                              ->groupBy('user_id')
                                              ->orderBy('count', 'desc')
                                              ->take(4)->get();
      $master_poster_0 = User::where('id',$master_poster_total[0]['user_id'])->first();
      $master_poster_1 = User::where('id',$master_poster_total[1]['user_id'])->first();
      $master_poster_2 = User::where('id',$master_poster_total[2]['user_id'])->first();
      $master_poster_3 = User::where('id',$master_poster_total[3]['user_id'])->first();
      if($master_poster_total[0]['count'] > 1){
        if(!is_null($master_poster_0)){
          $master_poster_data_0 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_0->alias.'"><img src="'.url('uploads').'/'.$master_poster_0->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_0->alias.'" class="blueText">'.$master_poster_0->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[0]['count'].' Ads & '.$master_poster_auctions[0]['count'].' Bids Listings</p></div></div></div>';
        }else{
            $master_poster_data_0 = '';
        }
      }else{
        $master_poster_data_0 = '';
      }
      if($master_poster_total[1]['count'] > 1){
        if(!is_null($master_poster_1)){
          $master_poster_data_1 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_1->alias.'"><img src="'.url('uploads').'/'.$master_poster_1->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_1->alias.'" class="blueText">'.$master_poster_1->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[1]['count'].' Ads & '.$master_poster_auctions[1]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_1 = '';
        }
      }else{
        $master_poster_data_1 = '';
      }
      if($master_poster_total[2]['count'] > 1){
        if(!is_null($master_poster_2)){
          $master_poster_data_2 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_2->alias.'"><img src="'.url('uploads').'/'.$master_poster_2->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_2->alias.'" class="blueText">'.$master_poster_2->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[2]['count'].' Ads & '.$master_poster_auctions[2]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_2 = '';
        }
      }else{
        $master_poster_data_2 = '';
      }
      if($master_poster_total[3]['count'] > 1){
        if(!is_null($master_poster_3)){
          $master_poster_data_3 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_3->alias.'"><img src="'.url('uploads').'/'.$master_poster_3->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_3->alias.'" class="blueText">'.$master_poster_3->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[3]['count'].' Ads & '.$master_poster_auctions[3]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_3 = '';
        }
      }else{
        $master_poster_data_3 = '';
      }
      $this->data['master_poster_html_0'] = $master_poster_data_0;
      $this->data['master_poster_html_1'] = $master_poster_data_1;
      $this->data['master_poster_html_2'] = $master_poster_data_2;
      $this->data['master_poster_html_3'] = $master_poster_data_3;
      // YAH IT WORKS TILL THE END!!!!

      return redirect()->action('HomeController@index');

    }
}
   