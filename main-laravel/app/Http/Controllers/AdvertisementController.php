<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\Country;
use App\City;
use App\SubAttributes;
use App\SubCategory;
use App\CustomAttributeValues;
use App\CustomAttributesForDropdown;
use App\CustomAttributesForTextbox;
use App\Http\Controllers\Controller;
use App\AdvertisementType;
use App\AdvertisementBidDurationDropdown;
use App\AdManagementSettings;
use App\AuctionBidders;
use App\SubCategoriesTwo;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Mail;
use Carbon\Carbon;
use App\AdRating;
use App\UserAdComments;
use App\UserAdCommentReplies;
use App\AdvertisementSpamAndBots;
use App\SettingForExchangePoints;
use App\SettingForSiteKey;
use App\AdFeatureTransactionHistory;
use App\UserNotificationsHeader;
use App\UserNotifications;

class AdvertisementController extends Controller
{
   public function indexCommentsAndReviews(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
    $result = $this->doListCommentsAndReviews();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['title'] = "Advertisement Management - Comments and Reviews";
    $this->data['refresh_route'] = url('admin/ads/comments-and-reviews/refresh');
    return View::make('admin.ads-management.comments-and-reviews.list', $this->data);
   }
   public function doListCommentsAndReviews(){
    $result['sort'] = Request::input('sort') ?: 'user_ad_comments.created_at';
    $result['order'] = Request::input('order') ?: 'desc';
    $search = Request::input('search') ? : '';
    $status = Request::input('status') ? : '';
    $listing_type = Request::input('listing_type') ? : '';
    $ad_type = Request::input('ad_type') ? : '';
    $per = Request::input('per') ?: 10;
          if (Request::input('page') != '»') {
                Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
          });

          $rows =  UserAdComments::leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                ->leftjoin('ad_ratings','ad_ratings.id','=','user_ad_comments.rating_id')
                                ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                ->select('users.name','users.username','user_ad_comments.*','ad_ratings.rate','advertisements.title','advertisements.slug','advertisements.parent_category_slug','advertisements.category_slug','advertisements.ad_type_slug','advertisements.country_code')
                                ->where(function($query) use ($search) {
                                        $query->where('users.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('user_ad_comments.id', 'LIKE', '%' . $search . '%');
                               })
                                ->where(function($query) use ($status) {
                                        $query->where('user_ad_comments.status', 'LIKE', '%' . $status . '%');
                               })
                               ->where(function($query) use ($ad_type) {
                                        $query->where('user_ad_comments.ad_type', 'LIKE', '%' . $ad_type . '%');
                               })
                               ->where(function($query) use ($listing_type) {
                                        $query->where('user_ad_comments.listing_type', 'LIKE', '%' . $listing_type . '%');
                               })
                               ->selectRaw("(SELECT COUNT(*) as CommentReply
                                FROM yakolak_user_ad_comment_replies
                                WHERE status = 2
                                AND comment_id = yakolak_user_ad_comments.id) as CommentReply")
                              ->where('user_ad_comments.status','!=',5)
                              // ->groupBy('advertisements.id')
                              // ->orderBy('CommentReply','desc')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                  // dd($rows);
        } else {
         $count =  UserAdComments::leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                ->leftjoin('ad_ratings','ad_ratings.id','=','user_ad_comments.rating_id')
                                ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                ->select('users.name','users.username','user_ad_comments.*','ad_ratings.rate','advertisements.title')
                                ->where(function($query) use ($search) {
                                       $query->where('users.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('user_ad_comments.id', 'LIKE', '%' . $search . '%');
                               })
                                ->where(function($query) use ($status) {
                                        $query->where('user_ad_comments.status', 'LIKE', '%' . $status . '%');
                               })
                               ->where(function($query) use ($ad_type) {
                                        $query->where('user_ad_comments.ad_type', 'LIKE', '%' . $ad_type . '%');
                               })
                               ->where(function($query) use ($listing_type) {
                                        $query->where('user_ad_comments.listing_type', 'LIKE', '%' . $listing_type . '%');
                               })
                               ->selectRaw("(SELECT COUNT(*) as CommentReply
                                FROM yakolak_user_ad_comment_replies
                                WHERE status = 2
                                AND comment_id = yakolak_user_ad_comments.id) as CommentReply")
                              ->where('user_ad_comments.status','!=',5)
                              // ->orderBy('CommentReply', 'desc')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

        $rows = UserAdComments::leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                ->leftjoin('ad_ratings','ad_ratings.id','=','user_ad_comments.rating_id')
                                ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                ->select('users.name','users.username','user_ad_comments.*','ad_ratings.rate','advertisements.title')
                                ->where(function($query) use ($search) {
                                       $query->where('users.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('user_ad_comments.id', 'LIKE', '%' . $search . '%');
                               })
                                ->where(function($query) use ($status) {
                                        $query->where('user_ad_comments.status', 'LIKE', '%' . $status . '%');
                               })
                               ->where(function($query) use ($ad_type) {
                                        $query->where('user_ad_comments.ad_type', 'LIKE', '%' . $ad_type . '%');
                               })
                               ->where(function($query) use ($listing_type) {
                                        $query->where('user_ad_comments.listing_type', 'LIKE', '%' . $listing_type . '%');
                               })
                               ->selectRaw("(SELECT COUNT(*) as CommentReply
                                FROM yakolak_user_ad_comment_replies
                                WHERE status = 2
                                AND comment_id = yakolak_user_ad_comments.id) as CommentReply")
                              ->where('user_ad_comments.status','!=',5)
                              // ->orderBy('CommentReply', 'desc')
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
         // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          return $result;
        }
   }
   public function indexSpamAndBots(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

    $result = $this->doListSpamAndBots();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['site_keys'] = SettingForSiteKey::latest()->take(1)->get();
    $this->data['title'] = "Advertisement Management - Spam and Bots";
    $this->data['refresh_route'] = url('admin/ads/spam-and-bots/refresh');
    return View::make('admin.ads-management.spam-and-bots.list', $this->data);
   }
   public function doListSpamAndBots(){
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search') ? : '';

      $status = Request::input('status') ? : '';
      $per = Request::input('per') ?: 10;
          if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
          });

              $rows =  AdvertisementSpamAndBots::where('status','!=',3)
                              ->where(function($query) use ($search) {
                                        $query->orWhere('restricted_word', 'LIKE', '%' . $search . '%');
                                    })
                              ->where(function($query) use ($status) {
                                        $query->orWhere('status', 'LIKE', '%' . $status . '%');
                                    })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                   
        } else {
             $count =   AdvertisementSpamAndBots::where('status','!=',3)
                              ->where(function($query) use ($search) {
                                        $query->orWhere('restricted_word', 'LIKE', '%' . $search . '%');
                                    })
                               ->where(function($query) use ($status) {
                                        $query->orWhere('status', 'LIKE', '%' . $status . '%');
                                })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  AdvertisementSpamAndBots::where('status','!=',3)
                              ->where(function($query) use ($search) {
                                        $query->orWhere('restricted_word', 'LIKE', '%' . $search . '%');
                                    })
                              ->where(function($query) use ($status) {
                                        $query->orWhere('status', 'LIKE', '%' . $status . '%');
                                    })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
         // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          return $result;
        }
       
   }
   public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $result = $this->doList();
      $this->data['rows'] = $result['rows'];
      $this->data['get_auction'] = $result['get_auction'];
      $this->data['ad_management_settings'] = $result['ad_management_settings'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management - Basic Ads";
      $this->data['ad_types']  = AdvertisementType::where('status',1)->get();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['custom_attributes'] = '';
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      

      return View::make('admin.ads-management.regular-ads.list', $this->data);
   }
   public function indexFeature(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $result = $this->doListFeature();
      $this->data['rows'] = $result['rows'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['price_matrix'] = SettingForExchangePoints::where('status',1)->get();
      $this->data['refresh_route'] = url('admin/ads/feature/refresh');
      $this->data['title'] = "Advertisement Management - Featured Ads";
      $this->data['ad_types']  = AdvertisementType::where('status',1)->get();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['custom_attributes'] = '';
      return View::make('admin.ads-management.featured-ads.list', $this->data);
   }
   public function doListFeature(){
      $result['sort'] = Request::input('sort') ?: 'ad_feature_transaction_history.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $ad_type = Request::input('ad_type') ? : '';
      $status = Request::input('status') ? : '';
      $country = Request::input('country') ? : '';
      $per = Request::input('per') ?: 10;
      if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
      });
      $rows = AdFeatureTransactionHistory::leftJoin('advertisements','advertisements.id', '=', 'ad_feature_transaction_history.ad_id')
                                           ->leftJoin('categories','categories.unique_id', '=', 'advertisements.parent_category')
                                           ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                           ->leftJoin('exchange_point_settings','exchange_point_settings.id', '=', 'ad_feature_transaction_history.duration_metric')
                                           ->select('advertisements.*','ad_feature_transaction_history.status as transact_status','ad_feature_transaction_history.duration','ad_feature_transaction_history.payment_method','ad_feature_transaction_history.duration_metric','ad_feature_transaction_history.cost','ad_feature_transaction_history.transaction_no','categories.name as cat_name','users.name as user_name','ad_feature_transaction_history.created_at as feature_created_at','exchange_point_settings.name as metric_name')
                                           // ->where(function($query) use ($ad_type) {
                                           //     $query->where('advertisements.ads_type_id','LIKE', '%' . $ad_type . '%');    
                                           //   })
                                            ->where(function($query) use ($status) {
                                                $query->where('advertisements.feature','LIKE', '%' . $status . '%');    
                                             })
                                            ->where(function($query) use ($country) {
                                                $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                             })
                                             ->where(function($query) use ($search) {
                                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.title', 'LIKE', '%' . $search . '%');
                                              })
                                            ->groupBy('advertisements.id')
                                            ->where('advertisements.feature','!=',0)
                                            ->orderBy($result['sort'], $result['order'])
                                            ->paginate($per);
                                            // dd($rows);
      // $rows =  Advertisement::with('getAdvertisementType')
      //                         ->with('getAdvertisementCategory')
      //                         ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
      //                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
      //                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
      //                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
      //                         ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
      //                                    'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code') 
      //                         ->where('advertisements_photo.primary', '=', '1')
      //                         ->where(function($query) use ($search) {
      //                                   $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
      //                                         ->orWhere('advertisements.title', 'LIKE', '%' . $search . '%');
      //                               })

      //                         ->where(function($query) use ($ad_type) {
      //                             $query->where('advertisements.ads_type_id','LIKE', '%' . $ad_type . '%');    
      //                          })
      //                         ->where(function($query) use ($status) {
      //                             $query->where('advertisements.feature','LIKE', '%' . $status . '%');    
      //                          })
      //                         ->where(function($query) use ($country) {
      //                             $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
      //                          })
      //                         ->where('advertisements.feature','!=',0)
      //                         ->where('advertisements.status',1)
      //                         ->orderBy($result['sort'], $result['order'])
      //                         ->paginate($per);
          


        } else {
             $count =  Advertisement::with('getAdvertisementType')
                              ->with('getAdvertisementCategory')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.title', 'LIKE', '%' . $search . '%');
                                              })
                               ->where(function($query) use ($ad_type) {
                                  $query->where('advertisements.ads_type_id','LIKE', '%' . $ad_type . '%');    
                               })
                               ->where(function($query) use ($status) {
                                  $query->where('advertisements.feature','LIKE', '%' . $status . '%');    
                               })
                               ->where(function($query) use ($country) {
                                  $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                               })
                              ->groupBy('advertisements.id')
                              ->where('advertisements.feature','!=',0)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->with('getAdvertisementCategory')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('advertisements.title', 'LIKE', '%' . $search . '%');
                                              })
                              ->where(function($query) use ($ad_type) {
                                  $query->where('advertisements.ads_type_id','LIKE', '%' . $ad_type . '%');    
                               })
                               ->where(function($query) use ($status) {
                                  $query->where('advertisements.feature','LIKE', '%' . $status . '%');    
                               })
                              ->where(function($query) use ($country) {
                                  $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                               })
                              ->groupBy('advertisements.id')
                              ->where('advertisements.feature','!=',0)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
          $countries = Country::where('status',1)->orderBy('countryName','asc')->get();
          $ad_management_settings = AdManagementSettings::first();
          $city = City::all();
         // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          return $result;
        }
       
   }
   public function indexAuction(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $result = $this->doListAuction();
      $this->data['rows'] = $result['rows'];
      $this->data['get_auction'] = $result['get_auction'];
      $this->data['ad_management_settings'] = $result['ad_management_settings'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/auction/refresh');
      $this->data['title'] = "Advertisement Management - Auction Ads";
      $this->data['ad_types']  = AdvertisementType::where('status',2)->get();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['custom_attributes'] = '';
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      

      return View::make('admin.ads-management.auction-ads.list', $this->data);
   }
   public function doList() {
      // sort and order
      $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status') ? : '';
      $country = Request::input('country') ? : '';                                   
      $per = Request::input('per') ?: 10;
      if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
      });

      // $search_category = Category::lists('unique_id');
      
      // if ($search) {
      //   $search_category = Category::where('name', 'LIKE', '%' . $search . '%')->lists('unique_id');
      // }
      // dd($search_category);
      $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',1)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                  
        } else {
             $count =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',1)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  } 
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',1)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
       
       $get_auction = "";
       $get_auction .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
                      '<div class="form-group">'.
                       '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Limit Amount</label>'.
                         '<div class="col-sm-9">'.
                           '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Start Amount</label>'.
                        '<div class="col-sm-9">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" >'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Minimum Allowed Bid</label>'.
                        '<div class="col-sm-9">'.
                          '<input type="number"  class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
                        '<div class="col-sm-9">'.
                          '<div class="col-sm-6 noleftPadding">'.
                          '<div class="form-group">'.
                            '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                            '<div class="col-sm-8">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start">'.
                              '</div>'.
                            '</div>'.
                          '</div>'.
                          '<div class="col-sm-6 norightPadding">'.
                            '<div class="form-group">'.
                              '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                              '<div class="col-sm-8"> '.        
                           '<input type="text" name="value" id="row-value" class="form-control borderZero" value="">' .
                          '<select name="bid_duration_dropdown_id" id="row-bid_duration_dropdown_id" class="form-control fullSize hide" required="required">'.
                          '<option class="hide" selected>Select:</option>';
                                $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get(); 
                                foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
                                          $get_auction .= '<option value="' . $bid_field->id . '" >'. $bid_field->value . '</option>';
                                         }
        $get_auction .='</select>'.'</div>'.
                            '</div>
                          </div>
                        </div>
                      </div>';

         $countries = Country::where('status',1)->orderBy('countryName','asc')->get();
         $ad_management_settings = AdManagementSettings::first();
         $city = City::all();

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          $result['ad_management_settings'] = $ad_management_settings->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          $result['get_auction']  = $get_auction;
          $result['ad_management_settings'] = $ad_management_settings;
          return $result;
        }
    }
     public function doListAuction() {
      // sort and order
        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $status = Request::input('status') ? : '';
        $country = Request::input('country') ? : '';
        $per = Request::input('per') ?: 10;
        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

      $rows =   Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status','advertisements.bid_limit_amount')

                              ->selectRaw("(SELECT yakolak_auction_bidders.bid
                                            FROM yakolak_auction_bidders 
                                            WHERE yakolak_auction_bidders.ads_id = yakolak_advertisements.id 
                                            ORDER BY yakolak_auction_bidders.bid DESC LIMIT 1) as ad_bidder") 

                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                              ->where(function($query) use ($status) {
                                  $query->where('advertisements.status', 'LIKE', '%' . $status . '%');
                                })
                               ->where(function($query) use ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',2)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                             
                   
        } else {
             $count =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status','advertisements.bid_limit_amount') 
                               ->selectRaw("(SELECT yakolak_auction_bidders.bid
                                            FROM yakolak_auction_bidders 
                                            WHERE yakolak_auction_bidders.ads_id = yakolak_advertisements.id 
                                            ORDER BY yakolak_auction_bidders.bid DESC LIMIT 1) as ad_bidder") 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                  $query->where('advertisements.status', 'LIKE', '%' . $status . '%');
                               })
                               ->where(function($query) use ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',2)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status','advertisements.bid_limit_amount') 
                              ->selectRaw("(SELECT yakolak_auction_bidders.bid
                                            FROM yakolak_auction_bidders 
                                            WHERE yakolak_auction_bidders.ads_id = yakolak_advertisements.id 
                                            ORDER BY yakolak_auction_bidders.bid DESC LIMIT 1) as ad_bidder") 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                  $query->where('advertisements.status', 'LIKE', '%' . $status . '%');
                               })
                               ->where(function($query) use ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.ads_type_id',2)
                              // ->whereIn('advertisements.parent_category', $search_category)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
       
       $get_auction = "";
       $get_auction .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
                      '<div class="form-group">'.
                       '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Limit Amount</label>'.
                         '<div class="col-sm-9">'.
                           '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Start Amount</label>'.
                        '<div class="col-sm-9">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" >'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Minimum Allowed Bid</label>'.
                        '<div class="col-sm-9">'.
                          '<input type="number"  class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
                        '<div class="col-sm-9">'.
                          '<div class="col-sm-6 noleftPadding">'.
                          '<div class="form-group">'.
                            '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                            '<div class="col-sm-8">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start">'.
                              '</div>'.
                            '</div>'.
                          '</div>'.
                          '<div class="col-sm-6 norightPadding">'.
                            '<div class="form-group">'.
                              '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                              '<div class="col-sm-8"> '.        
                           '<input type="text" name="value" id="row-value" class="form-control borderZero" value="">' .
                          '<select name="bid_duration_dropdown_id" id="row-bid_duration_dropdown_id" class="form-control fullSize hide" required="required">'.
                          '<option class="hide" selected>Select:</option>';
                                $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get(); 
                                foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
                                          $get_auction .= '<option value="' . $bid_field->id . '" >'. $bid_field->value . '</option>';
                                         }
        $get_auction .='</select>'.'</div>'.
                            '</div>
                          </div>
                        </div>
                      </div>';

         $countries = Country::where('status',1)->orderBy('countryName','asc')->get();
         $ad_management_settings = AdManagementSettings::first();
         $city = City::all();

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          $result['ad_management_settings'] = $ad_management_settings->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          $result['get_auction']  = $get_auction;
          $result['ad_management_settings'] = $ad_management_settings;
          return $result;
        }
    }

    public function edit(){
      $ads_id = Request::input('ads_id');
      $row = Advertisement::join('advertisements_photo', 'advertisements.id', '=', 'advertisements_photo.ads_id')
                               ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                               ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                               ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                               ->leftJoin('ad_bid_duratation_dropdown','advertisements.bid_duration_dropdown_id', '=', 'ad_bid_duratation_dropdown.id')
                               ->select('advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.sub_category_id','advertisements.parent_category', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'advertisements.address', 'advertisements.youtube', 'advertisements.bid_limit_amount', 'advertisements.bid_start_amount', 'advertisements.minimum_allowed_bid', 'advertisements.bid_duration_start', 'advertisements.bid_duration_dropdown_id',
                                         'ad_bid_duratation_dropdown.value')    
                               ->where(function($query) use($ads_id){
                                             $query->where('advertisements.id', '=', $ads_id);                                  
                                      })->first();

      $get_photo = AdvertisementPhoto::where('ads_id','=', $ads_id)->get(); 
        $parent_category = "";
      $get_parent_category  = Category::where('status',1)->get();
        foreach ($get_parent_category as $key => $field) {
         $parent_category .= '<option value="'.$field->unique_id.'" '.($field->unique_id == $row->parent_category ? 'selected':'').'>'.$field->name.'</option>';
        }
        $category = "";
      $get_category = SubCategory::where('status',1)->where('cat_id',$row->parent_category)->get();
        foreach ($get_category as $key => $field) {
        $category .= '<option value="'.$field->unique_id.'" '.($field->unique_id == $row->category_id ? 'selected':'').'>'.$field->name.'</option>';
        }
        $sub_category = "";
      $get_sub_category = SubCategoriesTwo::where('status',1)->where('subcat_main_id',$row->category_id)->get();
        foreach ($get_sub_category as $key => $field) {
           $sub_category .= '<option value="'.$field->unique_id.'" '.($field->unique_id == $row->sub_category_id ? 'selected':'').'>'.$field->name.'</option>';
        }
      //Append
      $result['photo']          = $get_photo;                      
      $result['row']            = $row;
      $result['parent_category']  = $parent_category ; 
      if($category == ""){
          $result['category']  = null;
      }else{
          $result['category']  = $category;
      }
      if($sub_category == ""){
          $result['sub_category']  = null;
      }else{
          $result['sub_category']  = $sub_category;
      }                                                       
      // $result['sub_category'] = $sub_category == "" ? null:$sub_category;
     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
   }

    public function saveRestrictedWord(){
        $new = true;
        $input = Input::all();
        if(array_get($input, 'id')) {
            // get the user info
            $row = AdvertisementSpamAndBots::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
         $rules = [
             'restricted_word'                    => 'required',
          
        ];
        // field name overrides
        $names = [   
            'restricted_word'                    => 'Restricted Word',
        ];  

           // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        if($new) {
          $row = new AdvertisementSpamAndBots;
        }
          $row->restricted_word = array_get($input,'restricted_word');
          $row->save();

        if($new){
               return Response::json(['body' => "Restricted Word Created"]);
        } else {
               return Response::json(['body' => "Restricted Word updated"]);
        }

    }
    public function updateFeatureSettings(){
      $row = SettingForExchangePoints::where('status',1)->get();
      if(!is_null($row)){
        return Response::json($row);
      }
    }
    public function saveFeaturedSettings(){
        $new = true;
        $input = Input::all();
        $price = Input::get('price');
        $points = Input::get('points');

        if(array_get($input, 'id')) {
            // get the user info
            $row = SettingForExchangePoints::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
           $rules = [
               'price'                    => 'required|min:1',
               'points'                    => 'required|min:1',
            
          ];
            $names = [   
            'price'                    => 'price',
            'points'                    => 'points',
           ];  
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        foreach ($points as $key => $value) {
            $row = SettingForExchangePoints::find($key);
            if(!is_null($row)){
                $row->price = $price[$key];
                $row->points = $points[$key];
                $row->save();
            }
          
          }

        return Response::json(['body' => "Featured Settings updated"]);

    }
    public function getRecaptchaSetting(){
      $row =SettingForSiteKey::all();
      if(!is_null($row)){
         return Response::json($row);
      }else{
         return Response::json(['error' => "Error"]);
      }
    }
    public function save(){
        $new = true;
        $input = Input::all();
        $ads_type_id = Input::get('ads_type_id');
        $category = Input::get('category');
        $photo = array_get($input, 'photo');
        $rem_img_upload = Input::get('rem_img_upload');
        
        $text_field = Input::get('text_field');
        $dropdown_field = Input::get('dropdown_field');
        $attribute_textbox_id = Input::get('attribute_textbox_id');
        $attribute_dropdown_id = Input::get('attribute_dropdown_id');

        $text_field_id = Input::get('text_field_id');
        $dropdown_field_id = Input::get('dropdown_field_id');
        $country = Input::get('country');
        $bid_limit_amount = Input::get('bid_limit_amount');
        $bid_start_amount = Input::get('bid_start_amount');
        $ads_id = array_get($input, 'id');
        $bid_duration_start = Input::get('bid_duration_start');
        $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');

        // check if an ID is passed
        if($ads_id) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

     // getting all of the post data
        $files = Input::file('photo');
        $rules = [
             'photo'                    => 'max:3000',
             'category'                 => 'required',
             // 'subcategory'              => 'required',
             'title'                    => 'required|min:4|max:100',
             'description'              => 'required',
             'price'                    => 'required|min:5|max:100000000|numeric',
             'bid_duration_start'       => 'min:1|max:99|numeric',
             'country'                  => 'required',
             'city'                     => 'required',
             'bid_duration_dropdown_id' => ($ads_type_id==2?'required':''),
             'bid_limit_amount'         => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_start_amount'         => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'minimum_allowed_bid'      => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_duration_start'       => ($ads_type_id==2?'min:1|max:99|numeric|required':''),


             
        ];
        // field name overrides
        $names = [   
            'photo'                    => 'photo',
            'category'                 => 'category',
            'subcategory'              => 'sub category',
            'photo'                    => 'photo',
            'title'                    => 'title',
            'description'              => 'description',
            'price'                    => 'price',
            'bid_duration_start'       => 'bid duration',
            'Country'                  => 'Country',
            'City'                     => 'City',
            'bid_duration_dropdown_id' => 'Bid duration',
            'bid_limit_amount'         => 'bid limit amount',
            'bid_start_amount'         => 'bid start amount',
            'minimum_allowed_bid'      => 'minimum allowed amount',
            'bid_duration_start'       => 'bid duration',

        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($ads_type_id == '2') {
            if ($bid_limit_amount <= $bid_start_amount) {
                return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start']); 
            }
        }
        
     // create model if new
      if($new) {
          $row = new Advertisement;
      }

          $row->ads_type_id                = array_get($input, 'ads_type_id');
          $row->cat_unique_id              = $category;
          $row->user_id                    = Auth::user()->id;
          $row->title                      = array_get($input, 'title');
          $row->price                      = array_get($input, 'price');
          $row->description                = array_get($input, 'description');
          if (array_get($input, 'category')) {
           $row->category_id          = array_get($input, 'category');
          }else {
           $row->category_id          = null;
          }
          if (array_get($input, 'subcategory')) {
           $row->sub_category_id      = array_get($input, 'subcategory');
          }else {
           $row->sub_category_id      = null;
          }
         
          if ($row->bid_duration_start != $bid_duration_start || $row->bid_duration_dropdown_id != $bid_duration_dropdown_id) {
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                }       
          } 

          if ($ads_type_id == 2) {
            $row->bid_limit_amount         = array_get($input, 'bid_limit_amount');
            $row->bid_start_amount         = array_get($input, 'bid_start_amount');
            if ($new == true) {
            $row->minimum_allowed_bid      = array_get($input, 'minimum_allowed_bid'); 
            } else {
             $reset_bidder = AuctionBidders::where('ads_id','=',$row->id)->where('bid_limit_amount_status','=',1)->first();
              if ($reset_bidder) {
                $reset_bidder->bid_limit_amount_status = '0';
                $reset_bidder->save();
              }
            }
            $row->bid_duration_start       = array_get($input, 'bid_duration_start');
            $row->bid_duration_dropdown_id = array_get($input, 'bid_duration_dropdown_id');
          } else {
            $row->bid_limit_amount          = null;
            $row->bid_start_amount          = null; 
            $row->minimum_allowed_bid       = null;
            $row->bid_duration_start        = null;
            $row->bid_duration_dropdown_id  = null;
          }

          if (array_get($input, 'youtube')) {
            $row->youtube                  = array_get($input, 'youtube');
          } else {
            $row->youtube                  = null;
          }
            if (array_get($input, 'country')) {
            $row->country_id               = array_get($input, 'country');
          } else {
            $row->country_id               = null;
          }
            if (array_get($input, 'city')) {
            $row->city_id                  = array_get($input, 'city');
          } else {
            $row->city_id                  = Auth::user()->city;
          }
            if (array_get($input, 'address')) {
            $row->address                  = array_get($input, 'address');
          } else {
            $row->address                  = null;
          }

          //for auction exp email trigger
          if ($row->ads_type_id == 2) {
           $row->ad_exp_email_trigger = 1;
           $row->auction_expiring_noti_trigger = 1;
          } else {
          $row->ad_expiration =  '0000-00-00 00:00:00';
          }
            // save model

     if ($new) {
      $i = $rem_img_upload;

      for ($x = 1; $x <= $i; $x++) {
        if(Input::file('photo'.$x.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$x.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$x.''))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = new AdvertisementPhoto;
             $savephoto->ads_id = $row->id;
                if ($x == 1) {
                    $savephoto->primary = 1;
                 }
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }
            else{
        $savephoto = new AdvertisementPhoto;
        $savephoto->ads_id = $row->id;
        $savephoto->photo = null;
        $savephoto->save();
        }
      }

      if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
      if(!is_null($dropdown_field)){
        foreach ($dropdown_field_id as $key => $xvalue) {
           $ad_attri = new CustomAttributesForDropdown;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $dropdown_field_id[$key];
           $ad_attri->attri_value_id  = $dropdown_field[$key];
           $ad_attri->save();
        }
      } 
   }
   //FOR EDITING ADS
  else{
    // Save the photo
         if (array_get($input, 'photo1')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo1'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo1'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id1);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo2')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo2'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo2'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id2);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo3')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo3'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo3'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id3);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo4')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo4'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo4'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id4);
            $row->photo = $FileName . '.jpg';
            $row->save();
        }

          $get_exist_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->get();
          if ($get_exist_dropdown) {
               foreach ($get_exist_dropdown as $values) {
                  $values->status = '2';
                  $values->save();    
               }

                if(!is_null($dropdown_field)){
                          foreach ($dropdown_field_id as $key => $xvalue) {     
                             $ad_attri = new CustomAttributesForDropdown;
                             $ad_attri->ad_id  = $row->id;
                             $ad_attri->attri_id = $dropdown_field_id[$key];
                             $ad_attri->attri_value_id  = $dropdown_field[$key];
                             $ad_attri->save();
                          }
                    } 
          }
          $get_existing_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->get();
          if ($get_existing_textbox) {
               foreach ($get_existing_textbox as $values) {
                  $values->status = '2';
                  $values->save();    
               }

             if(!is_null($text_field)){
                  foreach ($text_field_id as $key => $value) {
                     $ad_attri = new CustomAttributesForTextbox;
                     $ad_attri->ad_id  = $row->id;
                     $ad_attri->attri_id = $text_field_id[$key];
                     $ad_attri->attri_value  = $text_field[$key];
                     $ad_attri->save();   
                  }
                }
          }
  }

    
    $row->save();

        if(Input::get('no_preview') == 1)
        {
          $pix = AdvertisementPhoto::where('ads_id',Input::get('id'))->where('primary',1)->first();
          $pix->photo = 'nopreview.png';
          $pix->save();
        }
        else
        {
          $FileName = $row->id.rand(10,9999);
          Image::make(Input::file('photo'))->save('uploads/ads/' . $FileName .'.jpg');
          Image::make(Input::file('photo'))->fit(260, 160)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
          $pix = AdvertisementPhoto::where('ads_id',Input::get('id'))->first();
          $pix->photo = $FileName.'.jpg';
          $pix->save();
        }
    $row->save();
    if($new){
         return Response::json(['body' => "Advertisement Created"]);
    } else {
         return Response::json(['body' => "Advertisement Successfully updated"]);
    }




  }



    public function viewAds (){
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $ads_id = Request::input('ads_id');
      $row = Advertisement::join('advertisements_photo', 'advertisements.id', '=', 'advertisements_photo.ads_id')
                               ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                               ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                               ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                               ->leftJoin('ad_bid_duratation_dropdown','advertisements.bid_duration_dropdown_id', '=', 'ad_bid_duratation_dropdown.id')
                               ->select('advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.sub_category_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'advertisements.address', 'advertisements.youtube', 'advertisements.bid_limit_amount', 'advertisements.bid_start_amount', 'advertisements.minimum_allowed_bid', 'advertisements.bid_duration_start', 'advertisements.bid_duration_dropdown_id',
                                         'ad_bid_duratation_dropdown.value')    
                               ->where(function($query) use($ads_id){
                                             $query->where('advertisements.id', '=', $ads_id);                                  
                                      })->first();
      $get_photo = AdvertisementPhoto::where('ads_id','=', $ads_id)->get(); 
      $category = Category::with('subcategoryinfo')->get();
      $sub_categories = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', '26')->get();                            
      //Append
      $result['photo']          = $get_photo;                      
      $result['row']            = $row;                            
      $result['category']       = $category;                            
      $result['sub_categories'] = $sub_categories;                            

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }  
      }


   public function removeAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "7";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }


  public function removeFeatureRequest() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->feature = NULL;
        $row->featured_expiration = NULL;
        $row->save();
        return Response::json(['body' => 'Feature Request Successfully Removed']);
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

   public function approveFeature(){
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->feature = "1";
        $row->save();

        $notify_head = new UserNotificationsHeader;
        $notify_head->title       ='Feature';
        $notify_head->read_status = 1;
        $notify_head->reciever_id = $row->user_id;
        $notify_head->photo_id = $row->user_id;
        $notify_head->save();

        $notify_body = new UserNotifications;
        $notify_body->notification_id  = $notify_head->id;
        $notify_body->message = '<a href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'"> Your ad '.$row->title.' feature request has been approved. </a>';
        $notify_body->type= "Feature Approved";
        $notify_body->status= "1";
        $notify_body->save();

        $user = User::find($row->user_id);

        if($user) {
          Mail::send('email.feature', ['ad_name' => $row->title, 'action' => 'approved'], function ($m) use ($user) {
            $m->from('noreply@yakolak.com')->to($user->email)->subject('Yakolak - Feature Approved');
          });
        }
        

        return Response::json(['body' => 'Ad is now featured']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
   }
  public function unapproveFeature(){

      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->feature = "2";
        $row->save();

        $notify_head = new UserNotificationsHeader;
        $notify_head->title       ='Feature';
        $notify_head->read_status = 1;
        $notify_head->reciever_id = $row->user_id;
        $notify_head->photo_id = $row->user_id;
        $notify_head->save();

        $notify_body = new UserNotifications;
        $notify_body->notification_id  = $notify_head->id;
        $notify_body->message = '<a href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'"> Your ad '.$row->title.' feature request has been unapproved. </a>';
        $notify_body->type= "Feature Unapproved";
        $notify_body->status= "1";
        $notify_body->save();

        $user = User::find($row->user_id);

        if($user) {
          Mail::send('email.feature', ['ad_name' => $row->title, 'action' => 'unapproved'], function ($m) use ($user) {
            $m->from('noreply@yakolak.com')->to($user->email)->subject('Yakolak - Feature Unapproved');
          });
        }

        return Response::json(['body' => 'Ad has been unapproved']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
   }
   public function rejectFeature(){
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->feature = "3";
        $row->save();
         $updated_feature_transact = AdFeatureTransactionHistory::where('ad_id',Request::input('id'))->where('status',1)->first();
         if(!is_null($updated_feature_transact)){
            $updated_feature_transact->status = 3;
            $updated_feature_transact->save();
         }

          $notify_head = new UserNotificationsHeader;
          $notify_head->title       ='Feature';
          $notify_head->read_status = 1;
          $notify_head->reciever_id = $row->user_id;
          $notify_head->photo_id = $row->user_id;
          $notify_head->save();

          $notify_body = new UserNotifications;
          $notify_body->notification_id  = $notify_head->id;
          $notify_body->message = '<a href="'.url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug.'"> Your ad '.$row->title.' feature request has been rejected. </a>';
          $notify_body->type= "Feature Rejected";
          $notify_body->status= "1";
          $notify_body->save();

          $user = User::find($row->user_id);

          if($user) {
            Mail::send('email.feature', ['ad_name' => $row->title, 'action' => 'rejected'], function ($m) use ($user) {
              $m->from('noreply@yakolak.com')->to($user->email)->subject('Yakolak - Feature Rejected');
            });
          }

        return Response::json(['body' => 'Ad is rejected']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
   }
   public function markSpamAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        if($row->ads_type_id == "1"){
           $row->status = "4";
        }else{
           $row->status = "5";
        }
           $row->save();
        return Response::json(['body' => 'Ad Successfully Marked as Spam']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

     public function unmarkSpamAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Ad Successfully UnMarked as Spam']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
  public function getFeaturedSettings(){
    $row = SettingForExchangePoints::find(Request::input('id'));
    if(!is_null($row)){
      return Response::json($row);
    }else{
     return Response::json(['error' => ['error']]); 
    }
  }
  public function blockAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){

        Advertisement::where('user_id',$row->user_id)->update(['status' => 2]);
        User::where('id',$row->user_id)->update(['status' => 3]);

        return Response::json(['body' => 'Ad Successfully blocked']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
   public function unblockAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){

       Advertisement::where('user_id',$row->user_id)->update(['status' => 1]);
       User::where('id',$row->user_id)->update(['status' => 1]);

        return Response::json(['body' => 'Ad Successfully unblocked']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
   public function disableAd(){
      $row = Advertisement::find(Request::input('id'));
        if(!is_null($row)){
          if($row->ads_type_id == "1"){
             $row->status = "3";
          }else{
             $row->status = "4";
          }
             $row->save();
          return Response::json(['body' => 'Ad Disabled']);      
        }else{
          return Response::json(['error' => ['error on finding the user']]); 
      }
   }
  public function  enableAd(){
      $row = Advertisement::find(Request::input('id'));
        if(!is_null($row)){
          $row->status = "1";
          $row->save();
          return Response::json(['body' => 'Ad Enabled']);      
        }else{
          return Response::json(['error' => ['error on finding the user']]); 
      }
   }
   public function flagAd(){
      $row = Advertisement::find(Request::input('id'));
        if(!is_null($row)){
          $row->status = "6";
          $row->save();
          return Response::json(['body' => 'Ad Flagged']);      
        }else{
          return Response::json(['error' => ['error on finding the user']]); 
      }
   }
  public function unflagAd(){
      $row = Advertisement::find(Request::input('id'));
        if(!is_null($row)){
          $row->status = "1";
          $row->save();
          return Response::json(['body' => 'Ad UnFlagged']);      
        }else{
          return Response::json(['error' => ['error on finding the user']]); 
      }
   }
   public function enableKeyword(){
    $row = AdvertisementSpamAndBots::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Keyword enabled']);      
    }else{
        return Response::json(['error' => ['error on finding the user']]); 
    }
   }
   public function disableKeyword(){
    $row = AdvertisementSpamAndBots::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Keyword enabled']);      
    }else{
        return Response::json(['error' => ['error on finding the user']]); 
    }
   }

  public function saveAdManagemetSettings() {

        $input = Input::all();
        $basic_ad_expiration_start = Input::get('basic_ad_expiration_start');
        $basic_ad_expiration_end = Input::get('basic_ad_expiration_end');
        $ad_expiration_status = Input::get('ad_expiration_status');
        $bid_inc_value = Input::get('bid_inc_value');
        $bid_adding_time_start = Input::get('bid_adding_time_start');
        $bid_adding_time_end = Input::get('bid_adding_time_end');
        $expiring_auction_noti_time_start = Input::get('expiring_auction_noti_time_start');
        $expiring_auction_noti_time_end = Input::get('expiring_auction_noti_time_end');
   
   // getting all of the post data
      $files = Input::file('photo');
       $rules = [
             'basic_ad_expiration_start' => 'max:99|required|int',
             'basic_ad_expiration_end'   => 'required',
             'bid_inc_value'             => 'required|int|min:1|max:999999999',
             'bid_adding_time_start'     => 'max:99|required|int',
             'bid_adding_time_end'       => 'required',
        ];
        // field name overrides
        $names = [
            
            'basic_ad_expiration_start'  => 'basic ad expire time',
            'basic_ad_expiration_end'    => 'days',
            'bid_inc_value'              => 'bid value',
            'bid_adding_time_start'      => 'bid adding time',
            'bid_adding_time_end'        => 'bid adding time',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $rows = AdManagementSettings::find('1');
        $rows->basic_ad_expiration_start = $basic_ad_expiration_start;
        $rows->basic_ad_expiration_end   = $basic_ad_expiration_end;
        $rows->bid_inc_value             = $bid_inc_value;
        $rows->bid_adding_time_start     = $bid_adding_time_start;
        $rows->bid_adding_time_end       = $bid_adding_time_end;
        $rows->expiring_auction_noti_time_start  = $expiring_auction_noti_time_start;
        $rows->expiring_auction_noti_time_end    = $expiring_auction_noti_time_end;

        $rows->save();

        if($rows){
        return Response::json(['body' => 'Ad Settings Successfully Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

     public function doUserListingRefresh(){

        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $user_id = Request::input('user_id');
        $per = Request::input('per') ?: 10;
        $status = Request::input('status') ?: '';
        $country = Request::input('country') ?: '';

        if (Request::input('page') != '»') {

          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });
        $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
               
                
        } else {
             $count = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
       
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          return $result;
        }
    }
    public function doUserListingFlaggedRefresh(){

        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $user_id = Request::input('user_id');
        $per = Request::input('per') ?: 10;
        $status = Request::input('status') ?: '';
        $country = Request::input('country') ?: '';

        if (Request::input('page') != '»') {

          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });
        $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.status','=',6)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
               
                
        } else {
             $count = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.status','=',6)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.status','=',6)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
       
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          return $result;
        }
    }
    public function doUserListing($user_id){
       // sort and order
        // dd($user_id);
        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $user_id = Request::input('user_id') ? : $user_id;
        $per = Request::input('per') ?: 10;
        $status = Request::input('status') ?: '';
        $country = Request::input('country') ?: '';
        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });
        $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
               
                
        } else {
             $count = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.status','!=',7)
                              ->where('advertisements.user_id',$user_id)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
        $get_auction = "";
       $get_auction .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
                      '<div class="form-group">'.
                       '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Limit Amount</label>'.
                         '<div class="col-sm-9">'.
                           '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Start Amount</label>'.
                        '<div class="col-sm-9">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" >'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Minimum Allowed Bid</label>'.
                        '<div class="col-sm-9">'.
                          '<input type="number"  class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
                        '<div class="col-sm-9">'.
                          '<div class="col-sm-6 noleftPadding">'.
                          '<div class="form-group">'.
                            '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                            '<div class="col-sm-8">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start">'.
                              '</div>'.
                            '</div>'.
                          '</div>'.
                          '<div class="col-sm-6 norightPadding">'.
                            '<div class="form-group">'.
                              '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                              '<div class="col-sm-8"> '.        
                           '<input type="text" name="value" id="row-value" class="form-control borderZero" value="">' .
                          '<select name="bid_duration_dropdown_id" id="row-bid_duration_dropdown_id" class="form-control fullSize hide" required="required">'.
                          '<option class="hide" selected>Select:</option>';
                                $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get(); 
                                foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
                                          $get_auction .= '<option value="' . $bid_field->id . '" >'. $bid_field->value . '</option>';
                                         }
        $get_auction .='</select>'.'</div>'.
                            '</div>
                          </div>
                        </div>
                      </div>';

         $countries = Country::where('status',1)->orderBy('countryName','asc')->get();
         $ad_management_settings = AdManagementSettings::first();
         $city = City::all();

        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          $result['ad_management_settings'] = $ad_management_settings->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          $result['get_auction']  = $get_auction;
          $result['ad_management_settings'] = $ad_management_settings;
          return $result;
        }
    }
    public function doUserListingFlagged($user_id){
       // sort and order

        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;
        $status = Request::input('status') ? :'';
        $country = Request::input('country') ?: '';
        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

        $rows = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where(function($query){
                                        $query->where('advertisements.status','=',6);
                               })
                              ->where('advertisements.user_id',$user_id)
                             
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
               
                   
        } else {
             $count = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.user_id',$user_id)
                              ->where(function($query){
                                        $query->where('advertisements.status','=',6);
                               })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows = Advertisement::with('getAdvertisementType')
                              ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                              ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                              ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                              ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                              ->leftJoin('categories','advertisements.parent_category', '=', 'categories.unique_id')
                              ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id','advertisements.price', 'advertisements.id','advertisements.parent_category','advertisements.created_at','advertisements.feature', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName', 'categories.name as catName', 'country_cities.name as city', 'advertisements.address', 'advertisements.youtube','advertisements.country_code', 'categories.unique_id','users.status as user_status') 
                              ->where('advertisements_photo.primary', '=', '1')
                              ->where(function($query) use ($search) {
                                  $query->where('advertisements.title', 'LIKE', '%' . $search . '%')
                                        ->orWhere('advertisements.country_code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                        ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                                })
                               ->where(function($query) use ($status) {
                                   $query->where('advertisements.status','LIKE', '%' . $status . '%');    
                                })
                               ->where(function($query) use ($country) {
                                  if ($country) {
                                   $query->where('advertisements.country_code','LIKE', '%' . $country . '%');    
                                  }
                               })
                              ->where('advertisements.user_id',$user_id)
                              ->where(function($query){
                                        $query->where('advertisements.status','=',6);
                               })
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
       
     $get_auction = "";
       $get_auction .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
                      '<div class="form-group">'.
                       '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Limit Amount</label>'.
                         '<div class="col-sm-9">'.
                           '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Start Amount</label>'.
                        '<div class="col-sm-9">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" >'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Minimum Allowed Bid</label>'.
                        '<div class="col-sm-9">'.
                          '<input type="number"  class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
                        '<div class="col-sm-9">'.
                          '<div class="col-sm-6 noleftPadding">'.
                          '<div class="form-group">'.
                            '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                            '<div class="col-sm-8">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start">'.
                              '</div>'.
                            '</div>'.
                          '</div>'.
                          '<div class="col-sm-6 norightPadding">'.
                            '<div class="form-group">'.
                              '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                              '<div class="col-sm-8"> '.        
                           '<input type="text" name="value" id="row-value" class="form-control borderZero" value="">' .
                          '<select name="bid_duration_dropdown_id" id="row-bid_duration_dropdown_id" class="form-control fullSize hide" required="required">'.
                          '<option class="hide" selected>Select:</option>';
                                $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get(); 
                                foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
                                          $get_auction .= '<option value="' . $bid_field->id . '" >'. $bid_field->value . '</option>';
                                         }
        $get_auction .='</select>'.'</div>'.
                            '</div>
                          </div>
                        </div>
                      </div>';

         $countries = Country::where('status',1)->orderBy('countryName','asc')->get();
         $ad_management_settings = AdManagementSettings::first();
         $city = City::all();

        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          $result['ad_management_settings'] = $ad_management_settings->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          $result['get_auction']  = $get_auction;
          $result['ad_management_settings'] = $ad_management_settings;
          return $result;
        }
    }
    public function indexUserListings($user_id){
      $not_allowed = array(1,2);
      if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    } 

      $result = $this->doUserListing($user_id);

      $this->data['rows'] = $result['rows'];
      $this->data['get_auction'] = $result['get_auction'];
      $this->data['ad_management_settings'] = $result['ad_management_settings'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/user-listings/refresh');
      $get_user_info = User::find($user_id);
      $this->data['user_id']  = $get_user_info->id;
      $this->data['title'] = "Ad Listings - ".$get_user_info->name;
      $this->data['ad_types']  = AdvertisementType::where('status',1)->get();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      return View::make('admin.user-management.front-end-users.list-ad', $this->data);
    }
     
    public function indexUserListingFlagged($user_id){
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

      $result = $this->doUserListingFlagged($user_id);

      $this->data['rows'] = $result['rows'];
      $this->data['get_auction'] = $result['get_auction'];
      $this->data['ad_management_settings'] = $result['ad_management_settings'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/user-listings/flagged/refresh');
      $get_user_info = User::find($user_id);
      $this->data['user_id']  = $get_user_info->id;
      $this->data['title'] = "Ad Listings (Flagged) - ".$get_user_info->name;
      $this->data['ad_types']  = AdvertisementType::where('status',1)->get();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      return View::make('admin.user-management.front-end-users.list-ad', $this->data);

    }
    public function editAdManagemetSettings(){
      $settings_id = Request::input('settings_id');
       $row = AdManagementSettings::find($settings_id);
        if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
   }

   public function editSpamAndBotKeyword(){
       $id = Request::input('id');
       $row = AdvertisementSpamAndBots::find($id);
        if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
   public function  deleteSpamAndBotKeyword(){
       $id = Request::input('id');
       $row = AdvertisementSpamAndBots::find($id);
        if($row) {
          $row->status = 3;
          $row->save();
          return Response::json(['body' => 'Keyword Successfully deleted']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    } 
   public function viewCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::with('getCommentUser')
                            ->with('getCommentAd')
                            ->find($id);
        if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function approveCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::find($id);
       if(!is_null($row)) {
         
         $reviews = AdRating::where('id', $row->rating_id)->first();
         
          if ($reviews) {
             $reviews->status = 1;
             $reviews->save();
          }
         
         $update=UserAdComments::where('status',2)->where('vendor_id',$row->vendor_id)->get();
          if(count($update) > 0){
              foreach ($update as $key => $field) {

                 $update_status = UserAdComments::where('id',$field->id)->first();
                 $update_status->status = 1;
                 $update_status->save();
              }
                UserAdCommentReplies::where('comment_id',$id)->update(['status' => 1]);
          }else{
            $row->status = 1;
            $row->save();
          }
        
          // UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 1]);
          return Response::json(['body' => 'Comment has been approved']);      
       } else {
          return Response::json(['error' => "Invalid row specified"]);
       }
    }
    public function rejectCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::find($id);
        if(!is_null($row)) {
          
          $reviews = AdRating::where('id', $row->rating_id)->first();
         
          if ($reviews) {
             $reviews->status = 2;
             $reviews->save();
          }
          
          $update=UserAdComments::where('status',2)->where('vendor_id',$row->vendor_id)->get();

          if(count($update) > 0){
              foreach ($update as $key => $field) {
                 $update_status = UserAdComments::where('id',$field->id)->first();
                 $update_status->status = 3;
                 $update_status->save();
              }
                 UserAdCommentReplies::where('comment_id',$id)->update(['status' => 3]);

          }else{
            $row->status = 3;
            $row->save();
          }
          // UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 3]);
          return Response::json(['body' => 'Comment has been rejected']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function deleteCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::find($id);
        if(!is_null($row)) {
          
          $reviews = AdRating::where('id', $row->rating_id)->first();
         
          if ($reviews) {
             $reviews->delete();
          }
          
          
          $row->status = 5;
          $row->save();
          UserAdCommentReplies::where('comment_id',$row->id)->update(['status' => 5]);
          return Response::json(['body' => 'Comment has been deleted']);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function blockCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::find($id);
        if(!is_null($row)) {
          UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 4]);
          // Advertisement::where('user_id',$row->vendor_id)->update(['status' => 2]);
          UserAdCommentReplies::where('user_id',$row->vendor_id)->update(['status' => 4]);
          AdRating::where('id', $row->rating_id)->update(['status' => 2]);
          User::where('id',$row->vendor_id)->update(['status' => 3]);
          return Response::json(['body' => 'Comment has been blocked']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function unblockCommentAndReview(){
       $id = Request::input('id');
       $row = UserAdComments::find($id);
        if(!is_null($row)) {

          UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 1]);
          User::where('id',$row->vendor_id)->update(['status' => 1]);
          AdRating::where('id', $row->rating_id)->update(['status' => 1]);
          // Advertisement::where('user_id',$row->vendor_id)->update(['status' => 1]);
          UserAdCommentReplies::where('user_id',$row->vendor_id)->update(['status' => 1]);

          return Response::json(['body' => 'Comment has been unblocked']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
   public function viewCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::with('getCommentReplyUser')
                            ->find($id);
        if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

    public function approveCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::find($id);
       if(!is_null($row)) {
         $update=UserAdCommentReplies::where('status',2)->where('user_id',$row->user_id)->get();
          if(count($update) > 0){
              foreach ($update as $key => $field) {
                 $update_status = UserAdCommentReplies::where('id',$field->id)->first();
                 $update_status->status = 1;
                 $update_status->save();
              }
                UserAdComments::where('id',$row->comment_id)->update(['status' => 1]);
          }else{
            $row->status = 1;
            $row->save();
          }
          // UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 1]);
          return Response::json(['body' => 'Comment Reply has been approved']);      
       } else {
          return Response::json(['error' => "Invalid row specified"]);
       }
    }
    public function rejectCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::find($id);
        if(!is_null($row)) {
          $update=UserAdCommentReplies::where('status',2)->where('user_id',$row->user_id)->get();

          if(count($update) > 0){
              foreach ($update as $key => $field) {
                 $update_status = UserAdCommentReplies::where('id',$field->id)->first();
                 $update_status->status = 3;
                 $update_status->save();
              }
                 UserAdComments::where('id',$row->comment_id)->update(['status' => 3]);
          }else{
            $row->status = 3;
            $row->save();
          }
          // UserAdComments::where('vendor_id',$row->vendor_id)->update(['status' => 3]);
          return Response::json(['body' => 'Comment Reply has been rejected']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
      public function blockCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::find($id);
        if(!is_null($row)) {
          UserAdComments::where('vendor_id',$row->user_id)->update(['status' => 4]);
          // Advertisement::where('user_id',$row->user_id)->update(['status' => 2]);
          UserAdCommentReplies::where('user_id',$row->user_id)->update(['status' => 4]);
          User::where('id',$row->user_id)->update(['status' => 3]);
          return Response::json(['body' => 'Comment Reply has been blocked']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
    public function unblockCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::find($id);
        if(!is_null($row)) {

          UserAdComments::where('vendor_id',$row->user_id)->update(['status' => 1]);
          User::where('id',$row->user_id)->update(['status' => 1]);
          // Advertisement::where('user_id',$row->user_id)->update(['status' => 1]);
          UserAdCommentReplies::where('user_id',$row->user_id)->update(['status' => 1]);

          return Response::json(['body' => 'Comment Reply has been unblocked']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
  public function deleteCommentAndReviewReply(){
       $id = Request::input('id');
       $row = UserAdCommentReplies::find($id);
        if(!is_null($row)) {
          $row->status = 5;
          $row->save();
          return Response::json(['body' => 'Comment Reply has been deleted']);      
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
   public function listAdPictures(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $get_photos = AdvertisementPhoto::where('ads_id','=',$ads_id)->where('photo','!=', 'null')->get();
     $rows ='';
     foreach ($get_photos as $value) {
      $rows .='<p><img src="'.url('/').'/uploads/ads/thumbnail/'.$value->photo.'" id="row-photo" width="150px" height="120px" /></p><button type="button" class="file_remove btn btn-sm redButton">Delete</button> <button type="button" class="file_ups btn btn-sm blueButton">Browse</button>';
     }
      return Response::json($rows);
    }




   public function getCity(){
     $input = Input::all();
     $country_id = Request::input('country_id');
     $rows ="";
     $city = City::where('country_id','=',$country_id)->get();
        foreach($city as $cities) {
           $rows .='<option value="'.$cities->id.'">'. $cities->name .'</option>';
        }
     $rows .='</select>';
      return Response::json($rows);
    }




    public function getSubCategory(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $get_subcat = Advertisement::find($ads_id);
     if (Request::input('cat_id')) {
     $cat_id = Request::input('cat_id'); 
     } else {
     $cat_id = $get_subcat->category_id;
     }
     $sab_cat = $get_subcat->sub_category_id;

     $rows ="";
     $rows = '<option class="hide" value="">Select:</option>';
     $get_sub = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=',$cat_id)->get(); 
     foreach ($get_sub as $value) {
         $rows .= '<option value="'.$value->id.'" '.($sab_cat==$value->id?'selected':'' ).'>'.$value->name.'</option>';
     }

    return Response::json($rows);
    }




    public function getCategory(){
     $input = Input::all();
     $ads_type_id = Request::input('ads_type_id');
     $ads_id = Request::input('ads_id');
     $get_cat_id = Advertisement::find($ads_id);
     $cat_id = $get_cat_id->category_id;
     $rows ="";
     $rows .='<option class="hide" value="">Select:</option>';
     if ($ads_type_id == '1') {
           $get_cats = Category::where(function($query) {
                               $query->where('status', '=', '1')
                                     ->where('buy_and_sell_status', '=', '2')
                                     ->orWhere('buy_and_sell_status','=','3');
                                 })->get();
     } else {
           $get_cats = Category::where(function($query) {
                                $query->where('status', '=', '1')
                                      ->where('biddable_status', '=', '2')
                                      ->orWhere('biddable_status','=','3');                                  
                                 })->get();
     }

   foreach ($get_cats as $get_cat) {
     $rows .='<option value="'.$get_cat->id.'" '.($cat_id==$get_cat->id?'selected':'').'>'.$get_cat->name.'</option>';
   }

    return Response::json($rows);
    }




  public function fetchCustomAttributesDisabled(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $rows = "";
          $i = 1;
          foreach ($get_dropdown as $key => $field) {
            $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
             foreach ($get_attri_info as $key => $field_attri_info) {
                $rows .= '<div class="form-group" id="ads-custom_attribute">'.
                                     '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero readonly-view" style="cursor:default" id="row-custom_attr'.$i++.'" name="dropdown_field[]" disabled="disabled">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $rows .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $rows .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
               foreach ($get_attri_info as $key => $field_attri_info) {

                $rows .= '<div class="form-group">'.
                                        '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                          '<div class="col-sm-9">';                  
                $rows .= '<input type="text" class="form-control borderZero readonly-view" style="cursor:default" name="text_field[]" value="'.$field->attri_value.'" disabled="disabled">';                  
                                 
                $rows .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'">'.
                                  '</div>'; 
               }           
          }
    return Response::json($rows);


      }
       public function fetchCustomAttributesEnabled(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $rows = "";
          $i = 1;
          foreach ($get_dropdown as $key => $field) {
            $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
             foreach ($get_attri_info as $key => $field_attri_info) {
                $rows .= '<div class="form-group" id="ads-custom_attribute">'.
                                     '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="dropdown_field[]">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $rows .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $rows .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
               foreach ($get_attri_info as $key => $field_attri_info) {

                $rows .= '<div class="form-group">'.
                                        '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                          '<div class="col-sm-9">';                  
                $rows .= '<input type="text" class="form-control borderZero" name="text_field[]" value="'.$field->attri_value.'">';                  
                                 
                $rows .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'">'.
                                  '</div>'; 
               }           
          }
    return Response::json($rows);


      }

   public function getCustomAttributes(){
       $input = Input::all();
       $ads_id = Request::input('ads_id');
       $cat_id = Request::input('cat_id');
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$cat_id)->get();
                     $rows = '';
          $i = 1;          
           foreach ($get_sub_attri_id as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'; 
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="'.$field->name.'">
                      <option value="" class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'; 
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9">'; 
                      $rows .= '<input type="text" name="text_field[]" class="form-control borderZero">';
                      $rows .='</div></div>';
                      $rows .='<input type="hidden" name="text_field_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">';
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .='</select></div>';
                      $rows .='<input type="hidden" name="dropdown_field_id[]" value="'.$field->id.'"></div>';
                 }           
          }

      return Response::json($rows);
    }

    public function getCommentReplies(){
        $id = Request::input('id');
        $row = UserAdCommentReplies::where('comment_id',$id)->where('status','!=',5)->get();
        $rows = '<div class="table-responsive" id="table-comment-replies"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Status</small></th>' .
                      '<th><small>Listing Type</small></th>' .
                      '<th><small>Type</small></th>' .
                      '<th><center><small>User</small></center></th>'.
                      '<th><center><small>Created Date<small></center></th><tbody></tr>';
        if(count($row) > 0){
             foreach ($row as $key => $field) {
                $get_user = User::where('id',$field->user_id)->first();
                $get_ad_type = UserAdComments::where('id',$field->comment_id)->first();
                $rows .= '<tr data-id='.$field->id.' class="'.($field->status == 4 ? 'blockedColor':'').''.($field->status == 3 ? 'unapprovedColor':'').'">' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td><small>'.($field->status == 1 ? 'Approved': '').($field->status == 2 ? 'Pending Approval':'').($field->status==3 ? 'Rejected':'').($field->status==4 ? 'Blocked':'').'</small></td>' .
                             '<td><small>Comment</small></td>' .
                             '<td><small>'.($get_ad_type->ad_type == 1 ? 'Ad Listings':'Auction').'</small></td>' .
                             '<td><center><small>'.$get_user->name.'</small></center></td>' .
                             '<td><center><small>'.$field->created_at.'</small></center></td>'. 
                             '<td>';

                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-reply"><i class="fa fa-trash-o"></i></button>';
                     if($field->status == 1){
                          $rows .='<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-reject-reply"'.($field->status == 4 ? 'disabled':'').''.($field->status == 1 ? 'disabled':'').' ><i class="fa fa-close"></i></button>';
                     }else{
                          $rows .='<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approve-reply"'.($field->status == 4 ? 'disabled':'').' ><i class="fa fa-check"></i></button>';
                     }        
                    if($field->status == 4){
                          $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unblocked-reply"><i class="fa fa-shirtsinbulk "></i></button>';
                    }else{
                          $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-blocked-reply"><i class="fa fa-shield"></i></button>';
                    }
                  $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-reply"><i class="fa fa-eye"></i></button>';

                $rows .=   '</td>'.
                         '</tr>';
              }
           
            
        }else{
              $rows .= '<tr>' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td colspan ="5"><small><center>No Available Comments</center></small></td>' .
                     '</tr>';

        }

              $rows .= '</tbody></table></div>';
            
              return Response::json($rows);
    }
}