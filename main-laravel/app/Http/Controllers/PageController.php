<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Form;
use Auth;
use DB;
use View;
use Request;
use Paginator;
use Response;
use Input;
use Validator;
use HTML;
use App\AdRating;
use App\UserAdComments;
use App\User;
use App\Country;
use App\City;
use App\Pages;
use Session;
///

class PageController extends Controller
{
	public function index(){
		$result = $this->doList();
	    $this->data['rows'] = $result['rows'];
     	$this->data['pages'] = $result['pages'];
	   	$this->data['title'] = "Page Content Management";
	   	$this->data['refresh_route'] = url("admin/page/refresh");
	   	$this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
	  	return View::make('admin.page-management.list', $this->data);
	}
	public function doList(){
		  $result['sort'] = Request::input('sort') ?: 'name';
	      $result['order'] = Request::input('order') ?: 'asc';
	      $search = Request::input('search');
	      $status = Request::input('status');
	      $country = Request::input('country');
	      $per = Request::input('per') ?: 100;

	      if (Request::input('page') != '»') {
	        Paginator::currentPageResolver(function () {
	            return Request::input('page'); 
	        });


	        $rows = Pages::where(function($query) use ($search) {
	                                 	 $query->where('name', 'LIKE', '%' . $search . '%');     
	                                  })
	                                // ->where('type',1)
	                                ->where('status',1)
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);


	      } else {
	         $count = Pages::where(function($query) use ($search) {
	                               		    $query->where('name', 'LIKE', '%' . $search . '%');    
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	         						// ->where('type',1)
	         						->where('status',1)
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);

	        Paginator::currentPageResolver(function () use ($count, $per) {
	            return ceil($count->total() / $per);
	        });

	      $rows = Pages::where(function($query) use ($search) {
	                                	     $query->where('name', 'LIKE', '%' . $search . '%');      
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	      						    // ->where('type',1)
	      						    ->where('status',1)
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);
	      }

	      // return response (format accordingly)
	      if(Request::ajax()) {
	          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows->toArray();
	          return Response::json($result);
	      } else {
	          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows;
	          return $result;
	      }	
	}

	public function save(){
  	    $new = true;
        $input = Input::all();
// 		dd($input);
        
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Pages::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'content'       => 'required',
           
        ];
        // field name overrides
        $names = [
            'content'          		 => 'Content',
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new Pages;
            $row->content      = htmlentities(array_get($input, 'content'));
        } else {
            $row->content     = htmlentities(array_get($input, 'content'));
        }   
          	$row->save();
		Session::flash('page_update', 'Page contents updated');
		
		return redirect()->back();
//         if($new){
//         	return Response::json(['body' => 'New Page Added']);
//         }else{
//         	return Response::json(['body' => 'Page Updated']);
//         }
	  
	}

	public function manipulate(){
	
	 	$row = Pages::find(Request::input('id'));
	 	if(!is_null($row)){

	 	}
	 	return Response::json();
	}

	public function refreshPages(){

		$getPages = Pages::where('type',1)
						  ->orderBy('name','asc')
						  ->get();

		if(!is_null($getPages)){
			    $rows = '';
			foreach ($getPages as $key => $field) {
		$content = HTML::decode($field->content); 	 	        	    

    $rows .='<div class="panel-group hide" data-id="'.$field->country_id.'" id="accordion">'.Form::open(array('url' => 'admin/pages/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-form-pages','files' => 'true')).'
                <div id="form-notice"></div>
                <div class="panel panel-default" style="margin-bottom: 10px;">
                  <div class="panel-heading">
                   <a data-toggle="collapse" data-parent="#accordion" href="#'.$field->id.'">
                    <h4 class="panel-title">'.$field->name.'<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i></h4>
                   </a> 
                  </div>
                  <div id="'.$field->id.'" class="panel-collapse collapse">
                    <div class="panel-body">
                          <input type="hidden" id="row-id" name="id" value="'.$field->id.'"> 
                         <textarea class="form-control" rows="8" id="row-content" style="margin-bottom: 10px;" name="content">'.$content.'</textarea>
                          <button class="btn btn-success btn-submit hide-view pull-right" type="submit"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                    </div>
                 </div>'.Form::close().'</div></div>';

			}
		}
		$result['rows'] = $rows;
        return Response::json($result);
	}

}