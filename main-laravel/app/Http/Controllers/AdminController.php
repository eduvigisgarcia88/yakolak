<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use Mail;
use App\ContactDetails;

class AdminController extends Controller
{
   
	public function index(){

		$this->data['title'] = "Admin";
        return View::make('admin.form', $this->data);
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    public function ContactYakolak()
    {
        $messages = [
            'g-recaptcha-response.required' => 'Captcha is required',
            'message.required' => 'Message body is required',
            'message.min' => 'Message must be atleast 10 characters long',
            'email.email' => 'Email is not valid',
            'email.required' => 'Email is required',
            'name.min' => 'Name must be atleast 2 characters long'
        ];
        $validator = Validator::make(request()->all(), [
            'name' => 'required|min:2',
            'email' => 'email|required',
            'message' => 'required|min:10',
            'g-recaptcha-response' => 'required'
        ],$messages);

        if ($validator->fails())
        {
            $response['errors'] = $validator->errors()->all();
            $response['code'] = 0;
            return $response;
        }
        else
        {
            $sender_email = request()->email;
            $sender_name = request()->name;
            $receiver_email = ContactDetails::pluck('email');
            Mail::raw('Name: '.request()->name."\n".'Email: '.request()->email."\n".'Message: '.request()->message, function ($message) use ($receiver_email, $sender_email, $sender_name){
              $message->to($receiver_email);
              $message->subject('Contact Us - New Inquiry');
              $message->from($sender_email, $sender_name);
            });
            $response['code'] = 1;
            return $response;
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
     
    /**
     * Displays the login form
     *
     */
    public function login() {

        if (!Auth::check()) {
            $this->data['title'] = "Login";
            return view('admin.form', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    /**
     * Attempt to do login
     *
     */
    public function doLogin(Request $request) {

        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        $captcha = $request->input('g-recaptcha-response');
        // if(!$captcha){
        //   $error = "Captcha is required";
        // }else 
        if(!$username && !$password && !$captcha){
          $error['username_required'] = "Username is required";
          $error['password_required'] = "Password is required";
          $error['captcha_required'] = "Captcha is required";
        }else if(!$username && !$password){
          $error['username_required'] = "Username is required";
          $error['password_required'] = "Password is required";
        }else if(!$username){
          $error['username_required'] = "Username is required";
        }else if(!$password){
          $error['password_required'] = "Password is required";
        }
        // else if(!$captcha){
        //   $error['captcha_required'] = "Captcha is required";
        // }
        else if($honeypot){
          // robot detected
          $error = trans('validation.blank');
        }


        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
            // login successful
            $user = User::where('username', $username)->where('usertype_id',3)->orWhere('usertype_id',4)->first(); 

            if(!is_null($user)){
                 // return redirect('admin/dashboard/main');
                 return redirect('admin');
             }else{
                $error = "Invalid User";
             } 
           
     
        }

        
        else if(Auth::validate(['username' => $username, 'password' => $password])) {
            $not_allowed = array(1, 2);
            // $user = User::where('username', $username)->where('usertype_id',3)->orWhere('usertype_id',4)->first();
            $user = User::where('username', $username)->first(); 
            if($user->status == 2) {
                // user is not active
                $error = "This account is currently inactive.";

            }else if(in_array($user->usertype_id, $not_allowed)){
                 $error = "Invalid email or password.";
            }
        }else {
            // invalid login
            $error = 'Invalid email or password.';
        }
        // return error
        return redirect()->action('AdminController@login')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }

    /**
     * Displays the forgot password form
     *
     */
    public function forgotPassword() {
        $this->data['title'] = "Password Recovery";
        return View::make('users.forgotpass', $this->data);
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function doForgotPassword() {
        $user = User::where('email', Input::get('email'))
                    ->first();

        View::composer('emails.reminder', function($view) use ($user) {
            $view->with([
                'firstname' => $user->firstname
            ]);
        });

        $response = Password::remind(Input::only('email'), function($message) {
            $message->subject("Reset Password Request");
        });

        // (test) always say success
        $response = Password::REMINDER_SENT;

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'danger'
                ));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'success'
                ));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function resetPassword($token = null) {
        if(is_null($token)) App::abort(404);

        if(Auth::guest()) {
            $this->data['token'] = $token;
            $this->data['title'] = "Reset Password";

            return View::make('users.resetpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt change password of the user
     *
     */
    public function doResetPassword() {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 6 && $credentials['password'] != $credentials['email'];
        });

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()
                               ->withInput(Input::except('password'))
                               ->with('notice', array(
                                    'msg' => Lang::get($response),
                                    'type' => 'danger'
                ));
            case Password::PASSWORD_RESET:
                return Redirect::to('login')
                               ->with('notice', array(
                                    'msg' => Lang::get('reminders.complete'),
                                    'type' => 'success'
                ));
        }
    }

    public function logout() {
        Auth::logout();
        return redirect()->to('admin/login');
    }


     

}
