<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use View;
use App\User;
use Request;
use App\Http\Controllers\Controller;
use App\Country;
use Paginator;
use Response;
use Carbon\Carbon;
use Session;
use PDF;
use App\Invoice;


class ReportSalesController extends Controller
{
 	public function index(){
 	    $result = $this->doList();
	    $this->data['rows'] = $result['rows'];
     	$this->data['pages'] = $result['pages'];
	   	$this->data['title'] = "Sales Report Management";
	   	$this->data['refresh_route'] = url("admin/report-sales/refresh");
	  	return View::make('admin.report-management.sales', $this->data);
 	}
 	public function doList(){
		  	$result['sort'] = Request::input('sort') ?: 'created_at';
	      $result['order'] = Request::input('order') ?: 'asc';
	      $search = Request::input('search');
	      $status = Request::input('status');
	      $date_from = Request::input('date_from') ? Carbon::createFromFormat('m/d/Y', Request::input('date_from'))->toDateString().' 00:00:00' : '';
	      $date_to = Request::input('date_to') ? Carbon::createFromFormat('m/d/Y', Request::input('date_to'))->toDateString().' 23:59:59' : '';
	      $per = Request::input('per') ?: 10;

	      if (Request::input('page') != '»') {
	        Paginator::currentPageResolver(function () {
	            return Request::input('page'); 
	        });
          
          $rows = Invoice::select('users.alias', 'users.email', 'invoices.invoice_no', 'invoices.transaction_type', 'invoices.extended_details', 'invoices.cost', 'invoices.currency', 'invoices.created_at')
                                  ->leftjoin('users','users.id','=','invoices.user_id')
                                  ->where(function($query) use ($search) {
                               		   $query->where('alias', 'LIKE', '%' . $search . '%')
                                           ->orWhere('email', 'LIKE', '%' . $search . '%')
                                           ->orWhere('invoice_no', 'LIKE', '%' . $search . '%')
                                           ->orWhere('transaction_type', 'LIKE', '%' . $search . '%')
                                           ->orWhere('extended_details', 'LIKE', '%' . $search . '%')
                                           ->orWhere('cost', 'LIKE', '%' . $search . '%')
                                           ->orWhere('currency', 'LIKE', '%' . $search . '%');
                                  })
                                  ->where(function($query) use ($date_from, $date_to){
                                    if ($date_from && $date_to) {
                                        $query->whereBetween('invoices.created_at', [$date_from, $date_to]);
                                    }
                                  })
                                   ->orderBy($result['sort'], $result['order'])
                                   ->paginate($per);
	                            
	      } else {
	         $count =  Invoice::select('users.alias', 'users.email', 'invoices.invoice_no', 'invoices.transaction_type', 'invoices.extended_details', 'invoices.cost', 'invoices.currency', 'invoices.created_at')
                                  ->leftjoin('users','users.id','=','invoices.user_id')
                                  ->where(function($query) use ($search) {
                               		   $query->where('alias', 'LIKE', '%' . $search . '%')
                                           ->orWhere('email', 'LIKE', '%' . $search . '%')
                                           ->orWhere('invoice_no', 'LIKE', '%' . $search . '%')
                                           ->orWhere('transaction_type', 'LIKE', '%' . $search . '%')
                                           ->orWhere('extended_details', 'LIKE', '%' . $search . '%')
                                           ->orWhere('cost', 'LIKE', '%' . $search . '%')
                                           ->orWhere('currency', 'LIKE', '%' . $search . '%');
                                  })
                                  ->where(function($query) use ($date_from, $date_to){
                                    if ($date_from && $date_to) {
                                        $query->whereBetween('invoices.created_at', [$date_from, $date_to]);
                                    }
                                  })
                                   ->orderBy($result['sort'], $result['order'])
                                   ->paginate($per);


	        Paginator::currentPageResolver(function () use ($count, $per) {
	            return ceil($count->total() / $per);
	        });

	      $rows =  Invoice::select('users.alias', 'users.email', 'invoices.invoice_no', 'invoices.transaction_type', 'invoices.extended_details', 'invoices.cost', 'invoices.currency', 'invoices.created_at')
                                  ->leftjoin('users','users.id','=','invoices.user_id')
                                  ->where(function($query) use ($search) {
                               		   $query->where('alias', 'LIKE', '%' . $search . '%')
                                           ->orWhere('email', 'LIKE', '%' . $search . '%')
                                           ->orWhere('invoice_no', 'LIKE', '%' . $search . '%')
                                           ->orWhere('transaction_type', 'LIKE', '%' . $search . '%')
                                           ->orWhere('extended_details', 'LIKE', '%' . $search . '%')
                                           ->orWhere('cost', 'LIKE', '%' . $search . '%')
                                           ->orWhere('currency', 'LIKE', '%' . $search . '%');
                                  })
                                  ->where(function($query) use ($date_from, $date_to){
                                    if ($date_from && $date_to) {
                                        $query->whereBetween('invoices.created_at', [$date_from, $date_to]);
                                    }
                                  })
                                   ->orderBy($result['sort'], $result['order'])
                                   ->paginate($per);
	      }

	      // return response (format accordingly)
				
	      if(Request::ajax()) {
	          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows->toArray();
	          return Response::json($result);
	      } else {
	          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows;
	          return $result;
	      }	
	}

	public function showPDF(){
		
		$date_from = Request::get('from') ? Carbon::createFromFormat('m-d-Y', Request::get('from'))->toDateString().' 00:00:00' : '';
	  $date_to = Request::get('to') ? Carbon::createFromFormat('m-d-Y', Request::get('to'))->toDateString().' 23:59:59' : '';
		$table_data = '';
		
		$rows = Invoice::select('users.alias', 'users.email', 'invoices.invoice_no', 'invoices.transaction_type', 'invoices.extended_details', 'invoices.cost', 'invoices.currency', 'invoices.created_at')
                                  ->leftjoin('users','users.id','=','invoices.user_id')
                                  ->where(function($query) use ($date_from, $date_to){
                                    if ($date_from && $date_to) {
                                        $query->whereBetween('invoices.created_at', [$date_from, $date_to]);
                                    }
                                   })
                                   ->orderBy('created_at', 'desc')
                                   ->get();
		
		foreach ($rows as $val) {
			$table_data .= '<tr>';
			$table_data .= '<td>'.$val->invoice_no.'</td>';
			$table_data .= '<td>'.$val->alias.'</td>';
			$table_data .= '<td>'.$val->email.'</td>';
			$table_data .= '<td>'.$val->transaction_type.'</td>';
			$table_data .= '<td>'.$val->extended_details.'</td>';
			$table_data .= '<td>'.$val->cost.' '.$val->currency.'</td>';
			$table_data .= '<td>'.Carbon::createFromFormat('Y-m-d H:i:s', $val->created_at)->format('M d, y').'</td>';
			$table_data .= '</tr>';
		}
		
		$pdf = PDF::loadView('layout.sales-report', compact('table_data'));
    $pdf->setPaper('A4', 'landscape');
    return $pdf->stream('user-ads-report.pdf');
		
	}

}