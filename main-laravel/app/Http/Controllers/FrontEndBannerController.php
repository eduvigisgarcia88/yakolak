<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use App\CountryBannerPlacement;
use Request;
use Response;
use Input;
use Image;
use Validator;
use Carbon\Carbon;
use Paginator;
use App\Usertypes;
use App\BannerAdvertisementSubscriptions;
use App\PaypalPaymentForBannerPlan;
use App\ReferralTypes;
use App\UserReferrals;
use App\UserNotificationsHeader;
use App\UserNotifications;

class FrontEndBannerController extends Controller
{


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
      public function getBannerPlacementPlan() {

        $get_bannerplacementplan = CountryBannerPlacement::where('id', '=', Request::input('id'))
                                                          ->groupBy('placement_id')
                                                          ->get();       
        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_bannerplacementplan as $key => $field) {
          $rows .= '<option value="' . $field->id . '">' .'$'. $field->daily_cost_paid .' USD'.'</option>';
        }
        // foreach ($get_bannerplacementplan as $key => $field) {
        //   $rows .= '<option value="' . $field->id . '">' .'$'. $field->daily_cost_paid .' USD'.'</option>';
        // }
        return Response::json($rows);

     }
    public function saveBannerForPurchase()
    {
        $input = Input::all(); 
        $new = true;
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerAdvertisementSubscriptions::find(array_get($input, 'id'));
            
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
        $verifyBannerStatus = BannerAdvertisementSubscriptions::where('id',array_get($input, 'id'))->where('banner_status','=',4)->first();

        if(!is_null($verifyBannerStatus)){
            return Response::json(['errorVerify' => 'Invalid row specified']);
        }  
        $rules = [

              'country_id'                    => 'required',
              'page'                       => 'required',
              'banner_placement_id'        => 'required',
              // 'banner_placement_plan_id'   => 'required',
              'photo_desktop'                   => ($new ? 'required|max:1000':''),
              'photo_mobile'                   => ($new ? 'required|max:1000':''),
              'amount'                  => 'required',
              'target_url'                  => 'required',
              'duration'                  => 'required',
          
          ];
              // 'photo'                      => 'image|max:3000|'.($page == 1? 'image_size:<=295,<=390' :'').($page == 2 ? 'image_size:<=850,<=120':''),

          // field name overrides
          $names = [
              'country'                   => 'Country',
              'language'                  => 'Language',
              'page'                      => 'Page',
              'category'                  => 'Category',
              'banner_placement_id'       => 'Banner placement',
              // 'banner_placement_plan_id'  => 'Banner placement price',
              'photo_desktop'              => 'Image(Desktop)',
              'photo_mobile'               => 'Image(Mobile)',
              'amount'                     => 'Price',
              'target_url'                 => 'Target URL',
              'duration'                   => 'Duration',
             
          ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        $getPlacementID = CountryBannerPlacement::where('id',array_get($input,'banner_placement_id'))->first();
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if($new){
          $row = new BannerAdvertisementSubscriptions;
        }
          $cost = 0;
          // $row->title = array_get($input,'title');
          // $row->start_date = array_get($input,'start_date');
          $row->country_id = array_get($input,'country_id');
          // $row->language = array_get($input,'language');
          $row->amount = array_get($input,'amount');
          $row->page = array_get($input,'page');
          $row->user_id = Auth::user()->id;
          if($row->page == 1){
             $row->category = null;
          }else{
             $row->category = array_get($input,'category');
           }  
          $row->order_no = array_get($input,'order_no');
          $row->placement = array_get($input,'banner_placement_id');


          $row->placement_id = $getPlacementID->placement_id;
          $row->duration = array_get($input,'duration');
          $row->metric_duration = array_get($input,'metric_duration');
          // $row->placement_plan_id = array_get($input,'banner_placement_plan_id');
          // $row->expiration = array_get($input,'expiration_date');
          $row->target_url = array_get($input,'target_url');
          // $row->link = array_get($input,'link');
          $row->save();
          $row->unique_id = base64_encode('banner-'.$row->id);
          $row->save();
          $cost = $row->duration * $row->amount;
          $row->total_cost = $cost;
          $row->save();

          if(array_get($input,'status') == 7){
            $row->reserve_status = 2;
            $row->banner_status = 2;
            $row->save();
          }

          // $getbannerPlacementInfo = CountryBannerPlacement::where('id',Request::input('banner_placement_id'))->first();
          // if(!is_null($getbannerPlacementInfo)){
          //   $getPlacementInfo = BannerPlacementPlans::where('id',$getbannerPlacementInfo->placement_id)->first();

          // }
        if (Input::file('photo_desktop')) {
          
            // Save the photo

            // if($getPlacementInfo->image_type == "browse_side_A" || $getPlacementInfo->image_type == "browse_side_B"){ //left 
            //     Image::make(Input::file('photo'))->fit(292, 390)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(631, 390)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');

            // }else if($getPlacementInfo->image_type == "listings_side"){//listing right
            //     Image::make(Input::file('photo'))->fit(292, 390)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(631, 390)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }else if($getPlacementInfo->image_type == "listings_content"){//top
            //     Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }else if($getPlacementInfo->image_type == "browse_top"){//top
            //     Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }else if($getPlacementInfo->image_type == "browse_bottom"){//bottom
            //     Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg'); 
            // }else if($getPlacementInfo->image_type == "home_premium_A" || $getPlacementInfo->image_type == "home_premium_B" || $getPlacementInfo->image_type == "home_premium_C" ||  $getPlacementInfo->image_type == "home_main_A" ||  $getPlacementInfo->image_type == "home_main_B"){ //jumbomain
            //     Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(496, 367)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }else if($getPlacementInfo->image_type == "sub1_A" || $getPlacementInfo->image_type == "sub1_B" || $getPlacementInfo->image_type == "sub1_C" || $getPlacementInfo->image_type == "sub1_premium_1" || $getPlacementInfo->image_type == "sub1_premium_2"){ // jumboSmallA
            //     Image::make(Input::file('photo'))->fit(335, 178)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(355, 178)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }else if($getPlacementInfo->image_type == "sub2_A" || $getPlacementInfo->image_type == "sub2_B" || $getPlacementInfo->image_type == "sub2_C" || $getPlacementInfo->image_type == "sub2_premium_1" || $getPlacementInfo->image_type == "sub2_premium_2" ){ //jumboSmallb
            //     Image::make(Input::file('photo'))->fit(335, 178)->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            //     Image::make(Input::file('photo'))->fit(355, 178)->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            // }


            $row->image_desktop = $row->id . '.jpg';
            Image::make(Input::file('photo_desktop'))->save('uploads/banner/advertisement/desktop/' . $row->id . '.jpg');
            $row->save();
        }
        if (Input::file('photo_mobile')) {
            $row->image_mobile = $row->id . '.jpg';
            Image::make(Input::file('photo_mobile'))->save('uploads/banner/advertisement/mobile/' . $row->id . '.jpg');
            $row->save();
        }
           // if(Input::has('link')){
           //    $referral_code = substr(array_get($input,'link'), 36);
           //    $get_referral_points = BannerPlacementPlans::where('id',$row->placement_plan_id)->first();
           //    $find_user_referral_info = User::where('referral_code',$referral_code)->first();
           //      if(!is_null($find_user_referral_info)){
           //            if($find_user_referral_info->id != Auth::user()->id){
           //               $get_referral_type = ReferralTypes::where('id',3)->first();
           //                if($find_user_referral_info){
           //                  $field = new UserReferrals;
           //                  $field->user_id = $row->user_id;
           //                  $field->unique_id = $row->id;
           //                  $field->referral_type = $get_referral_type->id;
           //                  $field->referral_points = $get_referral_points->points;
           //                  $field->referrer_id = $find_user_referral_info->id;
           //                  $field->save();
           //                }
           //            }else{
           //                $row->status=2;
           //                $row->save();
           //                return Response::json(['error' => "You cannot use your own referral code"]);

           //            }
           //      }else{
           //         return Response::json(['error' => "Referral code not found"]);
           //      }          
           //  }
           $transaction_code = str_random(35);
           $banner_plan = new PaypalPaymentForBannerPlan;
           $banner_plan->user_id = Auth::user()->id;
           $banner_plan->placement_plan_id = $row->placement;
           $banner_plan->subscription_id = $row->id;
           $banner_plan->save();
           $banner_plan->invoice_number = base64_encode($row->id.$transaction_code);
           $banner_plan->save();
      
            $notiheaderuser = new UserNotificationsHeader;
            $notiheaderuser->title       ='Bidding';
            $notiheaderuser->read_status = 1;
            $notiheaderuser->reciever_id = Auth::user()->id;
            $notiheaderuser->photo_id = Auth::user()->id;
            $notiheaderuser->save();
            $notiuser = new UserNotifications;
            $notiuser->notification_id  = $notiheaderuser->id;
            $notiuser->message = 'Banner review usually takes 24 hours to complete. Please wait.';
            $notiuser->type= "Banner Review";
            $notiuser->status= "1";
            $notiuser->save();

       if($new){
              return Response::json(['body' => "Banner submitted for review"]);
        } 
        else {
             return Response::json(['body' => "Banner updated"]);
           }
       }

 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
      public function purchaseBannerPlacement(){
      }
         /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
      public function saveBannerPlacement(){

    $new = true;
        $input = Input::all();
        dd($input);
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerSubscriptions::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'                 => $new ? 'image|max:3000' : 'image|max:3000',
            'banner_name'           => 'required',
            'advertiser_name'         => 'required',
            'banner_placement_id'     => 'required',
            'banner_placement_plan'   => 'required',
            'cat_id'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'photo'              => 'photo',
            'banner_name'      => 'banner name',
            'advertiser_name'    => 'advertiser',
            'banner_placement_id'       => 'Banner placement',
            'banner_placement_plan'  => 'Banner placement plan',
            'cat_id'  => 'Category Id',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerSubscriptions;
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
        } else {
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->save();
        }   
            $row->save();
            $get_plan_duration = BannerPlacementPlans::find($row->placement_plan);
            $date = strtotime(date("Y-m-d H:i:s", strtotime($row->created_at)) . " +".$get_plan_duration->duration." month");
            $row->expiration = date("Y-m-d H:i:s", $date);
            $row->save();

          if (Input::file('photo')) {
              // Save the photo
              Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-'.$row->id . '.jpg');
              $row->photo = 'banner-'.$row->id . '.jpg';
              $row->save();
          }

      return Response::json(['body' => ['Banner Advertisement Saved']]);

  }

         /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
  public function viewBannerPlacement() {
      $banner_subs_id = Request::input('id');
       
      $row   = BannerSubscriptions::join('users', 'banner_subscriptions.user_id', '=', 'users.id') 
                                       ->join('banner_placement_plans','banner_placement_plans.id', '=', 'banner_subscriptions.placement_plan')
                                       ->join('languages','languages.id', '=', 'banner_subscriptions.language_id')
                                       ->select('users.*', 'languages.*', 'banner_placement_plans.*', 'banner_subscriptions.*')                          
                                       ->where(function($query) use ($banner_subs_id) {
                                             $query->where('banner_subscriptions.status', '=', '1')
                                                   ->where('banner_subscriptions.id', '=', $banner_subs_id);                                                                                                               
                                        })->first(); 

     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
  public function restrictBannerAction(){
    $verifyBannerStatus = BannerAdvertisementSubscriptions::where('id',Request::input('id'))->where('banner_status','=',6)->first();
    if(!is_null($verifyBannerStatus)){
       return Response::json(['errorVerify' => 'Invalid row specified']);
    }
  }
   
}