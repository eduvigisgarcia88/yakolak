<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
// use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use View;
use Response;
use Validator;
use Request;
use Mail;
use Input;
use Paginator;
use App\User;
use App\Localization;
use App\Country;
use App\Phrases;
use App\LocalizationPhrases;



class LocalizationController extends Controller
{
   
   public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

    $result = $this->doList();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['countries'] = Country::where('status',1)->get();
    $this->data['title'] = "Localization";
    $this->data['refresh_route'] =url('admin/localization/refresh');
    return View::make('admin.localization-management.list', $this->data);
   }
   public function doList(){
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search') ? : '';
      $status = Request::input('status') ? : '';
      $per = Request::input('per') ?: 10;
          if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
          });

              $rows =  Localization::where(function($query) use ($search) {
                                        $query->where('name', 'LIKE', '%' . $search . '%');
                                    })
                              ->where(function($query) use ($status) {
                                        $query->where('status', 'LIKE', '%' . $status . '%');
                                    })
                              ->where('status','!=',3)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
                   
        } else {
             $count =   Localization::where(function($query) use ($search) {
                                        $query->where('name', 'LIKE', '%' . $search . '%');
                                    })
                               ->where(function($query) use ($status) {
                                        $query->where('status', 'LIKE', '%' . $status . '%');
                                })
                              ->where('status','!=',3)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Localization::where(function($query) use ($search) {
                                        $query->where('name', 'LIKE', '%' . $search . '%');
                                    })
                              ->where(function($query) use ($status) {
                                        $query->where('status', 'LIKE', '%' . $status . '%');
                                    })
                              ->where('status','!=',3)
                              ->orderBy($result['sort'], $result['order'])
                              ->paginate($per);
        }
         // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          return $result;
        }
       
   }
   public function removeLocalization(){
    $row = Localization::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = 3;
        $row->save();
        return Response::json(['body'=> 'Localization Removed']);
    }else{
        return Response::json(['error'=> 'Error on finding the id']);
    }
   }
   public function enableLocalization(){
    $row = Localization::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = 1;
        $row->save();
        return Response::json(['body'=> 'Localization Enabled']);
    }else{
        return Response::json(['error'=> 'Error on finding the id']);
    }

   }
   public function disableLocalization(){
    $row = Localization::find(Request::input('id'));
    if(!is_null($row)){
        $row->status = 2;
        $row->save();
        return Response::json(['body'=> 'Localization Disabled']);
    }else{
        return Response::json(['error'=> 'Error on finding the id']);
    }

   }
   public function editLocalizationPhrase(){
    $rows = LocalizationPhrases::find(Request::input('id'));
     if(!is_null($rows)){
        return Response::json($rows);    
     }else{
        return Response::json(['error'=>'Erron on finding the id']);
     }
   }
  public function editLocalization(){
    $rows = Localization::find(Request::input('id'));
     if(!is_null($rows)){
        return Response::json($rows);    
     }else{
        return Response::json(['error'=>'Erron on finding the id']);
     }
   }
   public function getLocalizationPhrases(){
      $id = Request::input('id');
      $row = LocalizationPhrases::where('localization_id',$id)->where('status','=',1)->get();

      $rows = '<div class="table-responsive" id="table-localization-phrases"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Phrase Name</small></th>' .
                      '<th><small>Phrase Value</small></th>'.
                      '<th><small>Created Date</small></th>' .
                      '<th><small>Last Edited</small></th>' .
                      '<th class="text-right"><small>Tools<small></th><tbody></tr>';

        foreach ($row as $key => $field) {
             $rows .= '<tr data-id='.$field->id.' class="'.($field->status == 4 ? 'blockedColor':'').''.($field->status == 3 ? 'unapprovedColor':'').'">' .
                                 '<td><small>'.$field->localization_phrase_name.'</small></td>' .
                                 '<td><small>'.$field->localization_phrase_value.'</small></td>' .
                                 '<td><small>'.$field->created_at.'</small></td>'.
                                 '<td><small>'.$field->updated_at.'</small></td>'.  
                                 '<td>';      
             $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-phrase"><i class="fa fa-pencil"></i></button>';
         }

         return Response::json($rows);
   }
   public function localizationPhraseSave(){
        $new = true;
        $input = Input::all();
        if(array_get($input, 'id')) {
            // get the user info
            $row = LocalizationPhrases::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
         $rules = [
             // 'phrase_name'                    => 'required',
             'phrase_value'                    => 'required',
          
        ];
        // field name overrides
        $names = [   
            // 'phrase_name'                    => 'Phrase Name',
            'phrase_value'                    => 'Phrase Value',
        ];  

           // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        if($new) {
          $row = new LocalizationPhrases;
        }
          // $row->localization_phrase_name = array_get($input,'phrase_name');
          $row->localization_phrase_value = array_get($input,'phrase_value');
          $row->save();

        if($new){
               return Response::json(['body' => "Localization Phrase Created"]);
        } else {
               return Response::json(['body' => "Localization Phrase updated"]);
        }

   }
    public function localizationSave(){
        $new = true;
        $input = Input::all();
        if(array_get($input, 'id')) {
            // get the user info
            $row = Localization::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
         $rules = [
             'name'                    => 'required',
             // 'theme'                    => 'required',
          
        ];
        // field name overrides
        $names = [   
            'name'                    => 'Name',
            // 'theme'                    => 'Theme',
        ];  

           // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        if($new) {
          $row = new Localization;
        }
          $row->name = array_get($input,'name');
         /* $row->theme = array_get($input,'theme');*/
          $row->save();
          if($new){
                 $get_phrases = Phrases::where('status',1)->get();
              foreach ($get_phrases as $key => $field) {
                  $save_phrases = new LocalizationPhrases;
                  $save_phrases->localization_id = $row->id;
                  $save_phrases->localization_phrase_name = $field->phrase_name;
                  $save_phrases->localization_phrase_value = $field->phrase_value;
                  $save_phrases->save();
              }
          }
         

        if($new){
               return Response::json(['body' => "Localization Created"]);
        } else {
               return Response::json(['body' => "Localization updated"]);
        }

   }
}
