<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

//utils
use Response;
use Input;
use View;
use Validator;
use Carbon\Carbon;
use Auth;
use Paginator;
use DB;
use Mail;
use Session;
use Redirect;

//models
use App\User;
use App\Advertisement;
use App\Category;
use App\AdvertisementPhoto;
use App\Country;
use App\Usertypes;
use App\AdRating;
use App\UserAdComments;
use App\UserLogs;
use App\Newsletters;
use App\BannerPlacementPlans;
use App\BannerAdvertisementMerge;
use App\BannerAdvertisementSubscriptions;
use App\CountryBannerPlacement;
use App\BannerDefault;
use Socialite;
use Image;

class AuthController extends Controller
{
  public function index(){

    $this->data['title'] = "Admin";
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)
                                                  ->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)
                                                  ->get();
    return View::make('admin.form', $this->data);
  }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * Displays the login form
     *
     */
    public function frontendLogin() {

        if (!Auth::check()) {
            $this->data['title'] = "Login";
  
      $row  = Advertisement::orderBy('id', 'desc')->first();

      
      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city')                        
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
     
      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

     
      $this->data['adslatestbids_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->skip(4)->take(4)->get(); 

      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                           
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['latestads_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

//dd( $this->data['latestads2']);
       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
      $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get();                                    
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('status',1)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::where('type',1)->get();
      // $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get(); 
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

       $count_ads =   Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get(); 
      $count_bids = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->get();

       $this->data['max_ads_count'] = count($count_ads);
       $this->data['max_bids_count'] = count($count_bids);
         if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry  = Session::get('selected_location');
          } 

          $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first();
          

          $getBannerPlacementPlan = BannerPlacementPlans::where('status',1)
                                                        ->where('location_id',1)
                                                        ->lists('id');
 
          //15 placements                                     

          $jumboPaidBanners = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

          

          $jumboPaidBannersGetIds = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();
                                                         
          $getCountryBannerPlacementPlan = CountryBannerPlacement::where('status',1)
                                                                 ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                                if($selectedCountry != "ALL"){
                                                                                   $query->where('country_id','=',$getCountryCode->id);
                                                                                }else{
                                                                                  $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                   $query->where('country_id','=',$getDefaultCountry->id);
                                                                                 }       
                                                                       })
                                                                    ->where('reserve_status',1)
                                                                    ->groupBy('placement_id')
                                                                    ->get();

                                                              

          $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',1)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($jumboPaidBanners))
                                                               ->lists('id')
                                                               ->toArray();
          


          $merge_array = array_merge(array_filter($jumboPaidBanners),array_filter($getBannerDefaultsPerCountry));

           if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
          
        

          if(!is_null($jumboPaidBannersGetIds)){

              foreach ($jumboPaidBannersGetIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 1;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }

          if(!is_null($getBannerDefaultsPerCountry))
          {

              foreach ($getBannerDefaultsPerCountry as $key => $field) {

               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
               
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 1;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
                                                   
        

          //Types  
          // 1 for paid banners and 2 for Default

          $this->data['get_banner_jumbo_main'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })                                                                          ->where('position',1)
                                                                          ->where('designation',1)
                                                                          ->orderBy('order_no','asc')
                                                                          ->get();

          $this->data['get_banner_jumbo_sub_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',2)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();

          $this->data['get_banner_jumbo_sub_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',3)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();
            return view('/home', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
           // return Redirect::to('dashboard');
        }

    }
     
    /**
     * Attempt to do login
     *
     */
    public function doFrontendLogin(Request $request) {
      //      $request = Input::all();
        $username = $request->input('email');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        
        if($honeypot) {
            // robot detected
            $error = trans('validation.blank');
        } 
        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
            // login successful
            return redirect()->intended();
        }
        else if(Auth::validate(['username' => $username, 'password' => $password])) {
            $user = User::where('username', $username)->first();

            if($user->status == 0) {
                // user is not active
                $error = "This account is currently inactive.";
            }
        }
        else {
            // invalid login
            $error = 'Invalid email or password.';
        }
        // return error
        return redirect()->action('AuthController@login')
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
      // $rules = [
      //       'email' => 'required',
      //       'password' =>  'required',
      //       // 'g-recaptcha-response' => 'required|captcha'
      //   ];

      //   // field name overrides
      //   $names = [
      //       'email' => 'username|email',
      //       'password' => 'password',
      //       // 'g-recaptcha-response' => 'invalid captcha',
      //   ];

      //   // do validation
      //   $validator = Validator::make(Input::all(), $rules);
      //   $validator->setAttributeNames($names);

      //   // return errors
      //   if($validator->fails()) {
      //       return Response::json(['error' => array_unique($validator->errors()->all())]);
      //   }

      //   // $username = $request->input('email');
      //   // $password = $request->input('password');
      //   // $remember = (bool) $request->input('remember');
      //   // $honeypot = $request->input('honeypot');


      //   $username = array_get($request, 'email');
      //   $password = array_get($request, 'password');
      //   $remember = (bool) array_get($request, 'remember');
      //   // $honeypot = $request->input('honeypot');

      //   // if($honeypot) {
      //   //     // robot detected
      //   //     $error = trans('validation.blank');
      //   // } 
      //   if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
      //       // login successful
      //       return redirect()->intended();
      //   }
      //   else if(Auth::validate(['username' => $username, 'password' => $password])) {
      //       $user = User::where('username', $username)->first();

      //       if($user->status == 0) {
      //           // user is not active
      //           // $error = "This account is currently inactive.";
      //            return Response::json(['error' => "invalid username or password"]);
      //       }
      //   }
      //   else {
      //       // invalid login
      //         // return Response::json(['error' => "invalid username or password"]);
      //   }
      //   // return error
      // return Response::json(['error' => "invalid username or password"]);
    }


    public function login() {

      if (!Auth::check()) {
            $this->data['title'] = "Login";
      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['country_code_session'] = Session::get('default_location');
      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')                                       
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements_photo.id', 'desc')->take(8)->get(); 

      $this->data['adslatestbids_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country') 
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 

      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                           
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['latestads_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

//dd( $this->data['latestads2']);
       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get();
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
       if(Session::has('selected_location') == null){
        $selectedCountry = Session::get('default_location');
      }else{
        $selectedCountry = Session::get('selected_location');
      }
     
   
      $this->data['categories'] = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                           ->where('home',2)
                                           ->where('status',1)
                                           ->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::where('type',1)->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      
       $count_ads =   Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get(); 
      $count_bids = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->get();

       $this->data['max_ads_count'] = count($count_ads);
       $this->data['max_bids_count'] = count($count_bids);
          if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry  = Session::get('selected_location');
          } 

          $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first();
          $getBannerPlacementPlan = BannerPlacementPlans::where('status',1)
                                                        ->where('location_id',1)
                                                        ->lists('id');
 
          //15 placements                                     

          $jumboPaidBanners = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

          

          $jumboPaidBannersGetIds = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();
                                                         
          $getCountryBannerPlacementPlan = CountryBannerPlacement::where('status',1)
                                                                 ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                                if($selectedCountry != "ALL"){
                                                                                   $query->where('country_id','=',$getCountryCode->id);
                                                                                }else{
                                                                                  $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                   $query->where('country_id','=',$getDefaultCountry->id);
                                                                                 }       
                                                                       })
                                                                    ->where('reserve_status',1)
                                                                    ->groupBy('placement_id')
                                                                    ->get();

                                                              

          $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',1)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($jumboPaidBanners))
                                                               ->lists('id')
                                                               ->toArray();
          


          $merge_array = array_merge(array_filter($jumboPaidBanners),array_filter($getBannerDefaultsPerCountry));

           if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
          
        

          if(!is_null($jumboPaidBannersGetIds)){

              foreach ($jumboPaidBannersGetIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 1;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }

          if(!is_null($getBannerDefaultsPerCountry))
          {

              foreach ($getBannerDefaultsPerCountry as $key => $field) {

               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
               
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 1;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
                                                   
        

          //Types  
          // 1 for paid banners and 2 for Default

          $this->data['get_banner_jumbo_main'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })                                                                          ->where('position',1)
                                                                          ->where('designation',1)
                                                                          ->orderBy('order_no','asc')
                                                                          ->get();

          $this->data['get_banner_jumbo_sub_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',2)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();

          $this->data['get_banner_jumbo_sub_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',3)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();
        return view('ads.main', $this->data);
        }
        else {
            // if user is already logged in
            return redirect()->intended();
            // return redirect()->route('dashboard');
            //return Redirect::to('/');
        }

    }
     
    /**
     * Attempt to do login
     *
     */
    public function doLogin(Request $request) {
        $username = $request->input('username');
        $password = $request->input('password');
        $remember = (bool) $request->input('remember');
        $honeypot = $request->input('honeypot');
        $captcha = $request->input('g-recaptcha-response');
        $error = [];
//         if(!$username && !$password && !$captcha){
//           $error['username_required'] = "Username is required";
//           $error['password_required'] = "Password is required";
//           $error['captcha_required'] = "Captcha is required";
//         }
      
        if (!$captcha) {
          $error['invalid_login'] = 'Invalid email or password.';
          $error['captcha_required'] = 'Captcha is required';
        }
      
        if (!$password) {
          $error['invalid_login'] = 'Invalid email or password.';
        }
      
        if (!$username) {
          $error['invalid_login'] = 'Invalid email or password.';
        }

        // else if(!$username && !$password){
        //   $error['username_required'] = "Username is required";
        //   $error['password_required'] = "Password is required";
        // }else if(!$username){
        //   $error['username_required'] = "Username is required";
        // }else if(!$password){
        //   $error['password_required'] = "Password is required";
        // }
        // else if(!$captcha){
        //   $error['captcha_required'] = "Captcha is required";
        // }
        else if($honeypot){
          // robot detected
          $error = trans('validation.blank');
        }

        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
             $user = User::where('username', $username)->first();
              if($user->usertype_id != 3){
                $change_status = User::find(Auth::user()->id);
                $change_status->online_status = 1;
                $change_status->save();
                $save_logs = new UserLogs;
                $save_logs->user_id = Auth::user()->id;
                $save_logs->save();
                $change_status->last_login = $save_logs->created_at;
                $change_status->save();
                //for new users
                $check_logs = UserLogs::where('user_id',Auth::user()->id)->get();
                if(count($check_logs) == 1){
                   // for site tips
                   $newsletter_tips = Newsletters::where('id', '=', '20')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tips) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tips', ['newsletter' => $newsletter_tips], function($message) use ($newsletter_tips,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tips->subject.'');
                             });
                    }
                  // for site tutorials
                   $newsletter_tutoial = Newsletters::where('id', '=', '21')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tutoial) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tutorial', ['newsletter' => $newsletter_tutoial], function($message) use ($newsletter_tutoial,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tutoial->subject.'');
                             });
                    }
                }
                $client_ip = $request->ip();
                $client_location = geoip()->getLocation($client_ip)->iso_code;
                  if(Auth::user()->country == 0){
                    Session::set('default_location',strtolower($client_location));
                    Session::set('selected_location',Session::get('default_location'));
                    $country_code = Session::get('default_location');
                  }else{
                    $get_user_country = Country::where('id',Auth::user()->country)->first();
                    if($get_user_country == null){
                       Session::set('default_location','ALL');
                       Session::set('selected_location','ALL');
                    }else{
                       $country_code = strtolower($get_user_country->countryCode);
                       Session::set('default_location',$country_code);
                       Session::set('selected_location',$country_code);
                    }
                   
                  }
                // Session::forget('selected_location');
                if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->back();
                }
                 // return redirect()->route('dashboard');    
              }else{
                 if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->to('admin');
                }
              }
            
        }else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 2], $remember)) {
             $user = User::where('username', $username)->first();
              if($user->usertype_id != 3){
                $change_status = User::find(Auth::user()->id);
                $change_status->online_status = 1;
                $change_status->save();
                $save_logs = new UserLogs;
                $save_logs->user_id = Auth::user()->id;
                $save_logs->save();
                $change_status->last_login = $save_logs->created_at;
                $change_status->save();
                //for new users
                $check_logs = UserLogs::where('user_id',Auth::user()->id)->get();
                if(count($check_logs) == 1){
                   // for site tips
                   $newsletter_tips = Newsletters::where('id', '=', '20')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tips) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tips', ['newsletter' => $newsletter_tips], function($message) use ($newsletter_tips,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tips->subject.'');
                             });
                    }
                  // for site tutorials
                   $newsletter_tutoial = Newsletters::where('id', '=', '21')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tutoial) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tutorial', ['newsletter' => $newsletter_tutoial], function($message) use ($newsletter_tutoial,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tutoial->subject.'');
                             });
                    }
                }
                
                  
                if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->back();
                }
              }
              else
              {
                 if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->to('admin');
                }
              }
            
        }
        else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 3], $remember)) {
             $user = User::where('username', $username)->first();
              if($user->usertype_id != 3){
                $change_status = User::find(Auth::user()->id);
                $change_status->online_status = 1;
                $change_status->save();
                $save_logs = new UserLogs;
                $save_logs->user_id = Auth::user()->id;
                $save_logs->save();
                $change_status->last_login = $save_logs->created_at;
                $change_status->save();
                //for new users
                $check_logs = UserLogs::where('user_id',Auth::user()->id)->get();
                if(count($check_logs) == 1){
                   // for site tips
                   $newsletter_tips = Newsletters::where('id', '=', '20')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tips) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tips', ['newsletter' => $newsletter_tips], function($message) use ($newsletter_tips,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tips->subject.'');
                             });
                    }
                  // for site tutorials
                   $newsletter_tutoial = Newsletters::where('id', '=', '21')->where('newsletter_status', '!=', '2')->first();
                    if ($newsletter_tutoial) {
                             $user_email = Auth::user()->email;
                             Mail::send('email.site_tutorial', ['newsletter' => $newsletter_tutoial], function($message) use ($newsletter_tutoial,$user_email) {
                                   $message->to($user_email)->subject(''.$newsletter_tutoial->subject.'');
                             });
                    }
                }
                $client_ip = $request->ip();
                $client_location = geoip()->getLocation($client_ip)->iso_code;
                $country_list = array_map('strtolower',Country::where('status',1)->lists('countryCode')->toArray());
                  if(Auth::user()->country == 0)
                  {
                    if(in_array($client_location, $country_list))
                      {
                        Session::set('default_location',$client_location);
                        Session::set('selected_location',$client_location);
                      }
                      else
                      {
                        Session::set('default_location','ALL');
                        Session::set('selected_location','ALL');
                      }
                  }
                  else
                  {
                    $get_user_country = Country::where('id',Auth::user()->country)->where('status',1)->first();
                    if($get_user_country)
                    {
                      $country_code = strtolower($get_user_country->countryCode);

                      if(in_array($country_code, $country_list))
                      {
                        Session::set('default_location',$country_code);
                        Session::set('selected_location',$country_code);
                      }
                      else
                      {
                        Session::set('default_location','ALL');
                        Session::set('selected_location','ALL');
                      }
                    }
                    else
                    {
                      Session::set('default_location','ALL');
                      Session::set('selected_location','ALL');
                    }
                  }
                // Session::forget('selected_location');
                if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->back();
                }
                
                 // return redirect()->route('dashboard');    
              }else{
                 if ($error) {
                  Auth::logout();
                  return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
                } else {
                  return redirect()->to('admin');
                }
              }
            
        } else {
            // invalid login
            $error['invalid_login'] = 'Invalid email or password.';
        }
        
        // return error
        // dd($error);
        Auth::logout();
        return redirect()->back()
                       //->withInput($request->except('password'))
                       ->with('notice', array(
                            'msg' => $error,
                            'type' => 'danger'
                       ));
    }
    /**
     * Displays the forgot password form
     *
     */
    public function forgotPassword() {
        $this->data['title'] = "Password Recovery";
        return View::make('users.forgotpass', $this->data);
    }

    /**
     * Attempt to send change password link to the given email
     *
     */
    public function doForgotPassword() {
        $user = User::where('email', Input::get('email'))
                    ->first();

        View::composer('emails.reminder', function($view) use ($user) {
            $view->with([
                'firstname' => $user->firstname
            ]);
        });

        $response = Password::remind(Input::only('email'), function($message) {
            $message->subject("Reset Password Request");
        });

        // (test) always say success
        $response = Password::REMINDER_SENT;

        switch ($response) {
            case Password::INVALID_USER:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'danger'
                ));

            case Password::REMINDER_SENT:
                return Redirect::back()->with('notice', array(
                    'msg' => Lang::get($response),
                    'type' => 'success'
                ));
        }
    }

    /**
     * Shows the change password form with the given token
     *
     */
    public function resetPassword($token = null) {
        if(is_null($token)) App::abort(404);

        if(Auth::guest()) {
            $this->data['token'] = $token;
            $this->data['title'] = "Reset Password";

            return View::make('users.resetpass', $this->data);
        } else {
            return Redirect::to('/');
        }
    }

    /**
     * Attempt change password of the user
     *
     */
    public function doResetPassword() {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        Password::validator(function($credentials) {
            return strlen($credentials['password']) >= 6 && $credentials['password'] != $credentials['email'];
        });

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);
            $user->save();
        });

        switch ($response) {
            case Password::INVALID_PASSWORD:
            case Password::INVALID_TOKEN:
            case Password::INVALID_USER:
                return Redirect::back()
                               ->withInput(Input::except('password'))
                               ->with('notice', array(
                                    'msg' => Lang::get($response),
                                    'type' => 'danger'
                ));
            case Password::PASSWORD_RESET:
                return Redirect::to('login')
                               ->with('notice', array(
                                    'msg' => Lang::get('reminders.complete'),
                                    'type' => 'success'
                ));
        }
    }

    public function frontendLogout() {
        Session::forget('default_location');
        Session::forget('selected_location');
        Session::forget('verify_close');
        if(Auth::check()){
               $change_status = User::find(Auth::user()->id);
               $change_status->online_status = 2;
               $change_status->save();
            if($change_status->online_status == 2){
               Auth::logout();
               return Redirect::to('/');
            }
        }
       

      }
        
   

    public function logout() {
        Auth::logout();
        return redirect()->action('AuthController@login');
    }
  
    public function facebook()
    {
      if (!Auth::check()) {
        return Socialite::driver('facebook')->redirect();
      } else {
        return redirect('dashboard');
      }
    }
  
    public function facebook_callback()
    {
      $social = Socialite::driver('facebook')->user();
      $findUser = User::where('facebook_auth', $social->id)->first();
      if ($findUser) {
        
        Auth::loginUsingId($findUser->id);
        return redirect('dashboard');
        
      } else {
        
        $user = new User;
        $user->facebook_auth = $social->id;
        $user->alias = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->name = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->username = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->email = $social->email;
        $user->user_plan_types_id = 1;
        $user->status = 1;
        $user->usertype_id = 1;
        $user->facebook_account = $social->profileUrl;
        $user->save();
        Image::make($social->avatar_original)->fit(300, 300)->save('uploads/' . $user->id . '.jpg');
        $user->photo = $user->id . '.jpg';
        $user->save();
        Auth::loginUsingId($user->id);
        return redirect('dashboard');
        
      }
    }
  
    public function twitter()
    {
      if (!Auth::check()) {
        return Socialite::driver('twitter')->redirect();
      } else {
        return redirect('dashboard');
      }
    }
  
    public function twitter_callback()
    {
      $social = Socialite::driver('twitter')->user();
      $findUser = User::where('twitter_auth', $social->id)->first();
      if ($findUser) {
        
        Auth::loginUsingId($findUser->id);
        return redirect('dashboard');
        
      } else {
        
        $user = new User;
        $user->twitter_auth = $social->id;
        $user->alias = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->name = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->username = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->email = $social->email;
        $user->user_plan_types_id = 1;
        $user->status = 1;
        $user->usertype_id = 1;
        $user->description = $social->user['description'];
        $user->twitter_account = 'https://twitter.com/'.$social->nickname;
        $user->save();
        Image::make($social->avatar_original)->fit(300, 300)->save('uploads/' . $user->id . '.jpg');
        $user->photo = $user->id . '.jpg';
        $user->save();
        Auth::loginUsingId($user->id);
        return redirect('dashboard');
        
      }
    }
  
    public function google()
    {
      if (!Auth::check()) {
        return Socialite::driver('google')->redirect();
      } else {
        return redirect('dashboard');
      }
    }
  
    public function google_callback()
    {
      $social = Socialite::driver('google')->user();
      $findUser = User::where('google_auth', $social->id)->first();
      if ($findUser) {
        
        Auth::loginUsingId($findUser->id);
        return redirect('dashboard');
        
      } else {
        
        $user = new User;
        $user->google_auth = $social->id;
        $user->alias = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->name = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->username = $social->nickname ? str_replace(' ', '_', $social->nickname) : str_replace(' ', '_', $social->name);
        $user->email = $social->email;
        $user->user_plan_types_id = 1;
        $user->status = 1;
        $user->usertype_id = 1;
        $user->save();
        Image::make($social->avatar_original)->fit(300, 300)->save('uploads/' . $user->id . '.jpg');
        $user->photo = $user->id . '.jpg';
        $user->save();
        Auth::loginUsingId($user->id);
        return redirect('dashboard');
        
      }
    }

}
