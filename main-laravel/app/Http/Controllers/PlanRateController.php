<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\UserPlanTypes;
use App\UserPlanRates;
use App\PlanRatesDropdown;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Request;
use App\AdRating;
use App\UserAdComments;
use App\Advertisement;

class PlanRateController extends Controller
{

  public function indexPlanRatesVendor()
    {
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        // get row set
        $result = $this->doListPlanRatesVendor();
        // assign vars to template
        $this->data['rows'] = $result['rows'];

        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/plan-rates");
        $this->data['title'] = "Plan Rates Settings";
        $this->data['refresh_route'] = url("admin/plan-rates/vendor/refresh");
        $this->data['usertype']  = Usertypes::all();
        $this->data['plan_rate_dropdown']  = PlanRatesDropdown::all();
        
        return view('admin.plan-management.plan-rates.vendor.list', $this->data);

    
    }

  public function indexPlanRatesCompany()
    {
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        // get row set
        $result = $this->doListPlanRatesCompany();
        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/plan-rates");
        $this->data['title'] = "Plan Rates Settings";
        $this->data['refresh_route'] = url("admin/plan-rates/company/refresh");
        $this->data['usertype']  = Usertypes::all();
        $this->data['plan_rate_dropdown']  = PlanRatesDropdown::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
        return view('admin.plan-management.plan-rates.company.list', $this->data);

    
    }

    /**
     * Build the list
     *
     */
   public function doListPlanRatesVendor() {
      $result['sort'] = Request::input('sort') ?: 'user_plan_rates.created_at';
      $result['order'] = Request::input('order') ?: 'asc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 1;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                               ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
        $count = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('user_plan_rates.rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value')  
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
 public function doListPlanRatesCompany() {
   $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 2;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });


        $rows = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                               ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
        $count = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value') 
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('user_plan_rates.rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanRates::select('user_plan_rates.id', 'user_plan_rates.duration', 'user_plan_rates.rate_name', 'user_plan_rates.discount', 'user_plan_rates.description', 'user_plan_rates.status', 'user_plan_rates.rate_status',
                        'user_plan_rates.created_at', 'user_plan_rates_dropdown.name',  'user_plan_rates_dropdown.value')  
                         ->leftjoin('user_plan_rates_dropdown', 'user_plan_rates_dropdown.id', '=', 'user_plan_rates.duration')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }
      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editVendor($id) {
    

        if (is_null(UserPlanRates::find($id))) {
            return abort(404);
        } else {
            $this->data['planrate'] = UserPlanRates::find($id);
            $this->data['title'] = 'Editing '.$this->data['planrate']->rate_name ;

             $this->data['usertype_id'] = $this->data['planrate']->usertype_id;
             $this->data['usertype']  = Usertypes::all();
             $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
            $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
            return view('admin.plan-management.plan-rates.vendor.edit', $this->data);
        }
    } 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */   
    public function editCompany($id) {
    

        if (is_null(UserPlanRates::find($id))) {
            return abort(404);
        } else {
            $this->data['planrate'] = UserPlanRates::find($id);
            $this->data['title'] = 'Editing '.$this->data['planrate']->rate_name ;

             $this->data['usertype_id'] = $this->data['planrate']->usertype_id;
             $this->data['usertype']  = Usertypes::all();
             $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
              $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
            return view('admin.plan-management.plan-rates.company.edit', $this->data);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function savePlanRates() {    

        // $input = Request::all();
        //dd($input);
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = UserPlanRates::find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
             'rate_name'                  => 'required|min:5|max:100',
             'duration'                   =>  'min:1|max:36',
             'discount'                   => 'required|numeric|min:1|max:100',
        );

        // field name overrides
        $names = array(
            'rate_name'              => 'rate name',
            'duration'              => 'duration',
            'discount'              => 'discount',

        
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new UserPlanRates;
        }
          if (array_get($input, 'rate_name')) {
              $row->rate_name     = array_get($input, 'rate_name');
            }
          else{
            $row->rate_name  = null;
          }
            if (array_get($input, 'usertype_id')) {
              $row->usertype_id     = array_get($input, 'usertype_id');
            }
          else{
            $row->usertype_id  = null;
          }
            if (array_get($input, 'duration')) {
              $row->duration     = array_get($input, 'duration');
            }
          else{
            $row->duration       = null;
          }
             if (array_get($input, 'discount')) {
              $row->discount = array_get($input, 'discount');
            }
          else{
            $row->discount   = null;
          }
           if (array_get($input, 'description')) {
              $row->description = array_get($input, 'description');
            }
          else{
            $row->description  = null;
          }

       
        $row->status    = 1;
        // save model
        $row->save();

 if ($new) {
        // return
        return Response::json(['body' => 'Plan Rate successfully added.']);
      } else {
        return Response::json(['body' => 'Plan Rate successfully updated.']);

      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        $this->data['title'] = "Create News";
        $this->data['usertypes'] = Usertypes::all();

      return view('admin.plan-management.plan-rates.create', $this->data);
        
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

 public function removePlanRates() {
          $row = UserPlanRates::find(Request::input('id'));

      if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Plan Rate Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the rate']]); 
      }
    }
     public function disablePlanRates() {
          $row = UserPlanRates::find(Request::input('id'));

      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Plan Rate Successfully Disabled']);      
      }else{
        return Response::json(['error' => ['error on finding the rate']]); 
      }
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

        public function view() {
    $row = UserPlanRates::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
      public function changePlanRateStatus(){

    $row = UserPlanRates::find(Request::input('id'));
    if(!is_null($row)){

      $row->rate_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Plan Rate status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit() {
    $input = Input::all();
    $row = UserPlanRates::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function getDuration() {
     $input = Input::all();
     $duration = Request::input('duration');
     $rows ="";     
     $get_duration = PlanRatesDropdown::all();
        foreach($get_duration as $value) {

           $rows .='<option value="'.$value->id.'" '.($value->id==$duration?'selected':'').'>'.$value->name.'</option>';
        }
     $rows .='</select>';
      return Response::json($rows);
    }

}

