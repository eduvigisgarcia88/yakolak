<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\City;
use Paginator;
use Response;
use App\AdRating;
use App\UserAdComments;



class CityController extends Controller
{
	public function index(){
	    $result = $this->doList();
	    $this->data['rows'] = $result['rows'];
	   	$this->data['title'] = "City Management";
	   	$this->data['refresh_route'] = url("admin/city/refresh");
	   	$this->data['city_id'] = City::where('status',1)->get();
	   	$this->data['country'] = Country::where('status',1)->orderBy('countryName','asc')->get();
	   	$this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
	  	return View::make('admin.country-management.city-management.list', $this->data);
	}
	public function doList(){
		 $result['sort'] = Request::input('sort') ?: 'name';
	      $result['order'] = Request::input('order') ?: 'asc';
	      $search = Request::input('search');
	      $status = Request::input('status');
	      $country = Request::input('country');
	      $per = Request::input('per') ?: 100;

	      if (Request::input('page') != '»') {
	        Paginator::currentPageResolver(function () {
	            return Request::input('page'); 
	        });

	        $rows = City::where(function($query) use ($search) {
	                                  $query->where('name', 'LIKE', '%' . $search . '%');
	                                        
	                                  })->where(function($query) use ($country) {
	                                  $query->where('country_id',$country);
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);

	      } else {
	         $count = City::where(function($query) use ($search) {
	                                  $query->where('name', 'LIKE', '%' . $search . '%');
	                                        
	                                  })->where(function($query) use ($country) {
	                                  $query->where('country_id',$country);
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);

	        Paginator::currentPageResolver(function () use ($count, $per) {
	            return ceil($count->total() / $per);
	        });

	      $rows = City::where(function($query) use ($search) {
	                                  $query->where('name', 'LIKE', '%' . $search . '%');
	                                        
	                                  })->where(function($query) use ($country) {
	                                  $query->where('country_id',$country);
	                                        
	                                  })
	                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
	                                ->orderBy($result['sort'], $result['order'])
	                                ->paginate($per);
	      }

	      // return response (format accordingly)
	      if(Request::ajax()) {
	          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows->toArray();
	          return Response::json($result);
	      } else {
	          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows;
	          return $result;
	      }	
	}
	public function changeStatus(){
		 $row = City::find(Request::input('id'));
		   if(!is_null($row)){
		      $row->status = Request::input('stat');
		      $row->save();
		       return Response::json(['body' => ['Country status has been changed']]);
		   }else{
	      return Response::json(['error' => ['erro Finding the id']]);
	    }	
	}
}