<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\UserPlanTypes;
use App\Usertypes;
use App\PlanPrice;
use App\Http\Controllers\Controller;
use Request;
use App\AdRating;
use App\UserAdComments;
use App\Advertisement;
use App\PromotionPlanDiscounts;
use App\PromotionBidPlanDiscounts;
use App\ThemePlan;
use App\PlanTerm;
use App\BidPoints;

class PlanController extends Controller
{

public function indexPlanVendor()
    {
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        // get row set
        $result = $this->doListPlanVendor();
        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['url'] = url("admin/plan-features");
        $this->data['title'] = "Listing Plans";
        $this->data['refresh_route'] = url("admin/plan/vendor/refresh");
        $this->data['promotion_plan_discounts'] = PromotionPlanDiscounts::where('status',1)->get();
        $this->data['usertype']  = Usertypes::where('type',1)->where('status',1)->get();
        $this->data['plan_themes']  = ThemePlan::where('status',1)->get();
        $this->data['plan_term']  = PlanTerm::where('status',1)->get();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                    ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                    ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                    ->orderBy('user_ad_comments.created_at','desc')
                                                    ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                    ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

        return view('admin.plan-management.plan.vendor.list', $this->data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
 public function indexPlanCompany(){
      $not_allowed = array(1,2);
      if(Auth::check()){
        if(in_array(Auth::user()->usertype_id, $not_allowed)){
            return redirect()->to('error/access/denied');
        }
      }
      else
      {
        return redirect()->to('error/access/denied');
      }
        // get row set
        $result = $this->doListPlanCompany();
        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/plan-features");
        $this->data['title'] = "Listing Plans";
        $this->data['refresh_route'] = url("admin/plan/company/refresh");
        $this->data['usertype']  = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                    ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                    ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                    ->orderBy('user_ad_comments.created_at','desc')
                                                    ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

        return view('admin.plan-management.plan.company.list', $this->data);

    }

    /**
     * Build the list
     *
     */
public function doListPlanVendor() {
   $result['sort'] = Request::input('sort') ?: 'user_plan_types.created_at';
      $result['order'] = Request::input('order') ?: 'user_plan_types.desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : '';
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = UserPlanTypes::leftjoin('theme_plans','theme_plans.id','=','user_plan_types.theme')
                               ->leftjoin('usertypes','usertypes.id','=','user_plan_types.usertype_id')
                               ->select('user_plan_types.*','theme_plans.name as theme_name','usertypes.type_name') 
                               ->where(function($query) use ($status) {
                                    $query->where('user_plan_types.status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                    $query->where('user_plan_types.plan_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('usertypes.type_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('theme_plans.name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('user_plan_types.ads', 'LIKE', '%' . $search . '%')
                                          ->orWhere('user_plan_types.auction', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('user_plan_types.usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->where('user_plan_types.status','!=',3)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                              
      } else {
        $count = UserPlanTypes::select('user_plan_types.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                    $query->where('plan_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('total_ads_allowed', 'LIKE', '%' . $search . '%')
                                          ->orWhere('point', 'LIKE', '%' . $search . '%')
                                          ->orWhere('point_exchange', 'LIKE', '%' . $search . '%')
                                          ->orWhere('img_per_ad', 'LIKE', '%' . $search . '%');
                                })
                                ->where(function($query) use ($usertype_id) {
                                     $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');  
                                })
                                ->where('status','!=',3)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanTypes::select('user_plan_types.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                    $query->where('plan_name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('total_ads_allowed', 'LIKE', '%' . $search . '%')
                                          ->orWhere('point', 'LIKE', '%' . $search . '%')
                                          ->orWhere('point_exchange', 'LIKE', '%' . $search . '%')
                                          ->orWhere('img_per_ad', 'LIKE', '%' . $search . '%');
                                 })
                                 ->where(function($query) use ($usertype_id) {
                                       $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');  
                                  })
                                ->where('status','!=',3)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

   /**
     * Build the list
     *
     */
public function doListPlanCompany() {
      $result['sort'] = Request::input('sort') ?: 'id';
      $result['order'] = Request::input('order') ?: 'asc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 2;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = UserPlanTypes::select('user_plan_types.*') 
                               ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('plan_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
        $count = UserPlanTypes::select('user_plan_types.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('plan_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanTypes::select('user_plan_types.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('plan_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    public function indexBidPoints(){
        $result = $this->dolistBidPoints();
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['title'] = "Bid Plans";
        $this->data['promotion_bid_discounts'] = PromotionBidPlanDiscounts::where('status',1)->get(); 
        $this->data['refresh_route'] = url("admin/plans/bid/refresh");
        $this->data['bid_points'] = BidPoints::where('status',1)->get();
        return view('admin.plan-management.bid.list', $this->data);
    } 
    public function dolistBidPoints(){
        $result['sort'] = Request::input('sort') ?: 'price';
        $result['order'] = Request::input('order') ?: 'asc';
        $search = Request::input('search');
        $status = Request::input('status');
        $usertype_id = Request::input('usertype_id') ? : 2;
        $per = Request::input('per') ?: 10;

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = BidPoints::where(function($query) use ($status) {
                                      $query->where('status', 'LIKE', '%' . $status . '%');      
                                    })
                                  ->where(function($query) use ($search) {
                                      $query->where('points', 'LIKE', '%' . $search . '%')
                                            ->orWhere('price', 'LIKE', '%' . $search . '%'); 
                                    })
                                  ->where('status', '!=', '2')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate($per);
                    
        } else {
          $count = BidPoints::where(function($query) use ($status) {
                                      $query->where('status', 'LIKE', '%' . $status . '%');   
                                    })
                                  ->where(function($query) use ($search) {
                                       $query->where('points', 'LIKE', '%' . $search . '%')
                                             ->orWhere('price', 'LIKE', '%' . $search . '%');
                                          
                                    })
                                  ->where('status', '!=', '2')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate($per);

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });
          $rows = BidPoints::where(function($query) use ($status) {
                                      $query->where('status', 'LIKE', '%' . $status . '%');
                                    })
                                  ->where(function($query) use ($search) {
                                    $query->where('points', 'LIKE', '%' . $search . '%')
                                          ->orWhere('price', 'LIKE', '%' . $search . '%');  
                                    })
                                  ->where('status', '!=', '2')
                                  ->orderBy($result['sort'], $result['order'])
                                  ->paginate($per);
        }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit() {
    $row = UserPlanTypes::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function save() {    

        $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = UserPlanTypes::find(array_get($input, 'id'));
            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
        $rules = array(
            'plan_name'        => 'required|min:3|max:20',
            'ads'              => 'required|min:1|max:45|numeric',
            'auction'          => 'required|min:1|max:45|numeric',
             'img_per_ad'      => 'required|min:1|max:40|numeric',
             'img_total'       => 'required|min:1|max:800|numeric',
             'video_total'     => 'required|min:1|max:10|numeric',
             'point'           => 'required|min:1|max:100|numeric',
             'bid'             => 'required|min:1|max:50|numeric',
             'point_exchange'  => 'required|min:0|max:500|numeric',
             'sms'             => 'required|min:1|max:200|numeric',
             'description'     => 'required',

        );

        // field name overrides
        $names = array(
             'plan_name'       => 'plan_name' ,
             'ads'             => 'ads' ,
             'auction'         => 'auction' ,
             'img_per_ad'      => 'img_per_ad',
             'img_total'       => 'img_total',
             'video_total'     => 'video_total',
             'point'           => 'point',
             'bid'             => 'bid' ,
             'point_exchange'  => 'point_exchange' ,
             'sms'             => 'sms',
             'description'     => 'description',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new UserPlanTypes;
        }
          if (array_get($input, 'usertype_id')) {
              $row->usertype_id    = array_get($input, 'usertype_id');
            }
          else{
            $row->usertype_id      = null;
          }
                    if (array_get($input, 'plan_name')) {
              $row->plan_name      = array_get($input, 'plan_name');
            }
          else{
            $row->plan_name        = null;
          }

           if (array_get($input, 'months6_price')) {
              $row->months6_price  = array_get($input, 'months6_price');
            }
          else{ 
            $row->months6_price    = 0;
          }
          if (array_get($input, 'months12_price')) {
              $row->months12_price = array_get($input, 'months12_price');
            }
          else{
            $row->months12_price   = 0;
          }
             if (array_get($input, 'ads')) {
              $row->ads     = array_get($input, 'ads');
            }
          else{
            $row->ads              = null;
          }
          if (array_get($input, 'auction')) {
              $row->auction        = array_get($input, 'auction');
            }
          else{
            $row->auction          = null;
          }
          if (array_get($input, 'img_per_ad')) {
              $row->img_per_ad     = array_get($input, 'img_per_ad');
            }
       
          else{
            $row->img_per_ad       = null;
          }
          if (array_get($input, 'img_total')) {
              $row->img_total      = array_get($input, 'img_total');
            }
       
          else{
            $row->img_total        = null;
          }
          if (array_get($input, 'total_ads_allowed')) {
              $row->total_ads_allowed     = array_get($input, 'total_ads_allowed');
            }
       
          else{
            $row->total_ads_allowed = null;
          } 
          if (array_get($input, 'video_total')) {
              $row->video_total     = array_get($input, 'video_total');
            }
       
          else{
            $row->video_total       = null;
          } 
          if (array_get($input, 'point')) {
              $row->point     = array_get($input, 'point');
            }
          
          else{
            $row->point             = null;
          } 
    
          if (array_get($input, 'reward_point')) {
              $row->reward_point     = array_get($input, 'reward_point');
            }
          else{
            $row->reward_point             = null;
          } 
    
    
          if (array_get($input, 'bid')) {
              $row->bid     = array_get($input, 'bid');
            }
          else{
            $row->bid               = null;
          } 
          if (array_get($input, 'point_exchange')) {
              $row->point_exchange     = array_get($input, 'point_exchange');
           }
          else{
            $row->point_exchange    = null;
          } 
          if (array_get($input, 'sms')) {
              $row->sms     = array_get($input, 'sms');
           }
          else{
            $row->sms               = null;
          } 
          if (array_get($input, 'description')) {
              $row->description     = array_get($input, 'description');
           }
          else{
            $row->description       = null;
          } 
        $row->status    = 1;
        // save model
        $row->save();

        // return
        return Response::json(['body' => 'News successfully added.']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

        return view('news.create', $this->data);
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
  public function getUserType(){

    $user_type_id = Request::input('id');
    $plan_id      = Request::input('plan_id');

    $get_usertype  = Usertypes::where('id', '=', $user_type_id)->first();
    $get_plan   = UserPlanTypes::where('id', '=', $plan_id)->first();

      $rows = '';
 
       $rows .= '<div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color" style="padding-top: 0;">6 Months Price</label>
                       <div class="col-sm-7 col-xs-7">';
                             if ($user_type_id == '1') {
                                 $rows .= ' <input type="text" class="form-control" id="row-months6_price_vendor" name="months6_price" value="'.$get_plan->months6_price.'">';
                             } else {
                                 $rows .= ' <input type="text" class="form-control" id="row-months12_price_compnany" name="months12_price" value="'.$get_plan->months6_price.'">';
                             }
                  $rows .= '</div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color" style="padding-top: 0;">12 Months Price</label>
                       <div class="col-sm-7 col-xs-7">';
                             if ($user_type_id == '1') {
                                 $rows .= ' <input type="text" class="form-control" id="row-months12_price" name="months12_price" value="'.$get_plan->months12_price.'">';
                             } else {
                                 $rows .= ' <input type="text" class="form-control" id="row-months12_price" name="months12_price" value="'.$get_plan->months12_price.'">';
                            }
                     $rows .= '</div>
                   </div>
                </div>
  ';

          
      return Response::json($rows);
    }


 public function removePlan() {
      $row = UserPlanTypes::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Plan Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

           /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

  public function changePlanStatus(){
    $row = UserPlanTypes::find(Request::input('id'));
    if(!is_null($row)){

      $row->plan_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Plan status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }

           /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

      public function getPlanRate() {
      $rows = '<div class="type-table"><table class="table table-striped">' .  
              '<thead class="tbheader">
                  <tr>
                    <!--<th><i class="fa fa-sort"></i> </th>-->
                    <th colspan="4"><i class="fa fa-sort"></i> TITLE</th>
                    <th><i class="fa fa-sort"></i> UPDATED AT</th>
                    <th><i class="fa fa-sort"></i> CREATED AT</th>
                    <th class="rightalign">TOOLS</th>
                  </tr>
                </thead>'.  
              '<tbody>';



        $rows .= '<tr data-id="1">' . 
                  
                  '<td colspan="4">awdaw</td>' . 
                  '<td>awdawdawd</td>' . '<td> awdwad </td>' .
                  '<td class="rightalign">' . 
                    '<button type="button" class="btn btn-xs btn-table btn-property-edit"><i class="fa fa-pencil"></i></button>&nbsp;'.  
                    '<button type="button" class="btn btn-xs btn-table btn-delete-classification"><i class="fa fa-trash"></i></button>'.
                    '<button type="button" class="btn btn-xs btn-table btn-delete-category"><i class="fa fa-trash"></i></button>' .
                  '</td>'.
                  '</tr>'; 
      

      $rows .= '</tbody></table></div>';

      if ($rows) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function editPlanPrice() {
    $row  = PlanPrice::find(Request::input('price_id'));
     if($row) {
           return Response::json($row);
        } else {
           return Response::json($row);
         
        }
    }
  public function manipulateBidPoints(){
    $row = BidPoints::find(Request::input('id'));
    if($row){
        return Response::json($row);
    }else{
        return Response::json(['error' => "The requested item was not found in the database."]);
    }
  } 
          /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
    public function savePrice() {
        $new = true;
        $input = Input::all();
        // check if an ID is passed
         $row = PlanPrice::find(Request::input('price_id'));

        if($row) {
            // this is an existing row
            $new = false;
        }
        $rules = array(
             'price'            => 'required|min:0|max:999999',
             'term'             => 'required',
             'duration'         => 'required|min:1',
             'order_no'         => 'required|min:1',
             'points_exchange'  => 'required|min:1'
        );

        // field name overrides
        $names = array(
             'price'            => 'price' ,
             'term'             => 'term' ,
             'duration'         => 'duration' ,
             'order_no'         => 'order no'
        );


        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new PlanPrice;
        }

        $get_plan_info = UserPlanTypes::where('id',array_get($input,'plan_id'))->first();
       

        $row->usertype_id    = $get_plan_info->usertype_id;

        if (array_get($input, 'plan_id')) {
              $row->user_plan_id    = array_get($input, 'plan_id');
            }
        else{
            $row->user_plan_id      = null;
          }
        if (array_get($input, 'term')) {
              $row->term    = array_get($input, 'term');
            }
        else{
            $row->term      = null;
        }
        if (array_get($input, 'price')) {
              $row->price    = array_get($input, 'price');
            }
        else{
            $row->price      = null;
        }
                if (array_get($input, 'priceDescription')) {
              $row->priceDescription    = array_get($input, 'priceDescription');
            }
        else{
            $row->priceDescription      = null;
        }
        $row->duration  = array_get($input,'duration');
        $row->order_no  = array_get($input,'order_no');
        $row->points_exchange = array_get($input,'points_exchange');
        $row->status    = 1;
        $row->save();

        // return
        if ($new) {
        return Response::json(['body' => 'Price successfully added.']);
          # code...
        } else {
        return Response::json(['body' => 'Price successfully updated.']);

        }
    }
      public function saveBidPoints() {
        $new = true;
        $input = Input::all();
        // check if an ID is passed
         $row = BidPoints::find(Request::input('id'));

        if($row) {
            // this is an existing row
            $new = false;
        }
        $rules = array(
             'points'            => 'required|min:1|max:999999',
             'price'             => 'required|min:0'
        );

        // field name overrides
        $names = array(
             'points'            => 'Bid Points' ,
             'price'             => 'Amount' ,

        );


        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new BidPoints;
        }
        $row->points  = array_get($input,'points');
        $row->price  = array_get($input,'price');
        $row->save();

        // return
        if ($new) {
        return Response::json(['body' => 'Bid Plan successfully added.']);
          # code...
        } else {
        return Response::json(['body' => 'Bid Plan successfully updated.']);

        }
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function getPlanPrice() {

      $plan_id = Request::input('plan_id');
      $check_plan_type  = UserPlanTypes::where('id',$plan_id)->first();
      $get_price = PlanPrice::leftjoin('plan_term','plan_term.id','=','user_plan_price.term')
                            ->select('user_plan_price.*','plan_term.name')
                            ->where('user_plan_price.user_plan_id','=', $plan_id)
                            ->where('user_plan_price.status','=', '1')
                            ->get();
      $rows = '<div class="type-table"><table class="table condensed" id="plan_prices_table" style="margin-bottom:0 !important;background-color:#f7f7f7;">' .  
              '<thead class="tbheader" style="border:0px !important">
                  <tr>
                    <th><small>TERM</small></th>
                    <th><small>DURATION</small></th>
                    <th><small>PRICE</small></th>
                    <th><small>ORDER NO</small></th>
                    <th class="text-right"><small>TOOLS</small></th>
                  </tr>
                </thead>'.  
              '<tbody>';

      if($check_plan_type->plan_name == "Free"){
           $rows .= '<tr>' . 
                        '<td><small>N/A</small></td>' . 
                        '<td><small>N/A</small></td>' .
                        '<td><small>N/A</small></td>' .
                        '<td class="text-right">N/A</td>'.
                    '</tr>';    

      } else{
           foreach ($get_price as $row) {
            $rows .= '<tr data-plan_id="' . $plan_id . '" data-price_id="' . $row->id . '" data-price="' . $row->price . '">' . 
                        '<td><small>'.ucfirst($row->name).($row->duration > 1 ? 's':'').'</small></td>' . 
                        '<td><small>'.$row->duration.'</small></td>' .
                        '<td><small>'.$row->price.'</small></td>' .
                        '<td><small>'.$row->order_no.'</small></td>' .
                        '<td class="text-right">' .
                          '<button type="button" class="btn btn-sm btn-default btn-price-edit"><i class="fa fa-pencil"></i></button>'.  
                          '<button type="button" class="btn btn-sm btn-default btn-price-delete"><i class="fa fa-trash-o"></i></button>'.
                        '</td>'.
                        '</tr>'; 
         }
      }       
  

      $rows .= '</tbody></table></div>';

      if (Request::input('id')) {
        return Response::json($rows);
      } else {
        return Response::json(['error' => "The requested item was not found in the database."]);
      }
    }

    public function savePromotionPlan(){
        $input = Input::all();
        $discount = array_get($input,'discount');
        $period  = array_get($input,'period');
        // foreach ($discount as $key => $field) {

        //     $rules = array(
        //          'period['.$key.']'           => 'required|min:1',
        //          'discount['.$key.']'             => 'required|min:0|max:100',
        //     );

        //     // field name overrides
        //     $names = array(
        //         'period['.$key.']'           => 'period' ,
        //         'discount['.$key.']'         => 'discount' ,

        //     );
            
        //     $validator = Validator::make($period, $rules);
        //     $validator->setAttributeNames($names); 
        // }
      
        // // return errors
        // if($validator->fails()) {
        //     return Response::json(['error' => array_unique($validator->errors()->all())]);
        // }

        if(Input::has('discount')){
            foreach ($discount as $key => $field) {
                $row = PromotionPlanDiscounts::find($key);
                $row->discount = $field;
                $row->period = $period[$key];
                $row->save();
            }
          return Response::json(['body'=>'Plan Promotion Discount']);
        } 
    }
     public function savePromotionBid(){
        $input = Input::all();
        $discount = array_get($input,'discount');
        $period  = array_get($input,'period');
        // foreach ($discount as $key => $field) {

        //     $rules = array(
        //          'period['.$key.']'           => 'required|min:1',
        //          'discount['.$key.']'             => 'required|min:0|max:100',
        //     );

        //     // field name overrides
        //     $names = array(
        //         'period['.$key.']'           => 'period' ,
        //         'discount['.$key.']'         => 'discount' ,

        //     );
            
        //     $validator = Validator::make($period, $rules);
        //     $validator->setAttributeNames($names); 
        // }
      
        // // return errors
        // if($validator->fails()) {
        //     return Response::json(['error' => array_unique($validator->errors()->all())]);
        // }

        if(Input::has('discount')){
            foreach ($discount as $key => $field) {
                $row = PromotionBidPlanDiscounts::find($key);
                $row->discount = $field;
                $row->period = $period[$key];
                $row->save();
            }
          return Response::json(['body'=>'Bid Promotion Discount']);
        } 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
   
    public function removePlanPrice() {
     $row = PlanPrice::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Plan Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

    public function enablePlan(){
      $row = UserPlanTypes::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Plan Enabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    public function disablePlan(){
      $row = UserPlanTypes::find(Request::input('id'));
       if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Plan Disabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    public function removeBidPlan(){
      $row = BidPoints::find(Request::input('id'));
      if(!is_null($row)){
         $row->status = 2;
         $row->save();
         return Response::json(['body' => 'Bid plan removed']);
      }else{
         return Response::json(['error' => ['error on finding the user']]); 
      }
    }

}