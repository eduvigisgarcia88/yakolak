<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use Carbon\Carbon;
use App\UserImportContacts;
use Input;
use Validator;
use Redirect;
use Session;
use Paginator;
use Response;
use App\AdvertisementPhoto;
use App\Advertisement;

class GmailContactImporterController extends Controller
{


 public function index() {
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();     
 	  $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
   	  $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
     
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id') 
                                       ->select('user_notifications.*', 'user_notifications_header.*') 
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                     
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();                                                 
    
      return view('contact.import', $this->data);

 }
  public function callback(Request $request) {
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();     
 	  $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
   	  $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
     
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id') 
                                       ->select('user_notifications.*', 'user_notifications_header.*') 
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                     
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();                                                 
    
      return view('contact.callback', $this->data);
 }


	public function saveImport()
{
  $input = Input::all();
  $new = true;
  if(Auth::check()){
      $user_id = Auth::user()->id;
  }else{
      $user_id = null;
  }

foreach (Input::get('email') as $emails)
{
  $row = UserImportContacts::where('user_id','=',$user_id)->where('email','=',$emails)->first();
  if (is_null($row)) {
      $row = new UserImportContacts;          
      $row->email = $emails;
      $row->user_id = $user_id;
      $row->Save();
  }

}
    if($new){
      return Response::json(['body' => "Contacts Successfully Imported"]);
    } 
    else {
         return Response::json(['body' => "Advertisement Successfully updated"]);
       }
}

  /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */


    public function referralRegister($referral_code){
       Session::put('referral_code', $referral_code);

      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')                                       
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 
      $this->data['adslatestbids_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')  
                                                   ->where('advertisements.status', '!=', '4')                                                                           
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 

      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                           
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['latestads_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

//dd( $this->data['latestads2']);
       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      //$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      // $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',1)->get();
      // $this->data['bid'] = Category::where('biddable_status','=',1)->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id') 
                                       ->select('user_notifications.*', 'user_notifications_header.*') 
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                     
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get(); 

      return view('referral.register', $this->data);

    }
}
   