<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use App\CustomAttributes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\AdRating;
use App\UserAdComments;
use App\Country;


class CategoryController extends Controller
{

  public function category_batch() {

    $categories = [];

    foreach($categories as $key => $val)
    {
      $row = new Category;
      $row->name     =  $val['val'];
      $row->country_code     =  'US';
      $row->sequence     =  ($key+1);
      $row->biddable_status = 1;
      $row->buy_and_sell_status = 2;
      $row->description = 'Default / Preload';
      $row->home = 1;
      $row->slug = str_slug($val['val'], '-');
      $row->save();
      $row->unique_id = base64_encode('cat-'.$row->id);
      $row->save();
    }
    
  }


  public function subcategory_batch($cat_id) {
    $subcategories = [];
    foreach($subcategories as $key => $val)
    {
      $getCatId = Category::where('id', $cat_id)->first();
      $row = new SubCategory;
      $row->cat_id   =  $getCatId->unique_id;
      $row->description = 'Preload / Default';
      $row->name = $val['val'];
      $row->slug =   str_slug($val['val'], '-');
      $row->country_code = 'de';
      $row->status = 1;
      $row->biddable_status = $getCatId->biddable_status;
      $row->buy_and_sell_status = $getCatId->buy_and_sell_status;
      $row->home = $getCatId->home;
      $row->sequence = ($key+1);
      $row->save();
      $row->unique_id = base64_encode('subcat-'.$row->id);
      $row->save();
    }
    
  }

   public function edit(){

    $row = Category::where('id',Request::input('id'))->first();

    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }

   public function subEdit(){
    $row = SubCategory::where('id',Request::input('id'))->first();
    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }
     public function saveSubCategoryTwo(){

      $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = SubCategoriesTwo::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
  
            'cat_id' => 'required',
            'name'=> 'required|unique:sub_categories,name' . (!$new ? ',' . $row->id : ''),
  
        ];
        // field name overrides
        $names = [
            'name' => 'category name',
            'description' => 'category description',
        ];
// do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
          $row = new SubCategoriesTwo;
          $row->subcat_main_id     = array_get($input, 'cat_id');
          $row->name = array_get($input, 'name');
          $row->slug =   str_slug(array_get($input, 'name'), '-');
          $row->save();
          $row->unique_id = base64_encode('subcattwo-'.$row->id);
          $row->save();
        } else {
          $row->name = array_get($input, 'name');
          $row->slug =   str_slug(array_get($input, 'name'), '-');
          $row->save();
        }   

      return Response::json(['body' => 'Sub Category (level 2) created successfully.']);
   }
   public function saveSubCategory(){

      $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = SubCategory::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
  
            'name'=> 'required',
            'description' => 'required',
        ];
        // field name overrides
        $names = [
            'name' => 'category name',
            'description' => 'description',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new4


        if($new) {

          $getCatId = Category::where('id',array_get($input, 'cat_id'))->first();

          $row = new SubCategory;
          $row->cat_id   =  $getCatId->unique_id;
          $row->description = array_get($input, 'description');
          $row->name = array_get($input, 'name');
          $row->slug =   str_slug(array_get($input, 'name'), '-');
          $row->country_code = $getCatId->country_code;
          $row->save();
          $row->unique_id = base64_encode('subcat-'.$row->id);
          $row->save();
        } else {
          $row->description = array_get($input, 'description');
          $row->name = array_get($input, 'name');
          $row->slug =   str_slug(array_get($input, 'name'), '-');
          $row->save();
        }   
      if($new){
         return Response::json(['body' => 'Sub Category created successfully.']);
      }else{
         return Response::json(['body' => 'Sub Category updated successfully.']);
      }
     
   }
   public function save(){
    $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
           // array_get($input, 'cat_id')

              // dd(array_get($input, 'cat_id'));
               $row = Category::find(array_get($input, 'id'));

              if(is_null($row)) {
                  return Response::json(['error' => "The requested item was not found in the database."]);
              }
              // this is an existing row
              $new = false;
        }

        $rules = [
  
            'name'=> 'required',
            /*'name'=> 'required',*/
            'sequence'=> 'required',


        ];
        // field name overrides
        $names = [
            'description' => 'category description',
            'name' => 'category name',
            'sequence' => 'display order',


        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
             $row = new Category;
             $row->name     =  array_get($input, 'name');
             $row->country_code     =  array_get($input, 'country_code');
             $row->sequence     =  array_get($input, 'sequence');
             $row->biddable_status = array_get($input, 'biddable_status');
             $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
             $row->description = array_get($input, 'description');
             $row->home = array_get($input, 'home');
             $row->slug =   str_slug(array_get($input, 'name'), '-');
             $row->save();
             $row->unique_id = base64_encode('cat-'.$row->id);
             $row->save();
         
        } else {

            $row->name  = array_get($input, 'name');
            $row->country_code     =  array_get($input, 'country_code');
            $row->sequence     =  array_get($input, 'sequence');
            $row->description = array_get($input, 'description');
            $row->slug =   str_slug(array_get($input, 'name'), '-');
            $row->biddable_status = array_get($input, 'biddable_status');
            $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
            $row->home = array_get($input, 'home');
            $row->save();
           }
       
          if($new){
            return Response::json(['body' => 'Category created successfully.']);
          }else{
            return Response::json(['body' => 'Category updated successfully.']);
          } 
      
   }
   public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];

      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/category/refresh');
      $this->data['title'] = "Category Management";
      $this->data['countries'] = Country::where('status','!=',5)->where('status',1)->orderBy('countryName','asc')->get();

      $this->data['categories'] = Category::with('subcategoryinfo')->where('status',1)->get();
      $this->data['subcategories'] = SubCategory::where('status',1)->get();
      $this->data['subcategoriestwo'] = SubCategoriesTwo::where('status',1)->get();
      $this->data['custom_attri'] = CustomAttributes::where('status',1)->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.category-management.list', $this->data);
   }

  public function doList(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $country = Request::input('country');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Category::where(function($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('country_code', 'LIKE', '%' . $search . '%');
                           })
                          ->where(function($query) use ($country) {
                                    $query->where('country_code', 'LIKE', '%' . $country . '%');
                          })
                          ->where('status','!=',3)
                          ->where('tag',1)
                          ->orderBy($result['sort'], $result['order'])
                          ->paginate($per);
      } else {
         $count = Category::where(function($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('country_code', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($country) {
                                    $query->where('country_code', 'LIKE', '%' . $country . '%');
                                  })
                                ->where('status','!=',3)
                                ->where('tag',1)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = Category::where(function($query) use ($search) {
                                    $query->where('name', 'LIKE', '%' . $search . '%')
                                          ->orWhere('country_code', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($country) {
                                    $query->where('country_code', 'LIKE', '%' . $country . '%');
                                  })
                                ->where('status','!=',3)
                                ->where('tag',1)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  public function checkSequenceSubCat(){

     $result['rows'] = SubCategory::where('cat_id',Request::input('id'))
                                            ->where('sequence','=',Request::input('order'))
                                            ->get();
     return Response::json($result);     
                                  
  }
   public function checkSequenceCat(){

     $result['rows'] = Category::where('sequence','=',Request::input('order'))
                                  ->get();

    return Response::json($result);     
                                  
  }
  public function getCountryCategories(){

       $country_code = Request::input('country_code');
       $categories = Category::where('country_code',$country_code)->where('status',1)->get();
       $rows = '<option class="hide">Select</option>';
          foreach ($categories as $key => $field) {
            $rows .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
          }
       $result['rows'] = $rows;

       return Response::json($result);
  }
  public function getSubcategories(){
        $id = Request::input('id');
        $check_category_status = Category::where('id',$id)->first();
        $row = SubCategory::where('cat_id',$check_category_status->unique_id)->where('status','!=',3)->get();
       
        $rows = '<div class="table-responsive" id="table-sub-categories"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Name</small></th>' .
                      '<th><small>Description</small></th>' .
                      '<tbody></tr>';
        if(count($row) > 0){
             foreach ($row as $key => $field) {
              // <a class="btn btn-xs btn-table btn-expand-subcat" data-id="'.$field->id.'"><i class="fa fa-plus"></i></a>
                $rows .= '<tr data-parent-id = "'.$id.'" data-id='.$field->id.' class="'.($field->status == 2 ? 'disabledColorSub':'').'">' .
                             '<td><small>'.$field->name.'</small></td>' .
                             '<td><small>'.$field->description.'</small></td>' .
                             // '<td><center><small>'.$field->created_at.'</small></center></td>'. 
                             '<td>';
              
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-sub"><i class="fa fa-trash-o"></i></button>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button>';
                 if($field->status == 1){
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable-sub"><i class="fa fa-lock"></i></button>';
                 }else{
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable-sub" '.($check_category_status->status == 2 ? 'disabled':'').'><i class="fa fa-unlock"></i></button>';
                 }
                 // $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat-two"><i class="fa fa-plus"></i></button>';

                $rows .=   '</td>'.
                         '</tr>';
              }       
        }else{
              $rows .= '<tr>' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td colspan ="5"><small><center>No Available Categories</center></small></td>' .
                     '</tr>';

        }

              $rows .= '</tbody></table></div>';
      
              return Response::json($rows);

  }
  public function getSubCategoriesTwo(){
        $id = Request::input('id');
        $check_category_status = SubCategory::where('id',$id)->first();
        $row = SubCategoriesTwo::where('subcat_main_id',$check_category_status->unique_id)->where('status','!=',5)->get();
        $rows = '<div class="table-responsive" id="table-sub-categories-two"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Name</small></th>' .
                      '<th><small><center>Description</center></small></th>' .
                      '<th><center><small>Created Date<small></center></th><tbody></tr>';
        if(count($row) > 0){
             foreach ($row as $key => $field) {
                $rows .= '<tr data-id='.$field->id.' class="'.($field->status == 2 ? 'disabledColor':'').'">' .
                             '<td><small>'.$field->name.'</small></td>' .
                             '<td><center><small>'.$field->description.'</small></center></td>' .
                             '<td><center><small>'.$field->created_at.'</small></center></td>'. 
                             '<td>';
              
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-sub"><i class="fa fa-trash-o"></i></button>';
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat-two"><i class="fa fa-pencil"></i></button>';
                 if($field->status == 1){
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable-sub"><i class="fa fa-lock"></i></button>';
                 }else{
                    $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable-sub" '.($check_category_status->status == 2 ? 'disabled':'').'><i class="fa fa-unlock"></i></button>';
                 }
                 // $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add"><i class="fa fa-building"></i></button>';

                $rows .=   '</td>'.
                         '</tr>';
              }       
        }else{
              $rows .= '<tr>' .
                             // '<td><small>' .  $row->mobile. '</small></td>' .
                             '<td colspan ="5"><small><center>No Available Categories</center></small></td>' .
                     '</tr>';

        }

              $rows .= '</tbody></table></div>';
      
              return Response::json($rows);
  }
  public function getCategoryList(){

     $this->data['title'] = "Category";
     $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    return View::make('category.list', $this->data);
  }
  public function deleteCategory(){
    $row = Category::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status =3;
      $row->save();
      return Response::json(['body'=> 'Category deleted']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
  public function disableCategory(){
    $row = Category::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status =2;
      $row->save();
      SubCategory::where('cat_id',$row->unique_id)->update(['status' => 2]);
      return Response::json(['body'=> 'Category disabled']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
  public function enableCategory(){
    $row = Category::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status =1;
      $row->save();
      SubCategory::where('cat_id',$row->unique_id)->update(['status' => 1]);
      return Response::json(['body'=> 'Category enabled']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
  public function disableSubcategory(){
    $row = SubCategory::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status =2;
      $row->save();
      return Response::json(['body'=> 'Sub Category disabled']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
  public function enableSubcategory(){
    $row = SubCategory::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status =1;
      $row->save();
      return Response::json(['body'=> 'Sub Category enabled']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
  public function deleteSubcategory(){
    $row = SubCategory::where('id',Request::input('id'))->first();
    if(!is_null($row)){
      $row->status = 3;
      $row->save();
      return Response::json(['body'=> 'Sub Category deleted']);
    }else{
      return Response::json(['error'=> 'error on finding the id']);
    }
  }
}