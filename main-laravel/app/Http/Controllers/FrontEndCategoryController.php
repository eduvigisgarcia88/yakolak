<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use App\SubCategoriesThree;
use App\CustomAttributes;
use App\Advertisement;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Country;
use App\AdvertisementPhoto;
use App\UserMessagesHeader;
use App\AdvertisementType;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\UserNotificationsHeader;
use App\BannerSubscriptions;
use App\City;
use App\CustomAttributesForDropdown;
use App\AdRating;
use App\UserAdComments;
use App\UserSearchHistory;
use App\BannerDefault;
use App\BannerPlacementPlans;
use Carbon\Carbon;
use Route;
use App\BannerAdvertisementSubscriptions;
use App\BannerAdvertisementMerge;
use Illuminate\Database\Eloquent\Collection;

class FrontEndCategoryController extends Controller
{


  public function countrySet(){
    $countries = array_map('strtolower', Country::where('status',1)->lists('countryCode')->toArray());
    $detect_ip = Request::ip();
    $detect_location = strtolower(geoip()->getLocation($detect_ip)->iso_code);
    $current = current(array_filter(array(Session::get('selected_location'), Session::get('default_location'))));
    if(Session::has('selected_location') || Session::has('default_location'))
    {
      Session::set('selected_location', $current);
      Session::set('default_location', $current);
    }
    else
    {
      if(in_array($detect_location, $countries))
      {
        Session::set('default_location', $detect_location);
        Session::set('selected_location', $detect_location);
      }
      else
      {
        Session::set('default_location', 'all');
        Session::set('selected_location', 'all');
      }
    }
  }


  public function getCategoryList(){
      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['title'] = "Category";
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('status',1)->get();
      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
   

      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city')                        
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
     
      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

      $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                      ->where(function($query) {
                                            $query->where('advertisements_photo.primary', '=', '1')
                                                 ->where('advertisements.status', '=', '1');                                
                                        })->skip(4)->take(4)->get(); 
      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                           
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 

       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get();  
       $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies); 
        $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
        $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
        $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

     $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

     return View::make('category.list', $this->data);
  }

  // public function putCountryCode(){
  //   $advertisements = Advertisement::all();
  //   foreach($advertisements as $code ){
  //     $category_slug = SubCategory::where('unique_id','=',$code->category_id)->first();
  //     if ($category_slug){
  //       // $pool = '123456789';
  //       // $random_code = substr(str_shuffle(str_repeat($pool, rand(3,9))), 0, 5);
  //       $code->category_slug = str_slug($category_slug->name);
  //       $code->save();
  //     }
  //   }
  // }
  //   public function putCountryCode(){
  //   $advertisements = Advertisement::all();
  //   foreach($advertisements as $code ){
  //     $category_slug = Advertisement::where('category_slug','=','buy-and-sell')->first();
  //     if ($category_slug){
  //       $pool = '123456789';
  //       $random_code = substr(str_shuffle(str_repeat($pool, rand(3,9))), 0, 5);
  //       $code->category_slug = $random_code;
  //       $code->save();
  //     }
  //   }
  // }

  public function getBuyCategory(){
      $this->data['title'] = "Buy";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.buy', $this->data);
  }

  public function getSellCategory(){
      $this->data['title'] = "Sell";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.sell', $this->data);
  }
  public function getBidCategory(){
      $this->data['title'] = "Bid";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      
      // YAH IT WORKS TILL THE END!!!!
      return View::make('category.bid', $this->data);
  }


  public function doListBuyandSell($country_code){
      $search = Request::input('search') == null ? '' : Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 9;
      // dd($country_code);
      // $get_id_on_cat = SubCategory::where('slug',$slug)->first();
      // $id = $get_id_on_cat->unique_id;
      // $get_type_sub = Category::where('unique_id',$get_id_on_cat->cat_id)->first();
      // $type = $get_type_sub->cat_type;

      // if(is_null(Category::where('slug',$slug)->first()) and is_null(SubCategory::where('slug',$slug)->first()) and is_null(SubCategoriesTwo::where('slug',$slug)->first()))
      //   {
      //     return '404';
      //   }

      // $cat_type = $type;
      $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration','advertisements.parent_category', 'advertisements.title', 'advertisements.category_id','advertisements.user_plan_id', 'advertisements.price','advertisements.feature','advertisements.ads_type_id', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.hierarchy_order', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     // ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');
                                                     
                                          })
                                          // ->where(function($query) use ($id){
                                          //      $query->where('advertisements.parent_category','=',$id)
                                          //          ->orWhere('advertisements.cat_unique_id','=',$id)
                                          //          ->orWhere('advertisements.category_id','=',$id)
                                          //          ->orWhere('advertisements.sub_category_id','=',$id);
                                          // })
                                          ->where(function($query) use ($country_code){
                                            if($country_code != 'all')
                                             {
                                              $query->where('advertisements.country_code','=',$country_code);      
                                             }
                                          })
                                         ->where('advertisements.ads_type_id',1)                                      
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.feature', 'desc')
                                         ->orderBy('advertisements.hierarchy_order', 'asc')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->take($per)
                                         ->get();

     $max_ads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     // ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');
                                                     
                                          })
                                          // ->where(function($query) use ($id){
                                          //      $query->where('advertisements.parent_category','=',$id)
                                          //          ->orWhere('advertisements.cat_unique_id','=',$id);
                                          // })
                                         ->where('advertisements.ads_type_id',1)                                                       
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->get();
      if(Request::ajax())
      {
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      }
      else
      {
          $result['rows'] = $rows;
          $result['cat_type'] = 1;
          $result['per'] = $per;
          $result['past_ad_type'] = 1;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return $result;
      }
  }
  public function doListAuction($country_code){
      $search = Request::input('search') == null ? '' : Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 9;
      // $get_id_on_cat = SubCategory::where('slug',$slug)->first();
      // $id = $get_id_on_cat->unique_id;
      // $get_type_sub = Category::where('unique_id',$get_id_on_cat->cat_id)->first();
      // $type = $get_type_sub->cat_type;

      // if(is_null(Category::where('slug',$slug)->first()) and is_null(SubCategory::where('slug',$slug)->first()) and is_null(SubCategoriesTwo::where('slug',$slug)->first()))
      //   {
      //     return '404';
      //   }

      // $cat_type = $type;
      $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration','advertisements.parent_category', 'advertisements.title', 'advertisements.category_id','advertisements.user_plan_id', 'advertisements.price','advertisements.feature','advertisements.ads_type_id', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.hierarchy_order', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     // ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');
                                                     
                                          })
                                          // ->where(function($query) use ($id){
                                          //      $query->where('advertisements.parent_category','=',$id)
                                          //          ->orWhere('advertisements.cat_unique_id','=',$id)
                                          //          ->orWhere('advertisements.category_id','=',$id)
                                          //          ->orWhere('advertisements.sub_category_id','=',$id);
                                          // })
                                          ->where(function($query) use ($country_code){
                                            if($country_code != 'ALL')
                                             {
                                              $query->where('advertisements.country_code','=',$country_code);      
                                             }
                                          })
                                         ->where('advertisements.ads_type_id',2)                                     
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.feature', 'desc')
                                         ->orderBy('advertisements.hierarchy_order', 'asc')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->take($per)
                                         ->get();

     $max_ads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     // ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');
                                                     
                                          })
                                          // ->where(function($query) use ($id){
                                          //      $query->where('advertisements.parent_category','=',$id)
                                          //          ->orWhere('advertisements.cat_unique_id','=',$id);
                                          // })
                                         ->where('advertisements.ads_type_id',2)                                                               
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->get();
      if(Request::ajax())
      {
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      }
      else
      {
          $result['rows'] = $rows;
          $result['cat_type'] = 2;
          $result['per'] = $per;
          $result['past_ad_type'] = 2;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return $result;
      }
  }

  public function subcatview($country_code,$city,$cat,$slug){
      $this->countrySet();
      $search = Request::input('search') == null ? '' : Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 9;
      $get_id_on_cat = SubCategory::where('slug',$slug)->first();
      $id = $get_id_on_cat->unique_id;
      $get_type_sub = Category::where('unique_id',$get_id_on_cat->cat_id)->first();
      $type = $get_type_sub->cat_type;

      if(is_null(Category::where('slug',$slug)->first()) and is_null(SubCategory::where('slug',$slug)->first()) and is_null(SubCategoriesTwo::where('slug',$slug)->first()))
        {
          return '404';
        }

      $cat_type = $type;
      $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration','advertisements.parent_category', 'advertisements.title', 'advertisements.category_id','advertisements.user_plan_id', 'advertisements.price','advertisements.feature','advertisements.ads_type_id', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.hierarchy_order', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query) use ($cat_type){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');
                                                     
                                          })
                                          ->where(function($query) use ($id){
                                               $query->where('advertisements.parent_category','=',$id)
                                                   ->orWhere('advertisements.cat_unique_id','=',$id)
                                                   ->orWhere('advertisements.category_id','=',$id)
                                                   ->orWhere('advertisements.sub_category_id','=',$id);
                                          })
                                          ->where(function($query) use ($country_code){
                                            if($country_code != 'ALL')
                                             {
                                              $query->where('advertisements.country_code','=',$country_code);      
                                             }
                                          })
                                                                               
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.feature', 'desc')
                                         ->orderBy('advertisements.hierarchy_order', 'asc')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->take($per)
                                         ->get();

     $max_ads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query) use ($cat_type){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                          })
                                          ->where(function($query) use ($id){
                                               $query->where('advertisements.parent_category','=',$id)
                                                   ->orWhere('advertisements.cat_unique_id','=',$id);
                                          })                                             
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->get();
      if(Request::ajax())
      {
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      }
      else
      {
          $result['rows'] = $rows;
          $result['cat_type'] = $cat_type;
          $result['per'] = $per;
          $result['past_category'] = $id;
          $result['past_ad_type'] = $cat_type;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return $result;
      }
  }

  public function doListCategoryContent($country_code,$slug){
    // dd(Request::all());
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 9;
      $get_id_on_cat = Category::where('slug',$slug)->first();
      if(is_null($get_id_on_cat)){
         $get_id_on_sub_cat = SubCategory::where('slug',$slug)->first();
          if(!is_null($get_id_on_sub_cat)){
              $id = $get_id_on_sub_cat->unique_id;
              $get_type_sub = Category::where('unique_id',$get_id_on_sub_cat->cat_id)->first();
              $type = $get_type_sub->cat_type;
          }
          $get_id_on_sub_cat_two = SubCategoriesTwo::where('slug',$slug)->first();
          if(!is_null($get_id_on_sub_cat_two)){
              $id = $get_id_on_sub_cat_two->unique_id;
              $get_type_tier_two = SubCategory::where('unique_id',$get_id_on_sub_cat_two->subcat_main_id)->first();
              $get_type_tier_two_final = Category::where('unique_id',$get_type_tier_two->cat_id)->first();
              $type = $get_type_tier_two_final->cat_type;
          }
      }else{
        $id = $get_id_on_cat->unique_id;
        $type = $get_id_on_cat->cat_type;
      }
      $cat_type = (isset($type) ? $type : '');
      if(is_null(Category::where('slug',$slug)->first()) and is_null(SubCategory::where('slug',$slug)->first()) and is_null(SubCategoriesTwo::where('slug',$slug)->first()))
        {
          if($slug == 'all-categories')
          {
            $id = null;
            if(Request::get('listing'))
            {
              $cat_type = 2;
              $type = 2;
            }
            else
            {
              $cat_type = 1;
              $type = 1;
            }
          }
          else
          {
            return '404';
          }
        }

        // dd($cat_type);
      
      // $check_sub_cat = SubCategory::where('unique_id',$id)->first();
      
      // if(!is_null($check_sub_cat)){
      //    $check_cat_type = Category::where('unique_id',$check_sub_cat->cat_id)->first(); 
      // }else{
      //    $check_cat_type = Category::where('unique_id',$id)->first(); 
      // }
      // if(!is_null($check_cat_type)){
      //    if($check_cat_type->cat_type == 1){
      //       $cat_type = 1;
      //   }else{
      //       $cat_type = 2;
      //   }
      // }
      $rows = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration','advertisements.parent_category', 'advertisements.title', 'advertisements.category_id','advertisements.user_plan_id', 'advertisements.price','advertisements.feature','advertisements.ads_type_id', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.hierarchy_order', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where('advertisements_photo.primary', '=', '1')
                                         ->where('advertisements.status', '=', '1')
                                         ->where(function($query) use ($cat_type){
                                               $query->where('advertisements.ads_type_id',$cat_type);
                                          })
                                          ->where(function($query) use ($id){
                                            if(!is_null($id))
                                            {
                                               $query->where('advertisements.parent_category','=',$id)
                                                   ->orWhere('advertisements.cat_unique_id','=',$id)
                                                   ->orWhere('advertisements.category_id','=',$id)
                                                   ->orWhere('advertisements.sub_category_id','=',$id);
                                            }
                                          })
                                          ->where(function($query) use ($country_code){
                                            if($country_code != 'ALL')
                                             {
                                              $query->where('advertisements.country_code','=',$country_code);      
                                             }
                                          })
                                         ->where(function($query) use ($type){
                                              if($type != ""){
                                                 $query->where('advertisements.ads_type_id','=',$type);
                                              }
                                          })                                     
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.feature', 'desc')
                                         ->orderBy('advertisements.hierarchy_order', 'asc')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->take($per)
                                         ->get();
                                         // dd($rows->lists('user_plan_id'));
                                         // dd($rows);

     $max_ads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                         ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                         ->selectRaw('avg(rate) as average_rate')
                                         ->where(function($query) use ($cat_type){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.ads_type_id',$cat_type)  
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                          })
                                          ->where(function($query) use ($id){
                                            if(!is_null($id))
                                            {
                                               $query->where('advertisements.parent_category','=',$id)
                                                   ->orWhere('advertisements.cat_unique_id','=',$id);
                                            }
                                          })
                                          ->where(function($query) use ($type){
                                              if($type != ""){
                                                 $query->where('advertisements.ads_type_id','=',$type);
                                              }
                                          })                                                          
                                         ->groupBy('advertisements.id')
                                         ->orderBy('advertisements.created_at', 'desc')
                                         ->get();
      // return response (format accordingly)
      if(Request::ajax()) {
          // $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          // $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['cat_type'] = $cat_type;
          $result['per'] = $per;
          $result['past_category'] = $id;
          $result['past_ad_type'] = $cat_type;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return $result;
      }

  }

  public function fetchBuyandSellList($country_code){
      $result = $this->doListBuyandSell($country_code);

        // dd($result);
        Session::forget('session_main_category');
        Session::forget('session_cat_id');
        $type = 1;
        if($type == 1){
            Session::put('sesssion_ad_type_id',1);
        }else{
            Session::put('sesssion_ad_type_id',2);
        }
        if(Request::get('listing') == 'auctions')
        {
          Session::put('listing_name', 2);
        }
        else
        {
          Session::put('listing_name', 1);
        }
        if(Session::get('session_cat_id') == "Y2F0LTI5"){ //jobs
           Session::put('range_indicator',1);
        }else if(Session::get('session_cat_id') == "Y2F0LTI4"){ // events
           Session::put('range_indicator',2);
        }else{ // ads
           Session::put('range_indicator',3);
        }
        $this->data['rows'] = $result['rows'];
        $this->data['per'] = $result['per'];
        $this->data['count'] = $result['count'];

        $this->data['cat_type'] = 1;
        $this->data['past_ad_type'] = 1;
        $this->data['past_category'] = '';
        $this->data['current_time'] = $result['current_time'];
        $this->data['category_id'] = "";
        // dd(Session::get('selected_location'));
        if($country_code)
        {
          $url_country = Country::where('countryCode', strtolower($country_code))->where('status', 1)->pluck('id');
          if($url_country)
          {
            $this->data['search_country'] = $url_country;
          }
          else
          {
            $this->data['search_country'] = '';
          }
        }
        else
        {
          $this->data['search_country'] = '';
        }

        if(Request::route('city'))
        {
          if(Request::route('city') != 'all-cities')
          {
            $city_id = City::where('slug', Request::route('city'))->where('status', 1)->pluck('id');
            if($city_id)
            {
              $this->data['search_city'] = $city_id;
            }
            
          }
        }

        // $parent_category = $result['rows']->parent_category;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
         $preloaded_custom_attributes = '';
         $get_parent_attributes = null;
          if(!is_null($get_parent_attributes)){
                     
             foreach ($get_parent_attributes as $key => $field) {
                 if($field->attribute_type == 1){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .= $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $preloaded_custom_attributes .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding">'; 
                      $preloaded_custom_attributes .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $preloaded_custom_attributes .= '</div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .=  $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '</select></div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
            }
          }else{
              $preloaded_custom_attributes .= '';
          }
          $category_name = '';

         $find_cat_name = null;
           if(is_null($find_cat_name)){
              $find_subcat_name = null;
                if(is_null($find_subcat_name)){
                      $find_subcat_two_name = null;
                       if(is_null($find_subcat_two_name)){
                            $find_subcat_three_name = null;
                            if(is_null($find_subcat_three_name)){
                               $category_name .= '';
                            }else{
                               $category_name .= $find_subcat_three_name->name;
                            }
                       }else{
                        $category_name .= $find_subcat_two_name->name;
                       }    
                }else{
                  $category_name .= $find_subcat_name->name;
                }
           }else{
              $category_name .= $find_cat_name->name;
           } 
        // if(!is_null($check_sub_cat)){
        //    $get_parent_cat_name = Category::where('unique_id',$check_sub_cat->cat_id)->first(); 
        // }else{
           $get_parent_cat_name = null;
        // }
        // $get_parent_cat_name = Category::where('unique_id','=',$id)->first(); 
         $this->data['category_name'] = $category_name. ' FILTERS';
         $this->data['preloaded_custom_attributes'] = $preloaded_custom_attributes;

        if(!is_null($get_parent_cat_name)){
           Session::flash('session_parent_cat_name',$get_parent_cat_name->name);
        }
       

        $this->data['preloaded_categories'] = Category::where(function($query){
                                                      $query->where('status','=',1)
                                                             ->where('biddable_status','=',1);
                                                      })->get();
        if(Session::get('default_location') != "ALL"){
             $this->data['categories'] = Category::where('status',1)->where('buy_and_sell_status',2)->where('country_code',Session::get('default_location'))->get();
        }else{
             $this->data['categories'] = Category::where('status',1)->where('buy_and_sell_status',2)->where('country_code','us')->get();
        }
     

        $this->data['usertypes'] = Usertypes::where('status',1)->get();
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();                                               
        $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
        $this->data['ad_type'] = AdvertisementType::orderBy('name','asc')->get();
        $this->data['category'] = Category::with('subcategoryinfo')->get();

        $this->data['sub_category'] = SubCategory::where('status',1)->get();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['top_reviews'] = AdRating::select('users.*','ad_ratings.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                               
        $this->data['category_all'] = '';
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

       $get_sub_attri_id = null;
                     $container_attrib = '';

        if(!is_null($get_sub_attri_id)){
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $container_attrib .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9">'; 
                      $container_attrib .= '<input type="text" name="text_field_'.$field->id.'[]" class="form-control borderZero">';
                      $container_attrib .='</div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $container_attrib .='<input type="hidden" name="text_field_id" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .='</select></div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
                      $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
        }
          
      if(!Auth::check()){
         $this->data['user_country'] =  1;
          $this->data['user_city'] = "all";
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" '.($this->data['user_city'] == "all" ? 'selected':'').'>All Cities</option>';
          foreach ($get_city as $key => $field) {
            $container_city .= '<option value="' . $field->id . '" '.($field->id == $this->data['user_city'] ? 'selected':'').'>' . $field->name . '</option>';
          }
      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   // ->where('advertisements.parent_category', '=', $id)
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      $this->data['container_city'] = $container_city;
      $this->data['container_attrib'] = $container_attrib;
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies); 
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
      // $this->data['cat_id'] = $id;

      $this->data['banner_placement_top_status'] = 0;
      $this->data['banner_placement_bottom_status'] = 0;
      $this->data['banner_placement_left_status'] = 0;


       // dd($this->data['banner_placement_left_status']);
      // dd($this->data['banner_placement_top'] );
      // // dd($this->data['banner_placement_bottom']);
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }        
                                                               })
                                                               // ->where('category','=',$id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }        
                                                               })
                                                              // ->where('category','=',$id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

         if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
      $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
      
      $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->orderBy('order_no','asc')
                                                                      ->first();


      $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();

      $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();                                            
      return View::make('category.list',$this->data);
  }
  public function fetchAuctionList($country_code){
      $result = $this->doListAuction($country_code);
            // dd($result);
        Session::forget('session_main_category');
        Session::forget('session_cat_id');

        $type = 2;
        if($type == 1){
            Session::put('sesssion_ad_type_id',1);
            Session::put('listing_name', 1);
        }else{
            Session::put('sesssion_ad_type_id',2);
            Session::put('listing_name', 2);
        }
    
        if(Session::get('session_cat_id') == "Y2F0LTI5"){ //jobs
           Session::put('range_indicator',1);
        }else if(Session::get('session_cat_id') == "Y2F0LTI4"){ // events
           Session::put('range_indicator',2);
        }else{ // ads
           Session::put('range_indicator',3);
        }
        $this->data['rows'] = $result['rows'];
        $this->data['per'] = $result['per'];
        $this->data['count'] = $result['count'];

        $this->data['cat_type'] = 2;
        $this->data['past_ad_type'] = 2;
        $this->data['past_category'] = '';
        $this->data['current_time'] = $result['current_time'];
        $this->data['category_id'] = "";
        // dd(Session::all());
        if($country_code)
        {
          $url_country = Country::where('countryCode', strtolower($country_code))->where('status', 1)->pluck('id');
          if($url_country)
          {
            $this->data['search_country'] = $url_country;
          }
          else
          {
            $this->data['search_country'] = '';
          }
        }
        else
        {
          $this->data['search_country'] = '';
        }

        if(Request::route('city'))
        {
          if(Request::route('city') != 'all-cities')
          {
            $city_id = City::where('slug', Request::route('city'))->where('status', 1)->pluck('id');
            if($city_id)
            {
              $this->data['search_city'] = $city_id;
            }
            
          }
        }

        // $parent_category = $result['rows']->parent_category;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
         $preloaded_custom_attributes = '';
         $get_parent_attributes = null;
          if(!is_null($get_parent_attributes)){
                     
             foreach ($get_parent_attributes as $key => $field) {
                 if($field->attribute_type == 1){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .= $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $preloaded_custom_attributes .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding">'; 
                      $preloaded_custom_attributes .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $preloaded_custom_attributes .= '</div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .=  $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '</select></div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
            }
          }else{
              $preloaded_custom_attributes .= '';
          }
          $category_name = '';

         $find_cat_name = null;
           if(is_null($find_cat_name)){
              $find_subcat_name = null;
                if(is_null($find_subcat_name)){
                      $find_subcat_two_name = null;
                       if(is_null($find_subcat_two_name)){
                            $find_subcat_three_name = null;
                            if(is_null($find_subcat_three_name)){
                               $category_name .= '';
                            }else{
                               $category_name .= $find_subcat_three_name->name;
                            }
                       }else{
                        $category_name .= $find_subcat_two_name->name;
                       }    
                }else{
                  $category_name .= $find_subcat_name->name;
                }
           }else{
              $category_name .= $find_cat_name->name;
           } 
        // if(!is_null($check_sub_cat)){
        //    $get_parent_cat_name = Category::where('unique_id',$check_sub_cat->cat_id)->first(); 
        // }else{
           $get_parent_cat_name = null;
        // }
        // $get_parent_cat_name = Category::where('unique_id','=',$id)->first(); 
         $this->data['category_name'] = $category_name. ' FILTERS';
         $this->data['preloaded_custom_attributes'] = $preloaded_custom_attributes;

        if(!is_null($get_parent_cat_name)){
           Session::flash('session_parent_cat_name',$get_parent_cat_name->name);
        }
       

        $this->data['preloaded_categories'] = Category::where(function($query){
                                                      $query->where('status','=',1)
                                                             ->where('biddable_status','=',1);
                                                      })->get();
        if(Session::get('default_location') != "ALL"){
          $this->data['categories'] = Category::where('status',1)->where('biddable_status',2)->where('country_code',Session::get('default_location'))->get();

        }else{
          $this->data['categories'] = Category::where('status',1)->where('biddable_status',2)->where('country_code','us')->get();

        }
    

        $this->data['usertypes'] = Usertypes::where('status',1)->get();
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();                                               
        $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
        $this->data['ad_type'] = AdvertisementType::orderBy('name','asc')->get();
        $this->data['category'] = Category::with('subcategoryinfo')->get();

        $this->data['sub_category'] = SubCategory::where('status',1)->get();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['top_reviews'] = AdRating::select('users.*','ad_ratings.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                               
        $this->data['category_all'] = '';
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

       $get_sub_attri_id = null;
                     $container_attrib = '';

        if(!is_null($get_sub_attri_id)){
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $container_attrib .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9">'; 
                      $container_attrib .= '<input type="text" name="text_field_'.$field->id.'[]" class="form-control borderZero">';
                      $container_attrib .='</div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $container_attrib .='<input type="hidden" name="text_field_id" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .='</select></div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
                      $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
        }
          
      if(!Auth::check()){
         $this->data['user_country'] =  1;
          $this->data['user_city'] = "all";
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" '.($this->data['user_city'] == "all" ? 'selected':'').'>All Cities</option>';
          foreach ($get_city as $key => $field) {
            $container_city .= '<option value="' . $field->id . '" '.($field->id == $this->data['user_city'] ? 'selected':'').'>' . $field->name . '</option>';
          }
      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   // ->where('advertisements.parent_category', '=', $id)
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      $this->data['container_city'] = $container_city;
      $this->data['container_attrib'] = $container_attrib;
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies); 
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
      // $this->data['cat_id'] = $id;

      $this->data['banner_placement_top_status'] = 0;
      $this->data['banner_placement_bottom_status'] = 0;
      $this->data['banner_placement_left_status'] = 0;


       // dd($this->data['banner_placement_left_status']);
      // dd($this->data['banner_placement_top'] );
      // // dd($this->data['banner_placement_bottom']);
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                               // ->where('category','=',$id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                              // ->where('category','=',$id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

       if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
         }       

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
      $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
      
      $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->orderBy('order_no','asc')
                                                                      ->first();


      $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();

      $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();                                            
      return View::make('category.list',$this->data);
  }
  public function routeCatch($country_code, $slug){
        $this->countrySet();
        if($country_code == 'admin' && $slug == 'login')
        {
          if(Auth::check())
          {
            $not_allowed = array(1,2);
            if(in_array(Auth::user()->usertype_id, $not_allowed))
            {
              return redirect()->to('error/access/denied');
            }
            else
            {
              return app('App\Http\Controllers\DashboardController')->index();
            }
          }
          else
          {
            return app('App\Http\Controllers\AdminController')->login();
          }
        }
        else if($country_code == 'post' && $slug == 'ads')
        {
          return app('App\Http\Controllers\FrontEndAdvertisementController')->postAds();
        }
        else if($country_code == 'post' && $slug == 'auctions')
        {
          return app('App\Http\Controllers\FrontEndAdvertisementController')->postAds();
        }
        else if($country_code == 'hotmail' && $slug == 'import')
        {
          return app('App\Http\Controllers\ImportContactController')->microsoft();
        }
        else if($country_code == 'yahoo' && $slug == 'import')
        {
          return app('App\Http\Controllers\ImportContactController')->yahoo();
        }
        else if($country_code == 'company' || $country_code == 'vendor')
        {
          $get_type_user = User::where('alias',$slug)->first();
          if($get_type_user)
          {
            if($get_type_user->usertype_id == 2 && $country_code == 'company')
            {
              return app('App\Http\Controllers\FrontEndUserController')->fetchInfoPublic($slug);
            }
            else if($get_type_user->usertype_id == 1 && $country_code == 'vendor')
            {
              return app('App\Http\Controllers\FrontEndUserController')->fetchInfoPublic($slug);
            }
            else
            {
              return abort(404, 'User not found');
            }
          }
          else
          {
            return abort(404, 'User not found');
          }
        }
        else if($slug == 'all-categories')
        {
          return $this->fetchList();
           dd("test list");  
        }
        else if($slug == 'q')
        {
          return app('App\Http\Controllers\SearchController')->browse();
           dd("test browse");  
        }
        else if($slug == 'buy-and-sell')
        {
          return $this->fetchBuyandSellList($country_code); 
          dd("test buy");  
        }
        else if($slug == "auctions")
        {
           dd("test buy");  
          return $this->fetchAuctionList($country_code);
        }

          if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry  = Session::get('selected_location');
          }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->where('category','=',$cat)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('category','=',$cat)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

       if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
          

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
      $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
      
      $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->orderBy('order_no','asc')
                                                                      ->first();


      $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();

      $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          ->first();

      return View::make('category.list',$this->data);
  }


  public function fetchCategoryList($country_code, $city, $slug){

        $this->countrySet();
        if(Request::route('q') != 'q' && !is_null(Request::route('q')))
        {
          return $this->fetchSubCategoryList(Request::route('country_code'),$city,Request::route('slug'),Request::route('q'));
        }
        // else if($slug == 'all-categories')
        // {
        //   return $this->fetchList();
        // }
        else if($slug == 'q')
        {
          return app('App\Http\Controllers\SearchController')->browse();
          dd("Test browse");
        }
        else if($slug == 'buy-and-sell')
        {
          return $this->fetchBuyandSellList($country_code);  
           dd("buy"); 
        }
        else if($slug == "auctions")
        {
          return $this->fetchAuctionList($country_code);
           dd("auction");
        }


        $result = $this->doListCategoryContent($country_code,$slug);

        if($result == '404'){
          return abort(404, 'Category not found.');
        }else{
          // $type = $result['cat_type'];
        }
        $get_id_on_cat = Category::where('slug',$slug)->where('status',1)->first();

          if(is_null($get_id_on_cat))
          {
              $get_id_on_sub_cat = SubCategory::where('slug',$slug)->where('status',1)->first();
              if(is_null($get_id_on_sub_cat))
              {
                $get_id_on_sub_cat_two = SubCategoriesTwo::where('slug',$slug)->where('status',1)->first();
                if(!is_null($get_id_on_sub_cat_two))
                {
                    $id = $get_id_on_sub_cat_two->unique_id;
                }
              }
              else
              {
                  $id = $get_id_on_sub_cat->unique_id;
              }
          }
          else
          {
            $id = $get_id_on_cat->unique_id;
          }
          // dd($get_id_on_sub_cat);
        if(isset($get_id_on_sub_cat))
        {
          $this->data['direct_cat_type'] = Category::where('unique_id',$get_id_on_sub_cat->cat_id)->where('status',1)->pluck('cat_type');
          $this->data['direct_first_cat'] = Category::where('unique_id',$get_id_on_sub_cat->cat_id)->where('status',1)->pluck('unique_id');
          $this->data['for_auctions'] = SubCategory::where('cat_id',$this->data['direct_first_cat'])->pluck('cat_id');
          $this->data['direct_second_cat'] = $get_id_on_sub_cat->unique_id;
        }
        // dd($this->data['direct_second_cat']);



        $city_slug = City::where('slug', $city)->pluck('id');
        if($slug == 'all-categories')
        {
          $id = 'all';
          if(Request::get('listing'))
          {
            $cat_type = 2;
            $type = 2;
          }
          else
          {
            $cat_type = 1;
            $type = 1;
          }
        }
        else
        {
          $cat_type = 1;
          $type = 1;
        }

        // $check_sub_cat = SubCategory::where('unique_id',$id)->first();
        // if(!is_null($check_sub_cat)){
        //    $check_cat_type = Category::where('unique_id',$check_sub_cat->cat_id)->first(); 
        // }else{
        //    $check_cat_type = Category::where('unique_id',$id)->first(); 
        // }
        Session::forget('session_main_category');
        Session::put('session_cat_id', $id);

        if($type == 1){
            Session::put('sesssion_ad_type_id',1);
        }else{
            Session::put('sesssion_ad_type_id',2);
        }

        if(Request::get('listing') == 'auctions')
        {
          Session::put('listing_name', 2);
        }
        else
        {
          Session::put('listing_name', 1);
        }

        if(Session::get('session_cat_id') == "Y2F0LTI5"){ //jobs
           Session::put('range_indicator',1);
        }else if(Session::get('session_cat_id') == "Y2F0LTI4"){ // events
           Session::put('range_indicator',2);
        }else{ // ads
           Session::put('range_indicator',3);
        }
        $this->data['rows'] = $result['rows'];
        $this->data['per'] = $result['per'];
        $this->data['count'] = $result['count'];

        $this->data['cat_type'] = $result['cat_type'];
        $this->data['past_ad_type'] = $result['past_ad_type'];
        $this->data['past_category'] = $result['past_category'];
        $this->data['current_time'] = $result['current_time'];
        $this->data['category_id'] = "";
        if($country_code)
        {
          $url_country = Country::where('countryCode', strtolower($country_code))->where('status', 1)->pluck('id');
          if($url_country)
          {
            $this->data['search_country'] = $url_country;
          }
          else
          {
            $this->data['search_country'] = '';
          }
        }
        else
        {
          $this->data['search_country'] = '';
        }

        if(Request::route('city'))
        {
          if(Request::route('city') != 'all-cities')
          {
            $city_id = City::where('slug', Request::route('city'))->where('status', 1)->pluck('id');
            // dd($city_id);
            if($city_id)
            {
              $this->data['search_city'] = $city_id;
            }
            
          }
        }
        // $parent_category = $result['rows']->parent_category;
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
         $preloaded_custom_attributes = '';
         $get_parent_attributes = SubAttributes::where('attri_id',$id)->get();
          if(!is_null($get_parent_attributes)){
                     
             foreach ($get_parent_attributes as $key => $field) {
                 if($field->attribute_type == 1){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .= $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $preloaded_custom_attributes .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding">'; 
                      $preloaded_custom_attributes .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $preloaded_custom_attributes .= '</div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .=  $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '</select></div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
            }
          }else{
              $preloaded_custom_attributes .= '';
          }
          $category_name = '';
           $find_cat_name = Category::where('unique_id',$id)->where('status',1)->first();
           if(is_null($find_cat_name)){
              $find_subcat_name = SubCategory::where('unique_id',$id)->where('status',1)->first();
                if(is_null($find_subcat_name)){
                      $find_subcat_two_name = SubCategoriesTwo::where('unique_id',$id)->where('status',1)->first();
                       if(is_null($find_subcat_two_name)){
                            $find_subcat_three_name = SubCategoriesThree::where('unique_id',$id)->where('status',1)->first();
                            if(is_null($find_subcat_three_name)){
                               $category_name .= '';
                            }else{
                               $category_name .= $find_subcat_three_name->name;
                            }
                       }else{
                        $category_name .= $find_subcat_two_name->name;
                       }    
                }else{
                  $category_name .= $find_subcat_name->name;
                }
           }else{
              $category_name .= $find_cat_name->name;
           } 
        // if(!is_null($check_sub_cat)){
        //    $get_parent_cat_name = Category::where('unique_id',$check_sub_cat->cat_id)->first(); 
        // }else{
           $get_parent_cat_name = Category::where('unique_id',$id)->first(); 
        // }
        // $get_parent_cat_name = Category::where('unique_id','=',$id)->first(); 
         $this->data['category_name'] = $category_name. ' FILTERS';
         $this->data['preloaded_custom_attributes'] = $preloaded_custom_attributes;

        if(!is_null($get_parent_cat_name)){
           Session::flash('session_parent_cat_name',$get_parent_cat_name->name);
        }
       

        $this->data['preloaded_categories'] = Category::where(function($query){
                                                      $query->where('status','=',1)
                                                             ->where('biddable_status','=',1);
                                                      })->get();
        if(Session::get('default_location') != "ALL"){
            if($slug == "buy-and-sell"){
              $this->data['categories'] = Category::where('status',1)->where('cat_type','=',$type)->where('buy_and_sell_status',2)->where('country_code',Session::get('default_location'))->get();
            }else{

              $this->data['categories'] = Category::where('status',1)
                                                  ->where('country_code', Session::get('default_location'))
                                                  ->where('cat_type','=',$type)
                                                  ->where(function($query){
                                                    if(Request::get('listing'))
                                                    {
                                                      $query->where('biddable_status',2);
                                                    }
                                                    else
                                                    {
                                                      $query->where('buy_and_sell_status',2);
                                                    }
                                                  })->get();
              // dd($this->data['categories']);
              // Category::where(function($query) use ($id) {
              //                     $query->where('status',1)
              //                           ->where('country_code',Session::get('default_location'))
              //                           ->where('biddable_status',2);            
              //                     })->get();
              // dd($type);
            }
        }else{
            if($slug == "auctions"){
               $this->data['categories'] = Category::where('status',1)->where('buy_and_sell_status',2)->where('cat_type','=',$type)->where('country_code','us')->get();
            }else{
               // $this->data['categories'] = Category::where('status',1)->where('biddable_status',2)->where('country_code','us')->get();
              $this->data['categories'] = Category::where('status',1)
                                                  ->where('country_code', Session::get('default_location'))
                                                  ->where('cat_type','=',$type)
                                                  ->where(function($query){
                                                    if(Request::get('listing'))
                                                    {
                                                      $query->where('biddable_status',2);
                                                    }
                                                    else
                                                    {
                                                      $query->where('buy_and_sell_status',2);
                                                    }
                                                  })->get();
            }
        }
     

        $this->data['usertypes'] = Usertypes::where('status',1)->get();
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();                                               
        $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
        $this->data['ad_type'] = AdvertisementType::orderBy('name','desc')->get();

        $this->data['category'] = Category::with('subcategoryinfo')->get();

        $this->data['sub_category'] = SubCategory::where('status',1)->where('cat_id',$id)->get();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['top_reviews'] = AdRating::select('users.*','ad_ratings.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                               
        $this->data['category_all'] = Category::with('subcategoryinfo')
                                    ->where('unique_id','=',$id)
                                    ->get();
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

       $get_sub_attri_id = SubAttributes::where('attri_id','=',$id)->get();
                     $container_attrib = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $container_attrib .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9">'; 
                      $container_attrib .= '<input type="text" name="text_field_'.$field->id.'[]" class="form-control borderZero">';
                      $container_attrib .='</div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $container_attrib .='<input type="hidden" name="text_field_id" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .='</select></div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
                      $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
      if(!Auth::check()){
         $this->data['user_country'] =  1;
          $this->data['user_city'] = "all";
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" '.($this->data['user_city'] == "all" ? 'selected':'').'>All Cities</option>';
          foreach ($get_city as $key => $field) {
            $container_city .= '<option value="' . $field->id . '" '.($field->id == $this->data['user_city'] ? 'selected':'').'>' . $field->name . '</option>';
          }

      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.parent_category', '=', $id)
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      $this->data['container_city'] = $container_city;
      $this->data['container_attrib'] = $container_attrib;
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies); 
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
      $this->data['cat_id'] = $id;
      

       if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->where('category','=',$id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('category','=',$id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      // $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
      //     if(count($checKBannerPerCountry) > 0){
      //         BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
      //     }

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
      $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->orderBy('order_no','asc')
                                                                    // ->orderByRaw('RAND()')
                                                                    ->first();      
      $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();      
                                                                 


      $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();      

      $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                          ->first();      

      // $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
      //                                                         ->where('banner_status','=',4)
      //                                                         ->where(function($query) use ($selectedCountry,$getCountryCode){
      //                                                                   if($selectedCountry != "ALL"){
      //                                                                      $query->where('country_id','=',$getCountryCode->id);
      //                                                                   }else{
      //                                                                      $query->where('country_id','=', 150);
      //                                                                   }       
      //                                                          })
      //                                                         ->where('status',1)
      //                                                         ->lists('placement_id','id')
      //                                                         ->toArray();
      // dd($browseBanners);
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }

      // $get_banner_placement_top= BannerAdvertisementSubscriptions::where('page',2)
      //                                                             ->where('banner_status','=',4)
      //                                                             ->where('category',$id)
      //                                                             ->where('country_id',Session::get('default_location'))
      //                                                             ->get();

      //  if (count($get_banner_placement_top) > 0) {
      //    $this->data['banner_placement_top_status'] = 1;
      //    $this->data['banner_placement_top'] = $get_banner_placement_top->random(1);
      //   }else{
      //    $this->data['banner_placement_top_status'] = 0;
      //  }

      // $get_banner_placement_bottom = BannerAdvertisementSubscriptions::where('page',2)
      //                                                                 ->where('banner_status','=',4)
      //                                                                 ->where('category',$id)
      //                                                                 ->where('country_id',Session::get('default_location'))
      //                                                                 ->get();

      //  if (count($get_banner_placement_bottom) > 0) {
      //    $this->data['banner_placement_bottom_status'] = 1;
      //    $this->data['banner_placement_bottom'] = $get_banner_placement_bottom->random(1);
      //   }else{
      //    $this->data['banner_placement_bottom_status'] = 0;
      //  }

      // $get_banner_placement_left = BannerAdvertisementSubscriptions::where('page',2)
      //                                                              ->where('banner_status','=',4)
      //                                                              ->where('category',$id)
      //                                                              ->where('country_id',Session::get('default_location'))
      //                                                              ->get();
      //  if (count($get_banner_placement_left) > 0) {
      //    $this->data['banner_placement_left_status'] = 1;
      //    $this->data['banner_placement_left'] = $get_banner_placement_left->random(1);
      //   }else{
      //    $this->data['banner_placement_left_status'] = 0;
      //  }

      // dd($get_banner_placement_top);                                                          
      // dd($get_banner_placement_bottom);
      // dd($this->data['banner_placement_top'] );
    
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      
      // YAH IT WORKS TILL THE END!!!!
      return View::make('category.list',$this->data);
  }


  public function fetchSubCategoryList($country_code,$city,$cat,$slug){

        $this->countrySet();
        $result = $this->subcatview($country_code,$city,$cat,$slug);
        if($result == '404')
        {
          return abort(404,'Category not found');
        }
        else
        {
          $type = $result['cat_type'];
          $get_id_on_sub_cat = SubCategory::where('slug',$slug)->where(function($query) use ($country_code) {
            if($country_code == 'all')
            {
              $query->where('country_code', 'us');
            }
            else
            {
              $query->where('country_code', $country_code);
            }
          })->where('status',1)->first();
                    
          if($get_id_on_sub_cat)
          {
            $id = $get_id_on_sub_cat->unique_id;
          }
          else
          {
            return abort(404, 'Subcategory not found.');
          }
          
        }


        if(isset($get_id_on_sub_cat))
        {
          $this->data['direct_cat_type'] = Category::where('unique_id',$get_id_on_sub_cat->cat_id)->where('status',1)->pluck('cat_type');
          $this->data['direct_first_cat'] = Category::where('unique_id',$get_id_on_sub_cat->cat_id)->where('status',1)->pluck('unique_id');
          $this->data['for_auctions'] = SubCategory::where('cat_id',$this->data['direct_first_cat'])->pluck('cat_id');
          $this->data['direct_second_cat'] = $get_id_on_sub_cat->unique_id;
        }

    
        Session::forget('session_main_category');
        Session::put('session_cat_id',$id);

        if($type == 1){
            Session::put('sesssion_ad_type_id',1);
        }else{
            Session::put('sesssion_ad_type_id',2);
        }

        if(Request::get('listing') == 'auctions')
        {
          Session::put('listing_name', 2);
        }
        else
        {
          Session::put('listing_name', 1);
        }

        if(Session::get('session_cat_id') == "Y2F0LTI5"){ //jobs
           Session::put('range_indicator',1);
        }else if(Session::get('session_cat_id') == "Y2F0LTI4"){ // events
           Session::put('range_indicator',2);
        }else{ // ads
           Session::put('range_indicator',3);
        }
        $this->data['rows'] = $result['rows'];
        $this->data['per'] = $result['per'];
        $this->data['count'] = $result['count'];

        $this->data['cat_type'] = $result['cat_type'];
        $this->data['past_ad_type'] = $result['past_ad_type'];
        $this->data['past_category'] = $result['past_category'];
        $this->data['current_time'] = $result['current_time'];
        $this->data['category_id'] = "";
        if($country_code)
        {
          $url_country = Country::where('countryCode', strtolower($country_code))->where('status', 1)->pluck('id');
          if($url_country)
          {
            $this->data['search_country'] = $url_country;
          }
          else
          {
            $this->data['search_country'] = '';
          }
        }
        else
        {
          $this->data['search_country'] = '';
        }

        if(Request::route('city'))
        {
          if(Request::route('city') != 'all-cities')
          {
            $city_id = City::where('slug', Request::route('city'))->where('status', 1)->pluck('id');
            if($city_id)
            {
              $this->data['search_city'] = $city_id;
            }
            
          }
        }

        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
         $preloaded_custom_attributes = '';
         $get_parent_attributes = SubAttributes::where('attri_id',$id)->get();
          if(!is_null($get_parent_attributes)){
                     
             foreach ($get_parent_attributes as $key => $field) {
                 if($field->attribute_type == 1){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .= $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $preloaded_custom_attributes .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding">'; 
                      $preloaded_custom_attributes .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $preloaded_custom_attributes .= '</div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $preloaded_custom_attributes .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $preloaded_custom_attributes .=  $field->name.'</h5>';
                      $preloaded_custom_attributes .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $preloaded_custom_attributes .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $preloaded_custom_attributes .= '</select></div>';
                      $preloaded_custom_attributes .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $preloaded_custom_attributes .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
            }
          }else{
              $preloaded_custom_attributes .= '';
          }
          $category_name = '';
           $find_cat_name = Category::where('unique_id',$id)->where('status',1)->first();
           if(is_null($find_cat_name)){
              $find_subcat_name = SubCategory::where('unique_id',$id)->where('status',1)->first();
                if(is_null($find_subcat_name)){
                      $find_subcat_two_name = SubCategoriesTwo::where('unique_id',$id)->where('status',1)->first();
                       if(is_null($find_subcat_two_name)){
                            $find_subcat_three_name = SubCategoriesThree::where('unique_id',$id)->where('status',1)->first();
                            if(is_null($find_subcat_three_name)){
                               $category_name .= '';
                            }else{
                               $category_name .= $find_subcat_three_name->name;
                            }
                       }else{
                        $category_name .= $find_subcat_two_name->name;
                       }    
                }else{
                  $category_name .= $find_subcat_name->name;
                }
           }else{
              $category_name .= $find_cat_name->name;
           } 
           $get_parent_cat_name = Category::where('unique_id',$id)->first(); 
         $this->data['category_name'] = $category_name. ' FILTERS';
         $this->data['preloaded_custom_attributes'] = $preloaded_custom_attributes;

        if(!is_null($get_parent_cat_name)){
           Session::flash('session_parent_cat_name',$get_parent_cat_name->name);
        }
       

        $this->data['preloaded_categories'] = Category::where(function($query){
                                                      $query->where('status','=',1)
                                                             ->where('biddable_status','=',1);
                                                      })->get();

        $this->data['categories'] = Category::where('status',1)->where('cat_type','=',$type)->get();

        $this->data['usertypes'] = Usertypes::where('status',1)->get();
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();                                               
        $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
        $this->data['ad_type'] = AdvertisementType::all();
        $this->data['category'] = Category::with('subcategoryinfo')->get();

        $this->data['sub_category'] = SubCategory::where('status',1)->where('cat_id',$id)->get();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        $this->data['top_reviews'] = AdRating::select('users.*','ad_ratings.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();                               
        $this->data['category_all'] = Category::with('subcategoryinfo')
                                    ->where('unique_id','=',$id)
                                    ->get();
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

       $get_sub_attri_id = SubAttributes::where('attri_id','=',$id)->get();
                     $container_attrib = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $container_attrib .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9">'; 
                      $container_attrib .= '<input type="text" name="text_field_'.$field->id.'[]" class="form-control borderZero">';
                      $container_attrib .='</div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $container_attrib .='<input type="hidden" name="text_field_id" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .='</select></div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
                      $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
      if(!Auth::check()){
         $this->data['user_country'] =  1;
          $this->data['user_city'] = "all";
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" '.($this->data['user_city'] == "all" ? 'selected':'').'>All Cities</option>';
          foreach ($get_city as $key => $field) {
            $container_city .= '<option data-slug="'.$field->slug.'" value="' . $field->id . '" '.($field->id == $this->data['user_city'] ? 'selected':'').'>' . $field->name . '</option>';
          }
      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.parent_category', '=', $id)
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      $this->data['container_city'] = $container_city;
      $this->data['container_attrib'] = $container_attrib;
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies); 
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
      $this->data['cat_id'] = $id;
      
          if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry  = Session::get('selected_location');
          }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
//       $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
//                                                               ->where('banner_status','=',4)
//                                                               ->where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                })
//                                                                ->where('category','=',$cat)
//                                                               ->where('status',1)
//                                                               ->lists('placement_id','id')
//                                                               ->toArray();

//      $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
//                                                               ->where('banner_status','=',4)
//                                                               ->where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                })
//                                                               ->where('category','=',$cat)
//                                                               ->where('placement_id','!=','null')
//                                                               ->where('status',1)
//                                                               ->lists('id')
//                                                               ->toArray();


//      // dd($browseBannersIds);
//      $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                })
//                                                                ->groupBy('placement_id')
//                                                                ->where('page_location',2)
//                                                                ->where('status',1)
//                                                                ->whereNotIn('placement_id',array_filter($browseBanners))
//                                                                ->lists('id')
//                                                                ->toArray();

 

    
//       $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

//          if($selectedCountry != "ALL"){
//                  $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
//                     if(count($checKBannerPerCountry) > 0){
//                         BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
//                     }
//               }else{
//                  $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                  $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
//                     if(count($checKBannerPerCountry) > 0){
//                         BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
//                     }
//             }       
          

//       if(!is_null($browseBannersIds)){

//               foreach ($browseBannersIds as $key => $field) {
//                $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
//                $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
//                 $saveBanners = new BannerAdvertisementMerge;       
//                 $saveBanners->banner_id = $field;
//                 $saveBanners->country_id = $getCountryCode->id;
//                 $saveBanners->image_desktop = $getBannerDetails->image_desktop;
//                 $saveBanners->image_mobile = $getBannerDetails->image_mobile;
//                 $saveBanners->position = $getBannerPosition->position;
//                 $saveBanners->order_no = $getBannerPosition->order_no;
//                 $saveBanners->url = $getBannerPosition->target_url;
//                 $saveBanners->designation = 2;
//                 $saveBanners->type = 1;
//                 $saveBanners->save();
//               }
             
//           }
          
//        if(!is_null($getBannerDefaultsPerCountry))
//           {
//               foreach ($getBannerDefaultsPerCountry as $key => $field) {
//                $getBannerDetails = BannerDefault::find($field);
//                $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
//                 $saveBanners = new BannerAdvertisementMerge;       
//                 $saveBanners->banner_id = $field;
//                 if($selectedCountry != "ALL"){
//                    $saveBanners->country_id = $getCountryCode->id;
//                 }else{
//                    $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                    $saveBanners->country_id = $getDefaultCountry->id;
//                 }
//                 $saveBanners->image_desktop = $getBannerDetails->image_desktop;
//                 $saveBanners->image_mobile = $getBannerDetails->image_mobile;
//                 $saveBanners->position = $getBannerPosition->position;
//                 $saveBanners->order_no = $getBannerPosition->order_no;
//                 $saveBanners->url = $getBannerPosition->target_url;
//                 $saveBanners->designation = 2;
//                 $saveBanners->type = 2;
//                 $saveBanners->save();
//               }
//           }
       
//       // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

//       // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

//       // }
//       $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                       })  
//                                                                     ->where('position',1)
//                                                                     ->where('designation',2)
//                                                                     ->orderBy('order_no','asc')
//                                                                     ->first();
      
//       $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                       })  
//                                                                       ->where('position',2)
//                                                                       ->where('designation',2)
//                                                                       ->orderBy('order_no','asc')
//                                                                       ->first();


//       $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                       })  
//                                                                           ->where('position',3)
//                                                                           ->where('designation',2)
//                                                                           ->orderBy('order_no','asc')
//                                                                           ->first();

//       $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
//                                                                         if($selectedCountry != "ALL"){
//                                                                            $query->where('country_id','=',$getCountryCode->id);
//                                                                         }else{
//                                                                            $getDefaultCountry = Country::where('countryCode','US')->first(); 
//                                                                            $query->where('country_id','=',$getDefaultCountry->id);
//                                                                         }       
//                                                                       })  
//                                                                           ->where('position',4)
//                                                                           ->where('designation',2)
//                                                                           ->orderBy('order_no','asc')
//                                                                           ->first();
    
    
    $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$cat)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();



     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$cat)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();

     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                if (!$if_exist) {
                  $saveBanners = new BannerAdvertisementMerge;       
                  $saveBanners->banner_id = $field;
                  $saveBanners->country_id = $getCountryCode->id;
                  $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                  $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                  $saveBanners->position = $getBannerPosition->position;
                  $saveBanners->order_no = $getBannerPosition->order_no;
                  $saveBanners->url = $getBannerDetails->target_url;
                  $saveBanners->category_id = $getBannerDetails->category;
                  $saveBanners->designation = 2;
                  $saveBanners->type = 1;
                  $saveBanners->save();
                }
                
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                  if (!$if_exist) {

                    $saveBanners = new BannerAdvertisementMerge;       
                    $saveBanners->banner_id = $field;
                    if($selectedCountry != "ALL"){
                       $saveBanners->country_id = $getCountryCode->id;
                    }else{
                       $getDefaultCountry = Country::where('countryCode','US')->first(); 
                       $saveBanners->country_id = $getDefaultCountry->id;
                    }
                    $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                    $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                    $saveBanners->position = $getBannerPosition->position;
                    $saveBanners->order_no = $getBannerPosition->order_no;
                    $saveBanners->url = $getBannerDetails->target_url;
                    $saveBanners->designation = 2;
                    $saveBanners->type = 2;
                    $saveBanners->save();
                }
              }
                
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
//     header('HTTP/1.1 500 Internal Server Error');

      $banner_top_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 1)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $this->data['banner_placement_top'] = ($banner_top_purchased ?: $banner_top_default);
    
                                                                   
             

                                                                   
      $banner_bottom_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 1)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
    
      $banner_bottom_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
      $this->data['banner_placement_bottom'] = ($banner_bottom_purchased ?: $banner_bottom_default);
                                                                 

      $banner_left_A_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $this->data['banner_placement_left_one'] = ($banner_left_A_purchased ?: $banner_left_A_default);

     $banner_left_B_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
      $banner_left_B_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $this->data['banner_placement_left_two'] = ($banner_left_B_purchased ?: $banner_left_B_default);


      // dd($this->data['banner_placement_top']);
      // $get_banner_placement_top= BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',3)->get();

      //  if (count($get_banner_placement_top) > 0) {
      //    $this->data['banner_placement_top_status'] = 1;
      //    $this->data['banner_placement_top'] = $get_banner_placement_top->random(1);
      //   }else{
      //    $this->data['banner_placement_top_status'] = 0;
      //  }

      // $get_banner_placement_bottom = BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',4)->get();
      //  if (count($get_banner_placement_bottom) > 0) {
      //    $this->data['banner_placement_bottom_status'] = 1;
      //    $this->data['banner_placement_bottom'] = $get_banner_placement_bottom->random(1);
      //   }else{
      //    $this->data['banner_placement_bottom_status'] = 0;
      //  }

      // $get_banner_placement_left = BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',1)->get();
      //  if (count($get_banner_placement_left) > 0) {
      //    $this->data['banner_placement_left_status'] = 1;
      //    $this->data['banner_placement_left'] = $get_banner_placement_left->random(1);
      //   }else{
      //    $this->data['banner_placement_left_status'] = 0;
      //  }

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      return View::make('category.list',$this->data);
  }



  public function doCustomSearch(){
    // dd(Request::all());
      $input  = Input::all();
      Session::forget('session_main_category_name');
      Session::forget('session_category_name');
      Session::forget('session_sub_category_name');
      Session::forget('session_parent_cat_name');
      Session::forget('session_keyword');
      $main_cat_id = "all";

        if(Request::input('main_category') != "all" && Request::input('category') == "all" && Request::input('subcategory') == "all"  ){

          $main_cat_id = array_get($input,'main_category');

        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') == "all" ) {
          
          $main_cat_id = array_get($input,'category');
        
        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  ){
          
          $main_cat_id = array_get($input,'subcategory');

        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"){
          
          $main_cat_id = array_get($input,'subSubCategory');

        }
        else if(Request::input('main_category') == "all" && Request::input('category') == "all" && Request::input('subcategory') =="all"){

          $main_cat_id = "all";
        }

        $country = Request::input('country');
        $city = Request::input('city');
        $ad_type = Request::input('ad_type');
        $per = Request::input('per') ? :9;
        $keyword = Request::input('keyword');
        $order = Request::input('order') ? : 'desc';
        $sort = Request::input('sort') ? : 'created_at';
        $dropdown_attri_id  = Request::input('dropdown_attri_id');
        $custom_array = Request::input('custom_array') ? : null;
        $attri_type = Request::input('attri_type');
        $price_from = Request::input('price_from');
        $price_to = Request::input('price_to');
        $salary_from = Request::input('salary_from');
        $salary_to = Request::input('salary_to');


        if(Request::has('date_from')){
           $date_from =  Carbon::createFromFormat('m/d/Y', Request::input('date_from'))->format('Y-m-d');
        }else{
           $date_from = "";
        }
        if(Request::has('date_to')){
           $date_to = Carbon::createFromFormat('m/d/Y', Request::input('date_to'))->format('Y-m-d');
        }else{
           $date_to = "";
        }
      if(Request::input('main_category')){
         $find_main_category = Category::where('unique_id','=',Request::input('main_category'))->first();
         if(!is_null($find_main_category)){
            Session::flash('session_main_category_name',$find_main_category->name);   
         }
      }
      if(Request::input('category')|| Request::input('category') != "" || Request::input('category') != "all"){
         $find_category = SubCategory::where('unique_id','=',Request::input('category'))->first();
         if(!is_null($find_category)){
           Session::flash('session_category_name', $find_category->name);
         }
      }
     if(Request::input('subcategory') || Request::input('subcategory') != "" || Request::input('subcategory') != "all" ){
         $find_subcategory = SubCategoriesTwo::where('unique_id','=',Request::input('subcategory'))->first();
         if(!is_null($find_subcategory)){
            Session::flash('session_sub_category_name', $find_subcategory->name);
         }
      }
        $history = "";
      if(Session::has('session_main_category_name')){
         $history = Session::get('session_main_category_name');
      }
      if(Session::has('session_main_category_name') && Session::has('session_category_name')){
         $history = Session::get('session_main_category_name').' > '.Session::get('session_category_name');
      }
      if(Session::has('session_main_category_name') && Session::has('session_category_name') && Session::has('session_sub_category_name')){
         $history = Session::get('session_main_category_name').' > '.Session::get('session_category_name').' > '.Session::get('session_sub_category_name');
      }
      if($history == ''){
         if($ad_type == '1'){
          $history = 'Buy and Sell';
        }else if($ad_type == 2){
          $history = 'Live Auctions';
        }else if($ad_type == 4){
          $history = 'All Categories';
        }
      }
    


        if(Request::input('update_status') != 2){
          Session::flash('session_category', array_get($input,'category'));
          Session::flash('session_subcategory', array_get($input,'subcategory'));
          Session::flash('session_main_category', array_get($input,'main_category'));
          Session::flash('session_keyword', $keyword);
          Session::flash('session_ad_type', $ad_type);
          Session::flash('session_country', $country);
          Session::flash('session_city', $city);
          Session::flash('session_price_from', $price_from);
          Session::flash('session_price_to', $price_to);
          Session::flash('session_main_cat_id', $main_cat_id);
      }
      $get_country = Country::find($country);
      // $dropdown_field  = Input::get('dropdown_field_27');
      if(Auth::check()){
          if(Input::has('keyword')){
             $search_history = new UserSearchHistory;
             $search_history->user_id = Auth::user()->id;
             $search_history->email = Auth::user()->email;
             $search_history->search_tool = "Browse Page";
             if($get_country)
             {
              $search_history->country = $get_country->countryCode;
             }
             $search_history->category = $history;
             $search_history->keyword = array_get($input,'keyword');
             $search_history->save();
          } 
      }
      if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
      }
      $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first();
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$main_cat_id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();



     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$main_cat_id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();
//     header('HTTP/1.1 500 Internal Server Error');
//     dd($main_cat_id);

     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                if (!$if_exist) {
                  $saveBanners = new BannerAdvertisementMerge;       
                  $saveBanners->banner_id = $field;
                  $saveBanners->country_id = $getCountryCode->id;
                  $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                  $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                  $saveBanners->position = $getBannerPosition->position;
                  $saveBanners->order_no = $getBannerPosition->order_no;
                  $saveBanners->url = $getBannerDetails->target_url;
                  $saveBanners->category_id = $getBannerDetails->category;
                  $saveBanners->designation = 2;
                  $saveBanners->type = 1;
                  $saveBanners->save();
                }
                
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                  if (!$if_exist) {

                    $saveBanners = new BannerAdvertisementMerge;       
                    $saveBanners->banner_id = $field;
                    if($selectedCountry != "ALL"){
                       $saveBanners->country_id = $getCountryCode->id;
                    }else{
                       $getDefaultCountry = Country::where('countryCode','US')->first(); 
                       $saveBanners->country_id = $getDefaultCountry->id;
                    }
                    $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                    $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                    $saveBanners->position = $getBannerPosition->position;
                    $saveBanners->order_no = $getBannerPosition->order_no;
                    $saveBanners->url = $getBannerDetails->target_url;
                    $saveBanners->designation = 2;
                    $saveBanners->type = 2;
                    $saveBanners->save();
                }
              }
                
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
//     header('HTTP/1.1 500 Internal Server Error');

      $banner_top_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 1)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top = ($banner_top_purchased ?: $banner_top_default);
    
                                                                   
             

                                                                   
      $banner_bottom_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 1)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
    
      $banner_bottom_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
      $banner_bottom = ($banner_bottom_purchased ?: $banner_bottom_default);
                                                                 

      $banner_left_A_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A = ($banner_left_A_purchased ?: $banner_left_A_default);

     $banner_left_B_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
      $banner_left_B_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_B = ($banner_left_B_purchased ?: $banner_left_B_default);
    
    
      // $get_searched_ids = [];
      $ctr = 0;
      $get_all = [];
      if ($attri_type) {
      foreach ($attri_type as $key => $atvalue) {

        $at_attri_id  = Request::input($atvalue . '_attri_id');

        if ($at_attri_id) {
        foreach ($at_attri_id as $key => $attri_id) {

          $at_field  = Request::input($atvalue .'_field_' . $attri_id);
          if (!$at_field) {
            $at_field = [];
          }

          foreach (array_filter($at_field) as $key => $attri_value) {
            if ($ctr > 0 ) {
                $get_searched_id = DB::table('ad_custom_attribute_'. $atvalue)->where(function($query) use ($attri_id,$atvalue,$attri_value){
                                              if (strlen($attri_id) > 0 && strlen($attri_value) > 0) {
                                                if ($atvalue == "textbox") {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value', 'LIKE', '%' . $attri_value . '%');
                                                } else {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value_id', 'LIKE', '%' . $attri_value . '%');
                                                }
                                              }
                                             })
                                          ->where('status',1)
                                          ->whereIn('ad_id', $get_searched_id)                                         
                                          ->groupBy('ad_id')
                                          ->lists('ad_id');

            } else {
                $get_searched_id = DB::table('ad_custom_attribute_'. $atvalue)->where(function($query) use ($attri_id,$atvalue,$attri_value){
                                              if (strlen($attri_id) > 0 && strlen($attri_value) > 0) {
                                                if ($atvalue == "textbox") {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value', 'LIKE', '%' . $attri_value . '%');
                                                } else {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value_id', 'LIKE', '%' . $attri_value . '%');
                                                }
                                              }
                                             })
                                          ->where('status',1)                                       
                                          ->groupBy('ad_id')
                                          ->lists('ad_id');
              // dd($atvalue);
            }
            $ctr++;

          }

        }
      }

        }

      }
 if($ctr > 0 && Request::input('update_status') ==1){
          $rows = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.ads_type_id' ,'advertisements.feature','advertisements.created_at', 'advertisements.description','advertisements.feature', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate', 'advertisements.hierarchy_order')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                     if(Request::input('update_status') == 1){
                                          if(Request::input('price_from') || Request::input('price_from') != 0){
                                              $query->where('advertisements.price','>=',$price_from);
                                          }
                                          if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                              $query->where('advertisements.price','<=',$price_to);
                                          }
                                          if(Request::input('country') && Request::input('country') != "all"){
                                              $query->where('advertisements.country_id', '=', $country);
                                          }
                                          if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                          if(Request::input('city') && Request::input('city') != "all"){
                                              $query->where('advertisements.city_id', '=', $city);       
                                          }  
                                          if(Request::input('keyword') && Request::input('keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                          }
                                           if(Request::input('salary_from') ||  Request::input('salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',$salary_from) ;
                                          }
                                          if(Request::input('salary_to') ||  Request::input('salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',$salary_to) ;
                                          }
                                          if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                              $query->where('advertisements.event_date','>=',$date_from);
                                          }
                                          if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$date_to);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                          if(Request::input('previous_price_from') || Request::input('previous_price_from') != 0){
                                              $query->where('advertisements.price','>=',Request::input('previous_price_from'));
                                          }
                                          if(Request::input('previous_price_to') ||  Request::input('previous_price_to') != 0){
                                              $query->where('advertisements.price','<=',Request::input('previous_price_to'));
                                          }
                                          if(Request::input('previous_country') && Request::input('previous_country') != "all"){
                                              $query->where('advertisements.country_id', '=', Request::input('previous_country'));
                                          }
                                          if(Request::input('previous_ad_type') && Request::input('previous_ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', Request::input('previous_ad_type'));
                                          }
                                          if(Request::input('previous_city') && Request::input('previous_city') != "all"){
                                              $query->where('advertisements.city_id', '=', Request::input('previous_city'));       
                                          }  
                                          if(Request::input('previous_keyword') && Request::input('previous_keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.Request::input('previous_keyword').'%');  
                                          }
                                           if(Request::input('previous_salary_from') ||  Request::input('previous_salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',Request::input('previous_salary_from')) ;
                                          }
                                          if(Request::input('previous_salary_to') ||  Request::input('previous_salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',Request::input('previous_salary_to')) ;
                                          }
                                          if(Request::input('previous_date_from') ||  Request::input('previous_date_from') != ""){
                                              $query->where('advertisements.event_date','>=',Request::input('previous_date_from'));
                                          }
                                          if(Request::input('previous_date_to') ||  Request::input('previous_date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$Request::input('previous_date_to'));
                                          }
                                      }else if(Request::input('update_status') == 0){
                                           $query->where('advertisements.ads_type_id', '=',Request::input('past_ad_type'));
                                      } 
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                      if(Request::input('update_status') == 1){
                                        if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                            $query->where('advertisements.parent_category','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('previous_main_cat_id'));
                                      }else{
                                           $query->where('advertisements.parent_category','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('past_category'));
                                      } 
                                   })                                                    
                                  ->whereIn('advertisements.id',$get_searched_id)
                                  ->orderBy('advertisements.'.$sort, $order)
                                  // ->orderBy('advertisements.feature', 'desc')
                                  ->orderBy('advertisements.hierarchy_order', 'asc')
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->groupBy('advertisements.id')
                                  // ->take($per)
                                  ->get();


        $max_ads = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.ads_type_id','advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                    if(Request::input('update_status') == 0){
                                        if(Request::input('price_from') || Request::input('price_from') != 0){
                                            $query->where('advertisements.price','>=',$price_from);
                                        }
                                        if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                            $query->where('advertisements.price','<=',$price_to);
                                        }
                                        if(Request::input('country') && Request::input('country') != "all"){
                                            $query->where('advertisements.country_id', '=', $country);
                                        }
                                        if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                            $query->where('advertisements.ads_type_id', '=', $ad_type);
                                        }
                                        if(Request::input('city') && Request::input('city') != "all"){
                                            $query->where('advertisements.city_id', '=', $city);       
                                        }  
                                        if(Request::input('keyword') && Request::input('keyword') != "" ){
                                            $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                        }
                                         if(Request::input('salary_from') ||  Request::input('salary_from') != 0 ){
                                            $query->where('advertisements.salary','>=',$salary_from) ;
                                        }
                                        if(Request::input('salary_to') ||  Request::input('salary_to') != 0 ){
                                            $query->where('advertisements.salary','<=',$salary_to) ;
                                        }
                                        if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                            $query->where('advertisements.event_date','>=',$date_from);
                                        }
                                        if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                            $query->where('advertisements.event_date','<=',$date_to);
                                        }
                                      }else{
                                           $query->where('advertisements.ads_type_id', '=',Request::input('past_ad_type'));
                                      } 
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                      if(Request::input('update_status') == 1){
                                        if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }
                                      }else{
                                           $query->where('advertisements.parent_category','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('past_category'));
                                      }                                                                       
                                   })                                              
                                  ->whereIn('advertisements.id',$get_searched_id)
                                  ->orderBy('advertisements.'.$sort, $order)
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->groupBy('advertisements.id')
                                  ->get();                        
                                 
      }else{
          $rows = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('user_ad_comments','user_ad_comments.ad_id', '=', 'advertisements.id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency','advertisements.ads_type_id','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.feature','advertisements.created_at', 'advertisements.description', 'advertisements.feature','advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate','user_ad_comments.id as comment_id', 'advertisements.hierarchy_order')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                   })
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                      if(Request::input('update_status') == 1){
                                          if(Request::input('price_from') || Request::input('price_from') != 0){
                                              $query->where('advertisements.price','>=',$price_from);
                                          }
                                          if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                              $query->where('advertisements.price','<=',$price_to);
                                          }
                                          if(Request::input('country') && Request::input('country') != "all"){
                                              $query->where('advertisements.country_id', '=', $country);
                                          }
                                          if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                          if(Request::input('city') && Request::input('city') != "all"){
                                              $query->where('advertisements.city_id', '=', $city);       
                                          }  
                                          if(Request::input('keyword') && Request::input('keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                          }
                                           if(Request::input('salary_from') ||  Request::input('salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',$salary_from) ;
                                          }
                                          if(Request::input('salary_to') ||  Request::input('salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',$salary_to) ;
                                          }
                                          if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                              $query->where('advertisements.event_date','>=',$date_from);
                                          }
                                          if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$date_to);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                          if(Request::input('previous_price_from') || Request::input('previous_price_from') != 0){
                                              $query->where('advertisements.price','>=',Request::input('previous_price_from'));
                                          }
                                          if(Request::input('previous_price_to') ||  Request::input('previous_price_to') != 0){
                                              $query->where('advertisements.price','<=',Request::input('previous_price_to'));
                                          }
                                          if(Request::input('previous_country') && Request::input('previous_country') != "all"){
                                              $query->where('advertisements.country_id', '=', Request::input('previous_country'));
                                          }
                                          if(Request::input('previous_ad_type') && Request::input('previous_ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', Request::input('previous_ad_type'));
                                          }
                                          if(Request::input('previous_city') && Request::input('previous_city') != "all"){
                                              $query->where('advertisements.city_id', '=', Request::input('previous_city'));       
                                          }  
                                          if(Request::input('previous_keyword') && Request::input('previous_keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.Request::input('previous_keyword').'%');  
                                          }
                                           if(Request::input('previous_salary_from') ||  Request::input('previous_salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',Request::input('previous_salary_from')) ;
                                          }
                                          if(Request::input('previous_salary_to') ||  Request::input('previous_salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',Request::input('previous_salary_to')) ;
                                          }
                                          if(Request::input('previous_date_from') ||  Request::input('previous_date_from') != ""){
                                              $query->where('advertisements.event_date','>=',Request::input('previous_date_from'));
                                          }
                                          if(Request::input('previous_date_to') ||  Request::input('previous_date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$Request::input('previous_date_to'));
                                          }
                                      }else if(Request::input('update_status') == 0){
                                           $query->where('advertisements.ads_type_id', '=',Request::input('past_ad_type'));
                                      } 
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                      if(Request::input('update_status') == 1){
                                        if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                            $query->where('advertisements.parent_category','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('previous_main_cat_id'));
                                      }else{
                                           $query->where('advertisements.parent_category','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('past_category'));
                                      }                                                                       
                                   })                                                   
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.'.$sort, $order)
                                  // ->orderBy('advertisements.feature', 'desc')
                                  ->orderBy('advertisements.hierarchy_order', 'asc')
                                  ->orderBy('advertisements.created_at', 'desc')
                                  // ->take($per)
                                  ->get();


       $max_ads = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                   })
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                    if(Request::input('update_status') == 1){
                                          if(Request::input('price_from') || Request::input('price_from') != 0){
                                              $query->where('advertisements.price','>=',$price_from);
                                          }
                                          if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                              $query->where('advertisements.price','<=',$price_to);
                                          }
                                          if(Request::input('country') && Request::input('country') != "all"){
                                              $query->where('advertisements.country_id', '=', $country);
                                          }
                                          if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                          if(Request::input('city') && Request::input('city') != "all"){
                                              $query->where('advertisements.city_id', '=', $city);       
                                          }  
                                          if(Request::input('keyword') && Request::input('keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                          }
                                           if(Request::input('salary_from') ||  Request::input('salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',$salary_from) ;
                                          }
                                          if(Request::input('salary_to') ||  Request::input('salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',$salary_to) ;
                                          }
                                          if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                              $query->where('advertisements.event_date','>=',$date_from);
                                          }
                                          if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$date_to);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                          if(Request::input('previous_price_from') || Request::input('previous_price_from') != 0){
                                              $query->where('advertisements.price','>=',Request::input('previous_price_from'));
                                          }
                                          if(Request::input('previous_price_to') ||  Request::input('previous_price_to') != 0){
                                              $query->where('advertisements.price','<=',Request::input('previous_price_to'));
                                          }
                                          if(Request::input('previous_country') && Request::input('previous_country') != "all"){
                                              $query->where('advertisements.country_id', '=', Request::input('previous_country'));
                                          }
                                          if(Request::input('previous_ad_type') && Request::input('previous_ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', Request::input('previous_ad_type'));
                                          }
                                          if(Request::input('previous_city') && Request::input('previous_city') != "all"){
                                              $query->where('advertisements.city_id', '=', Request::input('previous_city'));       
                                          }  
                                          if(Request::input('previous_keyword') && Request::input('previous_keyword') != "" ){
                                              $query->where('advertisements.title', 'LIKE', '%'.Request::input('previous_keyword').'%');  
                                          }
                                           if(Request::input('previous_salary_from') ||  Request::input('previous_salary_from') != 0 ){
                                              $query->where('advertisements.salary','>=',Request::input('previous_salary_from')) ;
                                          }
                                          if(Request::input('previous_salary_to') ||  Request::input('previous_salary_to') != 0 ){
                                              $query->where('advertisements.salary','<=',Request::input('previous_salary_to')) ;
                                          }
                                          if(Request::input('previous_date_from') ||  Request::input('previous_date_from') != ""){
                                              $query->where('advertisements.event_date','>=',Request::input('previous_date_from'));
                                          }
                                          if(Request::input('previous_date_to') ||  Request::input('previous_date_to') != ""){
                                              $query->where('advertisements.event_date','<=',$Request::input('previous_date_to'));
                                          }
                                      }else if(Request::input('update_status') == 0){
                                           $query->where('advertisements.ads_type_id', '=',Request::input('past_ad_type'));
                                      } 
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                      if(Request::input('update_status') == 1){
                                        if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }
                                      }else if(Request::input('update_status') == 2){
                                            $query->where('advertisements.parent_category','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('previous_main_cat_id'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('previous_main_cat_id'));
                                      }else{
                                           $query->where('advertisements.parent_category','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.cat_unique_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.sub_category_id','=',Request::input('past_category'))
                                                  ->orWhere('advertisements.category_id','=',Request::input('past_category'));
                                      }                                                                       
                                   })                                                   
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.'.$sort, $order)
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->get();
      }                        
     if(Request::ajax()) {
          // $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $always_first = new Collection();
          $remaining = new Collection();
          foreach ($rows as $val) {
            if ($val->feature == 1) {
              $always_first->add($val);
            } else {
              $remaining->add($val);
            }
            $val->average_rate = ($val->getAdRatings->avg('rate') == null ? 0 : $val->getAdRatings->avg('rate'));
          }
          $result['sort'] = $sort;
          $result['rows'] = $always_first->merge($remaining)->take($per)->toArray();
          $result['history'] = $history;
          $result['order'] = $order;
          $result['per'] = $per;
          $result['count'] = count($rows);

          $result['banner_top'] = $banner_top;
          $result['banner_left_A'] = $banner_left_A;
          $result['banner_left_B'] = $banner_left_B;
          $result['banner_bottom'] = $banner_bottom;

           if(Request::input('update_status') == 1){
            $result['previous_main_category'] = Request::input('main_category');
            if($ad_type == 2){
                $result['previous_category_auction'] =  Request::input('category_auction');
            }
            $result['previous_main_cat_id'] = $main_cat_id;
            $result['previous_category'] =  Request::input('category');
            $result['previous_subcategory'] = Request::input('subcategory');
            $result['previous_subSubCategory'] =  Request::input('subSubCategory');
            $result['previous_country'] = Request::input('country') ;
            $result['previous_city'] = Request::input('city');
            $result['previous_keyword'] = Request::input('keyword');
            $result['previous_ad_type'] = Request::input('ad_type');
            $result['previous_price_from'] = Request::input('price_from');
            $result['previous_salary_to'] = Request::input('salary_to');
          }else if(Request::input('update_status') == 2){
            $result['previous_main_category'] = Request::input('previous_main_category');
            $result['previous_main_cat_id'] = Request::input('previous_main_cat_id');
            if($ad_type == 2){
              $result['previous_category_auction'] =  Request::input('previous_category_auction');
            }
            $result['previous_category'] =  Request::input('previous_category');
            $result['previous_subcategory'] = Request::input('previous_subcategory');
            $result['previous_subSubCategory'] =  Request::input('previous_subSubCategory');
            $result['previous_country'] = Request::input('previous_country') ;
            $result['previous_city'] = Request::input('previous_city');
            $result['previous_keyword'] = Request::input('previous_keyword');
            $result['previous_ad_type'] = Request::input('previous_ad_type');
            $result['previous_price_from'] = Request::input('previous_price_from');
            $result['previous_salary_to'] = Request::input('previous_salary_to');
          }

          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return Response::json($result);
      } else {
          // $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['categoy_id'] =Request::input('category');
          $result['per'] = $per;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');

 


          if(Request::input('update_status') == 1){
            $result['previous_main_category'] = Request::input('main_category');
            if($ad_type == 2){
              $result['previous_category_auction'] =  Request::input('category_auction');
            }
            $result['previous_category'] =  Request::input('category');
            $result['previous_main_cat_id'] = $main_cat_id;
            $result['previous_subcategory'] = Request::input('subcategory');
            $result['previous_subSubCategory'] =  Request::input('subSubCategory');
            $result['previous_country'] = Request::input('country') ;
            $result['previous_city'] = Request::input('city');
            $result['previous_price_from'] = Request::input('price_from');
            $result['previous_keyword'] = Request::input('keyword');
            $result['previous_ad_type'] = Request::input('ad_type');
            $result['previous_salary_to'] = Request::input('salary_to');
          }else if(Request::input('update_status') == 2){

            $result['previous_main_category'] = Request::input('previous_main_category');
            if($ad_type == 2){
              $result['previous_category_auction'] =  Request::input('previous_category_auction');
            }
            $result['previous_main_cat_id'] = Request::input('previous_main_cat_id');
            $result['previous_category'] =  Request::input('previous_category');
            $result['previous_subcategory'] = Request::input('previous_subcategory');
            $result['previous_subSubCategory'] =  Request::input('previous_subSubCategory');
            $result['previous_country'] = Request::input('previous_country') ;
            $result['previous_city'] = Request::input('previous_city');
            $result['previous_price_from'] = Request::input('previous_price_from');
            $result['previous_salary_to'] = Request::input('previous_salary_to');
            $result['previous_keyword'] = Request::input('previous_keyword');
            $result['previous_ad_type'] = Request::input('previous_ad_type');
          }
         


          return $result;
      }                                           
                                                   
  }
   public function fetchCustomSearch(){
    // dd(Request::all());
      $result = $this->doCustomSearch();
      $this->data['rows'] = $result['rows'];
      $this->data['per'] = $result['per'];
      $this->data['count'] = $result['count'];

      $this->data['previous_main_category'] = $result['previous_main_category'];
      $this->data['previous_category_auction'] = $result['previous_category_auction'];
      $this->data['previous_category'] = $result['previous_category'];
      $this->data['previous_subcategory'] = $result['previous_subcategory'];
      $this->data['previous_subSubCategory'] = $result['previous_subSubCategory'];
      $this->data['previous_country'] = $result['previous_country'];
      $this->data['previous_city'] = $result['previous_city'];
      $this->data['previous_price_from'] = $result['previous_price_from'];
      $this->data['previous_salary_to'] = $result['previous_salary_to'];
      $this->data['previous_main_cat_id'] = $result['previous_main_cat_id'];

      $this->data['banner_top'] = $result['banner_top'];
      $this->data['banner_bottom'] = $result['banner_bottom'];
      $this->data['banner_left_A'] = $result['banner_left_A'];
      $this->data['banner_left_B'] = $result['banner_left_B'];

      $id =  $result['categoy_id'];
      $this->data['category_id'] = $id;

      $this->data['category_title'] = Category::find($id); 
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'users.usertype_id', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'users.usertype_id', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.usertype_id', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      if(!Auth::check()){
         $this->data['user_country'] =  1;
         $this->data['user_city'] =  'all';
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_sub_attri_id = SubAttributes::where('attri_id','=',$id)->get();
                     $container_attrib = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';

                      $container_attrib .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $container_attrib .= '<div class="form-group">
                                <h5 class="col-sm-3 normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $container_attrib .= '<div class="col-sm-9">'; 
                      $container_attrib .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control borderZero">';
                      $container_attrib .= '</div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $container_attrib .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $container_attrib .= $field->name.'</h5>';
                      $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field_'.$field->id.'[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $container_attrib .='</select></div>';
                      $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
                      $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
        
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" '.($this->data['user_city'] == 'all' ? 'selected':'').'>All</option>';
          foreach ($get_city as $key => $field) {
            $container_city .= '<option value="' . $field->id . '" '.($field->id == $this->data['user_city'] ? 'selected':'').'>' . $field->name . '</option>';
          }
 
      $this->data['container_city'] = $container_city;
      $this->data['container_attrib'] = $container_attrib ? : "";
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies);
      $this->data['category_title'] = "";
      $this->data['countries'] = Country::all();
      $this->data['categories'] = Category::where('status',1)->get();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['countries'] = Country::orderBy('countryName','asc')->get();
      $this->data['ad_type'] = AdvertisementType::all();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      $this->data['sub_category'] = SubCategory::where('status',1)->where('cat_id', Session::get('session_cat_id'))->get();                            
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['category_all'] = Category::with('subcategoryinfo') ->get();
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get();  
      $this->data['cat_id'] = 0;
      $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
      $this->data['banner_placement_bottom'] = BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      
      // YAH IT WORKS TILL THE END!!!!                               
      return View::make('category.list',$this->data);
  }
   public function doListAll(){

      $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 100;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

       $rows = Advertisement::select('advertisements_photo.*','advertisements.*')
                                ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                ->where('advertisements_photo.primary','=',1)
                                ->where('advertisements_photo.status','=',1)
                                ->where(function($query) use ($status) {
                                    $query->where('advertisements.status', 'LIKE', '%' . $status . '%')
                                          ->where('advertisements.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('advertisements.status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);   
      } else {
        $count = Advertisement::where('category_id','=',$id)
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::where('category_id','=',$id)
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  // public function slugger(){
  //   $row = SubCategory::all();
  //   foreach ($row as $slug) {
  //     $slug->slug = str_slug($slug->name,'-') . '-' . $slug->id;
  //     $slug->save();
  //   }
  // }

  public function fetchList(){
// dd('adasd');
      $this->countrySet();
      $result = $this->doListAll();
      $this->data['rows'] = $result['rows'];
 
      // dd($this->data['rows']);
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      // dd($this->data['rows']);
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
          $this->data['featured_ads'] = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1')
                                                   ->where('advertisements.country_code','=','ph');                                
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies); 
        $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
        $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get(); 
        $this->data['category_title'] = "All";
        $this->data['countries'] = Country::all();
        // $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();
        if(Session::has('selected_location') == null){
          $selectedCountry = Session::get('default_location');
        }else{
          $selectedCountry = Session::get('selected_location');
        }
        $this->data['categories'] = Category::where(function($query) use ($selectedCountry){
                                                      if($selectedCountry != "ALL"){
                                                         $query->where('country_code','=',$selectedCountry);
                                                      }else{
                                                          $query->where('country_code','=','us');
                                                      }
                                                     
                                             }) 
                                             ->where('status',1)
                                             ->groupBy('name')
                                             ->get();
   
                                             
        $this->data['usertypes'] = Usertypes::all();

        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['countries'] = Country::orderBy('countryName','asc')->get();
      $this->data['ad_type'] = AdvertisementType::all();
      $this->data['category'] = Category::with('subcategoryinfo')
                                         ->get();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      // $this->data['category_all'] = Category::with('subcategoryinfo')
      //                               ->where('unique_id','=',$id)
      //                               ->get();
      $this->data['cat_id'] = null;
      // $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
      // $this->data['banner_placement_bottom'] =  BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);
      $get_banner_placement_right = BannerAdvertisementSubscriptions::where('page',6)->where('banner_status','=',4)->where('placement','=',2)->get();

      if (count($get_banner_placement_right) > 0) {
         $this->data['banner_placement_right_status'] = 1;
         $this->data['banner_placement_right'] = $get_banner_placement_right->random(1);
      }else{
         $this->data['banner_placement_right_status'] = 0;
      }

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      // $client_ip = Request::ip();
      // $client_location = geoip()->getLocation($client_ip)->iso_code;
      // if(Auth::check()){
      //   if(Auth::user()->country == 0){
      //     Session::set('default_location',strtolower($client_location));
      //     $country_code = Session::get('default_location');
      //   }else{
      //     Session::forget('default_location');
      //     $get_user_country = Country::where('id',Auth::user()->country)->first();
      //     $country_code = strtolower($get_user_country->countryCode);
      //     Session::set('default_location',$country_code);
      //   }
      // }else{
      //   Session::set('default_location',strtolower($client_location));
      //   $country_code = Session::get('default_location');
      // }
    $countries = array_map('strtolower',Country::where('status',1)->lists('countryCode')->toArray());
    $detect_ip = Request::ip();
    $detect_location = strtolower(geoip()->getLocation($detect_ip)->iso_code);
    if(Session::has('selected_location'))
    {
      Session::set('default_location', Session::get('selected_location'));
    }
    else
    {
      if(in_array($detect_location, $countries))
      {
        Session::set('default_location', $detect_location);
        Session::set('selected_location', $detect_location);
      }
      else
      {
        Session::set('default_location', 'ALL');
        Session::set('selected_location', 'ALL');
      }
    }

      // $get_banner_placement_top= BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',3)->get();

      //  if (count($get_banner_placement_top) > 0) {
      //    $this->data['banner_placement_top_status'] = 1;
      //    $this->data['banner_placement_top'] = $get_banner_placement_top->random(1);
      //   }else{
      //    $this->data['banner_placement_top_status'] = 0;
      //  }

      // $get_banner_placement_bottom = BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',4)->get();
      //  if (count($get_banner_placement_bottom) > 0) {
      //    $this->data['banner_placement_bottom_status'] = 1;
      //    $this->data['banner_placement_bottom'] = $get_banner_placement_bottom->random(1);
      //   }else{
      //    $this->data['banner_placement_bottom_status'] = 0;
      //  }

      // $get_banner_placement_left = BannerAdvertisementSubscriptions::where('page',2)->where('banner_status','=',4)->where('category',$id)->where('placement','=',1)->get();
      //  if (count($get_banner_placement_left) > 0) {
      //    $this->data['banner_placement_left_status'] = 1;
      //    $this->data['banner_placement_left'] = $get_banner_placement_left->random(1);
      //   }else{
      //    $this->data['banner_placement_left_status'] = 0;
      //  }


      $this->data['country_code_session'] = Session::get('selected_location');
      return View::make('category.all',$this->data);
  }
   public function getCustomAttributes(){
       // $cat_id = Request::input('id');
       // dd(Request::all());
       $main_category_id = Request::input('main_category_id');
       $category_id = Request::input('category_id');
       $category_auction_id = Request::input('category_auction_id');
       $sub_cat_one_id = Request::input('sub_cat_one_id');

       $sub_cat_two_id = Request::input('sub_cat_two_id');
       $ad_type = Request::input('ad_type') ? : 1;
       $cat_id = null;
       $rows = '';

//       if($ad_type != 2){
          if($main_category_id == "all"){
           $cat_id  = $main_category_id;
          }else if($category_id == "all"){
              $cat_id = $main_category_id;  
          }else if($sub_cat_one_id == "all"){
              $cat_id = $category_id;
          }else if($sub_cat_two_id == "all"){
              $cat_id = $sub_cat_one_id;
          }else{
              $cat_id = $sub_cat_two_id;
          }
//       }else{
//           if($main_category_id == "all"){
//             $cat_id  = $main_category_id;
//           }else if($category_auction_id == "all"){
//             $cat_id = $main_category_id;  
//           }else if($category_id == "all"){ 
//             $cat_id = $category_auction_id;  
//           }else if($sub_cat_one_id == "all"){
//             $cat_id = $category_id;
//           }else if($sub_cat_two_id == "all"){
//             $cat_id = $sub_cat_one_id;
//           }else{
//             $cat_id = $sub_cat_two_id;
//           }
//       }
       $category_name = '';
       $find_cat_name = Category::where('unique_id',$cat_id)->where('status',1)->first();
       if(is_null($find_cat_name)){
          $find_subcat_name = SubCategory::where('unique_id',$cat_id)->where('status',1)->first();
            if(is_null($find_subcat_name)){
                  $find_subcat_two_name = SubCategoriesTwo::where('unique_id',$cat_id)->where('status',1)->first();
                   if(is_null($find_subcat_two_name)){
                        $find_subcat_three_name = SubCategoriesThree::where('unique_id',$cat_id)->where('status',1)->first();
                        if(is_null($find_subcat_three_name)){
                           $category_name .= '';
                        }else{
                           $category_name .= $find_subcat_three_name->name;
                        }
                   }else{
                    $category_name .= $find_subcat_two_name->name;
                   }    
            }else{
              $category_name .= $find_subcat_name->name;
            }
       }else{
          $category_name .= $find_cat_name->name;
       }
      $parent_cat_id = '';
      $check_sub_category_two = SubCategoriesTwo::where('unique_id',$cat_id)->first();
        if(!is_null($check_sub_category_two)){
                $check_sub_category = Subcategory::where('unique_id',$check_sub_category_two->subcat_main_id)->first();
                   $parent_cat_id .= $check_sub_category->cat_id;
           }else{
             $check_sub_category_null = Subcategory::where('unique_id',$cat_id)->first();
              if(!is_null($check_sub_category_null)){
                   $parent_cat_id .= $check_sub_category_null->cat_id;
              }else{
                  $check_category_null = Category::where('unique_id',$cat_id)->first();
                   if(!is_null($check_category_null)){
                     $parent_cat_id .= $check_category_null->unique_id;
                   }
             }
        }
      if($ad_type){
         if(!is_null($parent_cat_id)){
          $get_parent_attributes = SubAttributes::where('attri_id',$parent_cat_id)->where('status',1)->get();
             foreach ($get_parent_attributes as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $rows .= '<div class="col-sm-6 leftPadding">'; 
                      $rows .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $rows .= '</div>';
                      $rows .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $rows .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .=  $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '</select></div>';
                      $rows .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $rows .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
            }
       }
      }
//        if($ad_type == 2){
//         if($category_auction_id != "all"){
//           $get_sub_attri_id = SubAttributes::where('attri_id',$category_auction_id)->get();
//            foreach ($get_sub_attri_id as $key => $field) {
//                  if($field->attribute_type == 1){
//                       $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
//                       $rows .= $field->name.'</h5>';
//                       $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
//                       $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
//                       foreach ($get_sub_attri_info as $key => $dropdown_info) {
//                         $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
//                       }
//                       $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
//                       $rows .= '</select></div></div>';
//                   }
//                  if($field->attribute_type == 2){
//                       $rows .= '<div class="form-group bottomMargin">
//                                 <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
//                       $rows .= '<div class="col-sm-6 leftPadding">'; 
//                       $rows .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
//                       $rows .= '</div>';
//                       $rows .= '<input type="hidden" name="attri_type[]" value = "textbox">';
//                       $rows .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
//                  }
//                  if($field->attribute_type == 3){
//                       $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
//                       $rows .=  $field->name.'</h5>';
//                       $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
//                       $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
//                       foreach ($get_sub_attri_info as $key => $dropdown_info) {
//                         $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
//                       }
//                       $rows .= '</select></div>';
//                       $rows .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
//                       $rows .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
//                  }           
//           }
//        }
//        }
       if($category_id != "all"){
          $get_sub_attri_id = SubAttributes::where('attri_id',$category_id)->get();
           foreach ($get_sub_attri_id as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $rows .= '<div class="col-sm-6 leftPadding">'; 
                      $rows .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $rows .= '</div>';
                      $rows .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $rows .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                 }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .=  $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '</select></div>';
                      $rows .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $rows .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
       }
      
        
       if($sub_cat_one_id != "all"){
          $get_sub_attri_id = SubAttributes::where('attri_id',$sub_cat_one_id)->get();
           foreach ($get_sub_attri_id as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $rows .= '<div class="col-sm-6 leftPadding">'; 
                      $rows .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $rows .= '</div>';
                      $rows .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $rows .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                 }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .=  $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '</select></div>';
                      $rows .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $rows .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
       }
        if($sub_cat_two_id != "all"){
          $get_sub_attri_id = SubAttributes::where('attri_id',$sub_cat_two_id)->get();
           foreach ($get_sub_attri_id as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide inputBox">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .= '</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group bottomMargin">
                                <h5 class="col-sm-6 norightPadding normalText" for="register4-email">'.$field->name.'</h5>'; 
                      $rows .= '<div class="col-sm-6 leftPadding">'; 
                      $rows .= '<input type="text" name="textbox_field_'.$field->id.'[]" class="form-control inputBox borderZero">';
                      $rows .= '</div>';
                      $rows .= '<input type="hidden" name="attri_type[]" value = "textbox">';
                      $rows .= '<input type="hidden" name="textbox_attri_id[]" value="'.$field->id.'"></div>';
                 }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group bottomMargin"><h5 class="col-sm-6 norightPadding normalText" for="register4-email">'; 
                      $rows .=  $field->name.'</h5>';
                      $rows .= '<div class="col-sm-6 leftPadding"><select class="form-control inputBox borderZero" name="dropdown_field_'.$field->id.'[]"><option value="">All</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                      foreach ($get_sub_attri_info as $key => $dropdown_info) {
                        $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                      }
                      $rows .= '</select></div>';
                      $rows .= '<input type="hidden" name="attri_type[]" id="attri_type" value = "dropdown">';
                      $rows .= '<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
                 }           
          }
       }
      // $result['category_id'] = $get_category_id;
      $result['rows'] = $rows;
      $result['category_name'] = $category_name.' FILTERS';
      return Response::json($result);
   }
    public function getCatType(){
      $id = Request::input('id') ? : Session::get('subcategory');
      if(Session::get('default_location') != "ALL"){
        if($id == 1){
            $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code',Session::get('default_location'))
                                        ->where('buy_and_sell_status',2);        
                                  })->get();
        }else if($id == 2){
            $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code',Session::get('default_location'))
                                        ->where('biddable_status',2);
                                  })->get();
        }
        else
        {
          $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code',Session::get('default_location'));
                                  })->get();
        }
      
      }else{
        if($id == 1){
           $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code','us')
                                        ->where('buy_and_sell_status',2);        
                                  })->get();
        }else if($id == 2){
           $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code','us')
                                        ->where('biddable_status',2);        
                                  })->get();
        }
        else
        {
          $row = Category::where(function($query) use ($id) {
                                  $query->where('status',1)
                                        ->where('country_code','us');
                                  })->get();
        }
       
      }
    
      $cat_type="";
      if(count($row)>0){
         $cat_type.= "<option data-slug='all-categories' value='all'>All Categories</option>";
        foreach ($row as $key => $field) {
         $cat_type .= '<option data-slug="'.$field->slug.'" value="'.$field->unique_id.'">'.$field->name.'</option>';
        }
      }else{
         $cat_type.= "<option data-slug='all-categories' value='all'>All Categories</option>";
         // $cat_type .= "undefined";
      }
      $result['cat_type'] = $cat_type;
      return Response::json($result);
    }
      public function getCatTypeForPost(){
      if(Session::has('selected_location') == null){
        $selectedCountry = Session::get('default_location');
      }else{
        $selectedCountry = Session::get('selected_location');
      }
     
       $id = Request::input('id') ? : Session::get('subcategory');
       if($id == 1){                         
          $row = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                          ->where(function($query) use ($id) {
                                            $query->where('status',1)
                                                  ->where('cat_type',$id);        
                                           })->where('buy_and_sell_status',2)->get();
       }else if($id == 2){
           $row = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                          ->where(function($query) {
                                            $query->where('status',1)
                                                  ->where('biddable_status', 2);
                                           })->get();
       }else{
        $row = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           })
                                          ->where('status',1)->get();
       }
        $cat_type="";
        if(count($row)>0){
          $cat_type .= "<option class='hide'>Select</option>";
          foreach ($row as $key => $field) {
           $cat_type .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
          }
        }else{
           $cat_type .= "undefined";
        }
        $result['cat_type'] = $cat_type;
        return Response::json($result);

    }
    public function getAuctionCategory(){
      $auction_id_list = Category::where(function($query) {
                                  $query->where('status',1)
                                        ->where('biddable_status',1);        
                                  })->lists('id');
      $row =  Category::whereIn('id',$auction_id_list)->get();
      $auction_cat="";
      if(count($row)>0){
         $auction_cat.= "<option data-slug='all-categories' value='all'>All Sub Categories</option>";
        foreach ($row as $key => $field) {
         $auction_cat .= '<option data-slug="'.$field->slug.'" value="'.$field->unique_id.'">'.$field->name.'</option>';
        }
      }else{
         $auction_cat .= "undefined";
      }
      $result['auction_cat'] = $auction_cat;
      return Response::json($result);
    }
    public function getSubSubCategory(){
       $row = SubCategoriesThree::where(function($query) {
                                  $query->where('status',1)
                                        ->where('subcat_tier_id',Request::input('id'));     
                                  })->get();
      $subsub_cat="";
      if(count($row)>0){
         $subsub_cat.= "<option value='all'>All</option>";
        foreach ($row as $key => $field) {
         $subsub_cat .= '<option value="'.$field->unique_id.'">'.$field->name.'</option>';
        }
      }else{
         $subsub_cat .= "undefined";
      }
      $result['subsub_cat'] = $subsub_cat;
      return Response::json($result);

    }
}