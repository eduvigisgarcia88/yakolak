<?php
namespace App\Http\Controllers;


// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Request;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\City;
use Paginator;
use Response;
use App\AdRating;
use App\UserAdComments;
use App\Localization;
use App\CountryBannerPlacement;
use App\BannerPlacementPlans;
use App\BannerDefault;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use Input;
use Validator;
use App\ThemePlan;
use App\ThemeCountry;

class ThemeController extends Controller
{
		public function indexThemeCountry(){
			$not_allowed = array(1,2);
		    if(Auth::check()){
		      if(in_array(Auth::user()->usertype_id, $not_allowed))
		      {
		          return redirect()->to('error/access/denied');
		      }
		    }
		    else
		    {
		      return redirect()->to('error/access/denied');
		    }
			$result = $this->doListThemeCountry();
		    $this->data['rows'] = $result['rows'];
			$this->data['pages'] = $result['pages'];
			$this->data['title'] = "Country Themes";
			$this->data['refresh_route'] = url("admin/settings/theme/country/refresh");
		    return View::make('admin.theme-management.country.list', $this->data);
		}
		public function doListThemeCountry(){
		     $result['sort'] = Request::input('sort') ?: 'created_at';
		     $result['order'] = Request::input('order') ?: 'desc';
		     $search = Request::input('search') ? : '';
		     $status = Request::input('status') ? : '';
		     $per = Request::input('per') ?:10;

		     if (Request::input('page') != '»') {
		        Paginator::currentPageResolver(function () {
		            return Request::input('page'); 
		     });

		        $rows = ThemeCountry::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);

		      } else {
		         $count = ThemeCountry::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);

		        Paginator::currentPageResolver(function () use ($count, $per) {
		            return ceil($count->total() / $per);
		        });

	    	      $rows = ThemeCountry::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);
		      }

		      // return response (format accordingly)
		      if(Request::ajax()) {
		          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
		          $result['rows'] = $rows->toArray();
		          return Response::json($result);
		      } else {
		          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
		          $result['rows'] = $rows;
		          return $result;
		      }	


		}

		public function indexThemePlan(){
			$not_allowed = array(1,2);
		    if(Auth::check()){
		      if(in_array(Auth::user()->usertype_id, $not_allowed))
		      {
		          return redirect()->to('error/access/denied');
		      }
		    }
		    else
		    {
		      return redirect()->to('error/access/denied');
		    }
			$result = $this->doListThemePlan();
		    $this->data['rows'] = $result['rows'];
			$this->data['pages'] = $result['pages'];
			$this->data['title'] = "Plan Themes";
			$this->data['refresh_route'] = url("admin/settings/theme/plan/refresh");
		    return View::make('admin.theme-management.plan.list', $this->data);

		}
		public function doListThemePlan(){
			 $result['sort'] = Request::input('sort') ?: 'created_at';
		     $result['order'] = Request::input('order') ?: 'desc';
		     $search = Request::input('search') ? : '';
		     $status = Request::input('status') ? : '';
		     $per = Request::input('per') ?:10;

		     if (Request::input('page') != '»') {
		        Paginator::currentPageResolver(function () {
		            return Request::input('page'); 
		     });

		         $rows = ThemePlan::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);

		      } else {
		         $count = ThemePlan::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);

		        Paginator::currentPageResolver(function () use ($count, $per) {
		            return ceil($count->total() / $per);
		        });

	    	      $rows = ThemePlan::where(function($query) use ($status) {
		                                  $query->where('status', 'LIKE', '%' . $status . '%');   
		                                  })
	                          ->where(function($query) use ($search) {
	                                        $query->where('name', 'LIKE', '%' . $search . '%');
	                           })
	                          ->where('status','!=',3)
		                      ->orderBy($result['sort'], $result['order'])
		                      ->paginate($per);
		      }

		      // return response (format accordingly)
		      if(Request::ajax()) {
		          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
		          $result['rows'] = $rows->toArray();
		          return Response::json($result);
		      } else {
		          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
		          $result['rows'] = $rows;
		          return $result;
		      }	


		}
		//for plan
		public function disableThemePlan(){
		   $row = ThemePlan::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=2;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Disabled']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function enableThemePlan(){
			$row = ThemePlan::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=1;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Enabled']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
 		public function removeThemePlan(){
			$row = ThemePlan::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=3;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Removed']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function manipulateThemePlan(){
		    $row = ThemePlan::find(Request::input('id'));
		    if(!is_null($row)){
		    	return Response::json($row);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function saveThemePlan(){
			$new = true;
		    $input = Input::all();
		        if(array_get($input, 'id')) {
		            // get the user info
		            $row = ThemePlan::find(array_get($input, 'id'));
		            if(is_null($row)) {
		                return Response::json(['error' => "The requested item was not found in the database."]);
		            }
		            // this is an existing row
		            $new = false;
		        }
		         $rules = [
		             'name'                    => 'required',
		             'status'			       => 'required',
		             'style_name'                    => 'required',
		             'style_path'					 => 'required',
		          
		        ];
		        // field name overrides
		        $names = [   
		            'name'                    => 'Name',
		            'status'			       => 'Status',
		            'style_name'                    => 'StyleSheet',
		            'style_path'					 => 'StyleSheet Path',
		        ];  

		           // do validation
		        $validator = Validator::make(Input::all(), $rules);
		        $validator->setAttributeNames($names); 

		        // return errors
		        if($validator->fails()) {
		            return Response::json(['error' => array_unique($validator->errors()->all())]);
		        }
		        if($new) {
		          $row = new ThemePlan;
		        }
		          $row->name = array_get($input,'name');
		          $row->status = array_get($input,'status');
		          $row->style_name = array_get($input,'style_name');
		          $row->style_path = array_get($input,'style_path');
		          $row->save();
		        if($new){
		        	return Response::json(['body' => 'Theme Added']);
		        }else{
		         	return Response::json(['body' => 'Theme Updated']);
		        }
	    }

		//for country
		public function disableThemeCountry(){
		   $row = ThemeCountry::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=2;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Disabled']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function enableThemeCountry(){
			$row = ThemeCountry::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=1;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Enabled']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function removeThemeCountry(){
			$row = ThemeCountry::find(Request::input('id'));
		    if(!is_null($row)){
		    	$row->status=3;
		    	$row->save();
		    	return Response::json(['body'=>'Theme Removed']);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function manipulateThemeCountry(){
		    $row = ThemeCountry::find(Request::input('id'));
		    if(!is_null($row)){
		    	return Response::json($row);
		    }else{
		    	return Response::json(['error'=>'Invalid ID']);
		    }
		}
		public function saveThemeCountry(){
			$new = true;
		    $input = Input::all();
		        if(array_get($input, 'id')) {
		            // get the user info
		            $row = ThemeCountry::find(array_get($input, 'id'));
		            if(is_null($row)) {
		                return Response::json(['error' => "The requested item was not found in the database."]);
		            }
		            // this is an existing row
		            $new = false;
		        }
		         $rules = [
		             'name'                    => 'required',
		             'status'			       => 'required',
		             'style_name'                    => 'required',
		             'style_path'					 => 'required',
		          
		        ];
		        // field name overrides
		        $names = [   
		            'name'                    => 'Name',
		            'status'			       => 'Status',
		            'style_name'                    => 'StyleSheet',
		            'style_path'					 => 'StyleSheet Path',
		        ];  

		           // do validation
		        $validator = Validator::make(Input::all(), $rules);
		        $validator->setAttributeNames($names); 

		        // return errors
		        if($validator->fails()) {
		            return Response::json(['error' => array_unique($validator->errors()->all())]);
		        }
		        if($new) {
		          $row = new ThemeCountry;
		        }
		          $row->name = array_get($input,'name');
		          $row->status = array_get($input,'status');
		          $row->style_name = array_get($input,'style_name');
		          $row->style_path = array_get($input,'style_path');
		          $row->save();

		        if($new){
		        	return Response::json(['body' => 'Theme Added']);
		        }else{
		         	return Response::json(['body' => 'Theme Updated']);
		        }
	    }


}