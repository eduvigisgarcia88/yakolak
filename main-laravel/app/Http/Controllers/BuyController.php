<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\AdRating;
use App\UserAdComments;

class BuyController extends Controller
{

	public function index(){
		$this->data['title'] = "Buy";
		if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id)
                                                      ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

		return View::make('buy.list', $this->data);
	}

}