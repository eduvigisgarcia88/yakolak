<?php 
namespace App\Http\Controllers;


use App\Http\Requests;
use App\AdvertisementPhoto;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Response;
use App\Advertisement;
use App\CustomAttributes;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\PromotionPopups;
use Request;
use App\Category;
// use App\AdvertisementWatchlist;
use App\AdvertisementWatchlists;
use App\Country;
use App\CountryCode;
use App\City;
use App\Usertypes;
use Excel;
use Response;
use Auth;
use Redirect;
use Input;
use Carbon\Carbon;
use App\User;
use App\UserMessages;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\AdRating;
use App\UserAdComments;
use App\BannerAdvertisementSubscriptions;
use App\BannerAdvertisementMerge;
use App\BannerPlacementPlans;
use App\CountryBannerPlacement;
use App\BannerDefault;
use Session;
use App\Newsletters;
use App\UserLogs;
use App\UserReferrals;
use App\UserPlanTypes;
use App\AdvertisementSpamAndBots;
use App\SettingForSiteKey;
use App\CategoryNew;
use App\Localization;
use Mail;
use Paginator;
use App\CountryDetails;
use Illuminate\Database\Eloquent\Collection;

class HomeController extends Controller {

  /*
  |--------------------------------------------------------------------------
  | Home Controller
  |--------------------------------------------------------------------------
  |
  | This controller renders your application's "dashboard" for users that
  | are authenticated. Of course, you are free to change or remove the
  | controller as you wish. It is just here to get your app started!
  |
  */

  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application dashboard to the user.
   *
   * @return Response
   */

  public function VerifyClose()
  {
    Session::set('verify_close',1);
  }

  public function LocationSwitch()
  {

    $countries = array_map('strtolower',Country::where('status',1)->lists('countryCode')->toArray());
    if(in_array(request()->data_code, $countries))
    {
      if(request()->data_code == '')
      {
        $response['code'] = 1;
        $response['country_code'] = '';
        Session::set('selected_location','ALL');
      }
      else
      {
        $response['code'] = 1;
        $response['country_code'] = request()->data_code;
        Session::set('selected_location',request()->data_code);
      }
      return $response;
    }
    else
    {
      $response['code'] = 0;
      $response['country_code'] = 'ALL';
      Session::set('selected_location','ALL');
      return $response;
    }
  }

  public function DeniedPage($master_country = null)
  {
    $countries = array_map('strtolower',Country::where('status',1)->lists('countryCode')->toArray());
    $detect_ip = Request::ip();
    $detect_location = strtolower(geoip()->getLocation($detect_ip)->iso_code);
    if($master_country == null)
    {
      if(in_array($detect_location, $countries))
      {
        Session::forget('default_location');

        if(Session::has('selected_location'))
        {
          Session::set('default_location',Session::get('selected_location'));
          $sess_master_country = Session::get('selected_location');
        }
        else
        {
          Session::set('default_location',$detect_location);
          $sess_master_country = $detect_location;
        }
      }
      else
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = 'ALL';
          Session::forget('default_location');
          Session::set('default_location','ALL');
        }
      }
    }
    else
    {
      if(in_array($master_country, $countries))
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = $master_country;
          Session::forget('default_location');
          Session::set('default_location',$master_country);
        }
      }
      else
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = 'ALL';
          Session::forget('default_location');
          Session::set('default_location','ALL');
        }
      }
    }
     Session::forget('keyword');
     $row  = Advertisement::orderBy('id', 'desc')->first();

      $count_ads =  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')
                                      ->get();                                
      $count_bids=  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      

      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();

      
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

                                                  
      $this->data['adslatestbidsmobile'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at','advertisements.hierarchy_order', 'advertisements.description', 'advertisements.feature', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')                                       
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '2');                                     
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

      $this->data['adslatestbids'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at','advertisements.hierarchy_order', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                     
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->take(8)->get();


     $this->data['adslatestbids_panel2'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->skip(4)->take(4)->get();

                                        // dd($this->data['adslatestbids_panel2']);
      $this->data['adslatestbids_panel3'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')  
                                                   ->where('advertisements.status', '!=', '4')                                                                           
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get();


      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price','advertisements.hierarchy_order', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                           
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.feature', 'desc')
                                       ->orderBy('advertisements.hierarchy_order', 'asc')
                                       ->orderBy('advertisements.created_at', 'desc')
                                       ->take(4)
                                       ->get();

       $this->data['latestads'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price','advertisements.hierarchy_order', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get()->take(8);

       $count_ads =   Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get(); 
      $count_bids = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies);

       $this->data['max_ads_count'] = count($count_ads);
       $this->data['max_bids_count'] = count($count_bids);
                                  
       $this->data['latestads_panel2'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

//dd( $this->data['latestads2']);
       $this->data['adslatestbuysellxs'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get();                             
      $this->data['get_banner_jumbo_main'] = BannerAdvertisementSubscriptions::where('page',1)
                                                                              ->where('country_id',Session::get('default_location'))
                                                                              ->where('banner_status','=',4)
                                                                              ->take(3)
                                                                              ->get();
      $this->data['get_banner_jumbo_top'] = BannerAdvertisementSubscriptions::where('page',1)
                                                                              ->where('country_id',Session::get('default_location'))
                                                                              ->where('banner_status','=',4)
                                                                              ->take(3)
                                                                              ->get();
      $this->data['get_banner_jumbo_bottom'] = BannerAdvertisementSubscriptions::where('page',1)
                                                                              ->where('country_id',Session::get('default_location'))
                                                                              ->where('banner_status','=',4)
                                                                              ->take(3)
                                                                              ->get(); 
    
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();

      //$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      
      $this->data['countries'] = Country::where('status',1)->get();
      $this->data['usertypes'] = Usertypes::where('type',1)->where('status',1)->get();
      $this->data['categories'] = Category::where('status',1)->get();
      $get_key =  SettingForSiteKey::orderBy('created_at','desc')->first();
      // Session::put('siteKey',$get_key->site_key);
      Session::put('siteKey',$get_key->site_key);
      // $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',1)->get();
      // $this->data['bid'] = Category::where('biddable_status','=',1)->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                            
      // dd($this->data['notifications_counter']);                                       
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();

      
      $this->data['country_code_session'] = Session::get('default_location');

      
      return view('errors.denied', $this->data);
  }



  public function index($master_country = null)
  { 
    $countries = array_map('strtolower',Country::where('status',1)->lists('countryCode')->toArray());
    $detect_ip = Request::ip();
    $detect_location = strtolower(geoip()->getLocation($detect_ip)->iso_code);
    if($master_country == null)
    {
      if(in_array($detect_location, $countries))
      {
        Session::forget('default_location');

        if(Session::has('selected_location'))
        {
          Session::set('default_location',Session::get('selected_location'));
          $sess_master_country = Session::get('selected_location');
        }
        else
        {
          Session::set('default_location',$detect_location);
          $sess_master_country = $detect_location;
        }
      }
      else
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = 'ALL';
          Session::forget('default_location');
          Session::set('default_location','ALL');
        }
      }
    }
    else
    {
      if(in_array($master_country, $countries))
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = $master_country;
          Session::forget('default_location');
          Session::set('default_location',$master_country);
        }
      }
      else
      {
        if(Session::has('selected_location'))
        {
          $sess_master_country = Session::get('selected_location');
          Session::forget('default_location');
          Session::set('default_location',Session::get('selected_location'));
        }
        else
        {
          $sess_master_country = 'ALL';
          Session::forget('default_location');
          Session::set('default_location','ALL');
        }
      }
    }
     Session::forget('keyword');
     $row  = Advertisement::orderBy('id', 'desc')->first();

          $count_ads =  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();


      $count_bids=  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      

      $this->data['top_members'] = User::with('getUserAdvertisements')
                                       ->take(4)
                                       ->get();

      
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();

      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

                                                  
      // $this->data['adslatestbidsmobile'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
      //                                 ->leftJoin('users','users.id', '=', 'advertisements.user_id')
      //                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
      //                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
      //                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
      //                                  ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','users.usertype_id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at','advertisements.hierarchy_order', 'advertisements.description', 'advertisements.feature', 'advertisements_photo.photo', 
      //                                    'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
      //                                  ->selectRaw('avg(rate) as average_rate')                                       
      //                                  ->where(function($query) use ($row){
      //                                        $query->where('advertisements_photo.primary', '=', '1')
      //                                              ->where('advertisements.status', '=', '1')
      //                                              ->where('advertisements.ads_type_id', '=', '2');                                     
      //                                   })
      //                                  ->where(function($query) use ($sess_master_country,$countries) {
      //                                     if(in_array($sess_master_country,$countries)) {
      //                                       $query->where('advertisements.country_code', '=', $sess_master_country);
      //                                     }
      //                                   })
      //                                   ->groupBy('advertisements.id')
      //                                   ->orderBy('advertisements.feature', 'desc')
      //                                   ->orderBy('advertisements.hierarchy_order', 'asc')
      //                                   ->orderBy('advertisements.created_at', 'desc')
      //                                  ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

      $raw_bids = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','users.usertype_id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at','advertisements.hierarchy_order', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                     
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        // ->where('advertisements.feature', '!=', 1)
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')->get();

      $raw_bids_featured = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','users.usertype_id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at','advertisements.hierarchy_order', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                     
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->where('advertisements.feature', 1)
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get()->merge($raw_bids)->unique('id');

      // $always_first_bids = new Collection;
      // $remaining_bids = new Collection; 
      // $re_sort_bids = $raw_bids->get();
      // foreach($re_sort_bids as $val) {
      //   if($val->feature == 1) {
      //     $always_first_bids->add($val);
      //   } else {
      //     $remaining_bids->add($val);
      //   }
      // }
       
      // $merged_bids = $always_first_bids->merge($remaining_bids);
      $this->data['total_bids_count'] = count($raw_bids_featured);
      $this->data['adslatestbidsmobile'] = $raw_bids_featured->take(4);
      $this->data['adslatestbids'] = $raw_bids_featured->take(8);




     $this->data['adslatestbids_panel2'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','users.usertype_id','advertisements.title', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->skip(4)->take(4)->get();

                                        // dd($this->data['adslatestbids_panel2']);
      $this->data['adslatestbids_panel3'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.usertype_id','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')  
                                                   ->where('advertisements.status', '!=', '4')                                                                           
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get();


      $raw_ads = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.currency','advertisements.hierarchy_order', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.usertype_id','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->groupBy('advertisements.id')
                                        // ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get();

       $raw_ads_featured = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.currency','advertisements.hierarchy_order', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.usertype_id','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->where(function($query) use ($sess_master_country,$countries) {
                                          if(in_array($sess_master_country,$countries)) {
                                            $query->where('advertisements.country_code', '=', $sess_master_country);
                                          }
                                        })
                                        ->where('advertisements.feature', 1)
                                        ->groupBy('advertisements.id')
                                        // ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get()->merge($raw_ads)->unique('id');


        // $always_first_ads = new Collection;
        // $remaining_ads = new Collection; 
        // $re_sort_ads = $raw_ads->get();
        // foreach($re_sort_ads as $val) {
        //   if($val->feature == 1) {
        //     $always_first_ads->add($val);
        //   } else {
        //     $remaining_ads->add($val);
        //   }
        // }
         
        // $merged = $always_first_ads->merge($remaining_ads);
      $this->data['total_ads_count'] = count($raw_ads_featured);
       $this->data['latestadsmobile'] = $raw_ads_featured->take(4);
       $this->data['latestads'] = $raw_ads_featured->take(8);
       


       $count_ads =   Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.usertype_id','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->get(); 

      $count_bids = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.usertype_id','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.feature', 'desc')
                                        ->orderBy('advertisements.created_at', 'desc')
                                        ->orderBy('advertisements.user_plan_id', 'desc')
                                        ->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies);

       $this->data['max_ads_count'] = count($count_ads);
       $this->data['max_bids_count'] = count($count_bids);
                                  
       $this->data['latestads_panel2'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                       ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','users.usertype_id','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')
                                       ->skip(8)
                                       ->take(8)
                                       ->get();                                        
//dd( $this->data['latestads2']);

       $this->data['adslatestbuysellxs'] = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                       ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->orderBy('advertisements_photo.created_at', 'desc')
                                       ->take(2)
                                       ->get();


          if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
          }else{
            $selectedCountry  = Session::get('selected_location');
          } 

          $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first();
          

          $getBannerPlacementPlan = BannerPlacementPlans::where('status',1)
                                                        ->where('location_id',1)
                                                        ->lists('id');
 
          //15 placements                                     

          $jumboPaidBanners = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

          

          $jumboPaidBannersGetIds = BannerAdvertisementSubscriptions::where('page',1)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();
                                                         
          $getCountryBannerPlacementPlan = CountryBannerPlacement::where('status',1)
                                                                 ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                                if($selectedCountry != "ALL"){
                                                                                   $query->where('country_id','=',$getCountryCode->id);
                                                                                }else{
                                                                                  $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                   $query->where('country_id','=',$getDefaultCountry->id);
                                                                                 }       
                                                                       })
                                                                    ->where('reserve_status',1)
                                                                    ->groupBy('placement_id')
                                                                    ->get();

                                                              

          $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',1)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($jumboPaidBanners))
                                                               ->lists('id')
                                                               ->toArray();
          


          $merge_array = array_merge(array_filter($jumboPaidBanners),array_filter($getBannerDefaultsPerCountry));

           if($selectedCountry != "ALL"){
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
                    }
              }else{
                 $getDefaultCountry = Country::where('countryCode','US')->first(); 
                 $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->get();
                    if(count($checKBannerPerCountry) > 0){
                        BannerAdvertisementMerge::where('country_id',$getDefaultCountry->id)->delete();
                    }
            }       
          
        

          if(!is_null($jumboPaidBannersGetIds)){

              foreach ($jumboPaidBannersGetIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                if (!$if_exist) {
                  $saveBanners = new BannerAdvertisementMerge;       
                  $saveBanners->banner_id = $field;
                  $saveBanners->country_id = $getCountryCode->id;
                  $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                  $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                  $saveBanners->position = $getBannerPosition->position;
                  $saveBanners->order_no = $getBannerPosition->order_no;
                  $saveBanners->url = $getBannerDetails->target_url;
                  $saveBanners->designation = 1;
                  $saveBanners->type = 1;
                  $saveBanners->save();
                }
              }
             
          }

          if(!is_null($getBannerDefaultsPerCountry))
          {

              foreach ($getBannerDefaultsPerCountry as $key => $field) {

               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                  if (!$if_exist) {
                    $saveBanners = new BannerAdvertisementMerge;       
                    $saveBanners->banner_id = $field;
                    if($selectedCountry != "ALL"){
                       $saveBanners->country_id = $getCountryCode->id;
                    }else{
                       $getDefaultCountry = Country::where('countryCode','US')->first(); 
                       $saveBanners->country_id = $getDefaultCountry->id;
                    }

                    $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                    $saveBanners->position = $getBannerPosition->position;
                    $saveBanners->order_no = $getBannerPosition->order_no;
                    $saveBanners->url = $getBannerDetails->target_url;
                    $saveBanners->designation = 1;
                    $saveBanners->type = 2;
                    $saveBanners->save();
                  }
              }
          }
                                                   
        

          //Types  
          // 1 for paid banners and 2 for Default

          $this->data['get_banner_jumbo_main'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })                                                                          ->where('position',1)
                                                                          ->where('designation',1)
                                                                          ->orderBy('order_no','asc')
                                                                          ->get();

          $this->data['get_banner_jumbo_sub_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',2)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();

          $this->data['get_banner_jumbo_sub_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                             if($selectedCountry != "ALL"){
                                                                                         $query->where('country_id','=',$getCountryCode->id);
                                                                                      }else{
                                                                                         $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                                         $query->where('country_id','=',$getDefaultCountry->id);
                                                                                      }       
                                                                             })
                                                                            ->where('position',3)
                                                                            ->where('designation',1)
                                                                            ->orderBy('order_no','asc')
                                                                            ->get();
      


      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::where('status',1)->get();
      $this->data['usertypes'] = Usertypes::where('type',1)->where('status',1)->get();
  
     
   
      $this->data['categories'] = Category::where(function($query) use ($selectedCountry){
                                                    if($selectedCountry != "ALL"){
                                                       $query->where('country_code','=',$selectedCountry);
                                                    }else{
                                                        $query->where('country_code','=','us');
                                                    }       
                                           }) 
                                           ->where('home',2)
                                           ->where('status',1)
                                           ->groupBy('name')
                                           ->get();

      $get_key =  SettingForSiteKey::orderBy('created_at','desc')->first();
      Session::put('siteKey',$get_key->site_key);
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      // dd($this->data['notifications_counter']);
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();

      
      $this->data['country_code_session'] = Session::get('default_location');

      
      return view('ads.main', $this->data);
  }

  public function editor()
  {
    return View::make('admin.ads-management.front-end-users.ckfinder.index'); 
    return view('plugins/ckfinder');  
  }

  public function bidsPagination(){

    $per = Request::input('per');

    
    $raw = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id','advertisements.hierarchy_order', 'advertisements.price', 'advertisements.currency', 'advertisements.id','advertisements.feature', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.usertype_id','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->where(function($query){
                                             if(Session::get('default_location') != 'ALL'){
                                              $query->where('advertisements.country_code','=',Session::get('default_location'));
                                             }
                                       })
                                        ->groupBy('advertisements.id')
//                                         ->orderBy('advertisements.feature','desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc');
    $always_first = new Collection;
    $remaining = new Collection; 
    $re_sort = $raw->get();
//     dd($re_sort);
    foreach($re_sort as $val) {
      if($val->feature == 1) {
        $always_first->add($val);
      } else {
        $remaining->add($val);
      }
      $val->approved_rating = ($val->getAdRatings->avg('rate') == null ? 0 : $val->getAdRatings->avg('rate'));
    }
     
    $merged = $always_first->merge($remaining);
    $last_id = $merged->last()->id;
    $rows = $merged->take($per);

    if(count($rows)>0){
       $result['rows'] = $rows; 
       $result['per'] = $per + 4;
       $result['is_last'] = $rows->contains('id', $last_id);
    }else{
       $result['rows'] = null;
    }
    
    $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
    return Response::json($result);
  }

   public function adsPagination(){
    // $last_id  = Request::input('last_id');
    $per = Request::input('per');

    
    $raw = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id','advertisements.hierarchy_order', 'advertisements.price', 'advertisements.currency', 'advertisements.id','advertisements.feature', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.usertype_id','countries.countryName','users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '1');                                       
                                        })
                                       ->where(function($query){
                                             if(Session::get('default_location') != 'ALL'){
                                              $query->where('advertisements.country_code','=',Session::get('default_location'));
                                             }
                                       })
                                        ->groupBy('advertisements.id')
//                                         ->orderBy('advertisements.feature','desc')
                                        ->orderBy('advertisements.hierarchy_order', 'asc')
                                        ->orderBy('advertisements.created_at', 'desc');
    $always_first = new Collection;
    $remaining = new Collection; 
    $re_sort = $raw->get();
    
    foreach($re_sort as $val) {
      if($val->feature == 1) {
        $always_first->add($val);
      } else {
        $remaining->add($val);
      }
      $val->approved_rating = ($val->getAdRatings->avg('rate') == null ? 0 : $val->getAdRatings->avg('rate'));
    }
     
    $merged = $always_first->merge($remaining);
    $last_id = $merged->last()->id;
    $rows = $merged->take($per);

    if(count($rows)>0){
       $result['rows'] = $rows; 
       $result['per'] = $per + 4;
       $result['is_last'] = $rows->contains('id', $last_id);
    }else{
       $result['rows'] = null;
    }
//      dd($result);
               
    return Response::json($result);
  }


  public function addWatchlist() {
     $row = AdvertisementWatchlists::find(Request::input('id'));
     $id = Request::input('id');
      if(is_null($row)){
        $row = new AdvertisementWatchlists;
        $row->user_id = Auth::user()->id;
        $row->ads_id = $id;
        $row->save();
        return Response::json(['body' => 'Ad Successfully added to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

  public function removeWatchlist() {
     $input = Input::all();
     $id = array_get($input, 'watchlist_id');
     $row = AdvertisementWatchlists::find($id);

      if(!(is_null($row))){
        $row->status = 2;
        $row->save();

        return Response::json(['body' => 'Ad Successfully removed to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }


  public function getCity() {
        // dd(Request::all());
        $getLocalization = Country::where('id',Request::input('id'))
                                   ->where('status',1)
                                   ->first();
         if(!is_null($getLocalization)){
              $fetchLocalization = Localization::where('id',$getLocalization->localization)
                                               ->where('status',1)
                                               ->first();
         } 
        if(!is_null($fetchLocalization)){
             $result['client_localization'] = $fetchLocalization->name;
        }
        $get_city = City::where('country_id', '=', Request::input('id'))
                        ->where('status',1)
                        ->orderBy('name','asc')
                        ->get();

        $rows = '<option class="hide" value="">Select:</option>';
        foreach ($get_city as $key => $field) {
          $rows .= '<option data-slug="'.$field->slug.'" value="' . $field->id . '">' . $field->name . '</option>';
        }

//         $get_tel_prefix = CountryCode::where('country_id',Request::input('id'))->first();
        $get_tel_prefix = CountryDetails::where('iso', $getLocalization->countryCode)->first();

        if(!is_null($get_tel_prefix)){
//            $result['tel_prefix'] = $get_tel_prefix->country_code;
           $result['tel_prefix'] = '+'.$get_tel_prefix->phonecode;
        }
        $result['rows'] = $rows;
        return Response::json($result);

  }
  public function getCityforSearch() {
        $get_city = City::where('country_id', '=', Request::input('id'))->orderBy('name','asc')->get();
       
        $rows = '<option class="hide" value="">Select:</option>';
        $rows = '<option value="all">All Cities</option>';
        foreach ($get_city as $key => $field) {
          $rows .= '<option data-slug="'.$field->slug.'" value="' . $field->id . '">' . $field->name . '</option>';
        }

        $get_tel_prefix = CountryCode::where('country_id',Request::input('id'))->first();
        if(strlen($get_city) == 0){
          $result['rows'] = "";
        }else{ 
          $result['rows'] = $rows;
        }
        if(strlen( $get_tel_prefix) == 0){
           $result['tel_prefix'] = "";
        }else{
           $result['tel_prefix'] = $get_tel_prefix->country_code;
        }
       
        
        return Response::json($result);

  }
  public function getCustomAttributes(){

       $id = 1;
       $get_cust_id= CustomAttributes::find($id);
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$get_cust_id->id)->get();
       $rows = '<label>'; 
           foreach ($get_sub_attri_id as $key => $field) {
                      $rows .= $field->name.'</label>';
                      $rows .= '<option class="hide">Select:</option>'; 
                    $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                       $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }     
          }

//dd($get_sub_attri_id);
                            
        return Response::json($rows);

    }

}
