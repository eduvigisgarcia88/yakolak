<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\UserPlanTypes;
use App\UserAdComments;
use App\UserAdCommentReplies;
use App\Usertypes;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Mail;
use Form;
use App\NewsLetters;
use Carbon\Carbon;
use App\Advertisement;


class NewsLetterController extends Controller
{
   public function FetchAdCreation(){
        $this->data['title'] = "My Ads"; 
        $this->data['name'] = Auth::user()->name;
        $this->data['title'] = "Pogi ako";
        $this->data['aw'] = "awwww";
        $this->data['secret_code'] = "malakas si alkein";

       $this->data['newsletter'] = NewsLetters::where('id', '=', '1')->first();
       $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      return view('email.ad.creation', $this->data);

   }

}