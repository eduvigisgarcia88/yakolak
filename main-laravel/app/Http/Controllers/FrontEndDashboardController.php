<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Advertisement;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\Usertypes;
use App\AdvertisementPhoto;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\Category;
use App\AdRating;
use App\UserAdComments;
use App\SMSLog;
use Session;
use Input;

class FrontEndDashboardController extends Controller
{
  
	public function index(){

    $this->data['vendorads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category', '=', 'Buy and Sell')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get();

    $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                                     
		$this->data['title'] = "Dashboard";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    return View::make('user.dashboard.index', $this->data);
	}
	public function getMessages(){

		$this->data['title'] = "Dashboard-Messages";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();

    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                     })->count();  

     $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count(); 


    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    
    return View::make('user.dashboard.messages', $this->data);
	}

  public function SMSNotify($message, $to){
      $url = 'https://rest.nexmo.com/sms/json?' . http_build_query(
          [
            'api_key' =>  env('NEXMO_KEY'),
            'api_secret' => env('NEXMO_SECRET'),
            'to' => $to,
            'from' => 'YAKOLAK',
            'text' => $message
          ]
      );
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $nexmo_response = curl_exec($ch);
      curl_close($ch);
      $nexmo_result = json_decode($nexmo_response);
      $nexmo_status = $nexmo_result->messages[0]->status;
      if($nexmo_status == 0)
      {
        $sms_log = new SMSLog;
        $sms_log->user_id = Auth::user()->id;
        $sms_log->message_id = $nexmo_result->messages[0]->{'message-id'};
        $sms_log->message_price = $nexmo_result->messages[0]->{'message-price'};
        $sms_log->message_body = $message;
        $sms_log->user_mobile = $to;
        $sms_log->sms_status = $nexmo_status;
        $sms_log->delivery_status = 'sent';
        $sms_log->remaining_balance = $nexmo_result->messages[0]->{'remaining-balance'};
        $sms_log->sms_network = $nexmo_result->messages[0]->network;
        $sms_log->sms_type = 'verification';
        $sms_log->error_text = NULL;
        $sms_log->save();
      }
      
      else
      {
        $sms_log = new SMSLog;
        $sms_log->user_id = Auth::user()->id;
        $sms_log->message_id = NULL;
        $sms_log->message_price = NULL;
        $sms_log->message_body = $message;
        $sms_log->user_mobile = $to;
        $sms_log->sms_status = $nexmo_status;
        $sms_log->delivery_status = 'failed';
        $sms_log->remaining_balance = NULL;
        $sms_log->sms_network = NULL;
        $sms_log->sms_type = 'verification';
        $sms_log->error_text = $nexmo_result->messages[0]->{'error-text'};
        $sms_log->save();
      }
      return $nexmo_response;
    }

    public function getSMSBalance(){
      $url = 'https://rest.nexmo.com/account/get-balance/'.env('NEXMO_KEY').'/'.env('NEXMO_SECRET');
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $nexmo_balance = curl_exec($ch);
      curl_close($ch);
      $nexmo_result = json_decode($nexmo_balance);
      $nextmo_remaining_balance = $nexmo_result->value;
      return $nextmo_remaining_balance;
    }

    public function CodeGenerator()
    {
      $pool = '23456789ABCDEFGHJKMNOPQRSTUVWXYZ';
      return substr(str_shuffle(str_repeat($pool, 4)), 0, 4);
    }

    public function newSMSCode()
    {
      Session::forget('verify');
      $mobile_number = Input::get('mobile_number');
      $code = $this->CodeGenerator();
      Session::set('verify', $code);
      $message = 'Verification code for Yakolak: '.$code;
      $send = $this->SMSNotify($message,$mobile_number);
      $nexmo_result = json_decode($send);
      $nexmo_status = $nexmo_result->messages[0]->status;
      if($nexmo_status == 0){
        return 1;
      }
      else
      {
        return 0;
      }
    }

    public function sendSMSCode()
    {
      Session::forget('verify');
      $mobile_number = Input::get('mobile_number');
      $code = $this->CodeGenerator();
      Session::set('verify', $code);
      Session::set('user_mobile_no',$mobile_number);
      $message = 'Verification code for Yakolak: '.$code;
      $send = $this->SMSNotify($message,$mobile_number);
      $nexmo_result = json_decode($send);
      $nexmo_status = $nexmo_result->messages[0]->status; // using [0] is trusting the first result
      if($nexmo_status == 0)
      {
        
        $err = "<div class='alert alert-danger alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Code not matched!</strong> Please check it again</div>";

        return '<form role="form" class="form-horizontal" id="sms_verify_codex"><p>Enter your code:</p>
        
        <span>
        <div class="input-group">
          <input type="text" class="code_verify form-control borderZero" name="code_verify" required="required">
          <div class="input-group-btn">
          <button type="submit" class="sms_code_btn btn blueButton borderZero">Verify <i></i></button>
          <button type="button" class="newSMSCode btn redButton leftMargin borderZero">New code <i></i></button>
          </div>
        </div>
        </span></form><script>
$("#sms_verify_codex").submit(function(e){
  e.preventDefault();
  var code_verify = $(".code_verify").val();
  var _token = "' . csrf_token() . '";
  var new_btn_text = $("button.newSMSCode").text();
  $.ajax({
    type: "POST",
    url: "' . url('dashboard/sms-verify-code') . '",
    data: {code_verify: code_verify, _token: _token},
    beforeSend: function(){$("button.sms_code_btn").html("Verifying <i></i>").attr("disabled","disabled").children().addClass("fa fa-spinner fa-spin");$("button.newSMSCode").attr("disabled","disabled");},
    complete: function(data){if(data.responseText == 1){setTimeout(function(){window.location.reload();},2500);}},
    success: function(data){if(data == 1){$("button.sms_code_btn").html("Verified <i></i>").attr("disabled","disabled").children().attr("class","fa fa-check");}else if(data == 0){$("button.sms_code_btn").html("Verify <i></i>").removeAttr("disabled").parent().parent().parent().parent().prepend("'. $err .'");if(/sent/i.test(new_btn_text)){$("button.newSMSCode").attr("disabled","disabled");}else{$("button.newSMSCode").removeAttr("disabled");}}}
  });
});
$(".newSMSCode").click(function(){
var _token = "' . csrf_token() . '";
var mobile_number = "' . Session::get('user_mobile_no') . '";
var btn = $(this);
$.ajax({
type: "POST",
url:"' . url('dashboard/sms-new-code') . '",
data: {mobile_number: mobile_number, _token: _token},
beforeSend: function(){btn.html("Sending <i></i>").attr("disabled","disabled").children().addClass("fa fa-spinner fa-spin");$("button.sms_code_btn").attr("disabled","disabled")},
complete: function(data){$("button.sms_code_btn").removeAttr("disabled");if(data.responseText == 1){startTimer();}else{btn.html("Failed. <i></i>").children().addClass("fa fa-times");}},
success: function(data){console.log("Done");},
});
});
</script>';
      }
      else if($nexmo_status == 1 || 4 || 5 || 8 || 11 || 12 || 13 || 14 || 16 || 23)
      {
        return 'SMS Account Verification is not available at the moment. Try again later.';
      }

      else if($nexmo_status == 6)
      {
        return 'Mobile number format is invalid.';
      }

      else if($nexmo_status == 7)
      {
        return 'This mobile number has been blacklisted in our system.';
      }
      else
      {
        return 'Something went wrong. Try again later.';
      }

    }

    public function smsVerifyCode()
    {
      $user_code = Input::get('code_verify');
      $session_code = Session::get('verify');
      if($session_code == $user_code)
      {
        $row = User::where('id','=',Auth::user()->id)->first();
        $row->sms_verification_status = 1;
        $row->mobile = Session::get('user_mobile_no');
        $row->save(); 
        return 1;
      }
      else
      {
        return 0;
      }
    }




	public function getBids(){

		$this->data['title'] = "Dashboard-Bids";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();                   
    return View::make('user.dashboard.bids', $this->data);
	}
	public function getListings(){


    $this->data['vendorads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 

    $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city')                        
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
     
    $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

    $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                      ->where(function($query) {
                                            $query->where('advertisements_photo.primary', '=', '1')
                                                 ->where('advertisements.status', '=', '1');                                
                                        })->skip(4)->take(4)->get(); 
    $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                           
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

    $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


     if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();                                      
		$this->data['title'] = "Dashboard-Listings";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    
    return View::make('user.dashboard.listings', $this->data);
	}
	public function getWatchlist(){

		$this->data['title'] = "Dashboard-WatchList";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
    return View::make('user.dashboard.watchlist', $this->data);
	}
	public function getSetttings(){

		$this->data['title'] = "Dashboard-Settings";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
  
    return View::make('user.dashboard.settings', $this->data);
	}
	
 } 