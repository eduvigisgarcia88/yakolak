<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use App\Advertisement;
use App\Category;
use App\SettingForSiteKey;
use App\APIKeys;
use App\ReserveVanity;
use App\ContactDetails;

class SettingsController extends Controller
{
	public function save(){
		 $new = true;
        $input = Input::all();
        if(array_get($input, 'id')) {
            // get the user info
            $row = SettingForSiteKey::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
         $rules = [
             'site_key'                    => 'required',
             'secret_key'                    => 'required',
          
        ];
        // field name overrides
        $names = [   
            'site_key'                    => 'Site Key',
            'secret_key'                    => 'Secret Key',
        ];  

           // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        if($new) {
          $row = new SettingForSiteKey;
        }
          $row->site_key = array_get($input,'site_key');
          $row->secret_key = array_get($input,'secret_key');
          $row->save();

        if($new){
               return Response::json(['body' => "Recaptcha Settings created"]);
        } else {
               return Response::json(['body' => "Recaptcha Settings updated"]);
        }

	}



    public function ListConfiguration()
    {
      $not_allowed = array(1,2);
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return response()->view('errors.denied', [], 401);
      }

      $this->data['apis'] = APIKeys::get();
      $this->data['vanity'] = ReserveVanity::where('status',1)->get();
      $this->data['contact_details'] = ContactDetails::get();
      if(request()->method() == 'GET')
      {
        return view('admin.api-management.list',$this->data);
      }
      else
      {
        return $this->data;
      }
    }

    public function EditConfiguration()
    {
      $input = Input::except('_token');
      $keys = $input['api_key'];
      $secrets = $input['api_secret'];
      $names = $input['api_name'];
      $api_ids = $input['api_id'];
      $ids = $input['id'];

      try
      {
        foreach($ids as $key => $data)
        {
          $edit_api = APIKeys::where('id',$data)->first();
          $edit_api->name = $names[$key];
          $edit_api->api_secret = $secrets[$key];
          $edit_api->api_key = $keys[$key];
          $edit_api->api_id = $api_ids[$key];
          $edit_api->save();
        }

        $response['message'] = 'API Successfully updated.';
        $response['code'] = 1;
        return $response;
      }

      catch(\Exception $e)
      {
        $response['code'] = 0;
        $response['message'] = $e->getMessage();
        return $response;
      }

      // try
      // {
      //   if(request()->new == 1)
      //   {
      //     $edit_api = new APIKeys;
      //     $edit_api->api_id = strtoupper(md5(microtime()));
      //     $response['message'] = 'API Successfully added.';
      //   }
      //   else
      //   {
      //     $edit_api = APIKeys::where('id',request()->id)->first();
      //     $response['message'] = 'API Successfully updated.';
      //   }
      //   $edit_api->name = request()->api_name;
      //   $edit_api->api_secret = request()->api_secret;
      //   $edit_api->api_key = request()->api_key;
      //   $edit_api->save();
      //   $response['code'] = 1;
      //   return $response;
      // }
      // catch(\Exception $e)
      // {
      //   $response['code'] = 0;
      //   $response['message'] = $e->getMessage();
      //   return $response;
      // }
    }

    public function CollectConfiguration()
    {
      $vanity_edit = ReserveVanity::where('status',1)->where('id',request()->id)->first();
      return $vanity_edit;
    }

    public function ReserveVanity()
    {
      $messages = [
          'vanity.min' => 'Vanity URL must be atleast 4 characters long.',
          'vanity.required' => 'Vanity URL is required.',
          'vanity.unique' => 'Vanity URL has already been taken.',
          'vanity.max' => 'Vanity URL must be maximum of 10 characters.',
          'vanity.regex' => 'Vanity URL must not have spaces.',
          'description.max' => 'Maximum of 255 characters only.'
      ];
      $validator = Validator::make(request()->all(), [
          'vanity' => 'required|regex:/^\S*$/u|min:4|max:20|unique:users,alias|unique:reserved_vanity,vanity,'.(request()->{'vanity-indicator'} == 1 ? '' : request()->id ),
          'description' => 'max:255'
      ],$messages);
      if ($validator->fails())
      {
          $response['errors'] = $validator->errors()->all();
          $response['code'] = 0;
          return $response;
      }
      else
      {
        try
        {
          if(request()->{'vanity-indicator'} == 1)
          {
            $edit_vanity = new ReserveVanity;
            $edit_vanity->status = 1;
            $response['message'] = request()->vanity.' added.';

          }
          else
          {
            $edit_vanity = ReserveVanity::where('id',request()->id)->first();
            $response['message'] = request()->vanity.' updated.';
          }
          $edit_vanity->vanity = request()->vanity;
          $edit_vanity->description = request()->description;
          $edit_vanity->save();
          $response['code'] = 1;
          return $response;
        }
        catch(\Exception $e)
        {
          $response['code'] = 0;
          $response['message'] = $e->getMessage();
          return $response;
        }
      }
    }

    public function DeleteVanity()
    {
      $delete_vanity = ReserveVanity::where('id',request()->id)->first();
      Session::set('deleted_vanity',$delete_vanity->vanity);
      $delete_vanity->delete();
      return Response::json(['body' => [ Session::get('deleted_vanity').' has been removed.' ]]);
    }


    public function EditContact()
    {
      $messages = [
          'email.required' => 'error-email|Email is required.',
          'email.email' => 'error-email|Email is invalid.',
          'description.required' => 'error-description|Description is required',
          'description.min' => 'error-description|Description must be atleast 10 characters long',
          'address.required' => 'error-address|Google Map is required.',
          'address.min' => 'error-address|Google Map must be atleast 5 characters long.',
					'address_line.required' => 'error-address_line|Address is required.',
          'address_line.min' => 'error-address_line|Address must be atleast 5 characters long.',
					'city.required' => 'error-city|City is required.',
					'tel.required' => 'error-tel|Telephone is required.',
					'tel.numeric' => 'error-tel|Telephone must be numeric.',
					'zip.required' => 'error-zip|Zip code is required.'
      ];
      $validator = Validator::make(request()->all(), [
          'email' => 'required|email|',
          'description' => 'required|min:10',
          'address' => 'required|min:5',
					'address_line' => 'required|min:5',
					'city' => 'required',
					'zip' => 'required',
					'tel' => 'required|numeric'
      ],$messages);
      if ($validator->fails())
      {
          $response['errors'] = $validator->errors()->all();
          $response['code'] = 0;
          return $response;
      }
      else
      {
        try
        {
          $edit_contact = ContactDetails::where('id',request()->id)->first();
          $edit_contact->email = request()->email;
          $edit_contact->address = request()->address;
					$edit_contact->address_line = request()->address_line;
					$edit_contact->city = request()->city;
					$edit_contact->zip = request()->zip;
					$edit_contact->tel = request()->tel;
          $edit_contact->description = request()->description;
          $edit_contact->save();
          $response['code'] = 1;
          $response['message'] = 'Contact Details successfully updated.';
          return $response;
        }
        catch(\Exception $e)
        {
          $response['code'] = 0;
          $response['message'] = $e->getMessage();
          return $response;
        }
      }

    }

}