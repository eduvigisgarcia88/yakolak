<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use App\Country;
use App\City;
use App\CompanyBranch;
use App\AdRating;
use App\UserAdComments;
use App\Advertisement;
use App\Newsletters;
use Mail;

class UserController extends Controller
{
   public function index(){
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/users/refresh');
   	  $this->data['title'] = "User Management - Front-End Users";
      $this->data['usertypes']  = Usertypes::where('type',1)->get();
      $this->data['branch'] = "";
      $this->data['country'] = Country::where('status',1)->orderBy('countryName','asc')->where('id','!=','20')->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.user-management.front-end-users.list', $this->data);
   }
   public function edit(){

    $row = User::with('getUserCompanyBranch')->find(Request::input('id'));
    $get_city = City::all();
      $city ='';
      foreach ($get_city as $key => $field) {
        $city .= '<option value="'.$field->id.'"'.($field->id == $row->city ? 'selected' : '').' >'.$field->name.'</option>';
      }
       //dd($rows);
    $get_countries = Country::all();
    // $get_cities = City::all();
    // dd($rows);
    $branch = "";
    if (count($row->getUserCompanyBranch) > 0) {
      $ctr = 0;      
      foreach ($row->getUserCompanyBranch as $key => $field) {
         if($field->status == 1){
         $branch .= '<div id ="accountBranch"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 borderbottomLight normalText noPadding bottomPadding topMarginB bottomMarginB font-color">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText">' . ($ctr == 0 ? '<button type="button" value="" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground font-color"><i class="fa fa-plus"></i> ADD BRANCH</button>' : '<button type="button" value="" class="hide-view btn-del-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-minus font-color"></i> REMOVE</button>') . '</span>' .
            '</div>' .
            '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="'.$field->company_tel_no.'" class="form-control borderZero inputBox fullSize disable-view" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize disable-view">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as $gckey => $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '" ' . ($field->company_country == $gcfield->id ? 'selected' : '') . '>' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="'.$field->company_address.'" class="form-control borderZero inputBox fullSize disable-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="'.$field->company_office_hours.'" class="form-control borderZero inputBox fullSize disable-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="'.$field->company_email.'" class="form-control borderZero inputBox fullSize disable-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="'.$field->company_mobile.'" class="form-control borderZero inputBox fullSize disable-view" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize disable-view ">' .
                    '<option class="hide">Select</option>';

                    $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get()->take(50);
                      // dd($get_cities);
                    foreach ($get_cities as $gkey => $gfield) {
                      
                      $branch .= '<option value="' . $gfield->id . '" ' . ($field->company_city == $gfield->id ? 'selected' : '') .'>' . $gfield->name . '</option>';

                    }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="'.$field->company_website.'" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id" value="'.$field->id.'">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status" value="'.$field->status.'">'.
          '</div></div>';

          $ctr += 1;
      }
    }
    }
    else {
      $branch .= '<div id ="accountBranch"><div class="col-lg-12  borderbottomLight normalText noPadding bottomPadding topMarginB bottomMargin font-color">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText"><button type="button" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus font-color"></i> ADD BRANCH</button></span>' .
            '</div>' .
            '<div class="col-lg-6 " style="padding-right:50px;">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as  $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '">' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 " style="padding-left:51px;">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
               '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';

                    // $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get();
                    //   // dd($get_cities);
                    // foreach ($get_cities as $gkey => $gfield) {
                      
                    //   $branch .= '<option value="' . $gfield->id . '">' . $gfield->name . '</option>';

                    // }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status">' .
          '</div></div>';
    }
    // $get_branch = CompanyBranch::where('user_id',Request::input('id'))->get();

    //   $branch = '';

    //     if(count($get_branch) > 0){
    //         foreach ($get_branch as $key => $field) {
    //         $branch .= '';

    //         }
    //     }else{
    //         $branch .='';
    //     }
       $result['branch']  = $branch;
       $result['city']  = $city;
       $result['row']  = $row;
       // dd($branch);
    return Response::json($result);
   }

   public function doList() {
      $result['sort'] = Request::input('sort') ?: 'users.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $country = Request::input('country') ? :'';
      $usertype_id = Request::input('usertype_id') ? : '';
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

              $rows = User::leftjoin('usertypes','usertypes.id','=','users.usertype_id')
                            ->leftjoin('user_plan_types','user_plan_types.id','=','users.user_plan_types_id')
                            ->select('users.*','user_plan_types.plan_name','usertypes.type_name')
                            ->selectRaw("(SELECT COUNT(*) as flagged
                              FROM yakolak_advertisements
                              WHERE user_id = yakolak_users.id) as ad_listings")
                            ->selectRaw("(SELECT COUNT(*) as flagged
                              FROM yakolak_advertisements
                              WHERE status = 6
                              AND user_id = yakolak_users.id) as ad_flagged")
                             ->where(function($query) {
                              $query->where('users.usertype_id','=',1)
                                    ->orWhere('users.usertype_id','=',2);
                            })
                            ->where(function($query) use ($search) {
                              $query->where('users.username', 'LIKE', '%' . $search . '%')
                                    ->orWhere('plan_name', 'LIKE', '%' . $search . '%')
                                    ->orWhere('type_name', 'LIKE', '%' . $search . '%')
                                    ->orWhere('users.email', 'LIKE', '%' . $search . '%');
                              })
                            ->where(function($query) use ($usertype_id) {
                                     $query->where('users.usertype_id','LIKE', '%' . $usertype_id . '%');    
                            })
                            ->where(function($query) use ($country) {
                                 $query->where('users.country','LIKE', '%' . $country . '%');    
                            })
                            ->where(function($query) {
                              $query->where('users.usertype_id','=',1)
                                    ->orWhere('users.usertype_id','=',2);
                            })
                           ->where('users.status','!=',4)
                           ->orderBy($result['sort'], $result['order'])
                           ->paginate($per);

                    
        

                         
      } else {
                  $count = User::select('users.*')->with('getUserType')
                                                  ->with('getUserFlaggedAds')
                                                  ->with('getUserPlanType')
                                                  ->with('getUserCountry')
                                                  ->with('getUserListings')
                                          ->where(function($query) use ($status) {
                                              $query->where('users.usertype_id', '!=', '8')
                                                    ->where('status', 'LIKE', '%' . $status . '%');
                                            })
                                          ->where(function($query) use ($search) {
                                            $query->where('username', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('email', 'LIKE', '%' . $search . '%');
                                            })
                                          ->where(function($query) use ($usertype_id) {
                                            $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                                  
                                            })
                                           ->where(function($query) {
                                            $query->where('users.usertype_id','=',1)
                                                  ->orWhere('users.usertype_id','=',2);
                                          })
                                          ->where('status','!=',4)
                                          // ->where('id','=',34)
                                          ->orderBy($result['sort'], $result['order'])
                                          ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

                    $rows = User::select('users.*')->with('getUserType')
                                                    ->with('getUserFlaggedAds')
                                                    ->with('getUserPlanType')
                                                    ->with('getUserCountry')
                                                    ->with('getUserListings')
                                            ->where(function($query) use ($status) {
                                                $query->where('users.usertype_id', '!=', '8')
                                                      ->where('status', 'LIKE', '%' . $status . '%');
                                              })
                                            ->where(function($query) use ($search) {
                                              $query->where('username', 'LIKE', '%' . $search . '%')
                                                    ->orWhere('email', 'LIKE', '%' . $search . '%');
                                              })
                                            ->where(function($query) use ($usertype_id) {
                                              $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                                    
                                              })
                                            ->where(function($query) {
                                              $query->where('users.usertype_id','=',1)
                                                    ->orWhere('users.usertype_id','=',2);
                                            })
                                            ->where('status','!=',4)
                                            // ->where('id','=',34)
                                            ->orderBy($result['sort'], $result['order'])
                                            ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
    public function doListBackEndUsers() {
      $result['sort'] = Request::input('sort') ?: 'users.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : '';
      // dd($usertype_id);
      $per = Request::input('per') ?: 100;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*')->with('getUserType')
                                        ->with('getUserFlaggedAds')
                                        ->with('getUserPlanType')
                                        ->with('getUserCountry')
                                        ->with('getUserListings')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('username', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');    
                                  })
                                ->where(function($query) {
                                  $query->where('usertype_id','=',3)
                                        ->orWhere('usertype_id','=',4);
                                })
                                ->where('status','!=',4)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

                              
      } else {
        $count = User::select('users.*')->with('getUserType')
                                        ->with('getUserFlaggedAds')
                                        ->with('getUserPlanType')
                                        ->with('getUserCountry')
                                        ->with('getUserListings')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('username', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->where(function($query) {
                                  $query->where('usertype_id','=',3)
                                        ->orWhere('usertype_id','=',4);
                                })
                                ->where('status','!=',4)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);


        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*')->with('getUserType')
                                        ->with('getUserFlaggedAds')
                                        ->with('getUserPlanType')
                                        ->with('getUserCountry')
                                        ->with('getUserListings')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('username', 'LIKE', '%' . $search . '%')
                                        ->orWhere('email', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->where(function($query) {
                                  $query->where('usertype_id','=',3)
                                        ->orWhere('usertype_id','=',4);
                                })
                                ->where('status','!=',4)
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
   public function indexBackend(){
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $result = $this->doListBackEndUsers();
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/users/back-end/refresh');
      $this->data['title'] = "User Management - Administrators";
      $this->data['usertypes']  = Usertypes::where('type',2)->get();
      $this->data['branch'] = "";
      $this->data['country'] = Country::orderBy('countryName','asc')->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.user-management.back-end-users.list', $this->data);
   }
   public function register() {

        if (!Auth::check()) {
           
            return view('auth/register');
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    public function doRegister(Request $request) {
    	
		$registerUser = new User;
		$registerUser->firstname = $request->input('firstname');
		$registerUser->lastname = $request->input('lastname');
		$registerUser->email = $request->input('email');
		$registerUser->username = $request->input('email');
		$registerUser->password =  Hash::make($request->password);
		$registerUser->remember_token = $request->input('_token');

		$registerUser->save();

    Session::flash('success', 'uploaded file is not valid');
    return Redirect::to('auth/register');
       
    }
     public function disable(){ //disable user
      $row = User::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = 2;
        $row->save();
        $get_user_advertisements = Advertisement::where('user_id',$row->id)->where('status',1)->get();
        foreach ($get_user_advertisements as $key => $field) {
           $update_ad = Advertisement::find($field->id);
           if($update_ad->ads_type_id == 1){
               $update_ad->status =3;
               $update_ad->save();
           }else{
               $update_ad->status =4;
               $update_ad->save();
           }
        
        }
        return Response::json(['body'=>'User has been disabled']);
      }else{
        return Response::json(['error'=>'Id not detected']);
      }
    }
     public function enable(){ //disable user
      if(!is_null(Request::input('id'))){
        User::where('id',Request::input('id'))->update(['status' => 1]);
        Advertisement::where('user_id',Request::input('id'))->update(['status' => 1]);
        return Response::json(['body'=>'User has been enabled']);
      }else{
        return Response::json(['error'=>'Id not detected']);
      }
    }
    public function  verifyEmail(){
        $validate = User::find(Request::input('id'));
        
              if(!is_null($validate)){
                $validate->verify_status = 2;
                $validate->save();
              }
              $contact_name = $validate->name ; 
              $user_name = $validate->username;
              $user_email = $validate->email;
              if ($validate->telephone) {
                  $user_phone =  $validate->telephone;
              } else {
                  $user_phone =  $validate->mobile;
              }  
           $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="'.url('/').'/register/verify/'.$validate->confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Verify your email address
              </a>
          </td>';
          
           $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
            if ($newsletter) {
                      Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $validate) {
                           $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

                       });

                return Response::json(['body'=>'Email verification Sent']);    
            }else{
                return Response::json(['error'=>'Error']);
            }
    
   }
    public function blockUser(){ //disable user
      if(!is_null(Request::input('id'))){
        User::where('id',Request::input('id'))->update(['status' => 3]);
        Advertisement::where('user_id',Request::input('id'))->update(['status' => 2]);
        return Response::json(['body'=>'User has been blocked']);
      }else{
        return Response::json(['error'=>'Id not detected']);
      }
    }
    public function unblockUser(){
      if(!is_null(Request::input('id'))){
        User::where('id',Request::input('id'))->update(['status' => 1]);
        Advertisement::where('user_id',Request::input('id'))->update(['status' => 1]);
        return Response::json(['body'=>'User has been unblocked']);
      }else{
        return Response::json(['error'=>'Id not detected']);
      }
    }
    public function removeUser(){
      $row = User::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = 4;
        $row->save();
        return Response::json(['body'=>'User has been removed']);
      }else{
        return Response::json(['error'=>'Id not detected']);
      }
    }

   public function getUserInfo(){
      $row = User::leftjoin('countries','countries.id','=','users.country')
                  ->select('countries.countryName','users.country')
                  ->with('getUserAdCount')
                  ->with('getUserBidCount')
                  ->find(Request::input('id'));

      $get_count = User::with('getUserAdCount')->with('getUserBidCount')->find(Request::input('id'));
      $get_count_inactive = User::with('getUserAdCountInactive')->with('getUserBidCountInactive')->find(Request::input('id'));
      $rows = '<div class="table-responsive"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
              '<thead>' . 
              // '<tr><th><small>Password</small></th>' .
              '<th> <small>Mobile No</small></th>' .
              '<th> <small>Phone No</small></th>' .
              '<th> <small>Country</small></th>' .
              '<th><center><small>Active Ads</small></center></th>'.
              '<th><center><small>Disable Ads</small></center></th>'.
              '<th><center><small>Active Auctions</small></center></th>'.
              '<th><center><small>Disable Auctions<small></center></th><tbody></tr>';


      $rows .= '<tr data-id='.$row->id.'>' .
                     // '<td><small>' .  $row->mobile. '</small></td>' .
                     '<td><small>' .($row->mobile == null ? 'N/A': $row->mobile). '</small></td>' .
                     '<td><small>' .($row->telephone == null ? 'N/A': $row->telephone).'</small></td>' .
                     '<td><small>' .($row->countryName == null ? 'N/A': $row->countryName). '</small></td>' .
                     '<td><center><small>' .count($get_count->getUserAdCount). '</small></center></td>' .
                     '<td><center><small>' .count($get_count->getUserAdCountInactive). '</small></center></td>' .
                     '<td><center><small>' .count($get_count->getUserBidCount) . '</small></center></td>'. 
                     '<td><center><small>' .count($get_count->getUserBidCountInactive) . '</small></center></td>'. 
                 '</tr>';
      $rows .= '</tbody></table></div>';

       return Response::json($rows);
  }
  public function getAdminInfo(){
      $row = User::leftjoin('countries','countries.id','=','users.country')
                  ->select('countries.countryName','users.country')
                  ->with('getUserAdCount')
                  ->with('getUserBidCount')
                  ->find(Request::input('id'));

      $get_count = User::with('getUserAdCount')->with('getUserBidCount')->find(Request::input('id'));
      $get_count_inactive = User::with('getUserAdCountInactive')->with('getUserBidCountInactive')->find(Request::input('id'));
      $rows = '<div class="table-responsive"><table class="table" style="margin-bottom:0px !important;">' .
              '<thead>' . 
              // '<tr><th><small>Password</small></th>' .
              '<th> <small>Mobile No</small></th>' .
              '<th> <small>Phone No</small></th>' .
              '<th> <small>Country</small></th>';


      $rows .= '<tr data-id='.$row->id.'>' .
                     // '<td><small>' .  $row->mobile. '</small></td>' .
                     '<td><center><small>' .($row->mobile == null ? 'N/A': $row->mobile). '</small></center></td>' .
                     '<td><center><small>' .($row->telephone == null ? 'N/A': $row->telephone).'</small></center></td>' .
                     '<td><center><small>' .($row->countryName == null ? 'N/A': $row->countryName). '</small></center></td>' .
                 '</tr>';
      $rows .= '</tbody></table></div>';

       return Response::json($rows);
  }
}

