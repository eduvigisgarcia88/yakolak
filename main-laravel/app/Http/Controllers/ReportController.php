<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
// use App\Http\Requests;
use Request;
use App\Http\Controllers\Controller;

use App\Country;
use Paginator;
use Response;
use Carbon\Carbon;
use Session;
use PDF;


class ReportController extends Controller
{
 	public function index(){
 	    $result = $this->doList();
	    $this->data['rows'] = $result['rows'];
     	$this->data['pages'] = $result['pages'];
	   	$this->data['title'] = "Report Management";
	   	$this->data['refresh_route'] = url("admin/report/refresh");
	   	$this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
	  	return View::make('admin.report-management.list', $this->data);
 	}
 	public function doList(){
		  	$result['sort'] = Request::input('sort') ?: 'created_at';
	      $result['order'] = Request::input('order') ?: 'asc';
	      $search = Request::input('search');
	      $status = Request::input('status');
	      $date_from = Request::input('date_from') ? Carbon::createFromFormat('m/d/Y', Request::input('date_from'))->toDateString().' 00:00:00' : '';
	      $date_to = Request::input('date_to') ? Carbon::createFromFormat('m/d/Y', Request::input('date_to'))->toDateString().' 23:59:59' : '';
	      $per = Request::input('per') ?: 10;

	      if (Request::input('page') != '»') {
	        Paginator::currentPageResolver(function () {
	            return Request::input('page'); 
	        });


	        $rows = User::where(function($query) use ($search) {
                               		   $query->where('name', 'LIKE', '%' . $search . '%');     
                                  })
											->where(function($query) use ($date_from, $date_to){
												if ($date_from && $date_to) {
													$query->whereBetween('created_at', [$date_from, $date_to]);
												}
											})
											->whereIn('usertype_id', [1,2])
	        				    ->orderBy($result['sort'], $result['order'])
	                    ->paginate($per);

	                            
	      } else {
	         $count =  User::where(function($query) use ($search) {
                               		   $query->where('name', 'LIKE', '%' . $search . '%');     
                                  })
	   									->where(function($query) use ($date_from, $date_to){
												if ($date_from && $date_to) {
													$query->whereBetween('created_at', [$date_from, $date_to]);
												}
											})
						 					->whereIn('usertype_id', [1,2])
	        				    ->orderBy($result['sort'], $result['order'])
	                    ->paginate($per);


	        Paginator::currentPageResolver(function () use ($count, $per) {
	            return ceil($count->total() / $per);
	        });

	      $rows =  User::where(function($query) use ($search) {
                               		   $query->where('name', 'LIKE', '%' . $search . '%');     
                                  })
	        				   	->where(function($query) use ($date_from, $date_to){
												if ($date_from && $date_to) {
													$query->whereBetween('created_at', [$date_from, $date_to]);
												}
											})
										->whereIn('usertype_id', [1,2])
	        				    ->orderBy($result['sort'], $result['order'])
	                    ->paginate($per);
	      }

	      // return response (format accordingly)
				
	      if(Request::ajax()) {
	          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows->toArray();
	          return Response::json($result);
	      } else {
	          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
	          $result['rows'] = $rows;
	          return $result;
	      }	
	}

	public function showPDF(){
		
		$date_from = Request::get('from') ? Carbon::createFromFormat('m-d-Y', Request::get('from'))->toDateString().' 00:00:00' : '';
	  $date_to = Request::get('to') ? Carbon::createFromFormat('m-d-Y', Request::get('to'))->toDateString().' 23:59:59' : '';
		$table_data = '';
		
		$rows = User::whereIn('usertype_id', [1,2])
								->where(function($query) use ($date_from, $date_to){
									if ($date_from && $date_to) {
										$query->whereBetween('created_at', [$date_from, $date_to]);
									}
								})
	        			->orderBy('created_at', 'desc')
								->get();
		
		foreach ($rows as $val) {
			$table_data .= '<tr>';
			$table_data .= '<td>'.$val->id.'</td>';
			$table_data .= '<td>'.$val->username.'</td>';
			$table_data .= '<td>'.$val->email.'</td>';
			$table_data .= '<td>'.$val->alias.'</td>';
			$table_data .= '<td>'.Carbon::createFromFormat('Y-m-d H:i:s', $val->created_at)->format('M d, y').'</td>';
			$table_data .= '</tr>';
		}
		
		$pdf = PDF::loadView('layout.user-report', compact('table_data'));
    $pdf->setPaper('A4', 'landscape');
    return $pdf->stream('user-registration-report.pdf');
		
	}

}