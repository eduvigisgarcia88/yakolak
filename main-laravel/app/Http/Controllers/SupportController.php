<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\AdRating;
use App\UserAdComments;
use App\Advertisement;
use App\ContactDetails;

class SupportController extends Controller
{

	public function index(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
	    $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
	                                                    $query->where('reciever_id','=',$user_id);
	                                                    })->where(function($status){
	                                                       $status->where('read_status','=',1);
	                                                    })->count();
	    $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        
		return View::make('support.list', $this->data);
	}

	public function showContactUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        
		return View::make('support.contact-us', $this->data);
	}

	public function showAboutUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
     	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
		return View::make('support.about-us', $this->data);
	}
	public function showCallUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
		return View::make('support.call-us', $this->data);
	}
	public function showFAQS(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      $master_poster_total = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                          ->where('ads_type_id',1)
                                          ->where('status',1)
                                          ->groupBy('user_id')
                                          ->orderBy('count', 'desc')
                                          ->take(4)
                                          ->get();
      $master_poster_ads = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                        ->where('ads_type_id',1)
                                        ->where('status',1)
                                        ->groupBy('user_id')
                                        ->orderBy('count', 'desc')
                                        ->take(4)
                                        ->get();
      $master_poster_auctions = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                              ->where('ads_type_id',2)
                                              ->where('status',1)
                                              ->groupBy('user_id')
                                              ->orderBy('count', 'desc')
                                              ->take(4)->get();
      $master_poster_0 = User::where('id',$master_poster_total[0]['user_id'])->first();
      $master_poster_1 = User::where('id',$master_poster_total[1]['user_id'])->first();
      $master_poster_2 = User::where('id',$master_poster_total[2]['user_id'])->first();
      $master_poster_3 = User::where('id',$master_poster_total[3]['user_id'])->first();
      if($master_poster_total[0]['count'] > 1){
        if(!is_null($master_poster_0)){
          $master_poster_data_0 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_0->alias.'"><img src="'.url('uploads').'/'.$master_poster_0->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_0->alias.'" class="blueText">'.$master_poster_0->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[0]['count'].' Ads & '.$master_poster_auctions[0]['count'].' Bids Listings</p></div></div></div>';
        }else{
            $master_poster_data_0 = '';
        }
      }else{
        $master_poster_data_0 = '';
      }
      if($master_poster_total[1]['count'] > 1){
        if(!is_null($master_poster_1)){
          $master_poster_data_1 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_1->alias.'"><img src="'.url('uploads').'/'.$master_poster_1->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_1->alias.'" class="blueText">'.$master_poster_1->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[1]['count'].' Ads & '.$master_poster_auctions[1]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_1 = '';
        }
      }else{
        $master_poster_data_1 = '';
      }
      if($master_poster_total[2]['count'] > 1){
        if(!is_null($master_poster_2)){
          $master_poster_data_2 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_2->alias.'"><img src="'.url('uploads').'/'.$master_poster_2->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_2->alias.'" class="blueText">'.$master_poster_2->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[2]['count'].' Ads & '.$master_poster_auctions[2]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_2 = '';
        }
      }else{
        $master_poster_data_2 = '';
      }
      if($master_poster_total[3]['count'] > 1){
        if(!is_null($master_poster_3)){
          $master_poster_data_3 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_3->alias.'"><img src="'.url('uploads').'/'.$master_poster_3->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_3->alias.'" class="blueText">'.$master_poster_3->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[3]['count'].' Ads & '.$master_poster_auctions[3]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_3 = '';
        }
      }else{
        $master_poster_data_3 = '';
      }
      $this->data['master_poster_html_0'] = $master_poster_data_0;
      $this->data['master_poster_html_1'] = $master_poster_data_1;
      $this->data['master_poster_html_2'] = $master_poster_data_2;
      $this->data['master_poster_html_3'] = $master_poster_data_3;
      // YAH IT WORKS TILL THE END!!!!
		return View::make('support.faqs', $this->data);
	}
	public function showAdvertising(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
		return View::make('support.advertising', $this->data);
	}
	public function showHelpUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      	$this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
                                                  
		return View::make('support.help-us', $this->data);
	}



}
   