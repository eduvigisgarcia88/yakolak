<?php
namespace App\Http\Controllers;

use App\Advertisement;
use App\AdvertisementPhoto;
use App\AdvertisementType;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
use App\Country;
use App\City;
use App\BannerSubscriptions;
use App\UserMessagesHeader;
use App\CustomAttributesForDropdown;
use App\UserNotificationsHeader;
use App\UserNotifications;
use App\UserSearchHistory;
use App\BannerDefault;
use App\BannerPlacementPlans;
use App\BannerAdvertisementMerge;
use App\AdRating;
// use App\UserAdComments;
use App\SubCategoriesTwo;
use App\BannerAdvertisementSubscriptions;
use Illuminate\Database\Eloquent\Collection;

class SearchController extends Controller
{

  public function browse(){

      $result = $this->doSearch();
      $this->data['title'] = "Search";
      // dd(Request::route('country_code'));
      $this->data['url_country_id'] = Country::where('countryCode',Request::route('country_code'))->where('status',1)->pluck('id');
      $this->data['rows'] = $result['rows'];
      Session::forget('session_main_category');
        if(Session::get('session_cat_id') != 'Y2F0LTI1'){
           Session::put('session_ad_type_id',1);
           Session::put('session_ad_type_hide',2);
        }else{
           Session::put('session_ad_type_id',2);
           Session::put('session_ad_type_hide',1);
        }
        if(Session::get('session_cat_id') == "Y2F0LTI4" || Session::get('session_cat_id') == "Y2F0LTI5"){
           Session::put('price_range_indicator',1);
        }else{
           Session::put('price_range_indicator',2);
        }
        if(Session::get('session_cat_id') == "Y2F0LTI5"){ //jobs
           Session::put('range_indicator',1);
        }else if(Session::get('session_cat_id') == "Y2F0LTI4"){ // events
           Session::put('range_indicator',2);
        }else{ // ads
           Session::put('range_indicator',3);
        }
        $this->data['per'] = $result['per'];
        $this->data['current_time'] = $result['current_time'];
        $this->data['count'] = $result['count'];
        $this->data['result_ad_type'] = $result['result_ad_type'];
        $result_cat_type = $result['result_ad_type'];

        $this->data['category_id'] = "";
        if(Auth::check()){
            $user_id = Auth::user()->id;
        }else{
            $user_id = null;
        }
        // $get_parent_cat_name = Category::where('unique_id','=',$id)->first();
        // Session::flash('session_parent_cat_name',$get_parent_cat_name->name);    
        // $this->data['countries'] = Country::where('status',1)->get();

        $this->data['categories'] = Category::where('status',1)
                                            ->where('country_code', Session::get('default_location'))
                                            ->where('cat_type','=',$result_cat_type)
                                            ->where(function($query){
                                              if(Request::get('listing'))
                                              {
                                                $query->where('biddable_status',2);
                                              }
                                              else
                                              {
                                                $query->where('buy_and_sell_status',2);
                                              }
                                            })->get();

                                     
        $this->data['usertypes'] = Usertypes::where('status',1)->get();

        $row = Category::where(function($query) use ($result_cat_type) {
                                  $query->where('status',1)
                                        ->where('cat_type',$result_cat_type);        
                                  })->get();
        $cat_type="";
        if(count($row)>0){
           $cat_type.= "<option value='all'>All Categories</option>";
          foreach ($row as $key => $field) {
           $cat_type .= '<option data-slug"'.$field->slug.'" value="'.$field->unique_id.'">'.$field->name.'</option>';
          }
        }else{
           $cat_type .= "undefined";
        }
        $result['cat_type'] = $cat_type;
        
        $this->data['cat_type'] =  $result['cat_type'];
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();

        $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
        $this->data['ad_type'] = AdvertisementType::all();
        $this->data['category'] = Category::with('subcategoryinfo')->get();
        $this->data['sub_category'] = SubCategory::where('status',1)->get();

        // $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
        //                                               $query->where('reciever_id','=',$user_id);
        //                                               })->where(function($status){
        //                                                  $status->where('read_status','=',1);
        //                                               })->count();
         $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();

        $this->data['top_reviews'] = AdRating::select('users.*','ad_ratings.*','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

        // $this->data['category_all'] = Category::with('subcategoryinfo')
        //                             ->where('unique_id','=',$id)
        //                             ->get();
        $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
      $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
      $count_companies = User::where('usertype_id',2)->where('status',1)->get();

       // $get_sub_attri_id = SubAttributes::where('attri_id','=',$id)->get();
       //               $container_attrib = '';
       //     foreach ($get_sub_attri_id as $key => $field) {

       //           if($field->attribute_type == 1){
       //                $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
       //                $container_attrib .= $field->name.'</h5>';
       //                $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
       //                $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
       //              foreach ($get_sub_attri_info as $key => $dropdown_info) {
       //                $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
       //              }
       //                $container_attrib .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
       //                $container_attrib .='</select></div></div>';
       //            }
       //           if($field->attribute_type == 2){
       //                $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
       //                $container_attrib .= $field->name.'</h5>';
       //                $container_attrib .= '<div class="col-sm-9">'; 
       //                $container_attrib .= '<input type="text" name="text_field_'.$field->id.'[]" class="form-control borderZero">';
       //                $container_attrib .='</div>';
       //                $container_attrib .= '<input type="hidden" name="attri_type[]" value = "textbox">';
       //                $container_attrib .='<input type="hidden" name="text_field_id['.$field->id.']" value="'.$field->id.'"></div>';
       //            }
       //           if($field->attribute_type == 3){
       //                $container_attrib .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
       //                $container_attrib .= $field->name.'</h5>';
       //                $container_attrib .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
       //                $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
       //              foreach ($get_sub_attri_info as $key => $dropdown_info) {
       //                $container_attrib .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
       //              }
       //                $container_attrib .='</select></div>';
       //                $container_attrib .= '<input type="hidden" name="attri_type[]" value = "dropdown">';
       //                $container_attrib .='<input type="hidden" name="dropdown_attri_id[]" value="'.$field->id.'"></div>';
       //           }           
       //    }
      if(!Auth::check()){
         $this->data['user_country'] =  1;
          $this->data['user_city'] = "all";
      }else{
         $this->data['user_country'] = Auth::user()->country;
         $this->data['user_city'] = Auth::user()->city;
      }
      $get_city = City::where('country_id', '=', $this->data['user_country'])->orderBy('name','asc')->get();
           $container_city = '<option class="hide" value="">Select:</option>';
           $container_city .= '<option value="all" selected>All Cities</option>';
//           foreach ($get_city as $key => $field) {
//             $container_city .= '<option value="' . $field->id . '" >' . $field->name . '</option>';
//           }
      $this->data['featured_ads']  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'users.alias','country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      $this->data['container_city'] = $container_city;
      // $this->data['container_attrib'] = $container_attrib;
      $this->data['total_no_of_ads'] = count($count_ads);
      $this->data['total_no_of_bids'] = count($count_bids);
      $this->data['total_no_of_vendors'] = count($count_vendors);
      $this->data['total_no_of_companies'] = count($count_companies); 
      $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
      if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$cat)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();



     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$cat)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();

     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                if (!$if_exist) {
                  $saveBanners = new BannerAdvertisementMerge;       
                  $saveBanners->banner_id = $field;
                  $saveBanners->country_id = $getCountryCode->id;
                  $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                  $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                  $saveBanners->position = $getBannerPosition->position;
                  $saveBanners->order_no = $getBannerPosition->order_no;
                  $saveBanners->url = $getBannerDetails->target_url;
                  $saveBanners->category_id = $getBannerDetails->category;
                  $saveBanners->designation = 2;
                  $saveBanners->type = 1;
                  $saveBanners->save();
                }
                
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                  if (!$if_exist) {

                    $saveBanners = new BannerAdvertisementMerge;       
                    $saveBanners->banner_id = $field;
                    if($selectedCountry != "ALL"){
                       $saveBanners->country_id = $getCountryCode->id;
                    }else{
                       $getDefaultCountry = Country::where('countryCode','US')->first(); 
                       $saveBanners->country_id = $getDefaultCountry->id;
                    }
                    $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                    $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                    $saveBanners->position = $getBannerPosition->position;
                    $saveBanners->order_no = $getBannerPosition->order_no;
                    $saveBanners->url = $getBannerDetails->target_url;
                    $saveBanners->designation = 2;
                    $saveBanners->type = 2;
                    $saveBanners->save();
                }
              }
                
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
//     header('HTTP/1.1 500 Internal Server Error');

      $banner_top_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 1)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $this->data['banner_placement_top'] = ($banner_top_purchased ?: $banner_top_default);
    
                                                                   
             

                                                                   
      $banner_bottom_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 1)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
    
      $banner_bottom_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
      $this->data['banner_placement_bottom'] = ($banner_bottom_purchased ?: $banner_bottom_default);
                                                                 

      $banner_left_A_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $this->data['banner_placement_left_one'] = ($banner_left_A_purchased ?: $banner_left_A_default);

     $banner_left_B_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
      $banner_left_B_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $this->data['banner_placement_left_two'] = ($banner_left_B_purchased ?: $banner_left_B_default);   
      
    return View::make('category.search', $this->data);


  }
  public function filteredBrowse(){
      $result = $this->doFilterSearch();
      $this->data['title'] = "Search";
      $this->data['rows'] = $result['rows'];
      $this->data['current_time'] = $result['current_time'];
      $this->data['banner_top'] = $result['banner_top'];
      $this->data['banner_bottom'] = $result['banner_bottom'];
      $this->data['banner_left_A'] = $result['banner_left_A'];
      $this->data['banner_left_B'] = $result['banner_left_B'];
      $this->data['featured_ads']  = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();  

    
       $count_ads =  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '1');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_bids=  Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })                                       
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->get();
        $count_vendors = User::where('usertype_id',1)->where('status',1)->get();
        $count_companies = User::where('usertype_id',2)->where('status',1)->get();
        $this->data['total_no_of_ads'] = count($count_ads);
        $this->data['total_no_of_bids'] = count($count_bids);
        $this->data['total_no_of_vendors'] = count($count_vendors);
        $this->data['total_no_of_companies'] = count($count_companies);
        $this->data['top_members'] = User::with('getUserAdvertisements')->take(4)->get();
        // $this->data['top_comments'] =User::with('getUserAdComments')->orderBy('created_at','desc')->take(5)->get();                                   
        $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('status',1)->get();
        $this->data['countries'] = Country::orderBy('countryName','asc')->get();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['ad_type'] = AdvertisementType::all();
        $this->data['category'] = Category::with('subcategoryinfo')->get();
        // $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
        // $this->data['banner_placement_bottom'] = BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);
       
       if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
      $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                              // ->where('category','=',$id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();

     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                              // ->where('category','=',$id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();


     // dd($browseBannersIds);
     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                      if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }   
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      // $checKBannerPerCountry = BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->get();
      //     if(count($checKBannerPerCountry) > 0){
      //         BannerAdvertisementMerge::where('country_id',$getCountryCode->id)->delete();
      //     }

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                $saveBanners->country_id = $getCountryCode->id;
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerDetails->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 1;
                $saveBanners->save();
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $saveBanners = new BannerAdvertisementMerge;       
                $saveBanners->banner_id = $field;
                if($selectedCountry != "ALL"){
                   $saveBanners->country_id = $getCountryCode->id;
                }else{
                   $getDefaultCountry = Country::where('countryCode','US')->first(); 
                   $saveBanners->country_id = $getDefaultCountry->id;
                }
                $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                $saveBanners->position = $getBannerPosition->position;
                $saveBanners->order_no = $getBannerPosition->order_no;
                $saveBanners->url = $getBannerPosition->target_url;
                $saveBanners->designation = 2;
                $saveBanners->type = 2;
                $saveBanners->save();
              }
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
      $this->data['banner_placement_top'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->orderBy('order_no','asc')
                                                                    // ->orderByRaw('RAND()')
                                                                    ->get();      
      $this->data['banner_placement_bottom'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->get();      
                                                                 


      $this->data['banner_placement_left_one'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->get();      

      $this->data['banner_placement_left_two'] = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                          ->get();      

          //dd($this->data['banners']);
          if(Auth::check()){
                $user_id = Auth::user()->id;
            }else{
                $user_id = null;
          }

        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                        $query->where('reciever_id','=',$user_id);
                                                          // ->OrWhere('sender_id','=',$user_id);
                                                        })->where(function($status){
                                                           $status->where('read_status','=',1);
                                                        })->count();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        return View::make('category.search', $this->data);


  }
  public function index(){
        $this->data['title'] = "Search";
        $this->data['featured_ads']  = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                           ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                           ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                           ->leftJoin('countries','countries.id', '=', 'users.country')
                                           ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                           ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                             'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                           ->where(function($query){
                                                 $query->where('advertisements_photo.primary', '=', '1')
                                                       ->where('advertisements.status', '=', '1')
                                                       ->where('advertisements.feature', '=', '1');                                      
                                            })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();  

                                           
        $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
        $this->data['countries'] = Country::orderBy('countryName','asc')->get();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['ad_type'] = AdvertisementType::all();
        $this->data['category'] = Category::with('subcategoryinfo')->get();
        if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }
        $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                          $query->where('reciever_id','=',$user_id);
                                                            // ->OrWhere('sender_id','=',$user_id);
                                                          })->where(function($status){
                                                             $status->where('read_status','=',1);
                                                          })->count();
        $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                          $query->where('reciever_id','=',$user_id);
                                                          })->where(function($status){
                                                             $status->where('read_status','=',1);
                                                          })->count();
        $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
        return View::make('category.search', $this->data);

  }
  public function doSearch(){

      $input = Input::all();
      $keyword = array_get($input,'search') ? : '';
      $ad_type = array_get($input,'ad_type') ? : 1;
      $category = array_get($input,'category');
      $country = array_get($input,'country');
      $city = array_get($input,'city');
      $custom_array = Input::get('custom_array');
      $dropdown_field  = Input::get('dropdown_field');
      $dropdown_attri_id  = Input::get('dropdown_attri_id');
      $price_from = Input::get('price_from') ? :100;
      $price_to = Input::get('price_to') ? : 1000;
      $per = Request::input('per') ? : 9;
      Session::put('keyword', $keyword);
      //save search history of the logged user
      
      $row  = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                  ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.created_at','advertisements.ads_type_id','advertisements.hierarchy_order', 'advertisements.description','advertisements.feature', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where('advertisements.ads_type_id','=',$ad_type)
                                  ->where('advertisements.title','LIKE','%'.$keyword.'%') 
                                  ->groupBy('advertisements.id')
                                  // ->orderBy('advertisements.user_plan_id','desc')
                                  ->orderBy('advertisements.feature', 'desc')
                                  ->orderBy('advertisements.hierarchy_order', 'asc')
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->take($per)
                                  ->get();
                                  
      $check_result_for_bids =  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                  ->select('users.*','advertisements_photo.*','advertisements.*','countries.countryName', 'country_cities.name as city')
                                  ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                  ->leftjoin('ad_custom_attribute_dropdown','ad_custom_attribute_dropdown.ad_id','=','advertisements.id')
                                  ->leftjoin('users','users.id','=','advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->where('advertisements_photo.primary','=',1)
                                  ->where('advertisements_photo.status','=',1)
                                  ->where('advertisements.status','=',1)
                                  ->where('advertisements.ads_type_id','=',2)
                                  ->where('advertisements.title','LIKE','%'.$keyword.'%') 
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.created_at','desc')
                                  ->get();        

      $check_result_for_ads =  Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                  ->select('users.*','advertisements_photo.*','advertisements.*','countries.countryName', 'country_cities.name as city')
                                  ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                  ->leftjoin('ad_custom_attribute_dropdown','ad_custom_attribute_dropdown.ad_id','=','advertisements.id')
                                  ->leftjoin('users','users.id','=','advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->where('advertisements_photo.primary','=',1)
                                  ->where('advertisements_photo.status','=',1)
                                  ->where('advertisements.status','=',1)
                                  ->where('advertisements.ads_type_id','=',1)
                                  ->where('advertisements.title','LIKE','%'.$keyword.'%') 
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.created_at','desc')
                                  ->get();                       
      if(count($check_result_for_ads) > 0  &&  count($check_result_for_bids) == 0){
          $result_ad_type = 1;
      }else if(count($check_result_for_ads) ==  0  &&  count($check_result_for_bids) > 0){
          $result_ad_type = 2;
      }else{
         $result_ad_type = "all"; 
      }         
       if(Auth::check()){
           if(Input::has('keyword')){
             $search_history = new UserSearchHistory;
             $search_history->user_id = Auth::user()->id;
             $search_history->email = Auth::user()->email;
             $search_history->search_tool = "Global";
             $search_history->country = Session::get('default_location');
             $search_history->keyword = array_get($input,'keyword');
             $search_history->save();
          }
      } 
      $max_ads  = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])
                                  ->select('users.*','advertisements_photo.*','advertisements.*','countries.countryName', 'country_cities.name as city')
                                  ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                  ->leftjoin('ad_custom_attribute_dropdown','ad_custom_attribute_dropdown.ad_id','=','advertisements.id')
                                  ->leftjoin('users','users.id','=','advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->where('advertisements_photo.primary','=',1)
                                  ->where('advertisements_photo.status','=',1)
                                  ->where('advertisements.status','=',1)
                                  ->where('advertisements.title','LIKE','%'.$keyword.'%') 
                                  ->groupBy('advertisements.id')
                                  // ->orderBy('advertisements.user_plan_id','desc')
                                  ->orderBy('advertisements.created_at','desc')
                                  ->get();   
      if(Request::ajax()) {
         $always_first = new Collection();
         $remaining = new Collection();
          foreach ($row as $val) {
            if ($val->feature == 1) {
              $always_first->add($val);
            } else {
              $remaining->add($val);
            }
            $val->average_rate = ($val->getAdRatings->avg('rate') == null ? 0 : $val->getAdRatings->avg('rate'));
          }
         $result['rows'] = $always_first->merge($remaining)->take($per)->toArray();
         return Response::json($result);
      } else {
         $result['rows'] = $row;
         // $result['fetched_category'] = $get_category;
         $result['per'] = $per;
         $result['count'] = count($max_ads);
         $result['result_ad_type'] = $ad_type;
         $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
         return $result;
      }                                                      

  }
   public function doFilterSearch(){
      $input  = Input::all();
     
      Session::forget('session_main_category_name');
      Session::forget('session_category_name');
      Session::forget('session_sub_category_name');
      Session::forget('session_parent_cat_name');

      $main_cat_id = "all";

      if(Request::input('main_category') != "all" && Request::input('category') == "all" && Request::input('subcategory') == "all"  ){

          $main_cat_id = array_get($input,'main_category');

        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') == "all" ) {
          
          $main_cat_id = array_get($input,'category');
        
        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  ){
          
          $main_cat_id = array_get($input,'subcategory');

        }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"){
          
          $main_cat_id = array_get($input,'subSubCategory');

        }
        else if(Request::input('main_category') == "all" && Request::input('category') == "all" && Request::input('subcategory') =="all"){

          $main_cat_id = "all";
        }

      // if(Request::input('ad_type') == "1"){
      //   if(Request::input('main_category') != "all" && Request::input('category') == "all" && Request::input('subcategory') == "all" && Request::input('subSubCategory') == "all" ){
      //     $main_cat_id = array_get($input,'main_category');
      //   }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') == "all" && Request::input('subSubCategory') == "all") {
          
      //     $main_cat_id = array_get($input,'category');
        
      //   }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  && Request::input('subSubCategory') == "all"){
          
      //     $main_cat_id = array_get($input,'subcategory');

      //   }else if(Request::input('main_category') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  && Request::input('subSubCategory') != "all"){
          
      //     $main_cat_id = array_get($input,'subSubCategory');

      //   }
      //   else if(Request::input('main_category') == "all" && Request::input('category') == "all" && Request::input('subcategory') =="all" && Request::input('subSubCategory') =="all"){

      //     $main_cat_id = "all";
      //   }

      // }else if(Request::input('ad_type') == "2"){

      //   if(Request::input('main_category') != "all" && Request::input('category_auction') == "all" && Request::input('category') == "all" && Request::input('subcategory') == "all" && Request::input('subSubCategory') == "all" ){

      //     $main_cat_id = array_get($input,'main_category');

      //   }else if(Request::input('main_category') != "all" && Request::input('category_auction') != "all" && Request::input('category') =="all" && Request::input('subcategory') == "all" && Request::input('subSubCategory') == "all") {
          
      //     $main_cat_id = array_get($input,'category_auction');
        
      //   }else if(Request::input('main_category') != "all" && Request::input('category_auction') != "all" && Request::input('category') !="all" && Request::input('subcategory') == "all" && Request::input('subSubCategory') == "all") {
          
      //     $main_cat_id = array_get($input,'category');
        
      //   }else if(Request::input('main_category') != "all" && Request::input('category_auction') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  && Request::input('subSubCategory') == "all"){
          
      //     $main_cat_id = array_get($input,'subcategory');

      //   }else if(Request::input('main_category') != "all" && Request::input('category_auction') != "all" && Request::input('category') !="all" && Request::input('subcategory') !="all"  && Request::input('subSubCategory') != "all"){
          
      //     $main_cat_id = array_get($input,'subSubCategory');

      //   }
      //   else if(Request::input('main_category') == "all" && Request::input('category_auction') == "all" && Request::input('category') == "all" && Request::input('subcategory') =="all" && Request::input('subSubCategory') =="all"){

      //     $main_cat_id = "all";
      //   }
      // }else if(Request::input('ad_type') == "4"){
      //     $main_cat_id = "all";
      // }
      // dd($main_cat_id);
      $ad_type = Request::input('ad_type');
      $country = Request::input('country');
      $per = Request::input('per') ? :9;
      $keyword = Request::input('keyword');
      $order = Request::input('order') ? : 'desc';
      $sort = Request::input('sort') ? : 'created_at';
      $city = Request::input('city');
      $dropdown_attri_id  = Request::input('dropdown_attri_id');
      $custom_array = Request::input('custom_array') ? : null;
      $attri_type = Request::input('attri_type');
      $price_from = Request::input('price_from');
      $price_to = Request::input('price_to');
      $salary_from = Request::input('salary_from');
      $salary_to = Request::input('salary_to');
      if(Request::has('date_from')){
         $date_from =  Carbon::createFromFormat('m/d/Y', Request::input('date_from'))->format('Y-m-d');
      }else{
         $date_from = "";
      }
      if(Request::has('date_to')){
         $date_to = Carbon::createFromFormat('m/d/Y', Request::input('date_to'))->format('Y-m-d');
      }else{
         $date_to = "";
      }
      if(Request::input('main_category')){
         $find_main_category = Category::where('unique_id','=',Request::input('main_category'))->first();
         if(!is_null($find_main_category)){
            Session::flash('session_main_category_name',$find_main_category->name);   
         }
      }
      if(Request::input('category')|| Request::input('category') != "" || Request::input('category') != "all"){
         $find_category = SubCategory::where('unique_id','=',Request::input('category'))->first();
         if(!is_null($find_category)){
         Session::flash('session_category_name', $find_category->name);
         }
      }
     if(Request::input('subcategory') || Request::input('subcategory') != "" || Request::input('subcategory') != "all" ){
         $find_subcategory = SubCategoriesTwo::where('unique_id','=',Request::input('subcategory'))->first();
         if(!is_null($find_subcategory)){
         Session::flash('session_sub_category_name', $find_subcategory->name);
         }
      }
        $history = "";
      if(Session::has('session_main_category_name')){
         $history = Session::get('session_main_category_name');
      }
      if(Session::has('session_main_category_name') && Session::has('session_category_name')){
         $history = Session::get('session_main_category_name').' > '.Session::get('session_category_name');
      }
      if(Session::has('session_main_category_name') && Session::has('session_category_name') && Session::has('session_sub_category_name')){
         $history = Session::get('session_main_category_name').' > '.Session::get('session_category_name').'>'.Session::get('session_sub_category_name');
      }
      $get_country = Country::find($country);
     if(Auth::check()){
           if(Input::has('keyword')){
             $search_history = new UserSearchHistory;
             $search_history->user_id = Auth::user()->id;
             $search_history->email = Auth::user()->email;
             $search_history->search_tool = "Global";
             if($get_country)
             {
              $search_history->country = $get_country->countryCode;
             }
             $search_history->keyword = array_get($input,'keyword');
             $search_history->save();
          } 
      }

      Session::put('session_category', array_get($input,'category'));
      Session::put('session_subcategory', array_get($input,'subcategory'));
      Session::put('session_main_category', array_get($input,'main_category'));
      Session::put('session_keyword', $keyword);
      Session::put('session_ad_type', $ad_type);
      Session::put('session_country', $country);
      Session::put('session_city', $city);
      Session::flash('session_price_from', $price_from);
      Session::flash('session_price_to', $price_to);


      $ctr = 0;
      $get_all = [];
      if ($attri_type) {
      foreach ($attri_type as $key => $atvalue) {

        $at_attri_id  = Request::input($atvalue . '_attri_id');

        if ($at_attri_id) {
        foreach ($at_attri_id as $key => $attri_id) {

          $at_field  = Request::input($atvalue .'_field_' . $attri_id);
          if (!$at_field) {
            $at_field = [];
          }

          foreach (array_filter($at_field) as $key => $attri_value) {
            if ($ctr > 0 ) {
                $get_searched_id = DB::table('ad_custom_attribute_'. $atvalue)->where(function($query) use ($attri_id,$atvalue,$attri_value){
                                              if (strlen($attri_id) > 0 && strlen($attri_value) > 0) {
                                                if ($atvalue == "textbox") {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value', 'LIKE', '%' . $attri_value . '%');
                                                } else {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value_id', 'LIKE', '%' . $attri_value . '%');
                                                }
                                              }
                                             })
                                          ->where('status',1)
                                          ->whereIn('ad_id', $get_searched_id)                                         
                                          ->groupBy('ad_id')
                                          ->lists('ad_id');

            } else {
                $get_searched_id = DB::table('ad_custom_attribute_'. $atvalue)->where(function($query) use ($attri_id,$atvalue,$attri_value){
                                              if (strlen($attri_id) > 0 && strlen($attri_value) > 0) {
                                                if ($atvalue == "textbox") {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value', 'LIKE', '%' . $attri_value . '%');
                                                } else {
                                                  $query->where('attri_id', '=', $attri_id)
                                                        ->where('attri_value_id', 'LIKE', '%' . $attri_value . '%');
                                                }
                                              }
                                             })
                                          ->where('status',1)                                       
                                          ->groupBy('ad_id')
                                          ->lists('ad_id');
              // dd($atvalue);
            }
            $ctr++;

          }

        }
      }

        }

      }

       if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
      }
       $getCountryCode = Country::where('countryCode',$selectedCountry)
                                   ->where('status',1)
                                   ->first(); 
       
     
     $browseBanners = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$main_cat_id)
                                                              ->where('status',1)
                                                              ->lists('placement_id','id')
                                                              ->toArray();



     $browseBannersIds = BannerAdvertisementSubscriptions::where('page',2)
                                                              ->where('banner_status','=',4)
                                                              ->where(function($query) use ($selectedCountry,$getCountryCode){
                                                                       if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
//                                                               ->where('category','=',$main_cat_id)
                                                              ->where('placement_id','!=','null')
                                                              ->where('status',1)
                                                              ->lists('id')
                                                              ->toArray();
//     header('HTTP/1.1 500 Internal Server Error');
//     dd($main_cat_id);

     $getBannerDefaultsPerCountry = BannerDefault::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                               })
                                                               ->groupBy('placement_id')
                                                               ->where('page_location',2)
                                                               ->where('status',1)
                                                               ->whereNotIn('placement_id',array_filter($browseBanners))
                                                               ->lists('id')
                                                               ->toArray();

 

    
      $merge_array = array_merge(array_filter($browseBanners),array_filter($getBannerDefaultsPerCountry));

      if(!is_null($browseBannersIds)){

              foreach ($browseBannersIds as $key => $field) {
               $getBannerDetails =  BannerAdvertisementSubscriptions::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                if (!$if_exist) {
                  $saveBanners = new BannerAdvertisementMerge;       
                  $saveBanners->banner_id = $field;
                  $saveBanners->country_id = $getCountryCode->id;
                  $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                  $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                  $saveBanners->position = $getBannerPosition->position;
                  $saveBanners->order_no = $getBannerPosition->order_no;
                  $saveBanners->url = $getBannerDetails->target_url;
                  $saveBanners->category_id = $getBannerDetails->category;
                  $saveBanners->designation = 2;
                  $saveBanners->type = 1;
                  $saveBanners->save();
                }
                
              }
             
          }
          
       if(!is_null($getBannerDefaultsPerCountry))
          {
              foreach ($getBannerDefaultsPerCountry as $key => $field) {
               $getBannerDetails = BannerDefault::find($field);
               $getBannerPosition = BannerPlacementPlans::find($getBannerDetails->placement_id);
                
                $if_exist = BannerAdvertisementMerge::where('banner_id', $field)->where('status', 1)->first();
                  if (!$if_exist) {

                    $saveBanners = new BannerAdvertisementMerge;       
                    $saveBanners->banner_id = $field;
                    if($selectedCountry != "ALL"){
                       $saveBanners->country_id = $getCountryCode->id;
                    }else{
                       $getDefaultCountry = Country::where('countryCode','US')->first(); 
                       $saveBanners->country_id = $getDefaultCountry->id;
                    }
                    $saveBanners->image_desktop = $getBannerDetails->image_desktop;
                    $saveBanners->image_mobile = $getBannerDetails->image_mobile;
                    $saveBanners->position = $getBannerPosition->position;
                    $saveBanners->order_no = $getBannerPosition->order_no;
                    $saveBanners->url = $getBannerDetails->target_url;
                    $saveBanners->designation = 2;
                    $saveBanners->type = 2;
                    $saveBanners->save();
                }
              }
                
          }
       
      // $getBannerAdvertisementMerge = BannerAdvertisementMerge::where()->get();

      // if(is_null($getBannerAdvertisementMerge) && count($getBannerAdvertisementMerge) > 0){

      // }
//     header('HTTP/1.1 500 Internal Server Error');

      $banner_top_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 1)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })
                                                                    ->where('position',1)
                                                                    ->where('designation',2)
                                                                    ->where('type', 2)
                                                                    ->orderBy('order_no','asc')
                                                                    ->first();
    
    $banner_top = ($banner_top_purchased ?: $banner_top_default);
    
                                                                   
             

                                                                   
      $banner_bottom_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 1)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
    
      $banner_bottom_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                      ->where('position',2)
                                                                      ->where('designation',2)
                                                                      ->where('type', 2)
                                                                      ->orderBy('order_no','asc')
                                                                      // ->orderByRaw('RAND()')
                                                                      ->first();
    
      $banner_bottom = ($banner_bottom_purchased ?: $banner_bottom_default);
                                                                 

      $banner_left_A_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',3)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_A = ($banner_left_A_purchased ?: $banner_left_A_default);

     $banner_left_B_purchased = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 1)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
      $banner_left_B_default = BannerAdvertisementMerge::where(function($query) use ($selectedCountry,$getCountryCode){
                                                                        if($selectedCountry != "ALL"){
                                                                           $query->where('country_id','=',$getCountryCode->id);
                                                                        }else{
                                                                           $getDefaultCountry = Country::where('countryCode','US')->first(); 
                                                                           $query->where('country_id','=',$getDefaultCountry->id);
                                                                        }       
                                                                      })  
                                                                          ->where('position',4)
                                                                          ->where('designation',2)
                                                                          ->where('type', 2)
                                                                          ->orderBy('order_no','asc')
                                                                          // ->orderByRaw('RAND()')
                                                                         ->first();
    
    $banner_left_B = ($banner_left_B_purchased ?: $banner_left_B_default);
     
     
 if($ctr > 0){
          $rows = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency','advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at','advertisements.ads_type_id', 'advertisements.description','advertisements.feature', 'advertisements.hierarchy_order', 'advertisements_photo.photo',
                                            'users.name','users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                        if(Request::input('price_from') ||  Request::input('price_from') != 0){
                                            $query->where('advertisements.price','>=',$price_from) ;
                                        }
                                        if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                            $query->where('advertisements.price','<=',$price_to) ;
                                        }
                                        if(Request::input('country') && Request::input('country') != "all"){
                                            $query->where('advertisements.country_id', '=', $country);
                                        }
                                        if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                        if(Request::input('city') && Request::input('city') != "all"){
                                            $query->where('advertisements.city_id', '=', $city);       
                                        } 
                                        if(Request::input('keyword') && Request::input('keyword') != ""){
                                            $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');
                                        }
                                        if(Request::input('salary_from') ||  Request::input('salary_from') != 0){
                                            $query->where('advertisements.salary','>=',$salary_from) ;
                                        }
                                        if(Request::input('salary_to') ||  Request::input('salary_to') != 0){
                                            $query->where('advertisements.salary','<=',$salary_to) ;
                                        }
                                        if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                            $query->where('advertisements.event_date','>=',$date_from) ;
                                        }
                                        if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                            $query->where('advertisements.event_date','<=',$date_to) ;
                                        }                                                                                        
                                   })
                                   ->where(function($query) use ($main_cat_id){
                                      if($main_cat_id != "all"){
                                        $query->where('advertisements.parent_category','=',$main_cat_id)
                                                   ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                   ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                   ->orWhere('advertisements.category_id','=',$main_cat_id);
                                       }                                                                     
                                   })     
                                  ->whereIn('advertisements.id',$get_searched_id)
                                  ->orderBy('advertisements.'.$sort, $order)
                                  // ->orderBy('advertisements.feature', 'desc')
                                  ->orderBy('advertisements.hierarchy_order', 'asc')
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->groupBy('advertisements.id')
                                  // ->take($per)
                                  ->get();



        $max_ads = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at','advertisements.ads_type_id',  'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                        if(Request::input('price_from') ||  Request::input('price_from') != 0){
                                            $query->where('advertisements.price','>=',$price_from) ;
                                        }
                                        if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                            $query->where('advertisements.price','<=',$price_to) ;
                                        }
                                        if(Request::input('country') && Request::input('country') != "all"){
                                            $query->where('advertisements.country_id', '=', $country);
  
                                        }
                                        if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                        if(Request::input('city') && Request::input('city') != "all"){
                                            $query->where('advertisements.city_id', '=', $city);       
                                        }
                                        if(Request::input('keyword') && Request::input('keyword') != ""){
                                            $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');
                                        }
                                        if(Request::input('salary_from') ||  Request::input('salary_from') != 0){
                                            $query->where('advertisements.salary','>=',$salary_from) ;
                                        }
                                        if(Request::input('salary_to') ||  Request::input('salary_to') != 0){
                                            $query->where('advertisements.salary','<=',$salary_to) ;
                                        }
                                        if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                            $query->where('advertisements.event_date','>=',$date_from) ;
                                        }
                                        if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                            $query->where('advertisements.event_date','<=',$date_to) ;
                                        }                           
                                   })
                                   ->where(function($query) use ($main_cat_id){
                                       if($main_cat_id != "all"){
                                          $query->where('advertisements.parent_category','=',$main_cat_id)
                                                     ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                     ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                     ->orWhere('advertisements.category_id','=',$main_cat_id);
                                       }                                                                           
                                   })
                                  ->whereIn('advertisements.id',$get_searched_id)
                                  ->orderBy('advertisements.'.$sort, $order)
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->groupBy('advertisements.id')
                                  ->get();                        
                                 
      }else{
          $rows = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at','advertisements.ads_type_id', 'advertisements.hierarchy_order', 'advertisements.description','advertisements.feature', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where('advertisements.status',1)
                                  ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                   })
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                        if(Request::input('price_from') ||  Request::input('price_from') != 0){
                                            $query->where('advertisements.price','>=',$price_from) ;
                                        }
                                        if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                            $query->where('advertisements.price','<=',$price_to) ;
                                        }
                                        if(Request::input('country') && Request::input('country') != "all"){
                                            $query->where('advertisements.country_id', '=', $country);
                                        }
                                        if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                        if(Request::input('city') && Request::input('city') != "all"){
                                            $query->where('advertisements.city_id', '=', $city);       
                                        }  
                                        if(Request::input('keyword') && Request::input('keyword') != ""){
                                            $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                        }
                                         if(Request::input('salary_from') ||  Request::input('salary_from') != 0){
                                            $query->where('advertisements.salary','>=',$salary_from) ;
                                        }
                                        if(Request::input('salary_to') ||  Request::input('salary_to') != 0){
                                            $query->where('advertisements.salary','<=',$salary_to) ;
                                        }
                                        if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                            $query->where('advertisements.event_date','>=',$date_from);
                                        }
                                        if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                            $query->where('advertisements.event_date','<=',$date_to) ;
                                        }                
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                          if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }                                                                       
                                   })                                                   
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.'.$sort, $order)
                                  // ->orderBy('advertisements.feature', 'desc')
                                  ->orderBy('advertisements.hierarchy_order', 'asc')
                                  ->orderBy('advertisements.created_at', 'desc')
                                  // ->take($per)
                                  ->get();
                                  
       $max_ads = Advertisement::with(['getAdRatings', 'getAdvertisementComments'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                  ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                  ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                  ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                  ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                  ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.created_at','advertisements.ads_type_id',  'advertisements.description', 'advertisements_photo.photo',
                                            'users.name', 'users.usertype_id', 'users.alias','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                  ->selectRaw('avg(rate) as average_rate')
                                  ->where(function($query){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1');                                                                             
                                   })
                                  ->where(function($query) use ($price_from,$price_to,$country,$city,$keyword,$ad_type,$main_cat_id,$salary_from,$salary_to,$date_from,$date_to){
                                        if(Request::input('price_from') ||  Request::input('price_from') != 0){
                                            $query->where('advertisements.price','>=',$price_from) ;
                                        }
                                        if(Request::input('price_to') ||  Request::input('price_to') != 0){
                                            $query->where('advertisements.price','<=',$price_to) ;
                                        }
                                        if(Request::input('country') && Request::input('country') != "all"){
                                            $query->where('advertisements.country_id', '=', $country);
                                        }
                                        if(Request::input('ad_type') && Request::input('ad_type') != "4"){
                                              $query->where('advertisements.ads_type_id', '=', $ad_type);
                                          }
                                        if(Request::input('city') && Request::input('city') != "all"){
                                            $query->where('advertisements.city_id', '=', $city);       
                                        }  
                                        if(Request::input('keyword') && Request::input('keyword') != ""){
                                            $query->where('advertisements.title', 'LIKE', '%'.$keyword.'%');  
                                        }
                                        if(Request::input('salary_from') ||  Request::input('salary_from') != 0){
                                            $query->where('advertisements.salary','>=',$salary_from) ;
                                        }
                                        if(Request::input('salary_to') ||  Request::input('salary_to') != 0){
                                            $query->where('advertisements.salary','<=',$salary_to) ;
                                        }
                                        if(Request::input('date_from') ||  Request::input('date_from') != ""){
                                            $query->where('advertisements.event_date','>=',$date_from) ;
                                        }
                                        if(Request::input('date_to') ||  Request::input('date_to') != ""){
                                            $query->where('advertisements.event_date','<=',$date_to) ;
                                        }                
                                    })
                                  ->where(function($query) use ($main_cat_id){
                                          if($main_cat_id != "all"){
                                             $query->where('advertisements.parent_category','=',$main_cat_id)
                                                  ->orWhere('advertisements.cat_unique_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.sub_category_id','=',$main_cat_id)
                                                  ->orWhere('advertisements.category_id','=',$main_cat_id);
                                          }                                                                                    
                                   })
                                  ->groupBy('advertisements.id')
                                  ->orderBy('advertisements.'.$sort, $order)
                                  ->orderBy('advertisements.created_at', 'desc')
                                  ->get();
      }                     
     if(Request::ajax()) {
          // $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());

          $always_first = new Collection();
           $remaining = new Collection();
            foreach ($rows as $val) {
              if ($val->feature == 1) {
                $always_first->add($val);
              } else {
                $remaining->add($val);
              }
              $val->average_rate = ($val->getAdRatings->avg('rate') == null ? 0 : $val->getAdRatings->avg('rate'));
            }
          $result['rows'] = $always_first->merge($remaining)->take($per)->toArray();
          // $result['rows'] = $rows->toArray();
          $result['sort'] = $sort;
          $result['history'] = $history;
          $result['order'] = $order;
          $result['per'] = $per;
          $result['keyword'] = $keyword;
          $result['count'] = count($rows);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s'); 
       
          $result['banner_top'] = $banner_top;
          $result['banner_left_A'] = $banner_left_A;
          $result['banner_left_B'] = $banner_left_B;
          $result['banner_bottom'] = $banner_bottom;
       
          return Response::json($result);
      } else {
          // $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          $result['categoy_id'] =Request::input('category');
          $result['per'] = $per;
          $result['count'] = count($max_ads);
          $result['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
          return $result;
      }                                                     
                                                   
  }
  public function indexRecentSearches(){
    $not_allowed = array(1,2);
    if(Auth::check()){
      if(in_array(Auth::user()->usertype_id, $not_allowed)){
          return redirect()->to('error/access/denied');
      }
    }else{
      return redirect()->to('error/access/denied');
    }
    $result = $this->doListRecentSearches();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['title'] = "Marketing - Recent Searches";
    $this->data['refresh_route'] = url('admin/dashboard/recent-searches/refresh');
    return View::make('admin.recent-searches-management.list', $this->data);
   }

 public function doListRecentSearches(){
      $result['sort'] = Request::input('sort') ?: 'user_search_history.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search') ? : '';
      $search_tool = Request::input('search_tool') ? : '';

      $status = Request::input('status') ? : '';
      $per = Request::input('per') ?: 10;
          if (Request::input('page') != '»') {
              Paginator::currentPageResolver(function () {
                  return Request::input('page'); 
          });

              $rows =  UserSearchHistory::leftJoin('users','users.id', '=','user_search_history.user_id')
                                           ->select('users.name','users.email','user_search_history.*')
                                            ->where(function($query) use ($search_tool) {
                                                $query->where('user_search_history.search_tool','LIKE', '%' . $search_tool . '%');    
                                             })
                                             ->where(function($query) use ($search) {
                                                  $query->where('user_search_history.search_tool', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.category', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.email', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.keyword', 'LIKE', '%' . $search . '%');
                                              })
                                            ->orderBy($result['sort'], $result['order'])
                                            ->paginate($per);
                   
                   
        } else {

             $count =   UserSearchHistory::leftJoin('users','users.id', '=','user_search_history.user_id')
                                           ->select('users.name','users.email','user_search_history.*')
                                            ->where(function($query) use ($search_tool) {
                                                $query->where('user_search_history.search_tool','LIKE', '%' . $search_tool . '%');    
                                             })
                                             ->where(function($query) use ($search) {
                                                  $query->where('user_search_history.search_tool', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.category', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.email', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.keyword', 'LIKE', '%' . $search . '%');
                                              })
                                            ->orderBy($result['sort'], $result['order'])
                                            ->paginate($per);
                   
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  UserSearchHistory::leftJoin('users','users.id', '=','user_search_history.user_id')
                                           ->select('users.name','users.email','user_search_history.*')
                                            ->where(function($query) use ($search_tool) {
                                                $query->where('user_search_history.search_tool','LIKE', '%' . $search_tool . '%');    
                                             })
                                             ->where(function($query) use ($search) {
                                                  $query->where('user_search_history.search_tool', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.category', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.email', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                                                        ->orWhere('user_search_history.keyword', 'LIKE', '%' . $search . '%');
                                              })
                                            ->orderBy($result['sort'], $result['order'])
                                            ->paginate($per);

                   
        }
         // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;

          return $result;
        }
       
   }

}