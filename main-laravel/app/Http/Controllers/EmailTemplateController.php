<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Category;
use App\Newsletters;
use Request;
use App\AdRating;
use App\UserAdComments;
use App\NewsletterModuleTriggers;


class EmailTemplateController extends Controller
{

  public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        // get row set
        $result = $this->doList();
        // dd($result);
        // assign vars to template
        $this->data['rows'] = $result['rows'];

        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        $this->data['refresh_route'] = url('admin/email-template/refresh');
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "Email Templates";
        $this->data['module_triggers'] = NewsletterModuleTriggers::where('status',1)->get();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        return view('admin.email-template-management.list', $this->data);

    
    }

    /**
     * Build the list
     *
     */
    public function doList() {

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search') ? : '';
      $status = Request::input('status') ? : '';

      $per = Request::input('per') ?: 10;
      // dd(Request::all());
        // if (!Auth::check()) {
        //     // return response (format accordingly)
        //     if(Request::ajax()) {
        //         return Response::json(['relogin' => "Relogin"]);
        //     }
        // }

        if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Newsletters::where(function($query) use ($search) {
                                        $query->where('newsletter_name', 'LIKE', '%' . $search . '%');
                             })
                            ->where(function($query) use ($status) {
                                        $query->where('newsletter_status', 'LIKE', '%' . $status . '%');
                             })
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      } else {
        $count = Newsletters::where(function($query) use ($search) {
                                        $query->where('newsletter_name', 'LIKE', '%' . $search . '%');
                             })
                            ->where(function($query) use ($status) {
                                        $query->where('newsletter_status', 'LIKE', '%' . $status . '%');
                             })
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Newsletters::where(function($query) use ($search) {
                                        $query->where('newsletter_name', 'LIKE', '%' . $search . '%');
                             })
                            ->where(function($query) use ($status) {
                                        $query->where('newsletter_status', 'LIKE', '%' . $status . '%');
                             })
                            ->orderBy($result['sort'], $result['order'])
                            ->paginate($per);

      }
      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
      $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
        if (is_null(Newsletters::find($id))) {
            return abort(404);
        } else {
            $this->data['newsletters'] = Newsletters::find($id);

            $this->data['title'] = $this->data['newsletters']->title;
            $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
            $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

            return view('admin.email-template-management.edit', $this->data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function save() {    

        $input = Request::all();
        //dd($input);
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Newsletters::find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'title' => 'required',
            'newsletter_name' => 'unique:newsletters',
            'body' => 'required',
            'subject' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'title',
            'body' => 'body',
            'subject' => 'subject',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Newsletters;
            $row->module_trigger_id     =  array_get($input, 'module_trigger');
            $row->newsletter_name     =  str_slug(array_get($input, 'newsletter_name'), '_');
            $row->title     = array_get($input, 'title');
            $row->subject   = array_get($input, 'subject');
            $row->body      = array_get($input, 'body');
        }else{
            $row->title     = array_get($input, 'title');
            $row->subject   = array_get($input, 'subject');
            $row->body      = array_get($input, 'body');
        }
        
        // save model
             $row->save();

        // return
        return Response::json(['body' => 'News successfully added.']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
        return view('news.create', $this->data);
    }

       /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

   public function view() {
    $row = Newsletters::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
       /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
  public function enable(){
   $row = Newsletters::find(Request::input('id'));
      if(!is_null($row)){
          $row->newsletter_status = 1;
          $row->save();
           return Response::json(['body' => "Template Enabled"]);
      }else{
          return Response::json(['error' => "Invalid row specified"]);
      }
  }
  public function disable(){
    $row = Newsletters::find(Request::input('id'));
      if(!is_null($row)){
          $row->newsletter_status = 2;
          $row->save();
           return Response::json(['body' => "Template Disabled"]);
      }else{
          return Response::json(['error' => "Invalid row specified"]);
      }
  }
  public function changeStatus(){

    $row = Newsletters::find(Request::input('id'));
    if(!is_null($row)){
      $row->newsletter_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Promotion status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
}