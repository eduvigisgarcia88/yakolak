<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Request;
use Input;
use Validator;
use Paginator;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Country;
use App\City;
use App\AdRating;
use App\UserAdComments;
use App\Localization;
use App\CountryBannerPlacement;
use App\BannerPlacementPlans;
use App\BannerDefault;
use App\Category;
use App\SubCategory;
use App\SubCategoriesTwo;
use App\ThemeCountry;
use App\CountryBannerPlacementRates;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\Pages;
class CountryController extends Controller
{
  
  public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
      $result = $this->doList();
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['title'] = "Country Management";
      $this->data['countries'] = Country::where('status','!=',5)->orderBy('countryName','asc')->get();
      $this->data['refresh_route'] = url("admin/country/refresh");
      $this->data['country_themes'] = ThemeCountry::where('status','!=',5)->where('status',1)->get();
      $this->data['localization'] = Localization::where('status',1)->orderBy('name','asc')->get();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
        $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.country-management.list', $this->data);
  }
  public function doList(){

        $result['sort'] = Request::input('sort') ?: 'countries.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search') ? : '';
        $status = Request::input('status') ? : '';
        $per = Request::input('per') ?:10;

        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });

          $rows = Country::leftjoin('localization','localization.id','=','countries.localization')
                          ->leftjoin('theme_localization','theme_localization.id','=','countries.theme')
                          ->select('localization.name','countries.*','theme_localization.name as country_theme')
                          ->where(function($query) use ($status) {
                                    $query->where('countries.status', 'LIKE', '%' . $status . '%');   
                                    })
                          ->where(function($query) use ($search) {
                                        $query->where('countries.countryCode', 'LIKE', '%' . $search . '%')
                                              ->orWhere('localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('theme_localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('countries.countryName', 'LIKE', '%' . $search . '%');
                           })
                           ->where('countries.status','!=',5)
                           ->where('countries.status','!=',4)
                           ->orderBy($result['sort'], $result['order'])
                           ->paginate($per);

        } else {
           $count = Country::leftjoin('localization','localization.id','=','countries.localization')
                          ->leftjoin('theme_localization','theme_localization.id','=','countries.theme')
                          ->select('localization.name','countries.*','theme_localization.name as country_theme')
                          ->where(function($query) use ($status) {
                                    $query->where('countries.status', 'LIKE', '%' . $status . '%');   
                                    })
                          ->where(function($query) use ($search) {
                                        $query->where('countries.countryCode', 'LIKE', '%' . $search . '%')
                                              ->orWhere('localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('theme_localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('countries.countryName', 'LIKE', '%' . $search . '%');
                           })
                           ->where('countries.status','!=',5)
                           ->where('countries.status','!=',4)
                           ->orderBy($result['sort'], $result['order'])
                           ->paginate($per);

          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

            $rows = Country::leftjoin('localization','localization.id','=','countries.localization')
                          ->leftjoin('theme_localization','theme_localization.id','=','countries.theme')
                          ->select('localization.name','countries.*','theme_localization.name as country_theme')
                          ->where(function($query) use ($status) {
                                    $query->where('countries.status', 'LIKE', '%' . $status . '%');   
                                    })
                          ->where(function($query) use ($search) {
                                        $query->where('countries.countryCode', 'LIKE', '%' . $search . '%')
                                              ->orWhere('localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('theme_localization.name', 'LIKE', '%' . $search . '%')
                                              ->orWhere('countries.countryName', 'LIKE', '%' . $search . '%');
                           })
                           ->where('countries.status','!=',5)
                           ->where('countries.status','!=',4)
                           ->orderBy($result['sort'], $result['order'])
                           ->paginate($per);
        }

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        } 
  }
  public function changeStatus(){
      $row = Country::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = Request::input('stat');
        $row->save();
         return Response::json(['body' => ['Country status has been changed']]);
     }else{
        return Response::json(['error' => ['error Finding the id']]);
      }
  }
  public function saveCity(){
       $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = City::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'country'           => 'required',
            'city_name'      => 'required|unique:country_cities,name'. (!$new ? ',' . $row->id : ''),
           
        ];
        // field name overrides
        $names = [
            'country'              => 'Country',
            'city_name'      => 'City Name',

           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new City;
            $row->country_id      = array_get($input, 'country');
            $row->name      = array_get($input, 'city_name');
            $row->status      = array_get($input, 'status');
        } else {
            $row->country_id      = array_get($input, 'country');
            $row->name      = array_get($input, 'city_name');
            $row->status      = array_get($input, 'status');
        
        }   
            $row->save();
       if($new){
         return Response::json(['body' => ['New City Added']]);
       }else{
         return Response::json(['body' => ['City Updated']]);
       }
  }
  public function save(){
      $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Country::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            // 'country_code'           => 'required|unique:countries,countryCode'. (!$new ? ',' . $row->id : ''),
            'country_currency'         => 'required',
            // 'country_name'      => 'required|unique:countries,countryName'. (!$new ? ',' . $row->id : ''),
            'country_localization'   => 'required',
            'country_theme'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'country_code'              => 'Code',
            'country_currency'      => 'Currency',
            'country_name'    => 'Name',
            'country_localization'       => 'Localization',
            'country_theme'   => 'Theme',

           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new Country;
            $row->countryCode      = strip_tags(array_get($input, 'country_code'));
            $row->currency      = strip_tags(array_get($input, 'country_currency'));
            $row->countryName     = array_get($input, 'country_name');
            $row->localization     = array_get($input, 'country_localization');
            $row->theme     = array_get($input, 'country_theme');
            $row->status     = array_get($input, 'status');
        } else {
            $row->countryCode      = strip_tags(array_get($input, 'country_code'));
            $row->currency      = strip_tags(array_get($input, 'country_currency'));
            $row->countryName     = array_get($input, 'country_name');
            $row->localization     = array_get($input, 'country_localization');
            $row->theme     = array_get($input, 'country_theme');
            $row->status     = array_get($input, 'status');
        }   
            $row->save();

            if($new){

              $getPages = Pages::where('type',2)->where('status',1)->get();
              foreach ($getPages as $key => $field) {
                $savePages = new Pages;
                $savePages->name = $field->name;
                $savePages->content = $field->content;
                $savePages->country_id = $row->id;
                $savePages->slug = $field->slug;
                $savePages->type = 1; 
                $savePages->save();
              }
              $getCategories = Category::where('default_category',1)->where('status',3)->where('country_code','DE')->get();
                foreach ($getCategories as $key => $field) {
                  $saveCategories = new Category;
                  $saveCategories->name = $field->name;
                  $saveCategories->slug = $field->slug;
                  $saveCategories->icon = $field->icon;
                  $saveCategories->country_code = $row->countryCode;
                  $saveCategories->icon_big = $field->icon_big;
                  $saveCategories->home = $field->home;
                  $saveCategories->biddable_status = $field->biddable_status;
                  $saveCategories->buy_and_sell_status = $field->buy_and_sell_status;
                  $saveCategories->save();
                  $saveCategories->unique_id = base64_encode($saveCategories->id.'-'.$field->unique_id.'-'.$row->countryCode);
                  $saveCategories->save();
                  
                   // $getCountryCategory = Category::where('country_code',$row->countryCode)->first();
                  
                  // duplicating categories custom attributes (sticky) - start
                    $get_cat_attri = SubAttributes::where('attri_id', $field->unique_id)->get();
                    foreach($get_cat_attri as $cat_key => $cat_val) {
                      $custom_attri = new SubAttributes;
                      $custom_attri->attri_id = $saveCategories->unique_id;
                      $custom_attri->name = $cat_val->name;
                      $custom_attri->sequence_no = $cat_val->sequence_no;
                      $custom_attri->status = $cat_val->status;
                      $custom_attri->attribute_type = $cat_val->attribute_type;
                      $custom_attri->status = $cat_val->status;
                      $custom_attri->save();
                      
                      $get_cat_attri_values = CustomAttributeValues::where('sub_attri_id', $cat_val->id)->get();
                      if($get_cat_attri_values)
                      {
                        foreach($get_cat_attri_values as $attri_key => $attri_val) {
                          $copy_attri_values = new CustomAttributeValues;
                          $copy_attri_values->sub_attri_id = $custom_attri->id;
                          $copy_attri_values->name = $attri_val->name;
                          $copy_attri_values->status = $attri_val->status;
                          $copy_attri_values->save();
                        }
                      }
                    }
                  // duplicating categories custom attributes (sticky) - end
                    $getBanners = BannerPlacementPlans::where('status',1)->get();
                    foreach ($getBanners as $key => $field_banner_default) { 
                       $saveBannerDefault = new BannerDefault;
                       $saveBannerDefault->country_id = $row->id;
                       $saveBannerDefault->placement_id = $field_banner_default->id;
                       $saveBannerDefault->image_type = $field_banner_default->image_type;
                       $saveBannerDefault->image_desktop = $field_banner_default->image_desktop;
                       $saveBannerDefault->image_mobile = $field_banner_default->image_mobile;
                       $saveBannerDefault->target_url = $field_banner_default->target_url;
                       $saveBannerDefault->page_location = $field_banner_default->location_id;
                       if($field_banner_default->location_id != 1){
                          $saveBannerDefault->category_id = $saveCategories->unique_id;
                       }
                       $saveBannerDefault->save();
                     }
   
                    foreach ($getBanners as $key => $field_banner_plan) { 
                       $savePlacement = new CountryBannerPlacement;
                       $savePlacement->country_id = $row->id;
                       $savePlacement->placement_id = $field_banner_plan->id;

                       if($field_banner_plan->location_id != 1){
                          $savePlacement->category_id = $saveCategories->unique_id;
                       }

                       $savePlacement->daily_cost_paid = $field_banner_plan->daily_cost_paid;
                       $savePlacement->monthly_cost_paid = $field_banner_plan->monthly_cost_paid;
                       $savePlacement->yearly_cost_paid = $field_banner_plan->yearly_cost_paid;
                       $savePlacement->save();
                    }
                      $getSubCategories = SubCategory::where('status',1)->where('cat_id',$field->unique_id)->get();

                      foreach ($getSubCategories as $key => $field_sub_cat_1) {
                              $saveSubCategories = new SubCategory;
                              $saveSubCategories->cat_id = $saveCategories->unique_id;
                              $saveSubCategories->slug = $field_sub_cat_1->slug;
                              $saveSubCategories->name = $field_sub_cat_1->name;
                              $saveSubCategories->description = $field_sub_cat_1->description;
                              $saveSubCategories->country_code = strip_tags(strtolower(array_get($input, 'country_code')));
                              $saveSubCategories->save();
                              $saveSubCategories->unique_id = base64_encode($saveSubCategories->id.'-'.$field_sub_cat_1->unique_id.'-'.$saveCategories->country_code);
                              $saveSubCategories->save();
                        
                              // duplicating sub categories custom attributes - start
                              $get_sub_cat_attri = SubAttributes::where('attri_id', $field_sub_cat_1->unique_id)->get();
                              foreach($get_sub_cat_attri as $sub_cat_key => $sub_cat_val) {
                                $sub_custom_attri = new SubAttributes;
                                $sub_custom_attri->attri_id = $saveSubCategories->unique_id;
                                $sub_custom_attri->name = $sub_cat_val->name;
                                $sub_custom_attri->sequence_no = $sub_cat_val->sequence_no;
                                $sub_custom_attri->status = $sub_cat_val->status;
                                $sub_custom_attri->attribute_type = $sub_cat_val->attribute_type;
                                $sub_custom_attri->status = $sub_cat_val->status;
                                $sub_custom_attri->save();

                                $get_sub_cat_attri_values = CustomAttributeValues::where('sub_attri_id', $sub_cat_val->id)->get();
                                if($get_sub_cat_attri_values)
                                {
                                  foreach($get_sub_cat_attri_values as $sub_attri_key => $sub_attri_val) {
                                    $copy_sub_attri_values = new CustomAttributeValues;
                                    $copy_sub_attri_values->sub_attri_id = $sub_custom_attri->id;
                                    $copy_sub_attri_values->name = $sub_attri_val->name;
                                    $copy_sub_attri_values->status = $sub_attri_val->status;
                                    $copy_sub_attri_values->save();
                                  }
                                }
                              }
                               // duplicating sub categories custom attributes - end
                                        
                              $getSubCategoriestwo = SubCategoriesTwo::where('status',1)->where('subcat_main_id',$field_sub_cat_1->unique_id)->get();
                            
                              foreach ($getSubCategoriestwo as $key => $field_sub_cat_2) {
                                    $saveSubCategoriesTwo = new SubCategoriesTwo;
                                    $saveSubCategoriesTwo->subcat_main_id = $saveSubCategories->id; 
                                    $saveSubCategoriesTwo->slug = $field_sub_cat_2->slug;
                                    $saveSubCategoriesTwo->name = $field_sub_cat_2->name;
                                    $saveSubCategoriesTwo->save();
                                    $saveSubCategoriesTwo->unique_id = base64_encode($saveSubCategoriesTwo->id.'-'.$field_sub_cat_2->unique_id.'-'.$saveCategories->country_code);
                                    $saveSubCategoriesTwo->save();

                              }
                      }
               }
              
            }
            
       if($new){
             return Response::json(['body' => ['New Country Added']]);
         }else{
             return Response::json(['body' => ['Country Updated']]);
       }
     
  }
  public function removeCountry(){
    $row = Country::find(Request::input('id'));
    if(!is_null($row)){
      $row->status =5;
      $row->save();
      BannerDefault::where('country_id',$row->id)->update(['status' => 2]);
      CountryBannerPlacement::where('country_id',$row->id)->update(['status' => 2]);
      Category::where('country_code',$row->countryCode)->update(['status' => 3]);
      return Response::json(['body' => ['Country has been deleted']]);
    }else{
      return Response::json(['error' => ['error Finding the id']]);
    }
  }
  public function manipulate(){
    $row = Country::find(Request::input('id'));
    if(!is_null($row)){
      return Response::json($row);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function enableCountry(){
    $row = Country::find(Request::input('id'));
    if(!is_null($row)){
       Country::where('id',$row->id)->update(['status' => 1]);
       City::where('country_id',$row->id)->update(['status' => 1]);
      return Response::json(['body' => ['Country has been enabled']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function disableCountry(){
    $row = Country::find(Request::input('id'));
    if(!is_null($row)){
      Country::where('id',$row->id)->update(['status' => 2]);
      City::where('country_id',$row->id)->update(['status' => 2]);
      return Response::json(['body' => ['Country has been disabled']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function removeCity(){
    $row = City::find(Request::input('id'));
      if(!is_null($row)){
        $row->status=5 ;
        $row->save();
        return Response::json(['body' => ['City has been deleted']]);
      }else{
        return Response::json(['error' => ['erro Finding the id']]);
      }
  }
   public function editCity(){
    $row = City::find(Request::input('id'));
      if(!is_null($row)){
        return Response::json($row);
      }else{
        return Response::json(['error' => ['erro Finding the id']]);
      }
  }
  public function enableCity(){
    $row = City::find(Request::input('id'));
      if(!is_null($row)){
       $row->status=1;
       $row->save();
       return Response::json(['body' => ['City has been enabled']]);
      }else{
        return Response::json(['error' => ['erro Finding the id']]);
      }
  }
  public function disableCity(){
    $row = City::find(Request::input('id'));
       if(!is_null($row)){
           $row->status=2;
           $row->save();
           return Response::json(['body' => ['City has been disabled']]);
          }else{
            return Response::json(['error' => ['erro Finding the id']]);
          }
  }
  public function getTheme(){
    $row = Localization::where('status',1)->where('id',Request::input('id'))->first();
    if(!is_null($row)){
      return Response::json($row);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }

  }
  public function getCities(){
  
      $id = Request::input('id');
      $row = City::where('country_id',$id)->where('status','!=',5)->orderBy('name','asc')->get();
      $getCountryStatus = Country::find($id);
      $rows = '<div class="table-responsive" id="table-comment-replies"><table class="table" style="margin-bottom:10px !important;background-color:transparent;">' .
                      '<thead>' . 
                      '<th><small>Status</small></th>' .
                      '<th><small>Name</small></th>' .
                      '<th><small>Created Date</small></th>' .
                      '<th class="text-right"><small>Tools<small></th><tbody></tr>';

        foreach ($row as $key => $field) {
             $rows .= '<tr data-id='.$field->id.' class="'.($field->status == 2 ? 'disabledColorCity':'').''.($field->status == 3 ? 'unapprovedColor':'').'">' .
                                 '<td><small>'.($field->status == 1 ? 'Enabled':'Disabled').'</small></td>' .
                                 '<td><small>'.$field->name.'</small></td>' .
                                 '<td><small>'.$field->created_at.'</small></td>'. 
                                 '<td>';      
             $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-city"><i class="fa fa-trash-o"></i></button>';
             $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-city"><i class="fa fa-pencil"></i></button>';
             // $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-city"><i class="fa fa-eye"></i></button>';

             if($field->status == 1){
                  $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable-city"><i class="fa fa-lock"></i></button>';
             }else{
                  $rows .= '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable-city"'.($getCountryStatus->status == 2 ? 'disabled':'').' ><i class="fa fa-unlock"></i></button>';
             }
         }

         return Response::json($rows);

  }
}