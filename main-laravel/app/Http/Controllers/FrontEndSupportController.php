<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use Session;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use Carbon\Carbon;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\AdRating;
use App\UserAdComments;
use App\Advertisement;
use App\ContactDetails;
use App\Pages;

class FrontEndSupportController extends Controller
{

	public function index(){
		$this->data['title'] = "Support";


  if (Auth::check()) {
      $user_type_id = Auth::user()->usertype_id;
      $created = new Carbon(Auth::user()->created_at);
      $now = Carbon::now();

      $difference = ($created->diff($now)->days < 1)
          ? 'today'
          : $created->diffForHumans($now);
          //check if days not months
          $arr = explode(' ',trim($difference));
          if ($arr[1] == 'days') {
           $diff = '1';
           $test = 'true';
          } elseif ($arr[1] == 'years' || $arr[1] == 'year') {
           $diff = '12';
          }
       else {
       $diff = substr($difference, 0, 1); 
       }


  } else {
      $user_type_id = '1';
      $diff = null;
  }

$banner_placement = BannerPlacement::all();

$bannerRates ="";
foreach ($banner_placement as $value) {
$bannerRates .='<div class="col-md-4">
                        <div class="panel panel-default borderZero"><div class="panel-heading text-center"><h4 class="blueText">'.$value->name.'</h4></div>
                          <div class="panel-body"><div class="text-center borderbottomLight"><div class="normalText lightgrayText">ADVERTISEMENT PLAN</div>
                             <h6 class="grayText"></h6>       
                               </div><div class="blockTop">';
$bannerplan_id = $value->id;
     if (Auth::check()) {
               $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                        ->select('banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period')
                                        ->where(function($query)  use($bannerplan_id, $user_type_id){
                                                $query->where('banner_placement_id', '=', $bannerplan_id)
                                                      ->where('usertype_id', '=', $user_type_id);
                                        })->get();
                              
           foreach ($get_banner_plan as $features) {
              $bannerRates .='<div class="normalText grayText"><span class=""><i class="fa fa-check-square rightMargin"></i> $';
                  
                          if ($diff <= $features->duration || !($features->duration == 0) && Auth::check() && !($diff == null)) { 
                                      if (!($features->discount == 0)) {
                                             $inti_discount = 100 - $features->discount;
                                             $total_discount = '.'.$inti_discount;
                                             $total_price = $features->price * $total_discount;
                                             $test = 'test1';
                                      } else {
                                         $total_price = $features->price; 
                                      }
                          } else {
                           $total_price = $features->price; 
                          }
              $bannerRates .=''.$total_price.' FOR '.$features->period.($features->period > 1 ? ' MONTHS':' MONTH').' </span></div>';
            }

     } else {
             $get_usertypes = Usertypes::all();
          
             foreach ($get_usertypes as $usertypes) {

             $usertype_id = $usertypes->id;
             $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                        ->leftjoin('usertypes','banner_placement_plans.usertype_id','=','usertypes.id')
                                        ->select('usertypes.type_name as type_name','banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period', 'banner_placement_plans.id as banner_plan_id')
                                        ->where(function($query)  use($bannerplan_id, $usertype_id){
                                                $query->where('banner_placement_id', '=', $bannerplan_id)
                                                      ->where('banner_placement_plans.usertype_id', '=', $usertype_id);
                                        })->get();
             
              $bannerRates .='<div class="normalText blueText"><h5>'.$usertypes->type_name.'</h5></div><h6 class="grayText"></h6> ';
           
              foreach ($get_banner_plan as $features) {
                  $bannerRates .='<div class="normalText grayText"><span class=""><i class="fa fa-check-square rightMargin"></i> $'.$features->price.' FOR '.$features->period.($features->period > 1 ? ' MONTHS':' MONTH').'</span></div>';
               }
           }
     }
$bannerRates .='</div></div><div class="panel-footer"><a href="#'.$value->id.'"><span class="normalText">Jump To Plan Details<span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span></span></a></div></div></div>';
 }                    

 //section
foreach ($banner_placement as $value) {
$bannerplan_id = $value->id;  
$bannerRates .='<div id="'.$value->id.'" class="col-md-12">
                        <div class="panel panel-default borderZero">
                        <div class="panel-heading"><span class="xlargeText blueText">'.$value->name.'</span><span class="pull-right"><button data-plan="6" data-id="1" data-title="Bronze" class="btn btn-sm blueButton borderZero noMargin btn-plan-subscription-upgrade" type="submit">GET THIS PLAN</button></span></div>
                          <div class="panel-body"><div class="text-center borderbottomLight"><div class="normalText lightgrayText">ADVERTISEMENT PLAN FOR</div>  </div><div class="blockTop">  <h5 class="grayText">';

         $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                    ->leftjoin('usertypes','banner_placement_plans.usertype_id','=','usertypes.id')
                                    ->select('usertypes.type_name as type_name','banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period', 'banner_placement_plans.id as banner_plan_id', 'banner_placement_plans.points')
                                    ->where(function($query)  use($bannerplan_id, $user_type_id){
                                            $query->where('banner_placement_id', '=', $bannerplan_id);
                                    })->get();
         foreach ($get_banner_plan as $features) {
              $bannerRates .='<div class="normalText grayText"><span class="">
              <i class="fa fa-check-square rightMargin"></i>'.$features->points.($features->points > 1 ? ' POINTS':' POINT').'</span></div>';
         }
       
$bannerRates .='</h5></div></div></div></div>';
}

 		$this->data['bannerRates']  = $bannerRates;
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['categories']   = Category::where('status',1)->get();
	  $this->data['countries']    = Country::all();
    $this->data['usertypes']    = Usertypes::all();

 if(Auth::check()){
     $user_id = Auth::user()->id;
 }else {
     $user_id = null;
 }
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
      $this->data['notifications'] = UserNotificationsHeader::join('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                       ->leftjoin('users', 'users.id', '=', 'user_notifications_header.reciever_id')    
                                       ->select('user_notifications.*', 'user_notifications_header.*','users.photo') 
                                       ->where('user_notifications.status',1)
                                       ->where('user_notifications_header.read_status',1)
                                       ->where(function($query) use ($user_id){
                                             $query->where('user_notifications_header.reciever_id', '=', $user_id);                                        
                                        })->orderBy('user_notifications_header.created_at', 'desc')->get();
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();

      $this->data['contact_details'] = ContactDetails::first();
      
      // $this->data['contact_details'] = Pages::where('status',1)->get();

       if(Session::has('selected_location') == null){
            $selectedCountry = Session::get('default_location');
       }else{
            $selectedCountry  = Session::get('selected_location');
       }
       if($selectedCountry == "ALL" || $selectedCountry == null ){
            $selectedCountry = "US";
            Session::put('default_location','US');
       } 


       $getCountryID = Country::where('countryCode',$selectedCountry)->where('status',1)->first();
       $this->data['page_help_center'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','help-center')
                                    ->first();

       $this->data['page_call_us'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','call-us')
                                    ->first();
       $this->data['page_terms_and_conditions'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','terms-and-conditions')
                                    ->first();
       $this->data['page_faq'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','faq')
                                    ->first();
       $this->data['page_company_information'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','company-information')
                                    ->first();
       $this->data['page_mission_and_vision'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','mission-and-vision')
                                    ->first();
       $this->data['page_sitemap'] = Pages::where('status',1)
                                    ->where('country_id',$getCountryID->id)
                                    ->where('slug','=','sitemap')
                                    ->first();      



      // dd($this->data['pages']);                                       
		return View::make('support.list', $this->data);
	}

	public function showContactUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
		return View::make('support.contact-us', $this->data);
	}

	public function showAboutUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      
      // YAH IT WORKS TILL THE END!!!!
		return View::make('support.about-us', $this->data);
	}
	public function showCallUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
		return View::make('support.call-us', $this->data);
	}
	public function showFAQS(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
    
      // YAH IT WORKS TILL THE END!!!!
		return View::make('support.faqs', $this->data);
	}
	public function showAdvertising(){


		
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      $master_poster_total = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                          ->where('ads_type_id',1)
                                          ->where('status',1)
                                          ->groupBy('user_id')
                                          ->orderBy('count', 'desc')
                                          ->take(4)
                                          ->get();
      $master_poster_ads = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                        ->where('ads_type_id',1)
                                        ->where('status',1)
                                        ->groupBy('user_id')
                                        ->orderBy('count', 'desc')
                                        ->take(4)
                                        ->get();
      $master_poster_auctions = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                              ->where('ads_type_id',2)
                                              ->where('status',1)
                                              ->groupBy('user_id')
                                              ->orderBy('count', 'desc')
                                              ->take(4)->get();
      $master_poster_0 = User::where('id',$master_poster_total[0]['user_id'])->first();
      $master_poster_1 = User::where('id',$master_poster_total[1]['user_id'])->first();
      $master_poster_2 = User::where('id',$master_poster_total[2]['user_id'])->first();
      $master_poster_3 = User::where('id',$master_poster_total[3]['user_id'])->first();
      if($master_poster_total[0]['count'] > 1){
        if(!is_null($master_poster_0)){
          $master_poster_data_0 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_0->alias.'"><img src="'.url('uploads').'/'.$master_poster_0->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_0->alias.'" class="blueText">'.$master_poster_0->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[0]['count'].' Ads & '.$master_poster_auctions[0]['count'].' Bids Listings</p></div></div></div>';
        }else{
            $master_poster_data_0 = '';
        }
      }else{
        $master_poster_data_0 = '';
      }
      if($master_poster_total[1]['count'] > 1){
        if(!is_null($master_poster_1)){
          $master_poster_data_1 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_1->alias.'"><img src="'.url('uploads').'/'.$master_poster_1->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_1->alias.'" class="blueText">'.$master_poster_1->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[1]['count'].' Ads & '.$master_poster_auctions[1]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_1 = '';
        }
      }else{
        $master_poster_data_1 = '';
      }
      if($master_poster_total[2]['count'] > 1){
        if(!is_null($master_poster_2)){
          $master_poster_data_2 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_2->alias.'"><img src="'.url('uploads').'/'.$master_poster_2->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_2->alias.'" class="blueText">'.$master_poster_2->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[2]['count'].' Ads & '.$master_poster_auctions[2]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_2 = '';
        }
      }else{
        $master_poster_data_2 = '';
      }
      if($master_poster_total[3]['count'] > 1){
        if(!is_null($master_poster_3)){
          $master_poster_data_3 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_3->alias.'"><img src="'.url('uploads').'/'.$master_poster_3->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_3->alias.'" class="blueText">'.$master_poster_3->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[3]['count'].' Ads & '.$master_poster_auctions[3]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_3 = '';
        }
      }else{
        $master_poster_data_3 = '';
      }
      $this->data['master_poster_html_0'] = $master_poster_data_0;
      $this->data['master_poster_html_1'] = $master_poster_data_1;
      $this->data['master_poster_html_2'] = $master_poster_data_2;
      $this->data['master_poster_html_3'] = $master_poster_data_3;
      // YAH IT WORKS TILL THE END!!!!
		return View::make('support.advertising', $this->data);
	}
	public function showHelpUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      // NOT THE CLEANEST CODE, BUT IT WORKS!!!!!
      $master_poster_total = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                          ->where('ads_type_id',1)
                                          ->where('status',1)
                                          ->groupBy('user_id')
                                          ->orderBy('count', 'desc')
                                          ->take(4)
                                          ->get();
      $master_poster_ads = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                        ->where('ads_type_id',1)
                                        ->where('status',1)
                                        ->groupBy('user_id')
                                        ->orderBy('count', 'desc')
                                        ->take(4)
                                        ->get();
      $master_poster_auctions = Advertisement::selectRaw('user_id, COUNT(*) as count')
                                              ->where('ads_type_id',2)
                                              ->where('status',1)
                                              ->groupBy('user_id')
                                              ->orderBy('count', 'desc')
                                              ->take(4)->get();
      $master_poster_0 = User::where('id',$master_poster_total[0]['user_id'])->first();
      $master_poster_1 = User::where('id',$master_poster_total[1]['user_id'])->first();
      $master_poster_2 = User::where('id',$master_poster_total[2]['user_id'])->first();
      $master_poster_3 = User::where('id',$master_poster_total[3]['user_id'])->first();
      if($master_poster_total[0]['count'] > 1){
        if(!is_null($master_poster_0)){
          $master_poster_data_0 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_0->alias.'"><img src="'.url('uploads').'/'.$master_poster_0->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_0->alias.'" class="blueText">'.$master_poster_0->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[0]['count'].' Ads & '.$master_poster_auctions[0]['count'].' Bids Listings</p></div></div></div>';
        }else{
            $master_poster_data_0 = '';
        }
      }else{
        $master_poster_data_0 = '';
      }
      if($master_poster_total[1]['count'] > 1){
        if(!is_null($master_poster_1)){
          $master_poster_data_1 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_1->alias.'"><img src="'.url('uploads').'/'.$master_poster_1->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_1->alias.'" class="blueText">'.$master_poster_1->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[1]['count'].' Ads & '.$master_poster_auctions[1]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_1 = '';
        }
      }else{
        $master_poster_data_1 = '';
      }
      if($master_poster_total[2]['count'] > 1){
        if(!is_null($master_poster_2)){
          $master_poster_data_2 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_2->alias.'"><img src="'.url('uploads').'/'.$master_poster_2->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_2->alias.'" class="blueText">'.$master_poster_2->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[2]['count'].' Ads & '.$master_poster_auctions[2]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_2 = '';
        }
      }else{
        $master_poster_data_2 = '';
      }
      if($master_poster_total[3]['count'] > 1){
        if(!is_null($master_poster_3)){
          $master_poster_data_3 = '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding"><div class="commentBlock"><div class="commenterImage"><a href="'.url().'/'.$master_poster_3->alias.'"><img src="'.url('uploads').'/'.$master_poster_3->photo.'" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"</a></div><div class="statisticcommentText"><div class="panelTitleB bottomPadding"><a href="'.url().'/'.$master_poster_3->alias.'" class="blueText">'.$master_poster_3->name.'</a></div><p class="normalText" style="margin-left:46px;">'.$master_poster_ads[3]['count'].' Ads & '.$master_poster_auctions[3]['count'].' Bids Listings</p></div></div></div>';
        }else{
          $master_poster_data_3 = '';
        }
      }else{
        $master_poster_data_3 = '';
      }
      $this->data['master_poster_html_0'] = $master_poster_data_0;
      $this->data['master_poster_html_1'] = $master_poster_data_1;
      $this->data['master_poster_html_2'] = $master_poster_data_2;
      $this->data['master_poster_html_3'] = $master_poster_data_3;
      // YAH IT WORKS TILL THE END!!!!
		return View::make('support.help-us', $this->data);
	}



}
   