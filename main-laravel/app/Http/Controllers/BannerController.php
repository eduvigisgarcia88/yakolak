<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use Request;
use Response;
use Input;
use Image;
use Validator;
use Carbon\Carbon;
use Paginator;
use App\Usertypes;
use App\AdRating;
use App\UserAdComments;
use App\Country;
use App\BannerPlacementPages;
use App\CountryBannerPlacement;
use App\BannerDefault;
use App\BannerPlacementLocation;
use App\BannerAdvertisementSubscriptions;
use App\UserNotifications;
use App\UserNotificationsHeader;

//
class BannerController extends Controller
{
  public function indexBannerDefault(){
     $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
    $result = $this->doListBannerDefault();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['banner_placement_location'] = BannerPlacementLocation::where('status',1)->get();
    $this->data['title'] = "Banner Management - Banner Defaults";
    $this->data['refresh_route'] = url("admin/banner-default/refresh");
    $this->data['categories'] = Category::where('default_category',1)->get();
    $this->data['countries'] = Country::where('status','!=',5)->where('status',1)->orderBy('countryName','asc')->get();
    return View::make('admin.banner-management.banner-default.list', $this->data);

  }
  public function doListBannerDefault(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $country = Request::input('country') ? : Country::where('status',1)->pluck('id');
      $category = Request::input('category');
      $per = Request::input('per') ? :10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

       $rows = BannerDefault::leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_defaults.placement_id')
                                ->leftjoin('countries','countries.id','=','banner_defaults.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
                                ->leftjoin('categories','categories.unique_id','=','banner_defaults.category_id')
                                ->select('banner_defaults.*','banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','banner_placement_location.name','categories.name as categoryName','countries.countryCode') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
                                           ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                               })
                               ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                               })
                              ->where(function($query) use ($category) {
                                     if($category != ""){ 
                                        $query->where('categories.name','=',$category);
                                     }
                               })
                                ->where('banner_defaults.status',1)
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('banner_placement_plans.id')
                                ->paginate($per);
                        
                              
      } else {
         $count = BannerDefault::leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_defaults.placement_id')
                                ->leftjoin('countries','countries.id','=','banner_defaults.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
                                ->leftjoin('categories','categories.unique_id','=','banner_defaults.category_id')
                                ->select('banner_defaults.*','banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','banner_placement_location.name','categories.name as categoryName','countries.countryCode') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
                                           ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                               })
                              ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                                })
                              ->where(function($query) use ($category) {
                                    if($category != ""){ 
                                        $query->where('categories.name','=',$category);
                                     }
                               })
                                ->where('banner_defaults.status',1)
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('banner_placement_plans.id')
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = BannerDefault::leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_defaults.placement_id')
                                ->leftjoin('countries','countries.id','=','banner_defaults.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
                                ->leftjoin('categories','categories.unique_id','=','banner_defaults.category_id')
                                ->select('banner_defaults.*','banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','banner_placement_location.name','categories.name as categoryName','countries.countryCode') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
                                           ->orWhere('categories.name', 'LIKE', '%' . $search . '%');
                               })
                              ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                               })
                              ->where(function($query) use ($category) {
                                     if($category != ""){ 
                                        $query->where('categories.name','=',$category);
                                     }
                              })
                              ->where('banner_defaults.status',1)
                              ->orderBy($result['sort'], $result['order'])
                              ->groupBy('banner_placement_plans.id')
                              ->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  public function index(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
    $result = $this->doList();
    $this->data['rows'] = $result['rows'];
    $this->data['pages'] = $result['pages'];
    $this->data['title'] = "Banner Management";
    $this->data['refresh_route'] = url("admin/banner/refresh");
    $this->data['banner_placement'] = BannerPlacement::all();
    $this->data['countries'] = Country::where('status',1)->get();
    $this->data['categories'] = Category::where('status',3)->where('country_code','DE')->get();
    $this->data['banner_placement_location'] = BannerPlacementLocation::where('status',1)->get();
    $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)
                                                  ->get();

    $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)
                                                  ->get();

    $get_plan_details = BannerPlacementPlans::all();
          $banner_placement_plan = '<option class="hide" value="">Select:</option>';
        foreach ($get_plan_details as $key => $field) {
          $banner_placement_plan .= '<option value="' . $field->id . '">'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }
        
    $this->data['banner_placement_plan'] = $banner_placement_plan;
    return View::make('admin.banner-management.list', $this->data);
  }
  public function doList(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $country = Request::input('country');
      $banner_status = Request::input('banner_status');


      $per = Request::input('per') ? : 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = BannerAdvertisementSubscriptions::leftjoin('banner_placement_location','banner_placement_location.id','=','banner_advertisement_subscriptions.page')
                                                        ->leftjoin('countries','countries.id','=','banner_advertisement_subscriptions.country_id')
                                                        ->leftjoin('categories','categories.unique_id','=','banner_advertisement_subscriptions.category')
                                                        ->select('banner_advertisement_subscriptions.*','banner_placement_location.name as placement_name','categories.name as category_name','countries.countryCode')
                                                         ->where(function($query) use ($search){
                                                              $query->where('banner_advertisement_subscriptions.amount','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_placement_location.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('categories.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('countries.countryCode','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.total_cost','LIKE','%'.'$'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.order_no','LIKE','%'.$search.'%');
                                                                    if(strtolower($search) == "pending"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',1);
                                                                    }else if(strtolower($search) == "approved"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',2);
                                                                    }else if(strtolower($search) == "rejected"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',3);
                                                                    }else if(strtolower($search) == "active"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',4);
                                                                    }else if(strtolower($search) == "disabled"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',6);
                                                                    }else if(strtolower($search) == "paid"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',1);
                                                                    }else if(strtolower($search) == "reserved"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',2);
                                                                    }
                                                                  $query->orWhere('banner_advertisement_subscriptions.id','LIKE','%'.$search.'%')
                                                                        ->orWhere('banner_advertisement_subscriptions.duration','LIKE','%'.$search.'%');
                                                        })
                                                       ->where(function($query) use ($country){
                                                          $query->where('countries.id','LIKE','%'.$country.'%');
                                                       })

                                                      ->where(function($query) use ($banner_status){
                                                          $query->where('banner_advertisement_subscriptions.reserve_status','LIKE','%'.$banner_status.'%');
                                                       })
                                                       ->where('banner_advertisement_subscriptions.status','!=',2)
                                                       ->orderBy($result['sort'],$result['order'])
                                                       ->paginate($per);
                            // dd($rows);
      } else {
         $count = BannerAdvertisementSubscriptions::leftjoin('banner_placement_location','banner_placement_location.id','=','banner_advertisement_subscriptions.page')
                                                        ->leftjoin('countries','countries.id','=','banner_advertisement_subscriptions.country_id')
                                                        ->leftjoin('categories','categories.unique_id','=','banner_advertisement_subscriptions.category')
                                                        ->select('banner_advertisement_subscriptions.*','banner_placement_location.name as placement_name','categories.name as category_name','countries.countryCode')
                                                         ->where(function($query) use ($search){
                                                              $query->where('banner_advertisement_subscriptions.amount','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_placement_location.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('categories.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('countries.countryCode','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.total_cost','LIKE','%'.'$'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.order_no','LIKE','%'.$search.'%');
                                                                    if(strtolower($search) == "pending"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',1);
                                                                    }else if(strtolower($search) == "approved"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',2);
                                                                    }else if(strtolower($search) == "rejected"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',3);
                                                                    }else if(strtolower($search) == "active"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',4);
                                                                    }else if(strtolower($search) == "disabled"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',6);
                                                                    }else if(strtolower($search) == "paid"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',1);
                                                                    }else if(strtolower($search) == "reserved"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',2);
                                                                    }
                                                                  $query->orWhere('banner_advertisement_subscriptions.id','LIKE','%'.$search.'%')
                                                                        ->orWhere('banner_advertisement_subscriptions.duration','LIKE','%'.$search.'%');
                                                        })
                                                       ->where(function($query) use ($banner_status){
                                                          $query->where('banner_advertisement_subscriptions.reserve_status','LIKE','%'.$banner_status.'%');
                                                       })
                                                       // ->where('banner_advertisement_subscriptions.user_id',Auth::user()->id)
                                                       ->where('banner_advertisement_subscriptions.status','!=',2)
                                                       ->orderBy($result['sort'],$result['order'])
                                                       ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = BannerAdvertisementSubscriptions::leftjoin('banner_placement_location','banner_placement_location.id','=','banner_advertisement_subscriptions.page')
                                                        ->leftjoin('countries','countries.id','=','banner_advertisement_subscriptions.country_id')
                                                        ->leftjoin('categories','categories.unique_id','=','banner_advertisement_subscriptions.category')
                                                        ->select('banner_advertisement_subscriptions.*','banner_placement_location.name as placement_name','categories.name as category_name','countries.countryCode')

                                                         ->where(function($query) use ($search){
                                                              $query->where('banner_advertisement_subscriptions.amount','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_placement_location.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('categories.name','LIKE','%'.$search.'%')
                                                                    ->orWhere('countries.countryCode','LIKE','%'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.total_cost','LIKE','%'.'$'.$search.'%')
                                                                    ->orWhere('banner_advertisement_subscriptions.order_no','LIKE','%'.$search.'%');
                                                                    if(strtolower($search) == "pending"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',1);
                                                                    }else if(strtolower($search) == "approved"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',2);
                                                                    }else if(strtolower($search) == "rejected"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',3);
                                                                    }else if(strtolower($search) == "active"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',4);
                                                                    }else if(strtolower($search) == "disabled"){
                                                                        $query->orWhere('banner_advertisement_subscriptions.banner_status',6);
                                                                    }else if(strtolower($search) == "paid"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',1);
                                                                    }else if(strtolower($search) == "reserved"){
                                                                       $query->orWhere('banner_advertisement_subscriptions.reserve_status',2);
                                                                    }
                                                                  $query->orWhere('banner_advertisement_subscriptions.id','LIKE','%'.$search.'%')
                                                                        ->orWhere('banner_advertisement_subscriptions.duration','LIKE','%'.$search.'%');
                                                        })
                                                       ->where(function($query) use ($banner_status){
                                                          $query->where('banner_advertisement_subscriptions.reserve_status','LIKE','%'.$banner_status.'%');
                                                       })
                                                       // ->where('banner_advertisement_subscriptions.user_id',Auth::user()->id)
                                                       ->where('banner_advertisement_subscriptions.status','!=',2)
                                                       ->orderBy($result['sort'],$result['order'])
                                                       ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  public function save(){

    $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerSubscriptions::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'                 => $new ? 'image|max:3000' : 'image|max:3000',
            'banner_name'           => 'required',
            'advertiser_name'         => 'required',
            'banner_placement_id'     => 'required',
            'banner_placement_plan'   => 'required',
            'cat_id'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'photo'              => 'photo',
            'banner_name'      => 'banner name',
            'advertiser_name'    => 'advertiser',
            'banner_placement_id'       => 'Banner placement',
            'banner_placement_plan'  => 'Banner placement plan',
            'cat_id'  => 'Category Id',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerSubscriptions;
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
        } else {
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->save();
        }   
            $row->save();
            $get_plan_duration = BannerPlacementPlans::find($row->placement_plan);
            $date = strtotime(date("Y-m-d H:i:s", strtotime($row->created_at)) . " +".$get_plan_duration->duration." month");
            $row->expiration = date("Y-m-d H:i:s", $date);
            $row->save();

          if (Input::file('photo')) {
              // Save the photo
              Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-'.$row->id . '.jpg');
              Image::make(Input::file('photo'))->fit(293, 390)->save('uploads/' . 'ads_banner-'.$row->id . '.jpg');
              $row->photo = 'banner-'.$row->id . '.jpg';
              $row->save();
          }

      return Response::json(['body' => ['Banner Advertisement Saved']]);

  }
    public function saveBannerDefault(){

    $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerDefault::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'photo'                 => $new ? 'image|max:3000' : 'image|max:3000',
            'target_url'           => 'required',
            'reserve_status'         => 'required',

           
        ];
        // field name overrides
        $names = [
            'photo'              => 'photo',
            'target_url'      => 'Target URL',
            'reserve_status'    => 'Reserve Status',

           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerDefault;
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->target_url      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
        } else {
            $row->target_url      = strip_tags(array_get($input, 'target_url'));
            $row->reserve_status     = array_get($input, 'reserve_status');
            $row->save();
        }   
            $row->save();
            if($row->image_type == "home_premium_A"){
             if (Input::file('photo')) {
                Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/main/' . 'MainPremiumA-'.$row->id . '.jpg');
                $row->image_desktop = 'MainPremiumA-'.$row->id . '.jpg';
                $row->save();
             }
            }else if($row->image_type == "home_premium_B"){
              if (Input::file('photo')) {
                Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/main/' . 'MainPremiumB-'.$row->id . '.jpg');
                $row->image_desktop = 'MainPremiumB-'.$row->id . '.jpg';
                $row->save();
             }
            }else if($row->image_type == "home_premium_C"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/main/' . 'Main_PremiumC-'.$row->id . '.jpg');
                  $row->image_desktop = 'Main_PremiumC-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "home_main_A"){
              if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/main/' . 'MainA-'.$row->id . '.jpg');
                  $row->image_desktop = 'MainA-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "home_main_B"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/main/' . 'MainB-'.$row->id . '.jpg');
                  $row->image_desktop = 'MainB-'.$row->id . '.jpg';
                  $row->save();
               }
                 ///end of jumboMain
            }else if($row->image_type == "sub1_A"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub1/' . 'Sub1A-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub1A-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "sub1_B"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub1/' . 'Sub2A-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2A-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "sub1_C"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub1/' . 'Sub3A-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub3A-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "sub1_premium_1"){
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub1/' . 'Sub2A-Premium1-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2A-Premium1-'.$row->id . '.jpg';
                  $row->save();
               }
            }else if($row->image_type == "sub1_premium_2"){
              if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub1/' . 'Sub2A-Premium1-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2A-Premium1-'.$row->id . '.jpg';
                  $row->save();
               }
               ///end of sub1
            }else if($row->image_type == "sub2_A"){
                if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub2/' . 'Sub1B-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub1B-'.$row->id . '.jpg';
                  $row->save();
                }
            }else if($row->image_type == "sub2_B"){ 
               if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub2/' . 'Sub2B-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2B-'.$row->id . '.jpg';
                  $row->save();
                }
            }else if($row->image_type == "sub2_C"){ 
                if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub2/' . 'Sub3B-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub3B-'.$row->id . '.jpg';
                  $row->save();
                }
            }else if($row->image_type == "sub2_premium_1"){ 
                if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub2/' . 'Sub2B-PremiumA-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2B-PremiumA-'.$row->id . '.jpg';
                  $row->save();
                }
            }else if($row->image_type == "sub2_premium_2"){ 
                if (Input::file('photo')) {
                  Image::make(Input::file('photo'))->fit(673, 367)->save('uploads/home_banners/sub2/' . 'Sub2B-PremiumB-'.$row->id . '.jpg');
                  $row->image_desktop = 'Sub2B-PremiumB-'.$row->id . '.jpg';
                  $row->save();
                } 
              ///end of sub2
            }else if($row->image_type == "browse_top"){
               if (Input::file('photo')) {
                Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/default_top-' . $row->id . '.jpg');
                Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/default_top-' . $row->id . '.jpg');
                $row->image_desktop = 'default_top-'.$row->id . '.jpg';
                $row->image_mobile = 'default_top-'.$row->id . '.jpg';
                $row->save();
               }
            }else if($row->image_type == "browse_bottom"){
               if (Input::file('photo')) {
                Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/default_bottom-' . $row->id . '.jpg');
                Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/default_bottom-' . $row->id . '.jpg');
                $row->image_desktop = 'default_bottom-'.$row->id . '.jpg';
                $row->image_mobile = 'default_bottom-'.$row->id . '.jpg';
                $row->save();
               }
            }else if($row->image_type == "browse_side_A"){
              if(Input::file('photo')){
                Image::make(Input::file('photo'))->fit(292, 390)->save('uploads/banner/advertisement/desktop/default_left_A-' . $row->id . '.jpg');
                Image::make(Input::file('photo'))->fit(631, 390)->save('uploads/banner/advertisement/mobile/default_left_A-' . $row->id . '.jpg');
                $row->image_desktop = 'default_left_A-'.$row->id . '.jpg';
                $row->image_mobile = 'default_left_A-'.$row->id . '.jpg';
                $row->save();
              }
            }else if($row->image_type == "browse_side_B"){
               Image::make(Input::file('photo'))->fit(292, 390)->save('uploads/banner/advertisement/desktop/default_left_B-' . $row->id . '.jpg');
               Image::make(Input::file('photo'))->fit(631, 390)->save('uploads/banner/advertisement/mobile/default_left_B-' . $row->id . '.jpg');
               $row->image_desktop = 'default_left_B-'.$row->id . '.jpg';
               $row->image_mobile = 'default_left_B-'.$row->id . '.jpg';
               $row->save();

            }else if($row->image_type == "listings_side"){
               if(Input::file('photo')){
                Image::make(Input::file('photo'))->fit(292, 390)->save('uploads/banner/advertisement/desktop/listing_side-' . $row->id . '.jpg');
                Image::make(Input::file('photo'))->fit(631, 390)->save('uploads/banner/advertisement/mobile/listing_side-' . $row->id . '.jpg');
                $row->image_desktop = 'listing_side-'.$row->id . '.jpg';
                $row->image_mobile = 'listing_side-'.$row->id . '.jpg';
                $row->save();
               }
            }else if($row->image_type == "listings_content"){ 
               if (Input::file('photo')) {
                Image::make(Input::file('photo'))->fit(817, 120)->save('uploads/banner/advertisement/desktop/listing_content-' . $row->id . '.jpg');
                Image::make(Input::file('photo'))->fit(693, 120)->save('uploads/banner/advertisement/mobile/listing_content-' . $row->id . '.jpg');
                $row->image_desktop = 'listing_content-'.$row->id . '.jpg';
                $row->image_mobile = 'listing_content-'.$row->id . '.jpg';
                $row->save();
               }        
            }else{

            }


      return Response::json(['body' => ['Banner Advertisement Saved']]);

  }
  public function getBannerPlacementPlan(){

        $get_plan_details = BannerPlacementPlans::where('banner_placement_id', '=',Request::input('id') )->orderBy('duration','asc')->get();
        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_plan_details as $key => $field) {
          $rows .= '<option value="' . $field->id . '">'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }
        return Response::json($rows);
  }
  
  public function deleteBanner(){

    $row = BannerSubscriptions::find(Request::input('id'));
    if(!is_null($row)){
      $row->status = '2';
      $row->save();
       return Response::json(['body' => ['Banner has been removed']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }

  public function reserveBannerPlacement(){
     $row = BannerDefault::find(Request::input('id'));
     if(!is_null($row)){
        $row->reserve_status = 2;
        $row->save();
        CountryBannerPlacement::where('placement_id',$row->placement_id)->where('country_id',$row->country_id)->update(['reserve_status' => 2]);
        return Response::json(['body' => ['Placement has been reserved']]);
     }else{
        return Response::json(['error' => ['erro Finding the id']]);
     }
  }
  public function unreserveBannerPlacement(){
     $row = BannerDefault::find(Request::input('id'));
     if(!is_null($row)){
        $row->reserve_status = 1;
        $row->save();
         CountryBannerPlacement::where('placement_id',$row->placement_id)->where('country_id',$row->country_id)->update(['reserve_status' => 1]);
        return Response::json(['body' => ['Placement has been unreserved']]);
     }else{
        return Response::json(['error' => ['erro Finding the id']]);
     }
  }
  public function editBanner(){

    $result['rows'] =BannerSubscriptions::find(Request::input('id'));

    $banner_plans = BannerPlacementPlans::where('banner_placement_id','=',$result['rows']->placement)->get();
    
          $plans = '<option class="hide" value="">Select:</option>';
        foreach ($banner_plans as $key => $field) {
          $plans .= '<option value="' . $field->id . '" '.($field->id == $result['rows']->placement_plan ? 'selected':'' ).'>'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }

    $result['data']  = $plans;
    return Response::json($result);
     
   }
   public function doListBannerRates(){
      $result['sort'] = Request::input('sort') ?: 'country_banner_placement_plans.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search') ? : '';
      $status = Request::input('status') ? : '';
      $country = Request::input('country') ? : Country::where('status',1)->pluck('id');

   /* $usertype_id = Request::input('usertype_id');*/
      $per = Request::input('per')  ? : 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

    $rows = CountryBannerPlacement::leftjoin('banner_placement_plans','banner_placement_plans.id','=','country_banner_placement_plans.placement_id')
                                ->leftjoin('countries','countries.id','=','country_banner_placement_plans.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
//                                 ->leftjoin('categories','categories.unique_id','=','country_banner_placement_plans.category_id')
                                ->select('country_banner_placement_plans.*','banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','banner_placement_location.name','countries.countryCode') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.monthly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
//                                            ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.yearly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.daily_cost_paid', 'LIKE', '%' . $search . '%');
                               })
                              ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                                })
                                ->where('country_banner_placement_plans.status',1)
                                ->orderBy($result['sort'], $result['order'])
                           ->groupBy('banner_placement_plans.id')
                                ->paginate($per);
                         // dd($rows);
      } else {
         $count =  CountryBannerPlacement::leftjoin('banner_placement_plans','banner_placement_plans.id','=','country_banner_placement_plans.placement_id')
                                ->leftjoin('countries','countries.id','=','country_banner_placement_plans.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
//                                 ->leftjoin('categories','categories.unique_id','=','country_banner_placement_plans.category_id')
                                ->select('banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','country_banner_placement_plans.*','countries.countryCode','banner_placement_location.name','countries.id') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.monthly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
//                                            ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.yearly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.daily_cost_paid', 'LIKE', '%' . $search . '%');
                               })
                              ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                                })
                                ->where('country_banner_placement_plans.status',1)
                                ->orderBy($result['sort'], $result['order'])
                                ->groupBy('banner_placement_plans.id')
                                ->paginate($per);
                         

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows =  CountryBannerPlacement::leftjoin('banner_placement_plans','banner_placement_plans.id','=','country_banner_placement_plans.placement_id')
                                ->leftjoin('countries','countries.id','=','country_banner_placement_plans.country_id')
                                ->leftjoin('banner_placement_location','banner_placement_location.id','=','banner_placement_plans.location_id')
//                                 ->leftjoin('categories','categories.unique_id','=','country_banner_placement_plans.category_id')
                                ->select('banner_placement_plans.banner_placement_name','banner_placement_plans.id as banner_placement_id','banner_placement_plans.location_id','country_banner_placement_plans.*','countries.countryCode','banner_placement_location.name','countries.id') 
                              ->where(function($query) use ($search) {
                                     $query->where('banner_placement_plans.banner_placement_name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.monthly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('countries.countryCode', 'LIKE', '%' . $search . '%')
//                                            ->orWhere('categories.name', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.yearly_cost_paid', 'LIKE', '%' . $search . '%')
                                           ->orWhere('country_banner_placement_plans.daily_cost_paid', 'LIKE', '%' . $search . '%');
                               })
                              ->where(function($query) use ($country) {
                                     if($country != ""){
                                        $query->where('countries.id','=',$country);
                                     }
                                })
                                ->where('country_banner_placement_plans.status',1)
                                ->orderBy($result['sort'], $result['order'])
                             ->groupBy('banner_placement_plans.id')
                                ->paginate($per);
                         
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
   }
   public function indexBannerRates(){
    $not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }

      $result = $this->doListBannerRates();

      // $getCountry = Country::where('status',1)->get();
      // foreach ($getCountry as $key => $field) {
      //     $getPlans = BannerPlacementPlans::where('status',1)->get();
      //     foreach ($getPlans as $key => $value) {
      //        $new_plans = new BannerPlacementPlans;
      //        $new_plans->
      //        $new_plans->save();
      //     }
         
      // }

      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['title'] = "Banner Rate Management";
      $this->data['banner_placement_pages'] = BannerPlacementPages::where('status',1)->get();
      $this->data['refresh_route'] = url("admin/banner/rates/refresh");
      $this->data['banner_placement'] = BannerPlacement::all();
      $this->data['usertype'] = Usertypes::where('type',1)->get();
      $this->data['countries'] = Country::where('status','!=',5)->where('status',1)->orderBy('countryName','asc')->get();
      $this->data['categories'] = Category::with('subcategoryinfo')->get();

      $this->data['top_comments'] = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title')
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->take(4)->get();
      $this->data['top_reviews'] = AdRating::select('users.*','advertisements.id','advertisements.title','ad_ratings.*')
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->take(4)->get();
      return View::make('admin.banner-management.banner-rates.list', $this->data);
   }
   public function manipulateBannerPlan(){
      $result['rows'] = CountryBannerPlacement::leftjoin('banner_placement_plans','banner_placement_plans.id','=','country_banner_placement_plans.placement_id')
                                               ->select('banner_placement_plans.banner_placement_name','banner_placement_plans.location_id','country_banner_placement_plans.*')
                                               ->where('country_banner_placement_plans.id',Request::input('id'))
                                               ->first();


                                               // dd($result['rows']);

      $getCountry = Country::where('id',$result['rows']->country_id)->where('status',1)->first();
      $getCategory= Category::where('country_code',$getCountry->countryCode)->get();
      $country = '';
      foreach ($getCategory as $key => $field) {
             $country.= '<option value="'.$field->unique_id.'" '.($field->unique_id == $result['rows']->category_id  ? 'selected':'').'>'.$field->name.'</option>';                         
       }                                
     $result['country'] = $country;
      // $banner_plans = BannerPlacement::all();
      //       $plans = '<option class="hide" value="">Select:</option>';
      //     foreach ($banner_plans as $key => $field) {
      //       $plans .= '<option value="' . $field->id . '" '.($field->id == $result['rows']->banner_placement_id ? 'selected':'' ).'>'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
      //     }

      // $result['data']  = $plans;
      return Response::json($result);

   }
   public function manipulateDefaultBanner(){

     $rows= BannerDefault::leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_defaults.placement_id')
                                               ->select('banner_placement_plans.banner_placement_name','banner_placement_plans.location_id','banner_defaults.*')
                                               ->where('banner_defaults.id',Request::input('id'))
                                               ->first();

      $getCountry = Country::where('id',$rows->country_id)->where('status',1)->first();
      $getCategory= Category::where('country_code',$getCountry->countryCode)->get();
      $country = '';
      foreach ($getCategory as $key => $field) {
             $country.= '<option value="'.$field->unique_id.'" '.($field->unique_id == $rows->category_id  ? 'selected':'').'>'.$field->name.'</option>';                         
       } 
      $result['rows'] = $rows;                              
      $result['country'] = $country;

      return Response::json($result);
   }
   public function saveBannerPlan(){

        $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = CountryBannerPlacement::find(array_get($input, 'id'));
          
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }
        $rules = [
            // 'banner_placement_id'           => 'required',
            // 'duration'         => 'required',

            // 'placement_name' => 'unique:banner_placement_plans,banner_placement_name' . (!$new ? ',' . $row->id : ''),
            'daily_cost_paid'   => 'required',
            'monthly_cost_paid'   => 'required',
            'yearly_cost_paid'   => 'required',
            // 'daily_cost_points'   => 'required',
            // 'monthly_cost_points'   => 'required',
            // 'yearly_cost_points'   => 'required',
           
        ];
        // field name overrides
        $names = [
            // 'banner_placement_id'      => 'banner placement',
            // 'duration'         => 'duration',
            'daily_cost_paid' => 'Daily Cost Paid',
            'monthly_cost_paid' => 'Monthly Cost Paid',
            'yearly_cost_paid' => 'Yearly Cost Paid',
            // 'daily_cost_points' => 'Daily Cost Points',
            // 'monthly_cost_points' => 'Monthly Cost Points',
            // 'yearly_cost_points' => 'Yearly Cost Points',
            // 'points'   => 'reward points'
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new CountryBannerPlacement;
            // $row->banner_placement_id     = array_get($input, 'banner_placement_id');
            // $row->banner_placement_name     = array_get($input, 'placement_name');
            // $row->location_id     = array_get($input, 'location_id');
            // // $row->duration     = array_get($input, 'duration');
            // // $row->price     = array_get($input, 'price');
            // // $row->country     = array_get($input, 'country');
            // // $row->period     = array_get($input, 'period');
            // $row->usertype_id = array_get($input, 'usertype_id');
            // // $row->discount     = array_get($input, 'discount');
            // $row->points     = array_get($input, 'points');

            $row->daily_cost_paid     = array_get($input, 'daily_cost_paid');
            $row->monthly_cost_paid     = array_get($input, 'monthly_cost_paid');
            $row->yearly_cost_paid     = array_get($input, 'yearly_cost_paid');
        } else {
            // $row->banner_placement_id     = array_get($input, 'banner_placement_id');
            // $row->banner_placement_name     = array_get($input, 'placement_name');
            // $row->location_id     = array_get($input, 'location_id');
            // $row->duration     = array_get($input, 'duration');
            // $row->price     = array_get($input, 'price');
            // $row->country     = array_get($input, 'country');
            // $row->period     = array_get($input, 'period');
            // $row->discount     = array_get($input, 'discount');
            // $row->usertype_id = array_get($input, 'usertype_id');
            $row->daily_cost_paid     = array_get($input, 'daily_cost_paid');
            $row->monthly_cost_paid     = array_get($input, 'monthly_cost_paid');
            $row->yearly_cost_paid     = array_get($input, 'yearly_cost_paid');
            $row->category_id     = array_get($input, 'category_id');


            // $row->points     = array_get($input, 'points');
        }   
            $row->save();


      return Response::json(['body' => ['Banner Rate Saved']]);

   }
   public function deleteBannerPlan(){
    $row = BannerPlacementPlans::find(Request::input('id'));
    if(!is_null($row)){
      $row->status=2;
      $row->save();
       return Response::json(['body' => ['Banner Rate has been Removed']]);
    }else{
       return Response::json(['error' => ['error on finding the id']]);
    }

   }
  
 
   
   public function approveBanner(){
    $row = BannerAdvertisementSubscriptions::find(Request::input('id'));
    if(!is_null($row)){
      $row->banner_status = 2;
      $row->save();

      $sendNotificationHeader = new UserNotificationsHeader;
      $sendNotificationHeader->reciever_id = $row->user_id;
      $sendNotificationHeader->title = "[System Message]";
      $sendNotificationHeader->save();
      $message = ' <a href="http://yakolak.xerolabs.co/dashboard#advertisingplans"> Your banner '.$row->order_no.'has been approved. </a>';
      $sendNotificationContent = new UserNotifications;
      $sendNotificationContent->notification_id = $sendNotificationHeader->id;
      $sendNotificationContent->type = "[System Message]";
      $sendNotificationContent->message = $message;
      $sendNotificationContent->save();


      return Response::json(['body' => ['Banner has been approved']]);
    }else{
      return Response::json(['error' => ['error on finding the id']]);
    }
   }
   public function declineBanner(){
    $row = BannerAdvertisementSubscriptions::find(Request::input('id'));
    if(!is_null($row)){
      $row->banner_status = 3;
      $row->reason = Request::input('reason');
      $row->save();
      $sendNotificationHeader = new UserNotificationsHeader;
      $sendNotificationHeader->reciever_id = $row->user_id;
      $sendNotificationHeader->title = "Banner ".$row->order_no;
      $sendNotificationHeader->save();
      $message = ' <a href="http://yakolak.xerolabs.co/dashboard#advertisingplans"> Banner Rejected: '.$row->reason.'</a>';
      $sendNotificationContent = new UserNotifications;
      $sendNotificationContent->notification_id = $sendNotificationHeader->id;
      $sendNotificationContent->type = "Banner ".$row->order_no;
      $sendNotificationContent->message = $message;
      $sendNotificationContent->save();
      return Response::json(['body' => ['Banner has been declined'], 'code' => [1]]);
    }else{
      return Response::json(['error' => ['error on finding the id'], 'code' => [0]]);
    }
   }
   public function removeBanner(){
    $row = BannerAdvertisementSubscriptions::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = 2;
        $row->save();
        return Response::json(['body' => ['Banner has been removed']]);
      }else{
        return Response::json(['error' => ['error on finding the id']]);
      }
   }
  public function disableBanner(){
    $row = BannerAdvertisementSubscriptions::find(Request::input('id'));
      if(!is_null($row)){
        $row->banner_status = 6;
        $row->save();
        return Response::json(['body' => ['Banner has been disabled']]);
      }else{
        return Response::json(['error' => ['error on finding the id']]);
      }
   }
  public function enableBanner(){
    $row = BannerAdvertisementSubscriptions::find(Request::input('id'));
      if(!is_null($row)){
        $row->banner_status = 1;
        $row->save();
        return Response::json(['body' => ['Banner has been enabled']]);
      }else{
        return Response::json(['error' => ['error on finding the id']]);
      }
   }
   
}