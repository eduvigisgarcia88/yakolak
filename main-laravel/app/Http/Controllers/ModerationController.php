<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\UserPlanTypes;
use App\Http\Controllers\Controller;
use Form;
use Hash;
use Validator;
use Redirect;
use Url;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Advertisement;
use App\Category;
use App\Country;
use App\City;
use App\AdvertisementPhoto;
use App\AdvertisementWatchlists;
use Image;
use Mail;
use App\CompanyBranch;
use Carbon\Carbon;
use App\UserRating;
use App\AdRating;
use App\UserComments;
use App\UserCommentReplies;
use App\BannerAdvertisementSubscriptions;
use App\UserAdComments;
use App\UserNotifications;
use App\UserPlanRates;
use App\UserAdCommentReplies;
use App\UserMessages;
use App\UserMessagesHeader;
use App\Newsletters;
use App\NewslettersUserCounters;
use App\Languages;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use App\BidPoints;
use App\AuctionBidders;
use App\UserNotificationsHeader;
use App\UserPlanPrices;
use App\UserAdViews;
use App\UserFavoriteProfile;
use App\UserBidList;
use App\UserImportContacts;
use App\UserRegisterReferral;
use App\CountryCode;
use App\UserReferrals;
use App\ReferralTypes;
use DateTime;
use Route;


class ModerationController extends Controller
{
	public function bannerList(){
		$not_allowed = array(1,2);
        if(Auth::check())
    {
      if(in_array(Auth::user()->usertype_id, $not_allowed))
      {
          return redirect()->to('error/access/denied');
      }
    }
    else
    {
      return redirect()->to('error/access/denied');
    }
	  $this->data['title'] = "Moderation Management - Banner";
      $this->data['user_banners'] = BannerAdvertisementSubscriptions::with('getBannerPlacement')->with('getBannerPlacementPlan')->orderBy('created_at','desc')->get();

      $this->data['categories'] = Category::where('status',1)->get();
      $this->data['countries'] = Country::where('status',1)->orderBy('countryName','asc')->get();
      return View::make('admin.moderation-management.banner.list', $this->data);
	}
	public function manipulateBannerStatus(){
		 $result['rows'] = BannerAdvertisementSubscriptions::with('getBannerPlacement')
										->with('getBannerPlacementPlan')
										->where('id',Request::input('id'))
										->orderBy('created_at','desc')
										->first();		

		return Response::json($result);								
	}
	public function bannerApproved(){
	   $row = BannerAdvertisementSubscriptions::where('id',Request::input('id'))->orderBy('created_at','desc')->first();
	   if(!is_null($row)){
	   	$row->banner_status = 2;
	   	$row->save();
	     return Response::json(['body' => "Banner Status Changed"]);
	   }else{
	   	 return Response::json(['error' => "cant find the id"]);
	   }

	}
	public function bannerRejected(){
	 $row = BannerAdvertisementSubscriptions::where('id',Request::input('id'))->orderBy('created_at','desc')->first();
	   if(!is_null($row)){
	   	$row->banner_status = 3;
	   	$row->save();
	     return Response::json(['body' => "Banner Status Changed"]);
	   }else{
	   	 return Response::json(['error' => "cant find the id"]);
    }

	}


}