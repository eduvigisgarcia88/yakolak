<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Route::get('dd/country/master', function(){
// 	return Session::get('default_location');
// });
// Route::get('city/slug/now', 'FrontEndAdvertisementController@citySlug');
// Route::get('a/l/k/a/n/e/1/2/3', 'CategoryController@category_batch');
// Route::get('a/l/k/a/n/e/1/2/{cat_id}','CategoryController@subcategory_batch');
Route::get('{vanity}','FrontEndUserController@VanityCatcher');
Route::post('locale/change/location', 'HomeController@LocationSwitch');
Route::post('verify/close', 'HomeController@VerifyClose');
Route::get('/', 'HomeController@index');
Route::post('get-city','HomeController@getCity');
Route::post('get-city-for-search','HomeController@getCityforSearch');
Route::get('custom','HomeController@getCustomAttributes');
Route::group(array('before' => 'ajax'), function() {
	Route::post('get-bids-pagination','HomeController@bidsPagination');
    Route::post('get-ads-pagination','HomeController@adsPagination');
});
//Route::get('dashboard', 'FrontEndDashboardController@index');

Route::get('support', 'FrontEndSupportController@index');
Route::post('contact/yakolak','AdminController@ContactYakolak');
Route::post('delete/vanity/url','SettingsController@DeleteVanity');
// Route::get('support/about-us', 'SupportController@showAboutUs');
// Route::get('support/contact-us', 'SupportController@showContactUs');
// Route::get('support/call-us', 'SupportController@showCallUs');
// Route::get('support/advertising', 'SupportController@showAdvertising');
// Route::get('support/faqs', 'SupportController@showFAQS');
// Route::get('support/help-us', 'SupportController@showHelpUs');

Route::get('dashboard/messages', 'FrontEndDashboardController@getMessages');
Route::get('dashboard/bids', 'FrontEndDashboardController@getBids');
Route::get('dashboard/listings', 'FrontEndDashboardController@getListings');
Route::get('dashboard/watchlist', 'FrontEndDashboardController@getWatchlist');
Route::get('dashboard/settings', 'FrontEndDashboardController@getSetttings');
Route::post('dashboard/sms-send-code', 'FrontEndDashboardController@sendSMSCode');
Route::post('dashboard/sms-verify-code', 'FrontEndDashboardController@smsVerifyCode');
Route::post('dashboard/sms-new-code', 'FrontEndDashboardController@newSMSCode');
Route::get('error/access/denied', 'HomeController@DeniedPage');
Route::get('plans', 'FrontEndPlanController@index');
// Route::get('search','SearchController@doSearch');

Route::post('filterSearch', 'SearchController@filteredBrowse');
Route::post('general-filter-search', 'SearchController@doFilterSearch');
Route::get('admin/marketing/recent-searches', 'SearchController@indexRecentSearches');
Route::post('admin/dashboard/recent-searches/refresh', 'SearchController@doListRecentSearches');

Route::get('buy','BuyController@index');
Route::get('sell','SellController@index');
Route::get('bid','BidController@index');
// Route::get('login',	'AuthController@login');

Route::post('login', 'AuthController@doLogin');
// Route::get('logout', 'AuthController@frontendLogout');

Route::get('dashboard', 'FrontEndUserController@fetchInfo');
Route::get('register', 'FrontEndUserController@register');
Route::get('register/verify/{confirmation_code}', 'FrontEndUserController@validateAccount');
Route::get('unsubscribe/newsletter/{code}', 'FrontEndUserController@unsubscribe_newsletter');
Route::post('register/save', 'FrontEndUserController@registerSave');
Route::post('setting/save', 'FrontEndUserController@settingSave');


  
  Route::get('{country_code}/all-categories', 'FrontEndCategoryController@fetchList');
  Route::get('buy', 'FrontEndCategoryController@getBuyCategory');
  Route::get('sell', 'FrontEndCategoryController@getSellCategory');
  Route::get('bid', 'FrontEndCategoryController@getBidCategory');
  Route::post('custom/search', 'FrontEndCategoryController@fetchCustomSearch');
  Route::post('custom-search', 'FrontEndCategoryController@doCustomSearch');
  Route::post('get-cat-type', 'FrontEndCategoryController@getCatType');
  Route::post('get-cat-type-for-post', 'FrontEndCategoryController@getCatTypeForPost');
  Route::post('get-sub-subcategory', 'FrontEndCategoryController@getSubSubCategory');
  Route::post('get-live-auction-category', 'FrontEndCategoryController@getAuctionCategory');
  Route::post('get-custom-attributes', 'FrontEndCategoryController@getCustomAttributes');
  Route::get('profiler', 'FrontEndUserController@index');
  Route::post('add-favorite-profile', 'FrontEndUserController@saveFavoriteProfile');
  Route::post('dashboard/remove-watchlist','FrontEndUserController@removeWatchlist');
  Route::post('dashboard/favorite-profile-remove', 'FrontEndUserController@removeWatchlist');
  Route::post('user/dashboard/watchlist/refresh', 'FrontEndUserController@refreshWatchlist');
  Route::post('get-banner-placements', 'FrontEndUserController@getBannerPlacementLocation');
  Route::post('get-banner-placements-price', 'FrontEndUserController@getBannerPlacementPrice');

 
  Route::post('get-user-ads-pagination', 'FrontEndUserController@adsUserPagination');
  Route::post('get-user-bids-pagination', 'FrontEndUserController@bidsUserPagination');
  Route::post('dashboard/favorite-profile/manipulate', 'FrontEndUserController@formWatchlist');
  Route::get('profile/{id}/ads', 'FrontEndUserController@viewUserAds');
  Route::get('profile/view/{id}', 'FrontEndUserController@fetchInfoPublic');
	Route::post('admin/login','AdminController@doLogin');
	Route::get('admin', 'AdminController@login');
Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/dashboard/logout','AdminController@logout');
	Route::get('admin/dashboard/main','DashboardController@index');
});

	Route::get('user/plan/expiring', 'FrontEndUserController@userPlanExpiring');
	Route::get('user/plan/expired', 'FrontEndUserController@userPlanExpired');
	Route::post('resend/email/verification', 'FrontEndUserController@ResendVerification');
	Route::post('clear/email/verification', 'FrontEndUserController@DeleteVerificationIntervals');
	Route::post('subscribe/email', 'FrontEndUserController@subscribe_email');
Route::group(array('before' => 'ajax'), function() {

	
	Route::post('user/get-prices', 'FrontEndUserController@getPlanPrices');
	Route::post('profile/fetch-info/{id}', 'FrontEndUserController@doFetchInfo');
	Route::post('profile/save', 'FrontEndUserController@registerSave');
	Route::post('profile/photo/save', 'FrontEndUserController@changePhoto');
	Route::post('profile/photo/remove', 'FrontEndUserController@removePhoto');
	Route::post('bid/purchase', 'FrontEndUserController@purchaseBid');
	Route::get('dashboard/message', 'FrontEndUserController@message');
	Route::post('get-city-for-dashboard','FrontEndUserController@getCityForDashboard');
	Route::post('user/rate', 'FrontEndUserController@rateUser');
	Route::post('user/comment', 'FrontEndUserController@saveComment');
	Route::post('user/notification/count', 'FrontEndUserController@getUserNotificationCount');
	Route::post('user/comment/reply', 'FrontEndUserController@saveCommentReply');
	Route::post('user/messages', 'FrontEndUserController@getMessages');
	Route::post('user/message/delete', 'FrontEndUserController@deleteMessage');
	Route::post('user/message/refresh', 'FrontEndUserController@doListMessage');
	Route::post('user/banner/refresh', 'FrontEndUserController@getUserBanners');
	Route::post('user/banner/validate', 'FrontEndUserController@getUserBannersValidate');
    Route::post('generate-expiration-date', 'FrontEndUserController@generateExpirationDate');
    Route::post('get-available-placement-plan', 'FrontEndUserController@getAvailablePlacement');
    Route::post('get-available-banner-slot', 'FrontEndUserController@getAvailableBannerSlot');
    Route::post('user/message/notification/refresh', 'FrontEndUserController@updateMessageNotificationCount');
	Route::post('user/message/newMessage', 'FrontEndUserController@newMessage');
	Route::post('user/send/message', 'FrontEndUserController@saveUserMessage');
	Route::post('user/send/message/vendor', 'FrontEndUserController@sendMessage');
	Route::post('user/send/message/vendor/refresh', 'FrontEndUserController@getMessages');
	Route::post('user/send/message/vendor/status', 'FrontEndUserController@changeMessageStatus');
	Route::post('user/read/notifications/vendor/status', 'FrontEndUserController@changeNotificationsStatus');
	Route::post('user/dashboard/auction/active/refresh', 'FrontEndUserController@refreshVendorAuctionActive');
	Route::post('user/dashboard/listing/active/refresh', 'FrontEndUserController@refreshVendorAdsActive');
	Route::post('user/dashboard/listing/save/make-auction', 'FrontEndUserController@adMakeAuction');
	Route::post('user/dashboard/listing/form/make-auction', 'FrontEndUserController@formMakeAuction');
	Route::post('user/dashboard/ad/make-basic-ad', 'FrontEndUserController@adMakeBasicAd');
	Route::get('dashboard/rewards', 'FrontEndUserController@importContact');
    Route::post('user/save/ad/view', 'FrontEndUserController@userAdViews');
    Route::post('user/dashboard/referral/manipulate', 'FrontEndUserController@ReferralInvitation');
    Route::post('user/dashboard/referral/send', 'FrontEndUserController@sendReferralInvitation');
    Route::post('user/dashboard/get-banner-info', 'FrontEndUserController@getUserBannerInfo');
    Route::post('user/dashboard/banner/disable', 'FrontEndUserController@disableBannerAdvertisement');
    Route::post('user/dashboard/banner/enable', 'FrontEndUserController@enableBannerAdvertisement');
    Route::post('user/read/notifications', 'FrontEndUserController@readNotifications');
});
	Route::get('{country_code}/{ad_type_slug}/{parent_category_slug}/{category_slug}/{slug}', 'FrontEndAdvertisementController@fetchInfoPublicAds')->name('SEO_URL');
	Route::get('ads/view/{id}', 'FrontEndAdvertisementController@redirectToSlug');
	Route::get('view/{slug}', 'FrontEndAdvertisementController@slugToSEO');
Route::group(array('before' => 'ajax'), function() {
	Route::get('user/post/ads/6789', 'FrontEndAdvertisementController@postAds');
	Route::post('ad/feature', 'FrontEndAdvertisementController@featureAd');
	Route::post('ad/unfeature', 'FrontEndAdvertisementController@unfeatureAd');
	Route::post('ads/fetch-info/{id}', 'FrontEndUserController@doFetchInfoPublicAds');
	Route::post('ads/refresh', 'FrontEndAdvertisementController@doList');
	Route::post('ads/save', 'FrontEndAdvertisementController@saveAds');
	Route::get('ads/redirect', 'FrontEndAdvertisementController@postAdRedirect');
	Route::post('get-sub-category','FrontEndAdvertisementController@getSubcategory');
	Route::post('get-sub-category-for-post','FrontEndAdvertisementController@getSubcategoryForPost');
	Route::post('get-sub-category-three-for-post','FrontEndAdvertisementController@getSubcategoryThreeForPost');
	Route::post('get-main-subcategory','FrontEndAdvertisementController@getMainSubCategory');
	Route::post('get-main-subcategory-for-post','FrontEndAdvertisementController@getMainSubCategoryForPost');
	Route::post('get-parent-category','FrontEndAdvertisementController@getParentCategory');
	Route::post('ads/remove', 'FrontEndAdvertisementController@removeAds');
	Route::post('ads/disable', 'FrontEndAdvertisementController@disableAds');
	Route::post('ads/disablelist', 'FrontEndAdvertisementController@disableadsList');
	// Route::post('ads/disable', 'FrontEndAdvertisementController@disableAdsList');
	Route::post('ads/enable', 'FrontEndAdvertisementController@enableAds');
	Route::get('ads/edit/{id}', 'FrontEndAdvertisementController@editAds');
	Route::post('custom-attributes', 'FrontEndAdvertisementController@getCustomAttributes');
	// Route::post('auction-type', 'FrontEndAdvertisementController@getAuctionTypes');
	Route::post('ad-type', 'FrontEndAdvertisementController@getAdTypes');	
	Route::post('add-watchlist','FrontEndAdvertisementController@addWatchlist');
	Route::post('remove-ad-watchlist','FrontEndAdvertisementController@removeWatchlist');
	Route::post('basic-ad/expired','FrontEndAdvertisementController@basicAdExpired');	 		
	Route::post('auction/expired','FrontEndAdvertisementController@auctionExpired');
	Route::post('auction/expiring','FrontEndAdvertisementController@auctionExpiring');
	Route::post('ad/comment/save', 'FrontEndAdvertisementController@saveAdComment');
	Route::post('ad/comment/reply/save', 'FrontEndAdvertisementController@saveAdCommentReply');
	Route::post('ad/rate', 'FrontEndAdvertisementController@rateAd');
	Route::post('ad/add/bidders', 'FrontEndAdvertisementController@addBidder');
	Route::post('ad/bidders/refresh', 'FrontEndAdvertisementController@refreshBidders');
	Route::post('ad/bidders-list/refresh', 'FrontEndAdvertisementController@refreshBidderList');
	Route::post('exchange-point-checker', 'FrontEndAdvertisementController@exhangePointChecker');

});

Route::get('admin/listings/ads', 'AdvertisementController@index');
Route::get('admin/listings/spam-and-bots', 'AdvertisementController@indexSpamAndBots');
Route::get('admin/listings/comments-and-reviews', 'AdvertisementController@indexCommentsAndReviews');
Route::get('admin/listings/featured-ad', 'AdvertisementController@indexFeature');
Route::get('admin/listings/auctions', 'AdvertisementController@indexAuction');
Route::get('admin/users/advertisement/list/all/{user_id}', 'AdvertisementController@indexUserListings');
Route::get('admin/users/advertisement/list/flagged/{user_id}', 'AdvertisementController@indexUserListingFlagged');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/ads/manipulate', 'AdvertisementController@viewAds');
	Route::post('admin/ads/save', 'AdvertisementController@save');
	Route::post('admin/ads/refresh', 'AdvertisementController@doList');
	Route::post('admin/user-listings/refresh', 'AdvertisementController@doUserListingRefresh');
	Route::post('admin/user-listings/flagged/refresh', 'AdvertisementController@doUserListingFlaggedRefresh');
	Route::post('admin/ads/auction/refresh', 'AdvertisementController@doListAuction');
	Route::post('admin/ads/feature/refresh', 'AdvertisementController@doListFeature');
    Route::post('admin/settings/recaptcha', 'AdvertisementController@getRecaptchaSetting');
	Route::post('admin/ads/spam-and-bots/edit', 'AdvertisementController@editSpamAndBotKeyword');
	Route::post('admin/ads/spam-and-bots/delete', 'AdvertisementController@deleteSpamAndBotKeyword');
	Route::post('admin/ads/spam-and-bots/save', 'AdvertisementController@saveRestrictedWord');
	Route::post('admin/ads/spam-and-bots/refresh', 'AdvertisementController@doListSpamAndBots');
	Route::post('admin/ads/spam-and-bots/enable', 'AdvertisementController@enableKeyword');
	Route::post('admin/ads/spam-and-bots/disable', 'AdvertisementController@disableKeyword');
	Route::post('admin/ads/comments-and-reviews/view', 'AdvertisementController@viewCommentAndReview');
	Route::post('admin/ads/comments-and-reviews/refresh', 'AdvertisementController@doListCommentsAndReviews');
	Route::post('admin/ads/comments-and-reviews/approve-comment', 'AdvertisementController@approveCommentAndReview');
    Route::post('admin/ads/comments-and-reviews/block-comment', 'AdvertisementController@blockCommentAndReview');
    Route::post('admin/ads/comments-and-reviews/unblock-comment', 'AdvertisementController@unblockCommentAndReview');
	Route::post('admin/ads/comments-and-reviews/reject-comment', 'AdvertisementController@rejectCommentAndReview');
	Route::post('admin/ads/comments-and-reviews/delete', 'AdvertisementController@deleteCommentAndReview');

    Route::post('admin/ads/comments-and-reviews/reply/approve-comment', 'AdvertisementController@approveCommentAndReviewReply');
    Route::post('admin/ads/comments-and-reviews/reply/block-comment', 'AdvertisementController@blockCommentAndReviewReply');
    Route::post('admin/ads/comments-and-reviews/reply/unblock-comment', 'AdvertisementController@unblockCommentAndReviewReply');
	Route::post('admin/ads/comments-and-reviews/reply/reject-comment', 'AdvertisementController@rejectCommentAndReviewReply');
	Route::post('admin/ads/comments-and-reviews/reply/delete', 'AdvertisementController@deleteCommentAndReviewReply');
	Route::post('admin/ads/comments-and-reviews/reply/view', 'AdvertisementController@viewCommentAndReviewReply');

	Route::post('admin/ads/featured-ad/settings', 'AdvertisementController@getFeaturedSettings');
    Route::post('admin/ads/featured-settings/save', 'AdvertisementController@saveFeaturedSettings');
    Route::post('admin/ads/featured-settings/refresh', 'AdvertisementController@updateFeatureSettings');
	Route::post('admin/ads/edit', 'AdvertisementController@edit');
	Route::post('admin/ads/block', 'AdvertisementController@blockAds');
	Route::post('admin/ads/verify-email', 'AdvertisementController@verifyEmail');
	Route::post('admin/ads/unblock', 'AdvertisementController@unblockAds');
	Route::post('admin/ads/delete', 'AdvertisementController@removeAds');
	Route::post('admin/ads/mark-spam', 'AdvertisementController@markSpamAds');
	Route::post('admin/ads/unmark-spam', 'AdvertisementController@unmarkSpamAds');
    Route::post('admin/ads/enable', 'AdvertisementController@enableAd');
	Route::post('admin/ads/disable', 'AdvertisementController@disableAd');
	Route::post('admin/ads/flag', 'AdvertisementController@flagAd');
	Route::post('admin/ads/unflag', 'AdvertisementController@unflagAd');
    Route::post('admin/ads/approve-feature', 'AdvertisementController@approveFeature');
	Route::post('admin/ads/reject-feature', 'AdvertisementController@rejectFeature');
	Route::post('admin/ads/unapprove-feature', 'AdvertisementController@unapproveFeature');
	Route::post('admin/ads/delete-feature', 'AdvertisementController@removeFeatureRequest');
	Route::get('admin/ads/{id}/view', 'AdvertisementController@viewAds');
	// Route::post('admin/auction-type', 'AdvertisementController@getAuctionTypes');
	Route::post('admin/ads/management-settings/edit', 'AdvertisementController@editAdManagemetSettings');
	Route::post('admin/ads/management-settings/save', 'AdvertisementController@saveAdManagemetSettings');
	Route::post('admin/ads/list-pictures', 'AdvertisementController@listAdPictures');
	Route::post('admin/ads/get-sub-category', 'AdvertisementController@getSubCategory');
	Route::post('admin/ads/get-category', 'AdvertisementController@getCategory');
	Route::post('admin/ads/get-city', 'AdvertisementController@getCity');
    Route::post('admin/ads/get-comment-replies', 'AdvertisementController@getCommentReplies');
	Route::post('admin/ads/get-custom-attributes', 'AdvertisementController@getCustomAttributes');
	Route::post('admin/ads/fetch-custom-attributes-enabled', 'AdvertisementController@fetchCustomAttributesEnabled');
	Route::post('admin/ads/fetch-custom-attributes-disabled', 'AdvertisementController@fetchCustomAttributesDisabled');
});
Route::get('admin/moderation/banner', 'ModerationController@bannerList');
Route::group(array('before' => 'ajax'), function() {
    Route::post('admin/moderation/banner/manipulate', 'ModerationController@manipulateBannerStatus');
    Route::post('admin/moderation/banner/approve', 'ModerationController@bannerApproved');
    Route::post('admin/moderation/banner/reject', 'ModerationController@bannerRejected');
  
});

Route::get('facebook/login', 'AuthController@facebook');
Route::get('facebook/login-callback', 'AuthController@facebook_callback');
Route::get('twitter/login', 'AuthController@twitter');
Route::get('twitter/login-callback', 'AuthController@twitter_callback');
Route::get('google/login', 'AuthController@google');
Route::get('google/login-callback', 'AuthController@google_callback');

Route::get('admin/report', 'ReportController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/report/refresh', 'ReportController@doList');

});

Route::get('admin/report-ads', 'ReportAdsController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/report-ads/refresh', 'ReportAdsController@doList');
});

Route::get('admin/report-sales', 'ReportSalesController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/report-sales/refresh', 'ReportSalesController@doList');
});

// Route::group(array('before' => 'ajax'), function() {
//     Route::post('admin/moderation/banner/manipulate', 'ModerationController@manipulateBannerStatus');
//     Route::post('admin/moderation/banner/approve', 'ModerationController@bannerApproved');
//     Route::post('admin/moderation/banner/reject', 'ModerationController@bannerRejected');
  
// });
Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/users/front-end', 'UserController@index');
	Route::get('admin/users/administrators', 'UserController@indexBackend');
	Route::post('admin/users/refresh', 'UserController@doList');
    Route::post('admin/users/back-end/refresh', 'UserController@doListBackEndUsers');
	Route::post('admin/users/block', 'UserController@blockUser');
	Route::post('admin/users/unblock', 'UserController@unblockUser');
    Route::post('admin/users/disable', 'UserController@disable');
	Route::post('admin/users/enable', 'UserController@enable');
	Route::post('admin/users/remove', 'UserController@removeUser');
    Route::post('admin/users/verify-email', 'UserController@verifyEmail');
	Route::post('admin/user/edit', 'UserController@edit');
    Route::post('get-user-info', 'UserController@getUserInfo');
    Route::post('get-admin-info', 'UserController@getAdminInfo');
});

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/user/save', 'SystemController@save');
});

	Route::get('admin/settings/categories', 'CategoryController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/category/manipulate', 'CategoryController@edit');
	Route::post('admin/category/save', 'CategoryController@save');
	Route::post('admin/subcategory/save', 'CategoryController@saveSubCategory');
	Route::post('admin/subcategorytwo/save', 'CategoryController@saveSubCategoryTwo');
	
	Route::post('admin/category/refresh', 'CategoryController@doList');
	Route::post('admin/category/get-subcat-sequence','CategoryController@checkSequenceSubCat');
	Route::post('admin/category/get-cat-sequence','CategoryController@checkSequenceCat');

	Route::post('admin/subcategory/manipulate','CategoryController@subEdit');
	Route::post('admin/category/delete','CategoryController@deleteCategory');
	Route::post('admin/category/disable','CategoryController@disableCategory');
	Route::post('admin/category/enable','CategoryController@enableCategory');
    Route::post('admin/subcategory/delete','CategoryController@deleteSubcategory');
	Route::post('admin/subcategory/disable','CategoryController@disableSubcategory');
	Route::post('admin/subcategory/enable','CategoryController@enableSubcategory');

	Route::post('admin/get-subcategories','CategoryController@getSubcategories');
	Route::post('admin/get-subcategories-two','CategoryController@getSubCategoriesTwo');
	Route::post('admin/get-country-categories','CategoryController@getCountryCategories');
});
	Route::get('admin/settings/custom-attributes', 'CustomAttributeController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/category/custom-attribute/save','CustomAttributeController@save');
	Route::post('admin/category/get-custom-attributes','CustomAttributeController@getCustomAttributes');
	Route::post('admin/subcategory/get-custom-attributes','CustomAttributeController@getSubCustomAttributes');
	Route::post('admin/category/custom-attribute/delete','CustomAttributeController@delete');
	Route::post('admin/category/custom-attribute/fetch','CustomAttributeController@getCategories');
	Route::post('admin/category/custom-attribute/fetch/subcategories','CustomAttributeController@getSubCategories');
	Route::post('admin/category/custom-attribute/enable','CustomAttributeController@enableAttri');
	Route::post('admin/category/custom-attribute/disable','CustomAttributeController@disableAttri');
	Route::post('admin/category/custom-attribute/refresh','CustomAttributeController@doList');
	Route::post('admin/category/custom-attribute/manipulate','CustomAttributeController@manipulateMainAttri');	
});
	Route::get('admin/marketing/banners', 'BannerController@index');
	Route::get('admin/marketing/banner-settings', 'BannerController@indexBannerRates');
	Route::get('admin/marketing/banner-default', 'BannerController@indexBannerDefault');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/get-banner-plans', 'BannerController@getBannerPlacementPlan');
	Route::post('admin/banner/save', 'BannerController@save');
	Route::post('admin/banner/refresh', 'BannerController@doList');
	Route::post('admin/banner/edit', 'BannerController@editBanner');
	Route::post('admin/banner/delete', 'BannerController@deleteBanner');
	Route::post('admin/banner/rates/refresh', 'BannerController@doListBannerRates');
    Route::post('admin/banner-default/refresh', 'BannerController@doListBannerDefault');
    Route::post('admin/banner-default/edit', 'BannerController@manipulateDefaultBanner');
 	Route::post('admin/banner/rate/save', 'BannerController@saveBannerPlan');
 	Route::post('admin/banner/rate/delete', 'BannerController@deleteBannerPlan');
	Route::post('admin/banner/plan/manipulate', 'BannerController@manipulateBannerPlan');
	Route::post('admin/banner/placement/reserve', 'BannerController@reserveBannerPlacement');
	Route::post('admin/banner/placement/unreserve', 'BannerController@unreserveBannerPlacement');
	Route::post('admin/banner/default/save', 'BannerController@saveBannerDefault');
	Route::post('admin/banner/approve', 'BannerController@approveBanner');
	Route::post('admin/banner/decline', 'BannerController@declineBanner');
	Route::post('admin/banner/remove', 'BannerController@removeBanner');
    Route::post('admin/banner/enable', 'BannerController@enableBanner');
	Route::post('admin/banner/disable', 'BannerController@disableBanner');

	
});
	Route::get('admin/settings/countries', 'CountryController@index');
	// Route::get('admin/banner/rates', 'CountryController@indexBannerRates');
Route::group(array('before' => 'ajax'), function() {
	// Route::post('admin/banner/save', 'CountryController@save');
	Route::post('admin/country/refresh', 'CountryController@doList');
	Route::post('admin/country/edit', 'CountryController@manipulate');
	Route::post('admin/country/enable', 'CountryController@enableCountry');
	Route::post('admin/country/disable', 'CountryController@disableCountry');
	Route::post('admin/country/city/save', 'CountryController@saveCity');
	Route::post('admin/country/delete', 'CountryController@removeCountry');
	Route::post('admin/country/city/delete', 'CountryController@removeCity');
	Route::post('admin/country/city/edit', 'CountryController@editCity');
	Route::post('admin/country/city/enable', 'CountryController@enableCity');
	Route::post('admin/country/city/disable', 'CountryController@disableCity');
	Route::post('admin/get-theme', 'CountryController@getTheme');
	Route::post('admin/country/change-status', 'CountryController@changeStatus');
	Route::post('admin/country/save', 'CountryController@save');
    Route::post('admin/dashboard/country/get-cities', 'CountryController@getCities');
});
	Route::get('admin/dashboard/cities', 'CityController@index');
	// Route::get('admin/banner/rates', 'CountryController@indexBannerRates');
Route::group(array('before' => 'ajax'), function() {
	// Route::post('admin/banner/save', 'CityController@save');
	Route::post('admin/city/refresh', 'CityController@doList');
	Route::post('admin/city/change-status', 'CityController@changeStatus');
});
	Route::get('admin/marketing/plans-and-promotions', 'PromotionController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/save', 'PromotionController@save');
	Route::post('admin/promotion/manipulate', 'PromotionController@manipulate');
	Route::post('admin/promotion/delete', 'PromotionController@delete');
	Route::post('admin/promotion/refresh', 'PromotionController@doList');
	Route::post('admin/promotion/get-city', 'PromotionController@getCity');
	Route::post('admin/promotion/change-status','PromotionController@changeStatus');
});
	Route::get('admin/promotion/message', 'PromotionController@indexPromotionMessage');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/message/save', 'PromotionController@savePromotionMessage');
	Route::post('admin/promotion/message/manipulate', 'PromotionController@manipulatePromotionMessage');
	Route::post('admin/promotion/message/delete', 'PromotionController@delete');
	Route::post('admin/promotion/message/refresh', 'PromotionController@doListPromotionMessage');
});
	Route::get('admin/settings/localization', 'LocalizationController@index');
	Route::match(['get','post'], 'admin/settings/configuration', 'SettingsController@ListConfiguration');
	Route::post('admin/dashboard/edit/config', 'SettingsController@EditConfiguration');
	Route::post('admin/dashboard/reserve/vanity', 'SettingsController@ReserveVanity');
	Route::post('admin/dashboard/collect/config', 'SettingsController@CollectConfiguration');
	Route::post('admin/dashboard/edit/contact', 'SettingsController@EditContact');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/message/save', 'LocalizationController@savePromotionMessage');
	Route::post('admin/promotion/message/manipulate', 'LocalizationController@manipulatePromotionMessage');
	Route::post('admin/promotion/message/delete', 'LocalizationController@delete');
	Route::post('admin/localization/refresh', 'LocalizationController@dolist');
    Route::post('admin/localization/phrase/save', 'LocalizationController@localizationPhraseSave');
    Route::post('admin/localization/save', 'LocalizationController@localizationSave');
    Route::post('get-localization-phrases', 'LocalizationController@getLocalizationPhrases');
    Route::post('edit-localization', 'LocalizationController@editLocalization');
    Route::post('admin/localization/phrase/edit', 'LocalizationController@editLocalizationPhrase');
    Route::post('admin/localization/enable', 'LocalizationController@enableLocalization');
    Route::post('admin/localization/disable', 'LocalizationController@disableLocalization');
    Route::post('admin/localization/remove', 'LocalizationController@removeLocalization');
});

//for themes
Route::get('admin/settings/theme/plan', 'ThemeController@indexThemePlan');
Route::get('admin/settings/theme/country', 'ThemeController@indexThemeCountry');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/settings/theme/plan/disable', 'ThemeController@disableThemePlan');
	Route::post('admin/settings/theme/plan/enable', 'ThemeController@enableThemePlan');
	Route::post('admin/settings/theme/plan/remove', 'ThemeController@removeThemePlan');
    Route::post('admin/settings/theme/plan/refresh', 'ThemeController@doListThemePlan');
    Route::post('admin/settings/theme/plan/edit', 'ThemeController@manipulateThemePlan');
    Route::post('admin/settings/theme/plan/save', 'ThemeController@saveThemePlan');	
    Route::post('admin/settings/theme/country/disable', 'ThemeController@disableThemeCountry');
	Route::post('admin/settings/theme/country/enable', 'ThemeController@enableThemeCountry');
	Route::post('admin/settings/theme/country/remove', 'ThemeController@removeThemeCountry');	
    Route::post('admin/settings/theme/country/refresh', 'ThemeController@doListThemeCountry');	
    Route::post('admin/settings/theme/country/edit', 'ThemeController@manipulateThemeCountry');				
   	Route::post('admin/settings/theme/country/save', 'ThemeController@saveThemeCountry');
});
	Route::get('admin/promotion/banners', 'PromotionController@indexPromotionBanner');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/banner/save', 'PromotionController@savePromotionBanner');
	Route::post('admin/promotion/banner/manipulate', 'PromotionController@manipulatePromotionBanner');
	Route::post('admin/promotion/banner/delete', 'PromotionController@deletePromotionBanner');
	Route::post('admin/promotion/banner/refresh', 'PromotionController@doListPromotionBanner');
});	

	Route::get('admin/marketing/newsletter', 'EmailTemplateController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/email-template/refresh', 'EmailTemplateController@doList');
	Route::get('admin/email-template/{id}/edit', 'EmailTemplateController@edit');
	Route::post('admin/email-template/manipulate', 'EmailTemplateController@view');
    Route::post('admin/email-template/enable', 'EmailTemplateController@enable');
    Route::post('admin/email-template/disable', 'EmailTemplateController@disable');
	Route::post('admin/email-template/change-status','EmailTemplateController@changeStatus');
	Route::post('admin/email-template/save', 'EmailTemplateController@save');	
});
	Route::get('admin/marketing/plans', 'PlanController@indexPlanVendor');
	Route::get('admin/plan/company', 'PlanController@indexPlanCompany');
	Route::get('admin/marketing/bids', 'PlanController@indexBidPoints');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/plan/vendor/refresh', 'PlanController@doListPlanVendor');
	Route::post('admin/plan/company/refresh', 'PlanController@doListPlanCompany');
	Route::post('admin/plans/bid/refresh', 'PlanController@dolistBidPoints');
	Route::post('admin/plan/save', 'PlanController@save');
	Route::post('admin/plan/enable', 'PlanController@enablePlan');
	Route::post('admin/plan/disable', 'PlanController@disablePlan');
	
	Route::post('user-types', 'PlanController@getUserType');
	Route::post('admin/plan/remove', 'PlanController@removePlan');
	Route::post('admin/plan/bid/remove', 'PlanController@removeBidPlan');
	Route::post('admin/plan/bid/save', 'PlanController@saveBidPoints');
	Route::post('admin/plan/promotion-bid', 'PlanController@savePromotionBid');
	Route::post('admin/plan/bid/manipulate', 'PlanController@manipulateBidPoints');
	Route::post('admin/plan/manipulate', 'PlanController@edit');
	Route::post('admin/plan/change-status','PlanController@changePlanStatus');
	Route::post('admin/plan/delete', 'PlanController@removePlan');
	Route::post('admin/discount/rate', 'PlanController@getPlanRate');
	Route::post('admin/plan/add-price', 'PlanController@savePrice');
	Route::post('admin/plan/promotion-discount', 'PlanController@savePromotionPlan');
	Route::post('admin/plan/get-plan-price', 'PlanController@getPlanPrice');
	Route::post('admin/plan/price/manipulate', 'PlanController@editPlanPrice');
	Route::post('admin/plan/price/delete', 'PlanController@removePlanPrice');
});
	Route::get('admin/plan-rates/vendor', 'PlanRateController@indexPlanRatesVendor');
	Route::get('admin/plan-rates/company', 'PlanRateController@indexPlanRatesCompany');

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/plan-rates/vendor/refresh', 'PlanRateController@doListPlanRatesVendor');
	Route::post('admin/plan-rates/company/refresh', 'PlanRateController@doListPlanRatesCompany');
	Route::post('admin/plan-rates/save', 'PlanRateController@savePlanRates');
	Route::post('admin/plan-rates/remove', 'PlanRateController@removePlanRates');
	// Route::get('admin/plan-rates/vendor/{id}/edit', 'PlanRateController@editVendor');
	// Route::get('admin/plan-rates/company/{id}/edit', 'PlanRateController@editCompany');
	Route::post('admin/plan-rates/manipulate', 'PlanRateController@view');
	Route::post('admin/plan-rates/edit', 'PlanRateController@edit');
	Route::post('admin/plan-rates/delete', 'PlanRateController@removePlanRates');
	Route::post('admin/plan-rates/disabled', 'PlanRateController@disablePlanRates');
	Route::post('admin/plan-rates/change-status','PlanRateController@changePlanRateStatus');
	Route::get('admin/plan-rates/create', 'PlanRateController@create');
	Route::post('admin/ads/get-duration', 'PlanRateController@getDuration');
});

   	Route::get('admin/settings/pages', 'PageController@index');
Route::group(array('before' => 'ajax'), function() {

    Route::post('admin/pages/refresh', 'PageController@refreshPages');
    Route::post('admin/pages/save', 'PageController@save');
});

Route::group(array('before' => 'ajax'), function() {
	Route::post('get-bannerplacementplans', 'FrontEndBannerController@getBannerPlacementPlan');
    Route::post('banner/payment/save', 'FrontEndBannerController@saveBannerForPurchase');
	Route::post('bannerplacement/save', 'FrontEndBannerController@saveBannerPlacement');
	Route::post('dasboard/banner/manipulate', 'FrontEndBannerController@viewBannerPlacement');
	Route::post('banner/verify', 'FrontEndBannerController@restrictBannerAction');
});

Route::get('banner/payment', 'FrontEndPaymentController@saveBannerPlacement');
Route::get('payment/create', 'FrontEndPaymentController@create');
Route::post('plan/payment/ipn', 'FrontEndPaymentController@ipnHandler');
Route::get('payment/plan/{in}', 'FrontEndPaymentController@verifyPayment');
Route::get('payment/bid/{in}', 'FrontEndPaymentController@verifyPaymentForBidPoints');
Route::get('payment/banner/{id}', 'FrontEndPaymentController@verifyPaymentForBannerPlan');
Route::get('payment/ad-feature/1016/{id}/{duration}/{duration_metric}/{callback}', 'FrontEndPaymentController@verifyPaymentForFeaturingAd');

Route::group(array('before' => 'ajax'), function() {
	Route::post('plan/payment', 'FrontEndPaymentController@purchasePlan');
    Route::post('plan/payment/points', 'FrontEndPaymentController@purchasePlanThruPoints');
	Route::post('bid/payment', 'FrontEndPaymentController@getPaymentBidPointsButton');
	Route::post('user/dashboard/banner/redirect', 'FrontEndPaymentController@aw');
});
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/settings/recaptcha/save', 'SettingsController@save');
});

Route::get('contact/import/google', ['as'=>'google.import', 'uses'=>'FrontEndReferralController@importGoogleContact']);
Route::get('referral/import', 'FrontEndReferralController@index');
Route::get('referral/callback/{code?}', 'FrontEndReferralController@callback');
Route::get('dasboard/{code?}', 'FrontEndReferralController@callback');
Route::get('register/{referral_code}', 'FrontEndReferralController@referralRegister');
Route::group(array('before' => 'ajax'), function() {
	Route::post('save/import', 'FrontEndReferralController@saveImport');
});
Route::get('{country_code}/{slug}', 'FrontEndCategoryController@routeCatch')->name('CAT_URL');
Route::get('{country_code}/{city}/{slug}/{q?}', 'FrontEndCategoryController@fetchCategoryList')->name('CAT_NEW_URL');
Route::get('{country_code}/{city}/{cat}/{slug}/{q?}', 'FrontEndCategoryController@fetchSubCategoryList')->name('SUB_CAT_URL');
Route::get('{country_code}/q', 'SearchController@browse');
Route::get('{country_code}/{category}/q', 'SearchController@browse');

//RELATIVE URLs
Route::get('uploads',[])->name('uploads');
// Route::get('invoice', 'FrontEndUserController@get_invoice');