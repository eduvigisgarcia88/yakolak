<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    public function categoryInfo(){
    	return $this->belongsToMany('App\Category','cat_id','unique_id');
    }
    public function getOtherSubcategories(){
         return $this->hasMany('App\SubCategoriesTwo','subcat_main_id','unique_id')->where('status',1);
    }
    public function getSubCatCustomAttributes(){
        return $this->hasMany('App\SubAttributes','attri_id','unique_id')->where('status',1);
    }             
}