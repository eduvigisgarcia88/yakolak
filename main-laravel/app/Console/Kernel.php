<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
        Commands\AdExpirationCommand::class,
        Commands\BannerExpirationCommand::class,
        Commands\FeaturedAdExpirationCommand::class,
        Commands\PlanExpirationCommand::class,
        Commands\HolidayNewsletterCommand::class,
        Commands\UserMonthlyStatusCommand::class,
        Commands\UserPlanExpirationCommand::class,
        Commands\AuctionExpirationCommand::class,
        Commands\UserStatusCommand::class,
        Commands\TransferPointsCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('featured_ad:expiration')->twiceDaily(8, 20);
        $schedule->command('holiday:trigger')->dailyAt('00:05');
        $schedule->command('user_monthly_status:trigger')->monthly();
        $schedule->command('plan_expiration:trigger')->twiceDaily(8, 20);
        $schedule->command('banner_expiration:trigger')->twiceDaily(8, 20);
        $schedule->command('ad_expiration:trigger')->twiceDaily(8, 20);
        $schedule->command('user_plan_expiration:trigger')->withoutOverlapping();
        $schedule->command('auction_expiration:trigger')->withoutOverlapping();
        $schedule->command('user_status:trigger')->withoutOverlapping();
        $schedule->command('transfer_referrals:go')->withoutOverlapping();
    }
}