<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BannerAdvertisementSubscriptions;
use App\Newsletters;
use Mail;
use App\User;
use Auth;
use Carbon\Carbon;
use App\BannerAdvertisementMerge;
class BannerExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'banner_expiration:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
         $get_banners = BannerAdvertisementSubscriptions::where('banner_status',4)->where('status',1)->get();
          foreach ($get_banners as $key => $field) {
//                    $get_user_info = User::find($field->user_id);
//                    $date_now= date_create($field->start_date);
//                    $date_expiration=date_create($field->expiration);
//                    $diff=date_diff($date_now,$date_expiration);
//                    $banner_name = $field->title;
//                    if($diff->format("%a") == '30' || $diff->format("%a") == '20' || $diff->format("%a") == '10'){
//                       $days_left = $diff->format("%a");
//                         $newsletter = Newsletters::where('id', '=', '26')->where('newsletter_status', '!=', '2')->first();
//                          if ($newsletter) {
//                                    Mail::send('email.expiration_banner', ['newsletter' => $newsletter,'days_left' => $days_left,'banner_name' => $banner_name], function($message) use ($newsletter, $get_user_info) {
//                                          $message->to($get_user_info->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
//                                    });
//                          }
//                    }
//                    if($get_user_info->promotion_newsletter == 1){
//                         if($diff->format("%a") == '1'){ //for banner promotion
//                            $promotion_link = '<a href="'.url('dashboard').'#advertisingplans'.'">Extend Banner</a>';
//                            $newsletter = Newsletters::where('id', '=', '29')->where('newsletter_status', '!=', '2')->first();
//                            if ($newsletter) {
//                                      Mail::send('email.expired_banner_promotion', ['newsletter' => $newsletter,'promotion_link' => $promotion_link,'banner_name' => $banner_name], function($message) use ($newsletter,$get_user_info) {
//                                            $message->to($get_user_info->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
//                                      });
//                            } 
//                         }
//                    }  
            if (strtotime(Carbon::now('Asia/Manila')) >= $field->expiration) {
              $field->banner_status = 6;
              $field->save();
              BannerAdvertisementMerge::where('banner_id', $field->id)->delete();
            }
          }
    }
}
