<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Newsletters;
use App\Advertisement;
use Mail;
use App\User;
class AdExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ad_expiration:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_date_today_for_advertisement_expiration = date("Y-m-d H:i:s");
        $get_advertisements = Advertisement::where('status',1)->get();
         foreach ($get_advertisements as $key => $field) {
                  $get_user_info = User::find($field->user_id);
                  $date_now=date_create($get_date_today_for_advertisement_expiration);
                  $date_expiration=date_create($field->ad_expiration);
                  $diff=date_diff($date_now,$date_expiration);
                  if($diff->format("%a") == '30' || $diff->format("%a") == '20' || $diff->format("%a") == '10'){

                     $days_left = $diff->format("%a");
                        $newsletter = Newsletters::where('id', '=', '26')->where('newsletter_status', '!=', '2')->first();
                         if ($newsletter) {
                                   Mail::send('email.expiration_ad', ['newsletter' => $newsletter,'days_left' => $days_left,'ad_name' => $field->title], function($message) use ($newsletter, $get_user_info) {
                                         $message->to($get_user_info->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                                   });
                         }
                  }
                 if($get_user_info->promotion_newsletter == 1){
                    if($diff->format("%a") == '1'){ //for advertisement promotion
                       $newsletter = Newsletters::where('id', '=', '15')->where('newsletter_status', '!=', '2')->first();
                       $promotion_link = url('dashboard');
                       if ($newsletter) {
                                 Mail::send('email.expired_ad_promotion', ['newsletter' => $newsletter,'promotion_link' => $promotion_link,'ad_name' => $field->title], function($message) use ($newsletter, $row) {
                                       $message->to($get_user_info->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                                 });
                       } 
                   }
                 }
                  if($field->ad_expiration == $get_date_today_for_advertisement_expiration){
                      $newsletter = Newsletters::where('id', '=', '27')->where('newsletter_status', '!=', '2')->first();
                         if ($newsletter) {
                                   Mail::send('email.expired_ad', ['newsletter' => $newsletter,'days_left' => $days_left,'ad_name' => $field->title], function($message) use ($newsletter, $get_user_info) {
                                         $message->to($get_user_info->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                                   });
                         }
                  }
           }
    }
}
