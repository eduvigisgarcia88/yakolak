<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Newsletters;
use Mail;
use Carbon\Carbon;
use App\User;
use App\ExternalSub;
class HolidayNewsletterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        
        $get_today = Carbon::now()->format('Y-m-d');
        //date setter 
        $date_for_christmas=date_create(date("Y")."-12-25");
        $christmas_day = date_format($date_for_christmas,"Y-m-d");

        $date_for_holloween=date_create(date("Y")."-11-01");
        $holloween_day= date_format($date_for_holloween,"Y-m-d");

        $date_for_new_year=date_create(date("Y")."-01-01");
        $new_year_day= date_format($date_for_new_year,"Y-m-d");

        $date_for_valentine=date_create(date("Y")."-02-14");
        $valentines_day= date_format($date_for_valentine,"Y-m-d");
        //start
        $find_subscribed_users = User::where('promotion_newsletter', 1)->where('status', 1)->get();
        foreach ($find_subscribed_users as $key => $val) {

            $contact_name = $val->name;
          
            $newsletter = Newsletters::where('id', '=', '17')->where('newsletter_status', '!=', '2')->first();

            if($get_today  == $christmas_day){
                $dated_holiday = "Have a beautiful Merry Christmas!!!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $new_year_day){
                $dated_holiday = "Have a prosperous New Year!!!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $holloween_day){
                $dated_holiday = "Happy Holloween !!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $valentines_day){
                $dated_holiday = "Happy Valentines Day !!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }
        }
      
        $find_external_users = ExternalSub::where('status', 1)->get();      
        foreach ($find_external_users as $key => $val) {

            $contact_name = $val->first.' '.$val->last;
          
            $newsletter = Newsletters::where('id', '=', '17')->where('newsletter_status', '!=', '2')->first();

            if($get_today  == $christmas_day){
                $dated_holiday = "Have a beautiful Merry Christmas!!!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $new_year_day){
                $dated_holiday = "Have a prosperous New Year!!!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $holloween_day){
                $dated_holiday = "Happy Holloween !!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }

            if($get_today  == $valentines_day){
                $dated_holiday = "Happy Valentines Day !!";
                if ($newsletter) {
                         Mail::send('email.postcard_email', ['contact_name' => $contact_name,'dated_holiday'=> $dated_holiday,'newsletter' => $newsletter], function($message) use ($newsletter, $val) {
                               $message->to($val->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
                }
            }
        }
      
      
    }
}