<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\User;
use DB;
use App\UserReferrals;


class TransferPointsCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfer_referrals:go';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_ref = UserReferrals::whereIn('referral_type', [2,3])->where('status', 1)->where('transferred', 0)->get();
        
        foreach ($get_ref as $key => $val) {
          $user_id = $val->referrer_id;
          $referral_points = $val->referral_points;
          $query = DB::table('users')->whereId($user_id)->increment('points', $referral_points);
          $val->transferred = 1;
          $val->save();
        }
    }
}