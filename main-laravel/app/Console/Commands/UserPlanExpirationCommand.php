<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\UserPlanTypes;
use App\User;
use App\Advertisement;
use Carbon\Carbon;

class UserPlanExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_plan_expiration:trigger';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users_with_plan = User::where('status', 1)->where('plan_expiration', '!=', '0000-00-00 00:00:00')->whereNotIn('user_plan_types_id', [1, 5])->get();
        foreach ($users_with_plan as $user) {
            $current_date    = strtotime(Carbon::now());
            $expiration_date = strtotime($user->plan_expiration);
            if ($current_date >= $expiration_date) {
                $plan_types = UserPlanTypes::where('usertype_id', $user->usertype_id)->where('status', 1)->lists('id')->toArray();
                if ($plan_types) {
                    $plan_info = UserPlanTypes::where('id', head($plan_types))->first();
                    $get_usertype_basic_plan             = head($plan_types);
                    $update_usertype                     = User::find($user->id);
                    $update_usertype->user_plan_types_id = $get_usertype_basic_plan;
                    $update_usertype->total_ads = $plan_info->ads;
                    $update_usertype->total_bids = $plan_info->auction;
                    $update_usertype->save();

                    //for listing disable ads - start
                    $user_allowed_listings = $update_usertype->total_ads;
                    $user_listing_count = Advertisement::where('status', 1)->where('ads_type_id', 1)->where('user_id', $user->id)->count();
                    $raw_listing = Advertisement::where('status', 1)->where('ads_type_id', 1)->where('user_id', $user->id)->latest('created_at');
                    if ($user_listing_count > $user_allowed_listings) {
                      $user_listings = $raw_listing->skip($user_allowed_listings)->take($user_listing_count - $user_allowed_listings)->get();
                    } else {
                      $user_listings = $raw_listing->get();
                    }
                    foreach ($user_listings as $disable_listing) {
                        $disable_listing->status = 3;
                        $disable_listing->save();
                    }
                    //for listing disable ads - end

                    //for auction disable ads - start
                    $user_allowed_auctions = $update_usertype->total_bids;
                    $user_auction_count = Advertisement::where('status', 1)->where('ads_type_id', 2)->where('user_id', $user->id)->count();
                    $raw_auction = Advertisement::where('status', 1)->where('ads_type_id', 2)->where('user_id', $user->id)->latest('created_at');
                    if ($user_auction_count > $user_allowed_auctions) {
                      $user_auctions = $raw_auction->skip($user_allowed_auctions)->take($user_auction_count - $user_allowed_auctions)->get();
                    } else {
                      $user_auctions = $raw_auction->get();
                    }
                    foreach ($user_auctions as $disable_auction) {
                        $disable_auction->status = 4;
                        $disable_auction->save();
                    }
                    //for auction disable ads - end

                }
            }
        }
    }
}