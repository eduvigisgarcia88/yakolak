<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use DB;

class UserStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_status:trigger';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'User online status modifier';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $now = strtotime(Carbon::now('Asia/Manila'));
      DB::table('users')->where('online_status', 1)->where('last_activity', '<=', $now)->update(['online_status' => 2]);
    }
}