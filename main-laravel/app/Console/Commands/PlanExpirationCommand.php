<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\Newsletters;
use App\User;
use Mail;
use Carbon\Carbon;

class PlanExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'plan_expiration:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_date_today_for_plan_expiration = date("Y-m-d H:i:s");
        $get_all_users = User::where('status',1)->get();
        foreach ($get_all_users as $key => $field) {
            $date_now=date_create($get_date_today_for_plan_expiration);
            $date_expiration=date_create($field->plan_expiration);
            $diff=date_diff($date_now,$date_expiration);
            if($diff->format("%a") == '30' || $diff->format("%a") == '20' || $diff->format("%a") == '10' ){
              $days_left = $diff->format("%a");
              $newsletter = Newsletters::where('id', '=', '15')->where('newsletter_status', '!=', '2')->first();
               if ($newsletter) {
                         Mail::send('email.expiration_plan', ['newsletter' => $newsletter,'days_left' => $days_left], function($message) use ($newsletter, $field) {
                               $message->to($field->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                         });
               }
            }
            if($get_user_info->promotion_newsletter == 1){
                if($diff->format("%a") == '1'){  //for plan promo
                   $newsletter = Newsletters::where('id', '=', '30')->where('newsletter_status', '!=', '2')->first();
                   $promotion_link = '<a href="'.url('dashboard').'#subscription'.'">Extend Plan</a>';
                   if ($newsletter) {
                             Mail::send('email.expired_plan_promotion', ['newsletter' => $newsletter,'promotion_link' => $promotion_link], function($message) use ($newsletter, $field) {
                                   $message->to($field->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                             });
                   } 
                }
            }
        }
    }
}
