<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\UserPlanTypes;
use App\User;
use App\Advertisement;
use App\AuctionBidders;
use App\UserNotificationsHeader;
use App\UserNotifications;
use App\UserMessagesHeader;
use App\UserMessages;
use Carbon\Carbon;

class AuctionExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auction_expiration:trigger';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_auctions = Advertisement::where('status', 1)->where('ads_type_id', 2)->get();
        foreach ($get_auctions as $auction) {
          
          $current_date    = strtotime(Carbon::now());
          $expiration_date = strtotime($auction->ad_expiration);
          
          if ($current_date >= $expiration_date) {
            $auction->status = 3;
            $auction->save();

            $get_bidder = AuctionBidders::where('ads_id', $auction->id)->orderBy('created_at', 'desc')->where('status', 1)->first();

            if ($get_bidder) {

              $get_bidder->bidder_status = 1;
              $get_bidder->save();

              $notiheader = new UserNotificationsHeader;
              $notiheader->title = 'Bidding';
              $notiheader->read_status = 1;
              $notiheader->reciever_id = $get_bidder->user_id;
              $notiheader->photo_id = $get_bidder->user_id;
              $notiheader->save();

              $href = 'http://yakolak.xerolabs.co/'.$auction->country_code.'/'.$auction->ad_type_slug.'/'.$auction->parent_category_slug.'/'.$auction->category_slug.'/'.$auction->slug;
              $noti = new UserNotifications;
              $noti->notification_id  = $notiheader->id;
              $noti->type = 'Bidding';
              $noti->status = 1;
              $noti->message = '<a class="grayText btn-change-noti-status" data-id="'.$noti->id.'" href="'.$href.'">You won the bidding on the auction '.$auction->title.'<div class="normalText grayText topPadding"></div></a>'; 
              $noti->save();

              $bidder_info = User::find($get_bidder->user_id);

            }

            $msgheaderowner = new UserMessagesHeader;
            $msgheaderowner->title = '[System Message]';
            $msgheaderowner->sender_id = 2;
            $msgheaderowner->read_status = 1;
            $msgheaderowner->reciever_id = $auction->user_id;
            $msgheaderowner->save();

            $msgowner = new UserMessages;
            $msgowner->message_id = $msgheaderowner->id;
            $msgowner->message = '<a href="'.$href.'">Your auction ad '.$auction->title.' has ended. '.($get_bidder != null?''.$bidder_info->name.' won the bidding!':'No one placed a bid.</a>');
            $msgowner->sender_id = 2;
            $msgowner->save();

            echo "no errors";
            
          }
        }
    }
}