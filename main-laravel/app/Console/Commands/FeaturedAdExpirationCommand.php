<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mail;
use App\Advertisement;
use Carbon\Carbon;
use App\UserNotificationsHeader;
use App\UserNotifications;
use App\User;


class FeaturedAdExpirationCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'featured_ad:expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $get_ads = Advertisement::where('status',1)->where('feature', 1)->whereNotNull('featured_expiration')->get();
        foreach ($get_ads as $key => $val) {
            $current_date    = strtotime(Carbon::now());
            $expiration_date = strtotime($val->featured_expiration);
            if ($current_date >= $expiration_date) {

                $val->feature = NULL;
                $val->featured_expiration = NULL;
                $val->save();

                $notify_head = new UserNotificationsHeader;
                $notify_head->title       ='Feature';
                $notify_head->read_status = 1;
                $notify_head->reciever_id = $val->user_id;
                $notify_head->photo_id = $val->user_id;
                $notify_head->save();
                $seo_url = 'https://yakolak.xerolabs.co/'.$val->country_code.'/'.$val->ad_type_slug.'/'.$val->parent_category_slug.'/'.$val->category_slug.'/'.$val->slug;
                $notify_body = new UserNotifications;
                $notify_body->notification_id  = $notify_head->id;
                $notify_body->message = '<a href="'.$seo_url.'">Your ad '.$val->title.' feature status has been expired.</a>';
                $notify_body->type= "Feature Expired";
                $notify_body->status= 1;
                $notify_body->save();

                $user = User::find($val->user_id);

                if ($user) {
                  Mail::send('email.feature', ['ad_name' => $val->title, 'action' => 'expired'], function ($m) use ($user) {
                    $m->from('noreply@yakolak.com')->to($user->email)->subject('Yakolak - Feature Expired');
                  });
                }

            }
        }
    }
}