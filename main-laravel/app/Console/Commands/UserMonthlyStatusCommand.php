<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Auth;
use App\Newsletters;
use App\UserPlanTypes;
use App\User;
use Mail;

class UserMonthlyStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_monthly_status:trigger';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
            $get_all_users = User::where('status',1)->where('monthly_newsletter',1)->get();
            foreach ($get_all_users as $key => $field) {
              $newsletter = Newsletters::where('id', '=', '34')->where('newsletter_status', '!=', '2')->first();
              $get_current_plan = UserPlanTypes::where('id',$field->user_plan_types_id)->first();
               if ($newsletter) {
                      Mail::send('email.vendor_monthly_status', ['newsletter' => $newsletter,'ad_posted' => $field->total_ads,'bid_posted' => $field->total_bids ,'referral_points'=> $field->points,'sms'=> $field->sms,'bid_points'=> $field->bid,'current_plan' => $get_current_plan->plan_name], function($message) use ($newsletter,$field) {
                              $message->to($field->email)->from('noreply@yakolak.com')->subject(''.$newsletter->subject.'');
                      });
               }
            }
    }
}
