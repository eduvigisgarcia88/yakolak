<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdCommentReplies extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_ad_comment_replies';

   public function getCommentReplyUser(){
   		return $this->hasOne('App\User','id','user_id');
   }
    
}