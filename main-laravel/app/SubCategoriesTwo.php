<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoriesTwo extends Model
{

		
  /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories_tier_two';

    public function getSubCatTwoCustomAttributes(){
        return $this->hasMany('App\SubAttributes','attri_id','unique_id')->where('status',1);
    }
    public function inverseSubcategory(){
        return $this->belongsToMany('App\SubCategory','cat_id','unique_id');
    }      
}