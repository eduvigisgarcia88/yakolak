<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementType extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'advertisements_types';

   	public function getAttriValues(){
 		return $this->belongsToMany('App\AdvertisementType','ads_type_id');
 	}

}