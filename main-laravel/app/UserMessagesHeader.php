<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMessagesHeader extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_messages_header';


   public function getUserMessages(){
   		return $this->hasMany('App\UserMessages','message_id');
   }
   
    
}