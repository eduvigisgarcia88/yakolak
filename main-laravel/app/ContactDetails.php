<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactDetails extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'contact_details';
  
}