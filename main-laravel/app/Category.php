<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

  public function subcategoryinfo(){
  		return $this->hasMany('App\SubCategory','cat_id','unique_id');
  }
  public function getCustomAttributes(){
  		return $this->hasMany('App\SubAttributes','attri_id','unique_id')->where('status',1);
  }

}