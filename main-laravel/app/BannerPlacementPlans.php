<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPlacementPlans extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner_placement_plans';

   public function getBannerPlacement(){
   	
   	return $this->belongsTo('App\BannerPlacement','banner_placement_id');
   }
    
}