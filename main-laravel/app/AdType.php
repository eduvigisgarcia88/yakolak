<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdType extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_type';
}