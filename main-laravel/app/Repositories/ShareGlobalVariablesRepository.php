<?php

namespace App\Repositories;

use App\Country;

use App\UserAdComments;

use App\AdRating;

use Session;

use App\Advertisement;

use App\User;

use App\SettingForSiteKey;

use Carbon\Carbon;

use App\SettingForExchangePoints;

use App\Languages;

use App\BannerPlacementPages;

use App\Category;

use App\UserSearchHistory;

use App\TopAds;

use Auth;

use App\VerificationIntervals;

use App\APIKeys;

use App\UserMessagesHeader;

use App\UnreadMessages;

use App\UserNotificationsHeader;

use App\Usertypes;

class ShareGlobalVariablesRepository
{

    public function TopAdsList()
    {
      $ads_var = Advertisement::select('users.username','users.alias', 'users.usertype_id', 'advertisements.title','advertisements.id','advertisements.ads_type_id','advertisements_photo.photo','advertisements_photo.primary','advertisements.user_id', 'advertisements.country_code', 'advertisements.ad_type_slug', 'advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.slug')
                                  ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                  ->leftjoin('users','users.id','=','advertisements.user_id')
                                   ->selectRaw("(SELECT COUNT(*) as ad_visits
                                                FROM yakolak_ad_visits
                                                WHERE ad_id = yakolak_advertisements.id) as ad_visits")
                                    ->where(function($query){
                                        $query->where('advertisements_photo.primary', '=', '1')
                                              ->where('advertisements.status',1)
                                              ->where('advertisements.ads_type_id',1);

                                    })
                                   ->orderBy('ad_visits','desc')
                                   ->take(5)
                                   ->get();

      return $ads_var;

    }

    public function TopBidList()
    {
      $bid_var = Advertisement::select('users.username','users.alias', 'users.usertype_id', 'advertisements.title','advertisements.id','advertisements.ads_type_id','advertisements_photo.photo','advertisements_photo.primary','advertisements.user_id', 'advertisements.country_code', 'advertisements.ad_type_slug', 'advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.slug')
                                  ->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                  ->leftjoin('users','users.id','=','advertisements.user_id')
                                   ->selectRaw("(SELECT COUNT(*) as ad_visits
                                                FROM yakolak_ad_visits
                                                WHERE ad_id = yakolak_advertisements.id) as ad_visits")
                                    ->where(function($query){
                                        $query->where('advertisements_photo.primary', '=', '1')
                                              ->where('advertisements.status',1)
                                              ->where('advertisements.ads_type_id',2);

                                    })
                                   ->orderBy('ad_visits','desc')
                                   ->take(5)
                                   ->get();

      return $bid_var;

    }

    public function RecentSearch()
    {
      $searches_var = UserSearchHistory::orderBy('created_at', 'desc')->take(5)->get();

      return $searches_var;
    }

    public function CountrySwitch()
    {
        $chunk_per = ceil(Country::where('status',1)->orWhere('status',4)->count() / 4);
        $country_switch_var['array'] = Country::where('status',1)
                                              ->orWhere('status',4)
                                              ->lists('countryName','countryCode')
                                              ->chunk($chunk_per,true);
        // dd($country_switch_var['array']);
        // dd($country_switch_var['array']);
        $country_switch_var['chunk_per'] = $chunk_per;
    // $country_switch_var = [];
  //       $chr = 65;

  //        while ($chr <= 90) {
  //        $get_countries = Country::where('status', 1)->where('countryName', 'LIKE', chr($chr) . '%')->get();

  //        if (count($get_countries) > 0) {
    //    $country_switch_var[$chr] = $get_countries;
    //  }
  //          $chr++;
  //        }

        return $country_switch_var;
    }

    public function LatestComments()
    {
      $latest_comments_per_country = UserAdComments::select('users.*','user_ad_comments.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code')
                                                  ->where(function($query){
                                                    if(Session::get('default_location') != 'ALL'){
                                                      $query->where('advertisements.country_code','=',Session::get('default_location'));
                                                     }
                                                  })
                                                  ->leftjoin('users','users.id','=','user_ad_comments.vendor_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','user_ad_comments.ad_id')
                                                  ->orderBy('user_ad_comments.created_at','desc')
                                                  ->where('user_ad_comments.status', 1)
                                                  ->take(4)->get();
      return $latest_comments_per_country;
    }

    public function LatestReviews()
    {
      $latest_reviews_per_country = AdRating::select('users.*','advertisements.id','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','ad_ratings.*')
                                                  ->where(function($query){
                                                    if(Session::get('default_location') != 'ALL'){
                                                      $query->where('advertisements.country_code','=',Session::get('default_location'));
                                                     }
                                                  })
                                                  ->leftjoin('users','users.id','=','ad_ratings.user_id')
                                                  ->leftjoin('advertisements','advertisements.id','=','ad_ratings.ad_id')
                                                  ->orderBy('ad_ratings.created_at','desc')
                                                  ->where('ad_ratings.status', 1)
                                                  ->take(4)->get();

      return $latest_reviews_per_country;
    }

    public function TopMembers()
    {
      $country_code = Session::get('default_location');
      $extension_query = '';
      if(Session::get('default_location') != 'ALL'){
        $extension_query = ' AND yakolak_advertisements.country_code = "' . $country_code. '"';
      }
       $getUsersListingsCount = User::select('users.*')->where('users.status',1)
                  ->selectRaw("(SELECT COUNT(*) as ad_listings
                                            FROM yakolak_advertisements
                                            WHERE user_id = yakolak_users.id AND ads_type_id = 1 AND status = 1 " . $extension_query . ") as ad_listings")
                 ->selectRaw("(SELECT COUNT(*) as auction_listings
                                            FROM yakolak_advertisements
                                            WHERE user_id = yakolak_users.id AND ads_type_id = 2 AND status = 1 " . $extension_query . ") as auction_listings")
                 ->selectRaw("(SELECT COUNT(*) as total_listings
                                            FROM yakolak_advertisements
                                            WHERE user_id = yakolak_users.id AND status = 1 " . $extension_query . ") as total_listings")
                  ->orderBy('total_listings','desc')
                  ->take(4)
                  ->get();

      return $getUsersListingsCount;
    }

    public function FeaturedAds()
    {
      $featured_ads  = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city')                          
                                      ->where(function($query){
                                         $query->where('advertisements_photo.primary', '=', '1')
                                               ->where('advertisements.status', '=', '1')
                                               ->where('advertisements.status', '=', '1')
                                               ->where('advertisements.ads_type_id', '=', '1')
                                               ->where('advertisements.feature', '=', '1');
                                      })

                                      ->where(function($query){
                                          if(Session::get('default_location') != 'ALL')
                                          {
                                            $query->where('advertisements.country_code','=',Session::get('default_location'));
                                          }
                                        })
                                      ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
      return $featured_ads;

    }

    public function TotalAds()
    {
      $count_ads =   Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->select('advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'users.alias','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name','advertisements.feature', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                      ->selectRaw('avg(rate) as average_rate')   
                                      ->where(function($query) {
                                         $query->where('advertisements_photo.primary', '=', '1')
                                              ->where('advertisements.ads_type_id', '=', '1')
                                               ->where('advertisements.status', '=', '1');
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements.feature', 'desc')
                                      ->orderBy('advertisements.user_plan_id', 'desc')
                                      ->orderBy('advertisements.created_at', 'desc')
                                      ->count(); 
      return $count_ads;
    }

    public function TotalAuctions()
    {
      $count_auctions = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'users.alias','advertisements.title','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','advertisements.feature','countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                      ->selectRaw('avg(rate) as average_rate')   
                                      ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements.feature', 'desc')
                                      ->orderBy('advertisements.created_at', 'desc')
                                      ->orderBy('advertisements.user_plan_id', 'desc')
                                      ->count();
        return $count_auctions;
    }

    public function TotalVendors()
    {
      $count_vendors = User::where('usertype_id',1)->where('status',1)->count();
      return $count_vendors;
    }
    // public function getKey(){

    //    $key= SettingForSiteKey::latest()->take(1)->get();

    //   return $key;
    // }
    public function TotalCompanies()
    {
      $count_companies = User::where('usertype_id',2)->where('status',1)->count();
      return $count_companies;
    }

    public function UserRegistration()
    {
      $array_month['user_count_today'] = User::whereMonth('created_at','=',Carbon::today()->format('m'))->count();
      $array_month['user_count_three'] = User::whereMonth('created_at','=',Carbon::today()->subMonths(3)->format('m'))->count();
      $array_month['user_count_two'] = User::whereMonth('created_at','=',Carbon::today()->subMonths(2)->format('m'))->count();
      $array_month['user_count_one'] = User::whereMonth('created_at','=',Carbon::today()->subMonths(1)->format('m'))->count();
      $array_month['highest_count_user_reg'] = max($array_month['user_count_today'],$array_month['user_count_three'],$array_month['user_count_two'],$array_month['user_count_one']);
      $array_month['lowest_count_user_reg'] = min($array_month['user_count_today'],$array_month['user_count_three'],$array_month['user_count_two'],$array_month['user_count_one']);
      return $array_month;
    }

    public function AdvertisementCount()
    {
      $array_ads['ads_count_today'] = Advertisement::whereMonth('created_at','=',Carbon::today()->format('m'))->count();
      $array_ads['ads_count_three'] = Advertisement::whereMonth('created_at','=',Carbon::today()->subMonths(3)->format('m'))->count();
      $array_ads['ads_count_two'] = Advertisement::whereMonth('created_at','=',Carbon::today()->subMonths(2)->format('m'))->count();
      $array_ads['ads_count_one'] = Advertisement::whereMonth('created_at','=',Carbon::today()->subMonths(1)->format('m'))->count();
      $array_ads['highest_count_ads'] = max($array_ads['ads_count_today'],$array_ads['ads_count_three'],$array_ads['ads_count_two'],$array_ads['ads_count_one']);
      $array_ads['lowest_count_ads'] = min($array_ads['ads_count_today'],$array_ads['ads_count_three'],$array_ads['ads_count_two'],$array_ads['ads_count_one']);
      return $array_ads;
    }

    public function PriceMatrix()
    {
      $price_matrix = SettingForExchangePoints::where('status',1)->get();
      return $price_matrix;
    }

    public function Languages()
    {
      $languages = Languages::all();
      return $languages;
    }

    public function BannerPlacement()
    {
      $banner_placement = BannerPlacementPages::where('status',1)->get();
      return $banner_placement;
    }

    public function Categories()
    {
      $categories = Category::with('subcategoryinfo')->get();
      return $categories;
    }

    public function FeaturedAuctions()
    {
      $featured = Advertisement::with(['getAdvertisementComments', 'getAdRatings'])->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.ads_type_id','advertisements.ad_expiration','advertisements.slug','advertisements.parent_category_slug', 'advertisements.category_slug', 'advertisements.ad_type_slug', 'advertisements.country_code','advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.currency', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name','users.alias','countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1')
                                                   ->where('advertisements.ads_type_id','=',2);
                                        })
                                       ->where(function($query){
                                          if(Session::get('default_location') != 'ALL')
                                          {
                                            $query->where('advertisements.country_code','=',Session::get('default_location'));
                                          }
                                        })
                                       ->orderBy('advertisements.ad_expiration', 'desc')->take(4)->get();
      return $featured;
    }

    public function GetVerificationInterval()
    {
      if(Auth::check())
      {
        $verification_intervals = VerificationIntervals::where('user_id',Auth::user()->id)->where('created_at','>',Carbon::today()->toDateString())->orderBy('created_at','desc')->first();
        if($verification_intervals)
        {
          $user_last_request = Carbon::now()->diffInSeconds($verification_intervals->created_at);
          if($user_last_request > 0)
          {
            return $verification_intervals;
          }
        }
      }
    }

    public function PaypalAPI()
    {
      $paypal_keys = APIKeys::where('id', 2)->first();
      if($paypal_keys)
      {
        return $paypal_keys;
      }
    }

    public function CategoriesPer()
    {
      if(Session::has('selected_location') == null){
        $selectedCountry = Session::get('default_location');
      }else{
        $selectedCountry = Session::get('selected_location');
      }
      $cat = Category::where(function($query) use ($selectedCountry){
                      if($selectedCountry != "ALL"){
                         $query->where('country_code','=',$selectedCountry);
                      }else{
                          $query->where('country_code','=','us');
                      }
                   }) 
                   ->where('status',1)
                   ->where('home',2)
                   ->groupBy('name')
                   ->get();

      return $cat;
    }

    public function messages()
    {
      if(Auth::check())
      {
        $user_id = Auth::user()->id;

        $message_count  = UnreadMessages::where('user_id', $user_id)
                                        ->where('read_status', 1)
                                        ->groupBy('message_id')
                                        ->get();

        return count($message_count);
      }
    }

    public function notifications()
    {
      if(Auth::check())
      {
        $user_id = Auth::user()->id;
        $notifications = UserNotificationsHeader::leftjoin('user_notifications', 'user_notifications.notification_id', '=', 'user_notifications_header.id')
                                                ->leftjoin('users', 'users.id', '=', 'user_notifications_header.photo_id')
                                                ->where('user_notifications_header.reciever_id', $user_id)
                                                ->where('user_notifications_header.read_status', 1)
                                                ->where('user_notifications_header.title', '!=', '[System Message]')
                                                ->whereNotNull('user_notifications.message')
                                                ->select('user_notifications_header.id', 'user_notifications_header.reciever_id', 'user_notifications_header.read_status', 'user_notifications_header.created_at', 'user_notifications.message', 'user_notifications.type', 'users.photo')
                                                ->latest('user_notifications_header.created_at')
                                                ->get();

        return $notifications;
      }
      
    }
  
    public function usertypes()
    {
      $user_types = Usertypes::where('type', 1)->where('status', 1)->get();
      return $user_types;
    }
  

}