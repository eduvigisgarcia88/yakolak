<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserBidList extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_bid_list';
}