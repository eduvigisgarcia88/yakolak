<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLogs extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_logs';

     public function getUser(){
         return $this->belongsTo('App\User','user_id');
  }
    
}