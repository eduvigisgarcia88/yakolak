<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReserveVanity extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'reserved_vanity';
}