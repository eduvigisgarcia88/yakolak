<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomAttributesForTextbox extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_custom_attribute_textbox';

}