<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cache;
class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token','confirmation_code'];

    // public function advertisement() {
    //     return $this->hasMany('App\Advertisement');
    // }

    //   public function userType() {
    //     return $this->belongsTo('App\UserType', 'usertype_id');
    // }

  public function getUserCompanyBranch(){
        return $this->hasMany('App\CompanyBranch','user_id');
  }
  public function getUserReferrals(){
        return $this->hasMany('App\UserReferrals','referrer_id');
  }
  public function getUserComments(){
         return $this->hasMany('App\UserComments','user_id')->orderby('created_at','desc');
  }
  public function getUserAdComments(){
         return $this->hasMany('App\UserAdComments','user_id');
  }
  public function getUserPlanType(){
         return $this->belongsTo('App\UserPlanTypes','user_plan_types_id')->orderby('created_at','desc');
  }
  public function getUserRating(){
        return $this->hasOne('App\UserRating','vendor_id');
  }
  public function getUserAdvertisements(){
        return $this->hasMany('App\Advertisement','user_id')->where('status', 1)->orderBy('created_at', 'desc');
  }
  public function getUserLog(){
        return $this->hasMany('App\UserLogs','user_id')->orderby('created_at','desc')->first();
  }
  public function getUserType(){
        return $this->hasOne('App\Usertypes','id','usertype_id');
  }
  public function getUserFlaggedAds(){
        return $this->hasMany('App\Advertisement','user_id')->where('status',4)->orWhere('status',5);
  }
  public function getUserCountry(){
        return $this->hasOne('App\Country','id','country');
  }
  public function getUserAdCount(){
        return $this->hasMany('App\Advertisement','user_id')->where('ads_type_id',1)->where('status',1);
  }
  public function getUserBidCount(){
        return $this->hasMany('App\Advertisement','user_id')->where('ads_type_id',2)->where('status',1);
  }
  public function getUserAdCountInactive(){
        return $this->hasMany('App\Advertisement','user_id')->where('ads_type_id',1)->where('status',3);
  }
  public function getUserBidCountInactive(){
        return $this->hasMany('App\Advertisement','user_id')->where('ads_type_id',2)->where('status',3);
  }
  public function getUserListings(){
        return $this->hasMany('App\Advertisement','user_id');
  }

}
