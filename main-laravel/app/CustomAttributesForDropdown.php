<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomAttributesForDropdown extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_custom_attribute_dropdown';

	
   public function getAdvertisement(){
   	 return $this->belongsTo('App\Advertisement','ad_id');
   }
}