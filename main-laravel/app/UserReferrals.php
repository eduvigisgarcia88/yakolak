<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReferrals extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_referrals';

    public function getUserInfo(){
         return $this->belongsTo('App\User','user_id');
    }
    public function getReferralType(){
    	 return $this->belongsTo('App\ReferralTypes','referral_type');
    }
    
}