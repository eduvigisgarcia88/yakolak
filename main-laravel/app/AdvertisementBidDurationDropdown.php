<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementBidDurationDropdown extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ad_bid_duratation_dropdown';

		public function advertisement()
		{
			return $this->belongsTo('App\Advertisement', 'bid_duration_id');
		}

}