<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnreadMessages extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'unread_messages';
    
}