<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPlacementPages extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner_placement_pages';
    
}