<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionMessages extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion_messages';

   public function getPromotionMessagesRecipient(){

   	return $this->hasMany('App\PromotionMessagesRecipient','promotion_message_id');
   }
    
}