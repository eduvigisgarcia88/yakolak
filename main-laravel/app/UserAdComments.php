<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdComments extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_ad_comments';

   
   public function getUserAdCommentReplies(){
   		return $this->hasMany('App\UserAdCommentReplies','comment_id')->where('status',1);
   }
   public function getCommentUser(){
   		return $this->hasOne('App\User','id','user_id');
   }
   public function getCommentAd(){
   		return $this->hasOne('App\Advertisement','id','ad_id');
   }
    public function getCommentRating(){
      return $this->belongsTo('App\AdRating','id','rating_id');
   }

}