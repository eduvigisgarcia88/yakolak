<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationIntervals extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'verification_intervals';
}