<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserComments extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_comments';

   	public function getUserCommentReplies(){
	  	return $this->hasMany('App\UserCommentReplies','comment_id');
	    
	}
	public function getUserInfo(){
		return $this->hasOne('App\User','id');
	}
    
}