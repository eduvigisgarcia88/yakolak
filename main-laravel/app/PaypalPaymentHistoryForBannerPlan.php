<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaypalPaymentHistoryForBannerPlan extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'paypal_payment_history_for_banner_plan';

    
}