<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token','confirmation_code','username'];

    // public function advertisement() {
    //     return $this->hasMany('App\Advertisement');
    // }

    //   public function userType() {
    //     return $this->belongsTo('App\UserType', 'usertype_id');
    // }

  public function getUserCompanyBranch(){

        return $this->hasMany('App\CompanyBranch','user_id');
  }

  public function getUserComments(){
         return $this->hasMany('App\UserComments','user_id')->orderby('created_at','desc');
  }
  public function getUserPlanType(){
         return $this->belongsTo('App\UserPlanTypes','user_plan_types_id')->orderby('created_at','desc');
  }

  public function getUserRating(){
        return $this->hasOne('App\UserRating','vendor_id');
  }
  public function getUserAdvertisements(){
        return $this->hasMany('App\Advertisement','user_id');
  }
}
