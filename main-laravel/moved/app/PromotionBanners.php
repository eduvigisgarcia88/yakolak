<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionBanners extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion_banners';

   
   public function getPromotionBannerPosition(){
   		return $this->hasOne('App\PromotionBannerPositions','banner_position_id');
   }
    
}