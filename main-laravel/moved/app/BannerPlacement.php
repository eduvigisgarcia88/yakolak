<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BannerPlacement extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'banner_placement';

   
  	public function getBannerPlacementPlans(){
  		return $this->hasMany('App\BannerPlacementPlans','banner_placement_id');
  	}
  

}