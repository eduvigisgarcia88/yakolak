<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAdComments extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_ad_comments';

   
   public function getUserAdCommentReplies(){

   		return $this->hasMany('App\UserAdCommentReplies','comment_id');
   }
    
}