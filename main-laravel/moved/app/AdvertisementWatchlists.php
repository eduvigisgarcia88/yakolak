<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisementWatchlists extends Model {

	protected $fillable = [
		'ads_id',
		'user_id'
		];

		protected $date = ['published_at'];

 		protected $table = 'advertisements_watchlists';

		// /** An articles is owned by a user. **/
			public function advertisement()
		{
			return $this->belongsToMany('App\Advertisement', 'ads_id');
		}
}
