<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    public function categoryInfo(){
     	
    	return $this->belongsToMany('App\Category','cat_id');
    }
    public function getSubCatCustomAttributes(){
        return $this->hasMany('App\SubAttributes','attri_id');
    }             
}