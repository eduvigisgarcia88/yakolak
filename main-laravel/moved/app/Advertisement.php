<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model {

	protected $fillable = [
		'title',
		'body',
		'published_at',
		'user_id',
		'photo'
		];

		protected $date = ['published_at'];

 		protected $table = 'advertisements';

		// /** An articles is owned by a user. **/
		public function user()
		{
			return $this->belongsToMany('App\User', 'user_id');
		}
		 public function advertisementphoto()
		{
			return $this->belongsToMany('App\Advertisement', 'user_id');
		}
		public function getAdvertisementComments(){
			return $this->hasMany('App\UserAdComments','ad_id');
		}
		public function getAdvertisementPhoto(){
			return $this->hasMany('App\AdvertisementPhoto','ads_id')->orderby('photo','desc');
		}
		public function getDropdownCustomAttributes(){
			return $this->hasMany('App\CustomAttributesForDropdown','ad_id');
		}
}
