<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PromotionPopups extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'promotion_popups';

   public function getPromotionPopupStatus(){
   	return $this->hasone('App\PromotionPopupStatus','popup_id');
   }
    
}