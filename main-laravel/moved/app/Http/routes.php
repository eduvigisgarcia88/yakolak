<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::get('/', 'HomeController@index');



Route::post('get-city','HomeController@getCity');
Route::get('custom','HomeController@getCustomAttributes');
Route::group(array('before' => 'ajax'), function() {
	
});
//Route::get('dashboard', 'FrontEndDashboardController@index');

Route::get('support', 'FrontEndSupportController@index');
// Route::get('support/about-us', 'SupportController@showAboutUs');
// Route::get('support/contact-us', 'SupportController@showContactUs');
// Route::get('support/call-us', 'SupportController@showCallUs');
// Route::get('support/advertising', 'SupportController@showAdvertising');
// Route::get('support/faqs', 'SupportController@showFAQS');
// Route::get('support/help-us', 'SupportController@showHelpUs');

Route::get('dashboard/messages', 'FrontEndDashboardController@getMessages');
Route::get('dashboard/bids', 'FrontEndDashboardController@getBids');
Route::get('dashboard/listings', 'FrontEndDashboardController@getListings');
Route::get('dashboard/watchlist', 'FrontEndDashboardController@getWatchlist');
Route::get('dashboard/settings', 'FrontEndDashboardController@getSetttings');



Route::get('plans', 'FrontEndPlanController@index');
Route::get('search','SearchController@doSearch');
Route::post('browse', 'SearchController@browse');

Route::get('buy','BuyController@index');
Route::get('sell','SellController@index');
Route::get('bid','BidController@index');
Route::get('login',	'AuthController@login');

Route::post('login', 'AuthController@doLogin');
Route::get('logout', 'AuthController@frontendLogout');

Route::get('dashboard', 'FrontEndUserController@fetchInfo');
Route::get('register', 'FrontEndUserController@register');
Route::get('register/verify/{confirmation_code}', 'FrontEndUserController@validateAccount');
Route::post('register/save', 'FrontEndUserController@registerSave');
Route::post('setting/save', 'FrontEndUserController@settingSave');

Route::get('user/vendor/{id}/{username}', 'FrontEndUserController@fetchInfoPublic');

  Route::get('category/list/{id}', 'FrontEndCategoryController@fetchCategoryList');
  Route::get('buy', 'FrontEndCategoryController@getBuyCategory');
  Route::get('sell', 'FrontEndCategoryController@getSellCategory');
  Route::get('bid', 'FrontEndCategoryController@getBidCategory');
  Route::post('custom/search', 'FrontEndCategoryController@fetchCustomSearch');
  Route::post('get-custom-attributes', 'FrontEndCategoryController@getCustomAttributes');
  Route::get('profiler', 'FrontEndUserController@index');
  //Route::get('dashboard', 'FrontEndUserController@fetchInfo');
  Route::get('profile/{id}/ads', 'FrontEndUserController@viewUserAds');
  Route::get('profile/view/{id}', 'FrontEndUserController@fetchInfoPublic');

	Route::post('admin/login','AdminController@doLogin');
	Route::get('admin', 'AdminController@login');
Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/logout','AdminController@logout');
	Route::get('admin/dashboard','DashboardController@index');		
});

	Route::get('user/plan/expiring', 'FrontEndUserController@userPlanExpiring');
	Route::get('user/plan/expired', 'FrontEndUserController@userPlanExpired');
Route::group(array('before' => 'ajax'), function() {
	Route::post('profile/fetch-info/{id}', 'FrontEndUserController@doFetchInfo');
	Route::post('profile/save', 'FrontEndUserController@registerSave');
	Route::post('profile/photo/save', 'FrontEndUserController@changePhoto');
	Route::post('profile/photo/remove', 'FrontEndUserController@removePhoto');
	Route::get('dashboard/message', 'FrontEndUserController@message');

	Route::post('user/rate', 'FrontEndUserController@rateUser');
	Route::post('user/comment', 'FrontEndUserController@saveComment');
	Route::post('user/comment/reply', 'FrontEndUserController@saveCommentReply');
	Route::post('user/messages', 'FrontEndUserController@getMessages');
	Route::post('user/send/message', 'FrontEndUserController@saveUserMessage');
	Route::post('user/send/message/vendor', 'FrontEndUserController@sendMessage');
	Route::post('user/send/message/vendor/refresh', 'FrontEndUserController@getMessages');
	Route::post('user/send/message/vendor/status', 'FrontEndUserController@changeMessageStatus');
	Route::post('user/dashboard/auction/active/refresh', 'FrontEndUserController@refreshVendorAuctionActive');
	Route::post('user/dashboard/listing/active/refresh', 'FrontEndUserController@refreshVendorAdsActive');
	Route::post('user/dashboard/listing/save/make-auction', 'FrontEndUserController@adMakeAuction');
	Route::post('user/dashboard/listing/form/make-auction', 'FrontEndUserController@formMakeAuction');
	Route::post('user/dashboard/ad/make-basic-ad', 'FrontEndUserController@adMakeBasicAd');

});

	
	Route::get('ads/view/{id}', 'FrontEndAdvertisementController@fetchInfoPublicAds');
Route::group(array('before' => 'ajax'), function() {
	Route::get('ads/post', 'FrontEndAdvertisementController@postAds');
	Route::post('ads/fetch-info/{id}', 'FrontEndUserController@doFetchInfoPublicAds');
	Route::post('ads/refresh', 'FrontEndAdvertisementController@doList');
	Route::post('ads/save', 'FrontEndAdvertisementController@saveAds');
	Route::get('ads/redirect', 'FrontEndAdvertisementController@postAdRedirect');
	Route::post('get-sub-category','FrontEndAdvertisementController@getSubcategory');
	Route::post('ads/remove', 'FrontEndAdvertisementController@removeAds');
	Route::post('ads/disable', 'FrontEndAdvertisementController@disableAds');
	Route::post('ads/enable', 'FrontEndAdvertisementController@enableAds');
	Route::get('ads/edit/{id}', 'FrontEndAdvertisementController@editAds');
	Route::post('custom-attributes', 'FrontEndAdvertisementController@getCustomAttributes');
	// Route::post('auction-type', 'FrontEndAdvertisementController@getAuctionTypes');
	Route::post('ad-type', 'FrontEndAdvertisementController@getAdTypes');	
	Route::post('add-watchlist','FrontEndAdvertisementController@addWatchlist');
    Route::post('remove-watchlist','FrontEndAdvertisementController@removeWatchlist');
	Route::post('basic-ad/expired','FrontEndAdvertisementController@basicAdExpired');	 		
	Route::post('auction/expired','FrontEndAdvertisementController@auctionExpired');
	Route::post('auction/expiring','FrontEndAdvertisementController@auctionExpiring');
	Route::post('ad/comment/save', 'FrontEndAdvertisementController@saveAdComment');
	Route::post('ad/comment/reply/save', 'FrontEndAdvertisementController@saveAdCommentReply');
	Route::post('ad/rate', 'FrontEndAdvertisementController@rateAd');
	Route::post('ad/add/bidders', 'FrontEndAdvertisementController@addBidder');
	Route::post('ad/bidders/refresh', 'FrontEndAdvertisementController@refreshBidders');
	Route::post('ad/bidders-list/refresh', 'FrontEndAdvertisementController@refreshBidderList');


});

Route::get('admin/ads', 'AdvertisementController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/ads/manipulate', 'AdvertisementController@viewAds');
	Route::post('admin/ads/save', 'AdvertisementController@save');
	Route::post('admin/ads/refresh', 'AdvertisementController@doList');
	Route::post('admin/ads/edit', 'AdvertisementController@edit');
	Route::post('admin/ads/disable', 'AdvertisementController@disableAds');
	Route::post('admin/ads/enable', 'AdvertisementController@enableAds');
	Route::post('admin/ads/delete', 'AdvertisementController@removeAds');
	Route::post('admin/ads/mark-spam', 'AdvertisementController@markSpamAds');
	Route::get('admin/ads/{id}/view', 'AdvertisementController@viewAds');
	// Route::post('admin/auction-type', 'AdvertisementController@getAuctionTypes');
	Route::post('admin/ads/management-settings/edit', 'AdvertisementController@editAdManagemetSettings');
	Route::post('admin/ads/management-settings/save', 'AdvertisementController@saveAdManagemetSettings');
	Route::post('admin/ads/list-pictures', 'AdvertisementController@listAdPictures');
	Route::post('admin/ads/get-sub-category', 'AdvertisementController@getSubCategory');
	Route::post('admin/ads/get-category', 'AdvertisementController@getCategory');
	Route::post('admin/ads/get-city', 'AdvertisementController@getCity');
	Route::post('admin/ads/get-custom-attributes', 'AdvertisementController@getCustomAttributes');
	Route::post('admin/ads/fetch-custom-attributes-enabled', 'AdvertisementController@fetchCustomAttributesEnabled');
	Route::post('admin/ads/fetch-custom-attributes-disabled', 'AdvertisementController@fetchCustomAttributesDisabled');


});

Route::group(array('before' => 'ajax'), function() {
	Route::get('admin/users', 'UserController@index');
	Route::post('admin/users/refresh', 'UserController@doList');
	Route::post('admin/user/edit', 'UserController@edit');
});

Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/user/save', 'SystemController@save');
});

	Route::get('admin/category', 'CategoryController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/category/manipulate', 'CategoryController@edit');
	Route::post('admin/category/save', 'CategoryController@save');
	Route::post('admin/category/refresh', 'CategoryController@doList');
	Route::post('admin/category/get-subcat-sequence','CategoryController@checkSequenceSubCat');
	Route::post('admin/category/get-cat-sequence','CategoryController@checkSequenceCat');
	Route::post('admin/subcategory/manipulate','CategoryController@subEdit');
});

	Route::get('admin/category/custom-attributes', 'CustomAttributeController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/category/custom-attribute/save','CustomAttributeController@save');
	Route::post('admin/category/custom-attribute/refresh','CustomAttributeController@doList');
	Route::post('admin/category/custom-attribute/manipulate','CustomAttributeController@manipulateMainAttri');	
});

	Route::get('admin/banner', 'BannerController@index');
	Route::get('admin/banner/rates', 'BannerController@indexBannerRates');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/get-banner-plans', 'BannerController@getBannerPlacementPlan');
	Route::post('admin/banner/save', 'BannerController@save');
	Route::post('admin/banner/refresh', 'BannerController@doList');
	Route::post('admin/banner/edit', 'BannerController@editBanner');
	Route::post('admin/banner/delete', 'BannerController@deleteBanner');
	Route::post('admin/banner/rates/refresh', 'BannerController@doListBannerRates');
 	Route::post('admin/banner/rate/save', 'BannerController@saveBannerPlan');
 	Route::post('admin/banner/rate/delete', 'BannerController@deleteBannerPlan');
	Route::post('admin/banner/plan/manipulate', 'BannerController@manipulateBannerPlan');
});
	Route::get('admin/countries', 'CountryController@index');
	// Route::get('admin/banner/rates', 'CountryController@indexBannerRates');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/banner/save', 'CountryController@save');
	Route::post('admin/country/refresh', 'CountryController@doList');
	Route::post('admin/country/change-status', 'CountryController@changeStatus');
});
	Route::get('admin/cities', 'CityController@index');
	// Route::get('admin/banner/rates', 'CountryController@indexBannerRates');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/banner/save', 'CityController@save');
	Route::post('admin/city/refresh', 'CityController@doList');
	Route::post('admin/city/change-status', 'CityController@changeStatus');
});
	Route::get('admin/promotion', 'PromotionController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/save', 'PromotionController@save');
	Route::post('admin/promotion/manipulate', 'PromotionController@manipulate');
	Route::post('admin/promotion/delete', 'PromotionController@delete');
	Route::post('admin/promotion/refresh', 'PromotionController@doList');
	Route::post('admin/promotion/get-city', 'PromotionController@getCity');
	Route::post('admin/promotion/change-status','PromotionController@changeStatus');
});
	Route::get('admin/promotion/message', 'PromotionController@indexPromotionMessage');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/message/save', 'PromotionController@savePromotionMessage');
	Route::post('admin/promotion/message/manipulate', 'PromotionController@manipulatePromotionMessage');
	Route::post('admin/promotion/message/delete', 'PromotionController@delete');
	Route::post('admin/promotion/message/refresh', 'PromotionController@doListPromotionMessage');
});
	Route::get('admin/promotion/banner', 'PromotionController@indexPromotionBanner');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/promotion/banner/save', 'PromotionController@savePromotionBanner');
	Route::post('admin/promotion/banner/manipulate', 'PromotionController@manipulatePromotionBanner');
	Route::post('admin/promotion/banner/delete', 'PromotionController@deletePromotionBanner');
	Route::post('admin/promotion/banner/refresh', 'PromotionController@doListPromotionBanner');
});	

	Route::get('admin/email-template', 'EmailTemplateController@index');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/email-template/refresh', 'EmailTemplateController@doList');
	Route::get('admin/email-template/{id}/edit', 'EmailTemplateController@edit');
	Route::post('admin/email-template/manipulate', 'EmailTemplateController@view');
	Route::post('admin/email-template/change-status','EmailTemplateController@changeStatus');
	Route::post('admin/email-template/save', 'EmailTemplateController@save');	
});
	Route::get('admin/plan/vendor', 'PlanController@indexPlanVendor');
	Route::get('admin/plan/company', 'PlanController@indexPlanCompany');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/plan/vendor/refresh', 'PlanController@doListPlanVendor');
	Route::post('admin/plan/company/refresh', 'PlanController@doListPlanCompany');
	Route::post('admin/plan/save', 'PlanController@save');
	Route::post('user-types', 'PlanController@getUserType');
	Route::post('admin/plan/remove', 'PlanController@removePlan');
	Route::post('admin/plan/manipulate', 'PlanController@edit');
	Route::post('admin/plan/change-status','PlanController@changePlanStatus');
	Route::post('admin/plan/delete', 'PlanController@removePlan');
	Route::post('admin/discount/rate', 'PlanController@getPlanRate');
	Route::post('admin/plan/add-price', 'PlanController@savePrice');
	Route::post('admin/plan/get-plan-price', 'PlanController@getPlanPrice');
	Route::post('admin/plan/get-plan-price', 'PlanController@getPlanPrice');
	Route::post('admin/plan/price/manipulate', 'PlanController@editPlanPrice');
	Route::post('admin/plan/price/delete', 'PlanController@removePlanPrice');


});

	Route::get('admin/plan-rates/vendor', 'PlanRateController@indexPlanRatesVendor');
	Route::get('admin/plan-rates/company', 'PlanRateController@indexPlanRatesCompany');
Route::group(array('before' => 'ajax'), function() {
	Route::post('admin/plan-rates/vendor/refresh', 'PlanRateController@doListPlanRatesVendor');
	Route::post('admin/plan-rates/company/refresh', 'PlanRateController@doListPlanRatesCompany');
	Route::post('admin/plan-rates/save', 'PlanRateController@savePlanRates');
	Route::post('admin/plan-rates/remove', 'PlanRateController@removePlanRates');
	Route::get('admin/plan-rates/vendor/{id}/edit', 'PlanRateController@editVendor');
	Route::get('admin/plan-rates/company/{id}/edit', 'PlanRateController@editCompany');
	Route::post('admin/plan-rates/manipulate', 'PlanRateController@view');
	Route::post('admin/plan-rates/delete', 'PlanRateController@removePlanRates');
	Route::post('admin/plan-rates/disabled', 'PlanRateController@disablePlanRates');
	Route::post('admin/plan-rates/change-status','PlanRateController@changePlanRateStatus');
	Route::get('admin/plan-rates/create', 'PlanRateController@create');

});

Route::group(array('before' => 'ajax'), function() {
	Route::post('get-bannerplacementplans', 'FrontEndBannerController@getBannerPlacementPlan');
	Route::post('bannerplacement/save', 'FrontEndBannerController@saveBannerPlacement');
	Route::post('dasboard/banner/manipulate', 'FrontEndBannerController@viewBannerPlacement');
});


Route::get('banner/payment', 'FrontEndPaymentController@purchaseBannerPlacement');
Route::get('payment/create', 'FrontEndPaymentController@create');
Route::group(array('before' => 'ajax'), function() {
	Route::post('plan/payment', 'FrontEndPaymentController@purchasePlan');
	Route::post('banner/payment/save', 'FrontEndPaymentController@saveBannerPlacement');
	Route::post('user/dashboard/banner/redirect', 'FrontEndPaymentController@aw');
});


	// Route::get('gauth/{auth?}', array('as'=> 'googleAuth', 'uses'=>'ImportContactController@loginWithGoogle');

Route::group(array('before' => 'ajax'), function() {

});
