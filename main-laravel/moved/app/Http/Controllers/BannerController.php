<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use Request;
use Response;
use Input;
use Image;
use Validator;
use Carbon\Carbon;
use Paginator;
use App\Usertypes;

class BannerController extends Controller
{

	public function index(){
		$result = $this->doList();
    $this->data['rows'] = $result['rows'];
		$this->data['title'] = "Banner Management";
		$this->data['refresh_route'] = url("admin/banner/refresh");
		$this->data['banner_placement'] = BannerPlacement::all();
		$this->data['categories'] = Category::with('subcategoryinfo')->get();

    $get_plan_details = BannerPlacementPlans::all();
          $banner_placement_plan = '<option class="hide" value="">Select:</option>';
        foreach ($get_plan_details as $key => $field) {
          $banner_placement_plan .= '<option value="' . $field->id . '">'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }
        
    $this->data['banner_placement_plan'] = $banner_placement_plan;
		return View::make('admin.banner-management.list', $this->data);
	}
	public function doList(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = BannerSubscriptions::select('banner_subscriptions.*','banner_placement_plans.duration','banner_placement_plans.price','banner_placement.name')
                  							->leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_subscriptions.placement_plan')
                  							->leftjoin('banner_placement','banner_placement.id','=','banner_subscriptions.placement')
                  							 ->where(function($query) use ($status) {
                                    $query->where('banner_subscriptions.status', 'LIKE', '%' . $status . '%')
                                          ->where('banner_subscriptions.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_subscriptions.banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                              
      } else {
         $count = BannerSubscriptions::select('banner_subscriptions.*','banner_placement_plans.duration','banner_placement_plans.price','banner_placement.name')
                                ->leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_subscriptions.placement_plan')
                                ->leftjoin('banner_placement','banner_placement.id','=','banner_subscriptions.placement')
                                ->where(function($query) use ($status) {
                                    $query->where('banner_subscriptions.status', 'LIKE', '%' . $status . '%')
                                          ->where('banner_subscriptions.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_subscriptions.banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = BannerSubscriptions::select('banner_subscriptions.*','banner_placement_plans.duration','banner_placement_plans.price','banner_placement.name')
                                ->leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_subscriptions.placement_plan')
                                ->leftjoin('banner_placement','banner_placement.id','=','banner_subscriptions.placement')
                                ->where(function($query) use ($status) {
                                    $query->where('banner_subscriptions.status', 'LIKE', '%' . $status . '%')
                                          ->where('banner_subscriptions.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_subscriptions.banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
	public function save(){

		$new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerSubscriptions::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'              	  => $new ? 'image|max:3000' : 'image|max:3000',
            'banner_name'      		  => 'required',
            'advertiser_name'      	  => 'required',
            'banner_placement_id'     => 'required',
            'banner_placement_plan'   => 'required',
            'cat_id'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'photo'          		 => 'photo',
            'banner_name'			 => 'banner name',
            'advertiser_name'		 => 'advertiser',
            'banner_placement_id'       => 'Banner placement',
            'banner_placement_plan'  => 'Banner placement plan',
            'cat_id'  => 'Category Id',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerSubscriptions;
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
        } else {
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
         	  $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->save();
        }   
          	$row->save();
          	$get_plan_duration = BannerPlacementPlans::find($row->placement_plan);
          	$date = strtotime(date("Y-m-d H:i:s", strtotime($row->created_at)) . " +".$get_plan_duration->duration." month");
          	$row->expiration = date("Y-m-d H:i:s", $date);
          	$row->save();

        	if (Input::file('photo')) {
	            // Save the photo
	            Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-'.$row->id . '.jpg');
	            $row->photo = 'banner-'.$row->id . '.jpg';
	            $row->save();
       		}

	    return Response::json(['body' => ['Banner Advertisement Saved']]);

	}
	
	public function getBannerPlacementPlan(){

        $get_plan_details = BannerPlacementPlans::where('banner_placement_id', '=',Request::input('id') )->orderBy('duration','asc')->get();
        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_plan_details as $key => $field) {
          $rows .= '<option value="' . $field->id . '">'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }
        return Response::json($rows);
	}
  
  public function deleteBanner(){

    $row = BannerSubscriptions::find(Request::input('id'));
    if(!is_null($row)){
      $row->status = '2';
      $row->save();
       return Response::json(['body' => ['Banner has been removed']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }

  public function editBanner(){

    $result['rows'] =BannerSubscriptions::find(Request::input('id'));

    $banner_plans = BannerPlacementPlans::where('banner_placement_id','=',$result['rows']->placement)->get();
    
          $plans = '<option class="hide" value="">Select:</option>';
        foreach ($banner_plans as $key => $field) {
          $plans .= '<option value="' . $field->id . '" '.($field->id == $result['rows']->placement_plan ? 'selected':'' ).'>'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
        }

    $result['data']  = $plans;
    return Response::json($result);
     
   }
   public function doListBannerRates(){
     $result['sort'] = Request::input('sort') ?: 'id';
      $result['order'] = Request::input('order') ?: 'asc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 1;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = BannerPlacementPlans::select('banner_placement_plans.*','placement.name as pl_name','placement.id as pl_id') 
                                ->leftjoin('banner_placement as placement','placement.id','=','banner_placement_plans.banner_placement_id')
                                ->where(function($query) use ($status) {
                                    $query->where('banner_placement_plans.status', 'LIKE', '%' . $status . '%')
                                          ->where('banner_placement_plans.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('placement.name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('banner_placement_plans.usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
         $count = BannerPlacementPlans::select('banner_subscriptions.*','banner_placement_plans.duration','banner_placement_plans.price','banner_placement.name')
                                ->leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_subscriptions.placement_plan')
                                ->leftjoin('banner_placement','banner_placement.id','=','banner_subscriptions.placement')
                                ->where(function($query) use ($status) {
                                    $query->where('banner_subscriptions.status', 'LIKE', '%' . $status . '%')
                                          ->where('banner_subscriptions.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_subscriptions.banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = BannerPlacementPlans::select('banner_subscriptions.*','banner_placement_plans.duration','banner_placement_plans.price','banner_placement.name')
                                ->leftjoin('banner_placement_plans','banner_placement_plans.id','=','banner_subscriptions.placement_plan')
                                ->leftjoin('banner_placement','banner_placement.id','=','banner_subscriptions.placement')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
   }
   public function indexBannerRates(){

      $result = $this->doListBannerRates();
      $this->data['rows'] = $result['rows'];
      $this->data['title'] = "Banner Rate Management";
      $this->data['refresh_route'] = url("admin/banner/rates/refresh");
      $this->data['banner_placement'] = BannerPlacement::all();
      $this->data['usertype'] = Usertypes::all();
      $this->data['categories'] = Category::with('subcategoryinfo')->get();
      return View::make('admin.banner-management.banner-rates.list', $this->data);
   }
   public function manipulateBannerPlan(){
    
      $result['rows'] = BannerPlacementPlans::with('getBannerPlacement')->find(Request::input('id'));

      $banner_plans = BannerPlacement::all();
            $plans = '<option class="hide" value="">Select:</option>';
          foreach ($banner_plans as $key => $field) {
            $plans .= '<option value="' . $field->id . '" '.($field->id == $result['rows']->banner_placement_id ? 'selected':'' ).'>'. $field->duration.' month'.($field->duration > 1 ? 's':'').' / '.'$'.$field->price.'</option>';
          }

      $result['data']  = $plans;
      return Response::json($result);

   }
   public function saveBannerPlan(){

        $new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerPlacementPlans::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'banner_placement_id'           => 'required',
            'duration'         => 'required',
            'price'     => 'required',
            'points'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'banner_placement_id'      => 'banner placement',
            'duration'         => 'duration',
            'price'     => 'price',
            'points'   => 'reward points'
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerPlacementPlans;
            $row->banner_placement_id     = array_get($input, 'banner_placement_id');
            $row->duration     = array_get($input, 'duration');
            $row->price     = array_get($input, 'price');
            $row->period     = array_get($input, 'period');
            $row->usertype_id = array_get($input, 'usertype_id');
            $row->discount     = array_get($input, 'discount');
            $row->points     = array_get($input, 'points');
        } else {
            $row->banner_placement_id     = array_get($input, 'banner_placement_id');
            $row->duration     = array_get($input, 'duration');
            $row->price     = array_get($input, 'price');
            $row->period     = array_get($input, 'period');
            $row->discount     = array_get($input, 'discount');

            $row->points     = array_get($input, 'points');
            $row->save();
        }   
            $row->save();


      return Response::json(['body' => ['Banner Rate Saved']]);

   }
   public function deleteBannerPlan(){
    $row = BannerPlacementPlans::find(Request::input('id'));
    if(!is_null($row)){
      $row->status=2;
      $row->save();
       return Response::json(['body' => ['Banner Rate has been Removed']]);
    }else{
       return Response::json(['error' => ['error on finding the id']]);
    }

   }
   
}