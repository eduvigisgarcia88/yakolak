<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\UserPlanTypes;
use App\UserPlanRates;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Request;


class PlanRateController extends Controller
{

         public function indexPlanRatesVendor()
    {
        // get row set
        $result = $this->doListPlanRatesVendor();
        // assign vars to template
        $this->data['rows'] = $result['rows'];

        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/plan-rates");
        $this->data['title'] = "Plan Rates Settings";
         $this->data['refresh_route'] = url("admin/plan-rates/vendor/refresh");
      $this->data['usertype']  = Usertypes::all();
        return view('admin.plan-management.plan-rates.vendor.list', $this->data);

    
    }

             public function indexPlanRatesCompany()
    {
        // get row set
        $result = $this->doListPlanRatesCompany();
        // assign vars to template
        $this->data['rows'] = $result['rows'];

        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/plan-rates");
        $this->data['title'] = "Plan Rates Settings";
         $this->data['refresh_route'] = url("admin/plan-rates/company/refresh");
      $this->data['usertype']  = Usertypes::all();
        return view('admin.plan-management.plan-rates.company.list', $this->data);

    
    }

    /**
     * Build the list
     *
     */
    public function doListPlanRatesVendor() {
   $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 1;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = UserPlanRates::select('user_plan_rates.*') 
                               ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
        $count = UserPlanRates::select('user_plan_rates.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('user_plan_rates.rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanRates::select('user_plan_rates.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
 public function doListPlanRatesCompany() {
   $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 2;
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = UserPlanRates::select('user_plan_rates.*') 
                               ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                  
      } else {
        $count = UserPlanRates::select('user_plan_rates.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('user_plan_rates.rate_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });
        $rows = UserPlanRates::select('user_plan_rates.*') 
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('banner_name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function editVendor($id) {
    

        if (is_null(UserPlanRates::find($id))) {
            return abort(404);
        } else {
            $this->data['planrate'] = UserPlanRates::find($id);
            $this->data['title'] = 'Editing '.$this->data['planrate']->rate_name ;

             $this->data['usertype_id'] = $this->data['planrate']->usertype_id;
             $this->data['usertype']  = Usertypes::all();
            return view('admin.plan-management.plan-rates.vendor.edit', $this->data);
        }
    } 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */   
    public function editCompany($id) {
    

        if (is_null(UserPlanRates::find($id))) {
            return abort(404);
        } else {
            $this->data['planrate'] = UserPlanRates::find($id);
            $this->data['title'] = 'Editing '.$this->data['planrate']->rate_name ;

             $this->data['usertype_id'] = $this->data['planrate']->usertype_id;
             $this->data['usertype']  = Usertypes::all();
            return view('admin.plan-management.plan-rates.company.edit', $this->data);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function savePlanRates() {    

        $input = Request::all();
        //dd($input);
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = UserPlanRates::find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
             'rate_name'                  => 'required|min:5|max:100',
             'duration'                  => 'required|numeric|min:1|max:12',
             'discount'                   => 'required|numeric|min:1|max:100',
             'description'               => 'required|min:5|', 
        );

        // field name overrides
        $names = array(
            'rate_name'              => 'rate name',
            'duration'              => 'duration',
            'discount'              => 'discount',
            'description'              => 'description',

        
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new UserPlanRates;
        }
          if (array_get($input, 'rate_name')) {
              $row->rate_name     = array_get($input, 'rate_name');
            }
          else{
            $row->rate_name  = null;
          }
            if (array_get($input, 'usertype_id')) {
              $row->usertype_id     = array_get($input, 'usertype_id');
            }
          else{
            $row->usertype_id  = null;
          }
            if (array_get($input, 'duration')) {
              $row->duration     = array_get($input, 'duration');
            }
          else{
            $row->duration       = null;
          }
             if (array_get($input, 'discount')) {
              $row->discount = array_get($input, 'discount');
            }
          else{
            $row->discount   = null;
          }
           if (array_get($input, 'description')) {
              $row->description = array_get($input, 'description');
            }
          else{
            $row->description  = null;
          }

       
        $row->status    = 1;
        // save model
        $row->save();

 if ($new) {
        // return
        return Response::json(['body' => 'Plan Rate successfully added.']);
      } else {
        return Response::json(['body' => 'Plan Rate successfully updated.']);

      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        $this->data['usertypes'] = Usertypes::all();

      return view('admin.plan-management.plan-rates.create', $this->data);
        
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

 public function removePlanRates() {
          $row = UserPlanRates::find(Request::input('id'));

      if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Plan Rate Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the rate']]); 
      }
    }
     public function disablePlanRates() {
          $row = UserPlanRates::find(Request::input('id'));

      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Plan Rate Successfully Disabled']);      
      }else{
        return Response::json(['error' => ['error on finding the rate']]); 
      }
    }

   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

        public function view() {
    $row = UserPlanRates::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
   /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
      public function changePlanRateStatus(){

    $row = UserPlanRates::find(Request::input('id'));
    if(!is_null($row)){

      $row->rate_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Plan Rate status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
}

