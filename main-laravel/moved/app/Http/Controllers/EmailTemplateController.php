<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Category;
use App\Newsletters;
use Request;


class EmailTemplateController extends Controller
{

         public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];
        
        $this->data['url'] = url("admin/news");
        $this->data['title'] = "Email Templates";
        return view('admin.email-template-management.list', $this->data);

    
    }

    /**
     * Build the list
     *
     */
    public function doList() {
    $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 15;
      // dd(Request::all());
      if (!Auth::check()) {

          // return response (format accordingly)
          if(Request::ajax()) {
              return Response::json(['relogin' => "Relogin"]);
          }
      }

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Newsletters::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      } else {
        $count = Newsletters::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Newsletters::where('title', 'LIKE', '%' . $search . '%')->orderBy($result['sort'], $result['order'])->paginate($per);

      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (is_null(Newsletters::find($id))) {
            return abort(404);
        } else {
            $this->data['newsletters'] = Newsletters::find($id);

            $this->data['title'] = $this->data['newsletters']->title;

            return view('admin.email-template-management.edit', $this->data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

  public function save() {    

        $input = Request::all();
        //dd($input);
        // assume this is a new row
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = Newsletters::find(array_get($input, 'id'));

            if(!$row) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }

            // this is an existing row
            $new = false;
        }

        $rules = array(
            'title' => 'required',
            'body' => 'required',
            'subject' => 'required',
        );

        // field name overrides
        $names = array(
            'title' => 'title',
            'body' => 'body',
            'subject' => 'subject',
        );

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($new) {
            $row = new Newsletters;
        }

        $row->title     = array_get($input, 'title');
        $row->subject   = array_get($input, 'subject');
        $row->body      = array_get($input, 'body');
        $row->status    = 1;
        // save model
        $row->save();

        // return
        return Response::json(['body' => 'News successfully added.']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        return view('news.create', $this->data);
    }

       /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

        public function view() {
    $row = Newsletters::find(Request::input('id'));
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }
       /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

  public function changeStatus(){

    $row = Newsletters::find(Request::input('id'));
    if(!is_null($row)){

      $row->newsletter_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Promotion status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
}