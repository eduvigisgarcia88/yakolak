<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\UserPlanTypes;
use App\UserAdComments;
use App\UserAdCommentReplies;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\AdvertisementType;
use App\Http\Controllers\Controller;
use App\Advertisement;
use App\AdvertisementPhoto;
use App\AdRating;
use App\AdvertisementBidDurationDropdown;
use App\SubAttributes;
use App\CompanyBranch;
use App\Country;
use App\City;
use App\CustomAttributes;
use App\CustomAttributeValues;
use App\CustomAttributesPerAd;
use App\CustomAttributesForTextbox;
use App\CustomAttributesForRadio;
use App\CustomAttributesForDropdown;
use App\CustomAttributesForCheckbox;
use App\AdvertisementWatchlists;
use App\Newsletters;
use App\PromotionPopups;
use App\PromotionPopupStatus;
use App\BannerSubscriptions;
use App\AuctionBidders;
use App\AdManagementSettings;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Mail;
use Form;
use Route;
use Carbon\Carbon;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\UserMessages;
use App\UserNotifications;


class FrontEndAdvertisementController extends Controller
{
   public function index(){

    $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";

      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

   public function doList() {
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Advertisement::select('advertisements.title', 'advertisements.category', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo')
                         ->leftjoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('advertisements_photo.primary', '=', '1')
                                          ->where('advertisements.status', '=', '1')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

      } else {
        $count = Advertisement::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::select('advertisements.*')
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

    }


    public function editAds($id){

     $row = Advertisement::find($id);
     // check ad owner
     if(Auth::user()->id != $row->user_id){
      return Redirect::to('/');
    }
     $ads_id = $row->id;

     if($row) {    
        $this->data['ads'] = Advertisement::join('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                       ->leftJoin('categories','advertisements.category_id', '=', 'categories.id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.bid_duration_dropdown_id','advertisements.bid_duration_start','advertisements.minimum_allowed_bid','advertisements.bid_start_amount','advertisements.bid_limit_amount', 
                                        'advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id',
                                         'advertisements.category_id','advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube') 
                                        ->where(function($query) use($ads_id) {
                                            $query->where('advertisements_photo.ads_id','=',$ads_id);
                                              })->first();

        $this->data['ads_photos'] = AdvertisementPhoto::where('advertisements_photo.ads_id','=',$ads_id)->where('advertisements_photo.status','=',1)->get();  

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $custom_attributes = "";
          foreach ($get_dropdown as $key => $field) {

            $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
          $i = 1;
             foreach ($get_attri_info as $key => $field_attri_info) {
                $custom_attributes .= '<div class="form-group" id="ads-custom_attribute'.$i++.'">'.
                                      '<h5 class="col-sm-3 normalText" for="register4-email">'.$field_attri_info->name.'</h5>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero" name="dropdown_field[]">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $custom_attributes .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $custom_attributes .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
               foreach ($get_attri_info as $key => $field_attri_info) {

                $custom_attributes .= '<div class="form-group">'.
                                        '<h5 class="col-sm-3 normalText" for="register4-email">'.$field_attri_info->name.'</h5>'.
                                          '<div class="col-sm-9">';                  
                $custom_attributes .= '<input type="text" class="form-control borderZero" name="text_field[]" value='.$field->attri_value.'>';                  
                                 
                $custom_attributes .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'">'.
                                  '</div>'; 
               }           
          }


          $this->data['countries'] = Country::all();
          $this->data['city'] = City::all();
          $this->data['get_ad_types'] = AdvertisementType::all(); 
          $this->data['country'] = Country::all();            
          $this->data['custom_attributes_checkbox'] = CustomAttributesForCheckbox::where('ad_id','=',$ads_id)->get();
          $this->data['custom_attributes_radio'] = CustomAttributesForRadio::where('ad_id','=',$ads_id)->get();
          $this->data['bid_duration_dropwdown'] = AdvertisementBidDurationDropdown::orderBy('id', 'desc')->get(); 
    
     if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }

         $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
         $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['category'] = Category::with('subcategoryinfo')->get();
         $this->data['countries'] = Country::all();
         $this->data['city'] = City::all();
         $this->data['usertypes'] = Usertypes::all();
         $this->data['ads_types'] = AdvertisementType::all();
         $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get(); 
         $this->data['sub_categories'] = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', $this->data['ads']->category_id)->get(); 

      }else{
          return Response::json(['error' => "Invalid row specified"]);
        }
        $this->data['title'] = "Edit My Ads"; 
        $this->data['category'] = Category::with('subcategoryinfo')->get();
        $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        $this->data['ads_types'] = AdvertisementType::all();
        $this->data['custom_attributes'] = $custom_attributes; 
        $this->data['sub_categories'] = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', $this->data['ads']->category_id)->get(); 
        
        return view('ads.edit', $this->data);
   }


public function postAds()
{
        if (!Auth::check()) {
        return Redirect::to('/');
        }
        $user_id = Auth::user()->id;
        $user_plan_type = Auth::user()->user_plan_types_id;
        $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

        $total_ad_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();

         $get_user_plan_types = UserPlanTypes::find($user_plan_type);
    
         $post_ad_btn = "";
          if (count($total_ad_posted) >= $get_user_plan_types->total_ads_allowed) {
        $post_ad_btn .= '<input type="button" class="btn blueButton borderZero noMargin pull-right btn-post_not_allowed" value="Post">';   
          } else {
           $post_ad_btn .= '<input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Post">';   
          }
          //compute listing available
         $listing_available = $get_user_plan_types->total_ads_allowed - count($total_ad_posted);
         if ($listing_available <= 0) {
           $this->data['get_listing_available'] = '0';
         } else{
          $this->data['get_listing_available'] = $listing_available;
         }

         //compute auction ad available
         $auction_ads_available = $get_user_plan_types->auction - count($total_ad_posted);
         if ($auction_ads_available <= 0) {
           $this->data['get_auction_ads_available'] = '0';
         } else{
          $this->data['get_auction_ads_available'] = $auction_ads_available;
         }

         if(Auth::check()){
              $user_id = Auth::user()->id;
          }else{
              $user_id = null;
          }

          $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
          $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
         $this->data['get_user_country'] = Auth::user()->country;
         $this->data['get_user_city'] = Auth::user()->city;
         $this->data['get_user_address'] = Auth::user()->address;
         $this->data['get_user_plan_types'] = $get_user_plan_types;
         $this->data['post_ad_btn'] = $post_ad_btn;
         $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
         $this->data['category'] = Category::with('subcategoryinfo')->get();
         $this->data['countries'] = Country::all();
         $this->data['city'] = City::all();
         $this->data['usertypes'] = Usertypes::all();
         $this->data['ads_types'] = AdvertisementType::all();
         $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get(); 
         $this->data['bid_duration_dropwdown'] = AdvertisementBidDurationDropdown::orderBy('id', 'desc')->get(); 
         

      return view('ads.post', $this->data);
}

    public function search(){
        $input = Input::all();
        $row = Advertisement::where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get(); 
        

}
    public function saveAds(){
        $input = Input::all();
        $new = true;
        $user_id = Auth::user()->id;
        $total_ad_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();

        $user_plan_type = Auth::user()->user_plan_types_id;                               
        $input = Input::all();
        $id = Input::get('id');
        $category = Input::get('category');
        $photo = array_get($input, 'photo');
        $rem_img_upload = Input::get('rem_img_upload');
        $delete_photo = Input::get('delete_photo[]');
        $text_field = Input::get('text_field');
        $dropdown_field = Input::get('dropdown_field');
        $attribute_textbox_id = Input::get('attribute_textbox_id');
        $attribute_dropdown_id = Input::get('attribute_dropdown_id');

        $text_field_id = Input::get('text_field_id');
        $dropdown_field_id = Input::get('dropdown_field_id');
        $country = Input::get('country');
        $photo_id1 = Input::get('photo_id1');
        $photo_id2 = Input::get('photo_id2');
        $photo_id3 = Input::get('photo_id3');
        $photo_id4 = Input::get('photo_id4');
        $bid_limit_amount = Input::get('bid_limit_amount');
        $bid_start_amount = Input::get('bid_start_amount');
        $bid_duration_start = Input::get('bid_duration_start');
        $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
        $ads_type_id = Input::get('ad_type');
        
        if (Input::get('city2')) {
         $city = 'city2';
        } else if (Input::get('city')) {
         $city = 'city';

        }
        $contact_info = Auth::user()->telephone;
        if (is_null($contact_info)) {
          $contact_info = Auth::user()->mobile; 
        }

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

     // getting all of the post data
      $files = Input::file('photo');

         $i = $rem_img_upload;

        $rules = [
             'photo1'                 => ($new==true?'image|max:3000|required':'image|max:3000'),
             'photo2'                 => 'image|max:3000',
             'photo3'                 => 'image|max:3000',
             'photo4'                 => 'image|max:3000',
             'photo5'                 => 'image|max:3000',
             'photo6'                 => 'image|max:3000',
             'photo7'                 => 'image|max:3000',
             'photo8'                 => 'image|max:3000',
             'photo9'                 => 'image|max:3000',
             'photo10'                => 'image|max:3000',
             'photo11'                => 'image|max:3000',
             'photo12'                => 'image|max:3000',
             'photo13'                => 'image|max:3000',
             'photo14'                => 'image|max:3000',
             'photo15'                => 'image|max:3000',
             'photo17'                => 'image|max:3000',
             'photo18'                => 'image|max:3000',
             'photo19'                => 'image|max:3000',
             'photo20'                => 'image|max:3000',
             'title'                  => 'required|min:4|max:100',
             'description'            => 'required',
             'category'               => 'required',
             'subcategory'            => 'required',
             'price'                  => 'required|min:5|max:100000000|numeric',
             'bid_limit_amount'       => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_start_amount'       => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'minimum_allowed_bid' => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_duration_start'     => ($ads_type_id==2?'min:1|max:99|numeric|required':''),
             'bid_duration_dropdown_id' => ($ads_type_id==2?'min:1|max:3|numeric|required':''),
             'country'                => 'min:1|numeric|required',
             (!is_null(Input::get('city2'))?'city2':'city')  => 'required',
        ];
        // field name overrides
        $names = [       
            'photo1'              => 'photo',
            'photo2'              => 'photo',
            'photo3'              => 'photo',
            'photo4'              => 'photo',
            'photo5'              => 'photo',
            'photo6'              => 'photo',
            'photo7'              => 'photo',
            'photo8'              => 'photo',
            'photo9'              => 'photo',
            'photo10'             => 'photo',
            'photo11'             => 'photo',
            'photo12'             => 'photo',
            'photo13'             => 'photo',
            'photo14'             => 'photo',
            'photo15'             => 'photo',
            'photo16'             => 'photo',
            'photo17'             => 'photo',
            'photo18'             => 'photo',
            'photo19'             => 'photo',
            'photo20'             => 'photo',
            'title'               => 'title',
            'category'            => 'category',
            'subcategory'         => 'sub category',
            'description'         => 'description',
            'price'                     => 'price',
            'bid_limit_amount'          => 'bid limit amount',
            'bid_start_amount'          => 'bid start amount',
            'minimum_allowed_bid'    => 'minimum allowed amount',
            'bid_duration_start'        => 'bid duration',
            'bid_duration_dropdown_id'  => 'bid duration',
            'country'             => 'Country',
            (!is_null(Input::get('city2'))?'city2':'city') => 'City',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($ads_type_id==2) {
           if ($bid_limit_amount <= $bid_start_amount) {
            return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start amount']); 
            }
        }
       
       //check post limit depends on user plan
        $get_plan = UserPlanTypes::find($user_plan_type);
          $get_posted_auctions = Advertisement::where('ads_type_id','=','2')->where('user_id','=', $user_id)->where('status','!=','2')->get();
          $get_posted_basic_ad = Advertisement::where('ads_type_id','=','1')->where('user_id','=', $user_id)->where('status','!=','2')->get();
          $get_all_post = Advertisement::where('user_id','=', $user_id)->where('status','!=','2')->get();
        if (count($get_all_post) >= $get_plan->total_ads_allowed && $new == true) {
                     return Response::json(['erroruser' => "You have reached the alowed Advetisement post"]);     
          } 
          //check limit on edit ad page
          if ($new == false && $row->ads_type_id != $ads_type_id) {
            if ($ads_type_id == '2') {
                if (count($get_posted_auctions) >= $get_plan->auction ) {
                     return Response::json(['erroruser' => "You have reached the alowed auction limit"]);     
                } 
            } else {
                if (count($get_posted_basic_ad) >= $get_plan->ads) {
                     return Response::json(['erroruser' => "You have reached the alowed basic ad limit"]);     
                }
            }

              $remove_existing_bidders = AuctionBidders::where('ads_id','=',$row->id)->get();
            if ($remove_existing_bidders) {
               foreach ($remove_existing_bidders as $value) {
                 $value->bid_limit_amount_status = 0;
                 $value->bidder_status = 0;
                 $value->status = 0;
                 $value->save();
               }
            }
  
        }
     // create model if new
      if($new) {
          $row = new Advertisement;
      } 
          $get_unique_id                   = Category::find($category);
          $row->ads_type_id                = array_get($input, 'ad_type');
          $row->cat_unique_id              = $get_unique_id->unique_id;
          $row->category_id                = $category;
          if (array_get($input, 'subcategory')) {
           $row->sub_category_id           = array_get($input, 'subcategory');
          }else {
           $row->sub_category_id           = null;
          }
          $row->user_id                    = Auth::user()->id;
          $row->title                      = array_get($input, 'title');
          $row->price                      = array_get($input, 'price');
          if ($contact_info) {
           $row->contact_info              = $contact_info;
          }else {
           $row->contact_info              = null;
          }

          if (array_get($input, 'description')) {
          $row->description                = array_get($input, 'description');
          } else {
           $row->description               = null;
          }

          //ad expiration auction
         if ($row->bid_duration_start != $bid_duration_start || $row->bid_duration_dropdown_id != $bid_duration_dropdown_id) {
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                }
          } 
          if ($ads_type_id == 2) {
            $row->bid_limit_amount         = array_get($input, 'bid_limit_amount');
            $row->bid_start_amount         = array_get($input, 'bid_start_amount');
            if ($new == true) {
            $row->minimum_allowed_bid      = array_get($input, 'minimum_allowed_bid'); 
            } else {
             $reset_bidder = AuctionBidders::where('ads_id','=',$row->id)->where('bid_limit_amount_status','=',1)->first();
              if ($reset_bidder) {
                $reset_bidder->bid_limit_amount_status = '0';
                $reset_bidder->save();
              }
            }
            $row->bid_duration_start       = array_get($input, 'bid_duration_start');
            $row->bid_duration_dropdown_id = array_get($input, 'bid_duration_dropdown_id');
          } else {
            $row->bid_limit_amount          = null;
            $row->bid_start_amount          = null; 
            $row->minimum_allowed_bid       = null;
            $row->bid_duration_start        = null;
            $row->bid_duration_dropdown_id  = null;
          }

          //ad expiration basic ad
          $get_ad_mgmt_expire = AdManagementSettings::where('id','=','1')->first();
          if (array_get($input, 'ad_type') == '1' && $get_ad_mgmt_expire->basic_ad_expiration_status == '1') {
             $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($get_ad_mgmt_expire->basic_ad_expiration_end == 'hours') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." hours"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'days'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'months'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                } elseif ($get_ad_mgmt_expire->basic_ad_expiration_end == 'years'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_mgmt_expire->basic_ad_expiration_start." years"));                 
                   $row->ad_expiration = $expiration_date;
                }
            } else if (array_get($input, 'ad_type') == '1') {
             $row->ad_expiration = '0000-00-00 00:00:00';
            }

          if (array_get($input, 'youtube')) {
            $row->youtube                  = array_get($input, 'youtube');
          } else {
            $row->youtube                  = null;
          }
            if (array_get($input, 'country')) {
            $row->country_id               = array_get($input, 'country');
          } else {
            $row->country_id               = null;
          }
            if (array_get($input, 'city2')) {
            $row->city_id                  = array_get($input, 'city2');
          } else {
            $row->city_id                 = array_get($input, 'city');
          }
            if (array_get($input, 'address')) {
            $row->address                  = array_get($input, 'address');
          } else {
            $row->address                  = null;
          }

          //for auction exp email trigger
          if ($row->ads_type_id == 2) {
           $row->ad_exp_email_trigger = 1;
           $row->auction_expiring_noti_trigger = 1;

          }
            // save model
          $row->save();

     if ($new) {
      $i = $rem_img_upload;

      for ($x = 1; $x <= $i; $x++) {
        if(Input::file('photo'.$x.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$x.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$x.''))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = new AdvertisementPhoto;
             $savephoto->ads_id = $row->id;
                if ($x == 1) {
                    $savephoto->primary = 1;
                 }
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }
            else{
        $savephoto = new AdvertisementPhoto;
        $savephoto->ads_id = $row->id;
        $savephoto->photo = null;
        $savephoto->save();
        }
      }

      if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
      if(!is_null($dropdown_field)){
        foreach ($dropdown_field_id as $key => $xvalue) {
           $ad_attri = new CustomAttributesForDropdown;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $dropdown_field_id[$key];
           $ad_attri->attri_value_id  = $dropdown_field[$key];
           $ad_attri->save();
        }
      } 
   }
   //FOR EDITING ADS
  else{
    // Save the photo
   $i = $rem_img_upload;
     $get_photos = AdvertisementPhoto::where('ads_id','=',$id)->get();
     $get_primary_photos = AdvertisementPhoto::where('ads_id','=',$id)->where('primary','=','1')->first();
     $primary_photo = array_get($input, 'delete_photo'.$get_primary_photos->id.'');

  foreach($get_photos as $value) {     
if ($primary_photo) {
   $rules = [
             'photo'.$primary_photo.'' => 'required',
            ];
   $names = [           
            'photo'.$primary_photo.''  => 'Primary Photo',
        ];

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
}
         
  //save primary
        if(Input::file('photo'.$primary_photo.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$primary_photo.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$primary_photo.''))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = AdvertisementPhoto::find($primary_photo);
             $savephoto->ads_id = $row->id;
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }
  //save other photos
       if (Input::file('photo'.$value->id.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$value->id.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$value->id.''))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = AdvertisementPhoto::find($value->id);
             $savephoto->ads_id = $row->id;
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }

  
   //delete photo    
         $delete = array_get($input, 'delete_photo'.$value->id);
         if ($delete) {
        $get_delete_photo = AdvertisementPhoto::find($delete);
        $get_delete_photo->photo = null;
        $get_delete_photo->save();
         }
       
  }

$get_exist_dropdown = CustomAttributesForDropdown::where('ad_id','=',$id)->get();
if ($get_exist_dropdown) {
     foreach ($get_exist_dropdown as $values) {
        $values->status = '2';
        $values->save();    
     }

      if(!is_null($dropdown_field)){
                foreach ($dropdown_field_id as $key => $xvalue) {     
                   $ad_attri = new CustomAttributesForDropdown;
                   $ad_attri->ad_id  = $row->id;
                   $ad_attri->attri_id = $dropdown_field_id[$key];
                   $ad_attri->attri_value_id  = $dropdown_field[$key];
                   $ad_attri->save();
                }
          } 
}
$get_existing_textbox = CustomAttributesForTextbox::where('ad_id','=',$id)->get();
if ($get_existing_textbox) {
     foreach ($get_existing_textbox as $values) {
        $values->status = '2';
        $values->save();    
     }

   if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
}
   
  }

       if($new){
             if (Auth::user()->name) {
                 $name = Auth::user()->name;
             }  else {
              $name = null;
             }

         //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
              if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               }  
              $item_title = $row->title;
              $item_url = 'http://www.yakolak.com/ads/view/'.$row->id;
              $item_link = 'http://www.yakolak.com/user/vendor/'.Auth::user()->id;
              $comment = 'http://www.yakolak.com/ads/view/'.$row->id;

                  //email user new ad created
             if ($row->ads_type_id == '1') {
                 $newsletter = Newsletters::where('id', '=', '2')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.ad_creation', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
                     }
                 
             }else{
                  //email user auction created
                 $newsletter = Newsletters::where('id', '=', '5')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.ad_creation', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
                     }
             }

        }

    if($new){
      return Response::json(['body' => "Advertisement Created"]);
    } 
    else {
         return Response::json(['body' => "Advertisement Successfully updated"]);
       }

    }



   public function fetchInfoPublicAds ($id){

      $result = $this->doFetchInfoPublicAds($id);

      $this->data['ads_id'] = $id;
      $this->data['getads_info']          = $result['getads_info'];
      $this->data['ads_photos']           = $result['ads_photos'];
      $this->data['primary_photos']       = $result['primary_photos'];
      $this->data['thumbnails']           = $result['thumbnails'];
      $this->data['thumbnails_set2']      = $result['thumbnails_set2'];
      $this->data['thumbnails_set3']      = $result['thumbnails_set3'];
      $this->data['vendor_ads']           = $result['vendor_ads'];
      $this->data['featured_ads']         = $result['featured_ads'];
      $this->data['related_ads_mobile']   = $result['related_ads_mobile'];
      $this->data['related_ads']          = $result['related_ads'];
      $this->data['related_adset2']       = $result['related_adset2'];
      $this->data['custom_attributes']    = $result['custom_attributes'];
      $this->data['comments']             = $result['comments'];
      $this->data['get_average_rate']     = $result['get_average_rate'];
      $this->data['watch_item']           = $result['watch_item'];
      $this->data['form']                 = $result['form'];
      $this->data['modal_related_popups'] = $result['modal_related_popups'];
      $this->data['modal_related_popups_duration'] = $result['modal_related_popups_duration'];
      $this->data['get_bidders']          = $result['get_bidders'];
      $this->data['get_all_bidders']      = $result['get_all_bidders'];
      $this->data['user_id']              = $result['user_id'];

      if (Auth::check()) {
        $user_id = Auth::user()->id;       
      } else {
        $user_id = null;
      }

      $this->data['get_winner_bidder'] = AuctionBidders::where('ads_id','=',$id)->where('bidder_status','=','1')->first();
       if ($this->data['getads_info']->status == '3') {
            if ($this->data['getads_info']->user_id != $user_id) {
                if ($this->data['get_winner_bidder']->user_id != $user_id) {
                   return Redirect::to('/');
                }
            }
       }
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s'); 
      $this->data['ads']          = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories']   = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();  
      $this->data['countries']    = Country::all();
      $this->data['usertypes']    = Usertypes::all();

      return view('ads.view', $this->data);
  }

 
public function doFetchInfoPublicAds($id){

  //get user
      $row = Advertisement::find($id);
      $get_all_rate = AdRating::where('ad_id','=',$id)->avg('rate');
      if(!is_null($get_all_rate)){
        $get_average_rate = $get_all_rate;
      }else{
        $get_average_rate = 0;
      }

      $user_id = $row->user_id;

       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                              
      $getads_info = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.minimum_allowed_bid','advertisements.status','advertisements.contact_info', 'advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.website', 'users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $id);                                       
                                        })->first(); 
      $primary_photos = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.ads_id', '=', $id) 
                                                  ->where('advertisements_photo.primary', '=', 1);                                
                                        })->first(); 

      $ads_photos = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.ads_id', '=', $id) 
                                                  ->where('advertisements_photo.primary', '=', 0)                                
                                            ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->get(); 

      $thumbnails = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->orderBy('advertisements_photo.id', 'asc')->take(10)->get(); 

      $thumbnails_set2 = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->orderBy('advertisements_photo.id', 'asc')->skip(4)->take(4)->get(); 

      $thumbnails_set3 = AdvertisementPhoto::where(function($query)  use ($id){
                                            $query->where('advertisements_photo.status', '=', '1')
                                                  ->where('advertisements_photo.ads_id', '=', $id)                                
                                                  ->where('advertisements_photo.photo', '!=', 'null');                                
                                        })->skip(8)->take(4)->get(); 

      $ads_id = $getads_info->id;
      $vendor_ads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($user_id, $ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.id', '!=', $ads_id)
                                                   ->where('advertisements.user_id', '=', $user_id);                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(3)->get(); 

        $featured_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();
   
        $related_popups = PromotionPopups::where('cat_id','=',$row->category_id)->first();
        $modal_related_popups = '';
        if(!is_null($related_popups)){
           $modal_related_popups .= '<div class="modal" id="modal-pop_up_message" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
                      <div class="modal-dialog modal-md">
                        <div class="modal-content">
                            <div id="load-form" class="loading-pane hide">
                              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                            </div>
                          <div class="modal-header modal-warning">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-info-circle"></i> Great Deals</h4>
                          </div>
                          <div class="modal-body">
                             <img src='.url('uploads').'/'.$related_popups->photo.' height="100%" width="100%">
                          </div> 
                          <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                          </div>
                        </div>
                      </div>
                    </div>';
                    $modal_related_popups_duration = $related_popups->duration;
        }else{
          $modal_related_popups .= '';
          $modal_related_popups_duration = 0;
        }

        $cat_unique_id = $getads_info->cat_unique_id;
        // $sub_category_id = $getads_info->sub_category_id;
        $related_ads_mobile  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($cat_unique_id, $ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.cat_unique_id', '=', $cat_unique_id)                                     
                                                   ->where('advertisements.id', '!=', $ads_id)                                                                        
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

        $related_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ( $ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.id', '!=', $ads_id)                                                                        
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

        $related_adset2  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($ads_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.id', '!=', $ads_id)                                     
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get();

            //Check ad if already on watch list
            if(Auth::check()){
                $get_user_id = Auth::user()->id;
            } else {
                $get_user_id = null;
            }
            $watch_item  = AdvertisementWatchlists::where('user_id', '=', $get_user_id)->where('ads_id', '=', $getads_info->id)->where('status', '=', 1)->first(); 

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$id)->where('status','=',1)->get();
    
           $custom_attributes = "";
               foreach ($get_dropdown as $key => $field) {
                  $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();

                  foreach ($get_attri_info as $key => $field_attri_info) {
                     $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->where('id','=',$field->attri_value_id)->first();
                   if ($get_attri_values) {
                     $custom_attributes .=  '<div class="form-group">'.
                                               '<h5 class="col-sm-3 normalText" for="title">'.$field_attri_info->name.'</h5>'.
                                              '<div class="col-sm-9">'.
                                                '<input type="text" name="title" class="borderZero form-control inputBox" id="ads-title" maxlength="30" value="'.$get_attri_values->name.'" disabled>'.
                                              '</div>'.
                                                '</div>';   
                   }
                                     
                }           
              }
  
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$id)->where('status','=',1)->get();
           if ($get_textbox) {
              foreach ($get_textbox as $key => $field) {
                $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
                 foreach ($get_attri_info as $key => $field_attri_info) {
                 $custom_attributes .=  '<div class="form-group">'.
                                               '<h5 class="col-sm-3 normalText" for="title">'.$field_attri_info->name.'</h5>'.
                                              '<div class="col-sm-9">'.
                                                '<input type="text" name="title" class="borderZero form-control inputBox" id="ads-title" maxlength="30" value="'.$field->attri_value.'" disabled>'.
                                              '</div>'.
                                         '</div>';          
                 }           
               }
           }
           $form="";

      if(Auth::check()){
          $user_ad_id = Auth::user()->id;
          $check_rates = AdRating::where('ad_id','=',$ads_id)->where('user_id','=',$user_ad_id)->first();
          $check_ad = Advertisement::where('user_id','=',$user_ad_id)->Where('id','=',$id)->first();
            $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body noPadding" id="notice-pane">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.url('uploads').'/'.Auth::user()->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'ad/comment/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class="noMargin"><textarea class="form-control borderZero fullSize" rows="2" id="user-ad-comment" name="comment"></textarea></p>'.
                              '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                              '<input type="hidden" name="vendor_id" value="'.Auth::user()->id.'">'.
                          '</div>'.
                          '<span class="pull-right topPadding" id="review-product-pane">'.
                            '<input type="hidden" name="ad_id" value="'.$ads_id.'">'.
                            '<span id="rateYo" class="mediumText adRating pull-left '.( Auth::user()->id  == $id ? "hide" : "").''.( !is_null($check_ad) ? 'hide' : '').' '.( !is_null($check_rates) ? 'hide' : '').'"></span>'.
                            '<button type="button" class="btn btn-sm rateButton borderZero btn-rate_product '.( Auth::user()->id  == $id ? 'hide' : '').' '.( !is_null($check_ad) ? 'hide' : '').' '.( !is_null($check_rates) ? 'hide' : '').'"><i class="fa fa-star"></i> Rate Product</button>'.
                            '<button type="submit" class="btn btn-sm blueButton borderZero btn-save_comment leftMarginB"><i class="fa fa-comments"></i> Leave Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
      }else{
          $user_ad_id = null;

          $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body noPadding">'.
                   '<p><div class="alert alert-info borderZero">You must be logged in to post a comment. <a class="btn btn-info borderZero" data-toggle="modal" data-target="#modal-login">Login</a> , <a class="btn btn-info borderZero" data-toggle="modal" data-target="#modal-register">Register</a> or <a class="btn btn-social btn-facebook borderZero"  href="#"><i class="fa fa-facebook"></i> Login with Facebook</a></div>
                   </p>'.
                   '</div>'.
                   '</div>'.
                   '</div>';
      }

        $stars = "";
        $get_comments = UserAdComments::with('getUserAdCommentReplies')->where('user_ad_comments.ad_id','=',$id)->orderBy('user_ad_comments.created_at','desc')->get();

           $comments = "";
        $ctr=1;
        foreach ($get_comments as $key => $comment_info) {
                  $get_vendor_info = User::find($comment_info->vendor_id);
                   $get_ad_rating = AdRating::where('user_id','=',$comment_info->vendor_id)->where('ad_id','=',$ads_id)->first();
                        if(!is_null($get_ad_rating)){
                            if($get_ad_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars">
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                      </span>';
                            }

                            if($get_ad_rating->rate == 4){
                              $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';   
                            }

                            if($get_ad_rating->rate == 3){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_ad_rating->rate == 2){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-sta rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_ad_rating->rate == 1){
                             $stars = '<span class="mediumText ">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                          }else{
                            $stars = "";
                          }
          $comments .= '<div id="reply-pane" class="'.($ctr % 2  == 0 ? 'panel panel-default bgGray borderZero borderBottom removeBorder' : 'panel panel-default bgWhite borderZero removeBorder').' ">'.
              '<div class="panel-body nobottomPadding" id="ad-replies-container">'.
                '<div class="commentBlock">'.
                  '<div class="commenterImage">'.
                     '<img src="'.url('uploads').'/'.$get_vendor_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                    '</div>'.
                    '<div class="commentText">'.
                    '<div class="panelTitle bottomPadding">'.$get_vendor_info->name.' '.$stars.'</div>'.
                    '<p class="normalText">'.$comment_info->comment.'</p>'.
                    '<div class="subText">'.
                    '<a '.($get_vendor_info->id == $user_ad_id ? '': 'data-name='.$get_vendor_info->name ).' id="'.($user_ad_id == null ? 'button-reply':'show').'" role="button" class="blueText normalText '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-comments"></i> Reply</a>'.
                      '<span class="leftMarginB redText normalText '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>'.
                      //'<span class="leftMarginB redText normalText '.($user_ad_id == null ? 'hide':'').'"></span>'.
                      '<span class="lightgrayText  topPadding normalText pull-right">'.date("F j, Y", strtotime($comment_info->created_at)).'</span>'.
                    '</div>';
                

        if(count($comment_info->getUserAdCommentReplies) > 0){
           foreach ($comment_info->getUserAdCommentReplies as $key => $reply_info) {

                         $get_user_info = User::find($reply_info->user_id);

        $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' borderZero removeBorder replyBlock bordertopLight nobottomMargin blockTop">
                      <div class="panel-body norightPadding nobottomPadding">
                        <div class="commentBlock">
                          <div class="commenterImage">
                             <img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                            </div>
                            <div class="commentText">
                            <div class="panelTitle bottomPadding">'.$get_user_info->name.'</div>
                            <p class="normalText">'.$reply_info->content.'</p>
                            <div class="subText">';

      $comments .= '<a'.($get_user_info->id == $user_ad_id ? '': 'data-name='.$get_user_info->name ).' id="'.($user_ad_id == null ? 'button-reply' : 'show_reply_box').'" role="button" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>
                              <span class="leftMarginB redText normalText '.($reply_info->user_id == $user_ad_id ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>
                              <span class="lightgrayText  topPadding normalText pull-right">'.date("F j, Y", strtotime($reply_info->created_at)).'</span>
                            </div>';


       $comments .= '<div class="panel panel-default borderZero removeBorder replyBlock bordertopLight blockTop inputReplyBox hidden">'.
                                  '<div class="panel-body norightPadding nobottomPadding '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').'">'.
                                    '<div class="commentBlock">'.
                                    Form::open(array('url' => 'ad/comment/reply/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'form-sub-reply')).
                                    '<textarea class="form-control borderZero fullSize" rows="2" id="reply-message" name="reply_content"></textarea>'.
                                    '<input type="hidden" name="comment_id" value="'.$comment_info->id.'">'.
                                    '<input type="hidden" name="user_id" value="'.$user_ad_id.'">'.
                                    '</div>'.
                                    '<span class="pull-right">'.
                                      '<button class="btn normalText blueButton submitReply borderZero topMargin"> Submit</button>'.
                                    '</span>'.
                                    Form::close().
                                  '</div>'.
                              '</div>                       
                            </div>
                        </div>
                      </div>
                  </div>';                
                 
              }
              
            }

           $comments .= '<div class="panel panel-default borderZero removeBorder replyBlock bordertopLight blockTop inputReply hidden">'.
                  '<div class="panel-body norightPadding '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomPadding '.($comment_info->vendor_id == $user_ad_id ? 'hide':'').'">'.
                    '<div class="commentBlock">'.
                    Form::open(array('url' => 'ad/comment/reply/save', 'role' => 'form', 'class' => 'form-horizontal','id'=>'form-main-reply')).
                    '<textarea class="form-control borderZero fullSize" rows="2" id="main_comment_reply" name="reply_content"></textarea>'.
                    '<input type="hidden" name="comment_id" value="'.$comment_info->id.'">'.
                    '<input type="hidden" name="user_id" value="'.$user_ad_id.'">'.
                    '</div>'.
                    '<span class="pull-right">'.
                      '<button class="btn normalText blueButton submitReply borderZero topMargin parentSubmit"> Submit</button>'.
                    '</span>'.
                    Form::close().
                  '</div>'.
              '</div>'.
                '</div>'.
            '</div>'.
          '</div>'.
        '</div>';
        $ctr++;
         }      

    $get_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.points')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->take(3)->get();

    $get_all_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.points', 'auction_bidders.created_at')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->get();

       if (Request::ajax()) {
          $result['getads_info']         = $getads_info->toArray();
          $result['ads_photos']          = $ads_photos->toArray();
          $result['primary_photos']      = $primary_photos->toArray();
          $result['thumbnails']          = $thumbnails->toArray();
          $result['thumbnails_set2']     = $thumbnails_set2->toArray();
          $result['thumbnails_set3']     = $thumbnails_set3->toArray();
          $result['vendor_ads']          = $vendor_ads->toArray();
          $result['related_ads_mobile']  = $related_ads_mobile->toArray();
          $result['related_ads']         = $related_ads->toArray();
          $result['related_adset2']      = $related_adset2->toArray();
          $result['featured_ads']        = $featured_ads->toArray();
          // $result['watch_item']        = $watch_item->toArray();
         
          return Response::json($result);
       } else {
          $result['getads_info']         = $getads_info;
          $result['ads_photos']          = $ads_photos;
          $result['primary_photos']      = $primary_photos;
          $result['thumbnails']          = $thumbnails;
          $result['thumbnails_set2']     = $thumbnails_set2;
          $result['thumbnails_set3']     = $thumbnails_set3;
          $result['vendor_ads']          = $vendor_ads;
          $result['featured_ads']        = $featured_ads;
          $result['related_ads_mobile']  = $related_ads_mobile;
          $result['related_ads']         = $related_ads;
          $result['related_adset2']      = $related_adset2;
          $result['custom_attributes']   = $custom_attributes;
          $result['watch_item']          = $watch_item;
          $result['modal_related_popups']         = $modal_related_popups;
          $result['modal_related_popups_duration']         = $modal_related_popups_duration;        
          //$result['get_related_banners'] =  $get_related_banners;
          $result['form']   = $form;
          $result['comments']   = $comments;
          $result['user_id']   = $user_id;
          $result['get_average_rate'] = $get_average_rate;
          $result['get_bidders'] = $get_bidders;
          $result['get_all_bidders'] = $get_all_bidders;
          return $result;
      }                                      

}

 public function removeAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
 // public function changePopupStatus(){
 //     $row = PromotionPopupStatus::where('popup_id','=',Request::input('popup_id'))->where('user_id','=',Request::input('user_id'))->first();
 //    if(is_null($row)){
 //       $row = new PromotionPopupStatus;
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = Request::input('popup_status');
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }
 //    else if($row->status == 1){
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = 2;
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }
 //    else if($row->status == 2){
 //       $row->popup_id = Request::input('popup_id');
 //       $row->user_id = Request::input('user_id');
 //       $row->status = 1;
 //        $row->save();
 //      return Response::json(['body' => 'Popup Enabled']);  
 //    }

 // }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

 public function disableAds() {
      $id = Request::input('id');

        if(Request::input('id')) {
            $row = Advertisement::find($id);
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            $new = false;
        }
        if (Auth::check()) {
           $user_id = Auth::user()->id;
              if ($user_id != $row->user_id) {
               return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);      
              }
        } 
     if ($row->ads_type_id == '2') {
         $current_time = Carbon::now()->format('Y-m-d H:i:s');    
         $date = date_create($current_time);               
         $sub_ad_expiration = date_sub($date, date_interval_create_from_date_string('5 mins'));
         $row->ad_expiration = $sub_ad_expiration;
         $row->status = "3";
         $row->save();
         $get_bidder = AuctionBidders::where('ads_id','=',$id)->orderBy('created_at', 'desc')->first();
         
         if (!(is_null($get_bidder))) {
         $get_bidder->bidder_status = '1';
         $get_bidder->save();


          //bidder message  
          // $msgheader = new UserMessagesHeader;
          // $msgheader->title       ='[System Message]';
          // $msgheader->sender_id   = 2;
          // $msgheader->read_status = 1;
          // $msgheader->reciever_id = $get_bidder->user_id;
          // $msgheader->save();

          // $msg = new UserMessages;
          // $msg->message_id  = $msgheader->id;
          // $msg->message     = 'You have won the bidding on the ad '.$row->title.'. <a href=' .url('/').'/ads/view/'.$row->id.'>Click here</a> to see the Ad';
          // $msg->sender_id   = 2;
          // $msg->save(); 


          //notify bidder on bid won  
          $notiheader = new UserNotificationsHeader;
          $notiheader->title       ='[System Message]';
          $notiheader->read_status = 1;
          $notiheader->reciever_id = $get_bidder->user_id;
          $notiheader->save();

          $noti = new UserNotifications;
          $noti->notification_id  = $notiheader->id;
          $noti->message     = 'You have won the bidding on the ad '.$row->title.'. <a href=' .url('/').'/ads/view/'.$row->id.'>Click here</a> to see the Ad';
          $noti->save(); 
          
        $bidder_info = User::find($get_bidder->user_id);

        } else { 
          $get_bidder_id = null;

        }
          //auction owner message
          $msgheaderowner = new UserMessagesHeader;
          $msgheaderowner->title       ='[System Message]';
          $msgheaderowner->sender_id   = 2;
          $msgheaderowner->read_status = 1;
          $msgheaderowner->reciever_id = $row->user_id;
          $msgheaderowner->save();

          $msgowner = new UserMessages;
          $msgowner->message_id  = $msgheaderowner->id;
          $msgowner->message     = 'Your '.$row->title.' Auction has been ended. '.($get_bidder != null?'<a href=' .url('/').'/user/vendor/'.$bidder_info->id.'/'.$bidder_info->username.'>'.$bidder_info->name.'</a> won the bidding!':'No Bidded to your Ad');
          $msgowner->sender_id   = 2;
          $msgowner->save(); 
     } else {  
         $row->status = "3";
         $row->save();    
    }

        return Response::json(['body' => 'Ad Successfully Disabled']);      

  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

 public function enableAds() {
      $row = Advertisement::find(Request::input('id'));
      //dd($row);
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Enabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function editor()
    {
       return View::make('editor.index');
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function sub2editor()
    {
       return View::make('editor.sub1.sub2.index');
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function formeditor()
    {
       return View::make('admin.ads-management.front-end-users.form');
    }
    
     /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function getCustomAttributes(){
       $cat_id = Request::input('id');
       // $id = explode(" ",$cat_id);
       // $get_cust_id = CustomAttributes::find($id[0]);
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$cat_id)->get();
                     $rows = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'">
                      <option value="" class="hide">Selefgfafd</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9">'; 
                      $rows .= '<input type="text" name="text_field[]" class="form-control borderZero">';
                      $rows .='</div></div>';
                      $rows .='<input type="hidden" name="text_field_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .='</select></div>';
                      $rows .='<input type="hidden" name="dropdown_field_id[]" value="'.$field->id.'"></div>';
                 }           
          }
      return Response::json($rows);
    }
    
    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */    

  // public function getAuctionTypes(){
  //     $ads_type_id = Request::input('id');

  //     $rows = '';
  //     if ($ads_type_id == 2) {

  //      $rows .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
  //                   '<div class="form-group">'.
  //                     '<h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>'.
  //                        '<div class="col-sm-9">'.
  //                          '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>'.
  //                       '<div class="col-sm-9">'.
  //                             '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>'.
  //                       '<div class="col-sm-9">'.
  //                         '<input type="number" class="form-control borderZero" id="register4-email" name="minimum_allowed_amount" placeholder="" required="required">'.
  //                       '</div>'.
  //                     '</div>'.
  //                     '<div class="form-group">'.
  //                        '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
  //                       '<div class="col-sm-9">'.
  //                         '<div class="col-sm-6 noleftPadding">'.
  //                         '<div class="form-group">'.
  //                           '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
  //                           '<div class="col-sm-8">'.
  //                             '<input type="number" class="form-control borderZero" id="ads-bid_duration_start" name="bid_duration_start" placeholder="" required="required">'.
  //                             '</div>'.
  //                           '</div>'.
  //                         '</div>'.
  //                         '<div class="col-sm-6 norightPadding">'.
  //                           '<div class="form-group">'.
  //                             '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
  //                             '<div class="col-sm-8"> '.        
                  
  //                          '<select id="account-company_city" name="bid_duration_dropdown_id" class="form-control borderZero inputBox fullSize" required="required">' .
  //                   '<option class="hide">Select</option>';

  //                   $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'desc')->get(); 
  //                   // dd($get_bid_duration_dropwdown);
  //                   foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
  //                             $rows .= '<option value="' . $bid_field->id . '" ' . ($bid_field->id ? 'selected' : '') .'>'. $bid_field->value . '</option>';
  //                            }

  //                $rows .= '</select></div>'.
  //                           '</div>
  //                         </div>
  //                       </div>
  //                     </div>';
  //     }
          
  //     return Response::json($rows);
  //   }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function rateAd(){
    $row = AdRating::where('ad_id','=',Request::input('id'))->where('user_id','=',Auth::user()->id)->first();
 
      if(is_null($row)){
        $row = new AdRating;
        $row->rate = Request::input('rate');
        $row->ad_id = Request::input('id');
        $row->user_id = Auth::user()->id;
        $row->save();

        $get_rater = User::where('id', '=', $row->user_id)->first();
        $rater     = $get_rater->name;
        $ads_id    = Request::input('id');
        $get_ad    = Advertisement::join('users','users.id', '=', 'advertisements.user_id')
                              ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description',  
                                'users.name', 'users.email')                          
                              ->where(function($query) use ($ads_id) {
                                      $query->where('advertisements.id', '=', $ads_id)                                                                        
                                            ->where('advertisements.status', '=', '1');                                     
                              })->first();
        $name = $get_ad->name;

$get_user_info = User::find($get_ad->user_id);

//Email Trigger review ads
             if (Auth::user()->name) {
                 $name = Auth::user()->name;
             }  else {
              $name = null;
             }

         //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
               if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               }  
              $item_title = $row->title;
              $item_url = 'http://www.yakolak.com/ads/view/'. $row->ad_id;
              $item_link = 'http://www.yakolak.com/user/vendor/'.Auth::user()->id;
              $comment = 'http://www.yakolak.com/ads/view/'. $row->ad_id;
              $rate = $row->rate;

if ($get_ad->ads_type_id == '1') {
         //email user basic ad rate
          $newsletter = Newsletters::where('id', '=', '4')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.ad_review', ['rate' => $rate,'contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $get_user_info) {
                                    $message->to($get_user_info->email)->subject(''.$newsletter->subject.'');
                                });
                     }
}else {
         //email user auction rate
          $newsletter = Newsletters::where('id', '=', '7')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.ad_review', ['rate' => $rate, 'contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $get_user_info) {
                                    $message->to($get_user_info->email)->subject(''.$newsletter->subject.'');
                                });
                     }

}

        return Response::json(['body' => 'Rate has been saved']);      
      }else{
        return Response::json(['error' => ['error']]); 
      }

    }
 
     /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function saveReview(){

      $row = Advertisement::find(Request::input('id'));
      //dd($row);
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Rate has been saved']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function saveAdComment(){

      $input = Input::all();
      $row = new UserAdComments;
      $row->user_id   = array_get($input, 'user_id');
      $row->ad_id     = array_get($input, 'ad_id');
      $row->vendor_id = array_get($input, 'vendor_id');
      $row->comment   = array_get($input, 'comment');
      $row->save();

      return Response::json(['body' => 'posted']);      

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

     public function saveAdCommentReply(){
      $input = Input::all();
      $row = new UserAdCommentReplies;
      $row->comment_id     = array_get($input, 'comment_id');
      $row->user_id     = array_get($input, 'user_id');
      $row->content     = array_get($input, 'reply_content');
      $row->save();
      return Response::json(['body' => 'replied']);      

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function addWatchlist() {
     $row = AdvertisementWatchlists::find(Request::input('id'));
     $id = Request::input('id');
      if(is_null($row)){
        $row = new AdvertisementWatchlists;
        $row->user_id = Auth::user()->id;
        $row->ads_id = $id;
        $row->save();
        return Response::json(['body' => 'Ad Successfully added to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function removeWatchlist() {
     $row = AdvertisementWatchlists::find(Request::input('id'));

      if(!(is_null($row))){
        $row->status = 2;
        $row->save();
        return Response::json(['body' => 'Ad Successfully removed to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function auctionExpired() {
    $input = Input::all();
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','2')->where('status','=', '1')->get();

foreach ($row as $value) {
     if($value->ad_expiration <= $current_time){
        $value->status = 3;
        $value->ad_exp_email_trigger = 0;
        $value->save();

    $get_bidder = AuctionBidders::where('ads_id','=',$value->id)->orderBy('created_at', 'desc')->first();
        if (!(is_null($get_bidder))) {
         $get_bidder->bidder_status = '1';
         $get_bidder->save();

          //bidder message  
          $msgheader = new UserMessagesHeader;
          $msgheader->title       ='[System Message]';
          $msgheader->sender_id   = 2;
          $msgheader->read_status = 1;
          $msgheader->reciever_id = $get_bidder->user_id;
          $msgheader->save();

          $msg = new UserMessages;
          $msg->message_id  = $msgheader->id;
          $msg->message     = 'You have won the bidding on the ad '.$row->title.'. <a href=' .url('/').'/ads/view/'.$row->id.'>Click here</a> to see the Ad';
          $msg->sender_id   = 2;
          $msg->save(); 

          //notify bidder on bid won  
          $notiheader = new UserNotificationsHeader;
          $notiheader->title       ='[System Message]';
          $notiheader->read_status = 1;
          $notiheader->reciever_id = $get_bidder->user_id;
          $notiheader->save();

          $noti = new UserNotifications;
          $noti->notification_id  = $notiheader->id;
          $noti->message     = 'You have won the bidding on the ad '.$row->title.'. <a href=' .url('/').'/ads/view/'.$row->id.'>Click here</a> to see the Ad';
          $noti->save(); 

          $bidder_info = User::find($get_bidder->user_id);
        } else { 
          $get_bidder_id = null;

        }

          //auction owner message
          $msgheaderowner = new UserMessagesHeader;
          $msgheaderowner->title       ='[System Message]';
          $msgheaderowner->sender_id   = 2;
          $msgheaderowner->read_status = 1;
          $msgheaderowner->reciever_id = $row->user_id;
          $msgheaderowner->save();

          $msgowner = new UserMessages;
          $msgowner->message_id  = $msgheaderowner->id;
          $msgowner->message     = 'Your '.$row->title.' Auction has been ended. '.($get_bidder != null?'<a href=' .url('/').'/user/vendor/'.$bidder_info->id.'/'.$bidder_info->username.'>'.$bidder_info->name.'</a> won the bidding!':'No Bidded to your Ad');
          $msgowner->sender_id   = 2;
          $msgowner->save(); 

      $name = $user->name;

              $contact_name = $user->name; 
              $user_name    = $user->username;
              $user_email   = $user->email;
              if ($user->telephone) {
                  $user_phone = $user->telephone;
              } else {
                  $user_phone = $user->mobile;
              }  
              $item_title = $value->title;
              $item_url = url('/').'/ads/view/'. $value->id;
              $item_link = url('/').'/user/vendor/'.$value->user_id;
              $comment = url('/').'/ads/view/'. $value->id;

           $newsletter = Newsletters::where('id', '=', '9')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.auction_expire', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $user) {
                                    $message->to($user->email)->subject(''.$newsletter->subject.'');
                                });
                     }
       }

    }

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function auctionExpiring() {
    $input = Input::all();
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','2')->where('status','=', '1')->get();

    $get_ad_settings = AdManagementSettings::where('expiring_auction_noti_time_status','=','1')->where('id','=','1')->first();
    $noti_time = $get_ad_settings->expiring_auction_noti_time_start.' '.$get_ad_settings->expiring_auction_noti_time_end;

foreach ($row as  $value) {
    $date = date_create($value->ad_expiration);  
    //subtract ad_expiration to noti time settings             
    $sub_ad_expiration = date_sub($date, date_interval_create_from_date_string($noti_time));

      if ($current_time > $sub_ad_expiration->format('Y-m-d H:i:s') && $value->auction_expiring_noti_trigger == '1') {
         
         $value->auction_expiring_noti_trigger = '0';
         $value->save();

          //bidder notification  
          $msgheader = new UserNotificationsHeader;
          $msgheader->title       ='[System Message]';
          $msgheader->read_status = 1;
          $msgheader->reciever_id = $value->user_id;
          $msgheader->save();

          $msg = new UserNotifications;
          $msg->notification_id  = $msgheader->id;
          $msg->message     = 'Your Auction ad '.$value->title.' will expire within '.$noti_time.'.  <a href=' .url('/').'/ads/edit/'.$value->id.'>Click here</a> to extend the Ad';
          $msg->save(); 


          $user = User::find($value->user_id);
          $name = $user->name;

              $contact_name = $user->name; 
              $user_name    = $user->username;
              $user_email   = $user->email;
              if ($user->telephone) {
                  $user_phone = $user->telephone;
              } else {
                  $user_phone = $user->mobile;
              }  
              $item_title = $value->title;
              $item_url = url('/').'/ads/view/'. $value->id;
              $item_link = url('/').'/user/vendor/'.$value->user_id;
              $comment = url('/').'/ads/view/'. $value->id;

           $newsletter = Newsletters::where('id', '=', '8')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                                Mail::send('email.auction_expire', ['contact_name' => $contact_name,'user_name' => $user_name,'item_url' => $item_url,'user_email' => $user_email,'user_phone' => $user_phone, 'item_title' => $item_title,'item_link' => $item_link, 'comment' => $comment, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $user) {
                                    $message->to($user->email)->subject(''.$newsletter->subject.'');
                                });
                     }

      } 
}


    }

   /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function basicAdExpired() {
    $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
    $row = Advertisement::where('ads_type_id','=','1')->where('status','=', '1')->get();
foreach ($row as $value) {
     if($value->ad_expiration <= $current_time && $value->ad_expiration != '0000-00-00 00:00:00'){
        $value->status = 3;
        $value->ad_exp_email_trigger = 0;
        $value->save();
 
      //auction owner message
      $msgheaderowner = new UserMessagesHeader;
      $msgheaderowner->title       ='[System Message]';
      $msgheaderowner->sender_id   = 2;
      $msgheaderowner->read_status = 1;
      $msgheaderowner->reciever_id = $value->user_id;
      $msgheaderowner->save();
      $msgowner = new UserMessages;
      $msgowner->message_id  = $msgheaderowner->id;
      $msgowner->message     = 'Your '.$value->title.' Ad has been expired. <a href=' .url('/').'/ads/view/'.$value->id.'>Click here</a> to view the Ad! ';
      $msgowner->sender_id   = 2;
      $msgowner->save(); 
       }
    }

    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function getSubcategory(){
      $id = Request::input('id');

      $get_subcat = SubCategory::where('cat_id',$id)->get();

      $subcat="";
      if(count($get_subcat)>0){
      $subcat.="<option value='' class='hide'>Select:</option>";
        foreach ($get_subcat as $key => $field) {
        $subcat .= '<option value="'.$field->id.'">'.$field->name.'</option>';

        }
      }else{
        $subcat.="<option class='hide' disable>No Available Subcategory</option>";
      }
      return Response::json($subcat);
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

        public function postAdRedirect()
    {

    $id = Session::get('ad_redirect');
    
    return Redirect::to(url('/').'/ads/view/'.$id);
    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

  public function addBidder(){
      $ads_id = Request::input('ads_id');
      $row = Advertisement::find($ads_id);
      if(Auth::check()) {
        $user_id = Auth::user()->id; 
      } 

    $get_user = User::where('id','=',$user_id)->first();
    $user_points = $get_user->points;
    if ($row->user_id != $user_id) {
          $total_points = $user_points - $row->minimum_allowed_bid;
          if (!($total_points < 0)) {
          $get_user->points = $total_points;  
          $get_user->save();

          $get_ad_settings = AdManagementSettings::where('bid_inc_value_status','=','1')->where('id','=','1')->first();
          $get_latest_bidder = AuctionBidders::where('status','=','1')->where('ads_id','=',$ads_id)->orderBy('created_at', 'desc')->first();
            if (is_null($get_latest_bidder)) {
              $latest_bidder = null;
              $total_points  = $row->minimum_allowed_bid + $get_ad_settings->bid_inc_value;
            } else {
              $total_points  = $get_latest_bidder->points + $get_ad_settings->bid_inc_value;
              $latest_bidder = $get_latest_bidder->user_id;
            }
          if ($latest_bidder != $user_id) {
                if(!(is_null($row)) && $row->status == "1" && $row->user_id != $user_id){
                   $bidder = new AuctionBidders;
                   $bidder->ads_id  = $row->id;
                   $bidder->user_id = $user_id;
                   $bidder->status  = '1';
                   $bidder->points  = $total_points;
                   $bidder->save();

                //compute for bid limit notify vendor
                $get_bidders = AuctionBidders::where('ads_id','=',$ads_id)->orderBy('id', 'desc')->first();
                $get_bid_limit_amount_status = AuctionBidders::where('ads_id','=',$ads_id)->where('bid_limit_amount_status','=',1)->get();
                // foreach ($get_bidders as $value) {
                  if ($total_points >= $row->bid_limit_amount) {
    
                     if (count($get_bid_limit_amount_status) == 0) {
                        $get_bidders->bid_limit_amount_status = '1';
                        $get_bidders->save();
                          //notify vendor
                          $bidder_info = User::find($get_bidders->user_id);

                          // $msgheaderowner = new UserMessagesHeader;
                          // $msgheaderowner->title       ='[System Message]';
                          // $msgheaderowner->sender_id   = 2;
                          // $msgheaderowner->read_status = 1;
                          // $msgheaderowner->reciever_id = $row->user_id;
                          // $msgheaderowner->save();

                          // $msgowner = new UserMessages;
                          // $msgowner->message_id  = $msgheaderowner->id;
                          // $msgowner->message     = '<a href=' .url('/').'/user/vendor/'.$bidder_info->id.'/'.$bidder_info->username.'>'.$bidder_info->name.'</a> reached the bid limit amount of '.$row->bid_limit_amount.' points on your <a href=' .url('/').'/ads/view/'.$row->id.'>'.$row->title.'</a>.';
                          // $msgowner->sender_id   = 2;
                          // $msgowner->save(); 

                          //notify vendor on bid limit reached  
                          $notiheader = new UserNotificationsHeader;
                          $notiheader->title       ='[System Message]';
                          $notiheader->read_status = 1;
                          $notiheader->reciever_id = $row->user_id;
                          $notiheader->save();

                          $noti = new UserNotifications;
                          $noti->notification_id  = $notiheader->id;
                          $noti->message     = '<a href=' .url('/').'/user/vendor/'.$bidder_info->id.'/'.$bidder_info->username.'>'.$bidder_info->name.'</a> reached the bid limit amount of '.$row->bid_limit_amount.' points on your <a href=' .url('/').'/ads/view/'.$row->id.'>'.$row->title.'</a>.';
                          $noti->save(); 
                      }
                    }         
                // }


                //add time on ad expiration 
                if ($get_ad_settings->bid_adding_time_status == '1') {
                    if ($get_ad_settings->bid_adding_time_end == 'minutes') {
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." minutes"));                 
              
                    } elseif ($get_ad_settings->bid_adding_time_end == 'hours'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." hours"));                 
                 
                    } elseif ($get_ad_settings->bid_adding_time_end == 'days'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." days"));                 
                      
                    } elseif ($get_ad_settings->bid_adding_time_end == 'months'){
                        $date = date_create($row->ad_expiration);
                        $expiration_date = date_add($date,date_interval_create_from_date_string($get_ad_settings->bid_adding_time_start." months"));                 
                    }
                    $row->ad_expiration = $expiration_date;
                    $row->save();
                }                
                    
                  return Response::json(['body'  => 'Bid added']);      
                }else{
                  return Response::json(['error' => ['error on adding bid']]); 
               }
          } else {
                  return Response::json(['errormsg' => 'Adding bid failed, your the latest bidder']); 
          }
    
      } else {
            return Response::json(['errormsgpoints' => 'Your points is not enough to bid']); 
      } 
    } else {
            return Response::json(['errormsgowner' => 'Error adding bid! Your the ownder of this Ad']); 
    }
 


    }

    /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

   public function refreshBidders(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
     $get_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.points')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->take(3)->get();

     $getads_info = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.website', 'users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $ads_id);                                       
                                        })->first(); 
                                     
        $rows = '';
        $rows .= '<div id="load-form_bidder" class="loading-pane hide">
                          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div></div>
                            <div class="panel panel-default bottomMarginLight removeBorder borderZero bgGray">';
               $i = 1;
          foreach ($get_bidders as $bidders) {
                if ($i == '1') {
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                    <div class="mediumText grayText"><strong><span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.
                        '</div>'.'<img src="'.url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'.url('/user/vendor ') . '/' . $getads_info->user_id .'/'. $bidders->name.'">'.$bidders->name.'</a> </span><span class="noPadding pull-right">'.number_format($bidders->points).' pts.</span>
                          </strong>
                        </div>
                      </div>';
                        if($getads_info->ad_expiration <= $current_time) {
                          if($getads_info->ads_type_id == '2' && $getads_info->status == '3'){
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                              <div class="blueText mediumText noPadding noMargin" style="text-align: center;">Bid Winner</div>
                            </div>';  
                            }
                        }
                } else {
                      if($getads_info->status != '3') {
        $rows .='<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                        <div class="normalText '.($getads_info->status == 3?'lightgrayText':'grayText').'">
                            <span class="col-lg-1 noPadding"><div style="display:none;">'.$i++.
                            '</div> <img src="'. url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText '.($getads_info->status == 3?'lightgrayText':'grayText').'" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="noPadding pull-right">'.number_format($bidders->points).' pts.</span>
                        </div></div>';
                      }
                }
          }
             
        $rows .='<a href="#" class="news-expand pull-left normalText" data-toggle="modal" data-target="#modal-list-bidders" aria-controls="rmjs-1">view&nbsp;more</a>';
                      
      
        return Response::json($rows);
    }
      /** Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

   public function refreshBidderList(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
       $get_all_bidders = AuctionBidders::join('users', 'auction_bidders.user_id', '=', 'users.id')
                                 ->select('users.photo', 'users.username', 'users.name', 'auction_bidders.points', 'auction_bidders.created_at')
                                 ->where(function($query) use ( $ads_id) {
                                             $query->where('auction_bidders.ads_id', '=', $ads_id)
                                                   ->where('auction_bidders.status', '=', '1');                                     
                                        })->orderBy('auction_bidders.created_at', 'desc')->get();
      $getads_info = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->select('advertisements.status','advertisements.contact_info', 'advertisements.ad_expiration', 'advertisements.ads_type_id', 'advertisements.title', 'advertisements.user_id as user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'users.email', 'users.mobile', 'users.address', 'users.telephone', 'users.website', 'users.office_hours', 'users.facebook_account', 'users.twitter_account', 'users.instagram_account', 'users.photo as user_photo',
                                          'advertisements.youtube')                          
                                      ->where(function($query) use ($ads_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.id', '=', $ads_id);                                       
                                        })->first(); 
      $i = 1;
      $rows ='';
      $rows .='<div class="modal fade" id="modal-list-bidders" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
              <div class="modal-dialog">
                <div class="modal-content borderZero">
                    <div id="load-form_bidder" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div></div>
                  <div class="modal-header modal-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-money"></i> Bidders</h4>
                  </div>';
          foreach ($get_all_bidders as $bidders) {
      $rows .='<div class="modal-body">
              <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
                <div class="mediumText grayText">';
                  if ($i == '1') {
      $rows .='<strong><span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.'</div><img src="'.url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="25" height="25"/> </span><span class="col-lg-5 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="col-lg-3 noPadding">'.number_format($bidders->points).' points</span> <span class="pull-right"><time class="timeago" datetime="'.$bidders->created_at.'" title=""></time></span></strong>';
               }else {
      $rows .='<span class="col-lg-1 noPadding"> <div style="display:none;">'.$i++.'</div><img src="'. url('/').'/uploads/'.$bidders->photo.'"  class="img-circle noPadding" alt="Cinque Terre" width="25" height="25"/> </span><span class="col-lg-5 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="'. url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name .'">'.$bidders->name.'</a> </span><span class="col-lg-3 noPadding">'.number_format($bidders->points).' points</span> <span class="pull-right"><time class="timeago" datetime="'.$bidders->created_at.'" title=""></time></span>';
                  }
      $rows .='</div>
              </div>
            </div>';
         }
      $rows .='<div class="modal-body"></div>
                <div class="modal-footer">
                  <input type="hidden" name="ads_id" id="row-ads_id" value="">
                  <button type="button" class="btn redButton borderZero" data-dismiss="modal"> Close</button>
                </div>
              </div>
            </div>
          </div>';               
      return Response::json($rows);
    }

}