<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\CustomAttributes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;


class CategoryController extends Controller
{

   public function edit(){

   	$row = Category::find(Request::input('id'));

    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }

   public function subEdit(){

    $row = SubCategory::find(Request::input('id'));

    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }



   public function saveSubCategory(){

      $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {

            // get the user info
            $row = SubCategory::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
  
            'cat_id' => 'required|min:2|max:100',
            'description' => 'required',
        ];
        // field name overrides
        $names = [
            'cat_id' => 'category name',
            'description' => 'category description',
        ];
// do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
          $row = new SubCategory;
          $row->cat_id     = array_get($input, 'cat_id');
          $row->description = array_get($input, 'description');
        } else {
          $row->cat_id     = array_get($input, 'cat_id');
          $row->description = array_get($input, 'description');
          $row->save();
        }   
        $row->save();

      return Response::json(['body' => 'Sub Category created successfully.']);
   }
   public function save(){

		$new = true;

        $input = Input::all();
        // dd($input);

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
           // array_get($input, 'cat_id')
           if(array_get($input, 'cat_id') !="parent"){
              // dd(array_get($input, 'cat_id'));
               $row = SubCategory::find(array_get($input, 'id'));
              if(is_null($row)) {
                  return Response::json(['error' => "The requested item was not found in the database."]);
              }
              // this is an existing row
              $new = false;
            }
           if(array_get($input, 'cat_id') == "parent"){
              $row = Category::find(array_get($input, 'id'));
              if(is_null($row)) {
                  return Response::json(['error' => "The requested item was not found in the database."]);
              }
              // this is an existing row
              $new = false;
            }


            
        }

        $rules = [
  
            'description' => 'required',
            'name'=> 'required|unique:categories,name' . (!$new ? ',' . $row->id : ''),



        ];
        // field name overrides
        $names = [
            'description' => 'category description',
        ];

        // $sub_cat_array = array();
        // $sub_cat_id = Input::get('sub_cat_id');
        // $sub_cat = Input::get('sub_cat');
        // $sequence = Input::get('sequence');

        // // validate child rows
        // if($sub_cat) {
        //     foreach($sub_cat as $key => $val) {
        //         if($sub_cat[$key]) {
        //             $subrules = array(
        //                 "sub_cat.$key" => 'required',
        //             );

        //             $subfn = array(
        //                 "sub_cat.$key" => 'contact',
        //             );

        //             if($sub_cat_id[$key]) {
        //                 $subrules["sub_cat_id.$key"] = 'exists:production_case_contacts,id';
        //             }

        //             $rules = array_merge($rules, $subrules);
        //             $names = array_merge($names, $subfn);
        //             $sub_cat_array[] = $sub_cat[$key];
        //         } else {
        //             unset($sub_cat_id[$key]);
        //             unset($sub_cat[$key]);
        //         }
        //     }
        // }
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
          if(array_get($input, 'cat_id') == "parent"){

             $row = new Category;
             $row->name     =  array_get($input, 'name');
             $row->sequence     =  array_get($input, 'sequence');
             $row->biddable_status = array_get($input, 'biddable_status');
             $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
             $row->description = array_get($input, 'description');

          } 
          if(array_get($input, 'cat_id') != "parent"){

            $row = new SubCategory;
            $row->cat_id = array_get($input,'cat_id');
            $row->name= array_get($input,'name');
            $row->sequence     =  array_get($input, 'sequence');
            $row->biddable_status = array_get($input, 'biddable_status');
            $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
            $row->sequence =array_get($input,'sequence');
            $row->description = array_get($input, 'description');

          }
         
        } else {

          if(array_get($input, 'cat_id') == "parent"){
  
            $row->name  = array_get($input, 'name');
            $row->sequence     =  array_get($input, 'sequence');
            $row->description = array_get($input, 'description');
            $row->biddable_status = array_get($input, 'biddable_status');
            $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
            $row->save();

           }
           if(array_get($input, 'cat_id') != "parent"){
            // dd($row);
            $row->cat_id = array_get($input,'cat_id');
            $row->name= array_get($input,'name');
            $row->sequence     =  array_get($input, 'sequence');
            $row->biddable_status = array_get($input, 'biddable_status');
            $row->buy_and_sell_status = array_get($input, 'buy_and_sell_status');
            $row->sequence =array_get($input,'sequence');
            $row->description = array_get($input, 'description');
            $row->save();
            $row->unique_id =  base64_encode('subcat-'.$row->id);
            $row->save();

           }
        }   
		        $row->save();
            $row->unique_id = base64_encode('cat-'.$row->id);
            $row->save();
        
		  return Response::json(['body' => 'Category created successfully.']);
   }
   public function index(){

      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];

      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/category/refresh');
   	  $this->data['title'] = "Category Management";
      $this->data['categories'] = Category::all();
      $this->data['custom_attri'] = CustomAttributes::all();
      return View::make('admin.category-management.list', $this->data);
   }

  public function doList(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Category::select('categories.*')->with('subcategoryinfo')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        // dd($rows);
        // $rows_data = Category::where('status','!=','2')->get();
       
        //                         $rows = Crypt::decrypt($rows_data);
                                // dd($rows);

                          
                      
      } else {
         $count = Category::select('categories.*')->with('subcategoryinfo')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = Category::select('categories.*')->with('subcategoryinfo')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  public function checkSequenceSubCat(){

     $result['rows'] = SubCategory::where('cat_id',Request::input('id'))
                                            ->where('sequence','=',Request::input('order'))
                                            ->get();
          return Response::json($result);     
                                  
  }
   public function checkSequenceCat(){

     $result['rows'] = Category::where('sequence','=',Request::input('order'))
                                  ->get();

          return Response::json($result);     
                                  
  }
  public function getCategoryList(){

     $this->data['title'] = "Category";
    return View::make('category.list', $this->data);
  }

}