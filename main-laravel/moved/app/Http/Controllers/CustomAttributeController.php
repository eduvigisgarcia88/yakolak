<?php
namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\CustomAttributes;
use App\CustomAttributeValues;
use App\SubAttributes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;


class CustomAttributeController extends Controller
{

	public function index(){

	  $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];

      $this->data['pages'] = $result['pages'];
    
      $this->data['refresh_route'] = url('admin/category/custom-attribute/refresh');
   	  $this->data['title'] = "Custom Attributes Management";
      $this->data['category'] = Category::with('subcategoryinfo')->get();
        
   	  $this->data['custom_attribute'] = CustomAttributes::where('status','!=',2)->get();
      return View::make('admin.category-management.custom-attribute-management.list', $this->data);
	}
  public function getAttributeValues(){

      $rows = SubAttributes::with('attriValues')->get();
    
  }
	public function doList(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = Category::with('subcategoryinfo')->with('subcategoryinfo.getSubCatCustomAttributes')->with('getCustomAttributes')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
             
      } else {
         $count = Category::select('categories.*')->with('subcategoryinfo')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = Category::select('categories.*')->with('subcategoryinfo')->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
	 public function save(){

		$new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
             
               $row = SubAttributes::find(array_get($input, 'id'));

              if(is_null($row)) {
                  return Response::json(['error' => "The requested item was not found in the database."]);
              }
              // this is an existing row
              $new = false;
        }

        $rules = [
  
            'name' => 'required',

        ];
        // field name overrides
        $names = [
            'name' => 'Attributed  name',
        ];

        
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        $custom_attri = array();
        $attribute_value_id = Input::get('attribute_value_id');
        $attribute_value_dropdown = Input::get('attribute_value_dropdown');

        if($custom_attri) {
          foreach($custom_attri as $key => $val) {
              if($custom_attri[$key]) {
                  $subrules = array(
                      "attribute_value_dropdown.$key" => 'required',
                  );

                  $subfn = array(
                      "attribute_value_dropdown.$key" => 'contact',
                  );

                  if($attribute_value_id[$key]) {
                      $subrules["attribute_value_id.$key"] = 'exists:production_case_contacts,id';
                  }

                  $rules = array_merge($rules, $subrules);
                  $names = array_merge($names, $subfn);
                  $custom_attri[] = $attribute_value_dropdown[$key];
              } else {
                  unset($attribute_value_id[$key]);
                  unset($attribute_value_dropdown[$key]);
              }
          }
      }
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
             $row = new SubAttributes;
             $row->attri_id     =  array_get($input, 'attri_id');
             $row->name     =  array_get($input, 'name');
             $row->attribute_type     =  array_get($input, 'attri_type');
        } else {
             $row->attri_id     =  array_get($input, 'attri_id');
             $row->name     =  array_get($input, 'name');
             $row->attribute_type   =  array_get($input, 'attri_type');
             $row->save();
           
        }   
		         $row->save();

          if($attribute_value_dropdown) {
            foreach($attribute_value_dropdown as $key => $child) {
                if($attribute_value_id[$key]) {
                    // this is an existing visa
                    $cinfo = CustomAttributeValues::find($attribute_value_id[$key]);
                } else {
                    // this is a new visa
                    $cinfo = new CustomAttributeValues;
                    $cinfo->sub_attri_id = $row->id;
                }
                $cinfo->name = $attribute_value_dropdown[$key];
                $cinfo->save();
                // add to visa ID array
            }
          }

        
        // $attribute_values = new CustomAttributeValues;
        // $attribute_values->sub_attri_id = $row->id; 
        // $attribute_values->name = ; 
        // $attribute_values->save();
      
		    if($new){
		    	return Response::json(['body' => 'Attribute created successfully.']);
		    }else{
		    	return Response::json(['body' => 'Attribute updated successfully.']);
		    }
		  
   }
   public function manipulateMainAttri(){

   	$row = CustomAttributes::find(Request::input('id'));

    
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }

   }

}