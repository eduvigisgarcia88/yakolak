<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;


class SupportController extends Controller
{

	public function index(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.list', $this->data);
	}

	public function showContactUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.contact-us', $this->data);
	}

	public function showAboutUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.about-us', $this->data);
	}
	public function showCallUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.call-us', $this->data);
	}
	public function showFAQS(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.faqs', $this->data);
	}
	public function showAdvertising(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.advertising', $this->data);
	}
	public function showHelpUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.help-us', $this->data);
	}



}
   