<?php 
namespace App\Http\Controllers;

use Paypalpayment;


class Paypal_IPNController extends Controller {

public function __construct($mode = 'live')
    {
        if ($mode == 'live') {
            $this->_url = 'https//:www.paypal.com/cgi-bin/webscr';
        } else {
            $this->_url = 'https//:www/sandbox.paypal.com/cgi-bin/webscr';

        }
 //https:www/sandbox.paypal.com/cgi-bin/webscr
 //https:www.paypal.com/cgi-bin/webscr
        //https//:www/sandbox.paypal.com/cgi-bin/webscr?cmd=_notify-validate&param=value&param2=value2

    }

    public function run()
    {
        $postFields = 'cmd_notify-validate';
        foreach ($_POST as $key => $value) {
            $postFields .= "&$key=".urlencode($value);
        }

        $ch = curl_init();

        curl_setopt_array($ch, array(
            CURLOPT_URL => $this->_url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER =>false,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postFields
        ));
        $result = curl_exec($ch);
        curl_close($ch);
        
        $fh = fopen('result.txt', 'w');
        fwrite($fh, $result . ' -- ' . $postFields);
       
        $this->data['result'] = $result;
        
        return view('payment.index', $this->data);
    }
}