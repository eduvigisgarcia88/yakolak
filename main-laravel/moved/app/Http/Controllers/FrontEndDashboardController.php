<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Advertisement;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\Usertypes;
use App\AdvertisementPhoto;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use App\Category;

class FrontEndDashboardController extends Controller
{
  
	public function index(){

    $this->data['vendorads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                      ->select('advertisements.title', 'advertisements.category',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category', '=', 'Buy and Sell')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get();

    $this->data['categories'] = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                                     
		$this->data['title'] = "Dashboard";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    return View::make('user.dashboard.index', $this->data);
	}
	public function getMessages(){

		$this->data['title'] = "Dashboard-Messages";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
       $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                                })->count();  


    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();                                               
    return View::make('user.dashboard.messages', $this->data);
	}
	public function getBids(){

		$this->data['title'] = "Dashboard-Bids";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();                   
    return View::make('user.dashboard.bids', $this->data);
	}
	public function getListings(){


    $this->data['vendorads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName')                          
                                      ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 

    $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city')                        
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
     
    $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

    $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                      ->where(function($query) {
                                            $query->where('advertisements_photo.primary', '=', '1')
                                                 ->where('advertisements.status', '=', '1');                                
                                        })->skip(4)->take(4)->get(); 
    $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                           
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

    $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


     if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();                                      
		$this->data['title'] = "Dashboard-Listings";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    return View::make('user.dashboard.listings', $this->data);
	}
	public function getWatchlist(){

		$this->data['title'] = "Dashboard-WatchList";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    return View::make('user.dashboard.watchlist', $this->data);
	}
	public function getSetttings(){

		$this->data['title'] = "Dashboard-Settings";
		$this->data['countries'] = Country::all();
    $this->data['usertypes'] = Usertypes::all();
    return View::make('user.dashboard.settings', $this->data);
	}
	
 } 