<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\Advertisement;
use App\AdvertisementPhoto;
use App\UserPlanTypes;
use App\UserPlanRates;
use App\PlanPrice;
use Carbon\Carbon;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;
use Session;


class FrontEndPlanController extends Controller  {

public function index(){
    $token = Session::token();

$this->data['title'] = "Plans";
$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
$this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
$this->data['countries'] = Country::all();
$this->data['usertypes'] = Usertypes::all();

if(Auth::check()){
     $user_id = Auth::user()->id;
}else{
     $user_id = null;
}

$this->data['messages_counter'] = UserMessagesHeader::where(function($query) use ($user_id){
                          $query->where('reciever_id','=',$user_id);
                                // ->OrWhere('sender_id','=',$user_id);
                              })->where(function($status){
                                    $status->where('read_status','=',1);
                              })->count();
$this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

 $current_time = Carbon::now()->format('Y-m-d H:i:s'); 




if(Auth::check()){
    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;

    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->get();         
    } else {

    $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '1')->where('status', '=', 1)->get();         
    $user_plan_types = null;
    }

//dd($get_plan);
$plan = "";
$plan .='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div id="section1" class="panel panel-default removeBorder borderZero nobottomMargin">
    <div class="panel-body">
    <div class="table-responsive">
    <table class="table nobottomMargin normalText">
    <thead>
      <tr>
        <th class="col-xs-3 text-center tablerightBorder"><h3 class="blueText">Upgrade your Plan Now!</th>';
        foreach ($get_plan as $value) {
            $get_plan_price = PlanPrice::where('user_plan_id','=',$value->id)->where('status','=','1')->first();
                  if ($get_plan_price) {
                     $total_price = $get_plan_price->price / $get_plan_price->term;
                    } else {
                      $total_price = '';
                    }
            if ($value->id == '1' || $value->id == '5') {
                 $plan .='<th class="col-xs-2 text-center mediumText bgGray tablerightBorder">'.$value->plan_name.'<br><h3 class="noMargin lightgrayText"><br> </h3><button class="btn btn-sm redButton fullSize borderZero noMargin '.(Auth::check() ?'':'btn-login" data-toggle="modal" data-target="#modal-login"').' type="submit">'.($value->id == '1' || $value->id == '5' ?'FREE': '').'</button></th>';
            } else {
                 $plan .='<th class="col-xs-2 text-center mediumText bgGray tablerightBorder">'.$value->plan_name.'<br><h3 class="noMargin lightgrayText">'.round($total_price, 2). '/mo*'.'</h3><button class="btn btn-sm redButton fullSize borderZero noMargin '.(Auth::check() ?'':'btn-login" data-toggle="modal" data-target="#modal-login"').' type="submit">'.($value->id == $user_plan_types ?'MY PLAN': 'DISCOUNT').'</button></th>';
            }
        }
        
$plan .='</tr>
    </thead>
    <tbody>
      <tr>
        <td class="bold blueText tablerightBorder">No. of Ads</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->ads.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">No. of Auctions</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->auction.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">No. of Image per Ad</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->img_per_ad.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">No. of Videos per Ad</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->video_total.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">Free SMS Notification</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->sms.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">No. of Bids</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->bid.'</td>';   
        }
$plan .='</tr>
        <tr>
        <td class="bold blueText tablerightBorder">Total Free Points</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->point.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td class="bold blueText tablerightBorder">Total Points Exchange</td>';
        foreach ($get_plan as $value) {
           $plan .='<td class="bold text-center bgGray tablerightBorder">'.$value->point_exchange.'</td>';   
        }
$plan .='</tr>
      <tr>
        <td></td>';
        foreach ($get_plan as $value) {
          if (Auth::check()) {
            if ($value->id=='1' || $value->id =='5') {
        $plan .='<td class="tablerightBorder bgGray"><button class="btn btn-sm blueButton fullSize borderZero noMargin" id="btn-get-started">FREE</button></td>';           
                 } else {
        $plan .='<td class="tablerightBorder bgGray">'.($user_plan_types  == $value->id ? '<span><a name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton fullSize borderZero noMargin" id="btn-get-started" href="#'.$value->plan_name.'">MY PLAN</a></span>' : '<span><a name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton fullSize borderZero noMargin" href="#'.$value->plan_name.'">GET STARTED</a></span>').'</td>'; 
                 }    
          } else {
        $plan .='<td class="tablerightBorder bgGray"><span><a class="btn btn-sm blueButton fullSize borderZero noMargin btn-login" href="#'.$value->plan_name.'">GET STARTED</a></span></td>';  
          }

        }

     
$plan .=' </tr>
    </tbody>
  </table>
  </div>
  </div>
</div> 
      <div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                     <!--  <?php echo($plan); ?> -->


        </div>
        

        
        </div>
        
        
      </div>';




//SUBSCRIPTION
$plan_description ="";
if(Auth::check()){
    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;

    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->where('status', '=', 1)->get();         
      
    $created = new Carbon(Auth::user()->created_at);
    $now = Carbon::now();
    $difference = ($created->diff($now)->days < 1)
    ? 'today'
    : $created->diffForHumans($now);
    //check if days not months
   
    $arr = explode(' ',trim($difference));
    
    if ($arr == 'today') {
      $diff = '0';
    } else {
           if ($arr == 'days') {
           $diff = '0';
           $test = 'true';
          } elseif ($arr == 'years' || $arr == 'year') {
           $diff = '12';
          }
          else {
           $diff = substr($difference, 0, 1); 
          }
    }


$get_plan_rate = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->first();

$duration = $get_plan_rate->duration;
$discount = $get_plan_rate->discount;

      } else {
        $user_id = null;
        $usertype_id = null;
        $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('plan_status', '!=', '5')->get();       
      
        $get_plan_rate = UserPlanRates::where('usertype_id', '=', 1)->where('rate_status', '=', 1)->first();
        $duration = null;
        $user_plan_types = '0';
      }
$rows = User::with("getUserCompanyBranch")->find($user_id);



//SECTION
foreach ($get_plan as  $value) {
  if(Auth::check()){
        if ($diff <= $duration || $diff == 0) {
           $inti_discount  = 100 - $discount;
           $total_discount = '.'.$inti_discount;

           $total_months12 = $value->months12_price * $total_discount;
           $total_months6  = $value->months6_price  * $total_discount;
           $test = 'test1';

} else {
   $get_plan_rate2 = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->orderBy('id', 'desc')->first();
       if ($get_plan_rate2) {
       $duration2 = $get_plan_rate2->duration + $duration;
       $discount2 = $get_plan_rate2->discount;
                    if ($diff <= $duration2 && $diff > $duration) {
                        $inti_discount2  = 100 - $discount2;
                        $total_discount2 = '.'.$inti_discount2;

                        $total_months12  = $value->months12_price * $total_discount2;
                        $total_months6   = $value->months6_price  * $total_discount2;
                   $test = 'test2';
                    } else {
                        $total_months12  = $value->months12_price;
                        $total_months6   = $value->months6_price; 
                   $test = 'test3';
                    }
        }
  } 

} else{ 
  $total_months12 = $value->months12_price;
  $total_months6  = $value->months6_price; 
}


//COMPUTE POSTED ADS
        $total_ad_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();

        $total_auction_posted = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get(); 

        $total_uploaded_photos = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                         ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.photo', '!=', 'null')
                                                    ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->get();

        $total_embed_video = Advertisement::where('user_id','=', $user_id)->where('youtube', '!=', 'null' )->get();


        if (Auth::check()) {
         $get_user_plan_types = UserPlanTypes::find($user_plan_types);
         $total_ads_allowed = $get_user_plan_types->total_ads_allowed;
         $auction = $get_user_plan_types->auction;
        $img_total =  $get_user_plan_types->img_total;
        $video_total = $get_user_plan_types->video_total;
        } else {
           $total_ads_allowed = '0';
         $auction = '0';
        $img_total =  '0';
        $video_total = '0';
        }
    
          //compute listing available
                  $listing_available = $total_ads_allowed - count($total_ad_posted);
                 if ($listing_available <= 0) {
                   $get_listing_available = '0';
                 } else{
                  $get_listing_available = $listing_available;

                 }
         //compute auction ad available
            $auction_ads_available = $auction - count($total_auction_posted);
               if ($auction_ads_available <= 0) {
                 $get_auction_ads_available = '0';
               } else{
                $get_auction_ads_available = $auction_ads_available;
               }
         //compute photos available
               $upload_photos_available = $img_total - count($total_uploaded_photos);
               if ($upload_photos_available <= 0) {
                 $get_upload_photos_available = '0';
               } else{
                $get_upload_photos_available = $upload_photos_available;
               }

          //compute videos available
               $embed_videos_available = $video_total - count($total_embed_video);
               if ($embed_videos_available <= 0) {
                 $get_embed_videos_available = '0';
               } else{
                $get_embed_videos_available = $embed_videos_available;
               }

 $plan_description .='<div id="'.$value->plan_name.'" class="col-md-12">
                        <div class="panel panel-default removeBorder borderZero borderBottom">
                          <div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>'.strtoupper($value->plan_name).'</strong></span>';
 if (Auth::check()) {
  $plan_description .='<form action="plan/payment" id="upgrade" method="post"><input type="hidden" name="_token" id="csrf-token" value="'.$token.'" />';
 }
  $plan_description .='</div><div class="panel-body"><div class="bottomPaddingB xxlargeText"><strong><span class="blueText">'.strtoupper($value->plan_name).'</span> <span class="lightgrayText"> ZERO COST FOREVER</span></strong>';

if (Auth::check()) {
    $plan_description .=($user_plan_types  == $value->id ? '<span class="pull-right"><button type="submit" name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton borderZero noMargin" type="submit" id="btn-get-started-below">GET STARTED</button></span>' : '<span class="pull-right"><button type="submit" name="plan_id" value="'.$value->id.'" class="btn btn-sm blueButton borderZero noMargin" type="submit">GET STARTED</button></span>');
     $plan_description .='</form>';

  } else {
$plan_description .='<span class="pull-right"><button class="btn btn-sm blueButton borderZero noMargin btn-login" data-toggle="modal" data-target="#modal-login" type="submit">GET STARTED</button></span>';
}
              $plan_description .=' </div>
              <div class="row borderbottomLight">                  
                          </div>
                           <div class="blockTop">
                           <div class="lineHeightC  normalText grayText bottomPaddingC">
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue leo eu erat elementum placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam at dui dui. Nunc rhoncus tellus eu neque ultrices molestie. Vivamus eget rutrum leo. Nulla dapibus ligula quis massa faucibus sollicitudin sed ac purus. Maecenas egestas rhoncus nunc, sit amet vestibulum lectus iaculis nec. Pellentesque at ipsum id odio suscipit commodo a a dui. Nam turpis nibh, imperdiet sed urna quis, auctor maximus quam. Cras scelerisque metus tempus ligula gravida blandit. Praesent dictum tempor quam varius laoreet. Morbi quis libero ut erat egestas lacinia laoreet nec mauris. Ut nec imperdiet lectus, at ullamcorper orci. Aenean ultrices iaculis augue at tempor.
                            </div>
                            <div class="col-lg-12 lineHeightC  noPadding">
                            <div class="col-lg-4 noleftPadding">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Ads Remaining <span class="pull-right blueText bold">'.$get_listing_available.'</span>':'No. of Ads <span class="pull-right blueText bold">'.$value->ads.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Auction Remaining <span class="pull-right blueText bold">'.$get_listing_available.'</span>':'No. of Auctions <span class="pull-right blueText bold">'.$value->auction.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Image per Ad Remaining <span class="pull-right blueText bold">'.$get_upload_photos_available.'</span>':' No. of Image per Ad <span class="pull-right blueText bold">'.$value->img_per_ad .'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Videos Embed per Ad Remaining <span class="pull-right blueText bold">'.$get_embed_videos_available.'</span>':' No. of Videos per Ad <span class="pull-right blueText bold">'.$value->video_total .'').'</span></span>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Free Points <span class="pull-right blueText bold">'.$value->point.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Bids <span class="pull-right blueText bold">'.$value->bid.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Free SMS Notification <span class="pull-right blueText bold">'.$value->point_exchange.' </span></span>
                            </div>
                            </div>
                            </div>
                          </div>
                          </div>
                      </div>
                    </div>
';
}

    $this->data['plan'] = $plan;
    $this->data['plan_description'] = $plan_description;

    

    return View::make('plans.list', $this->data);
  }

}
