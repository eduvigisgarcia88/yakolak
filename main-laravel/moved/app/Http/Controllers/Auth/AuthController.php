<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
    //  public function login() {

    //     if (!Auth::check()) {
    //         $this->data['title'] = "Login";
    //         return view('auth/login', $this->data);
    //     }
    //     else {
    //         // if user is already logged in
    //         return redirect()->intended();
    //     }

    // }
     
    // /**
    //  * Attempt to do login
    //  *
    //  */
    // public function doLogin(Request $request) {

    //     $username = $request->input('email');
    //     $password = $request->input('password');
    //     $remember = (bool) $request->input('remember');
    //     $honeypot = $request->input('honeypot');

    //     if($honeypot) {
    //         // robot detected
    //         $error = trans('validation.blank');
    //     } 
    //     else if(Auth::attempt(['username' => $username, 'password' => $password, 'status' => 1], $remember)) {
    //         // login successful
    //         return redirect()->intended();
    //     }
    //     else if(Auth::validate(['username' => $username, 'password' => $password])) {
    //         $user = User::where('username', $username)->first();

    //         if($user->status == 0) {
    //             // user is not active
    //             $error = "This account is currently inactive.";
    //         }
    //     }
    //     else {
    //         // invalid login
    //         $error = 'Invalid email or password.';
    //     }
    //     // return error
    //     return redirect()->action('AuthController@login')
    //                    //->withInput($request->except('password'))
    //                    ->with('notice', array(
    //                         'msg' => $error,
    //                         'type' => 'danger'
    //                    ));
    // }
    // /**
    //  * Displays the forgot password form
    //  *
    //  */
    // public function forgotPassword() {
    //     $this->data['title'] = "Password Recovery";
    //     return View::make('users.forgotpass', $this->data);
    // }

    // /**
    //  * Attempt to send change password link to the given email
    //  *
    //  */
    // public function doForgotPassword() {
    //     $user = User::where('email', Input::get('email'))
    //                 ->first();

    //     View::composer('emails.reminder', function($view) use ($user) {
    //         $view->with([
    //             'firstname' => $user->firstname
    //         ]);
    //     });

    //     $response = Password::remind(Input::only('email'), function($message) {
    //         $message->subject("Reset Password Request");
    //     });

    //     // (test) always say success
    //     $response = Password::REMINDER_SENT;

    //     switch ($response) {
    //         case Password::INVALID_USER:
    //             return Redirect::back()->with('notice', array(
    //                 'msg' => Lang::get($response),
    //                 'type' => 'danger'
    //             ));

    //         case Password::REMINDER_SENT:
    //             return Redirect::back()->with('notice', array(
    //                 'msg' => Lang::get($response),
    //                 'type' => 'success'
    //             ));
    //     }
    // }

    // /**
    //  * Shows the change password form with the given token
    //  *
    //  */
    // public function resetPassword($token = null) {
    //     if(is_null($token)) App::abort(404);

    //     if(Auth::guest()) {
    //         $this->data['token'] = $token;
    //         $this->data['title'] = "Reset Password";

    //         return View::make('users.resetpass', $this->data);
    //     } else {
    //         return Redirect::to('/');
    //     }
    // }

    // /**
    //  * Attempt change password of the user
    //  *
    //  */
    // public function doResetPassword() {
    //     $credentials = Input::only(
    //         'email', 'password', 'password_confirmation', 'token'
    //     );

    //     Password::validator(function($credentials) {
    //         return strlen($credentials['password']) >= 6 && $credentials['password'] != $credentials['email'];
    //     });

    //     $response = Password::reset($credentials, function($user, $password) {
    //         $user->password = Hash::make($password);
    //         $user->save();
    //     });

    //     switch ($response) {
    //         case Password::INVALID_PASSWORD:
    //         case Password::INVALID_TOKEN:
    //         case Password::INVALID_USER:
    //             return Redirect::back()
    //                            ->withInput(Input::except('password'))
    //                            ->with('notice', array(
    //                                 'msg' => Lang::get($response),
    //                                 'type' => 'danger'
    //             ));
    //         case Password::PASSWORD_RESET:
    //             return Redirect::to('login')
    //                            ->with('notice', array(
    //                                 'msg' => Lang::get('reminders.complete'),
    //                                 'type' => 'success'
    //             ));
    //     }
    // }

    // public function logout() {
    //     Auth::logout();
    //     return redirect()->action('AuthFrontendController@login');
    // }

}
