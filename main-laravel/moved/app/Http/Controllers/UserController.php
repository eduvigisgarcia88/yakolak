<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use App\Country;
use App\City;
use App\CompanyBranch;


class UserController extends Controller
{
   public function index(){

      $result = $this->doList();
      
      $this->data['rows'] = $result['rows'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/users/refresh');
   	  $this->data['title'] = "User Management";
      $this->data['usertypes']  = Usertypes::all();
      $this->data['branch'] = "";
      $this->data['country'] = Country::orderBy('countryName','asc')->get();

      return View::make('admin.user-management.front-end-users.list', $this->data);
   }
   public function edit(){

    $row = User::with('getUserCompanyBranch')->find(Request::input('id'));
    $get_city = City::all();
      $city ='';
      foreach ($get_city as $key => $field) {
        $city .= '<option value="'.$field->id.'"'.($field->id == $row->city ? 'selected' : '').' >'.$field->name.'</option>';
      }
       //dd($rows);
    $get_countries = Country::all();
    // $get_cities = City::all();
    // dd($rows);
    $branch = "";
    if (count($row->getUserCompanyBranch) > 0) {
      $ctr = 0;      
      foreach ($row->getUserCompanyBranch as $key => $field) {
         if($field->status == 1){
         $branch .= '<div id ="accountBranch"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 borderbottomLight normalText noPadding bottomPadding topMarginB bottomMarginB redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText">' . ($ctr == 0 ? '<button type="button" value="" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus redText"></i> ADD BRANCH</button>' : '<button type="button" value="" class="hide-view btn-del-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-minus redText"></i> REMOVE</button>') . '</span>' .
            '</div>' .
            '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="'.$field->company_tel_no.'" class="form-control borderZero inputBox fullSize" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as $gckey => $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '" ' . ($field->company_country == $gcfield->id ? 'selected' : '') . '>' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="'.$field->company_address.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="'.$field->company_office_hours.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="'.$field->company_email.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="'.$field->company_mobile.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';

                    $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get()->take(50);
                      // dd($get_cities);
                    foreach ($get_cities as $gkey => $gfield) {
                      
                      $branch .= '<option value="' . $gfield->id . '" ' . ($field->company_city == $gfield->id ? 'selected' : '') .'>' . $gfield->name . '</option>';

                    }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="'.$field->company_website.'" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id" value="'.$field->id.'">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status" value="'.$field->status.'">'.
          '</div></div>';

          $ctr += 1;
      }
    }
    }
    else {
      $branch .= '<div id ="accountBranch"><div class="col-lg-12  borderbottomLight normalText noPadding bottomPadding topMarginB bottomMargin redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText"><button type="button" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus redText"></i> ADD BRANCH</button></span>' .
            '</div>' .
            '<div class="col-lg-6 ">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as  $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '">' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 ">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
               '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';

                    // $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get();
                    //   // dd($get_cities);
                    // foreach ($get_cities as $gkey => $gfield) {
                      
                    //   $branch .= '<option value="' . $gfield->id . '">' . $gfield->name . '</option>';

                    // }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status">' .
          '</div></div>';
    }
    // $get_branch = CompanyBranch::where('user_id',Request::input('id'))->get();

    //   $branch = '';

    //     if(count($get_branch) > 0){
    //         foreach ($get_branch as $key => $field) {
    //         $branch .= '';

    //         }
    //     }else{
    //         $branch .='';
    //     }
       $result['branch']  = $branch;
       $result['city']  = $city;
       $result['row']  = $row;
       // dd($branch);
    return Response::json($result);
   }
   public function doList() {
        
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $usertype_id = Request::input('usertype_id') ? : 1;
      // dd($usertype_id);
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = User::select('users.*')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        // dd($rows);
      } else {
        $count = User::where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                 ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = User::select('users.*')
                                ->where(function($query) use ($status) {
                                    $query->where('users.usertype_id', '!=', '8')
                                          ->where('status', 'LIKE', '%' . $status . '%');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('code', 'LIKE', '%' . $search . '%')
                                        ->orWhere('name', 'LIKE', '%' . $search . '%');
                                  })
                                 ->where(function($query) use ($usertype_id) {
                                  $query->where('usertype_id','LIKE', '%' . $usertype_id . '%');
                                        
                                  })

                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }
    }
   public function register() {

        if (!Auth::check()) {
           
            return view('auth/register');
        }
        else {
            // if user is already logged in
            return redirect()->intended();
        }

    }

    public function doRegister(Request $request) {
    	
		$registerUser = new User;
		$registerUser->firstname = $request->input('firstname');
		$registerUser->lastname = $request->input('lastname');
		$registerUser->email = $request->input('email');
		$registerUser->username = $request->input('email');
		$registerUser->password =  Hash::make($request->password);
		$registerUser->remember_token = $request->input('_token');

		$registerUser->save();

    Session::flash('success', 'uploaded file is not valid');
    return Redirect::to('auth/register');
       
    }


}