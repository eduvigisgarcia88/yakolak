<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use Request;
use Response;
use Input;
use Image;
use Validator;
use Carbon\Carbon;
use Paginator;
use App\Usertypes;

class FrontEndBannerController extends Controller
{


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
      public function getBannerPlacementPlan() {
        $get_bannerplacementplan = BannerPlacementPlans::where('banner_placement_id', '=', Request::input('id'))->orderBy('duration','asc')->get();

        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_bannerplacementplan as $key => $field) {
          $rows .= '<option value="' . $field->id . '">' .'$'. $field->price .' USD '.$field->duration. ' months'. '</option>';
        }
        return Response::json($rows);

     }

 
        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
      public function purchaseBannerPlacement(){
      }
         /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
      public function saveBannerPlacement(){

    $new = true;
        $input = Input::all();
        dd($input);
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = BannerSubscriptions::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'                 => $new ? 'image|max:3000' : 'image|max:3000',
            'banner_name'           => 'required',
            'advertiser_name'         => 'required',
            'banner_placement_id'     => 'required',
            'banner_placement_plan'   => 'required',
            'cat_id'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'photo'              => 'photo',
            'banner_name'      => 'banner name',
            'advertiser_name'    => 'advertiser',
            'banner_placement_id'       => 'Banner placement',
            'banner_placement_plan'  => 'Banner placement plan',
            'cat_id'  => 'Category Id',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new BannerSubscriptions;
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
        } else {
            $row->banner_name      = strip_tags(array_get($input, 'banner_name'));
            $row->advertiser_name      = strip_tags(array_get($input, 'advertiser_name'));
            $row->placement     = array_get($input, 'banner_placement_id');
            $row->placement_plan     = array_get($input, 'banner_placement_plan');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->save();
        }   
            $row->save();
            $get_plan_duration = BannerPlacementPlans::find($row->placement_plan);
            $date = strtotime(date("Y-m-d H:i:s", strtotime($row->created_at)) . " +".$get_plan_duration->duration." month");
            $row->expiration = date("Y-m-d H:i:s", $date);
            $row->save();

          if (Input::file('photo')) {
              // Save the photo
              Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-'.$row->id . '.jpg');
              $row->photo = 'banner-'.$row->id . '.jpg';
              $row->save();
          }

      return Response::json(['body' => ['Banner Advertisement Saved']]);

  }

   
}