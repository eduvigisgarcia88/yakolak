<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\Country;
use App\City;
use App\SubAttributes;
use App\SubCategory;
use App\CustomAttributeValues;
use App\CustomAttributesForDropdown;
use App\CustomAttributesForTextbox;
use App\Http\Controllers\Controller;
use App\AdvertisementType;
use App\AdvertisementBidDurationDropdown;
use App\AdManagementSettings;
use App\AuctionBidders;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
class AdvertisementController extends Controller
{
   public function index(){
      $result = $this->doList();
      $this->data['rows'] = $result['rows'];
      $this->data['get_auction'] = $result['get_auction'];
      $this->data['ad_management_settings'] = $result['ad_management_settings'];
      $this->data['countries'] = $result['countries'];
      $this->data['city'] = $result['city'];
      $this->data['pages'] = $result['pages'];
      $this->data['refresh_route'] = url('admin/ads/refresh');
      $this->data['title'] = "Advertisement Management";
      $this->data['ad_types']  = AdvertisementType::all();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      $this->data['custom_attributes'] = '';

      return View::make('admin.ads-management.front-end-users.list', $this->data);
   }

   public function doList() {
      // sort and order
        $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
        $result['order'] = Request::input('order') ?: 'desc';
        $search = Request::input('search');
        $per = Request::input('per') ?: 10;
        if (Request::input('page') != '»') {
          Paginator::currentPageResolver(function () {
              return Request::input('page'); 
          });
      $rows =  Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status', 'advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube') 
                                  
                                    ->where('advertisements_photo.primary', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('title', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('title', 'LIKE', '%' . $search . '%');
                                    })
                                  ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
                                  // dd($rows);
        } else {
             $count =  Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube') 
                                   
                                    ->where('advertisements_photo.primary', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('title', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('title', 'LIKE', '%' . $search . '%');
                                    })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);
          
          Paginator::currentPageResolver(function () use ($count, $per) {
              return ceil($count->total() / $per);
          });

              $rows =  Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->select('advertisements.status','advertisements.updated_at','advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'users.country','users.city as user_city', 'countries.countryName',  'country_cities.name as city', 'advertisements.address', 'advertisements.youtube') 
                                    
                                    ->where('advertisements_photo.primary', '=', '1')
                                    ->where(function($query) use ($search) {
                                        $query->where('title', 'LIKE', '%' . $search . '%')
                                                  ->orWhere('title', 'LIKE', '%' . $search . '%');
                                    })
                                    ->orderBy($result['sort'], $result['order'])
                                    ->paginate($per);      
        }
       
       $get_auction = "";
       $get_auction .= '<h5 class="inputTitle borderbottomLight">Auction Information </h5>'.
                      '<div class="form-group">'.
                       '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Limit Amount</label>'.
                         '<div class="col-sm-9">'.
                           '<input type="number" class="form-control borderZero" id="row-bid_limit_amount"  name="bid_limit_amount">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Bid Start Amount</label>'.
                        '<div class="col-sm-9">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_start_amount"  name="bid_start_amount" >'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">Minimum Allowed Bid</label>'.
                        '<div class="col-sm-9">'.
                          '<input type="number"  class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid">'.
                        '</div>'.
                      '</div>'.
                      '<div class="form-group">'.
                         '<h5 class="col-sm-3 normalText" for="register4-email"></h5>'.
                        '<div class="col-sm-9">'.
                          '<div class="col-sm-6 noleftPadding">'.
                          '<div class="form-group">'.
                            '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                            '<div class="col-sm-8">'.
                              '<input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start">'.
                              '</div>'.
                            '</div>'.
                          '</div>'.
                          '<div class="col-sm-6 norightPadding">'.
                            '<div class="form-group">'.
                              '<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>'.
                              '<div class="col-sm-8"> '.        
                           '<input type="text" name="value" id="row-value" class="form-control borderZero" value="">' .
                          '<select name="bid_duration_dropdown_id" id="row-bid_duration_dropdown_id" class="form-control fullSize hide" required="required">'.
                          '<option class="hide" selected>Select:</option>';
                                $get_bid_duration_dropwdown = AdvertisementBidDurationDropdown::orderBy('id', 'asc')->get(); 
                                foreach ($get_bid_duration_dropwdown as $bid_key => $bid_field) {
                                          $get_auction .= '<option value="' . $bid_field->id . '" >'. $bid_field->value . '</option>';
                                         }
        $get_auction .='</select>'.'</div>'.
                            '</div>
                          </div>
                        </div>
                      </div>';

         $countries = Country::all();
         $ad_management_settings = AdManagementSettings::first();
         $city = City::all();

        // return response (format accordingly)
        if(Request::ajax()) {
          $result['pages']        = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows->toArray();
          $result['countries']    = $countries->toArray();
          $result['city']         = $city->toArray();
          $result['ad_management_settings'] = $ad_management_settings->toArray();
          return Response::json($result);
        } else {
          $result['pages']        = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows']         = $rows;
          $result['countries']    = $countries;
          $result['city']         = $city;
          $result['get_auction']  = $get_auction;
          $result['ad_management_settings'] = $ad_management_settings;
          return $result;
        }
    }


    public function edit(){
      $ads_id = Request::input('ads_id');
      $row = Advertisement::join('advertisements_photo', 'advertisements.id', '=', 'advertisements_photo.ads_id')
                               ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                               ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                               ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                               ->leftJoin('ad_bid_duratation_dropdown','advertisements.bid_duration_dropdown_id', '=', 'ad_bid_duratation_dropdown.id')
                               ->select('advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.sub_category_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'advertisements.address', 'advertisements.youtube', 'advertisements.bid_limit_amount', 'advertisements.bid_start_amount', 'advertisements.minimum_allowed_bid', 'advertisements.bid_duration_start', 'advertisements.bid_duration_dropdown_id',
                                         'ad_bid_duratation_dropdown.value')    
                               ->where(function($query) use($ads_id){
                                             $query->where('advertisements.id', '=', $ads_id);                                  
                                      })->first();

      $get_photo = AdvertisementPhoto::where('ads_id','=', $ads_id)->get(); 
      $category = Category::with('subcategoryinfo')->get();
      $sub_categories = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', '26')->get();                            
      //Append
      $result['photo']          = $get_photo;                      
      $result['row']            = $row;                            
      $result['category']       = $category;                            
      $result['sub_categories'] = $sub_categories;                            

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
   }


    public function save(){

        $new = true;
        $input = Input::all();
        $ads_type_id = Input::get('ads_type_id');
        $category = Input::get('category');
        $photo = array_get($input, 'photo');
        $rem_img_upload = Input::get('rem_img_upload');

        $text_field = Input::get('text_field');
        $dropdown_field = Input::get('dropdown_field');
        $attribute_textbox_id = Input::get('attribute_textbox_id');
        $attribute_dropdown_id = Input::get('attribute_dropdown_id');

        $text_field_id = Input::get('text_field_id');
        $dropdown_field_id = Input::get('dropdown_field_id');
        $country = Input::get('country');
        $bid_limit_amount = Input::get('bid_limit_amount');
        $bid_start_amount = Input::get('bid_start_amount');
        $ads_id = array_get($input, 'id');
        $bid_duration_start = Input::get('bid_duration_start');
        $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');

        // check if an ID is passed
        if($ads_id) {
            // get the user info
            $row = Advertisement::find(array_get($input, 'id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

     // getting all of the post data
        $files = Input::file('photo');
        $rules = [
             'photo'                    => 'max:3000',
             'category'                 => 'numeric|required',
             'subcategory'              => 'numeric|required',
             'title'                    => 'required|min:4|max:100',
             'description'              => 'required',
             'price'                    => 'required|min:5|max:100000000|numeric',
             'bid_duration_start'       => 'min:1|max:99|numeric',
             'country'                  => 'required',
             'city'                     => 'required',
             'bid_duration_dropdown_id' => ($ads_type_id==2?'required':''),
             'bid_limit_amount'         => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_start_amount'         => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'minimum_allowed_bid'      => ($ads_type_id==2?'min:1|max:999999999999|numeric|required':''),
             'bid_duration_start'       => ($ads_type_id==2?'min:1|max:99|numeric|required':''),


             
        ];
        // field name overrides
        $names = [   
            'photo'                    => 'photo',
            'category'                 => 'category',
            'subcategory'              => 'sub category',
            'photo'                    => 'photo',
            'title'                    => 'title',
            'description'              => 'description',
            'price'                    => 'price',
            'bid_duration_start'       => 'bid duration',
            'Country'                  => 'Country',
            'City'                     => 'City',
            'bid_duration_dropdown_id' => 'Bid duration',
            'bid_limit_amount'         => 'bid limit amount',
            'bid_start_amount'         => 'bid start amount',
            'minimum_allowed_bid'      => 'minimum allowed amount',
            'bid_duration_start'       => 'bid duration',

        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($ads_type_id == '2') {
            if ($bid_limit_amount <= $bid_start_amount) {
                return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start']); 
            }
        }
        
     // create model if new
      if($new) {
          $row = new Advertisement;
      }

          $row->ads_type_id                = array_get($input, 'ads_type_id');
          $row->cat_unique_id              = $category;
          $row->user_id                    = Auth::user()->id;
          $row->title                      = array_get($input, 'title');
          $row->price                      = array_get($input, 'price');
          $row->description                = array_get($input, 'description');
          if (array_get($input, 'category')) {
           $row->category_id          = array_get($input, 'category');
          }else {
           $row->category_id          = null;
          }
          if (array_get($input, 'subcategory')) {
           $row->sub_category_id      = array_get($input, 'subcategory');
          }else {
           $row->sub_category_id      = null;
          }
         
          if ($row->bid_duration_start != $bid_duration_start || $row->bid_duration_dropdown_id != $bid_duration_dropdown_id) {
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                }       
          } 

          if ($ads_type_id == 2) {
            $row->bid_limit_amount         = array_get($input, 'bid_limit_amount');
            $row->bid_start_amount         = array_get($input, 'bid_start_amount');
            if ($new == true) {
            $row->minimum_allowed_bid      = array_get($input, 'minimum_allowed_bid'); 
            } else {
             $reset_bidder = AuctionBidders::where('ads_id','=',$row->id)->where('bid_limit_amount_status','=',1)->first();
              if ($reset_bidder) {
                $reset_bidder->bid_limit_amount_status = '0';
                $reset_bidder->save();
              }
            }
            $row->bid_duration_start       = array_get($input, 'bid_duration_start');
            $row->bid_duration_dropdown_id = array_get($input, 'bid_duration_dropdown_id');
          } else {
            $row->bid_limit_amount          = null;
            $row->bid_start_amount          = null; 
            $row->minimum_allowed_bid       = null;
            $row->bid_duration_start        = null;
            $row->bid_duration_dropdown_id  = null;
          }

          if (array_get($input, 'youtube')) {
            $row->youtube                  = array_get($input, 'youtube');
          } else {
            $row->youtube                  = null;
          }
            if (array_get($input, 'country')) {
            $row->country_id               = array_get($input, 'country');
          } else {
            $row->country_id               = null;
          }
            if (array_get($input, 'city')) {
            $row->city_id                  = array_get($input, 'city');
          } else {
            $row->city_id                  = Auth::user()->city;
          }
            if (array_get($input, 'address')) {
            $row->address                  = array_get($input, 'address');
          } else {
            $row->address                  = null;
          }

          //for auction exp email trigger
          if ($row->ads_type_id == 2) {
           $row->ad_exp_email_trigger = 1;
           $row->auction_expiring_noti_trigger = 1;
          } else {
          $row->ad_expiration =  '0000-00-00 00:00:00';
          }
            // save model

     if ($new) {
      $i = $rem_img_upload;

      for ($x = 1; $x <= $i; $x++) {
        if(Input::file('photo'.$x.'')){
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo'.$x.''))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo'.$x.''))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
             $savephoto = new AdvertisementPhoto;
             $savephoto->ads_id = $row->id;
                if ($x == 1) {
                    $savephoto->primary = 1;
                 }
             $savephoto->photo = $FileName . '.jpg';
             $savephoto->save();
        }
            else{
        $savephoto = new AdvertisementPhoto;
        $savephoto->ads_id = $row->id;
        $savephoto->photo = null;
        $savephoto->save();
        }
      }

      if(!is_null($text_field)){
        foreach ($text_field_id as $key => $value) {
           $ad_attri = new CustomAttributesForTextbox;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $text_field_id[$key];
           $ad_attri->attri_value  = $text_field[$key];
           $ad_attri->save();   
        }
      }
      if(!is_null($dropdown_field)){
        foreach ($dropdown_field_id as $key => $xvalue) {
           $ad_attri = new CustomAttributesForDropdown;
           $ad_attri->ad_id  = $row->id;
           $ad_attri->attri_id = $dropdown_field_id[$key];
           $ad_attri->attri_value_id  = $dropdown_field[$key];
           $ad_attri->save();
        }
      } 
   }
   //FOR EDITING ADS
  else{
    // Save the photo
         if (array_get($input, 'photo1')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo1'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo1'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id1);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo2')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo2'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo2'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id2);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo3')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo3'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo3'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id3);
            $row->photo = $FileName . '.jpg';
            $row->save();
          }
        if (array_get($input, 'photo4')) {
            // Save the photo
            $FileName = $row->id.rand(10,9999);
            Image::make(Input::file('photo4'))->save('uploads/ads/' . $FileName .'.jpg');
            Image::make(Input::file('photo4'))->fit(300, 300)->save('uploads/ads/thumbnail/' . $FileName .'.jpg');
            $row = AdvertisementPhoto::find($photo_id4);
            $row->photo = $FileName . '.jpg';
            $row->save();
        }

          $get_exist_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->get();
          if ($get_exist_dropdown) {
               foreach ($get_exist_dropdown as $values) {
                  $values->status = '2';
                  $values->save();    
               }

                if(!is_null($dropdown_field)){
                          foreach ($dropdown_field_id as $key => $xvalue) {     
                             $ad_attri = new CustomAttributesForDropdown;
                             $ad_attri->ad_id  = $row->id;
                             $ad_attri->attri_id = $dropdown_field_id[$key];
                             $ad_attri->attri_value_id  = $dropdown_field[$key];
                             $ad_attri->save();
                          }
                    } 
          }
          $get_existing_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->get();
          if ($get_existing_textbox) {
               foreach ($get_existing_textbox as $values) {
                  $values->status = '2';
                  $values->save();    
               }

             if(!is_null($text_field)){
                  foreach ($text_field_id as $key => $value) {
                     $ad_attri = new CustomAttributesForTextbox;
                     $ad_attri->ad_id  = $row->id;
                     $ad_attri->attri_id = $text_field_id[$key];
                     $ad_attri->attri_value  = $text_field[$key];
                     $ad_attri->save();   
                  }
                }
          }
  }

  $row->save();


    if($new){
      return Response::json(['body' => "Advertisement Created"]);
    } 
    else {
         return Response::json(['body' => "Advertisement Successfully updated"]);
       }




  }



    public function viewAds (){
      $ads_id = Request::input('ads_id');
      $row = Advertisement::join('advertisements_photo', 'advertisements.id', '=', 'advertisements_photo.ads_id')
                               ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                               ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                               ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                               ->leftJoin('ad_bid_duratation_dropdown','advertisements.bid_duration_dropdown_id', '=', 'ad_bid_duratation_dropdown.id')
                               ->select('advertisements.ad_expiration','advertisements.ads_type_id','advertisements.status','advertisements.title','advertisements.user_id', 'advertisements.sub_category_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'advertisements.country_id','advertisements.city_id', 'advertisements.address', 'advertisements.youtube', 'advertisements.bid_limit_amount', 'advertisements.bid_start_amount', 'advertisements.minimum_allowed_bid', 'advertisements.bid_duration_start', 'advertisements.bid_duration_dropdown_id',
                                         'ad_bid_duratation_dropdown.value')    
                               ->where(function($query) use($ads_id){
                                             $query->where('advertisements.id', '=', $ads_id);                                  
                                      })->first();
      $get_photo = AdvertisementPhoto::where('ads_id','=', $ads_id)->get(); 
      $category = Category::with('subcategoryinfo')->get();
      $sub_categories = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=', '26')->get();                            
      //Append
      $result['photo']          = $get_photo;                      
      $result['row']            = $row;                            
      $result['category']       = $category;                            
      $result['sub_categories'] = $sub_categories;                            

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }  
      }


 public function removeAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "2";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Removed']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }



     public function markSpamAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "4";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Marked as Spam']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }



         public function disableAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "3";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Disabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }


             public function enableAds() {
      $row = Advertisement::find(Request::input('id'));
      if(!is_null($row)){
        $row->status = "1";
        $row->save();
        return Response::json(['body' => 'Ad Successfully Enabled']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }




  public function saveAdManagemetSettings() {
        $input = Input::all();
        $basic_ad_expiration_start = Input::get('basic_ad_expiration_start');
        $basic_ad_expiration_end = Input::get('basic_ad_expiration_end');
        $ad_expiration_status = Input::get('ad_expiration_status');
        $bid_inc_value = Input::get('bid_inc_value');
        $bid_adding_time_start = Input::get('bid_adding_time_start');
        $bid_adding_time_end = Input::get('bid_adding_time_end');
        $expiring_auction_noti_time_start = Input::get('expiring_auction_noti_time_start');
        $expiring_auction_noti_time_end = Input::get('expiring_auction_noti_time_end');
   
   // getting all of the post data
      $files = Input::file('photo');
       $rules = [
             'basic_ad_expiration_start' => 'max:99|required|int',
             'basic_ad_expiration_end'   => 'required',
             'bid_inc_value'             => 'required|int|min:1|max:999999999',
             'bid_adding_time_start'     => 'max:99|required|int',
             'bid_adding_time_end'       => 'required',
        ];
        // field name overrides
        $names = [
            
            'basic_ad_expiration_start'  => 'basic ad expire time',
            'basic_ad_expiration_end'    => 'days',
            'bid_inc_value'              => 'bid value',
            'bid_adding_time_start'      => 'bid adding time',
            'bid_adding_time_end'        => 'bid adding time',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $rows = AdManagementSettings::find('1');
        $rows->basic_ad_expiration_start = $basic_ad_expiration_start;
        $rows->basic_ad_expiration_end   = $basic_ad_expiration_end;
        $rows->bid_inc_value             = $bid_inc_value;
        $rows->bid_adding_time_start     = $bid_adding_time_start;
        $rows->bid_adding_time_end       = $bid_adding_time_end;
        $rows->expiring_auction_noti_time_start  = $expiring_auction_noti_time_start;
        $rows->expiring_auction_noti_time_end    = $expiring_auction_noti_time_end;

        $rows->save();

        if($rows){
        return Response::json(['body' => 'Ad Settings Successfully Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }




    public function editAdManagemetSettings(){
      $settings_id = Request::input('settings_id');

       $row = AdManagementSettings::find($settings_id);
     if($row) {
           return Response::json($row);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
   }




   public function listAdPictures(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $get_photos = AdvertisementPhoto::where('ads_id','=',$ads_id)->where('photo','!=', 'null')->get();
      $rows ='';
     foreach ($get_photos as $value) {
      $rows .='<img src="'.url('/').'/uploads/ads/thumbnail/'.$value->photo.'" id="row-photo" width="100px" height="100px" />';               
      
     }
      return Response::json($rows);
    }




   public function getCity(){
     $input = Input::all();
     $country_id = Request::input('country_id');
     $rows ="";     
     $city = City::where('country_id','=',$country_id)->get();
        foreach($city as $cities) {
           $rows .='<option value="'.$cities->id.'">'. $cities->name .'</option>';
        }
     $rows .='</select>';
      return Response::json($rows);
    }




    public function getSubCategory(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');
     $get_subcat = Advertisement::find($ads_id);
     if (Request::input('cat_id')) {
     $cat_id = Request::input('cat_id'); 
     } else {
     $cat_id = $get_subcat->category_id;
     }
     $sab_cat = $get_subcat->sub_category_id;

     $rows ="";
     $rows = '<option class="hide" value="">Select:</option>';
     $get_sub = SubCategory::where('buy_and_sell_status','=',1)->where('biddable_status','=',1)->where('cat_id','=',$cat_id)->get(); 
     foreach ($get_sub as $value) {
         $rows .= '<option value="'.$value->id.'" '.($sab_cat==$value->id?'selected':'' ).'>'.$value->name.'</option>';
     }

    return Response::json($rows);
    }




    public function getCategory(){
     $input = Input::all();
     $ads_type_id = Request::input('ads_type_id');
     $ads_id = Request::input('ads_id');
     $get_cat_id = Advertisement::find($ads_id);
     $cat_id = $get_cat_id->category_id;
     $rows ="";
     $rows .='<option class="hide" value="">Select:</option>';
     if ($ads_type_id == '1') {
           $get_cats = Category::where(function($query) {
                               $query->where('status', '=', '1')
                                     ->where('buy_and_sell_status', '=', '2')
                                     ->orWhere('buy_and_sell_status','=','3');
                                 })->get();
     } else {
           $get_cats = Category::where(function($query) {
                                $query->where('status', '=', '1')
                                      ->where('biddable_status', '=', '2')
                                      ->orWhere('biddable_status','=','3');                                  
                                 })->get();
     }

   foreach ($get_cats as $get_cat) {
     $rows .='<option value="'.$get_cat->id.'" '.($cat_id==$get_cat->id?'selected':'').'>'.$get_cat->name.'</option>';
   }

    return Response::json($rows);
    }




      public function fetchCustomAttributesDisabled(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $rows = "";
          $i = 1;
          foreach ($get_dropdown as $key => $field) {
            $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
             foreach ($get_attri_info as $key => $field_attri_info) {
                $rows .= '<div class="form-group" id="ads-custom_attribute">'.
                                     '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="dropdown_field[]" disabled="disabled">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $rows .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $rows .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'" disabled="disabled">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'" disabled="disabled">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
               foreach ($get_attri_info as $key => $field_attri_info) {

                $rows .= '<div class="form-group">'.
                                        '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                          '<div class="col-sm-9">';                  
                $rows .= '<input type="text" class="form-control borderZero" name="text_field[]" value='.$field->attri_value.'>';                  
                                 
                $rows .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'" disabled="disabled">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'" disabled="disabled">'.
                                  '</div>'; 
               }           
          }
    return Response::json($rows);


      }
       public function fetchCustomAttributesEnabled(){
     $input = Input::all();
     $ads_id = Request::input('ads_id');

        $get_dropdown = CustomAttributesForDropdown::where('ad_id','=',$ads_id)->where('status','=','1')->get();
          $rows = "";
          $i = 1;
          foreach ($get_dropdown as $key => $field) {
            $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
             foreach ($get_attri_info as $key => $field_attri_info) {
                $rows .= '<div class="form-group" id="ads-custom_attribute">'.
                                     '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                        '<div class="col-sm-9">'.
                                          '<select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="dropdown_field[]">';
                                          $get_attri_values =  CustomAttributeValues::where('sub_attri_id','=',$field_attri_info->id)->get();
                                          foreach ($get_attri_values as $key => $attri) {
                                            //dd($attri);
                $rows .= '<option value="'.$attri->id.'"'.($attri->id == $field->attri_value_id ? 'selected':'').'>'.$attri->name.'</option>';                  
                                          }                      
                $rows .= '</select>'.
                                      '</div>'.
                                    '<input type="hidden" name="dropdown_field_id[]" value="'.$field_attri_info->id.'">'.
                                    '<input type="hidden" name="attribute_dropdown_id[]" value="'.$field->id.'">'.
                                    '</div>';
            }           
          }
        $get_textbox = CustomAttributesForTextbox::where('ad_id','=',$ads_id)->where('status','=','1')->get();
           foreach ($get_textbox as $key => $field) {
              $get_attri_info = SubAttributes::where('id','=',$field->attri_id)->get();
               foreach ($get_attri_info as $key => $field_attri_info) {

                $rows .= '<div class="form-group">'.
                                        '<label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'.$field_attri_info->name.'</label>'.
                                          '<div class="col-sm-9">';                  
                $rows .= '<input type="text" class="form-control borderZero" name="text_field[]" value='.$field->attri_value.'>';                  
                                 
                $rows .= '</div>'.
                                      '<input type="hidden" name="text_field_id[]" value="'.$field_attri_info->id.'">'.
                                      '<input type="hidden" name="attribute_textbox_id[]" value="'.$field->id.'">'.
                                  '</div>'; 
               }           
          }
    return Response::json($rows);


      }

        public function getCustomAttributes(){
       $input = Input::all();
       $ads_id = Request::input('ads_id');
       $cat_id = Request::input('cat_id');
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$cat_id)->get();
                     $rows = '';
          $i = 1;          
           foreach ($get_sub_attri_id as $key => $field) {
                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'; 
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="'.$field->name.'">
                      <option value="" class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">'; 
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9">'; 
                      $rows .= '<input type="text" name="text_field[]" class="form-control borderZero">';
                      $rows .='</div></div>';
                      $rows .='<input type="hidden" name="text_field_id[]" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group"><label for="row-name" class="col-sm-3 col-xs-3 control-label font-color">';
                      $rows .= $field->name.'</label>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" id="row-custom_attr'.$i++.'" name="dropdown_field[]"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .='</select></div>';
                      $rows .='<input type="hidden" name="dropdown_field_id[]" value="'.$field->id.'"></div>';
                 }           
          }

      return Response::json($rows);
    }
}