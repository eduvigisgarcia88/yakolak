<?php 
namespace App\Http\Controllers;


use App\Http\Requests;
use App\AdvertisementPhoto;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Response;
use App\Advertisement;
use App\CustomAttributes;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\PromotionPopups;
use Request;
use App\Category;
// use App\AdvertisementWatchlist;
use App\AdvertisementWatchlists;
use App\Country;
use App\City;
use App\Usertypes;
use Excel;
use Response;
use Auth;
use Redirect;
use Input;
use Carbon\Carbon;
use App\UserMessages;
use App\UserMessagesHeader;
use App\UserNotificationsHeader;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
     $row  = Advertisement::orderBy('id', 'desc')->first();

      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')                                       
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get(); 

  		$this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.id as get_ads_id', 'advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.status', '!=', '4')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 
      $this->data['adslatestbids_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id', 'advertisements.user_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')  
                                                   ->where('advertisements.status', '!=', '4')                                                                           
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 

      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                           
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 


       $this->data['latestads_panel2'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.user_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')   
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')->skip(8)->take(8)->get();                                        

//dd( $this->data['latestads2']);
       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
		  $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
		  $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
		  //$this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
      
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      // $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',1)->get();
      // $this->data['bid'] = Category::where('biddable_status','=',1)->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
     
      return view('ads.main', $this->data);
	}

public function editor()
{
  return View::make('admin.ads-management.front-end-users.ckfinder.index');	
  return view('plugins/ckfinder');	
}


  public function addWatchlist() {
     $row = AdvertisementWatchlists::find(Request::input('id'));
     $id = Request::input('id');
      if(is_null($row)){
        $row = new AdvertisementWatchlists;
        $row->user_id = Auth::user()->id;
        $row->ads_id = $id;
        $row->save();
        return Response::json(['body' => 'Ad Successfully added to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }

  public function removeWatchlist() {
     $input = Input::all();
     $id = array_get($input, 'watchlist_id');
     $row = AdvertisementWatchlists::find($id);

      if(!(is_null($row))){
        $row->status = 2;
        $row->save();

        return Response::json(['body' => 'Ad Successfully removed to Watchlist']);      
      }else{
        return Response::json(['error' => ['error on finding the ad']]); 
      }
  }


  public function getCity() {

        $get_city = City::where('country_id', '=', Request::input('id'))->orderBy('name','asc')->get();

        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_city as $key => $field) {
          $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
        }
        return Response::json($rows);

     }

  public function getCustomAttributes(){

       $id = 1;
       $get_cust_id= CustomAttributes::find($id);
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$get_cust_id->id)->get();
       $rows = '<label>'; 
           foreach ($get_sub_attri_id as $key => $field) {
                      $rows .= $field->name.'</label>';
                      $rows .= '<option class="hide">Select:</option>'; 
                    $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                       $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }     
          }

//dd($get_sub_attri_id);
                            
        return Response::json($rows);

    }
   



}
