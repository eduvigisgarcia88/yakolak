<?php

namespace App\Http\Controllers;


use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\UserPlanTypes;
use App\Http\Controllers\Controller;
use Form;
use Hash;
use Validator;
use Redirect;
use Url;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Advertisement;
use App\Category;
use App\Country;
use App\City;
use App\AdvertisementPhoto;
use App\AdvertisementWatchlists;
use Image;
use Mail;
use App\CompanyBranch;
use Carbon\Carbon;
use App\UserRating;
use App\AdRating;
use App\UserComments;
use App\UserCommentReplies;
use App\UserAdComments;
use App\UserPlanRates;
use App\UserAdCommentReplies;
use App\UserMessages;
use App\UserMessagesHeader;
use App\Newsletters;
use App\NewslettersUserCounters;
use App\Languages;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use App\AuctionBidders;
use App\UserNotificationsHeader;


class FrontEndUserController extends Controller
{
  public function index(){
      $this->data['title'] = "Dashboard"; 
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      return view('user.view', $this->data);
  
  }

  public function doFetchInfo($id){
    
    $rows = User::with("getUserCompanyBranch")->find($id);
    $get_all_rate = UserRating::where('vendor_id','=',$id)->avg('rate');

      if(!is_null($get_all_rate)){
        $get_average_rate = $get_all_rate;
      }else{
        $get_average_rate = 0;
      }
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $get_countries = Country::all();
    // $get_cities = City::all();
    // dd($rows);
    $branch = "";
    if (count($rows->getUserCompanyBranch) > 0) {
      $ctr = 0;      
      foreach ($rows->getUserCompanyBranch as $key => $field) {
         if($field->status == 1){
         $branch .= '<div id ="accountBranch"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 borderbottomLight normalText noPadding bottomPadding topMarginB bottomMarginB redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText">' . ($ctr == 0 ? '<button type="button" value="" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus redText"></i> ADD BRANCH</button>' : '<button type="button" value="" class="hide-view btn-del-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-minus redText"></i> REMOVE</button>') . '</span>' .
            '</div>' .
            '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="'.$field->company_tel_no.'" class="form-control borderZero inputBox fullSize" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as $gckey => $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '" ' . ($field->company_country == $gcfield->id ? 'selected' : '') . '>' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="'.$field->company_address.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="'.$field->company_office_hours.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="'.$field->company_email.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="'.$field->company_mobile.'" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';

                    $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get()->take(50);
                      // dd($get_cities);
                    foreach ($get_cities as $gkey => $gfield) {
                      
                      $branch .= '<option value="' . $gfield->id . '" ' . ($field->company_city == $gfield->id ? 'selected' : '') .'>' . $gfield->name . '</option>';

                    }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="'.$field->company_website.'" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id" value="'.$field->id.'">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status" value="'.$field->status.'">'.
          '</div></div>';

          $ctr += 1;
      }
    }
    }
    else {
      $branch .= '<div id ="accountBranch"><div class="col-lg-12  borderbottomLight normalText noPadding bottomPadding topMarginB bottomMargin redText">' .
              'BRANCH INFORMATION' .
              '<span class="pull-right normalText"><button type="button" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus redText"></i> ADD BRANCH</button></span>' .
            '</div>' .
            '<div class="col-lg-6 ">' .
             ' <div class="form-group bottomMargin">'.
               ' <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>'. 
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<input name="company_tel_no[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                 ' </div>'.
                '</div>'.
             '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                    '<select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';
                    foreach ($get_countries as  $gcfield) {
                      $branch .= '<option value="' . $gcfield->id . '">' . $gcfield->countryName . '</option>';
                    }
                $branch .= '</select>' .
                 ' </div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_address[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
             '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>'.
                '<div class="inputGroupContainer">'.
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                        '<input name="company_office_hours[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
          '</div>'.
         ' <div class="col-lg-6 ">'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_email[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
              '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label> ' .
                '<div class="inputGroupContainer">'.
                 ' <div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
                       ' <input name="company_mobile[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
                  '</div>'.
                '</div>'.
              '</div>'.
               '<div class="form-group bottomMargin">'.
                '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>'.
               ' <div class="inputGroupContainer">'. 
                  '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'. 
                    '<select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">' .
                    '<option class="hide">Select</option>';

                    // $get_cities = City::where('country_id', $field->company_country)->orderBy('name', 'asc')->get();
                    //   // dd($get_cities);
                    // foreach ($get_cities as $gkey => $gfield) {
                      
                    //   $branch .= '<option value="' . $gfield->id . '">' . $gfield->name . '</option>';

                    // }

                 $branch .= '</select></div>'.
                '</div>'.
              '</div>'.
              // '<div class="form-group bottomMargin">'.
              //   '<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> '.
              //   '<div class="inputGroupContainer">'.
              //     '<div class="input-group col-md-8 col-sm-12 col-xs-12 ">'.
              //           '<input name="company_website[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">'.
              //     '</div>'.
              //   '</div>'.
              // '</div>'.
              '<input type="hidden" name="company_branch_id[]" id="branch_id">'.
              '<input type="hidden" name="company_branch_status[]" id="branch_status">' .
          '</div></div>';
    }
    $branches_raw = "";
      if($rows->usertype_id == 2){

      if (count($rows->getUserCompanyBranch) > 0) {
        $ctr = 0;      
        foreach ($rows->getUserCompanyBranch as $key => $field) {
           if($field->status == 1){
              $get_public_country = Country::where('id','=',$field->company_country)->first(); 
              $get_public_city = City::where('id','=',$field->company_city)->first();
              //dd($get_public_city);
              $branches_raw .= '<div class="container-fluid topMarginB borderbottomLight"><div class="container"><div class="col-md-12 col-sm-12 col-xs-12 noPadding">'.
                          '<div class="col-md-12 col-sm-12 col-xs-12 panelTitle">'.$get_public_country->countryName.' BRANCH INFORMATION<a class="btn pull-right grayText normalText" data-toggle="collapse" data-target="#'.$field->id.'"><i class="fa fa-angle-down"></i></a></div>'.
                          '<div id="'.$field->id.'" class="col-md-12 col-sm-12 col-xs-12 noPadding collapse">'.
                            '<div class="col-md-3 col-sm-5 col-xs-12">'.
                            '<iframe width="100%" height="206px"frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDuGKZMGH7Fo4H8SWyvNJWYelH5brT4bTg&q='.$get_public_city->name.'+'.$get_public_country->countryName.'" allowfullscreen></iframe>'.
                            '</div>'.
                            '<div class="col-md-9 col-sm-7 col-xs-12">'.
                              '<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText">'.$field->company_address.'('.$get_public_city->name.','.$get_public_country->countryName.')</span></div>'.
                              '<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText">'.$field->company_mobile.'/'.$field->company_tel_no.'</span></div>'.
                              '<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText">'.$field->company_email.'</span></div>'.
                              '<div class="mediumText grayText"><strong>Office Hours:</strong><span>'.$field->company_office_hours.'</span></div>'.
                            '</div>'.
                            '</div>'.
                            '</div>'.
                          '</div></div>';

            $ctr += 1;
           }
      }
    }else{
         $branches_raw .= '<div class="container topMarginB"><div class="col-md-12 col-sm-12 col-xs-12 noPadding">'.
                          '</div>';
    }
  }else{
    $branches_raw = "";
  }
       $form="";

   if(Auth::check()){

          $user_id = Auth::user()->id;

          $check_rates = UserRating::where('vendor_id','=',$id)->where('user_id','=',$user_id)->first();

           $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body noPadding">'.
                      '<div class="commentBlock">'.
                        '<div class="commenterImage">'.
                           '<img src="'.url('uploads').'/'.Auth::user()->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                          '</div>'.
                          '<div class="commentText">'.
                          Form::open(array('url' => 'user/comment', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment'))
                          .'<p class=""><textarea class="form-control borderZero fullSize" rows="2" id="ad-comment" name="comment"></textarea></p>'.
                              '<input type="hidden" name="user_id" value="'.$id.'">'.
                              '<input type="hidden" name="vendor_id" value="'.Auth::user()->id.'">'.
                          '</div>'.
                          '<span class="pull-right" id="review-product-pane">'.
                            '<span id="rateYo" class="mediumText adRating pull-left '.( Auth::user()->id  == $id ? "hide" : "").''.( is_null($check_rates) ? '' : 'hide').'"></span>'.
                            '<button type="button" class="btn btn-sm rateButton borderZero btn-rate_product '.( Auth::user()->id  == $id ? 'hide' : '').''.( $check_rates  != null ? 'hide' : '').'"><i class="fa fa-star"></i> Rate Product</button>'.
                            '<button type="submit" class="btn btn-sm blueButton borderZero btn-save_comment leftMargin"><i class="fa fa-comments"></i> Leave Comment</button>'.
                          '</span>'.
                           Form::close().
                      '</div>'.
                    '</div>'.
                  '</div>'.
               '</div>';
      }else{
          $user_id = null;
          
              $form .= '<div class="col-md-12 noPadding">'.
                  '<div class="panel panel-default borderZero removeBorder noMargin">'.
                   '<div class="panel-body noPadding">'.
                   '<p><div class="alert alert-info borderZero">You must be logged in to post a comment. <a class="btn btn-info borderZero" data-toggle="modal" data-target="#modal-login">Login</a> , <a class="btn btn-info borderZero" data-toggle="modal" data-target="#modal-register">Register</a> or <a class="btn btn-info borderZero" style="background-color:#3B5998;" href="#"><i class="fa fa-facebook"></i> Login with Facebook</a></div>
                   </p>'.
                   '</div>'.
                   '</div>'.
                   '</div>';
      }

   
    $stars ="";
    // $stars_reply = "";
    $comment_info= User::with('getUserComments')->find($id);
    $comments ="";
    $ctr = 1;

    if (count($comment_info->getUserComments) > 0) {
      foreach ($comment_info->getUserComments as $key => $field) {

                    $get_vendor_info = User::find($field->vendor_id);  
                    $get_user_rating = UserRating::where('user_id','=',$field->vendor_id)->where('vendor_id','=',$id)->first();
            
                        if(!is_null($get_user_rating)){
                            if($get_user_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars">
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                      </span>';
                            }

                            if($get_user_rating->rate == 4){
                              $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';   
                            }

                            if($get_user_rating->rate == 3){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_user_rating->rate == 2){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-sta rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_user_rating->rate == 1){
                             $stars = '<span class="mediumText ">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_user_rating->rate == null){
                              $stars = "";
                            }
                          }else{
                            $stars = "";
                          }
                    
       $comments .= '<div id="reply-pane" class="'.($ctr % 2  == 0 ? 'panel panel-default bgGray nobottomMargin borderZero borderBottom removeBorder' : 'panel panel-default bgWhite borderZero removeBorder').' ">'.
          '<div class="panel-body nobottomPadding" id="replies-container">'.
            '<div class="commentBlock">'.
              '<div class="commenterImage">'.
                 '<img src="'.url('uploads').'/'.$get_vendor_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                '</div>'.
                '<div class="commentText">'.
                '<div class="panelTitle bottomPadding">'.$get_vendor_info->name.' '.$stars.'</div>'.
                '<p class="normalText">'.$field->comment.'</p>'.
                '<div class="subText">'.
                '<a '.($get_vendor_info->id == $user_id ? '': 'data-name='.$get_vendor_info->name ).' id="show" role="button" class="blueText normalText '.($field->vendor_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-comments"></i> Reply</a>'.
                  // '<span class="blueText normalText "><i class="fa fa-comments"></i> Reply</span>'.
                  '<span class="redText normalText leftPaddingB '.($field->vendor_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>'.
                  '<span class="lightgrayText  topPadding normalText pull-right">'.date("F j, Y", strtotime($field->created_at)).'</span>'.
                '</div>';
                 $get_replies = UserCommentReplies::where('comment_id','=',$field->id)->orderBy('created_at','desc')->get();

                foreach ($get_replies  as $key => $reply_info) {
                    $get_user_info = User::where('id','=',$reply_info->user_id)->first();
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomMargin borderZero removeBorder replyBlock bordertopLight topMarginB">
                      <div class="panel-body norightPadding nobottomPadding">
                        <div class="commentBlock">
                          <div class="commenterImage">
                             <img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                            </div>
                            <div class="commentText">
                            <div class="panelTitle bottomPadding">'.$get_user_info->name.'</div>
                            <p class="normalText">'.$reply_info->content.'</p>
                            <div class="subText">
                              <a '.($get_user_info->id == $user_id ? '': 'data-name='.$get_user_info->name ).' id="show_reply_box" role="button" class="blueText normalText '.($user_id == null ? 'hide':'').' "><i class="fa fa-comments"></i> Reply</a>
                              <span class="redText normalText leftPadding '.($reply_info->user_id == $user_id ? 'hide':'').' '.($user_id == null ? 'hide':'').'"><i class="fa fa-paper-plane"></i> Direct Message</span>
                              <span class="lightgrayText  topPadding normalText pull-right">'.date("F j, Y", strtotime($reply_info->created_at)).'</span>
                            </div>';
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomMargin borderZero removeBorder replyBlock bordertopLight topMarginB inputReplyBox hidden">'.
                                  '<div class="panel-body norightPadding nobottomPadding">'.
                                    '<div class="commentBlock">'.
                                    Form::open(array('url' => 'user/comment/reply', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment_reply')).
                                    '<textarea class="form-control borderZero fullSize" rows="2" id="reply-message" name="reply_content"></textarea>'.
                                    '<input type="hidden" name="comment_id" value="'.$field->id.'">'.
                                    '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                                    '</div>'.
                                    '<span class="pull-right">'.
                                      '<button class="btn normalText blueButton submitReply borderZero topMargin"> Submit</button>'.
                                    '</span>'.
                                    Form::close().
                                  '</div>'.
                              '</div>                       
                            </div>
                        </div>
                      </div>
                  </div>';                          

                }
     
      $comments .= '<div class="panel panel-default '.($ctr % 2  == 0 ? 'bgGray':'bgWhite').' nobottomMargin borderZero removeBorder replyBlock bordertopLight topMarginB inputReply hidden">'.
                  '<div class="panel-body norightPadding nobottomPadding '.($field->vendor_id == $user_id  ? 'hide':'').'">'.
                    '<div class="commentBlock">'.
                    Form::open(array('url' => 'user/comment/reply', 'role' => 'form', 'class' => 'form-horizontal','id'=>'modal-save_comment_reply')).
                    '<textarea class="form-control borderZero fullSize" rows="2" id="main_comment_reply" name="reply_content"></textarea>'.
                    '<input type="hidden" name="comment_id" value="'.$field->id.'">'.
                    '<input type="hidden" name="user_id" value="'.$user_id.'">'.
                    '</div>'.
                    '<span class="pull-right">'.
                      '<button class="btn normalText blueButton submitReply borderZero topMargin parentSubmit"> Submit</button>'.
                    '</span>'.
                    Form::close().
                  '</div>'.
              '</div>'.
                '</div>'.
            '</div>'.
          '</div>'.
        '</div>';
          $ctr++;

      }

    }else{
      $comments .= '';
    }
     
    $get_user_messages = UserMessagesHeader::where('reciever_id','=',$user_id)->orderBy('created_at','desc')->get();
   
      //dd($get_user_messages );
      if(count($get_user_messages)>0){
              $messages_header ='';
              foreach ($get_user_messages as $key => $field) 
                  {    
                 $messages_header .=  '<tr data-header-id="'.$field->id.'" class="view-message"><a href="#" role="button"><td><i class="'.($field->read_status == 1 ? 'fa fa-envelope':'fa fa-envelope-o').' grayTextB rightPadding">&nbsp;&nbsp;'.$field->title.'</i></td>
                             <td  class="text-right smallText"><span class="rightPadding lightgrayText">'.date("m/d/y g:i A", strtotime($field->created_at)).'</span><span class="lightgrayText"><i class="fa fa-trash"></i></span></td></a>
                          </tr>';
              }
      }else{
          $messages_header = '';
      }
     


   if(Auth::check() != false){   

            $get_advertisement_info = User::with('getUserAdvertisements')
                                          ->with('getUserAdvertisements.getAdvertisementComments')->find($user_id);

                  $ctr=1;                          
                    $review_summary = "";
                      if(count($get_advertisement_info->getUserAdvertisements) > 0){

                          foreach ($get_advertisement_info->getUserAdvertisements as $key => $field) {

                             if(count($field->getAdvertisementComments) > 0){

                                  foreach ($field->getAdvertisementComments as $key => $value) {
                                    
                                if($value->vendor_id != $user_id){   
                           $review_summary .= '<div class="col-sm-12 '.($ctr % 2  == 0 ? 'bgWhite bottomPaddingB':'bgGray bottomPaddingB borderBottom').'">
                                              <div class="panelTitle bottomPaddingB">'.$field->title.'</div>';

                            $get_advertisement_comments =  UserAdComments::where('ad_id','=',$field->id)->get();

                              foreach ($get_advertisement_comments as $key => $field_info) {
                                          $get_user_info = User::where('id','=',$field_info->vendor_id)->first();
                                          $get_ad_rating = AdRating::where('user_id','=',$field_info->vendor_id)->where('ad_id','=',$field->id)->first();
                        if(!is_null($get_ad_rating)){
                            if($get_ad_rating->rate == 5){
                              $stars = '<span class="mediumText rateStars">
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                          <i class="fa fa-star"></i>
                                      </span>';
                            }

                            if($get_ad_rating->rate == 4){
                              $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';   
                            }

                            if($get_ad_rating->rate == 3){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_ad_rating->rate == 2){
                             $stars = '<span class="mediumText">
                                          <i class="fa fa-sta rateStars"></i>
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                            if($get_ad_rating->rate == 1){
                             $stars = '<span class="mediumText ">
                                          <i class="fa fa-star rateStars"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                          <i class="fa fa-star rateStarsB"></i>
                                      </span>';  
                            }
                          }else{
                            $stars = "";
                          }                   
                                 $review_summary .= '<div class="commentBlock">
                                                    <div class="commenterImage">
                                                       <img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                                                      </div>
                                                      <div class="commentText">
                                                      <div class="panelTitleB bottomPadding">'.$get_user_info->name.''.$stars.'</div>
                                                      <p class="normalText">'.$field_info->comment.'</p>
                                                      <div class="subText">
                                                        <a id="show_reply_box" href= "'.url('ads/view').'/'.$field->id.'" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>
                                                        <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                                                        <span class="lightgrayText  topPadding normalText pull-right">'.$field_info->created_at.'</span>
                                                      </div>
                                                      </div>
                                                  </div>';
                           }           
                           $review_summary .= '</div>';
                           $ctr++;
                          }else{
                            $review_summary .= '';
                          }  
                          }
                          }
                          else{
                            $review_summary .= '';
                          } 
                          
                          }

               }else{
                  $review_summary = "";
                }
          }else{
            $review_summary = "";
          }

 


//SUBSCRIPTION
if(Auth::check()){
    $token = Session::token();

    $user_id = Auth::user()->id;
    $usertype_id = Auth::user()->usertype_id;
    $user_plan_types = Auth::user()->user_plan_types_id;
    $get_plan = UserPlanTypes::where('usertype_id', '=', $usertype_id)->where('plan_status', '=', 1)->where('id', '!=', '1')->where('id', '!=', '5')->where('id','!=', $user_plan_types)->where('status', '=', 1)->get();         
    $get_current_plan = UserPlanTypes::find($user_plan_types);         
    $created = new Carbon(Auth::user()->created_at);
    $now = Carbon::now();
    $difference = ($created->diff($now)->days < 1)
    ? 'today'
    : $created->diffForHumans($now);
    //check if days not months
   
    $arr = explode(' ',trim($difference));
    
    if ($arr == 'today') {
      $diff = '0';
    } else {
           if ($arr == 'days') {
           $diff = '0';
           $test = 'true';
          } elseif ($arr == 'years' || $arr == 'year') {
           $diff = '12';
          }
          else {
           $diff = substr($difference, 0, 1); 
          }
    }


$get_plan_rate = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->first();

$duration = $get_plan_rate->duration;
$discount = $get_plan_rate->discount;

$plan = "";
$plan .=' <div id="" class="col-md-12 noPadding">
                          <div><div class=" xxlargeText"><strong><span class="blueText">'.strtoupper($get_current_plan->plan_name).'</span> <span class="lightgrayText"> ZERO COST FOREVER</span></strong> </div>
                           <div>
                           <div class="normalText grayText bottomPaddingC">
                              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue leo eu erat elementum placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam at dui dui. Nunc rhoncus tellus eu neque ultrices molestie. Vivamus eget rutrum leo. Nulla dapibus ligula quis massa faucibus sollicitudin sed ac purus. Maecenas egestas rhoncus nunc, sit amet vestibulum lectus iaculis nec. Pellentesque at ipsum id odio suscipit commodo a a dui. Nam turpis nibh, imperdiet sed urna quis, auctor maximus quam. Cras scelerisque metus tempus ligula gravida blandit. Praesent dictum tempor quam varius laoreet. Morbi quis libero ut erat egestas lacinia laoreet nec mauris. Ut nec imperdiet lectus, at ullamcorper orci. Aenean ultrices iaculis augue at tempor.
                            </div>
                            <div class="col-lg-12 noPadding">
                            <div class="col-lg-4 noleftPadding">
                            <div class="normalText normalText grayText">
                              <span class=""> No. of Ads <span class="pull-right blueText bold">'.$get_current_plan->ads.'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> No. of Auctions <span class="pull-right blueText bold">'.$get_current_plan->auction.'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class="">  No. of Image per Ad <span class="pull-right blueText bold">'.$get_current_plan->img_per_ad.'</span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class="">  No. of Videos per Ad <span class="pull-right blueText bold">'.$get_current_plan->video_total.'</span></span>
                            </div>
                            </div>
                            <div class="col-lg-4">
                            <div class="normalText normalText grayText">
                              <span class=""> Total Free Points <span class="pull-right blueText bold">20 </span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Total Bids <span class="pull-right blueText bold">20 </span></span>
                            </div>
                            <div class="normalText normalText grayText">
                              <span class=""> Free SMS Notification <span class="pull-right blueText bold">200 </span></span>
                            </div>
                            </div>
                            </div>
                          </div>
                          </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" id="subscription-upgrade">
                      <hr class="nobottomMargin">
                     '; 


foreach ($get_plan as  $value) {
if(Auth::check()){
        if ($diff <= $duration || $diff == 0) {
           $inti_discount  = 100 - $discount;
           $total_discount = '.'.$inti_discount;

           $total_months12 = $value->months12_price * $total_discount;
           $total_months6  = $value->months6_price  * $total_discount;
           $test = 'test1';

} else {
   $get_plan_rate2 = UserPlanRates::where('usertype_id', '=', $usertype_id)->where('rate_status', '=', 1)->orderBy('id', 'desc')->first();
       if ($get_plan_rate2) {
       $duration2 = $get_plan_rate2->duration + $duration;
       $discount2 = $get_plan_rate2->discount;
                    if ($diff <= $duration2 && $diff > $duration) {
                        $inti_discount2  = 100 - $discount2;
                        $total_discount2 = '.'.$inti_discount2;

                        $total_months12  = $value->months12_price * $total_discount2;
                        $total_months6   = $value->months6_price  * $total_discount2;
                   $test = 'test2';
                    } else {
                        $total_months12  = $value->months12_price;
                        $total_months6   = $value->months6_price; 
                   $test = 'test3';
                    }
        }
  } 

} else{ 
  $total_months12 = $value->months12_price;
  $total_months6  = $value->months6_price; 
}

    
$plan .='<div class="col-md-'.(count($get_plan) == 4 ? '3':'4').'">
                        <div class="row panel panel-default borderZero nobottomMargin removeBorder">
                          <div class="panel-heading panelTitleBarLight rightMargin"><div class="grayText mediumText"><strong>'.strtoupper($value->plan_name).  '</strong></div></div>
                          <div class="panel-body">
                            <div class="bottomPaddingC text-center borderbottomLight">
                            '.($user_plan_types == $value->id ? '<div class="normalText" style="color:#2C9245; ">&rarr;&larr;</div>' : '<div class="normalText lightgrayText">60% OFF THE REGULAR $9.95 MO.</div> ').'                       
                                <div class="'.(count($get_plan) == 4 ? 'largeText':'xlargeText').' xxlargeText blueText"><strong>'.($value->id == 1 || $value->id == 5? 'FREE' : '$'.$total_months6.'/mo.').'</strong></div>                          
                            </div>
                            <div class="blockTop lineHeightC">
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Ads Remaining <span class="pull-right blueText bold">'.$get_listing_available.'</span>':'No. of Ads <span class="pull-right blueText bold">'.$value->ads.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types == $value->id ? 'No. of Auction Remaining <span class="pull-right blueText bold">'.$get_listing_available.'</span>':'No. of Auctions <span class="pull-right blueText bold">'.$value->auction.'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Image per Ad Remaining <span class="pull-right blueText bold">'.$get_upload_photos_available.'</span>':' No. of Image per Ad <span class="pull-right blueText bold">'.$value->img_per_ad .'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> '.($user_plan_types  == $value->id ? 'No. of Video per Ad Remaining <span class="pull-right blueText bold">'.$get_embed_videos_available.'</span>':' No. of Videos per Ad <span class="pull-right blueText bold">'.$value->video_total .'').'</span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Free Points <span class="pull-right blueText bold">'.$value->point.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Total Bids <span class="pull-right blueText bold">'.$value->bid.' </span></span>
                            </div>
                            <div class="'.(count($get_plan) == 4 ? 'smallText':'normalText').' normalText grayText">
                              <span class=""> Free SMS Notification <span class="pull-right blueText bold">'.$value->point_exchange.' </span></span>
                            </div>
                            <div class="blockTop text-center">';
        if (Auth::check()) {
     $plan .='<form action="payment" id="upgrade" method="post">
              <input type="hidden" name="_token" id="csrf-token" value="'.$token.'" />
              <input type="hidden" name="plan_id" value="'.$value->id.'" />';
  }  
    if (Auth::check()) {
            $plan .=($user_plan_types  == $value->id ? '<div class="normalText" style="color:#2C9245; ">&rarr;&larr;</div>' : '<button data-user_id="'.Auth::user()->id.'" data-plan="'.$value->id.'" data-id="'.$rows->id.'" data-title="'.$value->plan_name.'" class="btn btn-sm blueButton fullSize borderZero noMargin" type="submit">GET STARTED</button>');
    } else {
           $plan .='<button class="btn btn-sm blueButton fullSize borderZero noMargin btn-login" data-toggle="modal" data-target="#modal-login" type="submit">GET STARTED</button>';
    }

    $plan .='</form>
                 </div>
                     </div>
                          </div>
                          <div class="panel-footer bgWhite rightMargin borderZero borderBottom"><a href="#'.$value->plan_name.'"><span class="normalText redText">View Details<span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span></span></a></div>
                        </div>
                      </div> ';

  //COMPUTE POSTED ADS FOR REPORTS TAB
        $total_ad_posted = Advertisement::where(function($query) use($user_id){
                                             $query->where('status', '!=', '2')
                                                   ->where('user_id', '=', $user_id);                                      
                                        })->orderBy('created_at', 'desc')->get();

        $total_auction_posted = Advertisement::where(function($query) use($user_id){
                                             $query->where('status', '!=', '2')
                                                   ->where('ads_type_id', '=', '2')
                                                   ->where('user_id', '=', $user_id);                                      
                                        })->orderBy('created_at', 'desc')->get(); 
        $total_uploaded_photos = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                         ->where(function($query) use($user_id){
                                             $query->where('advertisements_photo.photo', '!=', 'null')
                                                    ->where('advertisements.status', '!=', '2')
                                                   ->where('advertisements.user_id', '=', $user_id);                                      
                                        })->get();

        $total_embed_video = Advertisement::where('user_id','=', $user_id)->where('youtube', '!=', 'null' )->get();
        $get_user_plan_types = UserPlanTypes::find($user_plan_types);
        $total_ads_allowed = $get_user_plan_types->total_ads_allowed;
        $auction = $get_user_plan_types->auction;
        $img_total =  $get_user_plan_types->img_total;
        $video_total = $get_user_plan_types->video_total;
  
          //compute listing available
                  $listing_available = $total_ads_allowed - count($total_ad_posted);
                 if ($listing_available <= 0) {
                   $get_listing_available = '0';
                 } else{
                  $get_listing_available = $listing_available;

                 }
         //compute photos available
               $upload_photos_available = $img_total - count($total_uploaded_photos);
               if ($upload_photos_available <= 0) {
                 $get_upload_photos_available = '0';
               } else{
                $get_upload_photos_available = $upload_photos_available;
               }

          //compute videos available
               $embed_videos_available = $video_total - count($total_embed_video);
               if ($embed_videos_available <= 0) {
                 $get_embed_videos_available = '0';
               } else{
                $get_embed_videos_available = $embed_videos_available;
               }
 
}

      } else {
        $user_id = null;
        $usertype_id = null;
        $get_plan = UserPlanTypes::where('usertype_id', '=', 1)->where('plan_status', '=', 1)->get();       
        $get_plan_rate = UserPlanRates::where('usertype_id', '=', 1)->where('rate_status', '=', 1)->first();
        $duration = null;
        $user_plan_types = '0';
        $plan = null;
        $total_ad_posted = null;
        $total_auction_posted = null;
        $total_uploaded_photos = null;
      }

  $row  = Advertisement::orderBy('id', 'desc')->first();

  $vendoradsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->select('advertisements.title', 'advertisements.status', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'countries.countryName')                          
                                         ->where(function($query)use($id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $id)    
                                                     ->where('advertisements.status', '!=', 2);                                    
                                          })->orderBy('advertisements_photo.created_at', 'desc')->take(6)->get(); 

   $featuredads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $id)                                                                           
                                                     ->where('advertisements.feature', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();                                        



  $vendorbidsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                        ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                        ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                        ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                        ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                        ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                        ->selectRaw('avg(rate) as average_rate')
                                        ->selectRaw('count(rate) as comments_total')
                                        ->where(function($query) use($id){
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.status', '=', '1')
                                                     ->where('advertisements.ads_type_id', '=', '2')
                                                     ->where('advertisements.user_id', '=', $id);                                       
                                          })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

    $vendorbids = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                      ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                      ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                      ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                      ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                      ->select('advertisements.ad_expiration', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '2')
                                                   ->where('advertisements.user_id', '=', $id);                                       
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements.created_at', 'desc')->take(3)->get();   

   $featured_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })
                                       ->groupBy('advertisements.id')
                                       ->orderBy('advertisements_photo.created_at', 'desc')
                                       ->take(4)->get(); 
    
   $latestadsmobile = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->leftjoin('user_ad_comments','user_ad_comments.ad_id','=','advertisements.id')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->selectRaw('count(rate) as comments_total')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $id)
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                      ->groupBy('advertisements.id')
                                      ->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
                                      
    $lastestads = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->select('advertisements.category_id', 'advertisements.sub_category_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.user_id', '=', $id) 
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })
                                        ->groupBy('advertisements.id')
                                        ->orderBy('advertisements_photo.created_at', 'desc')->take(6)->get(); 
                                  
    $watchlistads = AdvertisementWatchlists::join('advertisements','advertisements.id', '=', 'advertisements_watchlists.ads_id')
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftJoin('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city', 'advertisements_watchlists.id as watchlist_id')                          
                                       ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements_watchlists.user_id', '=', $id) 
                                                   ->where('advertisements_watchlists.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->get();


     $related = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                      ->select('advertisements.category_id', 'advertisements.sub_category_id', 'advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city','ad_ratings.rate')                          
                                      ->selectRaw('avg(rate) as average_rate')
                                      ->where(function($query) use ($id){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        }) ->groupBy('advertisements.id')
                                           ->orderBy('advertisements_photo.created_at', 'desc')->first(); 

     $category_id = $related->category_id;

     $related_ads_mobile  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id','ad_ratings.rate')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                            
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(1)->take(4)->get();

      $related_ads  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                         
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();

      $related_adset2  = Advertisement::with('getAdvertisementComments')->join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'advertisements.country_id')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'advertisements.city_id')
                                       ->leftjoin('ad_ratings','ad_ratings.ad_id','=','advertisements.id')
                                       ->selectRaw('avg(rate) as average_rate')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id', 'advertisements.sub_category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city','ad_ratings.rate')                          
                                       ->where(function($query) use ($category_id) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.category_id', '=', $category_id)                                                                                                         
                                                   ->where('advertisements.status', '=', '1');                                     
                                        })->orderBy('advertisements_photo.created_at', 'desc')->skip(4)->take(4)->get(); 


      $user_banner  = BannerSubscriptions::join('users', 'banner_subscriptions.user_id', '=', 'users.id') 
                                       ->join('banner_placement_plans','banner_placement_plans.id', '=', 'banner_subscriptions.placement_plan')
                                       ->join('languages','languages.id', '=', 'banner_subscriptions.language_id')
                                       ->select('users.*', 'languages.*', 'banner_placement_plans.*', 'banner_subscriptions.*')                          
                                       ->where(function($query) use ($user_id) {
                                             $query->where('banner_subscriptions.status', '=', '1')
                                                   ->where('banner_subscriptions.user_id', '=', $user_id);                                                                                                               
                                        })->orderBy('banner_subscriptions.created_at', 'desc')->get(); 

    if (Request::ajax()) {
      
        $result['vendoradsmobile']      = $vendoradsmobile->toArray();
        $result['featuredads']          = $featuredads->toArray();
        $result['vendorbidsmobile']     = $vendorbidsmobile->toArray();
        $result['vendorbids']           = $vendorbids->toArray();
        $result['featured_ads']         = $featured_ads->toArray();
        $result['latestadsmobile']      = $latestadsmobile->toArray();
        $result['lastestads']           = $lastestads->toArray();
        $result['watchlistads']         = $watchlistads->toArray();
        $result['rows']                 = $rows->toArray();
        $result['related_ads_mobile']   = $related_ads_mobile->toArray();
        $result['related_ads']          = $related_ads->toArray();
        $result['related_adset2']       = $related_adset2->toArray();
        $result['total_ad_posted']      = $total_ad_posted->toArray();
        $result['total_auction_posted'] = $total_auction_posted->toArray();




        return Response::json($result);
    } else {

        $result['vendoradsmobile']     = $vendoradsmobile;
        $result['featuredads']         = $featuredads;
        $result['vendorbids']          = $vendorbids;
        $result['featured_ads']        = $featured_ads;
        $result['latestadsmobile']     = $latestadsmobile;
        $result['vendorbidsmobile']    = $vendorbidsmobile;
        $result['lastestads']          = $lastestads;
        $result['watchlistads']        = $watchlistads;
        $result['rows']                = $rows;
        $result['branches']            = $branch;
        $result['branches_all']        = $branches_raw;
        $result['plan']                = $plan;
        $result['comments']            = $comments;
        $result['form']                = $form;
        $result['messages_header']     = $messages_header;
        $result['get_average_rate']    = $get_average_rate;
        $result['related_ads_mobile']  = $related_ads_mobile;
        $result['related_ads']         = $related_ads;
        $result['related_adset2']      = $related_adset2;
        $result['review_summary']      = $review_summary;
        $result['total_ad_posted']      = $total_ad_posted;
        $result['total_auction_posted'] = $total_auction_posted;
        $result['total_uploaded_photos'] = $total_uploaded_photos;
        $result['user_banner'] = $user_banner;



        return $result;
     }
    

    }
    
    public function fetchInfo() {

      $id = Auth::user()->id;

      $result = $this->doFetchInfo($id);
  
      $this->data['user_id']           = $id;
      $this->data['rows']              = $result['rows'];
      $this->data['watchlistads']      = $result['watchlistads'];
      $this->data['form']              = $result['form'];
      if(Auth::check()){
         $this->data['review_summary'] = $result['review_summary'];
       }else{
         $this->data['review_summary'] = "";
       } 
      $this->data['vendorbidsmobile']   = $result['vendorbidsmobile'];
      $this->data['vendoradsmobile']   = $result['vendoradsmobile'];
      $this->data['featuredads']       = $result['featuredads'];
      $this->data['branches']          = $result['branches'];
      $this->data['messages_header']   = $result['messages_header'];
      $this->data['comments']          = $result['comments'];
      $this->data['plan']              = $result['plan'];
      $this->data['user_banner']              = $result['user_banner'];
      //dd($this->data['user_banner']);
      $this->data['total_ad_posted']   = $result['total_ad_posted'];
      $this->data['total_auction_posted']   = $result['total_auction_posted'];
      $this->data['total_uploaded_photos'] = $result['total_uploaded_photos'];
      $this->data['buy_and_sell']      = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable']          = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories']        = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get(); 
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['countries']         = Country::all();
      $this->data['languages']         = Languages::all();
      $this->data['usertypes']         = Usertypes::all();
      $this->data['banner_placement']  = BannerPlacement::all();


      return view('user.dashboard.index', $this->data);
    }

    public function fetchInfoPublic($id) {
    
    $result = $this->doFetchInfo($id);
    
    if(Auth::check()){
      $user_id = Auth::user()->id;
    }else{
      $user_id=null;
    }
    $this->data['user_id'] = $id;
    $this->data['vendorbids']         = $result['vendorbids'];
    $this->data['featured_ads']       = $result['featured_ads'];
    $this->data['latestadsmobile']    = $result['latestadsmobile'];
    $this->data['lastestads']         = $result['lastestads'];
    $this->data['rows']               = $result['rows'];
    $this->data['comments']           = $result['comments'];
    $this->data['form']               = $result['form'];
    $this->data['get_average_rate']   = $result['get_average_rate'];
    $this->data['related_ads_mobile'] = $result['related_ads_mobile'];
    $this->data['vendorbidsmobile']   = $result['vendorbidsmobile'];
    $this->data['related_ads']        = $result['related_ads'];
    $this->data['related_adset2']     = $result['related_adset2'];
    $this->data['messages_header']    = $result['messages_header'];
    $this->data['branches_public'] = $result['branches_all'];
    $this->data['buy_and_sell']    = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable']        = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['categories']      = Category::where('buy_and_sell_status','!=',3)->where('biddable_status','=',3)->get(); 
    $this->data['countries']       = Country::all();
    $this->data['rating']          = UserRating::where('user_id','=',$user_id)->where('vendor_id','=',$id)->first();
    $this->data['usertypes']       = Usertypes::all();
    return view('user.vendor.list', $this->data);
    
    }


     public function registerSave() {

        $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'              => $new ? 'image|max:3000' : 'image|max:3000',
            'password'           => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
            'name'               => 'required|unique:users,name' . (!$new ? ',' . $row->id : ''),
            'email'              => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'mobile'             => 'numeric|unique:users,mobile'. (!$new ? ',' . $row->id : ''),
            'telephone'          => 'numeric',
            'date_of_birth'      => 'date',
            'company_start_date' => 'date',
        ];

        // field name overrides
        $names = [
            'email'           => 'email address',
            'telephone'       => 'telephone',
            'country'         => 'country',
            'city'            => 'city',
            'address'         => 'address',
            'office_hours'    => 'office hours',
            'website'         => 'website',
            'gender'          => 'gender',
            'date_of_birth'   => 'date of birth',
        ];

        $branch_array            = array();
        $company_tel_no          = Input::get('company_tel_no');
        $company_mobile          = Input::get('company_mobile');
        $company_country         = Input::get('company_country');
        $company_address         = Input::get('company_address');
        $company_city            = Input::get('company_city');
        $company_email           = Input::get('company_email');
        $company_office_hours    = Input::get('company_office_hours');
        $company_branch_id       = Input::get('company_branch_id');
        // $company_website         = Input::get('company_website');
        $company_branch_status   = Input::get('company_branch_status');
        // validate child rows
        if($company_tel_no) {
            foreach($company_tel_no as $key => $val) {
                if($company_tel_no[$key]) {
                    $subrules = array(
                        "company_tel_no.$key"         => 'required|numeric',
                        "company_mobile.$key"         => 'required|numeric',
                        "company_country.$key"        => 'required',
                        "company_city.$key"           => 'required',
                        "company_email.$key"          => 'required|email',
                        "company_address.$key"        => 'required',
                        "company_office_hours.$key"   => 'required',
                       
                    );
                    $subfn = array(
                        "company_tel_no.$key"       => 'company telephone no',
                        "company_mobile.$key"       => 'company mobile no',
                        "company_country.$key"      => 'company country',
                        "company_city.$key"         => 'company city',
                        "company_email.$key"        => 'company email address',
                        "company_address.$key"      => 'company address',
                        "company_office_hours.$key" => 'company office hours',
                        
                    );
                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $branch_array[] = $company_tel_no[$key];
                } else {
                    unset($company_branch_id[$key]);
                    unset($company_tel_no[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $confirmation_code = str_random(30);
        // create model if new
        if($new) {
       

            $row = new User;
            $row->name      = strip_tags(array_get($input, 'name'));
            $row->email     = array_get($input, 'email');
            $row->usertype_id     = array_get($input, 'usertype');
            if (array_get($input, 'usertype') == '2') {
              $row->user_plan_types_id = '5';
            } else {
              $row->user_plan_types_id = '1';
            }
            $row->mobile    = array_get($input, 'mobile');
            $row->username  = array_get($input, 'email');

            if(array_get($input, 'date_of_birth')){
               $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
             }else{
               $row->date_of_birth = null;
             }
            if(array_get($input, 'company_start_date')){
               $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
             }else{
               $row->company_start_date = null;
             }
            if(array_get($input, 'telephone')){
               $row->telephone  = array_get($input, 'telephone');
             }else{
               $row->telephone = null;
             }
             if(array_get($input, 'country')){
               $row->country  = array_get($input, 'country');
             }
             if(array_get($input, 'city')){
               $row->city  = array_get($input, 'city');
             }
             if(array_get($input, 'address')){
               $row->address  = array_get($input, 'address');
             }else{
               $row->address = null;
             }
             if(array_get($input, 'office_hours')){
               $row->office_hours  = array_get($input, 'office_hours');
             }else{
               $row->office_hours = null;
             }
             if(array_get($input, 'website')){
               $row->website  = array_get($input, 'website');
             }else{
               $row->website = null;
             }
             if(array_get($input, 'gender')){
                 $row->gender  = array_get($input, 'gender');
              }else{
                 $row->gender = null;
              }
              if(array_get($input, 'facebook_account')){
                 $row->facebook_account  = array_get($input, 'facebook_account');
              }else{
                 $row->facebook_account = null;
              }
               if(array_get($input, 'twitter_account')){
                 $row->twitter_account  = array_get($input, 'twitter_account');
              }else{
                 $row->twitter_account    = null;
              }
              if(array_get($input, 'instagram_account')){
                 $row->instagram_account  = array_get($input, 'instagram_account');
              }else{
                 $row->instagram_account = null;
              }
              if(array_get($input, 'youtube_account')){
                 $row->youtube_account  = array_get($input, 'youtube_account');
              }else{
                 $row->youtube_account   = null;
              }
            $row->confirmation_code = $confirmation_code;
            $row->status = 2;
        } else {

            $row->name        = strip_tags(array_get($input, 'name'));
            $row->email       = array_get($input, 'email');
            $row->usertype_id = array_get($input, 'usertype');
            $row->mobile      = array_get($input, 'mobile');
            $row->username    = array_get($input, 'email');
            if(array_get($input, 'date_of_birth')){
               $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
             }else{
               $row->date_of_birth = "null";
             }
            if(array_get($input, 'company_start_date')){
               $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
             }
            if(array_get($input, 'telephone')){
               $row->telephone  = array_get($input, 'telephone');
             }else{
               $row->telephone           = null;
             } 
             if(array_get($input, 'country')){
               $row->country  = array_get($input, 'country');
             }
             if(array_get($input, 'description')){
               $row->description  = array_get($input, 'description');
             }else{
               $row->description         = null;
             }
             if(array_get($input, 'city')){
               $row->city  = array_get($input, 'city');
             }
             if(array_get($input, 'address')){
               $row->address  = array_get($input, 'address');
             }else{
               $row->address             = null;
             }
             if(array_get($input, 'office_hours')){
               $row->office_hours  = array_get($input, 'office_hours');
             }else{
               $row->office_hours        = null;
             }
             if(array_get($input, 'website')){
               $row->website  = array_get($input, 'website');
             }else{
               $row->website             = null;
             }
             if(array_get($input, 'gender')){
                 $row->gender            = array_get($input, 'gender');
              }else{
                 $row->gender            = null;
              }
             if(array_get($input, 'facebook_account')){
                 $row->facebook_account  = array_get($input, 'facebook_account');
              }else{
                 $row->facebook_account  = null;
              }
               if(array_get($input, 'twitter_account')){
                 $row->twitter_account   = array_get($input, 'twitter_account');
              }else{
                 $row->twitter_account   = null;
              }
               if(array_get($input, 'instagram_account')){
                 $row->instagram_account = array_get($input, 'instagram_account');
              }else{
                 $row->instagram_account = null;
              }
              if(array_get($input, 'youtube_account')){
                 $row->youtube_account   = array_get($input, 'youtube_account');
              }else{
                 $row->youtube_account   = null;
              }
              if(array_get($input, 'linkedin_account')){
                 $row->linkedin_account  = array_get($input, 'linkedin_account');
              }else{
                 $row->linkedin_account  = null;
              }
              $row->save();
        }   

        // set type only if this isn't me
        if($new || ($new = false && $row->id = Auth::user()->id)) {
          $log_usertype_id = $row->usertype_id;
          $row->usertype_id = array_get($input, 'usertype');
        }

        // do not change password if old user and field is empty
        if(array_get($input, 'password')) {
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        if($branch_array) {
          foreach($branch_array as $key => $child) {
              if($company_branch_id[$key]) {
                  // this is an existing visa
                  $cinfo = CompanyBranch::find($company_branch_id[$key]);
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = $company_branch_status[$key];
            
              } else {
                  // this is a new visa
                  $cinfo = new CompanyBranch;
                  $cinfo->user_id = $row->id;
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = "1";
                 
              }
                  $cinfo->save();
            

              // add to visa ID array
              if(!$company_branch_id[$key]) {
                  $company_branch_id[] = $cinfo->id;
              }
          }
      }

        if (Input::file('photo')) {
            // Save the photo
            Image::make(Input::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        }else{
            if($new){
              $row->photo ='default_image.png';
              $row->save();
            }
        }



        $validate = User::find($row->id);

         //tagging
              $contact_name = $row->name ; 
              $user_name = $row->username;
              $user_email = $row->email;
              if ($row->telephone) {
                  $user_phone =  $row->telephone;
              } else {
                  $user_phone =  $row->mobile;
              }  
           $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="http://yakolak.xerolabs.co/register/verify/'.$confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Verify your email address
              </a>
          </td>';
    //register user email validation sent
        if($new){
             $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
            if ($newsletter) {
                      Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'confirmation_code' => $confirmation_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $validate) {
                           $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

                       });
                }
        }
      if($new){
          return Response::json(['body' => 'Account Created<br>Please check your email for verification']);
      }else{
          return Response::json(['body' => ['Account Updated']]);
      }
    }

    public function register() {

      $this->data['title'] = "Register";
      $this->data['adslatestbids'] = Advertisement::latest()->take(4)->get(); 
      $this->data['adslatestbids2'] = Advertisement::latest()->skip(4)->take(4)->get();             
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['ads2'] = Advertisement::where(function($query) {
                            $query->where('ads_type_id', '=', '1');    
                        })->take(8)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all(); 

      return view('user.register', $this->data);
       
    }
    public function validateAccount($confirmation_code){

      $row = User::where('confirmation_code','=',$confirmation_code)->first();


      if(!is_null($row)){
        $row->confirmation_code = null;
        $row->status = 1;
        $row->save();

           //tagging
              $contact_name = $row->name ; 
              $user_name = $row->username;
              $user_email = $row->email;
              if ($row->telephone) {
                  $user_phone =  $row->telephone;
              } else {
                  $user_phone =  $row->mobile;
              }  
    //email welcome user
        if(!is_null($row)){
             $newsletter = Newsletters::where('id', '=', '11')->where('newsletter_status','=','1')->first();
            if ($newsletter) {
                      Mail::send('email.welcome_email', ['contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                           $message->to($row->email)->subject(''.$newsletter->subject.'');
                       });
                }
        }

       $counter = new NewslettersUserCounters;
       $counter->user_id = $row->id;
       $counter->newsletter_id = '14';
       $counter->save();

       $counter = new NewslettersUserCounters;
       $counter->user_id = $row->id;
       $counter->newsletter_id = '15';
       $counter->save();

        return redirect('/')->with('status', 'Account Validated');
      }else{
        return redirect('/')->with('error', 'Account is already validated');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function viewUserAds(){

      $this->data['title'] = "My Ads"; 
      $this->data['ads'] = Advertisement::latest()->get();
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
    
      return view('user.ads.list', $this->data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function changePhoto(){
      $input = Input::all();

      $row = User::find(array_get($input, 'id'));

      if(!is_null($row)){

          $rules = [
            'photo' => 'required|image|max:3000',
        ];

        // field name overrides
        $names = [
            'photo' => 'photo',
        ];

        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);
        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        Image::make(Input::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
        $row->photo = $row->id . '.jpg';
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
      
    }
       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 
    public function removePhoto(){

      $row = User::find(Request::input('id'));
      if(!is_null($row)){
        $row->photo = "default_image.png";
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function rateUser(){
    $row = UserRating::where('vendor_id','=',Request::input('vendor_id'))->where('user_id','=',Auth::user()->id)->first();
      if(is_null($row)){
        $row = new UserRating;
        $row->rate      = Request::input('rate');
        $row->vendor_id = Request::input('vendor_id');
        $row->user_id   = Auth::user()->id;
        $row->save();
        return Response::json(['body' => 'Rate has been saved']);      
      }else{
        return Response::json(['error' => ['error']]); 
      }
    }
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function saveComment(){
      $input = Input::all();
      $row = new UserComments;
      $row->user_id     = array_get($input, 'user_id');
      $row->vendor_id     = array_get($input, 'vendor_id');
      $row->comment    = array_get($input, 'comment');
      $row->save();
      return Response::json(['body' => 'posted']);      
    }
      /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function saveCommentReply(){
        $input = Input::all();
        $row = new UserCommentReplies;
        $row->comment_id = array_get($input, 'comment_id');
        $row->content = array_get($input, 'reply_content');
        $row->user_id = array_get($input, 'user_id');
        $row->save();
        return Response::json(['body' => 'Replied']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function getAds(){
        $this->data['title'] = "Dashboard-Settings";
        $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
        return View::make('user.vendor.list', $this->data);
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  }
  public function deleteBranch(){

        $row = CompanyBranch::find(Request::input('id'));
        if(!is_null($row)){
        $row->status="2";
        $row->save();
        return Response::json(['body' => 'Photo Updated']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }

  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
      public function message(){

        $input = Input::all();
       if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['categories']   = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
    $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['countries']    = Country::all();
    $this->data['usertypes']    = Usertypes::all();
    
         return View::make('user.dashboard.messages', $this->data);
  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  
  public function getMessages(){
    $get_message_title =  UserMessagesHeader::where('id','=',Request::input('id'))->first(); 
    $get_messages      = UserMessages::where('message_id','=',Request::input('id'))->get();
 
      $message_content ='';
      $ctr=1;
    if(count($get_messages)>0){
      foreach ($get_messages as $key => $field) {
          $get_user_info = User::where('id','=',$field->sender_id)->first();

        $message_content .= '<tr><td><div class="col-sm-12 '.($ctr % 2 ==0 ? 'bgWhite':'bgGray bottomBorder').'">'.
                            '<div class="commentBlock topPaddingB">'.
                              '<div class="commenterImage">'.
                                '<img src="'.url('uploads').'/'.$get_user_info->photo.'" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">'.
                              '</div>'.
                              '<div class="commentText">'.
                              '<div class="panelTitleB bottomPadding">'.$get_user_info->name.'<span class="smallText lightgrayText pull-right">'.date("F j, Y g:i A", strtotime($field->created_at)).'</div>'.
                              '<p class="normalText lightgrayText">'.$field->message.'</p>'.
                            '</div>'.
                            '</div>'.
                            '</div></td>'.
                          '</tr>';
      $ctr++;
      }
   }else{
      $message_content ='';
   }
    $result['message_title']   = $get_message_title->title;
    $result['message_content'] = $message_content;
    return Response::json($result);
  }

  public function saveUserMessage(){
    
        $input = Input::all();
       
        // create model if new
        if(array_get($input,'message_id')){
            $header = UserMessagesHeader::where('id','=',array_get($input,'message_id'))->first();
            $header->read_status = 1;
            $header->save();
            $row = new UserMessages;
            $row->message_id = strip_tags(array_get($input, 'message_id'));
            $row->message    = array_get($input, 'message');
            $row->sender_id  = Auth::user()->id;
            $row->save();
         
        }
      
      return Response::json(['body' => ['Message Sent']]);
  }

  public function sendMessage(){

        $input = Input::all();
       
        // create model if new
        if(array_get($input,'reciever_id')){

            $row = new UserMessagesHeader;
            $row->title       = array_get($input, 'title');
            $row->sender_id   = Auth::user()->id;
            $row->read_status = 1;
            $row->reciever_id = array_get($input, 'reciever_id');
            $row->save();

            $rows = new UserMessages;
            $rows->message_id  = $row->id;
            $rows->message     = array_get($input, 'message');
            $rows->sender_id   = Auth::user()->id;
            $rows->save();

        }


            $get_user_email = User::find($row->reciever_id);
            $useremail      = $get_user_email->email;
         //tagging
            $contact_name = Auth::user()->name; 
            $user_name    = Auth::user()->username;
            $user_email   = Auth::user()->email;
            if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
            } else {
                  $user_phone = Auth::user()->mobile;
            } 
            if ($user_phone == null) {
              $user_phone = 'N/A';
            }
            $title = $row->title;
            $messages = $rows->message;
               //email vendor inquiry
             $newsletter = Newsletters::where('id', '=', '13')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.vendor_inquiry', ['contact_name' => $contact_name, 'user_name' => $user_name, 'user_email' => $user_email,'user_phone' => $user_phone, 'messages' => $messages, 'title' => $title, 'newsletter' => $newsletter], function($message) use ($newsletter, $useremail) {
                                    $message->to($useremail)->subject(''.$newsletter->subject.'');
                                });
                     }
                     
      return Response::json(['body' => ['Message Sent']]);

  }
  public function changeMessageStatus(){
          $row = UserMessagesHeader::find(Request::input('id'));
        
          if(!is_null($row)){
            if($row->read_status != 2){
              $row->read_status="2";
              $row->save();
            }
          }
     
  }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
public function upgradeUserPlan(){
       $input = Input::all();
       $plan_id = Request::input('plan_id');
       $months_subscribed = Request::input('months_subscribed');
       $row = User::find(Request::input('user_id'));
        if(!is_null($row)){
        $row->user_plan_types_id = $plan_id;
        //Plan Expiration
        $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
        $date = date_create($current_time);
        $row->plan_expiration = date_add($date,date_interval_create_from_date_string($months_subscribed." months"));                   
        $row->save();

        $plan = UserPlanTypes::where('id','=', $plan_id)->first();
     //tagging
              $contact_name = Auth::user()->name; 
              $user_name = Auth::user()->username;
              $user_email = Auth::user()->email;
              if (Auth::user()->phone) {
                  $user_phone = Auth::user()->telephone;
               } else {
                  $user_phone = Auth::user()->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;
                  //email user new upgrade subscription
              $newsletter = Newsletters::where('id', '=', '12')->where('newsletter_status', '!=', '2')->first();
                     if ($newsletter) {
                                Mail::send('email.purchased_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row) {
                                    $message->to(Auth::user()->email)->subject(''.$newsletter->subject.'');
                                });
              } 
                       ;
        return Response::json(['body' => 'User Plan Upgraded']);      
      }else{
        return Response::json(['error' => ['error on finding the user']]); 
      }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function userPlanExpiring(){
      $get_users = User::select('users.id', 'users.user_plan_types_id', 'users.name', 'users.email', 'users.created_at', 'users.plan_expiration')
              ->where(function($query){
               $query ->where('user_plan_types_id', '!=', '1') 
                      ->where('status', '!=', '2')   
                      ->where('user_plan_types_id', '!=', '5');                                    
              })->get();

      foreach ($get_users as $value) {
      $date_trigger = '1 week after';

      $now = Carbon::now();
      $startTime = Carbon::parse($now);
      $finishTime = Carbon::parse($value->plan_expiration);
      $totalDuration = $finishTime->diffForHumans($startTime);

      if ($totalDuration == $date_trigger) {
      $plan = UserPlanTypes::where('id','=', $value->user_plan_types_id)->first();

             //tagging
              $contact_name = $value->name; 
              $user_name = $value->username;
              $user_email = $value->email;
              if ($value->phone) {
                  $user_phone = $value->telephone;
               } else {
                  $user_phone = $value->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;

                  $counter = NewslettersUserCounters::where('newsletter_id','=','14')->where('user_id','=',$value->id)->first();
                  if ($counter->counter_status == '1') {
                        $counter->counter_status = '2';
                        $counter->save();
                          //email user new upgrade subscription
                        $newsletter = Newsletters::where('id', '=', '14')->where('newsletter_status', '!=', '2')->first();
                        if ($newsletter) {
                                  Mail::send('email.expiration_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $value) {
                                      $message->to(''.$value->email.'')->subject(''.$newsletter->subject.'');
                                  });
                        } 
                  }
      }
 }         
}
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function userPlanExpired(){
$get_users = User::select('users.id', 'users.user_plan_types_id', 'users.name', 'users.email', 'users.created_at', 'users.plan_expiration')
              ->where(function($query){
               $query ->where('user_plan_types_id', '!=', '1') 
                      ->where('status', '!=', '2')    
                      ->where('user_plan_types_id', '!=', '5');                                    
              })->get();

      foreach ($get_users as $value) {

      $now           = Carbon::now();
      $startTime     = Carbon::parse($now);
      $finishTime    = Carbon::parse($value->plan_expiration);
      $totalDuration = $finishTime->diffForHumans($startTime);

       $diff = substr($totalDuration, -6); 

          if ($diff == 'before') {
          $plan = UserPlanTypes::where('id','=', $value->user_plan_types_id)->first();
          $test = 'test';
             //tagging
              $contact_name = $value->name; 
              $user_name    = $value->username;
              $user_email   = $value->email;
              if ($value->phone) {
                  $user_phone = $value->telephone;
               } else {
                  $user_phone = $value->mobile;
               } 
               if ($user_phone == null) {
              $user_phone = 'N/A';
               } 
              $plan_name         = $plan->plan_name;
              $months6_price     = $plan->months6_price;
              $months12_price    = $plan->months12_price;
              $ads               = $plan->ads;
              $auction           = $plan->auction;
              $img_per_ad        = $plan->img_per_ad;
              $img_total         = $plan->img_total;
              $total_ads_allowed = $plan->total_ads_allowed;
              $video_total       = $plan->video_total;
              $point             = $plan->point;
              $bid               = $plan->bid;
              $point_exchange    = $plan->point_exchange;
              $sms               = $plan->sms;

                  $counter = NewslettersUserCounters::where('newsletter_id','=','15')->where('user_id','=',$value->id)->first();
                  if ($counter->counter_status == '1') {
                        $counter->counter_status = '2';
                        $counter->save();
                          //email user new upgrade subscription
                        $newsletter = Newsletters::where('id', '=', '15')->where('newsletter_status', '!=', '2')->first();
                        if ($newsletter) {
                                  Mail::send('email.expiration_plan', ['sms' => $sms,'point_exchange' => $point_exchange,'bid' => $bid,'point' => $point,'video_total' => $video_total,'total_ads_allowed' => $total_ads_allowed,'img_total' => $img_total,'img_per_ad' => $img_per_ad,'img_per_ad' => $img_per_ad,'auction' => $auction,'ads' => $ads,'months12_price' => $months12_price,'months6_price' => $months6_price,'plan_name' => $plan_name,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $value) {
                                      $message->to(''.$value->email.'')->subject(''.$newsletter->subject.'');
                                  });
                        } 
                  }
     $disable_account = User::find($value->id);
     $disable_account->status = '2';
     $disable_account->save(); 
      }
   
     
 }         
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function settingSave(){
        $new = true;

        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = User::find(array_get($input, 'id'));
            
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [
            'password'           => 'required|min:6|confirmed',

        ];

        // field name overrides
        $names = [
            'password'       => 'password',
        ];
        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // dd(Hash::make(array_get($input,'password')));
        // set type only if this isn't me


       if(array_get($input,'password')){
          $row->password = Hash::make(array_get($input, 'password'));
          $row->save();
        }

        // do not change password if old user and field is empty
        // save model
      

    //     $validate = User::find($row->id);

    //      //tagging
    //           $contact_name = $row->name ; 
    //           $user_name = $row->username;
    //           $user_email = $row->email;
    //           if ($row->telephone) {
    //               $user_phone =  $row->telephone;
    //           } else {
    //               $user_phone =  $row->mobile;
    //           }  
    //        $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
    //           <a href="http://yakolak.xerolabs.co/register/verify/'.$confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
    //              Verify your email address
    //           </a>
    //       </td>';
    // //register user email validation sent
    //     if($new){
    //          $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
    //         if ($newsletter) {
    //                   Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'confirmation_code' => $confirmation_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $validate) {
    //                        $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

    //                    });
    //             }
    //     }
  
          return Response::json(['body' => ['Account Settings Updated']]);
}

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function refreshVendorAdsActive(){
      $user_id = Auth::user()->id;
      $featuredads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.status', '=', '1')                                      
                                                     ->where('advertisements.feature', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $activeads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.feature', '!=', '1')                                     
                                                     ->where('advertisements.ads_type_id', '!=', '2')                                     
                                                     ->where('advertisements.status', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $disabledads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '1')                                      
                                                     ->where('advertisements.status', '=', '3');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
     $spamdads = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.status', '=', '4');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();

   
   $rows = '';
   $rows .= '<div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
         
        <!-- Mobile View -->
        <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs" >
          <div class="panel panel-default removeBorder borderZero">
            <div class="panel-heading panelTitleBarLightB">
                <span class="panelTitle">Listings</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
                  <span class="pull-right"><a href="'.url('category/list').'" class="redText">view more</a></span>
               </div>
            <div class="col-sm-12 col-xs-12 blockBottom noPadding">
             <div class="panel-body noPadding">
            </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="panel panel-default removeBorder borderZero hidden-xs hidden-sm">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Listings</span>
              </div>
                 <div id="load-basic-listing-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 

          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 noPadding">';
            $i = 4;
             for ($x = 1; $x <= $i; $x++) {
              if ($x == 1) {
                   $ads =  $featuredads;
              } elseif ($x == 2) {
                   $ads =  $activeads;
              } elseif ($x == 3) {
                   $ads =  $disabledads;  
              } elseif ($x == 4) {
                   $ads =  $spamdads;  
              }
          $rows .=' <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                <div class="panel-body nobottomPadding" >
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div  class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="list" class="grayText" data-toggle="collapse"  href="#collapse'.$x.'">
                '.($x == 1?'<strong>Featured </strong><span class="leftMargin lightgrayText">| You can upgrade "Active" listings to Featured.</span> <span class="redText pull-right">'.count($featuredads).' Featured':'').
                  ($x == 2?'<strong>Active </strong><span class="leftMargin lightgrayText">| Free account type can have only 4 active listings.</span><span class="redText pull-right">'.count($activeads).' Active':'').
                  ($x == 3?'<strong>Disabled </strong><span class="leftMargin lightgrayText">|  Free account type can only have 4 active listings you must disable active first.</span><span class="redText pull-right">'.count($disabledads).' Disabled':'').
                  ($x == 4?'<strong>Mark as Spam </strong><span class="leftMargin lightgrayText">| Warning listing marked as span will not be display unless they comply with our terms and conditions.</span><span class="redText pull-right">'.count($spamdads). ' Spam':'').
                '<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse'.$x.'" class="panel-collapse collapse '.($x == '2'? 'in':'').'">
              <div class="panel-body borderBottom row removeBorder">';
         foreach ($ads as $row)  { 
                  $rows .='<div class="col-lg-3 col-md-3" id="ads-list">
                            
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="'. url('/ads/view') . '/' . $row->id.'"><div class="adImage" style="background: url('. url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding" >
                              <div class="visible-lg">'.substr($row->title, 0, 26). 
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                                <div class="visible-md">'.substr($row->title, 0, 20).
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by '.$row->name .'<span class="pull-right"><strong class="blueText">
                                   '.number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">'
                                    .($row->city != null?'
                                <i class="fa fa-map-marker rightMargin"></i>
                                '.$row->city.'':'').($row->city != null && $row->countryName != null ? ', ':'').
                                    ($row->countryName != null ? ''.$row->countryName.'': '') .
                                    ($row->city == null && $row->countryName == null ? '<i class="fa fa-map-marker rightMargin"></i> not available':'').
                             '</div>
                                <div>
                                    <span class="blueText rightMargin">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-empty"></i>
                                    </span>
                                    <span class="normalText lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                  </div>
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                        <span class="dropdown">
                        <a class="dropdown-toggle noBackground noPadding normalText grayText" type="" id="menu1" data-toggle="dropdown"><i class="fa fa-angle-down"></i> Tools
                        </a>
                        <ul class="dropdown-menu borderZero" role="menu" aria-labelledby="menu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="'.url('ads/edit').'/'.$row->id.'">Edit</a></li>
                          <li role="presentation"><a role="menuitem" data-id="'.$row->id.'" data-title="'. $row->title .'" '.($x == 3 ? 'class="btn-enable-ads-alert" tabindex="-1" href="#">Enable</a>':'class="btn-disable-ads-alert" tabindex="-1" href="#">Disable</a>').' </li>
                          <li role="presentation"><a role="menuitem" data-ads_id="'.$row->id.'" data-title="'. $row->title .'" class="'.($x == 1?'btn-make-basic-ad':'btn-make-auction').'">'.($x == 1?'Make Basic Ad':'Make Auction').'</a></li>
                          <li role="presentation"><a role="menuitem" data-id="'.$row->id.'" data-title="'. $row->title .'" class="btn-delete-ads" tabindex="-1" href="#">Delete</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Featured</a></li>
                        </ul>
                      </span>
                      <span class="normalText blueText pull-right">Featured</span>
                      </div>
                    </div>
                 </div>';
                }
             $rows .= '</div>
            </div>
            <div class="row borderBottom"></div>
          </div>
        </div>
        </div>
        </div>';
}
       $rows .='</div>
          <!-- Mobile Listing -->
    </div>
  </div>
';
               
      return Response::json($rows);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */  

    public function refreshVendorAuctionActive(){
      $user_id = Auth::user()->id;
   $activeauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.feature', '!=', '1')                                     
                                                     ->where('advertisements.ads_type_id', '=', '2')                                     
                                                     ->where('advertisements.status', '=', '1');                                      
                                          })->orderBy('advertisements_photo.created_at', 'desc')->get();
      $endedauction = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                         ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                         ->leftJoin('countries','countries.id', '=', 'users.country')
                                         ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                         ->select('advertisements.ad_expiration','advertisements.title',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                          'users.name', 'advertisements.status', 'countries.countryName')                          
                                         ->where(function($query)use($user_id) {
                                               $query->where('advertisements_photo.primary', '=', '1')
                                                     ->where('advertisements.user_id', '=', $user_id)                                                                           
                                                     ->where('advertisements.ads_type_id', '=', '2')
                                                     ->where('advertisements.status', '=', '3');
                                                 })->orderBy('advertisements_photo.created_at', 'desc')->get();

   
   $rows = '';
   $rows .= '<div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
         
        <!-- Mobile View -->
        <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs" >
          <div class="panel panel-default removeBorder borderZero">
            <div class="panel-heading panelTitleBarLightB">
                <span class="panelTitle">Listings</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
                  <span class="pull-right"><a href="'.url('category/list').'" class="redText">view more</a></span>
               </div>
            <div class="col-sm-12 col-xs-12 blockBottom noPadding">
             <div class="panel-body noPadding">
            </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="panel panel-default removeBorder borderZero hidden-xs hidden-sm">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Auction Advertisements</span>
              </div>
                 <div id="load-auction-listing-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 

          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 noPadding">';
            $i = 2;
             for ($x = 1; $x <= $i; $x++) {
              if ($x == 1) {
                   $ads =  $activeauction;
              } else {
                   $ads =  $endedauction;
              }
          $rows .=' <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                <div class="panel-body nobottomPadding" >
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div  class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="list" class="grayText" data-toggle="collapse"  href="#collapse'.($x=='1'?'aucActive':'aucEnded').'">
                '.($x == 1?'<strong>Active Auctions </strong><span class="leftMargin lightgrayText">| You can upgrade "Active" listings to Featured.</span> <span class="redText pull-right">'.count($activeauction).' Active':'').
                  ($x == 2?'<strong>Ended Auctions </strong><span class="leftMargin lightgrayText">| Free account type can have only 4 active listings.</span><span class="redText pull-right">'.count($endedauction).' Ended':'').
                '<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse'.($x=='1'?'aucActive':'aucEnded').'" class="panel-collapse collapse '.($x == '1'? 'in':'').'">
              <div class="panel-body borderBottom row removeBorder">';
         foreach ($ads as $row)  { 
                  $rows .='<div class="col-lg-3 col-md-3" id="ads-list">
                            
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                      <div class="normalText redText bottomPadding borderZero">
                         <div data-countdown="'.$row->ad_expiration.'">D: 1 - H: 23 - M: 17</div>
                        </div>
                          <div class="">
                          <a href="'. url('/ads/view') . '/' . $row->id.'"><div class="adImage" style="background: url('. url('uploads/ads/thumbnail').'/'.$row->photo.'); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding" >
                              <div class="visible-lg">'.substr($row->title, 0, 26). 
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                                <div class="visible-md">'.substr($row->title, 0, 20).
                                    (strlen($row->title) >= 37 ? '..':'').'
                                </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by '.$row->name .'<span class="pull-right"><strong class="blueText">
                                   '.number_format($row->price) .' USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">'
                                    .($row->city != null?'
                                <i class="fa fa-map-marker rightMargin"></i>
                                '.$row->city.'':'').($row->city != null && $row->countryName != null ? ', ':'').
                                    ($row->countryName != null ? ''.$row->countryName.'': '') .
                                    ($row->city == null && $row->countryName == null ? '<i class="fa fa-map-marker rightMargin"></i> not available':'').
                             '</div>
                                <div>
                                    <span class="blueText rightMargin">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-half-empty"></i>
                                    </span>
                                    <span class="normalText lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                  </div>
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                        <span class="dropdown">
                        <a class="dropdown-toggle noBackground noPadding normalText grayText" type="" id="menu1" data-toggle="dropdown"><i class="fa fa-angle-down"></i> Tools
                        </a>
                        <ul class="dropdown-menu borderZero" role="menu" aria-labelledby="menu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="'.url('ads/edit').'/'.$row->id.'">Edit</a></li>
                          <li role="presentation"><a role="menuitem" data-ads_id="'.$row->id.'" data-id="'.$row->id.'" data-title="'. $row->title .'" '.($x == 2 ? 'class="btn-make-auction" tabindex="-1" href="#">Start Auction</a>':'class="btn-disable-ads-alert" tabindex="-1" href="#">End Auction</a>').' </li>
                          <li role="presentation"><a role="menuitem" data-id="'.$row->id.'" data-title="'. $row->title .'" class="btn-delete-ads" tabindex="-1" href="#">Delete</a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Featured</a></li>
                        </ul>
                      </span>
                      <span class="normalText blueText pull-right">Featured</span>
                      </div>
                    </div>
                 </div>';
                }
             $rows .= '</div>
            </div>
            <div class="row borderBottom"></div>
          </div>
        </div>
        </div>
        </div>';
}
       $rows .='</div>
          <!-- Mobile Listing -->
    </div>
  </div>
';           
      return Response::json($rows);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
  public function formMakeAuction() {
     $input = Input::all();  
    $row = Advertisement::find(Request::input('ads_id'));

      $result['row'] = $row;                      

     if($row) {
           return Response::json($result);
        } else {
          return Response::json(['error' => "Invalid row specified"]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

public function adMakeAuction(){
  $new = true;
  $input = Input::all();
  $bid_limit_amount = Input::get('bid_limit_amount');
  $bid_start_amount = Input::get('bid_start_amount');
  $minimum_allowed_bid = Input::get('minimum_allowed_bid');
  $bid_duration_start = Input::get('bid_duration_start');
  $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
  $ads_id = Input::get('ads_id');
  $type_id = Input::get('type_id');
 
  if(array_get($input, 'ads_id')) {
            $row = Advertisement::find(array_get($input, 'ads_id'));
            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
       }
     $new = false;
  }

  if($ads_id) {
     $row = Advertisement::find($ads_id);
       if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
       }
       $new = false;
   }

  if (Auth::check()) {
     $user_id = Auth::user()->id;
     $user_plan_types_id = Auth::user()->user_plan_types_id;

        if ($user_id != $row->user_id) {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);      
        }
  } else  {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);    
  } 
  if ($type_id != '2') {
         return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);     
  }
  $get_plan = UserPlanTypes::find($user_plan_types_id);
  $get_posted_auctions = Advertisement::where('ads_type_id','=','2')->where('user_id','=', $user_id)->where('status','!=','2')->get();
  if ($row->ads_type_id != '2') {
     if (count($get_posted_auctions) >= $get_plan->auction) {
         return Response::json(['erroruser' => "You have reached the alowed auction limit"]);     
    }
  }
   
  $remove_existing_bidders = AuctionBidders::where('ads_id','=',$ads_id)->get();
  if ($remove_existing_bidders) {
     foreach ($remove_existing_bidders as $value) {
       $value->bid_limit_amount_status = 0;
       $value->bidder_status = 0;
       $value->status = 0;
       $value->save();
     }
  }
  
 

        $rules = [
             'bid_limit_amount'               => 'required|int|min:1|max:999999999',
             'bid_start_amount'               => 'required|int|min:1|max:999999999999',
             'minimum_allowed_bid'            => 'required|int|min:1|max:999999999',
             'bid_duration_start'             => 'required|int|min:1|max:999',
             'bid_duration_dropdown_id'       => 'required|int|min:1',
             'type_id'                        => 'required|int|min:1',
        ];
        // field name overrides
        $names = [  
             'bid_limit_amount'               => 'Bid Limit Amount',
             'bid_start_amount'               => 'Bid Start Amount',
             'minimum_allowed_bid'            => 'Minimum Allowed Amount',
             'bid_duration_start'             => 'Bid Duration',
             'bid_duration_dropdown_id'       => 'Bid Duration',
             'ads_id'                         => 'required',
             'type_id'                        => 'photo',
        ];

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names); 

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

        if ($bid_limit_amount <= $bid_start_amount) {
            return Response::json(['errorbidlimit' => 'Bid limit must be greater than bid start']); 
        }

       $row->ads_type_id = $type_id;
       $row->bid_limit_amount = $bid_limit_amount;
       $row->bid_start_amount = $bid_start_amount;
       $row->minimum_allowed_bid = $minimum_allowed_bid;
       $row->bid_duration_start = $bid_duration_start;
       $row->bid_duration_dropdown_id = $bid_duration_dropdown_id;

        //ad expiration auction
          if ($bid_duration_start) {
           $bid_duration_start = Input::get('bid_duration_start');
           $bid_duration_dropdown_id = Input::get('bid_duration_dropdown_id');
           $current_time = Carbon::now()->format('Y-m-d H:i:s');                   
                if ($bid_duration_dropdown_id == '1') {
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." days"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '2'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." weeks"));                 
                   $row->ad_expiration = $expiration_date; 
                } elseif ($bid_duration_dropdown_id == '3'){
                    $date = date_create($current_time);
                    $expiration_date = date_add($date,date_interval_create_from_date_string($bid_duration_start." months"));                 
                   $row->ad_expiration = $expiration_date;
                } else {
                  return Response::json(['erroruser' => "Your Not Allowed to do this Action"]);     
                }
          $row->bid_duration_start         = $bid_duration_start;
          } else {
            $row->bid_duration_start       = null;
          }
        $row->ad_exp_email_trigger = '1';
        $row->auction_expiring_noti_trigger = '1';
        $row->status = '1';
        $row->save();

        if ($new == false) {
           return Response::json(['body' => "Advertisement Successfully updated"]); 
        }

   

}

}