<?php
namespace App\Http\Controllers;
use App\User;
use App\UserPlanTypes;
use App\UserAdComments;
use App\UserAdCommentReplies;
use App\Usertypes;
use App\Category;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Mail;
use Form;
use App\NewsLetters;
use Carbon\Carbon;
use View;
use App\PromotionPopups;
use App\PromotionMessages;
use App\Country;
use App\City;
use App\PromotionMessagesRecipient;
use App\UserMessages;
use App\UserMessagesHeader;
use App\PromotionBanners;
use App\PromotionBannerPositions;
use Auth;


class PromotionController extends Controller
{
   public function index(){
   	$result = $this->doList();
    $this->data['rows'] = $result['rows'];
   	$this->data['title'] = "Promotion Management";
   	$this->data['refresh_route'] = url("admin/promotion/refresh");
    $this->data['countries'] = Country::orderBy('countryName','asc')->get();
   	$this->data['categories'] = Category::with('subcategoryinfo')->get();
	  return View::make('admin.promotion-management.list', $this->data);
   }

   public function doList(){
      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = PromotionPopups::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
                              
      } else {
         $count = PromotionPopups::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows = PromotionPopups::where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }	
   }
   public function save(){
      	$new = true;
        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = PromotionPopups::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'              	  => $new ? 'image|max:3000' : 'image|max:3000',
            'name'      		  => 'required',
            'duration'   => 'required|numeric|max:3',
            'country'   => 'required',
            'city'   => 'required',
            'cat_id'   => 'required',
           
        ];
        // field name overrides
        $names = [
            'photo'          		 => 'photo',
            'name'			 => 'name',
            'duration'  => 'Duration',
            'cat_id'  => 'Category Id',
            'country'  => 'Country',
            'city'  => 'City',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            $row = new PromotionPopups;
            $row->name      = strip_tags(array_get($input, 'name'));
            $row->duration     = array_get($input, 'duration');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->country     = array_get($input, 'country');
            $row->city     = array_get($input, 'city');
        } else {
            $row->name      = strip_tags(array_get($input, 'name'));
            $row->duration     = array_get($input, 'duration');
            $row->cat_id     = array_get($input, 'cat_id');
            $row->country     = array_get($input, 'country');
            $row->city     = array_get($input, 'city');
            $row->save();
        }   
          	$row->save();
          	// $get_plan_duration = BannerPlacementPlans::find($row->placement_plan);
          	// $date = strtotime(date("Y-m-d H:i:s", strtotime($row->created_at)) . " +".$get_plan_duration->duration." month");
          	// $row->expiration = date("Y-m-d H:i:s", $date);
          	// $row->save();

        	if (Input::file('photo')) {
	            // Save the photo
	            Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-'.$row->id . '.jpg');
	            $row->photo = 'banner-'.$row->id . '.jpg';
	            $row->save();
       		}

	    return Response::json(['body' => ['Popup Advertisement Saved']]);

   }
  public function manipulate(){
   $result['rows'] = PromotionPopups::find(Request::input('id'));

   $get_city = City::where('country_id','=',$result['rows']->country)->get();
          $city_info = '<option class="hide" value="">Select:</option>';
        foreach ($get_city as $key => $field) {
          $city_info .= '<option value="' . $field->id . '" '.($field->id == $result['rows']->city ? 'selected':'' ).'>'.$field->name.'</option>';
        }

  $result['city_info'] = $city_info;
	 return Response::json($result);

   }
  public function delete(){

    $row = PromotionPopups::find(Request::input('id'));
    if(!is_null($row)){
      $row->status = '2';
      $row->save();
       return Response::json(['body' => ['Banner has been removed']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function doListPromotionMessage(){

      $result['sort'] = Request::input('sort') ?: 'created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = PromotionMessages::select('usertypes.type_name','usertypes.id','promotion_messages.*')
                                ->leftjoin('usertypes','usertypes.id','=','promotion_messages.usertype_id')

                                ->where(function($query) use ($status) {
                                    $query->where('promotion_messages.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_messages.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_messages.subject', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

                               
                              
      } else {
         $count =  PromotionMessages::select('usertypes.type_name','usertypes.id','promotion_messages.*')
                                ->leftjoin('usertypes','usertypes.id','=','promotion_messages.usertype_id')
                                ->where(function($query) use ($status) {
                                    $query->where('promotion_messages.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_messages.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_messages.subject', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows =  PromotionMessages::select('usertypes.type_name','usertypes.id','promotion_messages.*')
                                ->leftjoin('usertypes','usertypes.id','=','promotion_messages.usertype_id')
                                ->where(function($query) use ($status) {
                                    $query->where('promotion_messages.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_messages.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_messages.subject', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      } 
      
  }

  public function indexPromotionMessage(){
    $result = $this->doListPromotionMessage();
    $this->data['rows'] = $result['rows'];
    $this->data['title'] = "Promotion Message Management";
    $this->data['refresh_route'] = url("admin/promotion/message/refresh");
    $this->data['categories'] = Category::with('subcategoryinfo')->get();
    $this->data['usertypes'] =  Usertypes::all();
    return View::make('admin.promotion-management.promotion-message.list', $this->data);

  }
  public function savePromotionMessage(){

        $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $header = PromotionMessages::find(array_get($input, 'id'));

            if(is_null($header)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'usertype_id'     => 'required',
            'subject'         => 'required',
            'message'         => 'required',
        
        ];
        // field name overrides
        $names = [
            'usertype_id'    => 'Usertype',
            'subject'       => 'subject',
            'message'       => 'message',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        // create model if new
        if($new) {
            if(array_get($input, 'usertype_id') == "1"){

                $get_users = User::where('usertype_id','=',1)->get();
                $header = new PromotionMessages;
                $header->usertype_id =array_get($input, 'usertype_id');
                $header->subject     = array_get($input, 'subject');
                $header->message      = array_get($input, 'message');
                $header->save();
              

              foreach ($get_users as $key => $field) {
                
                $row = new PromotionMessagesRecipient;
                $row->user_id      = $field->id;
                $row->promotion_message_id = $header->id;
                $row->save();
                
                $header_vendor = new UserMessagesHeader;
                $header_vendor->title = array_get($input, 'subject');
                $header_vendor->sender_id = Auth::user()->id;
                $header_vendor->reciever_id =$field->id;
                $header_vendor->save();

                $message_vendor = new UserMessages;
                $message_vendor->message_id =  $header_vendor->id;
                $message_vendor->message =  array_get($input, 'message');
                $message_vendor->sender_id = Auth::user()->id;
                $message_vendor->save();

              }
             

            }

            if(array_get($input, 'usertype_id') == "2"){
                $get_users = User::where('usertype_id','=',2)->get();
                $header = new PromotionMessages;
                $header->usertype_id =array_get($input, 'usertype_id');
                $header->subject     = array_get($input, 'subject');
                $header->message      = array_get($input, 'message');
                $header->save();

              foreach ($get_users as $key => $field) {
                $row = new PromotionMessagesRecipient;
                $row->user_id      = $field->id;
                $row->promotion_message_id = $header->id;
                $row->save();

                $header_vendor = new UserMessagesHeader;
                $header_vendor->title = array_get($input, 'subject');
                $header_vendor->sender_id = Auth::user()->id;
                $header_vendor->reciever_id =$field->id;
                $header_vendor->save();

                $message_vendor = new UserMessages;
                $message_vendor->message_id =  $header_vendor->id;
                $message_vendor->message =  array_get($input, 'message');
                $message_vendor->sender_id = Auth::user()->id;
                $message_vendor->save();

              }
            }
           
       
        } else {
           if(array_get($input, 'usertype_id') == "1"){

                $get_users = User::where('usertype_id','=',1)->get();
                $header->usertype_id =array_get($input, 'usertype_id');
                $header->subject     = array_get($input, 'subject');
                $header->message      = array_get($input, 'message');
                $header->save();

              foreach ($get_users as $key => $field) {
                $row =  PromotionMessagesRecipient::where('id','=',$header->id)->first();
                $row->user_id      = $field->id;
                $row->promotion_message_id = $header->id;
                $row->save();

              }
            }

            if(array_get($input, 'usertype_id') == "2"){
                $get_users = User::where('usertype_id','=',2)->get();
                $header->usertype_id =array_get($input, 'usertype_id');
                $header->subject     = array_get($input, 'subject');
                $header->message      = array_get($input, 'message');
                $header->save();

              foreach ($get_users as $key => $field) {
                $row =  PromotionMessagesRecipient::where('id','=',$header->id)->first();
                $row->user_id      = $field->id;
                $row->promotion_message_id = $header->id;
                $row->save();

              }
            }
        }   
         
         
     
      return Response::json(['body' => ['Message Sent']]);

  }
  public function manipulatePromotionMessage(){
      $result['rows'] = PromotionMessages::find(Request::input('id'));
      return Response::json($result);
  }
  public function deletePromotionMessage(){
    $row = PromotionMessages::find(Request::input('id'));
    if(!is_null($row)){
      $row->status = '2';
      $row->save();
       return Response::json(['body' => ['Promotion message has been removed']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function indexPromotionBanner(){
    $result = $this->doListPromotionBanner();
    $this->data['rows'] = $result['rows'];
    $this->data['title'] = "Promotion Banner Management";
    $this->data['refresh_route'] = url("admin/promotion/banner/refresh");
    $this->data['categories'] = Category::with('subcategoryinfo')->get();
    $this->data['usertypes'] =  Usertypes::all();
    $this->data['position'] =  PromotionBannerPositions::all();
    return View::make('admin.promotion-management.promotion-banners.list', $this->data);

  }
  public function doListPromotionBanner(){
    
      $result['sort'] = Request::input('sort') ?: 'promotion_banners.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 10;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

        $rows = PromotionBanners::select('promotion_banners.*','banner_position.name as position')
                                ->leftjoin('promotion_banner_position as banner_position','banner_position.id','=','promotion_banners.banner_position_id')
                                ->where(function($query) use ($status) {
                                    $query->where('promotion_banners.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_banners.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_banners.name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

                // dd($rows);                
                              
      } else {
         $count =  PromotionBanners::select('promotion_banners.*','banner_position.name as position')
                                ->leftjoin('promotion_banner_position as banner_position','banner_position.id','=','promotion_banners.banner_position_id')
                                ->where(function($query) use ($status) {
                                    $query->where('promotion_banners.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_banners.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_banners.name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

      $rows =  PromotionBanners::select('promotion_banners.*','banner_position.name as position')
                                ->leftjoin('promotion_banner_position as banner_position','banner_position.id','=','promotion_banners.banner_position_id')
                                ->where(function($query) use ($status) {
                                    $query->where('promotion_banners.status', 'LIKE', '%' . $status . '%')
                                          ->where('promotion_banners.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('promotion_banners.name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                //->leftjoin('sub_categories','sub_categories.cat_id','=','categories.id')
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      } 
  }
  public function savePromotionBanner(){
     $new = true;
        
        $input = Input::all();

        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = PromotionBanners::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'  => $new ? 'image|max:3000' : 'image|max:3000',
            'name'         => 'required',
            'banner_position_id'         => 'required',
        
        ];
        // field name overrides
        $names = [
            'photo'    => 'Photo',
            'name'       => 'Name',
            'banner_position_id'       => 'position',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        if($new){
          $row = new PromotionBanners;
          $row->name = array_get($input,'name');
          $row->banner_position_id = array_get($input,'banner_position_id');
         
        }else{
          $row->name = array_get($input,'name');
          $row->banner_position_id = array_get($input,'banner_position_id');
          $row->save();
        }
          $row->save();
        if (Input::file('photo')) {
              // Save the photo
              Image::make(Input::file('photo'))->fit(848, 120)->save('uploads/' . 'banner-promotion-'.$row->id . '.jpg');
              $row->photo = 'banner-promotion-'.$row->id . '.jpg';
              $row->save();
        }

      return Response::json(['body' => ['Promotion Banner Saved']]);

  }
   public function manipulatePromotionBanner(){
    
      $result['rows'] = PromotionBanners::find(Request::input('id'));
      return Response::json($result);
  }
  public function deletePromotionBanner(){
    $row = PromotionBanners::find(Request::input('id'));
    if(!is_null($row)){
      $row->status = '2';
      $row->save();
       return Response::json(['body' => ['Promotion banner has been removed']]);
    }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  public function changeStatus(){
    $row = PromotionPopups::find(Request::input('id'));
    if(!is_null($row)){

      $row->popup_status = Request::input('stat');
      $row->save();
       return Response::json(['body' => ['Promotion status has been changed']]);
   }else{
      return Response::json(['error' => ['erro Finding the id']]);
    }
  }
  
  public function getCity() {

        $get_city = City::where('country_id', '=', Request::input('id'))->orderBy('name','asc')->get();

        $rows = '<option class="hide" value="">Select:</option>';

        foreach ($get_city as $key => $field) {
          $rows .= '<option value="' . $field->id . '">' . $field->name . '</option>';
        }
        return Response::json($rows);

     }

}