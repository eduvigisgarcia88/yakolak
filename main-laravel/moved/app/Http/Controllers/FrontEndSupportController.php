<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Country;
use App\Usertypes;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\BannerSubscriptions;
use Carbon\Carbon;
use App\UserMessagesHeader;


class FrontEndSupportController extends Controller
{

	public function index(){
		$this->data['title'] = "Support";


  if (Auth::check()) {
      $user_type_id = Auth::user()->usertype_id;
      $created = new Carbon(Auth::user()->created_at);
      $now = Carbon::now();

      $difference = ($created->diff($now)->days < 1)
          ? 'today'
          : $created->diffForHumans($now);
          //check if days not months
          $arr = explode(' ',trim($difference));
          if ($arr[1] == 'days') {
           $diff = '1';
           $test = 'true';
          } elseif ($arr[1] == 'years' || $arr[1] == 'year') {
           $diff = '12';
          }
       else {
       $diff = substr($difference, 0, 1); 
       }


  } else {
      $user_type_id = '1';
      $diff = null;
  }

$banner_placement = BannerPlacement::all();

$bannerRates ="";
foreach ($banner_placement as $value) {
$bannerRates .='<div class="col-md-4">
                        <div class="panel panel-default borderZero"><div class="panel-heading text-center"><h4 class="blueText">'.$value->name.'</h4></div>
                          <div class="panel-body"><div class="text-center borderbottomLight"><div class="normalText lightgrayText">ADVERTISEMENT PLAN</div>
                             <h6 class="grayText"></h6>       
                               </div><div class="blockTop">';
$bannerplan_id = $value->id;
     if (Auth::check()) {
               $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                        ->select('banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period')
                                        ->where(function($query)  use($bannerplan_id, $user_type_id){
                                                $query->where('banner_placement_id', '=', $bannerplan_id)
                                                      ->where('usertype_id', '=', $user_type_id);
                                        })->get();
                              
           foreach ($get_banner_plan as $features) {
              $bannerRates .='<div class="normalText grayText"><span class=""><i class="fa fa-check-square rightMargin"></i> $';
                  
                          if ($diff <= $features->duration || !($features->duration == 0) && Auth::check() && !($diff == null)) { 
                                      if (!($features->discount == 0)) {
                                             $inti_discount = 100 - $features->discount;
                                             $total_discount = '.'.$inti_discount;
                                             $total_price = $features->price * $total_discount;
                                             $test = 'test1';
                                      } else {
                                         $total_price = $features->price; 
                                      }
                          } else {
                           $total_price = $features->price; 
                          }
              $bannerRates .=''.$total_price.' FOR '.$features->period.($features->period > 1 ? ' MONTHS':' MONTH').' </span></div>';
            }

     } else {
             $get_usertypes = Usertypes::all();
          
             foreach ($get_usertypes as $usertypes) {

             $usertype_id = $usertypes->id;
             $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                        ->leftjoin('usertypes','banner_placement_plans.usertype_id','=','usertypes.id')
                                        ->select('usertypes.type_name as type_name','banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period', 'banner_placement_plans.id as banner_plan_id')
                                        ->where(function($query)  use($bannerplan_id, $usertype_id){
                                                $query->where('banner_placement_id', '=', $bannerplan_id)
                                                      ->where('banner_placement_plans.usertype_id', '=', $usertype_id);
                                        })->get();
             
              $bannerRates .='<div class="normalText blueText"><h5>'.$usertypes->type_name.'</h5></div><h6 class="grayText"></h6> ';
           
              foreach ($get_banner_plan as $features) {
                  $bannerRates .='<div class="normalText grayText"><span class=""><i class="fa fa-check-square rightMargin"></i> $'.$features->price.' FOR '.$features->period.($features->period > 1 ? ' MONTHS':' MONTH').'</span></div>';
               }
           }
     }
$bannerRates .='</div></div><div class="panel-footer"><a href="#'.$value->id.'"><span class="normalText">Jump To Plan Details<span class="pull-right"><i class="fa fa-arrow-circle-down"></i></span></span></a></div></div></div>';
 }                    

 //section
foreach ($banner_placement as $value) {
$bannerplan_id = $value->id;  
$bannerRates .='<div id="'.$value->id.'" class="col-md-12">
                        <div class="panel panel-default borderZero">
                        <div class="panel-heading"><span class="xlargeText blueText">'.$value->name.'</span><span class="pull-right"><button data-plan="6" data-id="1" data-title="Bronze" class="btn btn-sm blueButton borderZero noMargin btn-plan-subscription-upgrade" type="submit">GET THIS PLAN</button></span></div>
                          <div class="panel-body"><div class="text-center borderbottomLight"><div class="normalText lightgrayText">ADVERTISEMENT PLAN FOR</div>  </div><div class="blockTop">  <h5 class="grayText">';

         $get_banner_plan = BannerPlacement::join('banner_placement_plans','banner_placement.id','=','banner_placement_plans.banner_placement_id')
                                    ->leftjoin('usertypes','banner_placement_plans.usertype_id','=','usertypes.id')
                                    ->select('usertypes.type_name as type_name','banner_placement.*','banner_placement_plans.price as price','banner_placement_plans.duration', 'banner_placement_plans.usertype_id', 'banner_placement_plans.discount', 'banner_placement_plans.period', 'banner_placement_plans.id as banner_plan_id', 'banner_placement_plans.points')
                                    ->where(function($query)  use($bannerplan_id, $user_type_id){
                                            $query->where('banner_placement_id', '=', $bannerplan_id);
                                    })->get();
         foreach ($get_banner_plan as $features) {
              $bannerRates .='<div class="normalText grayText"><span class="">
              <i class="fa fa-check-square rightMargin"></i>'.$features->points.($features->points > 1 ? ' POINTS':' POINT').'</span></div>';
         }
       
$bannerRates .='</h5></div></div></div></div>';
}

 		$this->data['bannerRates']  = $bannerRates;
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable']     = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['categories']   = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	  $this->data['countries']    = Country::all();
    $this->data['usertypes']    = Usertypes::all();

 if(Auth::check()){
     $user_id = Auth::user()->id;
 }else {
     $user_id = null;
 }

$this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
		return View::make('support.list', $this->data);
	}

	public function showContactUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.contact-us', $this->data);
	}

	public function showAboutUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.about-us', $this->data);
	}
	public function showCallUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.call-us', $this->data);
	}
	public function showFAQS(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.faqs', $this->data);
	}
	public function showAdvertising(){


		
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.advertising', $this->data);
	}
	public function showHelpUs(){
		$this->data['title'] = "Support";
		$this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
        $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
        $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
	    $this->data['countries'] = Country::all();
        $this->data['usertypes'] = Usertypes::all();
		return View::make('support.help-us', $this->data);
	}



}
   