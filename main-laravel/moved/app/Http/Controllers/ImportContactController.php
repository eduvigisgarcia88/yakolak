<?php

namespace App\Http\Controllers;


use Auth;
use Crypt;
use Input;
use Validator;
use Response;
use Hash;
use Image;
use Paginator;
use Session;
use Redirect;
use App\User;
use App\Banner;
use App\UserType;
use App\News;
use App\Client;
use App\Services;
use App\About;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Category;
use App\Newsletters;
use Request;


class ImportContactController extends Controller
{
public function loginWithGoogle(Request $request)
{
    // get data from request
    $code = $request->get('code');

    // get google service
    $googleService = \OAuth::consumer('Google');

    // check if code is valid

    // if code is provided get user data and sign in
    if ( ! is_null($code))
    {
        // This was a callback request from google, get the token
        $token = $googleService->requestAccessToken($code);

        // Send a request with it
        $result = json_decode($googleService->request('https://www.googleapis.com/oauth2/v1/userinfo'), true);

        $message = 'Your unique Google user id is: ' . $result['id'] . ' and your name is ' . $result['name'];
        echo $message. "<br/>";

        //Var_dump
        //display whole array.
        dd($result);
    }
    // if not ask for permission first
    else
    {
        // get googleService authorization
        $url = $googleService->getAuthorizationUri();

        // return to google login url
        return redirect((string)$url);
    }
}
}