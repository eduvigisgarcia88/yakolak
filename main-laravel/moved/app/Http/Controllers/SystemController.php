<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Auth;
use DB;
use View;
use App\User;
// use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
class SystemController extends Controller
{
   

	public function save(){

		  $new = true;

        $input = Input::all();
        // check if an ID is passed
        if(array_get($input, 'id')) {
            // get the user info
            $row = User::find(array_get($input, 'id'));

            if(is_null($row)) {
                return Response::json(['error' => "The requested item was not found in the database."]);
            }
            // this is an existing row
            $new = false;
        }

        $rules = [

            'photo'              => $new ? 'image|max:3000' : 'image|max:3000',
            'password'           => $new ? 'required|min:6|confirmed' : 'min:6|confirmed',
            'name'               => 'required|unique:users,name' . (!$new ? ',' . $row->id : ''),
            'email'              => 'required|email|unique:users,email' . (!$new ? ',' . $row->id : ''),
            'mobile'             => 'numeric|unique:users,mobile'. (!$new ? ',' . $row->id : ''),
            'telephone'          => 'numeric',
            'date_of_birth'      => 'date',
            'company_start_date' => 'date',
        ];

        // field name overrides
        $names = [
            'email'           => 'email address',
            'telephone'       => 'telephone',
            'country'         => 'country',
            'city'            => 'city',
            'address'         => 'address',
            'office_hours'    => 'office hours',
            'website'         => 'website',
            'gender'          => 'gender',
            'date_of_birth'   => 'date of birth',
        ];

        $branch_array            = array();
        $company_tel_no          = Input::get('company_tel_no');
        $company_mobile          = Input::get('company_mobile');
        $company_country         = Input::get('company_country');
        $company_address         = Input::get('company_address');
        $company_city            = Input::get('company_city');
        $company_email           = Input::get('company_email');
        $company_office_hours    = Input::get('company_office_hours');
        $company_branch_id       = Input::get('company_branch_id');
        // $company_website         = Input::get('company_website');
        $company_branch_status   = Input::get('company_branch_status');
        // validate child rows
        if($company_tel_no) {
            foreach($company_tel_no as $key => $val) {
                if($company_tel_no[$key]) {
                    $subrules = array(
                        "company_tel_no.$key"         => 'required|numeric',
                        "company_mobile.$key"         => 'required|numeric',
                        "company_country.$key"        => 'required',
                        "company_city.$key"           => 'required',
                        "company_email.$key"          => 'required|email',
                        "company_address.$key"        => 'required',
                        "company_office_hours.$key"   => 'required',
                       
                    );
                    $subfn = array(
                        "company_tel_no.$key"       => 'company telephone no',
                        "company_mobile.$key"       => 'company mobile no',
                        "company_country.$key"      => 'company country',
                        "company_city.$key"         => 'company city',
                        "company_email.$key"        => 'company email address',
                        "company_address.$key"      => 'company address',
                        "company_office_hours.$key" => 'company office hours',
                        
                    );
                    $rules = array_merge($rules, $subrules);
                    $names = array_merge($names, $subfn);
                    $branch_array[] = $company_tel_no[$key];
                } else {
                    unset($company_branch_id[$key]);
                    unset($company_tel_no[$key]);
                }
            }
        }

        // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }
        $confirmation_code = str_random(30);
        // create model if new
        if($new) {
       

            $row = new User;
            $row->name      = strip_tags(array_get($input, 'name'));
            $row->email     = array_get($input, 'email');
            $row->usertype_id     = array_get($input, 'usertype');
            if (array_get($input, 'usertype') == '2') {
              $row->user_plan_types_id = '5';
            } else {
              $row->user_plan_types_id = '1';
            }
            $row->mobile    = array_get($input, 'mobile');
            $row->username  = array_get($input, 'email');

            if(array_get($input, 'date_of_birth')){
               $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
             }else{
               $row->date_of_birth = null;
             }
            if(array_get($input, 'company_start_date')){
               $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
             }else{
               $row->company_start_date = null;
             }
            if(array_get($input, 'telephone')){
               $row->telephone  = array_get($input, 'telephone');
             }else{
               $row->telephone = null;
             }
             if(array_get($input, 'country')){
               $row->country  = array_get($input, 'country');
             }
             if(array_get($input, 'city')){
               $row->city  = array_get($input, 'city');
             }
             if(array_get($input, 'address')){
               $row->address  = array_get($input, 'address');
             }else{
               $row->address = null;
             }
             if(array_get($input, 'office_hours')){
               $row->office_hours  = array_get($input, 'office_hours');
             }else{
               $row->office_hours = null;
             }
             if(array_get($input, 'website')){
               $row->website  = array_get($input, 'website');
             }else{
               $row->website = null;
             }
             if(array_get($input, 'gender')){
                 $row->gender  = array_get($input, 'gender');
              }else{
                 $row->gender = null;
              }
              if(array_get($input, 'facebook_account')){
                 $row->facebook_account  = array_get($input, 'facebook_account');
              }else{
                 $row->facebook_account = null;
              }
               if(array_get($input, 'twitter_account')){
                 $row->twitter_account  = array_get($input, 'twitter_account');
              }else{
                 $row->twitter_account    = null;
              }
              if(array_get($input, 'instagram_account')){
                 $row->instagram_account  = array_get($input, 'instagram_account');
              }else{
                 $row->instagram_account = null;
              }
              if(array_get($input, 'youtube_account')){
                 $row->youtube_account  = array_get($input, 'youtube_account');
              }else{
                 $row->youtube_account   = null;
              }
            $row->usertype_id = array_get($input, 'usertype_id');
            $row->confirmation_code = $confirmation_code;
            $row->status = 2;
        } else {

            $row->name        = strip_tags(array_get($input, 'name'));
            $row->email       = array_get($input, 'email');
            $row->usertype_id = array_get($input, 'usertype');
            $row->mobile      = array_get($input, 'mobile');
            $row->username    = array_get($input, 'email');
            if(array_get($input, 'date_of_birth')){
               $row->date_of_birth  = Carbon::createFromFormat('m/d/Y', array_get($input, 'date_of_birth'));
             }else{
               $row->date_of_birth = "null";
             }
            if(array_get($input, 'company_start_date')){
               $row->company_start_date  = Carbon::createFromFormat('m/d/Y', array_get($input, 'company_start_date'));
             }
            if(array_get($input, 'telephone')){
               $row->telephone  = array_get($input, 'telephone');
             }else{
               $row->telephone           = null;
             } 
             if(array_get($input, 'country')){
               $row->country  = array_get($input, 'country');
             }
             if(array_get($input, 'description')){
               $row->description  = array_get($input, 'description');
             }else{
               $row->description         = null;
             }
             if(array_get($input, 'city')){
               $row->city  = array_get($input, 'city');
             }
             if(array_get($input, 'address')){
               $row->address  = array_get($input, 'address');
             }else{
               $row->address             = null;
             }
             if(array_get($input, 'office_hours')){
               $row->office_hours  = array_get($input, 'office_hours');
             }else{
               $row->office_hours        = null;
             }
             if(array_get($input, 'website')){
               $row->website  = array_get($input, 'website');
             }else{
               $row->website             = null;
             }
             if(array_get($input, 'gender')){
                 $row->gender            = array_get($input, 'gender');
              }else{
                 $row->gender            = null;
              }
             if(array_get($input, 'facebook_account')){
                 $row->facebook_account  = array_get($input, 'facebook_account');
              }else{
                 $row->facebook_account  = null;
              }
               if(array_get($input, 'twitter_account')){
                 $row->twitter_account   = array_get($input, 'twitter_account');
              }else{
                 $row->twitter_account   = null;
              }
               if(array_get($input, 'instagram_account')){
                 $row->instagram_account = array_get($input, 'instagram_account');
              }else{
                 $row->instagram_account = null;
              }
              if(array_get($input, 'youtube_account')){
                 $row->youtube_account   = array_get($input, 'youtube_account');
              }else{
                 $row->youtube_account   = null;
              }
              if(array_get($input, 'linkedin_account')){
                 $row->linkedin_account  = array_get($input, 'linkedin_account');
              }else{
                 $row->linkedin_account  = null;
              }
              $row->usertype_id = array_get($input, 'usertype_id');
              $row->save();
        }   

        // set type only if this isn't me
       
         

        // do not change password if old user and field is empty
        if(array_get($input, 'password')) {
          $row->password = Hash::make(array_get($input, 'password'));
        }

        // save model
        $row->save();

        if($branch_array) {
          foreach($branch_array as $key => $child) {
              if($company_branch_id[$key]) {
                  // this is an existing visa
                  $cinfo = CompanyBranch::find($company_branch_id[$key]);
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = $company_branch_status[$key];
            
              } else {
                  // this is a new visa
                  $cinfo = new CompanyBranch;
                  $cinfo->user_id = $row->id;
                  $cinfo->company_tel_no = $company_tel_no[$key];
                  $cinfo->company_mobile = $company_mobile[$key];
                  $cinfo->company_country = $company_country[$key];
                  $cinfo->company_city = $company_city[$key];
                  $cinfo->company_address = $company_address[$key];
                  $cinfo->company_email = $company_email[$key];
                  $cinfo->company_office_hours = $company_office_hours[$key];
                  // $cinfo->company_website = $company_website[$key];
                  $cinfo->status = "1";
                 
              }
                  $cinfo->save();
            

              // add to visa ID array
              if(!$company_branch_id[$key]) {
                  $company_branch_id[] = $cinfo->id;
              }
          }
      }

        if (Input::file('photo')) {
            // Save the photo
            Image::make(Input::file('photo'))->fit(300, 300)->save('uploads/' . $row->id . '.jpg');
            $row->photo = $row->id . '.jpg';
            $row->save();
        }else{
            if($new){
              $row->photo ='default_image.png';
              $row->save();
            }
        }



        $validate = User::find($row->id);

         //tagging
              $contact_name = $row->name ; 
              $user_name = $row->username;
              $user_email = $row->email;
              if ($row->telephone) {
                  $user_phone =  $row->telephone;
              } else {
                  $user_phone =  $row->mobile;
              }  
           $verification_link = '<td align="center" height="40" bgcolor="#d9534f" style="color: #fff; display: block;">
              <a href="http://yakolak.xerolabs.co/register/verify/'.$confirmation_code.'" style="color: #fff; font-size:14px; font-weight: bold; font-family: Helvetica, Arial, sans-serif; text-decoration: none; line-height: 40px; width: 100%; display: inline-block">
                 Verify your email address
              </a>
          </td>';
    //register user email validation sent
        if($new){
             $newsletter = Newsletters::where('id', '=', '10')->where('newsletter_status','=','1')->first();
            
            if ($newsletter) {
                      Mail::send('email.validate', ['verification_link' => $verification_link,'validate' => $validate,'confirmation_code' => $confirmation_code,'contact_name' => $contact_name,'user_name' => $user_name,'user_email' => $user_email,'user_phone' => $user_phone, 'newsletter' => $newsletter], function($message) use ($newsletter, $row, $validate) {
                           $message->to(''.$validate->email.'')->subject(''.$newsletter->subject.'');
                     

                       });
                }
        }
      if($new){
          return Response::json(['body' => 'Account Created<br>Please check your email for verification']);
      }else{
          return Response::json(['body' => ['Account Updated']]);
      }

	} 
}