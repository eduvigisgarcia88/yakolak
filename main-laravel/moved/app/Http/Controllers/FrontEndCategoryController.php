<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\CustomAttributes;
use App\Advertisement;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Crypt;
use App\Country;
use App\AdvertisementPhoto;
use App\UserMessagesHeader;
use App\AdvertisementType;
use App\SubAttributes;
use App\CustomAttributeValues;


class FrontEndCategoryController extends Controller
{


	public function getCategoryList(){
     $row  = Advertisement::orderBy('id', 'desc')->first();
     $this->data['title'] = "Category";
     $this->data['ads'] = Advertisement::latest()->get();
  	 $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
  	 $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
  	 $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();


      $this->data['adslatestbidsmobile'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id', 'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo',
                                          'users.name', 'countries.countryName', 'country_cities.name as city')                        
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1') 
                                                   ->where('advertisements.ads_type_id', '=', '2');                                                                             
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get(); 
     
      $this->data['adslatestbids'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')                                       
                                                   ->where('advertisements.ads_type_id', '=', '2');                                       
                                        })->orderBy('advertisements.created_at', 'desc')->take(4)->get(); 

      $this->data['adslatestbids2'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id')                           
                                      ->where(function($query) {
                                            $query->where('advertisements_photo.primary', '=', '1')
                                                 ->where('advertisements.status', '=', '1');                                
                                        })->skip(4)->take(4)->get(); 
      $this->data['latestadsmobile'] = AdvertisementPhoto::join('advertisements', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName', 'country_cities.name as city')                           
                                       ->where(function($query) {
                                            $query->where('advertisements.ads_type_id', '=', '1')
                                                  ->where('advertisements_photo.primary', '=', '1') 
                                                  ->where('advertisements.ads_type_id', '=', '1') 
                                                  ->where('advertisements.status', '=', '1');                              
                                        })->orderBy('advertisements.created_at', 'desc')->take(2)->get(); 

       $this->data['latestads'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                        'users.name', 'countries.countryName', 'country_cities.name as city')                          
                                       ->where(function($query) {
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.ads_type_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(8)->get(); 

       $this->data['adslatestbuysellxs'] = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->select('advertisements.title', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName')                          
                                       ->where(function($query) use ($row){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                  ->where('advertisements.category_id', '=', '1')
                                                   ->where('advertisements.status', '=', '1');                                       
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(2)->get();  
     $this->data['countries'] = Country::all();
     $this->data['usertypes'] = Usertypes::all();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

     $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

     return View::make('category.list', $this->data);
  }

  public function getBuyCategory(){
  		$this->data['title'] = "Buy";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.buy', $this->data);
  }

  public function getSellCategory(){
  		$this->data['title'] = "Sell";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.sell', $this->data);
  }
  public function getBidCategory(){
  	  $this->data['title'] = "Bid";
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get(); 
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      return View::make('category.bid', $this->data);
  }


  public function doListCategoryContent($id){

      $result['sort'] = Request::input('sort') ?: 'advertisements.created_at';
      $result['order'] = Request::input('order') ?: 'desc';
      $search = Request::input('search');
      $status = Request::input('status');
      $per = Request::input('per') ?: 5;

      if (Request::input('page') != '»') {
        Paginator::currentPageResolver(function () {
            return Request::input('page'); 
        });

       $rows = Advertisement::select('advertisements_photo.*','advertisements.*')
                                ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                ->where('category_id','=',$id)
                                ->where('advertisements_photo.primary','=',1)
                                ->where('advertisements_photo.status','=',1)
                                ->where(function($query) use ($status) {
                                    $query->where('advertisements.status', 'LIKE', '%' . $status . '%')
                                          ->where('advertisements.status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('advertisements.status', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
        // dd($rows);            
      } else {
        $count = Advertisement::where('category_id','=',$id)
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);

        Paginator::currentPageResolver(function () use ($count, $per) {
            return ceil($count->total() / $per);
        });

        $rows = Advertisement::where('category_id','=',$id)
                                ->where(function($query) use ($status) {
                                    $query->where('status', 'LIKE', '%' . $status . '%')
                                          ->where('status', '!=', '2');
                                  })
                                ->where(function($query) use ($search) {
                                  $query->where('name', 'LIKE', '%' . $search . '%');
                                        
                                  })
                                ->orderBy($result['sort'], $result['order'])
                                ->paginate($per);
      }

      // return response (format accordingly)
      if(Request::ajax()) {
          $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }

  }
  public function fetchCategoryList($id){

      $result = $this->doListCategoryContent($id);
      $this->data['rows'] = $result['rows'];
      // dd($this->data['rows']);
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      // dd($this->data['rows']);
      $this->data['category_title'] = Category::find($id);
      $this->data['countries'] = Country::all();
      $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();

      $this->data['usertypes'] = Usertypes::all();
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

      $this->data['countries'] = Country::orderBy('countryName','asc')->get();
      $this->data['ad_type'] = AdvertisementType::all();
      $this->data['category'] = Category::with('subcategoryinfo')
                                         ->get();
      $this->data['category_all'] = Category::with('subcategoryinfo')
                                    ->where('unique_id','=',$id)
                                    ->get();
      $this->data['cat_id'] = $id;

      return View::make('category.list',$this->data);
  }
  public function doCustomSearch(){

      $input  = Input::all();
      $ad_type = array_get($input,'ad_type');
      $category = array_get($input,'category');
      $country = array_get($input,'country');
      $city = array_get($input,'city');
      $custom_array = Input::get('custom_array');
      $dropdown_field  = Input::get('dropdown_field');
      $dropdown_attri_id  = Input::get('dropdown_attri_id');
      $price_from = Input::get('price_from');
      $price_to = Input::get('price_to');
      // dd($input);
      // $dropdown_combine_attri = array_combine($dropdown_attri_id, $dropdown_field);
      $per = Request::input('per') ? : 30;

      $rows  = Advertisement::select('advertisements_photo.*','advertisements.*','ad_custom_attribute_dropdown.attri_id','ad_custom_attribute_dropdown.attri_value_id')
                                ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                ->leftjoin('ad_custom_attribute_dropdown','ad_custom_attribute_dropdown.ad_id','=','advertisements.id')
                                ->where('advertisements_photo.primary','=',1)
                                ->where('advertisements_photo.status','=',1)
                                ->where('advertisements.status','=',1)
                                ->where(function($search) use ($ad_type,$category,$country,$city,$custom_array,$price_from,$price_to,$dropdown_field,$dropdown_attri_id){
                                              if(Input::has('ad_type')){
                                                  $search->orWhere('advertisements.ads_type_id','=',$ad_type);
                                               }
                                               if(Input::has('category')){
                                                  $search->orWhere('advertisements.category_id','=',$category);
                                               }
                                               if(Input::has('country')){
                                                  $search->orWhere('advertisements.country_id','=',$country);
                                               }
                                               if(Input::has('city')){
                                                  $search->orWhere('advertisements.city_id','=',$city);
                                               }
                                               if(Input::has('price_from') || Input::has('price_to')){
                                                  $search->where('advertisements.price','>=',$price_from)
                                                         ->where('advertisements.price','<=',$price_to);
                                               }
                                            
                                 })
                                ->where(function($dropdown_attributes) use ($dropdown_field,$dropdown_attri_id){
                                  if(Input::has('dropdown_field')){
                                              $dropdown_attributes->where('ad_custom_attribute_dropdown.attri_id','=',$dropdown_attri_id)
                                                                  ->where('ad_custom_attribute_dropdown.attri_value_id','=',$dropdown_field);
                                    }

                                })
                                ->groupBy('advertisements.id')     
                                ->paginate($per);
      
      // dd($rows);       
     if(Request::ajax()) {
          // $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows->toArray();
          return Response::json($result);
      } else {
          // $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
          $result['rows'] = $rows;
          return $result;
      }                                           
                                                   
  }
   public function fetchCustomSearch(){

      $result = $this->doCustomSearch();
      $this->data['rows'] = $result['rows'];
      // dd($this->data['rows']);
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
      $this->data['category_title'] = "";
      $this->data['countries'] = Country::all();
      $this->data['categories'] = Category::where('buy_and_sell_status','=',3)->where('biddable_status','=',3)->get();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['countries'] = Country::orderBy('countryName','asc')->get();
      $this->data['ad_type'] = AdvertisementType::all();
      $this->data['category'] = Category::with('subcategoryinfo')->get();
      $this->data['category_all'] = Category::with('subcategoryinfo')
                                  ->get(); 
      $this->data['cat_id'] = 0;                                     
      return View::make('category.list',$this->data);
  }

   public function getCustomAttributes(){
       $cat_id = Request::input('id');
       // $id = explode(" ",$cat_id);
       // $get_cust_id = CustomAttributes::find($id[0]);
       $get_sub_attri_id = SubAttributes::where('attri_id','=',$cat_id)->get();
                     $rows = '';
           foreach ($get_sub_attri_id as $key => $field) {

                 if($field->attribute_type == 1){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="'.$field->name.'"><option class="hide">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .= '<input type="text" name="dropdown_attri_id" value = "'.$field->id.'">';
                      $rows .='</select></div></div>';
                  }
                 if($field->attribute_type == 2){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9">'; 
                      $rows .= '<input type="text" name="text_field" class="form-control borderZero">';
                      $rows .='</div></div>';
                      $rows .='<input type="hidden" name="text_field_id" value="'.$field->id.'"></div>';
                  }
                 if($field->attribute_type == 3){
                      $rows .= '<div class="form-group"><h5 class="col-sm-3 normalText" for="register4-email">'; 
                      $rows .= $field->name.'</h5>';
                      $rows .= '<div class="col-sm-9"><select class="form-control borderZero" name="dropdown_field"><option class="hide" value="">Select:</option>'; 
                      $get_sub_attri_info = CustomAttributeValues::where('sub_attri_id','=',$field->id)->get();
                    foreach ($get_sub_attri_info as $key => $dropdown_info) {
                      $rows .= '<option value="' . $dropdown_info->id . '">' . $dropdown_info->name . '</option>';
                    }
                      $rows .='</select></div>';
                      $rows .='<input type="hidden" name="dropdown_attri_id" value="'.$field->id.'"></div>';
                 }           
          }
      return Response::json($rows);
    }
    public function getAdType(){

      
    }
}