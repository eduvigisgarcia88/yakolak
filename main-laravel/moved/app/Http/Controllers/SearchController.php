<?php

namespace App\Http\Controllers;


use App\Advertisement;
use App\AdvertisementPhoto;
use App\AdvertisementType;
use Auth;
use DB;
use View;
use App\User;
use App\Usertypes;
use App\Category;
use App\SubCategory;
use App\Http\Controllers\Controller;
use Hash;
use Validator;
use Redirect;
use Session;
use Input;
use Request;
use Paginator;
use Response;
use Image;
use Carbon\Carbon;
use App\Country;
use App\BannerSubscriptions;
use App\UserMessagesHeader;
use App\CustomAttributesForDropdown;
use App\UserNotificationsHeader;

class SearchController extends Controller
{

	public function browse(){
    $result = $this->doSearch();
		$this->data['title'] = "Search";
    $this->data['rows'] = $result['rows'];


    $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();  

		

    $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
		$this->data['countries'] = Country::orderBy('countryName','asc')->get();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['ad_type'] = AdvertisementType::all();
    $this->data['category'] = Category::with('subcategoryinfo')->get();
    $this->data['banner_placement_top'] = BannerSubscriptions::where('status','=',1)->where('placement','=',1)->get()->random(1);
		$this->data['banner_placement_bottom'] = BannerSubscriptions::where('status','=',1)->where('placement','=',2)->get()->random(1);
    //dd($this->data['banners']);
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
    }

    $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();

    return View::make('category.search', $this->data);


	}
  public function index(){

    $this->data['title'] = "Search";
    $this->data['featured_ads']  = Advertisement::join('advertisements_photo', 'advertisements_photo.ads_id', '=', 'advertisements.id') 
                                       ->leftJoin('users','users.id', '=', 'advertisements.user_id')
                                       ->leftJoin('countries','countries.id', '=', 'users.country')
                                       ->leftJoin('country_cities','country_cities.id', '=', 'users.city')
                                       ->select('advertisements.title','advertisements.user_id', 'advertisements.category_id',  'advertisements.price', 'advertisements.id', 'advertisements.created_at', 'advertisements.description', 'advertisements_photo.photo', 
                                         'users.name', 'countries.countryName',  'country_cities.name as city')                          
                                       ->where(function($query){
                                             $query->where('advertisements_photo.primary', '=', '1')
                                                   ->where('advertisements.status', '=', '1')
                                                   ->where('advertisements.feature', '=', '1');                                      
                                        })->orderBy('advertisements_photo.created_at', 'desc')->take(4)->get();  

                                       
    $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
    $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
    $this->data['categories'] = Category::where('buy_and_sell_status','!=',2)->where('biddable_status','=',2)->get();
    $this->data['countries'] = Country::orderBy('countryName','asc')->get();
    $this->data['usertypes'] = Usertypes::all();
    $this->data['ad_type'] = AdvertisementType::all();
    $this->data['category'] = Category::with('subcategoryinfo')->get();
    if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }
    $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
  $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
    return View::make('category.search', $this->data);

  }
  public function doSearch(){

    $input = Input::all();
    // dd($input);
    $keyword = array_get($input,'keyword') ? : '*';
    $input  = Input::all();
    $ad_type = array_get($input,'ad_type');
    $category = array_get($input,'category');
    $country = array_get($input,'country');
    $city = array_get($input,'city');
    $custom_array = Input::get('custom_array');
    $dropdown_field  = Input::get('dropdown_field');
    $dropdown_attri_id  = Input::get('dropdown_attri_id');
    $price_from = Input::get('price_from');
    $price_to = Input::get('price_to');
    $per = Request::input('per') ? : 30;
    $row  = Advertisement::select('advertisements_photo.*','advertisements.*','ad_custom_attribute_dropdown.attri_id','ad_custom_attribute_dropdown.attri_value_id')
                                ->leftjoin('advertisements_photo','advertisements_photo.ads_id','=','advertisements.id')
                                ->leftjoin('ad_custom_attribute_dropdown','ad_custom_attribute_dropdown.ad_id','=','advertisements.id')
                                ->where('advertisements_photo.primary','=',1)
                                ->where('advertisements_photo.status','=',1)
                                ->where('advertisements.status','=',1)
                                ->where(function($search) use ($ad_type,$category,$country,$city,$custom_array,$price_from,$price_to,$dropdown_field,$dropdown_attri_id,$keyword){
                                              if(Input::has('ad_type')){
                                                  $search->orWhere('advertisements.ads_type_id','=',$ad_type);
                                               }
                                               if(Input::has('category')){
                                                  $search->orWhere('advertisements.category_id','=',$category);
                                               }
                                               if(Input::has('country')){
                                                  $search->orWhere('advertisements.country_id','=',$country);
                                               }
                                               if(Input::has('city')){
                                                  $search->orWhere('advertisements.city_id','=',$city);
                                               }
                                               if(Input::has('keyword')){
                                                   $search->orWhere('advertisements.title','LIKE','%'.$keyword.'%');
                                               }                       
                                 })
                                ->where(function($dropdown_attributes) use ($dropdown_field,$dropdown_attri_id){
                                  if(Input::has('dropdown_field')){
                                              $dropdown_attributes->where('ad_custom_attribute_dropdown.attri_id','=',$dropdown_attri_id)
                                                                  ->where('ad_custom_attribute_dropdown.attri_value_id','=',$dropdown_field);
                                    }

                                })
                                ->where(function($price_range) use ($price_from,$price_to){
                                    if(Input::has('price_from') || Input::has('price_to')){
                                                  $price_range->whereBetween('advertisements.price', [$price_from, $price_to]);
                                     }
                                 })
                                ->groupBy('advertisements.id')     
                                ->paginate($per);



                    // dd($row);
    if(Request::ajax()) {
       $result['rows'] = $row->toArray();
       return Response::json($result);
    } else {
       $result['rows'] = $row;
       return $result;
    }                                                      

  }
  
  

}