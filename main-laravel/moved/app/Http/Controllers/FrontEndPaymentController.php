<?php 
namespace App\Http\Controllers;

use View;
use App\Http\Requests;
use App\AdvertisementPhoto;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Response;
use App\Advertisement;
use App\CustomAttributes;
use App\SubAttributes;
use App\CustomAttributeValues;
use App\PromotionPopups;
use Request;
use App\Category;
// use App\AdvertisementWatchlist;
use App\AdvertisementWatchlists;
use App\Country;
use App\City;
use App\Usertypes;
use Excel;
use Response;
use Auth;
use Redirect;
use Input;
use Carbon\Carbon;
use App\UserMessages;
use App\UserMessagesHeader;
use App\UserPlanTypes;
use App\PlanPrice;
use Session;
use App\User;
use App\PaypalPayments;
use App\BannerPlacement;
use App\BannerPlacementPlans;
use App\UserNotificationsHeader;
use Paypalpayment;
use Validator;




class FrontEndPaymentController extends Controller {

private $_apiContext;

     function __construct()
    {

        $this->_apiContext = Paypalpayment::apiContext(config('paypal_payment.Account.ClientId'), config('paypal_payment.Account.ClientSecret'));

    }

 public function index()
    {
        echo "<pre>";

        $payments = Paypalpayment::getAll(array('count' => 1, 'start_index' => 0), $this->_apiContext);

        // dd($addr);
    }

    public function create()
    {

        return View('payment.test');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function purchasePlan()
    {
        $input = Input::all();
        $plan_id = Input::get('plan_id');
        $price_chosen = Input::get('price_chosen');
        //dd($input);
        $get_plan = UserPlanTypes::where('id','=',$plan_id)->first();
        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        $user_id = Auth::user()->id;
        $get_user = User::find($user_id)->first();
 
        $addr= Paypalpayment::address();
        $addr->setLine1("3909 Witmer Road");
        $addr->setLine2("Niagara Falls");
        $addr->setCity($get_user->city);
        $addr->setState("NY");
        $addr->setPostalCode("14305");
        $addr->setCountryCode("US");
        $addr->setPhone($get_user->telephone);

        // ### CreditCard
        $card = Paypalpayment::creditCard();
        $card->setType("visa")
            ->setNumber("4758411877817150")
            ->setExpireMonth("05")
            ->setExpireYear("2019")
            ->setCvv2("456")
            ->setFirstName("Joe")
            ->setLastName("Shopper");

        // ### FundingInstrument
        // A resource representing a Payer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        $fi = Paypalpayment::fundingInstrument();
        $fi->setCreditCard($card);

        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("credit_card")
       
            ->setFundingInstruments(array($fi));

        $item1 = Paypalpayment::item();
        $item1->setName('Ground Coffee 40 oz')
                ->setDescription($get_user->name)
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setTax(0.3)
                ->setPrice(7.50);

        $item2 = Paypalpayment::item();
        $item2->setName('Granola bars')
                ->setDescription('Granola Bars with Peanuts')
                ->setCurrency('USD')
                ->setQuantity(5)
                ->setTax(0.2)
                ->setPrice(2);


        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1,$item2));


        $details = Paypalpayment::details();
        $details->setShipping("1.2")
                ->setTax("1.3")
                //total of items prices
                ->setSubtotal("17.5");

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
                // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
                ->setTotal("20")
                ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {
            // ### Create Payment
            // Create a payment by posting to the APIService
            // using a valid ApiContext
            // The return object contains the status;
            $payment->create($this->_apiContext);
        } catch (\PPConnectionException $ex) {
            return  "Exception: " . $ex->getMessage() . PHP_EOL;
            exit(1);
        }

$new = true;
$row = Paypalpayments::where('user_id','=', Auth::user()->id)->where('plan_id','=',$plan_id)->where('status','=','1')->first();
if (is_null($row)) {
$row = new Paypalpayments;
$row->user_id = Auth::user()->id;
$row->plan_id = $plan_id;
$row->invoice_number = $transaction->invoice_number;
$row->status = '1';
$row->save();
Session::put('invoice_number', $transaction->invoice_number);

$new = false;
} else {
Session::put('invoice_number', $row->invoice_number);
}
     
        $this->data['get_user'] = $get_user;
        $this->data['payment'] = $payment;
$form ="";
$form .='
    <div class="panel-heading"><span class="mediumText grayText"><strong>UPGRADE TO '.strtoupper($get_plan->plan_name).' PLAN </strong></span></div>
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" id="element-to-hide" value="tejerogerald123@gmail.com">
    <input type="hidden" name="item_name" value="'.strtoupper($get_plan->plan_name).' ">
    <input type="hidden" name="currency_code" value="USD">
    <input id="discount_amount" name="discount_amount" type="hidden" value="4.00">
    <div class="form-group bottomMargin">
    <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">
    <input type="hidden" name="on0" value="Period">Period</label>
    <div class="inputGroupContainer">
         <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
    <p>60% OFF THE REGULAR $9.95 MO</p>
    <select name="amount" class="form-control borderZero inputBox fullSize" >';
$plan_prices = PlanPrice::where('usertype_id','=',Auth::user()->usertype_id)->where('user_plan_id','=',$plan_id)->where('status','=','1')->get();
   foreach ($plan_prices as $plan_price) {
     $form .='<option value="'.$plan_price->price.'" '.($plan_price->id==$price_chosen ? 'selected':'' ).'>'.$plan_price->priceDescription.'</option>';
   }        
$form .='</select></div></div></div>
    <div class="form-group bottomMargin">
    <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label>
    <div class="inputGroupContainer">
    <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
    <input type="hidden" name="currency_code" value="USD">
    <input type="image" src="'.url('/').'/'.'img/paypal-checkout-button3.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
    </div></div></div></form>';
$this->data['form'] = $form;

      $value = Session::token();
      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['trans_id_number'] = $transaction->invoice_number;
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

   $this->data['get_userinfo'] = User::find($user_id);
if (Auth::user()->country != null) {
   $this->data['get_usercountry'] = Country::find(Auth::user()->country);
  
} else {

$this->data['get_usercountry'] = null;
  
}

   $this->data['get_usercity'] = city::find(Auth::user()->city);
   $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
   $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();                                                 
       return View('payment.plan.form', $this->data);
    } 

       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function success() 
    {

     $this->data['invoincecode'] = Session::get('invoice_number');




      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();
      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

      $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                      // ->OrWhere('sender_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      $this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
       return View('payment.success', $this->data);
    }


       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

    public function saveBannerPlacement()
    {
        $new = true;

        $input = Input::all();

        $title = Input::get('title');
        $start_date = Input::get('start_date');
        $country = Input::get('country');
        $language = Input::get('language');
        $page = Input::get('page');
        $banner_placement_id = Input::get('banner_placement_id');
        $end_date = Input::get('end_date');
        $banner_placement_plan_id = Input::get('banner_placement_plan_id');
        $link = Input::get('link');
        $photo = Input::get('photo');

        Session::put('title', $title);
        Session::put('start_date', $start_date);
        Session::put('country', $country);
        Session::put('language', $language);
        Session::put('page', $page);
        Session::put('banner_placement_id', $banner_placement_id);
        Session::put('end_date', $end_date);
        Session::put('banner_placement_plan_id', $banner_placement_plan_id);
        Session::put('link', $link);
        Session::put('photo', $photo);

        Session::put('banner_transaction_number', "BP-".strtoupper(uniqid()));

      $rules = [

            'title'                      => 'required|max:40',
            'start_date'                 => 'required',
            'country'                    => 'required',
            'language'                   => 'required',
            'page'                       => 'required',
            'category'                   => 'required',
            'banner_placement_id'        => 'required',
            'banner_placement_plan_id'   => 'required',
            'end_date'                   => 'required',
            'photo'                      => 'image|max:3000|'.($page == 1? 'image_size:<=295,<=390' :'').($page == 2 ? 'image_size:<=850,<=120':''),

        ];
        // field name overrides
        $names = [
            'title'                     => 'Title',
            'start_date'                => 'Start Date',
            'country'                   => 'Country',
            'language'                  => 'Language',
            'page'                      => 'Page',
            'category'                  => 'Category',
            'banner_placement_id'       => 'Banner placement',
            'banner_placement_plan_id'  => 'Banner placement price',
            'end_date'                  => 'Expiration Date',
            'photo'                     => 'Photo',
           
        ];

  // do validation
        $validator = Validator::make(Input::all(), $rules);
        $validator->setAttributeNames($names);

        // return errors
        if($validator->fails()) {
            return Response::json(['error' => array_unique($validator->errors()->all())]);
        }

  
 if($new){
      return Response::json(['body' => "Advertisement Created"]);
    } 
    else {
         return Response::json(['body' => "Advertisement Successfully updated"]);
       }
     }

       /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */ 

           public function purchaseBannerPlacement()
    {
     $title = Session::get('title');
     $start_date = Session::get('start_date');
     $country = Session::get('country');
     $language = Session::get('language');
     $page = Session::get('page');
     $category = Session::get('category');
     $banner_placement_id = Session::get('banner_placement_id');
     $duration = Session::get('duration');
     $end_date = Session::get('end_date');
     $banner_placement_plan_id = Session::get('banner_placement_plan_id');
     $link = Session::get('link');
     $photo = Session::get('photo');

     $get_banner_placement    = BannerPlacement::where('id','=', Session::get('banner_placement_id'))->first();
        
     $this->data['title']        = Session::get('title');
     $this->data['placement']    = $get_banner_placement->name;
     $this->data['start_date']   = Session::get('start_date');
     $this->data['end_date']     = Session::get('end_date');
     $this->data['banner_transaction_number']    = Session::get('banner_transaction_number');

    $form ="";
    $form .='
        <div class="panel-heading"><span class="mediumText grayText"><strong>PURCHASE '.strtoupper($get_banner_placement->name).'</strong></span></div>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
        <input type="hidden" name="cmd" value="_xclick">
        <input type="hidden" name="business" id="element-to-hide" value="tejerogerald123@gmail.com">
        <input type="hidden" name="item_name" value="'.strtoupper($get_banner_placement->name).' ">
        <input type="hidden" name="currency_code" value="USD">
        <input id="discount_amount" name="discount_amount" type="hidden" value="4.00">
        <div class="form-group bottomMargin">
        <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">
        <input type="hidden" name="on0" value="Period">Period</label>
        <div class="inputGroupContainer">
             <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
        <p>60% OFF THE REGULAR $9.95 MO</p>
        <select name="amount" class="form-control borderZero inputBox fullSize" >';
    $banner_prices = BannerPlacementPlans::where('banner_placement_id','=',$get_banner_placement->id)->where('status','=','1')->get();
         foreach ($banner_prices as $banner_price) {
           $form .='<option value="'.$banner_price->price.'" '.($banner_price->id==Session::get('banner_placement_plan_id') ? 'selected':'' ).'>'.'$'.$banner_price->price.' USD '.$banner_price->duration.' days'.'</option>';
         }        
    $form .='</select></div></div></div>
      <div class="form-group bottomMargin">
      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label>
      <div class="inputGroupContainer">
           <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
      <input type="hidden" name="currency_code" value="USD">
      <input type="image" src="'.url('/').'/'.'img/paypal-checkout-button3.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
      <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
      </div></div></div></form>';
        
    $this->data['form'] = $form;

      $row  = Advertisement::orderBy('id', 'desc')->first();
      $this->data['current_time'] = Carbon::now()->format('Y-m-d H:i:s');  
      $this->data['buy_and_sell'] = Category::where('buy_and_sell_status','=',2)->get();
      $this->data['biddable'] = Category::where('buy_and_sell_status','=',1)->get();
      $this->data['countries'] = Country::all();
      $this->data['usertypes'] = Usertypes::all();
      $this->data['categories'] = Category::where('biddable_status','=','3')->where('buy_and_sell_status','=','3')->get();

      if(Auth::check()){
          $user_id = Auth::user()->id;
      }else{
          $user_id = null;
      }

     $this->data['get_userinfo'] = User::find($user_id);
     if (Auth::user()->country != null) {
         $this->data['get_usercountry'] = Country::find(Auth::user()->country);
        
      } else {
      $this->data['get_usercountry'] = null;  
      }

     $this->data['get_usercity'] = city::find(Auth::user()->city);
     $this->data['messages_counter']  = UserMessagesHeader::where(function($query) use ($user_id){
                                                      $query->where('reciever_id','=',$user_id);
                                                        // ->OrWhere('sender_id','=',$user_id);
                                                      })->where(function($status){
                                                         $status->where('read_status','=',1);
                                                      })->count();
$this->data['notifications_counter']  = UserNotificationsHeader::where(function($query) use ($user_id){
                                                    $query->where('reciever_id','=',$user_id);
                                                    })->where(function($status){
                                                       $status->where('read_status','=',1);
                                                    })->count();
      return view('payment.banner.form', $this->data);

    }

}