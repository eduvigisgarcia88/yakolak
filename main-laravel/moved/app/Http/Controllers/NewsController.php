<?php

namespace App\Http\Controllers;

use Auth;
use Crypt;
use Input;
use Request;
use Validator;
use Response;
use Hash;
use Image;
use Redirect;
use App\User;
use App\UserType;
use App\Profile;
use App\News;
use App\Contact;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;

class NewsController extends Controller
{

    /**
     * Middleware
     *
     * @return Response
     */
    public function __construct() {
        $this->middleware('auth', ['except' => ['view', 'all']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['title_arabic'] = 'أخبار';
        $this->data['url'] = url("config/news");
        $this->data['title'] = "News";
        return view('news.list', $this->data);
    }

    /**
     * Build the list
     *
     */
    public function doList() {
        // sort and order
        $result['sort'] = Request::input('s') ?: 'created_at';
        $result['order'] = Request::input('o') ?: 'desc';

        $rows = News::orderBy($result['sort'], $result['order'])->paginate(10);

        // return response (format accordingly)
        if(Request::ajax()) {
            $result['pages'] = str_replace('/refresh/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows->toArray();
            return Response::json($result);
        } else {
            $result['pages'] = str_replace('/?', '?', $rows->appends(['s' => $result['sort'], 'o' => $result['order']])->render());
            $result['rows'] = $rows;
            return $result;
        }
    }

    /**
     * Save model
     *
     */
    public function save(NewsRequest $request) {    

        $input = Request::all();

        $row = new News;

        $row->title_eng     = array_get($input, 'title_eng');
        $row->desc_eng      = array_get($input, 'desc_eng');
        $row->title_arabic  = array_get($input, 'title_arabic');
        $row->desc_arabic   = array_get($input, 'desc_arabic');
        $row->photo         = 'no_photo.jpg';
        $row->status        = 1;

        // save model
        $row->save();

        $file               = array_get($input, 'photo');
        $destinationPath    = 'uploads/news';
        $extension          = $file->getClientOriginalExtension();
        $fileName           = $row->id . '.' . $extension;
        $upload_success     = $file->move($destinationPath, $fileName);

        if ($upload_success) {
            // create thumbnail
            $img = Image::make('uploads/news/' . $fileName);
            $img->fit(125);
            $img->save('uploads/news/thumbnail/' . $fileName);

            $photo          = News::find($row->id);
            $photo->photo   = $row->id . '.' . $extension;
            $photo->save();
        }

        $url = url('news/view/' . $row->id);

        // return error
        return redirect()->action('NewsController@index')
                       ->with('notice', array(
                            'msg' => 'News successfully added.',
                            'url' => $url,
                            'type' => 'success'
                       ));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $this->data['title'] = "Create News";
        return view('news.create', $this->data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function view($id)
    {        
        if (is_null(News::find($id))) {
            return abort(404);
        } else {
            $this->data['news'] = News::find($id);

            $this->data['contact'] = Contact::first();
            $this->data['title'] = $this->data['news']->title_eng;

            $this->data['title_arabic'] = $this->data['news']->title_arabic;
            return view('news.view', $this->data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function all()
    {
        // get row set
        $result = $this->doList();

        // assign vars to template
        $this->data['rows'] = $result['rows'];
        $this->data['pages'] = $result['pages'];
        $this->data['sort'] = $result['sort'];
        $this->data['order'] = $result['order'];

        $this->data['contact'] = Contact::first();

        $this->data['title_arabic'] = 'أخبار';
        $this->data['url'] = url("config/news");
        $this->data['title'] = "News";
        $this->data['profiles'] = Profile::orderBy('created_at', 'desc')->take(1)->first();
        return view('news.all', $this->data);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (is_null(News::find($id))) {
            return abort(404);
        } else {
            $this->data['news'] = News::find($id);

            $this->data['title'] = $this->data['news']->title_eng;

            return view('news.edit', $this->data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        $input = Input::all();

        // $rules = [
        //     'title_eng' => 'required',
        //     'desc_eng' => 'required',
        //     'title_arabic' => 'required',
        //     'desc_arabic' => 'required',
        // ];

        // $names = [
        //     'title_eng' => 'news title (english)', 
        //     'desc_eng' => 'description (english)', 
        //     'title_arabic' => 'news title (arabic)', 
        //     'desc_arabic' => 'description (arabic)',
        // ];

        // if(array_get($input, 'photo')) {
        //     $rules = [
        //         'photo' => 'required|image|max:3000'
        //     ];
        // }

        // // do validation
        // $validator = Validator::make(Input::all(), $rules);
        // $validator->setAttributeNames($names); 

        // // return errors
        // if($validator->fails()) {
        //     return redirect('config/news/' . $id . '/edit')
        //                 ->withErrors($validator)
        //                 ->withInput();
        // }



        $row = News::find($id);
        $row->title_eng     = array_get($input, 'title_eng');
        $row->desc_eng      = array_get($input, 'desc_eng');
        $row->title_arabic  = array_get($input, 'title_arabic');
        $row->desc_arabic   = array_get($input, 'desc_arabic');

        // save model
        $row->save();

        if(array_get($input, 'photo')) {
            $file               = array_get($input, 'photo');
            $destinationPath    = 'uploads/news';
            $extension          = $file->getClientOriginalExtension();
            $fileName           = $id . '.' . $extension;
            $upload_success     = $file->move($destinationPath, $fileName);

            if ($upload_success) {
                // create thumbnail
                $img = Image::make('uploads/news/' . $fileName);
                $img->fit(125);
                $img->save('uploads/news/thumbnail/' . $fileName);

                $photo          = News::find($row->id);
                $photo->photo   = $row->id . '.' . $extension;
                $photo->save();
            }
        }

        $url = url('news/view/' . $row->id);

        return redirect('config/news')
                ->with('notice', array(
                        'msg' => 'News successfully updated.',
                        'url' => $url,
                        'type' => 'success'
                    ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        News::destroy($id);

        return redirect('config/news')
                ->with('notice', array(
                        'msg' => 'News successfully removed.',
                        'url' => 'none',
                        'type' => 'success'
                    ));
    }

    /**
     * Delete the user
     *
     */
    public function delete() {
        $row = News::find(Request::input('id'));

        // check if user exists
        if(!is_null($row)) {
            News::destroy(Request::input('id'));

            // return
            return Response::json(['body' => 'News has been deleted.']);
        } else {
            // not found
            return Response::json(['error' => "The requested item was not found in the database."]);
        }
    }

}
