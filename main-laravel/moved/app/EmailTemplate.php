<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTemplate extends Model {

	protected $fillable = [
		'ads_id',
		'photo'
		];

		protected $date = ['published_at'];


 		protected $table = 'newsletters';

		// /** An articles is owned by a user. **/
		public function user()
		{
			return $this->belongsTo('App\Advertisement', 'ads_id');
		}
}
