@extends('layouts.frontend')

@section('scripts')

@stop

@section('content')

@if(Session::has('locale'))

    @if (Session::get('locale') == 'sa')

        @include('promo.sa.view')

    @else

        @include('promo.en.view')

    @endif

@else

    @include('promo.en.view')

@endif

@stop