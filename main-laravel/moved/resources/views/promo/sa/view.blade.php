<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque" style="background-color: #ffffff !important;">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo sa-logo" src="{{ url('/') }}/uploads/logo/samaya-logo-ar.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-left" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">الرئيسية</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#about-us">حول نحن</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#services">خدمات العياده</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#medical-team">لفريق الطبى</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#news">الفريق الطبي</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- Jumbotron -->
<section id="jumbotron" class="promo">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">سمايا عيادة المتقدم تجميل الاسنان</p>
            {!! Form::open(array('url' => 'language', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <select name="language">
                    <option value="sa">عربي</option>
                    <option value="en">English</option>
                </select>
                <button type="submit" class="btn btn-xs">تغيير</button>
            {!! Form::close() !!}
        </div>
    </div>
	<div id="promo-content" class="container">
		<img src="{{ url('/') }}/uploads/promo/{{ $promo->photo }}" onerror="this.style.display='none'">
		<h1>{{ $promo->title_arabic }}</h1>
		<span><pre>{{ $promo->desc_arabic }}</pre></span>
	</div>
</section>