@extends('layouts.frontend')

@section('scripts')

@stop

@section('content')

<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo" src="{{ url('/') }}/uploads/logo/samaya-logo.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll active" href="#jumbotron">HOME</a></li>
            <li><a class="smoothscroll" href="#about-us">ABOUT US</a></li>
            <li><a class="smoothscroll" href="#services">SERVICES</a></li>
            <li><a class="smoothscroll" href="#medical-team">MEDICAL TEAM</a></li>
            <li><a class="smoothscroll" href="#news">NEWS</a></li>
            <li><a class="smoothscroll" href="#magazine">MAGAZINE</a></li>
            <li><a class="smoothscroll" href="#gallery">GALLERY</a></li>
            </ul>
        </div>
    </div>
</nav>
<section id="jumbotron" class="news">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">Samaya Clinic Advanced Cosmetic Dentistry</p>
            <p>
            @if(Auth::guest())
            <a href="{{ url('login') }}">LOGIN</a>
            @else
            <a href="{{ url('config') }}">Welcome! {{ Auth::user()->name }} | </a>
            <a href="{{ url('logout') }}">LOGOUT</a>
            @endif
            </p>
        </div>
    </div>
	<div id="news" class="container" style="padding-top: 120px; padding-bottom: 60px;">
        @foreach ($rows as $row)
            <div class="row news-info">
                <a href="{{ url('news/view/' . $row->id )}}"><div class="pull-left news-img" style="background-image: url({{ url('/') }}/uploads/news/{{ $row->photo }});"></div></a>
                <p class="news-title"><a href="{{ url('news/view/' . $row->id )}}">{{ $row->title_eng }}</a></p>
                <p class="news-date">{{ date("F j, Y  h:i A", strtotime($row->created_at)) }}</p>
                <p class="news-article">{{ substr($row->desc_eng, 0, 310) }}...</p>
                <p class="news-readmore"><a href="{{ url('news/view/' . $row->id )}}">READ MORE</a></p>
            </div>
        @endforeach
        <div id="row-pages" class="text-center">{!! $pages !!}</div>
	</div>
</section>
@stop