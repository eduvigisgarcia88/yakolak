@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

	// refresh the list
	function refresh() {
			var sort = urlData('s') || '';
			var order = urlData('o') || '';
			var page = urlData('page') || '';
			var loading = $("#load-list");
			var table = $("#rows");
			$_token = "{{ csrf_token() }}";
			var url = "{{ url('/') }}";
			loading.removeClass('hide');
			
			$.post("{{ url('config/promo/refresh') }}", { s: sort, o: order, page: page, _token: $_token }, function(response) {
					// clear table
					table.html("");

					// populate table
					$.each(response.rows.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							table.append(
									'<tr data-id="' + row.id + '">' +
									'<td><img src="' + url + "/uploads/promo/thumbnail/" + row.photo + '" onerror="this.style.display="none"" style="max-width: 150px;" class="img-responsive"></td>' +
									'<td>' + row.title_eng + '</td>' +
									'<td>' + desc_eng + '</td>' +
									'<td>' + moment(row.created_at).format('LLL') + '</td>' +
									'<td class="col-lg-2 text-right">' + 
										'<a target="_blank" href="' + url + '/promo/view/' + row.id + '" class="btn btn-xs btn-success btn-view" title="View promo" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
										'&nbsp;<a href="' + url + '/config/promo/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit promo" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
										'&nbsp;<a class="btn btn-xs btn-danger btn-delete" title="Delete promo" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// update links
					$("#row-pages").html(response.pages);

					loading.addClass('hide');
			}, 'json');
	}
	
	$(document).ready(function() {

		refresh();
		// delete promo
		$("#promo-lists").on("click", ".btn-delete", function() {
	  		var id = $(this).parent().parent().data('id');
	  		var name = $(this).parent().parent().find('td:nth-child(2)').html();

			$(".modal-header").removeAttr("class").addClass("modal-header delete-bg");

	  		dialog('promo Deletion', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('config/promo/delete') }}", id);
		});
	});

</script>
@stop

@section('content')

<div class="container">
    @if (Session::has("notice"))
        <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }} <a target="_blank" href="{{ Session::get('notice.url') }}">View Here</a>
        </div>
    @endif
    <div class="panel panel-default panelShadow">
	  <div class="panel-heading">
	  	<h4>FEATURED ARTICLES
	  		<span class="pull-right"><a class="btn btn-addnew" href="{{ url('config/promo/create') }}" style="margin: -6px 0 0px;"><i class="fa fa-plus"></i> Add New</a></span>
	  	</h4>
	  </div>
	  <div class="panel-body">
	  	<div class="table-responsive padding-none">
			<table id="promo-lists" class="table panelShadow">
				<thead>
					<tr>
						<th>Photo</th>
						<th><a href="{{ $url . '?s=title_eng&amp;o=' . ($sort == 'title_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">promo TITLE{!! ($sort == 'title_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
						<th><a href="{{ $url . '?s=desc_eng&amp;o=' . ($sort == 'desc_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">DESCRIPTION{!! ($sort == 'desc_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
						<th><a href="{{ $url . '?s=created_at&amp;o=' . ($sort == 'created_at' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">CREATED AT{!! ($sort == 'created_at' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>			
						<th><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
					</tr>
				</thead>
				<tbody id="rows">
				</tbody>
			</table>
		</div>
	  </div>
	  	<div class="container-fluid marginTopZero">
	  	<div id="row-pages" class="text-right">{!! $pages !!}</div>
	  	</div>
	</div>
</div>
@stop