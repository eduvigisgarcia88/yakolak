@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">
	    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2,
           toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });


    /* $('#row-photo').on('change', function() {
		var tmppath = URL.createObjectURL(event.target.files[0]);
    	$("img#preview").attr('src',tmppath);
    }); */

	$("input.file").fileinput({
	        maxFileCount: 1,
        	maxFileSize: 1024,
        	allowedFileTypes: ['image'],
			allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
	});

	$(".btn-change").on('click', function() {
		$("#post-photo").addClass("hide");
		$("#change-photo").removeClass("hide");
		$("#row-photo").attr('required', 'required');
	});

	$(".btn-cancel-change").on('click', function() {
		$("#post-photo").removeClass("hide");
		$("#change-photo").addClass("hide");
		$("#row-photo").trigger('reset');
		$("#txt-change").val("");
		$("#row-photo").removeAttr('required');
	});

	$("#row-photo").on('change', function() {
		$("#txt-change").val("change");
	});

	$(".fileinput-remove").on('click', function() {
		$("#txt-change").val("");
	});

</script>
@stop

@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($promo, ['action' => ['PromoController@update', $promo->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
  <!-- Nav tabs -->
  <ul class="myNavs nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a class="borderZero" href="#english" aria-controls="english" role="tab" data-toggle="tab">English</a></li>
    <li role="presentation"><a class="borderZero" href="#arabic" aria-controls="arabic" role="tab" data-toggle="tab">Arabic</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content myTabs">
    <div role="tabpanel" class="tab-pane active" id="english">
    	<div class="container-fluid">
  		<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-title_eng', 'Promo Title (English)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
				{!! Form::text('title_eng', null, ['class' => 'form-control', 'id' => 'row-title_eng']) !!}
			</div>
			</div>
			<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-desc_eng', 'Body (English)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
		 		{!! Form::textarea('desc_eng', null, ['class' => 'form-control summernote', 'id' => 'row-desc_eng']) !!}
		
			</div>
			</div>
	    </div>
	  </div>
    <div role="tabpanel" class="tab-pane" id="arabic">
    	<div class="container-fluid">
  		<div class="form-group">
			<div class="col-sm-2">
				{!! Form::label('row-title_arabic', 'Promo Title (Arabic)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-10">
				{!! Form::text('title_arabic', null, ['class' => 'form-control', 'id' => 'row-title_arabic', 'style' => 'direction: rtl;']) !!}
			</div>
			</div>
			<div class="form-group">
			<div class="col-sm-2">
				{!! Form::label('row-desc_arabic', 'Body (Arabic)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-10">
		 		{!! Form::textarea('desc_arabic', null, ['class' => 'form-control summernote', 'id' => 'row-desc_arabic']) !!}
		
			</div>
			</div>
	    </div>
    </div>
  </div>
<div class="pan panel panel-default borderZero">
  <div class="panel-body">
  	<div class="col-sm-4">
				{!! Form::label('row-photo', 'Cover Photo', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-8">
					<div id="post-photo">
					<img style="max-width: 200px;" onerror="this.style.display='none'" src="{{ url('/')  . '/uploads/promo/' . $promo->photo }}"/>
				<a id="txt-change" class="btn btn-primary btn-sm btn-change borderZero pull-right">Change</a>
			</div>
		</div>
		<div class="col-sm-4">
		</div>
		
		<div class="col-sm-8">
			<div class="form-group hide" id="change-photo">
			{!! Form::file('photo', ['class' => 'file pull-right', 'id' => 'row-photo', 'data-show-upload' => 'false', 'data-show-caption' => 'true', 'multiple' => 'true']) !!}
		
		<input type="hidden" id="txt-change"><a class="btn btn-cancel-change pull-right">Cancel</a>
		</div>
	</div>
  </div>
</div>
<div class="pan panel panel-default borderZero">
  <div class="panel-body">
  	<div class="row">
  		<div class="col-lg-6">
  			<a class="btn btn-goback btn-warning borderZero" href="{{ url('config/promo') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
  		</div>
  		<div class="col-lg-6 text-right">
  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update Promo</button>
  		</div>
  	</div>
  </div>
</div>
</div>
{!! Form::close() !!}

	<!-- <div class="well">
		<div class="form-group hide" id="tester">
			<img style="max-width: 200px;" id="preview" src=""/>
		</div>
		</center>
			<a class="btn btn-goback" href="{{ url('config/news') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-sm-3 text-right">
			{!! Form::label('row-photo', 'Photo', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::file('photo', ['class' => 'file', 'id' => 'row-photo', 'data-show-upload' => 'false', 'data-show-caption' => 'true', 'multiple' => 'true' ]) !!}
			
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3 text-right">
			{!! Form::label('row-title_eng', 'News Title (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('title_eng', null, ['class' => 'form-control', 'id' => 'row-title_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3 text-right">
			{!! Form::label('row-title_arabic', 'News Title (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('title_arabic', null, ['class' => 'form-control', 'id' => 'row-title_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3 text-right">
			{!! Form::label('row-desc_eng', 'Body (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
		 	{!! Form::textarea('desc_arabic', null, ['class' => 'form-control summernote arabic', 'id' => 'row-desc_eng summernote', 'style' => 'direction: rtl;text-align: right !important;']) !!}
			
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3 text-right">
			{!! Form::label('row-desc_arabic', 'Body (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::textarea('desc_arabic', null, ['class' => 'form-control summernote arabic', 'id' => 'row-desc_eng summernote', 'style' => 'direction: rtl;text-align: right !important;']) !!}
			

	</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Create News</button>
		</div>
		</div>
	</div> --> 



@stop