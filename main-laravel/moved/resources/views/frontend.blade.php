<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yakolak</title>

    <!-- Bootstrap -->
     {!! HTML::style('css/bootstrap/bootstrap.min.css') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- Font Awesome -->
      {!! HTML::style('font-awesome/css/font-awesome.min.css') !!}
      <!-- Glyphicons -->
     <!--  {!! HTML::style('font/font-awesome.min.css') !!} -->


      <!-- Plugins -->
      {!! HTML::style('/plugins/css/fileinput.min.css') !!}
      {!! HTML::style('/plugins/css/toastr.min.css') !!}
      {!! HTML::style('/plugins/css/jquery.rateyo.min.css') !!}
       <!-- Editor -->
      <!-- Xerolabs -->
      {!! HTML::style('/css/xerolabs/style.css') !!}
      {!! HTML::style('/css/xerolabs/common.css') !!}
      {!! HTML::style('/css/xerolabs/frontend/custom.css') !!}
   
 
      {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}

</head>
<body>

@if (Auth::guest())
<div class="modal fade" id="modal-ads" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content borderZero">
      <div class="modal-header modal-warning">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Warning</h4>
        </div>
        <div class="modal-body">
        <h5>Your not logged in!</h5>
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default borderZero" data-dismiss="modal">Close</button>
          <a href="{{ url('auth/login') }}"><button type="submit" class="btn btn-warning borderZero" data-dismiss="modal">Login</button></a>

        </div>
      </div>
    </div>
  </div>
 @else
  <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderZero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn btn-default borderZero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

 @endif
<div id="mobilesidebarAccount" class="visible-sm visible-xs">
<div class="panel-group" id="mobileAccordion">
  <li class="whiteText sidebarTitle"><span>Welcome @if (!(Auth::guest())) {{Auth::user()->name}} @endif</span></li>
 
  <li><a class="whiteText normalText" href="{{ url('/') }}">Home</a></li>
   @if (Auth::guest()) 
  <li class="modalSidebar"><a class="whiteText normalText" href="#"  data-toggle="modal" data-target="#modal-login">Login</a></li>
  <li class="modalSidebar"><a class="whiteText normalText" href="#" class="modalSidebar" data-toggle="modal" data-target="#modal-register">Register</a></li>
  @else
  <li><a class="whiteText normalText normalText" href="{{url('dashboard')}}">Dashboard</a></li>
  <li><a class="whiteText normalText" href="{{url('user/vendor').'/'.Auth::user()->id.'/'.Auth::user()->name}}">Public Page</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/messages')}}">Messages</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/listings')}}">Listings</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/watchlist')}}">Watchlist</a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/settings')}}">Settings</a></li>
  <li><a class="whiteText normalText" href="{{url('logout')}}">Logout</a></li>
<!--   <li class="borderbottomLight"></li> -->
  @endif
  @if(Auth::check())
    @if(Auth::user()->usertype_id == 3)
   
    @else
     <li><a class="whiteText normalText" href="{{ url('ads/post') }}">Post Ads</a></li>
    @endif
  @endif
  <li><a class="whiteText normalText" href="{{url('category/list')}}">Buy</a></li>
  <li><a class="whiteText normalText" href="{{ url('ads/post') }}">Sell</a></li>
  <li><a class="whiteText normalText" href="{{url('category/list')}}">Bid</a></li>
  <li><a class="whiteText normalText" href="{{url('category/list')}}">Categories</a></li>
  <li><a class="whiteText normalText" href="{{url('support')}}">Support</a></li>
  <div class="panel panel-default noBackground">
    <li class="panel-heading noPadding">
        <a class="whiteText normalText" data-toggle="collapse" data-parent="#mobileAccordion" href="#accordionLanguage">
        Language<span class="pull-right"><i class="fa fa-angle-down"></i></span></a>
    </li>
    <div id="accordionLanguage" class="panel-collapse collapse">
      <div class="panel-body noPadding accordionBody normalText">
        <ul class="noPadding noMargin">
          <li class=""><a class="leftPaddingC whiteText">English</a></li>
          <li class=""><a class="leftPaddingC whiteText">Chinese</a></li>
          <li class=""><a class="leftPaddingC whiteText">Arabic</a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
</div>
<!-- header start -->
<nav class="navbar navbar-default navbar-fixed-top bgWhite borderBottom">
  <div class="container noPadding">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 leftPadding noPaddingXs">
        <div class="minPadding hidden-md hidden-lg noleftPadding toggleSize">
          <button class="btn noBGButton menuToggle borderZero sidebar-btn" type="button"> <i class="fa fa-bars"></i> </button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-9 col-xs-6 leftPaddingMinXs">
             <a class="navbar-brand logo" href="{{ url('/') }}">
               <img src="{{ url('img').'/'}}yakolaklogo.png" class="img-responsive responsiveImgXS">
             </a>
        </div>
      </div>
      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 pull-right">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" style="padding-top:3px" >
            {!! Form::open(array('url' => 'browse', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}         
                <div class="input-group minPadding noleftPadding norightPadding text-right" >
                  <input type="text" class="form-control borderZero inputBox inputSearch" required name="keyword" placeholder="What are you looking for?">
                  <span class="input-group-btn">
                    <button class="btn blueButton borderZero hidden-xs" type="submit"><i class="fa fa-search"></i> Search</button>
                    <button class="btn blueButton borderZero hidden-sm hidden-md hidden-lg" type="submit"><i class="fa fa-search"></i></button>
                   @if(!Auth::check())
                        <button class="btn mediumText redButton borderZero postAds hidden-xs hidden-sm" type="button" class="#" data-toggle="modal" data-target="#modal-login">Post Ads</button>
                   @else
                      @if(Auth::check())
                          @if(Auth::user()->usertype_id == 3)
                           
                          @else
                             <a href="{{ url('ads/post') }}" class="hidden-xs hidden-sm"><button class="btn mediumText redButton borderZero postAds" type="button">Post Ads</button></a>                          @endif
                          @endif
                   @endif
                  </span>
                </div>
             {!! Form::close() !!}   
        </div>
        <div class="collapse navbar-collapse col-lg-12 col-md-12 col-sm-12 col-xs-12 bottomMargin pull-right">
                <ul class="nav navbar-nav hidden-sm">
                  <li><a href="#">Buy</a></li>
                  <li><a href="{{ url('ads/post') }}">Sell</a></li>
                  <li><a href="#">Bid</a></li>
                  <li class=""><a href="#" class="button-plans">Plans <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <li class=""><a href="#" class="button-categories">Categories <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <!-- <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categories
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('category/list')}}">Buy and Sell</a></li>
                      <li><a href="#">Live Auctions</a></li>
                      <li><a href="#">Cars and Vehicles</a></li> 
                      <li><a href="#">Clothing</a></li>
                      <li><a href="#">Events</a></li>
                      <li><a href="#">Jobs</a></li>
                      <li><a href="#">Real Estate</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">All Categories</a></li>
                    </ul>
                  </li> -->
                  <li class=""><a href="#" class="button-support">Support <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <!-- <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Support
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('support')}}" onclick="selectContactUs()">Contact us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAboutUs()">About us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectCallUs()">Call us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectFaqs()">FAQ's</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAdvertising()">Advertising</a></li>
                    </ul>
                  </li> -->
                  @if(!Auth::check())
                  <li class="borderleftLight"><a href="#" class="btn-login" data-toggle="modal" data-target="#modal-login">Login</a></li>
                  <li class="borderleftLight"><a href="#" class="#" data-toggle="modal" data-target="#modal-register">Register</a></li>
                  @else
                  <li class="borderleftLight"></li>
                  <li class=""><a href="#" data-toggle="modal" class="button-user">
                    <img src="{{ url('uploads').'/'.Auth::user()->photo}}" class="avatar" alt="user profile image" style="width: 25px; height: 25px;">
                     {{Auth::user()->name}} <i class="fa fa-angle-down leftMargin"></i></a>
                  </li>
                  <li class="borderleftLight"><a class="blueText" href="{{ url('dashboard')}}#messages">{{$messages_counter}} <i class="fa fa-envelope"></i></a></li>
                  <li><a href="#" class="blueText">{{$notifications_counter}} <i class="fa fa-flag"></i></a></li>
                   @endif
                   <li class="borderleftLight"><a href="#" class="button-region">EN <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <!-- <li class="borderleftLight dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">EN
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">English</a></li>
                      <li><a href="#">Chinese</a></li>
                      <li><a href="#">Arabic</a></li>
                    </ul>
                  </li> -->
                </ul>
        </div>
      </div>

    </div>
  </div>
</nav>
  @if (!(Auth::guest()))
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding userPanel">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 11px;"><strong class="whiteText">Account Navigation:</strong></li>
        <li><a class="whiteText" href="{{url('dashboard')}}">Dashboard</a></li>
        <li><a class="whiteText" href="{{url('user/vendor').'/'.Auth::user()->id.'/'.Auth::user()->name}}">Public Page</a></li>
        <li><a class="whiteText" href="{{url('dashboard/messages')}}">Messages</a></li>
        <li><a class="whiteText" href="{{url('dashboard/listings')}}">Listings</a></li>
        <li><a class="whiteText" href="{{url('dashboard/bids')}}">Bids</a></li>
        <li><a class="whiteText" href="{{url('dashboard/watchlist')}}">Watchlist</a></li>
        <li><a class="whiteText" href="{{url('dashboard/settings')}}">Settings</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('logout') }}">Logout</a></li>
      </ul>
      </div>
    </div>
@endif
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs plan">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 11px;"><strong class="whiteText">Plans:</strong></li>
        <li><a class="whiteText" href="{{url('plans')}}">Bronze</a></li>
        <li><a class="whiteText" href="{{url('plans')}}">Silver</a></li>
        <li><a class="whiteText" href="{{url('plans')}}">Gold</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
<!--         <li><a href="{{url('plans')}}">Plans</a></li> -->
        <li class="pull-right"><a class="whiteText" href="{{url('plans')}}">All Plans</a></li>
      </ul>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding quickSupport">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 11px;"><strong class="whiteText">Quick Support:</strong></li>
        <li><a class="whiteText" href="{{url('support')}}">Contact us</a></li>
        <li><a class="whiteText" href="{{url('support')}}">About us</a></li>
        <li><a class="whiteText" href="{{url('support')}}">Call us</a></li>
        <li><a class="whiteText" href="{{url('support')}}">FAQ's</a></li>
        <li><a class="whiteText" href="{{url('support')}}">Advertising</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('') }}">Support Page</a></li>
      </ul>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding availableRegion">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 11px;"><strong class="whiteText">Available Regions:</strong></li>
        <li><a class="whiteText" href="{{url('')}}">English</a></li>
        <li><a class="whiteText" href="{{url('')}}">Chinese</a></li>
        <li><a class="whiteText" href="{{url('')}}">Arabic</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('') }}">View All</a></li>
      </ul>
      </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding categoriesNav">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 11px;"><strong class="whiteText">Categories:</strong></li>
        @foreach($categories as $row)
            <li><a class="whiteText" href="{{url('category/list').'/'.$row->id}}">{{$row->name}}</a></li>
        @endforeach
      
       <!--  <li><a class="whiteText" href="{{url('category/list')}}">Bidding</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Cars and Vehicles</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Clothing</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Events</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Jobs</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Real Estate</a></li>
        <li><a class="whiteText" href="{{url('category/list')}}">Services</a></li> -->
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('') }}">All Categories</a></li>
      </ul>
      </div>
    </div>

<!-- <div id="mobilesidebarCategory" class="visible-sm visible-xs">
  <h4 class="whiteText leftMarginB">Categories</h4>
  <div class="form-group" id="accordion">
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" class="whiteText" data-toggle="collapse" data-parent="#accordion" href="#buyandsell">Buy and Sell</a>
      <div id="buyandsell" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#bidding">Bidding</a>
      <div id="bidding" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#carsandvehicles">Cars and Vehicles</a>
      <div id="carsandvehicles" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#clothings">Clothings</a>
      <div id="clothings" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#events">Events</a>
      <div id="events" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#jobs">Jobs</a>
      <div id="jobs" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#realestate">Real Estate</a>
      <div id="realestate" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#services">Services</a>
      <div id="services" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#all">All Categories</a>
      <div id="all" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
  </div>
  </div> -->
<!-- header end -->
<div id="wrapper" class="">
 <div id="content">
     @yield('content')
 </div>

@if(Route::current()->getPath() == 'ads/post' || Route::current()->getPath() == 'profile' || Route::current()->getPath() == 'search' || Route::current()->getPath() == 'plans' || Route::current()->getPath() == 'support' || Route::current()->getPath() == 'dashboard' || Route::current()->getPath() == 'buy' || Route::current()->getPath() == 'bid' || Route::current()->getPath() == 'sell' || Route::current()->getPath() == 'support/contact-us' || Route::current()->getPath() == 'support/call-us' || Route::current()->getPath() == 'support/about-us' || Route::current()->getPath() == 'support/faqs' || Route::current()->getPath() == 'support/advertising' || Route::current()->getPath() == 'support/help-us' || Route::current()->getPath() == 'ads/view/{id}' ||  Route::current()->getPath() == 'dashboard/messages' || Route::current()->getPath() == 'dashboard/listing' || Route::current()->getPath() == 'dashboard/bids' || Route::current()->getPath() == 'dashboard/watchlist' || Route::current()->getPath() == 'dashboard/settings' || Route::current()->getPath() == 'user/vendor/{id}' || Route::current()->getPath() == 'ads/edit/{id}' || Route::current()->getPath() == 'category/list' || Route::current()->getPath() == 'ads/view/{id}' || Route::current()->getPath() == 'browse')

@else
<!-- main information start -->
<div class="container-fluid bgGray ">
  <div class="container noPadding">
    <div class="col-lg-12 col-md-12 noPadding">

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom" style="min-height: 312px;">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Yakolak Statistic</strong></span>
        </div>
        <div class="panel-body lineHeight">
          <p>Total Ads</p>
          <h4 class="blueText">48,670</h4>
          <p>Total Bids</p>
          <h4 class="blueText">10,001</h4>
          <p>Total Vendors</p>
          <h4 class="blueText">300</h4>
          <p>Total Companies</p>
          <h4 class="blueText">48,670</h4>
        </div>
      </div>  
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Top Members</strong></span>
        </div>
        <div class="panel-body lineHeight">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Vendor Name</div>
                <p class="normalText">10 Ads & 3 Bids Listings</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Vendor Name</div>
                <p class="normalText">10 Ads & 3 Bids Listings</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Vendor Name</div>
                <p class="normalText">10 Ads & 3 Bids Listings</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Vendor Name</div>
                <p class="normalText">10 Ads & 3 Bids Listings</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>

      <div class="clearfix visible-xs-block visible-sm-block"></div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Latest Comments</strong><span class="pull-right"><button type="button" class="btn btn-default borderZero bgGray removeBorder bottomMarginB anglePadding"><i class="fa redText fa-angle-left"></i></button><button type="button" class="bottomMarginB anglePadding btn borderZero btn-default leftMargin bgGray removeBorder"><i class="fa redText fa-angle-right"></i></button></span></span>
        </div>
        <div class="panel-body lineHeight">
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Listing Name</div>
                <p class="normalText">How much per order?</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Listing Name</div>
                <p class="normalText">How much per order?</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Listing Name</div>
                <p class="normalText">How much per order?</p>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding">Listing Name</div>
                <p class="normalText">How much per order?</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Latest Reviews</strong><span class="pull-right"><button type="button" class="btn btn-default borderZero bgGray removeBorder bottomMarginB anglePadding"><i class="fa redText fa-angle-left"></i></button><button type="button" class="bottomMarginB anglePadding btn borderZero btn-default leftMargin bgGray removeBorder"><i class="fa redText fa-angle-right"></i></button></span></span>
        </div>
        <div class="panel-body lineHeight">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding">Listing Name
              <span class="blueText pull-right rightMargin hidden-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </span>
            </div>
            <div class="blueText bottomPadding rightMargin visible-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </div>
          </div>
          <div class="col-xs-12 noPadding">
            <div class="normalText">Fast transaction will do it again.</div>
            <div class="smallText lightgrayText">Reviewed by: Username</div>
          </div>
        </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding">Listing Name
              <span class="blueText pull-right rightMargin hidden-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </span>
            </div>
            <div class="blueText bottomPadding rightMargin visible-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </div>
          </div>
          <div class="col-xs-12 noPadding">
            <div class="normalText">Fast transaction will do it again.</div>
            <div class="smallText lightgrayText">Reviewed by: Username</div>
          </div>
        </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding">Listing Name
              <span class="blueText pull-right rightMargin hidden-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </span>
            </div>
            <div class="blueText bottomPadding rightMargin visible-xs">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star-half-empty"></i>
              </div>
          </div>
          <div class="col-xs-12 noPadding">
            <div class="normalText">Fast transaction will do it again.</div>
            <div class="smallText lightgrayText">Reviewed by: Username</div>
          </div>
        </div>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>
<!-- main information end -->
@endif

              
<!-- main footer start -->
<div class="container-fluid bgWhite borderBottom ">
  <div class="container noPadding ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Customer Service</strong></span>
        </div>
        <div class="panel-body normalText">
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()" class="grayText">Help Center</a></p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()" class="grayText">Contact Us</a></p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()" class="grayText">Terms and Conditions</a></p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()" class="grayText">Frequently Asked Questions</a></p>
          <p>&nbsp;</p>
          <p>Facebook Page</p>
          <p>Twitter</p>
          <p>Google Plus</p>
        </div>
      </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>About Yakolak</strong></span>
        </div>
        <div class="panel-body normalText">
          <p>Company Information</p>
          <p>Mission and Vision</p>
          <p>&nbsp;</p>
          <p><a href="{{Route::current()->getPath() == 'support' ? '#': url('support')}}" onclick="selectAdvertising()" class="grayText">Advertising</a></p>
          <p>Sitemap</p>
        </div>
      </div>
      </div>

      <div class="clearfix visible-xs-block visible-sm-block"></div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Sell on Yakolak</strong></span>
        </div>
        <div class="panel-body normalText">
          <p><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#btn-register" class="grayText">Create an account</a></p>
          <p><a href="{{url('ads/post')}}" class="grayText">Publish Ads</a></p>
          <button class="btn redButton borderZero noMargin fullSize" type="button">LIST AD FOR FREE</button>
          <p>&nbsp;</p>
          <p>Account Upgrade</p>
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Subscribe to our newsletter</strong></span>
        </div>
        <div class="panel-body normalText">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Email Address">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="First Name">
          <input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Last Name">
          <button class="btn blueButton borderZero noMargin fullSize" type="button">SUBSCRIBE</button>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>

@include('layout.form')
<div class="container-fluid bgGray">
<div class="container noPadding">
<div class="col-lg-12 ">
  <div class="col-lg-12 minPadding noleftPadding lightgrayText">© 2016 Yakolak.com. All rights reserved.</div>
</div>
</div>
</div>

<!-- main footer end -->

<!-- search start -->

<!-- sea    
        
      </div>
    </div>
  </div>
</div>
<!-- account end -->


<!-- NAVIGATOR -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
   <!-- jQuery -->
{!! HTML::script('/js/jquery.js') !!}
{!! HTML::script('/js/form.js') !!}
{!! HTML::script('/plugins/js/jquery.rateyo.min.js') !!}
{!! HTML::script('/plugins/js/timeago.js') !!}
{!! HTML::script('/plugins/js/toastr.min.js') !!}
{!! HTML::script('/plugins/js/fileinput.min.js') !!}
{!! HTML::script('/plugins/js/jquery.countdown.js') !!}
{!! HTML::script('/plugins/js/jquery.countdown.min.js') !!}
{!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
{!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
{!! HTML::script('/plugins/readmore.js') !!}

    <!-- Bootstrap -->
{!! HTML::script('/js/bootstrap.min.js') !!}
    <!-- Editor -->
{!! HTML::script('/plugins/js/summernote.js') !!}
 @yield('scripts')
<script type="text/javascript">

 $_token = '{{ csrf_token() }}';

    //   var CaptchaCallback = function(){
    //     grecaptcha.render('RecaptchaField1', {'sitekey' : '6LeMqBYTAAAAAEOhsIBOdqDtkxT67Gu192cUZdp4'});
    //     grecaptcha.render('RecaptchaField2', {'sitekey' : '6LeMqBYTAAAAAEOhsIBOdqDtkxT67Gu192cUZdp4'});
    // };

$(document).ready(function() {

$(".sidebar-btn").click(function(){
    $("#mobilesidebarAccount").toggleClass("active");
    $("#wrapper").toggleClass("active");
    $(".navbar-fixed-top").toggleClass("active");
});
$("#button-reply").on('click', function(event) {
    var html = $(this).parent().parent().find('#reply-pane:first').html();
    $(".comments-container").find('#reply-pane:last').find("#ad-replies-containter").html(html);
    //console.log(container);
   //$("html, body").animate({ scrollTop: $("#notice-pane").offset().top}, 1500);
});
// $("#button-reply").on('click', function(e) {
//    // prevent default anchor click behavior
//    e.preventDefault();
//   console.log(hash);
//    // store hash
//    var hash = this.hash;

//    // animate
//    $('html, body').animate({
//        scrollTop: $(hash).offset().top - 100
//      }, 1000, function(){

//        // when done, add hash to url
//        // (default click behaviour)
//        window.location.hash = hash;
//      });

// });     
$(".modalSidebar").click(function(){
    $("#mobilesidebarAccount").toggleClass("active");
    $("#wrapper").toggleClass("active");
    $(".navbar-fixed-top").toggleClass("active");
});
$('#bidsCarousel').carousel({
  pause: true,
  interval: false
  });
  $('#adsCarousel').carousel({
  pause: true,
  interval: false
  });
// $(".active").click(function(){
//     $("#wrapper").removeClass("active");
//     $("#mobilesidebarAccount").removeClass("active");
//     $(".navbar-fixed-top").removeClass("active");
// });
$('.carousel').carousel();
// $(".button-categories").click(function(){
//   $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
//   $(".button-support").find('i').toggleClass('fa-angle-down fa-angle-up')
// });
// $(".button-support").click(function(){
//   $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
// });
// $(".button-region").click(function(){
//   $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
// });
// $(".searchToggle").click(function(){
  //$(this).find('i').fadeToggle()('fa-search ', 1000).fadeToggle()('fa-close', 1000);

//   $(this).find('i').toggleClass('fa-search fa-close')
//   $(this).find('i').fadeOut(150);
//   $(this).find('i').fadeIn(150);
//     $("#mobileSearchpad").toggleClass("active");
// });
  $('[data-toggle=offcanvas]').click(function() {
    $('#sidebar-wrapper').toggleClass('active');
  });
   $(function () {
        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });


       
       
      });
$(".button-plans").click(function(){
    $(".plan").toggleClass("active");
    $(".userPanel").removeClass("active");
    $(".categoriesNav").removeClass("active");
    $(".quickSupport").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-user").click(function(){
    $(".userPanel").toggleClass("active");
    $(".categoriesNav").removeClass("active");
    $(".plan").removeClass("active");
    $(".quickSupport").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-categories").click(function(){
    $(".categoriesNav").toggleClass("active");
    $(".plan").removeClass("active")
    $(".quickSupport").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-support").click(function(){
    $(".quickSupport").toggleClass("active");
    $(".plan").removeClass("active")
    $(".categoriesNav").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-region").click(function(){
    $(".availableRegion").toggleClass("active");
    $(".plan").removeClass("active")
    $(".quickSupport").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".categoriesNav").removeClass("active");   
});


$("#client-usertype").change(function(){
    $(this).find("option:selected").each(function(){
        if($(this).attr("value")=="2"){
            $("#addBranch").show();
            $("#startDate").show();
            $("#officeHours").show();
            $("#gender").hide();
            $("#birthDate").hide();
            $(".common-name-pane").text("Company Name");
            $("#client-country-pane").hide();
            $("#client-city-pane").hide();
            $("#client-address-pane").hide();
            // $(".common-name-pane").html('<h5 class="normalText col-sm-3" for="email">Company Name</h5><div class="col-sm-9"><input type="text" class="form-control borderZero inputBox" name="name" id="client-vendor_name" placeholder=""></div>');
        }
        else{
            $("#addBranch").hide();
            $("#startDate").hide();
            $("#officeHours").hide();
            $(".common-name-pane").text("Vendor Name");
            // $(".common-name-pane").html('<h5 class="normalText col-sm-3" for="email">Vendor Name</h5><div class="col-sm-9">
            //     <input type="text" class="form-control borderZero inputBox" name="vendor_name" id="client-vendor_name" placeholder="">
            //   </div>');
            $("#gender").show();
            $("#birthDate").show();
            $("#client-country-pane").show();
            $("#client-city-pane").show();
            $("#client-address-pane").show();
        }
    });
}).change();
$(".btn-nav").click(function(){
    $("#navULxs").toggleClass("active");
    $("#loginULxs").removeClass("active");
});
$(".btn-prof").click(function(){
    $("#loginULxs").toggleClass("active");
    $("#navULxs").removeClass("active");
});
$("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $(".button-pane").addClass('hide');

  });
$("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide");
        $(".photo-pane").removeClass("hide");
        $(".button-pane").removeClass('hide');

});
@if(Session::has('notice'))
  $("#modal-login").modal("show");
@endif

});


$("#modal-login").on("hidden.bs.modal", function (event) {
  $("#modal-login .alert").remove();
});

$("#modal-ads-need-login").on("hidden.bs.modal", function (event) {
 $("#modal-login .alert").remove();
});

$("#modal-register").on("hidden.bs.modal", function (event) {
  $("#modal-register .alert").remove();
  $("#modal-register").find("form")[0].reset();
});

$("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });

 $(".modal").on("change", ".company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().find("#client-company_city");
        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
var branch = $("#branchContainer").html();

 $(".btn-add-branch").click(function() {
        $("#branchContainer").append(branch);
        var country = $("#branchContainer").find('section:first').find("#client-company_country").val();
        var city = $("#branchContainer").find('section:first').find("#client-company_city").html();

        $("#branchContainer").find('section:last').find("#client-company_country").val(country);
        $("#branchContainer").find('section:last').find("#client-company_city").html(city);
        $("#branchContainer").find('section:last').find(".btn-add-branch").removeClass("btn-add-branch").addClass("btn-del-branch").html("<i class='fa fa-minus redText'></i>");
        //$("#branchContainer").find('section:last').find(".btn-add-branch").html("<button type='button' value='' class='hide-view btn-add-branch pull-right borderZero inputBox'></button>");
 });
 $(".modal").on("click", ".btn-del-branch", function() { 
    $(this).parent().parent().remove();
 });
  $('#client-gender-male').change(function() {
      if($(this).is(":checked")) {
        $("#client-gender").val("m");
      }
  });
  $('#client-gender-female').change(function() {
      if($(this).is(":checked")) {
         $("#client-gender").val("f");
      }
  });
  @if(session('status'))
      status("Error", "Account Validated", 'alert-success');
  @endif
  @if(session('error'))
      status("Error", "Account is already validated", 'alert-danger');
  @endif


     $("#getting-started").countdown("2017/01/01", function(event) {
     $(this).text(
       event.strftime('D: %D - H: %H - M:%M - S:%S')
     );
  });

          //readmore
  $('#info').readmore({
      moreLink: '<a href="#">Usage, examples, and options</a>',
      collapsedHeight: 600,
      afterToggle: function(trigger, element, expanded) {
        if(! expanded) { // The "Close" link was clicked
          $('html, body').animate({scrollTop: element.offset().top}, {duration: 600});
        }
      }
    });
    $('article').readmore({speed: 500});
</script>
</div>
</body>
</html>