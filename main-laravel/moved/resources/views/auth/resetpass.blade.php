<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Latest compiled and minified CSS -->
    <title>{{ $title }}</title>
    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! HTML::style('/css/login-style.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="bg-login">
    <div class="container v-centered">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-8 centered">
          <div class="well">
            {!! Form::open(array('url' => 'reset-password', 'role' => 'form', 'class' => 'form-horizontal')) !!}
              @if(Session::has('notice'))
              <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
              </div>
              @endif
              <fieldset>
                <legend>Reset Password</legend>
                <div class="form-group">
                  <label for="email" class="col-lg-3 control-label">Email</label>
                  <div class="col-lg-9">
                    <input type="text" class="form-control" name="email" id="email" placeholder="Email">
                    <input type = "hidden" name="token" id="token" value = "{{$token}}">
                  </div>
                </div>
                <div class="form-group">
                  <label for="password" class="col-lg-3 control-label">Password</label>
                  <div class="col-lg-9">
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                  </div>
                </div>
                <div class="form-group">
                  <label for="confirm-password" class="col-lg-3 control-label">Password Confirmation</label>
                  <div class="col-lg-9">
                    <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Confirm Password">
                  </div>
                </div>
                <div class="form-group">
                </div>
                <div class="form-group">
                  <div class="col-lg-9 col-lg-offset-3">
                    <input class="hide" name="honeypot" value="">
                    <button type="submit" class="btn btn-primary btn-responsive"><i class="fa fa-fw fa-floppy-o"></i>Change Password</button>
                   </div>
                </div>
              </fieldset>
            {!! Form::close() !!}
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}

  </body>
</html>