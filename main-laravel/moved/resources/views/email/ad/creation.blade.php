@extends('layout.email')
@section('scripts')
@stop
@section('content')
<link rel="stylesheet" href="sample.css" type="http://yakolak.xerolabs.co/plugins/css/summernote.css" />
<p><?php
 $search =  array('{CONTACT_NAME}','{USER_NAME}','{USER_EMAIL}','{USER_PHONE}','{ITEM_TITLE}','{ITEM_URL','{ITEM_LINK}','{COMMENT}');
 $replace = array($contact_name, $user_name, $user_email, $user_phone, $item_title, $item_url, $item_link, $comment);                        
 $body = str_replace($search, $replace, $newsletter->body);
echo ($body);
 ?></p>
 <p style="margin-top: 20px;">Thank you!</p>

 <script src="http://yakolak.xerolabs.co/plugins/summernote.js"></script>
@stop
