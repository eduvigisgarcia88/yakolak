@extends('layout.email')

@section('content')

<p><?php
 $search =  array('{CONTACT_NAME}','{USER_NAME}','{USER_EMAIL}','{USER_PHONE}','{VERIFICATION_LINK}');
 $replace = array($contact_name, $user_name, $user_email, $user_phone, $verification_link);                        
 $body = str_replace($search, $replace, $newsletter->body);
echo ($body);
 ?></p>
 <p style="margin-top: 20px;">Thank you!</p>

@stop
