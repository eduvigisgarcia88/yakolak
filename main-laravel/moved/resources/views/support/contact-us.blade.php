@extends('layout.frontend')
@section('scripts')

@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 noPadding">
			<div class="col-sm-3 noPadding">
				<div class="panel-body notopPadding">
				<ul id="supportTab" class="vertical ex">
				  <li role="presentation" class="active"><a class="active" href="#contactUs" aria-controls="contactUs" role="tab" data-toggle="tab">Contact us</a></li>
			    <li role="presentation"><a href="#aboutUs" aria-controls="aboutUs" role="tab" data-toggle="tab">About us</a></li>
			    <li role="presentation"><a href="#callUs" aria-controls="callUs" role="tab" data-toggle="tab">Call us</a></li>
			    <li role="presentation"><a href="#faq" aria-controls="faq" role="tab" data-toggle="tab">FAQ's</a></li>
			    <li role="presentation"><a href="#advertising" aria-controls="advertising" role="tab" data-toggle="tab">Advertising</a></li>
				</ul>
			</div>
			</div>
			<div class="col-sm-9 noPadding">
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">Support</span>
					</div>
					<div class="panel-body">
					<div class="tab-content normalText">
			    <div role="tabpanel" class="tab-pane" id="contactUs">Contact us</div>
			    <div role="tabpanel" class="tab-pane" id="aboutUs">About us</div>
			    <div role="tabpanel" class="tab-pane" id="callUs">Call us</div>
			    <div role="tabpanel" class="tab-pane" id="faq">FAQ's</div>
			    <div role="tabpanel" class="tab-pane" id="advertising">Advertising</div>
			 	</div>
        </div>
				</div>
				 </div>
      </div>
    </div>
  </div>
@stop
