@extends('layout.frontend')
@section('scripts')
<script>
  $("#nav div a[href^='#']").on('click', function(e) {
   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top - 100
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 

       $("#content-wrapper").on("click", ".btn-plan-subscription-upgrade", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        var plan_id = $(this).data('plan');
        $("#row-id").val("");
      $("#row-id").val(id);
      $("#row-title").val(title);
      $("#row-plan_id").val(plan_id);
      $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
          $("#modal-form").find('form').trigger("reset");
          $(".modal-title").html('<strong> Upgrade to '+ title +' </strong>', title);
          $("#modal-plan-subscription").modal('show');    
    });


  function selectContactUs(){
    $(".btn-contact-us").click();
  }
  function selectCallUs(){
    $(".btn-call-us").click();
  }
  function selectAboutUs(){
    $(".btn-about-us").click();
  }
  function selectFaqs(){
    $(".btn-faqs").click();
  }
  function selectAdvertising(){
    $(".btn-advertising").click();
  }
</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron nobottomMargin">
		<div class="col-lg-12 col-md-12 noPadding">
			<div class="col-sm-3">
				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SUPPORT</span>
					</div>
					<div id="supportTab" class="panel-body normalText">
						<ul class="nav nav-pills nav-stacked noPadding" role="tablist">
							<li role="presentation"><a href="#contactUs" aria-controls="contactUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i>  Contact us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#helpCenter" aria-controls="helpCenter" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question rightPadding"></i>  Help Center <i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#aboutUs" aria-controls="aboutUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-info rightPadding"></i>  About us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#callUs" aria-controls="callUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-phone rightPadding"> </i> Call us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#termsandCondition" aria-controls="termsandCondition" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-file-text-o rightPadding"></i> Terms and Conditions<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#faqs" aria-controls="faqs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question-circle rightPadding"></i> FAQ's<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#advertising" aria-controls="advertising" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-volume-up rightPadding"></i> Advertising<i class="fa fa-angle-right pull-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-9 noPadding sideBlock">
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="contactUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Contact Us</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam posuere metus tellus. Mauris et mi at quam viverra mollis quis in arcu. Maecenas sit amet elit quis massa ornare hendrerit nec id mauris. Aliquam erat volutpat. Etiam metus risus, laoreet eget lorem non, scelerisque dapibus nisi. Integer id consectetur sem, a rutrum sem. Mauris magna odio, bibendum vitae libero nec, bibendum euismod diam. Mauris pellentesque eleifend urna et pellentesque. Duis quis tellus in sapien sollicitudin cursus id eget nisl. Fusce eu rhoncus arcu. Curabitur urna erat, luctus non cursus in, congue et nibh. Pellentesque condimentum nunc in nisi efficitur scelerisque eget sed quam. Morbi nec orci sed nisi scelerisque dictum in eu nibh. Quisque tincidunt, lectus quis mollis varius, nisi est pellentesque velit, vitae vestibulum lorem urna ut mi. Mauris nunc eros, suscipit ut vulputate bibendum, gravida at magna. Vestibulum ornare metus et fermentum condimentum.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="helpCenter">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Help Center</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									Nullam augue felis, accumsan nec dolor nec, facilisis viverra urna. Fusce vitae est mollis, hendrerit ligula nec, aliquam velit. Aenean sem augue, venenatis a mattis eget, auctor quis enim. Nullam efficitur, est nec rhoncus egestas, sapien ex mattis velit, quis auctor dolor purus id dolor. Sed eget bibendum turpis. Phasellus ut metus arcu. Aliquam blandit eleifend magna nec tempor. In hac habitasse platea dictumst. Cras quam purus, ultrices lacinia lorem eget, pharetra vehicula ligula. Maecenas tincidunt tempor tellus, at viverra dolor viverra ut. Aenean sit amet egestas neque.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="aboutUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">About us</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									Morbi et est vel felis condimentum placerat ut sed nisi. Integer sed erat scelerisque, vestibulum orci quis, rhoncus augue. Nunc a tincidunt velit. Mauris efficitur elementum enim auctor gravida. In id ante risus. In in accumsan massa. Curabitur et ultricies metus, venenatis semper arcu. Duis sagittis, ex placerat tristique rhoncus, justo ex consequat quam, eget aliquam est quam ut ipsum. Vestibulum a egestas ipsum. Proin sit amet augue cursus arcu lacinia scelerisque in eget felis. Sed tempor luctus sapien.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="callUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Call us</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									Nam porta imperdiet mattis. Morbi eget condimentum erat. Fusce vel lectus vel nisl molestie lacinia. Proin iaculis posuere quam, sit amet bibendum ipsum blandit in. Etiam non leo nec sapien pulvinar molestie. Sed non aliquam ligula. Phasellus vel viverra tellus. Aliquam at aliquet urna. Nunc blandit eu eros ut laoreet. Proin ac sapien sed mauris luctus ultricies. Vestibulum pellentesque luctus arcu, ac blandit sem gravida eget. Vivamus eu nunc fringilla, eleifend purus ac, vulputate nisl. Ut sed lobortis libero. Quisque id enim suscipit, egestas nibh nec, pellentesque mi. Donec posuere nisi a tellus condimentum vestibulum.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="termsandCondition">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Terms and Conditions</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									Morbi malesuada ex tempor libero lobortis scelerisque. Curabitur sed efficitur eros. Mauris id consequat metus. Mauris et facilisis neque, sit amet sodales turpis. Maecenas euismod erat nisl, a ullamcorper leo tempor at. Pellentesque a leo sit amet massa dignissim hendrerit vitae eu ligula. Ut ultricies nisl at felis scelerisque, eu placerat erat tempor. Maecenas tincidunt elit at odio consectetur, ac varius tortor tristique.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="faqs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">FAQ's</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									Nunc eleifend tellus vel laoreet hendrerit. Integer porta vel mauris ut dictum. Morbi egestas velit sit amet risus finibus, quis porttitor arcu luctus. Nulla facilisi. Curabitur facilisis nibh enim, vel congue sem scelerisque quis. Sed tempus a lorem eget molestie. Ut non tortor vitae nulla tincidunt euismod. Maecenas non massa non dui sollicitudin pharetra gravida efficitur massa. Integer dignissim euismod ipsum quis efficitur. Nulla pretium ipsum a sodales imperdiet. Sed quis enim urna. Mauris tempor turpis et ligula semper malesuada. Nam nisi mauris, ultrices quis vehicula non, congue non enim. Donec facilisis egestas ligula, vitae accumsan justo interdum a. Morbi ultricies eget erat sed facilisis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="advertising">
						<div class="panel panel-default removeBorder borderZero borderBottom " id="content-wrapper">
							      <div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">

							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Advertising</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
								<?php echo $bannerRates; ?>
								</div>
							</div>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
