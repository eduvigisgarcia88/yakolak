@extends('layout.master')
@section('scripts')

<script type="text/javascript">
   $_token = '{{ csrf_token() }}';
function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '">' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small>' + row.description + '</small></td>' +
              '<td><small>' + row.sequence + '</small></td>' +
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';

           $.each(row, function(sindex, srow) {
               if(sindex == "subcategoryinfo"){
                    $.each(srow, function(xindex, xrow) {
                          body += '<tr data-subcat-id="' + xrow.id + '">' + 
                            '<td class="hide">' + xrow.status + '</td>' +
                            //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                            '<td style="text-indent:10px;"><small>' + xrow.name + '</small></td>' +
                            '<td> '+ xrow.description +'</td>' +
                            '<td>' + xrow.sequence + '</td>' +
                            '<td class="rightalign">'+
                               // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                               '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button>' +
                               '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button>' +
                               // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
                            '</td></tr>';
                      });
               }


            });
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);

        // $(".tablesorter").trigger('update');

        // // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

        loading.addClass("hide");

      }, 'json');
    } 

    function search() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";
      console.log(response);

        $.each(response.rows.data, function(index, row) {

           body += '<tr data-id="' + row.id + '"' + ((row.status == 1) ? ((row.id == {{ Auth::user()->id }}) ? 'class=info' : '') : 'class="tr-disabled"') + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td>' + row.name + '</td>' +
              '<td>' + row.description + '</td>' +
              //'<td class="hidden-xs">' + row.type_name + '</td>' +
              '<td class="rightalign">';
                  
              body += '</td>'+
            '</tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

      }, 'json');
    
    }

    $(".content-wrapper").on("click", ".btn-add", function() {
           $("#modal-form").find('form').trigger("reset");
           $(".category-pane").removeClass("hide");
           $(".sequence_pane").addClass("hide");
           $("#button-save").removeClass("hide");
           $("#category_type_one").removeAttr('checked','checked');
           $("#category_type_two").removeAttr('checked','checked');
           $("#row-name").removeAttr('disabled','disabled');
           $("#row-sequence").removeAttr('disabled','disabled');
           $("#category_type_one").removeAttr('disabled','disabled');
           $("#category_type_two").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#row-custom_attributes").removeAttr('disabled','disabled');           
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Category</strong>');
           $("#row-id").val("");
           $("#modal-form").find('form')[0].reset();
           $("#modal-form").find("#form-notice").find('.alert').remove();
           $("#modal-form").modal('show');
        
    });
    $(".content-wrapper").on("click", ".btn-add-subcat", function() { 
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Sub Category</strong>');
           $("#modal-sub-category").modal('show');
        
    });
     $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                                           
                          if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          }
                      });
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                        
                          if(field.length > 0) {
                              field.val(value);
                            
                          }

                           if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          }
                      });
                      $(".category-pane").addClass("hide");
                      $("#row-name").attr('disabled','disabled');
                      $("#row-sequence").attr('disabled','disabled');
                      $("#category_type_one").attr('disabled','disabled');
                      $("#category_type_two").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-custom_attributes").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-view-sub-cat", function() {
        var id = $(this).parent().parent().data('subcat-id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $('#row-cat_id').removeAttr("selected", "selected");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/subcategory/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                           if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          } 
                      });
                      $(".category-pane").addClass("hide");
                      $("#row-name").attr('disabled','disabled');
                      $("#row-sequence").attr('disabled','disabled');
                      $("#category_type_one").attr('disabled','disabled');
                      $("#category_type_two").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-custom_attributes").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
        $(".content-wrapper").on("click", ".btn-edit-sub-cat", function() {
        var id = $(this).parent().parent().data('subcat-id');
        var btn = $(this);
        console.log(id);
        // reset all form fields
        $("#modal-form").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/subcategory/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data

                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          // console.log(value);
                          if(field.length > 0) {
                              field.val(value);
                            
                          }

                          if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          }
                      });
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

      $("#row-cat_id").on("change", function() { 
          var id = $("#row-cat_id").val();
          var order= $("#row-sequence").val();

          $(".sequence_pane").addClass("hide");
          $_token = "{{ csrf_token() }}";
          if(id == "parent"){
            $(".sequence_pane").addClass("hide");
          }else if(id != "parent"){
            $(".sequence_pane").removeClass("hide");
  
          }
          
        });

      $("#row-sequence").on("keyup", function() {
         var id = $("#row-cat_id").val();
          var order= $("#row-sequence").val();
          if(id == "parent"){


            $.post("{{ url('admin/category/get-cat-sequence') }}", { order:order,_token: $_token }, function(response) {
              $.each(response.rows, function(index, row) {
             
                    if(row.sequence == order){        
                      status('','<ul><li> Selected display number already in used</li></ul>','alert-danger', '#form-notice');
                    }         
              });
            }, 'json');

          }else{

            $.post("{{ url('admin/category/get-subcat-sequence') }}", { id: id, order:order,_token: $_token }, function(response) {
              $.each(response.rows, function(index, row) {
             
                    if(row.sequence == order){        
                      status('','<ul><li> Selected sequence number already in used</li></ul>','alert-danger', '#form-notice');
                    }         
              });
            }, 'json');
          }
          

       });

    $(document).ready(function() {
    $('#category_type_one').click(function() {
        if($("#category_type_one").is(":checked")){
             $("#row-biddable_status").val("2");

        }else{
             $("#row-biddable_status").val("1");
        } 
        
       });
        $('#category_type_two').click(function() {
          if($("#category_type_two").is(":checked")){
             $("#row-buy_and_sell_status").val("2");
           }else{
            $("#row-buy_and_sell_status").val("1");
           } 
        }); 
    }); 
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-hover">
            <thead>
                <tr>
                  <th class="th-color-cat"><small>Category Name</small></th>
                  <th class="th-color-cat"><small>Description</small></th>
                  <th class="th-color-cat"><small>Order No</small>
                  <th class="text-right th-color-cat"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td><small>{{strToUpper($row->name)}}</small></td>
                <td><small>{{strToUpper($row->description)}}</small></td>
                <td>{{$row->sequence}}</td>
                 <td>
                    <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button> -->
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                    <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
                </td>
              </tr>
                @if(count($row->subcategoryinfo) > 0 )
                    @foreach($row->subcategoryinfo as $row_subcat)
                    <tr data-subcat-id = "{{$row_subcat->id}}">
                      <td style="text-indent:10px;"><small><i class="fa fa-caret"></i>{{$row_subcat->name}}</small></td>
                      <td>{{$row_subcat->description}}</td>
                      <td><small>{{$row_subcat->sequence}}</small></td>
                      <td>
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button> -->
                        <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button>
                        <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button>
                        <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
                      </td>
                    </tr>
                    @endforeach
                @endif
            @endforeach
          </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.category-management.form')