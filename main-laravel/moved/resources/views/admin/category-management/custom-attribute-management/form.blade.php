<!-- modal add user -->
  <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/category/custom-attribute/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i></h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group category-pane">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Category</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-attri_id" name="attri_id">
                        @foreach($category as $row)
                          <option value="{{$row->id}}">{{$row->name}}</option>
                          @if(count($row->subcategoryinfo) > 0)
                            @foreach($row->subcategoryinfo as $row_subcat)
                                <option value="{{$row_subcat->id}}">&nbsp;&nbsp;{{$row_subcat->name}}</option>
                            @endforeach
                          @endif
                        @endforeach
                        </optgroup>
                  </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Attribute Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="name" class="form-control" id="row-name" maxlength="30" placeholder="Enter your name...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-attri_type" class="col-sm-3 col-xs-5 control-label font-color">Attribute Type</label>
                <div class="col-sm-9 col-xs-7">
                     <select class="form-control" id="row-attri_type" name="attri_type">
                          <option class="hide">Select</option>
                          <option value="1">CHECKBOX</option>
                          <option value="2">TEXTBOX</option>  
                          <option value="3">DROPDOWN</option> 
                     </select>
                </div>
                </div>
                <div class="form-group attribute_dropdown hide">
                  <div class="col-lg-12">
                    <div class="row">
                    <label for="row-code" class="col-sm-3 control-label"><button type="button" value="" class="hide-view btn-add-attribute-value "> <i class="fa fa-plus Add"> </i></button> Attribute Values</label>
                      <div class="table-responsive">
                      <table id="attribute-value-table" class="table">
                          <tbody id="attribute-value-table body">
                              <tr>
                                  <td class="col-xs-12">
                                      <div class="col-xs-12 input-group">
                                          <input type="text" name="attribute_value_dropdown[]" class="form-control borderzero  requiredcol-lg-12" id="row-contact" maxlength="30">
                                      </div>
                                  </td>
                                  <td class="hide-sta text-right">
                                      <input type="hidden" name="attribute_value_id[]">
                                      <button type="button" value="" class="hide-view btn-del-attribute-value" title="Remove"> <i class="fa fa-close"> </i></button>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                      </div>
                  </div>
                </div>
                </div>
                <div class="form-group attribute_textbox hide">
                    <label for="row-attribute_value_textbox" class="col-sm-3 col-xs-5 control-label font-color">Attribute Value</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="attribute_value_textbox" class="form-control" id="row-attribute_value_textbox" maxlength="30" placeholder="">
                </div>
                </div>
                <!-- <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Display Order</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="sequence" class="form-control" id="row-sequence" maxlength="30" placeholder="Enter your sequence...">
                </div>
                </div>
                 <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-6 control-label font-color">Category Option</label>
                <div class="col-sm-5 col-xs-3">
                    <label class="checkbox-inline"><input type="checkbox" id="category_type_one">Buy and Sell</label>
                </div>
                <div class="col-sm-4 col-xs-3">
                    <label class="checkbox-inline"><input type="checkbox" id="category_type_two">Biddable</label>
                </div>
                   
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="description" id ="row-description" placeholder="Enter your category description"></textarea>
                </div>
                </div>  -->

               <!--  <!-- <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>-->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
