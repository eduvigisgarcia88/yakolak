@extends('layout.master')
@section('scripts')

<script>
   $_token = '{{ csrf_token() }}';
	function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            // console.log(row);
            body += '<tr data-id="' + row.id + '>' + 
              '<td class="hide">' + row.status + '</td>' +
              //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
              '<td><small><i class="fa fa-caret"></i>' + row.name + '</small></td></tr>';
              // '<td class="rightalign">'+
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              // '</td>

           $.each(row, function(sindex, srow) {
               if(sindex == "get_custom_attributes"){
                    $.each(srow, function(xindex, xrow) {
                          body += '<tr data-sub-attri-id="' + xrow.id + '"'+ '>' + 
                            '<td class="hide">' + xrow.status + '</td>' +
                            //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                            '<td style="text-indent:10px;color:red;"><small><i class="fa fa-caret"></i>' + xrow.name + '</small></td></tr>';
                            // '<td class="rightalign">'+
                            //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                            //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button>' +
                            //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button>' +
                            //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
                            // '</td>
                      });
               }
            });
           
           $.each(row, function(xindex, xvalue) {
                if(xindex == "subcategoryinfo"){
                    $.each(xvalue, function(yindex, yvalue) {

                       body += '<tr data-id="' + yvalue.id + '>' + 
                          '<td class="hide">' + yvalue.status + '</td>' +
                          //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                          '<td style="text-indent:10px;"><small><i class="fa fa-caret"></i>' + yvalue.name + '</small></td></tr>';
                          // '<td class="rightalign">'+
                          //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                          //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                          //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                          //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
                          // '</td>
                          $.each(yvalue, function(zindex, zvalue) {
                            if(zindex == "get_sub_cat_custom_attributes"){
                                   $.each(zvalue, function(tindex, tvalue) {
                                     body += '<tr data-sub-attri-id="' + tvalue.id + '"'+ '>' + 
                                      '<td class="hide">' + tvalue.status + '</td>' +
                                      //'<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-circle"/></td>' +
                                      '<td style="text-indent:20px;color:red;"><small><i class="fa fa-caret"></i>' + tvalue.name + '</small></td></tr>';
                                      // '<td class="rightalign">'+
                                      //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                                      //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button>' +
                                      //    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button>' +
                                      //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
                                      // '</td>
                                   });
                              }
                            });
                       });
                  }
            });
           
           
        
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);

        // $(".tablesorter").trigger('update');

        // // populate cache
        // $('#my-table').find('tbody tr').each(function() {
        //   $('#my-table').dataTable().fnAddData(this);
        // });

        loading.addClass("hide");

      }, 'json');
    } 
     $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                            
                          }  
                      });
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
 		   $(".content-wrapper").on("click", ".btn-add", function() {
          
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Custom Attribute</strong>');
           $("#modal-form").modal('show');
        });
     $(".btn-add-attribute-value").click(function() {
        var table = $("#attribute-value-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();

        newrow.appendTo(table);

        table.find("select[name='attribute_value[]']:last").focus();
        var last_tr = table.find("tr:last");
        last_tr.find('input').val("");
    });

    $(document).on('click', '.btn-del-attribute-value', function() {
        var table = $("#attribute-value-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            row.find(":input").val("");
        }
    });
    $("#row-attri_type").on("change", function() {
      if($("#row-attri_type").val() == "1"){
         $(".attribute_dropdown").removeClass("hide");
         $(".attribute_textbox").addClass("hide");
      }else if($("#row-attri_type").val() == "2"){
         $(".attribute_textbox").removeClass("hide");
         $(".attribute_dropdown").addClass("hide");

      }else if($("#row-attri_type").val() == "3"){
         $(".attribute_dropdown").removeClass("hide");
         $(".attribute_textbox").addClass("hide");

      }
   });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-hover">
            <thead>
                <tr>
                  <th class="th-color-cat"><small>Attribute Name</small></th>
                  <!-- <th class="th-color-cat text-right"><small>Tools</small></th> -->
                </tr>
              </thead>
            <tbody id="rows">
            	 @foreach($rows as $row)
              <tr data-id="{{$row->id}}">
                <td><small>{{ $row->name }}</small></td>
                 <!-- <td> -->
                  <!--   <button type="button" class="btn btn-default btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button> -->
                    <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button> -->
                    <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
               <!--  </td> -->
              </tr>
              @if(count($row->getCustomAttributes) > 0)
                    @foreach($row->getCustomAttributes as $row_sub_attri)
                    <tr data-sub-attri-id = "{{$row_sub_attri->id}}">
                      <td style="text-indent:10px;color:red;"><small><i class="fa fa-caret"></i>{{$row_sub_attri->name}}</small></td>
                      <!-- <td> -->
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                        <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button> -->
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button> -->
                        <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
                      <!-- </td> -->
                    </tr>
                    @endforeach
                @endif
                @if(count($row->subcategoryinfo) > 0)
                  @foreach($row->subcategoryinfo as $subcat)
                    <tr data-sub-attri-id = "{{$subcat->id}}">
                      <td style="text-indent:10px;"><small><i class="fa fa-caret"></i>{{$subcat->name}}</small></td>
        <!--               <td> -->
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                        <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button> -->
                       <!--  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button> -->
                        <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
             <!--          </td> -->
                    </tr>
                    @if(count($subcat->getSubCatCustomAttributes) > 0)
                    @foreach($subcat->getSubCatCustomAttributes as $subcat_attri)
                    <tr data-sub-attri-id = "{{$subcat_attri->id}}">
                      <td style="text-indent:20px;color:red;"><small><i class="fa fa-caret"></i>{{$subcat_attri->name}}</small></td>
                <!--       <td> -->
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                        <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view-sub-cat"><i class="fa fa-eye"></i></button> -->
                        <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit-sub-cat"><i class="fa fa-pencil"></i></button> -->
                        <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button> -->
                 <!--      </td> -->
                    </tr>
                    @endforeach
                @endif
                  @endforeach
                @endif
                

            @endforeach
            </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.category-management.custom-attribute-management.form')