@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      console.log($usertype_id);
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
              console.log(row);
            body += '<tr data-id="' + row.id + '">' + 
              '<td class="hide">' + row.status + '</td>' +
              // '<td><img id="system_user_photo" src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30"/></td>' +
              '<td>'+ row.pl_name +'</td>'+
              '<td>'+ row.period +'</td>'+
              '<td>'+ row.duration +' '+(row.duration > 1 ? 'months':'month')+'</td>'+
              '<td>'+ '$ '+row.price +'</td>'+
              '<td>'+row.discount +'</td>'+
              '<td>'+ row.points +'</td>'+
              '<td class="rightalign">'+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
  $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Banner </strong>');

                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                          if(index == "name"){
                            $
                          }
                          if(index =="get_banner_placement_plans"){
                               $.each(value, function(sindex, svalue) {
                                       $.each(svalue, function(findex, fvalue) {
                                           var field = $("#row-" + findex);
                    
                                            if(field.length > 0) {
                                                field.val(fvalue);
                                            }
                                       });
                                 });
                          }

                      });
                      $("#button-save").removeClass("hide");
                      $("#modal-add-banner").find('form').find(".hide-view").removeClass("hide");
                      $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
$(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      
                       $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Banner Rate </strong>');
                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                            console.log(index +"="+value);
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                        
                          if(index =="get_banner_placement_plans"){
                               $.each(value, function(sindex, svalue) {
                                       $.each(svalue, function(findex, fvalue) {
                                         console.log(findex+"="+fvalue);
                                           var field = $("#row-" + findex);
                                           
                                            if(field.length > 0) {
                                                field.val(fvalue);
                                               
                                            }

                                             
                                       });
                                 });
                          }

                      });
                      $("#button-save").addClass("hide");
                      $("#modal-add-banner").find('form').find(".hide-view").addClass("hide");
                      $("#modal-add-banner").find('form').find(":input").not('#button-close').not('.close').attr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-add", function() {
        $(".uploader-pane").removeClass("hide");
        $(".usertype-pane").removeClass("hide");
        $("#modal-add-banner").find(".btn-change").addClass("hide");
        $("#row-id").val("");
        $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled"); 
        $("#modal-add-banner").find('form').trigger("reset");
        $("#modal-add-banner").modal('show');
        $("#row-placement_plan").html(""); 
        $("#row-photo").addClass('hide');
        $("#button-save").removeClass("hide");
    });
     $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Banner', 'Are you sure you want to delete this banner rate</strong>?', "{{ url('admin/banner/rate/delete') }}",id);
  }); 
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right">
        <div class="col-lg-6 col-sm-6 col-xs-6">
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
         <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Usertype</label>
                  <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-filter_usertype_id" onchange="refresh()">
                          @foreach($usertype as $row)
                            <option value="{{$row->id}}">{{$row->type_name}}</option>
                          @endforeach
                    </select>
        </div>
      </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
            <thead>
                <tr>
                  <th><small>Banner Placement</small></th>
                  <th><small>Period</small></th>
                  <th><small>Discount Duration</small></th>
                  <th><small>Price</small></th>
                  <th><small>discount</small></th>
                  <th><small>Points</small></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">       
            @foreach($rows as $row)
                <tr data-id = "{{$row->id}}">
                  <td>{{$row->pl_name}}</td>
                  <td>{{$row->period.' '.($row->period > 1 ? 'months':'month')}} </td>
                  <td>{{$row->duration.' '.($row->duration > 1 ? 'months':'month')}} </td>
                  <td>{{'$ '.$row->price}} </td>
                  <td>{{$row->discount}} </td>
                  <td>{{$row->points}} </td>
                  <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                  </td> 
                </tr>
            @endforeach
            </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.banner-management.banner-rates.form')