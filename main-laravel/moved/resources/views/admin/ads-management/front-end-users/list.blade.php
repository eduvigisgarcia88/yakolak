
@extends('layout.master')
  @section('scripts')
<script type="text/javascript">

function refresh() {
  $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";

        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();
        $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 3  ? 'tr-disabled' : '')+'">' + 
              '<td>' + (row.ads_type_id == '1'?'Basic Ad':'Auction') + '</td>' +
              '<td><img id="system_user_photo" src="{{ url('uploads/ads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-responsive"/></td>' +
              '<td>' + row.title + '</td>' +
              '<td>' + row.description + '</td>' +
              '<td>' + row.updated_at  + '</td>' +
              '<td>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Deleted':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Mark as Spam':'') + '</td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam" data-toggle="modal" data-target="#"><i class="fa fa-exclamation"></i></button>' +
                (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa fa-adjust"></i></button>':'') +
                (row.status == '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-check"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>') +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>':'') +      
              '</td></tr>';
        });
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }



    function search() {
  $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";

        $.each(response.rows.data, function(index, row) {

           body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 3  ? 'tr-disabled' : '')+'">' + 
              '<td>' + (row.ads_type_id == '1'?'Basic Ad':'Auction') + '</td>' +
              '<td><img id="system_user_photo" src="{{ url('uploads/ads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30" class="img-responsive"/></td>' +
              '<td>' + row.title + '</td>' +
              '<td>' + row.description + '</td>' +
              '<td>' + row.updated_at  + '</td>' +
              '<td>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Deleted':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Mark as Spam':'') + '</td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam" data-toggle="modal" data-target="#"><i class="fa fa-exclamation"></i></button>' +
                (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa fa-adjust"></i></button>':'') +
                (row.status == '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-check"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>') +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>':'') +      
              '</td></tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");

      }, 'json');
    
    }

    $(".content-wrapper").on("click", ".btn-add", function() {
           $_token = $("#row-token").val();
           $("#modal-form-add").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add Advertisement</strong>');
           $("#modal-form-add").modal('show');
        
    });


 $(".content-wrapper").on("click", ".btn-edit", function() {
      $("#modal-form_ad_management").removeClass("hide");
      $("#form-ad_management_notice").addClass("hide");
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');

      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                  if(index=="category_id"){
                    $("#row-cat_id").attr('value', +value);
                 }
              });
   
        var cat_id = $("#row-cat_id").val();
        var country_id = $("#row-country_id").val();
        var subcategory = $("#row-sub_category_id");
        var category = $("#row-category_id");
        var cusAttr = $("#custom-attributes-pane");
        var fetchcity = $("#row-city_id");

      
        if ($("#row-ads_type_id").val()) {
        $_token = $("#row-token").val();
          subcategory.html("");
          $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            subcategory.html(response);
          }, 'json');
          }
        if ($("#row-ads_type_id").val()) {
        $_token = $("#row-token").val();
           category.html("");
          $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
            category.html(response);
          }, 'json');
        }
        if ($("#row-country_id").val()) {
         $_token = $("#row-token").val();
        fetchcity.html("");
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            fetchcity.html(response);
          }, 'json');
        }
     
       if ($("#row-ads_type_id").val()) {
        $_token = $("#row-token").val();
          cusAttr.html("");
          $.post("{{ url('admin/ads/fetch-custom-attributes-enabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttr.html(response);
          }, 'json');
        }
                      $("#button-save").removeClass("hide");
                      $("#row-ads_type_id").removeAttr('disabled','disabled');
                      $("#row-category_id").removeAttr('disabled','disabled');
                      $("#row-sub_category_id").removeAttr('disabled','disabled');
                      $("#row-title").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-countryName").removeAttr('disabled','disabled');
                      $("#row-city").removeAttr('disabled','disabled');
                      $("#row-custom_attr1").removeAttr('disabled','disabled');
                      $("#row-custom_attr2").removeAttr('disabled','disabled');
                      $("#row-address").removeAttr('disabled','disabled');
                      $("#row-youtube").removeAttr('disabled','disabled');
                      $("#row-price").removeAttr('disabled','disabled');
                      $("#row-country_id").removeAttr('disabled','disabled');
                      $("#row-city_id").removeAttr('disabled','disabled');
                      $("#row-bid_limit_amount").removeAttr('disabled','disabled');
                      $("#row-bid_start_amount").removeAttr('disabled','disabled');
                      $("#row-minimum_allowed_bid").removeAttr('disabled','disabled');
                      $("#row-bid_duration_start").removeAttr('disabled','disabled');
                      $("#row-value").removeAttr('disabled','disabled');


              // show form
              $("#modal-form_ad_management").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          
            if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }    
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");      
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      }); 
    });





$(".content-wrapper").on("click", ".btn-view", function() {
  $_token = $("#row-token").val();
      $("#form-ad_management_notice").addClass("hide");  
      $("#modal-form_ad_management").removeClass("hide");  
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');

      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                  if(index=="category_id"){
                    $("#row-cat_id").attr('value', +value);
                     }
                  });

            var cat_id = $("#row-cat_id").val();
            var country_id = $("#row-country_id").val();
            var subcategory = $("#row-sub_category_id");
            var category = $("#row-category_id");
            var cusAttr = $("#custom-attributes-pane");
            var fetchcity = $("#row-city_id");

            if ($("#row-ads_type_id").val()) {
            $_token = $("#row-token").val();
              cusAttr.html("");
              $.post("{{ url('admin/ads/fetch-custom-attributes-disabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
                cusAttr.html(response);
              }, 'json');

            }
            if ($("#row-ads_type_id").val()) {
            $_token = $("#row-token").val();
              subcategory.html("");
              $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
                subcategory.html(response);
              }, 'json');
              }
            if ($("#row-ads_type_id").val()) {
            $_token = $("#row-token").val();
               category.html("");
              $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
                category.html(response);
              }, 'json');
            }
            if ($("#row-country_id").val()) {
             $_token = $("#row-token").val();
            fetchcity.html("");
              $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
                fetchcity.html(response);
              }, 'json');
            }
            var adPictureslist = $("#list-ad-pictures");      
              adPictureslist.html("");
                $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
                  adPictureslist.html(response);
                }, 'json');

                      $("#row-ads_type_id").attr('disabled','disabled');
                      $("#row-category_id").attr('disabled','disabled');
                      $("#row-sub_category_id").attr('disabled','disabled');
                      $("#row-title").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-countryName").attr('disabled','disabled');
                      $("#row-city").attr('disabled','disabled');
                      $("#row-address").attr('disabled','disabled');
                      $("#row-youtube").attr('disabled','disabled');
        
                      $("#row-price").attr('disabled','disabled');
                      $("#row-country_id").attr('disabled','disabled');
                      $("#row-city_id").attr('disabled','disabled');
                      $("#row-bid_limit_amount").attr('disabled','disabled');
                      $("#row-bid_start_amount").attr('disabled','disabled');
                      $("#row-minimum_allowed_bid").attr('disabled','disabled');
                      $("#row-bid_duration_start").attr('disabled','disabled');
                      $("#row-value").attr('disabled','disabled');
                      $("#row-bid_duration_dropdown_id").attr('disabled','disabled');
                      $("#button-save").addClass("hide");

                  

              // show form
              $("#modal-form_ad_management").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

             if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }  
                
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });





 $("#client-country").on("change", function() { 
    $("#client-city").removeAttr('disabled','disabled');
  });  


   $(".content-wrapper").on("click", ".btn-delete-ads", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Delete Ad', 'Are you sure you want to delete ' + title + '</strong>?', "{{ url('admin/ads/delete') }}",id);
  });


   $(".content-wrapper").on("click", ".btn-mark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Mark as Spam', 'Are you sure you want to mark as spam ' + title + '</strong>?', "{{ url('admin/ads/mark-spam') }}",id);
  });


      $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Ad', 'Are you sure you want to disable ' + title + '</strong>?', "{{ url('admin/ads/disable') }}",id);
  });


      $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Ad', 'Are you sure you want to enable ' + title + '</strong>?', "{{ url('admin/ads/enable') }}",id);
  });



 $("#row-ads_type_id").on("change", function() { 
        $_token = $("#row-token").val();
        var ads_id = $("#row-id").val();
        var id = $("#row-ads_type_id").val();
        $("#auction-type-pane").removeClass("hide");
        var getcategory = $("#row-category_id"); 
                if (id == 2) {
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#auction-type-pane").addClass("hide");
        }
        // var auction = $("#auction-type-pane");
        // auction.html(" ");
        // if ($("#row-ads_type_id").val()) {
        //   $.post("{{ url('admin/auction-type') }}", { id: id, _token: $_token }, function(response) {
        //     auction.html(response);
        //   }, 'json');
          getcategory.html("");
          $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: id, ads_id: ads_id, _token: $_token }, function(response) {
            getcategory.html(response);
          }, 'json');
             
  });  

       $("#row-country_id").on("change", function() { 
        $("#row-city_id").addClass("hide");
           $("#row-city_id2").removeClass("hide");
       });  

    $("#row-country_id").on("change", function() { 
  $_token = $("#row-token").val();

        $("#row-city_id").removeClass("hide");
        var country_id = $("#row-country_id").val();  
        var city = $("#row-city_id");
        city.html("");
        if ($("#row-country_id").val()) {
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            city.html(response);
          }, 'json');

        }
        $("#row-city_id2").remove();
       });  

     $("#row-value").on("click", function() { 
     $("#row-value").addClass("hide");
     $("#row-bid_duration_dropdown_id").removeClass("hide");
    });            




    $(".content-wrapper").on("click", ".btn-settings", function() {
  $_token = $("#row-token").val();

      var settings_id = $(this).data('settings_id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-settings_id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-cog");
      $.post("{{ url('admin/ads/management-settings/edit') }}", { settings_id: settings_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-cog"></i><strong> Ad Settings</strong>');
              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                      $("#row-basic_ad_expiration_time").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
              // show form
              $("#modal-form-ad-settings").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-cog");
      }, 'json');
    });

 // $('[data-countdown]').each(function() {
 //   var $this = $(this), finalDate = $(this).data('countdown');
 //   $this.countdown(finalDate, function(event) {
 //   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
 //   });

 // });

  @foreach($rows as $row)
         $('#{{$row->id}}').countdown('{{$row->ad_expiration}}', function(event) {
   var $this = $(this).html(event.strftime(''
     + ' W: <span>%w</span> -'
     + ' D: <span>%d</span> -'
     + ' H: <span>%H</span> -'
     + ' M: <span>%M</span> -'
     + ' S: <span>%S</span>'));
 });
 @endforeach


     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");
        var ads_id = $("#ads_id").val();
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      });


         $("#row-category_id").on("change", function() { 
  $_token = $("#row-token").val();
          
        var cat_id = $("#row-category_id").val();
        var ads_id = $("#row-id").val();
        $("#custom-attribute-container").removeClass("hide");
        $("#ads-subcategory").removeClass("hide");
        $("#ads-sub_category_id").addClass("hide");
        $("#custom-attr-container").remove();
        $_token = "{{ csrf_token() }}";
        var cusAttrs = $("#custom-attributes-pane");
        cusAttrs.html(" ");
        var subcategory = $("#row-sub_category_id");
        if ($("#row-category_id").val()) {
          $.post("{{ url('admin/ads/get-custom-attributes') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttrs.html(response);
        }, 'json');
          subcategory.html("");
          $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            subcategory.html(response);
        }, 'json');

       }    
  });

    $("#row-price").change(function() {                  
        $('#row-bid_limit_amount').val(this.value);                  
    });
</script>
@stop
@section('content')
  <!-- include jquery -->
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-md btn-attr pull-right btn-settings" data-settings_id="{{$ad_management_settings->id}}"><i class="fa fa-cog"></i> Settings</button>   
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-warning btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-shield"></i> Block</button>
     <!--    <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>    
       --></div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-hover">
          <thead>
            <tr>
              <!-- <th>user</th> -->
              <th>Ad Type</th>
              <th>Photo</th>
              <th>title</th>
              <th>Description</th>
              <th>Updated at</th>
              <th>Status</th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}" data-title="{{ $row->title }}" data-ads_type_id="{{ $row->ads_type_id }}" data-cat_id="{{ $row->category_id }}" class="{{($row->status == 3 ? 'tr-disabled' : '') }}">
                <td>
                  @if ($row->ads_type_id == '1')
                    Basic Ad
                  @else
                    Auction
                  @endif
                </td>
                <td><img src = "{{url('uploads/ads').'/'.$row->photo}}" height="30" width="30" class="img-responsive"></td>
                <td>{{$row->title}}</td>
                <td>{{$row->description}}</td>
                <td>{{$row->updated_at}}</td>
                <td>
                  @if ($row->status == '1')
                      Active
                  @elseif ($row->status == '2')
                      Deleted
                  @elseif ($row->status == '3')
                      Disabled 
                  @else
                      Mark as Spam
                  @endif
                </td>
                 <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam" data-toggle="modal" data-target="#"><i class="fa fa-exclamation"></i></button>
                 @if ($row->status == '2')
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-check"></i></button>
                 @else 
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right {{ ($row->status == 1? 'btn-disable':'btn-enable')}}" data-toggle="modal" data-target="#"><i class="fa fa-adjust"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                 @endif 
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                 @if ($row->status != '2')
                  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                 @endif
                </td> 
              </tr>
            @endforeach
          <!--   <tr>
              <td>John</td>
              <td>Doe</td>
              <td>john@example.com</td>
              
            </tr> -->
       </table>

     
     </div>
       <div class="panel-footer panelFooter">
    <div class="row">
       <div id="row-pages" class="col-lg-6 col-xs-8">{!! $pages !!}</div>
      <div class="col-lg-6 col-xs-4">
        <select class="form-control borderZero pull-right" onchange="search()" id="row-per" style="margin: 0; width:70px; border: 0px;">
          <option value="10">10</option>
          <option value="25">25</option>
          <option value="50">50</option>
          <option value="50">100</option>
        </select>
      </div>
    </div>
  </div>
  </div>
</div>
@stop
@include('admin.ads-management.front-end-users.form')