@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  $loading_expand = false;
  $expand_id = 0;


  // refresh the list
  function refresh() {
      var table = $("#rows");
      $loading_expand = false;
      $expand_id = 0;
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      console.log($usertype_id);
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
           
            body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.plan_status == 2  ? 'tr-disabled' : '')+'">' 
              // '<td class="hide">' + row.status + '</td>' +
                 + '<td>' + '<a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a>' + '</td>' + 
             '<td>'+ row.plan_name +'</td>'+
              '<td>'+ row.total_ads_allowed +'</td>'+
              '<td>'+row.point +'</td>'+
              '<td>'+ '$ '+row.months12_price +'</td>'+
           '<td class="rightalign">'+  
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" title="Delete"><i class="fa fa-trash-o"></i></button>'+ 
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add"><i class="fa fa-plus"></i></button>' 
                 
               +'</td>'+
              '<td class="rightalign">'+ (row.plan_status == 1 ? '<input data-id = "'+row.id+'" type="checkbox" name="my-checkbox-'+row.id+'" id="my-checkbox-'+row.id+'" checked>':'<input data-id = "'+row.id+'" type="checkbox" id="my-checkbox-'+row.id+'" name="my-checkbox">')+
              '</td>'+'</tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        @foreach($rows as $row)
        $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
        @endforeach
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }

  
      $(".content-wrapper").on("click", ".btn-add", function() {
          $("#modal-form-add").find('form').trigger("reset");
                     $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add User Plan</strong>');
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#row-plan_name").removeAttr('disabled','disabled');
                      $("#row-months6_price").removeAttr('disabled','disabled');
                      $("#row-months12_price").removeAttr('disabled','disabled');
                      $("#row-ads").removeAttr('disabled','disabled');
                      $("#row-auction").removeAttr('disabled','disabled');
                      $("#row-img_per_ad").removeAttr('disabled','disabled');
                      $("#img_per_ad").removeAttr('disabled','disabled');
                      $("#row-img_total").removeAttr('disabled','disabled');
                      $("#row-total_ads_allowed").removeAttr('disabled','disabled');
                      $("#row-video_total").removeAttr('disabled','disabled');
                      $("#row-point").removeAttr('disabled','disabled');
                      $("#row-bid").removeAttr('disabled','disabled');
                      $("#row-point_exchange").removeAttr('disabled','disabled');
                      $("#row-sms").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");   
                      $("#modal-form-add-plan").modal('show');
    });


    $(".content-wrapper").on("click", ".btn-edit", function() {
       var id = $(this).parent().parent().data('id');
       console.log(id);
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.plan_name + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#row-plan_name").removeAttr('disabled','disabled');
                      $("#row-months6_price").removeAttr('disabled','disabled');
                      $("#row-months12_price").removeAttr('disabled','disabled');
                      $("#row-ads").removeAttr('disabled','disabled');
                      $("#row-auction").removeAttr('disabled','disabled');
                      $("#row-img_per_ad").removeAttr('disabled','disabled');
                      $("#img_per_ad").removeAttr('disabled','disabled');
                      $("#row-img_total").removeAttr('disabled','disabled');
                      $("#row-total_ads_allowed").removeAttr('disabled','disabled');
                      $("#row-video_total").removeAttr('disabled','disabled');
                      $("#row-point").removeAttr('disabled','disabled');
                      $("#row-bid").removeAttr('disabled','disabled');
                      $("#row-point_exchange").removeAttr('disabled','disabled');
                      $("#row-sms").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");

              // show form
              $("#modal-form-add-plan").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });




   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form-add").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Viewing <strong>' + response.plan_name + '</strong>');
                  
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                    
                      $("#row-usertype_id").attr('disabled','disabled');
                      $("#row-plan_name").attr('disabled','disabled');
                      $("#row-months6_price").attr('disabled','disabled');
                      $("#row-months12_price").attr('disabled','disabled');
                      $("#row-ads").attr('disabled','disabled');
                      $("#row-auction").attr('disabled','disabled');
                      $("#row-img_per_ad").attr('disabled','disabled');
                      $("#img_per_ad").attr('disabled','disabled');
                      $("#row-img_total").attr('disabled','disabled');
                      $("#row-total_ads_allowed").attr('disabled','disabled');
                      $("#row-video_total").attr('disabled','disabled');
                      $("#row-point").attr('disabled','disabled');
                      $("#row-bid").attr('disabled','disabled');
                      $("#row-point_exchange").attr('disabled','disabled');
                      $("#row-sms").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form-add-plan").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

//DONT DELETE
   $("#usertype_id").on("change", function() { 
        var id = $("#usertype_id").val();
       var plan_id = $("#row-id").val();
 console.log(plan_id);
        // $("#user-type-container").addClass("hide");
        $_token = "{{ csrf_token() }}";
        var usertype = $("#user-type-pane");
        usertype.html("");
        if ($("#usertype_id").val()) {
          $.post("{{ url('user-types') }}", { id: id, plan_id: plan_id, _token: $_token }, function(response) {
            usertype.html(response);
          }, 'json');
       }
  });  


  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Plan', 'Are you sure you want to delete ' + plan_name + '</strong>?', "{{ url('admin/plan/delete') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-disabled", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      console.log(plan_name)
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Plan', 'Are you sure you want to disable ' + plan_name +'</strong>?', "{{ url('admin/plan/disable') }}",id);
  });

    $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      console.log(plan_name);
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Plan', 'Are you sure you want to enable ' + plan_name +'</strong>?', "{{ url('admin/plan/enable') }}",id);
  });


  @foreach($rows as $row)
  $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
 
  $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-{{$row->id}}', function(event, state) {

      var id = $(this).data('id');
      var status = state;
      var stat = 0;
      if(status == true){
          stat = 1;
      }
      if(status == false){
          stat = 2;
      }
       $.post("{{ url('admin/plan/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
        refresh();
      }, 'json');
   
    });
   @endforeach



    $(".content-wrapper").on("click", ".btn-add-price", function() {

      var plan_id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
       $("#row-plan_id").val(plan_id);
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/plan/price/manipulate') }}",{ plan_id: plan_id,plan_name: plan_name, _token: $_token}, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-plus"></i> Adding Price to <strong>' + plan_name + '</strong>');
              // output form data
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");

              // show form
          } else {
              status(false, response.error, 'alert-danger');
          }
        $("#modal-form-plan-price").find(".btn-change").addClass("hide");
        $("#modal-form-plan-price").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled"); 
        $("#modal-form-plan-price").find('form').trigger("reset");
        $("#modal-form-plan-price").modal('show');

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });



    $(".content-wrapper").on("click", ".btn-expand", function() {
       $(".btn-expand").html('<i class="fa fa-minus"></i>');
       $(".btn-expand").html('<i class="fa fa-plus"></i>');
      $type_id = 0;
       var plan_id = $(this).parent().parent().data('id');
       console.log(plan_id);
      if ($loading_expand){
        return;
      }
      $tr = $(this).parent().parent();
      $id = '1';
      $_token = '{{ csrf_token() }}';
      $selected = '.row-expand-' + $expand_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $expand_id) {
        $expand_id = 0;
        return
      }
      $expand_id = $id;
      $button = $(this);
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_expand = true;

      $.post("{{ url('admin/plan/get-plan-price') }}", { id: $id, plan_id: plan_id, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding:10px"><div class="row-expand-1" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        } else {
          status(false, response.error, 'alert-danger');
        }
      });
    });



 $(".content-wrapper").on("click", ".btn-price-edit", function() {
       var price_id = $(this).parent().parent().data('price_id');
       var plan_id = $(this).parent().parent().data('plan_id');
       console.log(plan_id);
       var price = $(this).parent().parent().data('price');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-price_id").val(price_id);
      $("#row-plan_id").val(plan_id);
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan/price/manipulate') }}", { price_id: price_id, plan_id: plan_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.price + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }

              });
                $("#row-usertype_id").removeAttr('disabled','disabled');
                $("#row-plan_name").removeAttr('disabled','disabled');
                $("#button-save").removeClass("hide");
              // show form
        $("#modal-form-plan-price").modal('show');
              
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });



  $(".content-wrapper").on("click", ".btn-price-delete", function() {
       var id = $(this).parent().parent().data('price_id');
       var price = $(this).parent().parent().data('price');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Price', 'Are you sure you want to delete ' + price + '</strong>?', "{{ url('admin/plan/price/delete') }}",id);
  });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#" disabled><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
                  <div class="col-lg-12 col-sm-12 col-xs-12 pull-right">
        <div class="col-lg-6 col-sm-6 col-xs-6">
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
         <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Usertype</label>
                  <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-filter_usertype_id" onchange="javascript:window.location.href='company'">
                          @foreach($usertype as $row)
                            <option value="{{$row->id}}">{{$row->type_name}}</option>
                          @endforeach
                    </select>
        </div>
      </div>
    </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
          <thead>
              <th></th>
              <th>Plan</th>
              <th>Allowed Ad Post</th>
              <th>Points</th>
              <th>12 months Price</th>

              <th class="text-right">Tools</th>
            </tr>
          </thead>
            <tbody id="rows"> 
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-plan_name="{{ $row->plan_name }}" data-usertype_id="{{ $row->usertype_id }}" class="{{($row->plan_status == 2 ? 'tr-disabled' : '') }}">
                 <td><a class="btn btn-xs btn-table btn-expand"><i class="fa fa-plus"></i></a>
              </td>
              <td>
              <?php echo($row->plan_name); ?> 
              </td>
              <td>
              <?php echo($row->total_ads_allowed); ?> 
              </td>
               <td>
              <?php echo($row->point); ?> 
              </td>
               <td>
              $<?php echo($row->months12_price); ?> 
              
              </td>
               <td class="rightalign">
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" title="Delete"><i class="fa fa-trash-o"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" title="View"><i class="fa fa-eye"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add"><i class="fa fa-plus"></i></button>

              </td>
              <td class="rightalign">
                    @if($row->plan_status == 1)
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}" checked> 
                  
                   @else
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}"> 
                    @endif
              </td>
           </tr>
           @endforeach
          </tbody>
       </table>
{!! $pages !!}   
     </div>
  </div>
</div>
@stop
@include('admin.plan-management.plan.form')