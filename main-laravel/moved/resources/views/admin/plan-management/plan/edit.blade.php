@extends('layout.master')
@section('scripts')
  <script type="text/javascript">
    $_token = '{{ csrf_token() }}';
    
    $(document).ready(function() {
      $('.summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });

	
	// submit form
    $("#form-save").find("#modal-save_form").submit(function(e) {
  		var form = $("#form-save").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: '{{ url("admin/news/save") }}',
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {
				
		        if (response.unauthorized) {
		          window.location.href = '{{ url("login")}}';
		        }
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#load-success").removeClass('hide');
				    $.ajax({
				        type: 'POST',
				        url: '{{ url("admin/news/flash/edit") }}',
				        data: { _token: $_token }
				    }).done(function () {
				        window.location.href = '{{ url("admin/news")}}';
				    });
				}
				loading.addClass('hide');
			}
  		});
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li><a href="{{ url('admin/email-template') }}">Email Template</a></li>
    <li><a href="#">Edit</a></li>
  </ol>
</div>
	<div id="save-email-template" class="col-lg-10">
	<div id="load-form" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	<div id="email-load-success" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	{!! Form::open(array('url' => 'admin/email-template/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'form_email-template', 'files' => 'true')) !!}
<div class="borderZero panel">
  <div co�s��"�q8m-�-Ey>
  	<div id="email-template-notice"></div>
	<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-title', 'Ti
lr[.?�%��#s�' ]>8��oGvr{L(Labu� nold-color']) !!}
	</div>
	<div class="col-sm-10">
		<input type="text" class="form-control borderZero" n�-D-�dapNu" id="roq�t�4.�"4�m-tm"�[,.ewsletters->title }}">
	</div>
	</div>
		<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-internal name', 'Internal name', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<div class="col-sm-4">
		<input type="text" class="form-control borderZero" name="internal name" id="row-internal-name" value="{{ $newsletters->newsletter_name }}" disabled>
		</div>
			<div class="col-sm-8">
		<span>Used to identify email template</span>
			</div>
	</div>
	</div>
	<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-body', 'Body', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<textarea name="body" class="form-control summernote borderZero" id="row-body">{{$newsletters->body}}</textarea>
	</div>
	</div>
  </div>
  <div class="panelFooter">
  	<div class="row">
  		<div class="col-lg-6 col-xs-4">
  			<a class="btn btn-goback btn-back borderZero" href="{{ url('admin/email-template') }}">Back</a>
  		</div>
  		<div class="col-lg-6 col-xs-8 text-right">
  			<input type="hidden" name="id" value="{{ $newsletters->id }}">
  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update Template</button>
  		</div>
  	</div>
  </div>
</div>
{!! Form::close() !!}
</div>
	<div id="save-email-template" class="col-lg-2 col-sm-4">
		<h5><center>LEGEND</center></h5>
		<div class="panel panel-default bottomMarginLight removeBorder borderZero">
		<div class="panel-body topPaddingB bgGray normalText">
			<p>{CONTACT_NAME}</p>
			<small>Contact name</small>
			<hr>
			<p>{USER_NAME}</p>
			<small>User name</small>
			<hr>
			<p>{USER_PHONE}</p>
			<small>User phone number</small>
			<hr>
			<p>{ITEM_TITLE}</p>
			<small>Listing Title</small>
			<hr>
		</div>
		</div>
</div>
@stop