
<div class="modal fade" id="modal-form-add" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-plus"></i> Add User Plan</h4>
        </div>
         
            <div id="form-user_plan_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan-features/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-user_plan_save_form', 'files' => true)) !!}

            <div class="modal-body"> 
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="form-group">
                        <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">UserType</label>
                <div class="col-sm-7 col-xs-7">
                       <select type="text" class="form-control" id="row-usertype_id" name="usertype_id">
                              @foreach ($usertype as $usertypes)
                            <option value="{{ $usertypes->id}}" {{($usertypes->id==$plan->usertype_id ? 'selected' : '')}}>{{ $usertypes->type_name }}</option>
                            @endforeach
                          </select> 
                </div>
                </div>
            </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Plan Name</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-plan_name" name="plan_name">
                       </div>
                   </div>
                </div>
                    <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Ad</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-ads" name="ads">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Auction</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-auction" name="auction">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Image per Ad</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-img_per_ad" name="img_per_ad">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Image</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-img_total" name="img_total">
                       </div>
                   </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Video</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-video_total" name="video_total">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Points</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-point" name="point">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Bids</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-bid" name="bid">
                       </div>
                   </div>
                </div>
                                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Point Exchange</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-point_exchange" name="point_exchange">
                       </div>
                   </div>
                </div>
                                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">SMS</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-sms" name="sms">
                       </div>
                   </div>
                </div>
              <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Ads Allowed</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-total_ads_allowed" name="total_ads_allowed">
                       </div>
                   </div>
                </div>
                   <div id="user-type-pane"></div>
     <div  id="user-type-container">            
        <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color" style="padding-top: 9;">6 Months Price</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-months6_price" name="months6_price">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color" style="padding-top: 9;">12 Months Price
                    </label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-months12_price" name="months12_price">
                       </div>
                   </div>
                </div>

               </div>

                      
   </div>
   </div>
   </div>



        <div class="modal-footer">
         <input type="hidden" name="id" id="row-id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>











    <div class="modal fade" id="modal-plan-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'admin/plan-features/remove', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-plan-delete_form', 'files' => true)) !!}
      <div class="modal-header modal-delete">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Ads</h4>
      </div>
      <div class="modal-body">
        <div id="form-plan-notice"></div>
        <div class="form-group">
        <div class="col-lg-12">
          Do you want to delete? 
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="plan_id" id="row-plan_id" value="">
        <button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
        <button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
