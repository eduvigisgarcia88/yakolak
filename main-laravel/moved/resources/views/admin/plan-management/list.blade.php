@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';

  // refresh the list
  function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#row");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
        console.log($_token);
      $.post("{{ url('admin/plan-management/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

        // clear table
        table.html("");
        // populate table
        $.each(response.rows.data, function(index, row) {
          console.log(row);
            table.append(

                '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '">' +
                '<td>' + row.plan_name + '</td>' +
                // '<td>' + desc_eng + '</td>' +
                '<td>' + row.ads + '</td>' +
                '<td class="text-right">' + 
                      '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
             
                '</td>' +
                '</tr>'
            );
        });

      // update links
      $("#row-pages").html(response.pages);

      $(".sort-refresh").find('i').addClass('fa fa-sort-up');
      loading.addClass("hide");

      }, 'json');
  }
  
      $(".content-wrapper").on("click", ".btn-add", function() {
          $("#modal-form-add").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add User Plan</strong>');
           $("#modal-form-add").modal('show');
    });



    $(".content-wrapper").on("click", ".btn-edit", function() {
       var id = $(this).parent().parent().data('id');
       console.log(id);
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#modal-form-add").find('form').trigger("reset");
      $("#row-id").val("");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan-management/manipulate') }}", { id: id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.plan_name + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }
              });
              // show form
              $("#modal-form-add").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });



   $("#user-type").on("change", function() { 
        var id = $("#user-type").val();
 
        $("#user-type-container").addClass("hide");
        $_token = "{{ csrf_token() }}";
        var usertype = $("#user-type-pane");
        usertype.html("");
        if ($("#user-type").val()) {
          $.post("{{ url('user-types') }}", { id: id, _token: $_token }, function(response) {
            usertype.html(response);
          }, 'json');
       }
  });  



//              $(".content-wrapper").on("click", ".btn-delete", function() {
//        var plan_id = $(this).parent().parent().data('id');
//         var plan_name = $(this).parent().parent().data('plan_name');
// console.log(plan_name);
//       $("#row-plan_id").val(plan_id);
//       $("#row-plan_name").val(plan_name);
//           $("#modal-plan-delete").find('form').trigger("reset");
//           $(".modal-title").html('<strong>Delete Ad '+ plan_name +' </strong>', plan_name);
//            $(".modal-title").html('<i class="fa fa-plus"></i><strong>Delete User Plan</strong>');
//            $("#modal-plan-delete").modal('show');
//     });


  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Plan', 'Are you sure you want to delete this Plan</strong>?', "{{ url('admin/plan-management/delete') }}",id);
  });

</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#" disabled><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
          <thead>
              <th>Plan</th>
              <th>Allowed Ad Post</th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
            <tbody id="rows"> 
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-plan_name="{{ $row->plan_name }}">
              <td>
              <?php echo strtoupper($row->plan_name); ?> 
              </td>
              <td>
              <?php echo($row->ads); ?> 
              </td>
               <td class="rightalign">
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>
              </td>
           </tr>
           @endforeach
          </tbody>
       </table>
{!! $pages !!}   
     </div>
  </div>
</div>
@stop
@include('admin.plan-management.form')