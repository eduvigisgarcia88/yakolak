@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
      $(document).ready(function() {
      $('.summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });

  // refresh the list
  function refresh() {
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      console.log($usertype_id);
      $per = $("#row-per").val();
        var url = "{{ url('/') }}";
      
      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
   // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
              console.log(row);
            body += '<tr data-id="' + row.id + '" class="'+ (row.rate_status == 2  ? 'tr-disabled' : '')+'">' + 
              '<td class="hide">' + row.status + '</td>' +
             '<td>'+ row.rate_name +'</td>'+
              '<td>'+ row.duration +' '+(row.duration > 1 ? 'months':'month')+'</td>'+
              '<td>'+row.discount +'%'+'</td>'+
              '<td>'+ row.description +'</td>'+
              '<td class="rightalign">'+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 ' <a href="' + url + '/admin/plan-rates/company/' + row.id + '/edit"><button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>' +
            '</td>';
          body += '<td class="rightalign">'+ (row.rate_status == 1 ? '<input data-id = "'+row.id+'" type="checkbox" name="my-checkbox-'+row.id+'" id="my-checkbox-'+row.id+'" checked>':'<input data-id = "'+row.id+'" type="checkbox" id="my-checkbox-'+row.id+'" name="my-checkbox">')+
              '</td>'+'</tr>';
        });       
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        @foreach($rows as $row)
        $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
        @endforeach
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");
      }, 'json');

  }
  
  $(".content-wrapper").on("click", ".btn-disabled", function() {
      var id = $(this).parent().parent().data('id');
      var rate_name = $(this).parent().parent().data('rate_name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Plan Rate', 'Are you sure you want to delete ' + rate_name +'</strong>?', "{{ url('admin/plan-rates/disabled') }}",id);
  });


  $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form-plan-rate").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/plan-rates/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Viewing <strong>' + response.plan_name + '</strong>');
                  
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);           
                          }
                      });
                    
                       $("#row-rate_name").attr('disabled','disabled');
                       $("#row-duration").attr('disabled','disabled');
                       $("#row-description").attr('disabled','disabled');
                       $("#row-discount").attr('disabled','disabled');
                       $("#row-user-type").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form-plan-rate").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }
                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json'); 
      });


 @foreach($rows as $row)
  $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
 
  $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-{{$row->id}}', function(event, state) {

      var id = $(this).data('id');
      var status = state;
      var stat = 0;
      if(status == true){
          stat = 1;
      }
      if(status == false){
          stat = 2;
      }
       $.post("{{ url('admin/plan-rates/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
        refresh();
      }, 'json');
   
    });
   @endforeach
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#" disabled><i class="fa fa-trash-o"></i> Remove</button>  -->
        <a href="{{ url('admin/plan-rates/create') }}"><button type="button" class="btn btn-info btn-md btn-attr pull-right"><i class="fa fa-book"></i> Add</button></a>
      </div>

            <div class="col-lg-12 col-sm-12 col-xs-12 pull-right">
        <div class="col-lg-6 col-sm-6 col-xs-6">
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
         <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Usertype</label>
                  <div class="col-sm-9 col-xs-7">
                    <select class="form-control" name="usertype_id" id="row-filter_usertype_id" onChange="javascript:window.location.href='vendor'">
                          @foreach($usertype as $row)
                            <option value="2" selected>{{$row->type_name}}</option>
                          @endforeach
                    </select>
        </div>
      </div>
    </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
          <thead>
              <th>Rate Name</th>
              <th>Period</th>
              <th>Discount</th>
              <th>Description</th>
              <th class="text-right">Tools</th>
              <th></th>
            </tr>
          </thead>
            <tbody id="rows"> 
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-rate_name="{{ $row->rate_name }}" class="{{($row->rate_status == 2 ? 'tr-disabled' : '') }}">
              <td>
              <?php echo strtoupper($row->rate_name); ?> 
              </td>
              <td>
              <?php echo($row->duration); ?> 
              </td>
              <td>
              <?php echo($row->discount); ?>%
              </td>
               <td>
              <?php echo($row->description); ?> 
              </td>
               <td class="rightalign">
                  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>
                 <a href="{{ url('admin/plan-rates/company') . '/' . $row->id . '/edit'}}"><button type="button" class="btn btn-default btn-sm btn-attr pull-right"><i class="fa fa-pencil"></i></button></a>
                 
              </td>
                   <td class="rightalign">
                    @if($row->rate_status == 1)
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}" checked> 
                  
                   @else
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}"> 
                    @endif
              </td>
           </tr>
           @endforeach
          </tbody>
       </table>
{!! $pages !!}   
     </div>
  </div>
</div>
@stop
@include('admin.plan-management.plan-rates.form')