@extends('layout.master')
@section('scripts')
  <script type="text/javascript">
    $_token = '{{ csrf_token() }}';
    
    $(document).ready(function() {
      $('.summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });

	
	// submit form
    $("#form-save").find("#modal-save_form").submit(function(e) {
  		var form = $("#form-save").find("form");
  		var loading = $("#load-form");

  		// stop form from submitting
  		e.preventDefault();

  		loading.removeClass('hide');
  		// console.log(form.serialize());
  		// push form data
  		$.ajax({
			type: "post",
			url: '{{ url("admin/news/save") }}',
			//data: form.serialize(),
			data: new FormData($("#modal-save_form")[0]),
			dataType: "json",	
			async: true,
	  		processData: false,
	  		contentType: false,

			success: function(response) {
				
		        if (response.unauthorized) {
		          window.location.href = '{{ url("login")}}';
		        }
				if(response.error) {
					var errors = '<ul>';

					$.each(response.error, function(index, value) {
						errors += '<li>' + value + '</li>';
					});

					errors += '</ul>';

					status('Please correct the following:', errors, 'alert-danger', '#form-notice');
				} else {
					$("#load-success").removeClass('hide');
				    $.ajax({
				        type: 'POST',
				        url: '{{ url("admin/plan-rates/flash/edit") }}',
				        data: { _token: $_token }
				    }).done(function () {
				        window.location.href = '{{ url("admin/dashboard")}}';
				    });
				}
				loading.addClass('hide');
			}
  		});
    });
</script>
@stop

@section('content')
<div class="page-header">
  <h1>Dashboard</h1>
  <ol class="breadcrumb">
    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
    <li><a href="{{ url('admin/plan-rates') }}">Plan Rate</a></li>
    <li><a href="#">Edit</a></li>
  </ol>
</div>
	<div id="form-plan_rate_save" class="col-lg-10">
	<div id="load-form" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	<div id="email-load-success" class="loading-pane hide">
	  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
	</div>
	{!! Form::open(array('url' => 'admin/plan-rates/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-user_plan_rate_save', 'files' => 'true')) !!}
<div class="borderZero panel">
  <div class="panel-body">
  	<div id="plan-rate-notice"></div>
    <div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-usertype_id', 'User Type', ['class' => 'control-label font-color']) !!} 
	</div>
	<div class="col-sm-10">
         <select class="form-control borderZero"  name="usertype_id" id="row-usertype_id">
  	       @foreach($usertype as $usertypes)
                <option value="{{ $usertypes->id}}" {{($usertypes->id==$usertype_id ? 'selected' : '')}}>{{ $usertypes->type_name }}</option>
            @endforeach
     </select>
	</div>
	</div>
	<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-rate_name', 'Rate Name', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<input type="text" class="form-control borderZero" name="rate_name" id="row-rate_name" value="{{ $planrate->rate_name }}">
	</div>
	</div>
		<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-duration', 'Duration', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<input type="text" class="form-control borderZero" name="duration" id="row-duration" value="{{ $planrate->duration }}">
	</div>
	</div>
		<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-discount', 'Discount', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<input type="text" class="form-control borderZero" name="discount" id="row-discount" value="{{ $planrate->discount }}">
	</div>
	</div>

	<div class="form-group">
	<div class="col-sm-2">
		{!! Form::label('row-description', 'Description', ['class' => 'control-label font-color']) !!}
	</div>
	<div class="col-sm-10">
		<textarea name="description" class="form-control summernote borderZero" id="row-description">{{$planrate->description}}</textarea>
	</div>
	</div>
  </div>
  <div class="panelFooter">
  	<div class="row">
  		<div class="col-lg-6 col-xs-4">
  			<a class="btn btn-goback btn-back borderZero" href="{{ url('admin/plan-rates/company') }}">Back</a>
  		</div>
  		<div class="col-lg-6 col-xs-8 text-right">
  			<input type="hidden" name="id" value="{{ $planrate->id }}">
  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update Rate</button>
  		</div>
  	</div>
  </div>
</div>
{!! Form::close() !!}
</div>
	<div id="save-email-template" class="col-lg-2 col-sm-4">
		<h5><center>LEGEND</center></h5>
		<div class="panel panel-default bottomMarginLight removeBorder borderZero">
		<div class="panel-body topPaddingB bgGray normalText">
			<p>{CONTACT_NAME}</p>
			<small>Contact name</small>
			<hr>
			<p>{USER_NAME}</p>
			<small>User name</small>
			<hr>
			<p>{USER_PHONE}</p>
			<small>User phone number</small>
			<hr>
			<p>{ITEM_TITLE}</p>
			<small>Listing Title</small>
			<hr>
		</div>
		</div>
</div>
@stop