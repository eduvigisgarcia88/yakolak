
<div class="modal fade" id="modal-form-plan-rate" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-rate-title"><i class="fa fa-plus"></i> Add User Plan</h4>
        </div>
         
            <div id="form-user_plan_rate_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan-rates/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-user_plan_rate_save_form', 'files' => true)) !!}

            <div class="modal-body"> 
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="form-group">
                        <label for="row-name" class="col-sm-4 col-xs-4 control-label font-color">UserType</label>
                <div class="col-sm-8 col-xs-8">
                       <select type="text" class="form-control" id="row-user-type" name="user_type">
                              @foreach ($usertype as $rows)
                            <option value="{{ $rows->id}}" {{($rows->id==$row->usertype_id ? 'selected' : '')}}>{{ $rows->type_name }}</option>
                            @endforeach
                          </select> 
                </div>
                </div>
            </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Rate Name</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-rate_name" name="rate_name">
                       </div>
                   </div>
                </div>
                    <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-4 col-xs-4 control-label font-color">Duration</label>
                       <div class="col-sm-8 col-xs-8">
                         <input type="text" class="form-control" id="row-duration" name="duration">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Discount</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control" id="row-discount" name="discount">
                       </div>
                   </div>
                </div>
<!--                 <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-2 col-xs-2 control-label font-color">Description</label>
                       <div class="col-sm-10 col-xs-10">
                         <textarea class="form-control" id="row-description" name="description">{{ $row->description}}</textarea>
                       </div>
                   </div>
                </div> -->

               

                      
   </div>
   </div>
   </div>



        <div class="modal-footer">
         <input type="hidden" name="id" id="row-id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>











    <div class="modal fade" id="modal-plan-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'admin/plan-rates/remove', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-plan-delete_form', 'files' => true)) !!}
      <div class="modal-header modal-delete">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Ads</h4>
      </div>
      <div class="modal-body">
        <div id="form-plan-notice"></div>
        <div class="form-group">
        <div class="col-lg-12">
          Do you want to delete? 
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="plan_id" id="row-plan_id" value="">
        <button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
        <button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
