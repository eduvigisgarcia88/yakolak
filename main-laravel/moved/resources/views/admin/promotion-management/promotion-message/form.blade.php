<!-- modal add user -->
  <div class="modal fade" id="modal-promotion-message" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/promotion/message/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-promotion', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plane"></i> PROMOTION MESSAGE</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Send To</label>
                <div class="col-sm-9 col-xs-7">
                    <select class="form-control" id="row-usertype_id" name="usertype_id">
                          <option class="hide" value="">Select</option>
                          @foreach($usertypes as $row)
                            <option value="{{$row->id}}">{{$row->type_name}}</option>
                          @endforeach
                    </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Subject</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="subject" class="form-control" id="row-subject" maxlength="30">
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Message</label>
                <div class="col-sm-9 col-xs-7">
                    <textarea class="form-control" rows="5" id="row-message" name="message"></textarea>
                </div>
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-send"></i> Send</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  