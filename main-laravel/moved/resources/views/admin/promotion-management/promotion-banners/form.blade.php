<!-- modal add user -->
  <div class="modal fade" id="modal-promotion-banner" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/promotion/banner/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-promotion', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD PROMOTION BANNER</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                    <img src="#" id="row-photo" height="100" width="100%">
                    <button type="button" class="btn btn-primary pull-right hide-view btn-change"><i class="fa fa-camera"></i> Change Photo</button>
                    <button type="button" class="btn btn-primary pull-right btn-cancel">Cancel</button>
                    <div class="uploader-pane">
                        <div class='change'>
                            <input name='photo' id='row-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo'>
                        </div>
                     </div>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="name" class="form-control" id="row-name" maxlength="30">
                </div>               
                </div>
                <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Position</label>
                   <div class="col-sm-9 col-xs-7">
                     <select class="form-control" id="row-banner_position_id" name="banner_position_id">
                      <option class="hide">Select</option>
                      @foreach($position as $row)
                        <option value ="{{$row->id}}">{{$row->name}}</option>
                      @endforeach
                     </select>
                </div>
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  