@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';

  // refresh the list
  function refresh() {
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
        console.log($_token);
      $.post("{{ url('admin/email-template/refresh') }}", { page: $page, sort: $sort, order: $order, search: $search, per: $per, _token: $_token }, function(response) {
        
        if (response.unauthorized) {
          window.location.href = '{{ url("login")}}';
        }

    var body = "";
        // clear table
        table.html("");
        // populate table
        $.each(response.rows.data, function(index, row) {
           
            body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? 'tr-disabled' : '')+'">' + 
              '<td class="hide">' + row.status + '</td>' +
              '<td>'+ row.newsletter_name + '</td>' +
              '<td>'+ row.title +'</td>'+
              '<td>'+ row.created_at +'</td>'+
              '<td class="rightalign">'+   
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 ' <a href="' + url + '/admin/email-template/' + row.id + '/edit"><button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>' 
               +'</td>'+
              '<td class="rightalign">'+ (row.newsletter_status == 1 ? '<input data-id = "'+row.id+'" type="checkbox" name="my-checkbox-'+row.id+'" id="my-checkbox-'+row.id+'" checked>':'<input data-id = "'+row.id+'" type="checkbox" id="my-checkbox-'+row.id+'" name="my-checkbox">')+
              '</td>'+'</tr>';
         
        });
        table.html(body);

      // update links
         $("#modal-form").find('form').trigger("reset");
        table.html(body);
        @foreach($rows as $row)
        $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
        @endforeach
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
  




   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var body = $(this).data('body');
     
        console.log(body);
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form-plan-rate").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/email-template/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Viewing <strong>' + response.newsletter_name + '</strong>');
                  
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                    
                       $("#row-newsletter_name").attr('disabled','disabled');
                       $("#row-title").attr('disabled','disabled');
                       $("#row-created_at").attr('disabled','disabled');
                       $("#row-discount").attr('disabled','disabled');
                       $("#row-user-type").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-email-template").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

  @foreach($rows as $row)
  $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
 
  $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-{{$row->id}}', function(event, state) {

      var id = $(this).data('id');
      var status = state;
      var stat = 0;
      if(status == true){
          stat = 1;
      }
      if(status == false){
          stat = 2;
      }
       $.post("{{ url('admin/email-template/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
        refresh();
      }, 'json');
   
    });
   @endforeach
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
<!--         <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#" disabled><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add" disabled><i class="fa fa-book"></i> Add</button>     -->
      </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
          <thead>
              <th>Name</th>
              <th>title</th>
              <th>Created at</th>
              <th class="text-right">Tools</th>
              <th></th>
            </tr>
          </thead>
            <tbody id="rows">
           
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-name="{{ $row->newsletter_name }}" class="{{($row->newsletter_status == 2 ? 'tr-disabled' : '') }}">
             <td>
               <?php echo($row->newsletter_name); ?>
             
            </td>
              <td>
               <?php echo($row->title); ?>
              </td>
              <td>
              <?php echo($row->created_at); ?> 
              </td>
               <td class="rightalign">
                <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" title="View"><i class="fa fa-eye"></i></button>
                 
                 <a href="{{ url('admin/email-template') . '/' . $row->id . '/edit'}}"><button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" title="Edit"><i class="fa fa-pencil"></i></button></a>  
                
              </td>
              <td class="rightalign">
                     @if($row->newsletter_status == 1)
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}" checked>
                    @else
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}"> 
                    @endif
              </td>       
           </tr>
@endforeach
          </tbody>
       </table>

     </div>
  </div>
</div>
@stop
@include('admin.email-template-management.form')