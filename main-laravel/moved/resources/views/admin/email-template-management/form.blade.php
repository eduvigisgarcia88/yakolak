<!-- modal add user -->
  <div class="modal fade" id="modal-email-template" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/banner/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD BANNER</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="newsletter_name" class="form-control" id="row-newsletter_name" maxlength="30" placeholder="Enter your name...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Title</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="title" class="form-control" id="row-title" maxlength="30" placeholder="Enter your sequence...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Subject</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="subject" class="form-control" id="row-subject" maxlength="30" placeholder="Enter your sequence...">
                </div>
                </div>
                <!-- <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Body</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="body" id ="row-body" placeholder="Enter your category description"></textarea>
                </div>
                </div> --> 
                <div class="form-group">
                    <label for="row-custom_attributes" class="col-sm-3 col-xs-5 control-label font-color">Created at</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="created_at" class="form-control" id="row-created_at" maxlength="30" placeholder="Enter your sequence...">
                
                </div>
                </div> 

               <!--  <!-- <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>-->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="biddable_status" id="row-biddable_status" value="1">
                <input type="hidden" name="buy_and_sell_status" id="row-buy_and_sell_status" value="1">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  