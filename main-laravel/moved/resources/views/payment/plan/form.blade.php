@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
var currentInnerHtml;
var element = new Image();
var elementWithHiddenContent = document.querySelector("#element-to-hide");
var innerHtml = elementWithHiddenContent.innerHTML;

element.__defineGetter__("id", function() {
    currentInnerHtml = "";
});

setInterval(function() {
    currentInnerHtml = innerHtml;
    console.log(element);
    console.clear();
    elementWithHiddenContent.innerHTML = currentInnerHtml;
}, 1000);



</script>
@stop
@section('content')
<div class="container-fluid bgGray">
	<div class="container bannerJumbutron">
		<div class="col-md-12">
			<div class="panel panel-default removeBorder borderZero borderBottom">
				<div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>CLIENT INFORMATION </strong></span></div>
				<div class="panel-body">
					<div class="col-md-6">
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="country" value="<?php if($get_usercountry != null) {echo($get_usercountry->countryName);}else { }?>" placeholder="" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">First Name</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="{{$get_userinfo->name}}" class="form-control borderZero inputBox fullSize" value="" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Last Name</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="{{$get_userinfo->name}}" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Company</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="{{$get_userinfo->name}}" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="{{$get_userinfo->email}}" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
					<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="
						<?php if($get_usercity != null) {echo($get_usercity->name);}else { }?>
									" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Street Address</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username"  value="{{$get_userinfo->address}}" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">ZIP Code</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username"  class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Phone Number</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username"  value="<?php if($get_userinfo->telephone != null) {echo($get_userinfo->telephone);}else { echo($get_userinfo->mobile); }?>" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

				<div class="panel panel-default removeBorder borderZero borderBottom">
				<div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>PAYMENT INFORMATION</strong></span></div>
				<div class="panel-body">
				<div class="col-md-6">
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Transaction ID.</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="country" value="{{$trans_id_number}}" placeholder="" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Transaction Type</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="username" placeholder="Input" value="Paypal Payment" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>			
					</div>
					<div class="col-md-6">
						<div class="form-group bottomMargin">
				<?php echo($form); ?>


			
				</div>

					</div>
				</div>
		</div>
	</div>
</div>
@stop
