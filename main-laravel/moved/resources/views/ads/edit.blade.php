@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
          location.reload();  
        loading.addClass('hide');

    } 
   $("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $("#btn-cancel").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $("#btn-change_photo").addClass("hide");
    });
    $("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide"); 
        $(".uploader-pane2").removeClass("hide");
        $(".uploader-pane3").removeClass("hide");
        $(".uploader-pane4").removeClass("hide");
         $("#btn-cancel").addClass("hide");
        $("#btn-change_photo").removeClass("hide");
    });

 $("#ads-type").on("change", function() { 
        var id = $("#ads-type").val();
      var id = $("#ads-type").val();
        if (id == 2) {
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#auction-type-pane").addClass("hide");
        }
  });  


   $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $("#profile-photo").addClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });

        @foreach($ads_photos as $row)
         $(".remove-image_{{$row->id}}").on("click", function() { 
        $("#photo_{{$row->id}}").addClass("hide");
        console.log(photo_{{$row->id}});
        $("#delete-photo_{{$row->id}}").removeAttr('disabled','disabled');
        $(".uploader-pane_{{$row->id}}").removeClass("hide");
          });
        @endforeach

         $("#ads-category_id").on("change", function() { 
        var id = $("#ads-category_id").val();
        $("#custom-attribute-container").removeClass("hide");
        $("#ads-subcategory").removeClass("hide");
        $("#ads-sub_category_id").addClass("hide");
        $("#custom-attr-container").remove();
        $_token = "{{ csrf_token() }}";
        var city = $("#custom-attributes-pane");
        city.html(" ");
        var subcategory = $("#ads-subcategory");
        if ($("#ads-category_id").val()) {
          $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
            city.html(response);
          }, 'json');
          subcategory.html("");
          $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
            subcategory.html(response);
          }, 'json');

       }    
  });


  $("#ads-price").change(function() {                  
        $('#ads-bid_limit_amount').val(this.value);                  
    });

 // $("#ads-category").on("change", function() { 
 //        var id = $("#ads-category").val();
 //        $("#custom-attribute-container").removeClass("hide");
 //        $_token = "{{ csrf_token() }}";
 //        var customAtt = $("#custom-attributes-pane");
 //        customAtt.html(" ");
 //        var subcategory = $("#ads-subcategory");
 //        if ($("#ads-category").val()) {
 //          $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
 //            customAtt.html(response);
 //          }, 'json');
 //          subcategory.html("");
 //          $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
 //            subcategory.html(response);
 //          }, 'json');

 //       }    
 //  });

</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 noPadding">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Create Ad</span>
          </div>
          <div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
           
           {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_ads', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
            <div id="ads-notice"></div>

            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-type" name="ad_type">
                    @foreach ($get_ad_types as $get_ad_type) 
                  <option value="{{ $get_ad_type->id}}" {{ ($ads->ads_type_id == $get_ad_type->id ? 'selected' : 'none') }}>{{ $get_ad_type->name }}</option>                 
                @endforeach
                </select> 
              </div>
            </div>

                           
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title</h5>
              <div class="col-sm-9">
                <input type="text" name="title" class="borderZero form-control borderzero" id="row-title" maxlength="30" value="{{ $ads->title }}">

              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="ads-price" name="price" placeholder="" value="{{ $ads->price }}">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
               <select type="text" class="form-control borderZero" id="ads-category_id"  name="category">
                  <option class="hide">Select Category</option>
                  @foreach($category as $row)
                    @if($row->buy_and_sell_status == 3)
                      <option value="{{$row->id}}" {{($ads->category_id== $row->id?'selected':'')}}>{{$row->name}}</option>
                    @endif
                  @endforeach
               </select>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category</h5>
              <div class="col-sm-9">
               <select type="text" class="form-control borderZero" id="ads-sub_category_id"  name="subcategory">
                @if($ads->sub_category_id == null)
                      <option value="" class="hide">Select:</option>
                @endif 
                @foreach($sub_categories as $row)
                      @if($row->buy_and_sell_status != 3)
                        <option value="{{$row->id}}" {{($ads->sub_category_id== $row->id?'selected':'')}}>{{$row->name}}</option>
                      @endif
                @endforeach
               </select>

               <select type="text" class="form-control borderZero hide" id="ads-subcategory"  name="subcategory">
               </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description</h5>
              <div class="col-sm-9">
                <textarea  class="form-control borderZero" id="ads-description" rows="10"  name="description">{{ $ads->description }}</textarea>
              </div>
            </div>
   <?php      
   $i = 0;   


  foreach($ads_photos as $row) {     
  echo ' <div class="form-group nobottomMargin">
      <h5 class="col-sm-3 normalText" for="register4-email">'.($row->primary == 1 ? 'Ad Photos (Max 4)':'').'</h5>
              <div class="col-sm-9">
                  <div class="panel panel-default nobottomMargin removeBorder borderZero borderBottom" id="panel-profile">
              <div class="panel-body nobottomPadding noleftPadding normalText"> 
                 <div id="load-form" class="loading-pane hide">
                           <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                   </div>';
                  if($row->photo != null){
                 echo '<img src ="'.url('uploads/ads').'/'.$row->photo.'" class="img-responsive" id="photo_'.$row->id.'" width="100%">
                <input type="hidden" name="delete_photo'.$row->id.'" value="'.$row->id.'" id="delete-photo_'.$row->id.'" disabled>';
                  } 
                  echo'  <div class="minPadding noleftPadding norightPadding">
                      <div class="col-sm-10 noleftPadding">
                        <div class="uploader-pane_'.$row->id.' '.($row->photo != null ? 'hide':'').'">
                                <div class="change">
                                  <input name="photo'.$row->id.'" id="photo_upload_'.$row->id.'" type="file" title="aw" class="file borderZero file_photo" data-show-upload="false" placeholder="remove"  accept="image/*">
                                </div>
                            </div>
                            <!-- <button class="btn redButton borderZero" type="button">Browse</button> -->
                    </div>
              <div class="col-sm-2 noPadding">
                     <input type="hidden" name="id" value="{{$row->id}}">
                <button class="btn blueButton borderZero noMargin fullSize remove-image_'.$row->id.' pull-right" type="button">x</button>
              </div>
              </div>
             </div>
           </div>
        </div>
     </div>';
  echo ' <input type="hidden" name="rem_img_upload" value="'.$i.'">';   
}

  ?>       
   


                 <div class="{{ ($ads->ads_type_id == 1 ? 'hide':'') }}" id="auction-type-pane">
                    <h5 class="inputTitle borderbottomLight">Auction Information </h5>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="ads-bid_limit_amount" name="bid_limit_amount" value="{{$ads->bid_limit_amount}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="ads-bid_start_amount" name="bid_start_amount" value="{{$ads->bid_start_amount}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="ads-minimum_allowed_bid" name="minimum_allowed_bid" placeholder="" value="{{$ads->minimum_allowed_bid}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                      <div class="col-sm-9">
                        <div class="col-sm-6 noleftPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8">
                              <input type="number" class="form-control borderZero" id="ads-bid_duration_start" name="bid_duration_start" placeholder="" value="{{$ads->bid_duration_start}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 norightPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8"> 
                              <select id="ads-bid_duration_dropdown_id" name="bid_duration_dropdown_id" class="form-control borderZero">
                                <option value="" class="hide">Select:</option>
                           @foreach ($bid_duration_dropwdown as $value)
                              <option value="{{$value->id}}" {{($ads->bid_duration_dropdown_id==$value->id?'selected':'')}}>{{$value->value}}</option>
                           @endforeach
                              </select>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <div class="{{ ($custom_attributes == "" ? 'hide':'') }}" id="custom-attr-container">
               <h5 class="inputTitle borderbottomLight bottomPadding">Custom Attribute</h5>
                   <?php echo $custom_attributes;?>
            </div>    
                
              <h5 class="inputTitle borderbottomLight bottomPadding hide" id="custom-attribute-container">Custom Attribute</h5>
                  <div id="custom-attributes-pane">
              </div>
                <h5 class="inputTitle borderbottomLight bottomPadding">Additional Information </h5>
                        <div class="form-group">
                  <h5 class="normalText col-sm-3" for="email">Country</h5>
                  <div class="col-sm-9">
                    <select class="form-control borderZero" id="client-country" name="country">
                      <option value="" class="hide">Select:</option>
                      @foreach($countries as $row)
                         <option value="{{ $row->id}}" {{ ($ads->country_id == $row->id ? 'selected' : 'none') }}>{{ $row->countryName }}</option>                 
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <h5 class="normalText col-sm-3" for="email">City or State</h5>
                  <div class="col-sm-9">
                    <select class="form-control borderZero" id="client-city" name="city">
                      <option value="" class="hide">Select:</option>
                        @foreach($city as $row)
                             <option value="{{ $row->id}}" {{ ($ads->city_id == $row->id ? 'selected' : 'none') }}>{{ $row->name }}</option>                 
                        @endforeach
                    </select>
                  </div>
                </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Address</h5>
                        <div class="col-sm-9">
                              <input class="form-control borderZero" type="text" id="#"  name="address" value=''>
                        </div>
                      </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Youtube</h5>
                        <div class="col-sm-9">
                            <input type="embed" name="youtube" pattern="<iframe.+" class="form-control borderZero" value='<?php echo($ads->youtube) ?>'>
                        </div>
                      </div>

            <div class="bordertopLight topPaddingB">
              <input type="hidden" name="id" value='<?php echo($ads->id) ?>'>
              <input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Update">
            </div>
           
          {!! Form::close() !!}
          </div>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span class="pull-right">10</span></div>
            <hr>
            <div class=""><i class="fa fa-th"></i> Auction Ads Available <span class="pull-right">1</span></div>
          </div>
        </div>
          
        <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
       </div>
      </div>
        <div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        <div class="panel-body adspanelPadding">
        <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-5">
            <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
          </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-7">
            <div class="mediumText grayText">Featured Ads 2</div>
            <div class="normalText lightgrayText">by Dell Distributor</div>
            <div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
            <div class="mediumText blueText">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-empty"></i>
              <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
            </div>
          </div>
        </div>
        </div>
       </div>
          
        </div>


      </div>
    </div>
  </div>
</div>

@stop
