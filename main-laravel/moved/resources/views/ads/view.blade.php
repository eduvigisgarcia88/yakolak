@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
$_token = "{{ csrf_token() }}";

function refresh(){
	 var id = "{{ $ads_id }}";
	 var loading = $(".loading-pane");
	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
	 $.post("{{ url('ads/fetch-info/'.$ads_id) }}", { id: id, _token: $_token }, function(response) {
	 	$.each(response.rows, function(index, value) {
	 		//console.log(index+"="+value);
	 		var field = $("#rows-" + index);

 			if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	console.log(value);
             	 $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }       
          }); 
      }, 'json');
	 loading.addClass("hide");
	}
	$('#thumbCarousel').carousel({
	pause: true,
	interval: false
	});
	$('#adsCarousel').carousel({
	pause: true,
	interval: false
	});


 function refreshBidders(){
        var ads_id = $("#ads_id").val();
        var bidder = $("#refresh-bidders");
			$("#countdown").load("{{ url('/') }}"+" #countdown>*","");
        if ($("#ads_id").val()) {
        	$.post("{{ url('ad/bidders/refresh') }}", { ads_id:ads_id, _token: $_token }, function(response) {
            bidder.html(response);
          }, 'json'); 

        };
		$("#bidders").addClass('hide','hide');  
		 refreshBidderList();   
 	}


$("#content-wrapper").on("click", ".btn-watchlist", function() {
	var id = $(this).data('id');
	console.log(id);
	var title = $(this).data('title');
		$("#row-id").val("");
		$("#row-id").val(id);
		$("#row-title").val(title);
		$("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
		$("#modal-form").find('form').trigger("reset");
		$(".modal-title").html('<strong>Watchlists '+ title +' </strong>', title);
		$("#modal-watchlist").modal('show');    
});

$(function () {
	$("#rateYo").rateYo({
	 starWidth: "15px",
	 fullStar: true,
	 ratedFill: "#5da4ec",
	});

	$("#averageRate").rateYo({
	 starWidth: "15px",
	 rating: {{$get_average_rate}},
	 ratedFill: "#5da4ec",
	 readOnly: true,
	});
	@foreach($featured_ads as $row)
		$("#featured_ads_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
		});
	@endforeach
	@foreach($vendor_ads as $row)
	    $("#vendor_ads_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
	});
	@endforeach
	@foreach($related_ads as $row)
	    $("#related_ads_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
	});
	@endforeach
	@foreach($related_ads_mobile as $row)
	    $("#related_ads_mobile_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
	});
	@endforeach
	@foreach($related_adset2 as $row)
	    $("#related_adset2_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
	});
	@endforeach

	var starWidth = $("#rateYo").rateYo("option", "starWidth"); //returns 40px
		$("#rateYo").rateYo("option", "starWidth", "15px"); //returns a jQuery Element
		$("#rateYo").rateYo()
		.on("rateyo.set", function (e, data) {
		var rating = data.rating;
		if(rating == 0){
		$(".btn-rate_product").attr('disabled','disabled');
		}else{
		$(".btn-rate_product").removeAttr('disabled','disabled');
		}
	});
});

$(document).ready(function(){
	
	setTimeout(function(){
		$("#modal-pop_up_message").modal("show");
		}, 5000);
 	// setTimeout(function(){
  //         $('#modal-pop_up_message').modal('hide')
  //       }, {{$modal_related_popups_duration}}000);

	$(".btn-rate_product").attr('disabled','disabled');
	$(".btn-save_comment").attr('disabled','disabled');
	$(".submitReply ").attr('disabled','disabled');

    // $('').scrollspy({target: ".rateButton ", offset: 50});

  //   $("#btn-top_rate_product").on('click', function(event) {

  // 		$("html, body").animate({ scrollTop: $(".btn-rate_product").offset().top}, 1500);
 	// });   
});

$("#review-product-pane").on("click", ".btn-rate_product", function() {	
	var $rateYo = $("#rateYo").rateYo();
	var rate = $rateYo.rateYo("rating");
	var id = {{ $ads_id }};
	//var ad_id = {{ $ads_id }};
	$(".modal-header").removeAttr("class").addClass("modal-header modal-info");
	dialog('Rate Ad', 'Are you sure you want to rate this ad</strong>?', "{{ url('ad/rate') }}",id,rate);
});

$("#comment-pane").on("keyup", "#user-ad-comment", function() {	
	var comment = $("#user-ad-comment").val();
		if(comment == ""){
		$(".btn-save_comment").attr('disabled','disabled');
		}else{
		$(".btn-save_comment").removeAttr('disabled','disabled');
		}	
});

$(".comments-container").on("keyup", "#reply-message", function() {  
	var reply_message = $(this).parent().parent().parent().parent().parent().find('#reply-message').val();
		if(reply_message == ""){
		$(this).parent().parent().parent().parent().parent().find('.submitReply').attr('disabled','disabled');
		}else{
		$(this).parent().parent().parent().parent().parent().find('.submitReply').removeAttr('disabled','disabled');
		} 
});

$(".comments-container").on("keyup", "#main_comment_reply", function() {  
	var main_reply = $(this).parent().parent().parent().find('#main_comment_reply').val();
		if(main_reply == ""){
		$(this).parent().parent().parent().parent().find('.parentSubmit').attr('disabled','disabled');
		}else{
		$(this).parent().parent().parent().parent().find('.parentSubmit').removeAttr('disabled','disabled');
		} 
});

$(".comments-container").on("click", "#show", function() {
$(this).parent().parent().parent().find('.inputReply').toggleClass("hidden");
var get_name  = $(this).data('name');
	if(get_name != undefined){
var parse_name = get_name+' :';
	$(this).parent().parent().parent().parent().find('#main_comment_reply').val(parse_name);
	}else{
	parse_name = "";
	}
});

$(".comments-container").on("click", "#show_reply_box", function() {
$(this).parent().parent().parent().parent().parent().find('.inputReplyBox').toggleClass("hidden");
var get_name  = $(this).data('name');
	if(get_name != undefined){
	var parse_name = get_name+' :';
	$(this).parent().parent().parent().parent().parent().find('#reply-message').val(parse_name);
	}else{
	parse_name = "";
	}
});


     $("#content-wrapper").on("click", ".btn-remove-watchlist", function() {
    	  var watchitem_id = $(this).data('watchitem_id');
    	  var ad_id = $(this).data('ad_id');
    	  var user_id = $(this).data('user_id');
    	  var title = $(this).data('title');
    	  $("#row-watchitem_id").val(watchitem_id);
    	  $("#row-title").val(title);
		  $("#row-ad_id").val(ad_id);
		  $("#row-user_id").val(user_id);
		
		  $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
          $("#modal-form").find('form').trigger("reset");
          $(".modal-title").html('<strong>Remove Watchlist '+ title +' </strong>', title);
          $("#modal-watchlist-remove").modal('show');    
    });

    // $('#user-ad-comment').on("change", function(e){
    //      var regex = /(<([^>]+)>)/ig;
    //      var message = $('#user-ad-comment').val();
  		//  var validated = message.replace(regex, "");
  		//  if(validated == ""){
  		//  	 $('#user-ad-comment').val("");
  		//  	 $(".btn-save_comment").attr('disabled','disabled');	
  		//  }else{
  		//  	 $('#user-ad-comment').val(validated);
  		//  }
    // });

    $(function(){
 
		$('#user-ad-comment').keyup(function()
		{
			var yourInput = $(this).val();
			re = /[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
			var isSplChar = re.test(yourInput);
			if(isSplChar)
			{
				var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
				$(this).val(no_spl_char);
			}
		});
 
	});
	  $(function(){
        $("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
              $('#qnimate').removeClass('popup-box-on');
            });
  })


  $("#content-wrapper").on("click", ".btn-add-biding", function() {
     var ads_id = $(this).data('id');
     console.log(ads_id);
     var title = $(this).data('title');
     $("#row-ads_id").val(ads_id);
     $("#row-title").val(title);
	 $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
     $("#modal-form").find('form').trigger("reset");
     $(".modal-title-bidder").html('<strong>Bid on '+ title +' </strong>', title);
     $("#modal-bidding").modal('show'); 
     $("#modal-list-bidders").find('form').trigger("reset");

    });


 $('[data-countdown]').each(function() {
	  var $this = $(this), finalDate = $(this).data('countdown');
	   $this.countdown(finalDate, function(event) {
	   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
	   });
 });


$(function() {
       $.post("{{ url('auction/expired') }}", { _token: $_token}, function(response) {
        ads_id.append(response);   
    }, 'json');
});

$(function() {
       $.post("{{ url('basic-ad/expired') }}", { _token: $_token}, function(response) {
         basic_ads_id.append(response);   
    }, 'json');
});
$(function() {
       $.post("{{ url('auction/expiring') }}", { _token: $_token}, function(response) {
         basic_ads_id.append(response);   
    }, 'json');
});


 jQuery(document).ready(function() {
     $("time.timeago").timeago();
   });



   function refreshBidderList(){
        var bidderlist = $("#refresh-bidders-list");
        var ads_id = $("#ads_id").val();
        bidderlist.html("");
          $.post("{{ url('ad/bidders-list/refresh') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            bidderlist.html(response);
          }, 'json');   
          location.reload("{{ url('ads/view/141') }}") ;
  	}
     $(function () {
        var bidderlistRefresh = $("#refresh-bidders-list");
        var ads_id = $("#ads_id").val();
        bidderlistRefresh.html("");
          $.post("{{ url('ad/bidders-list/refresh') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            bidderlistRefresh.html(response);
          }, 'json');
      });
        
</script>
@stop
@section('content')
<div class="container-fluid bgWhite noPadding" id="ad-view-container">
	<div class="container bannerJumbutron">
		<div class="col-md-12 col-sm-12 col-xs-12 blockTop noPadding">
			<div class="col-md-9 col-sm-9 col-xs-12">
	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
		<div class="panel panel-default bottomMarginLight removeBorder borderZero">
			<div id="adsCarousel" class="carousel slide carousel-fade" data-ride="carousel">

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<div class="" style="background: url({{ url('uploads/ads').'/'.$primary_photos->photo}}); background-position: center center; background-repeat: no-repeat; background-size: contain; height: 390px; width: 100%;">
						</div>
					</div>
  					@foreach ($ads_photos as $rows)
					<div class="item">
						<div class="" style="background: url({{ url('uploads/ads').'/'.$rows->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: 100%;">
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
<div class="panel-body bgGray borderBottom blockTop">
<div class="col-md-12 col-sm-12 col-xs-12">
	<ul class="horizontal-slide">
	<!--            <li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100" height="80px" width="auto"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li>
	<li class="col-md-3"><img class="thumbnail" src="http://placehold.it/100x100"/></li> -->
	  <?php $i=0; ?>
	@foreach ($thumbnails as $rows)
	<li class="col-md-2 col-sm-4 col-xs-4 minPadding"><div data-target="#adsCarousel" data-slide-to="{{ $i++ }}" class="" style="height: 80px; width: auto; background: url({{ url('uploads/ads/thumbnail').'/'.$rows->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
	</div></li>  
	@endforeach
	</ul>
	<a class="left carousel-control" href="#adsCarousel" role="button" data-slide="prev" style="width: 5px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:20px; left:-10px;">
		<span class="fa fa-angle-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#adsCarousel" role="button" data-slide="next" style="width: 5px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:20px; right:0px;">
		<span class="fa fa-angle-right" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
</div>
</div> 
</div>
</div>
</div>
			<div class="col-md-3 col-sm-3 col-xs-12 noPadding" id="content-wrapper">
				<div class="col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
					<div class="panel panel-default bottomMarginLight removeBorder borderZero">
					  <div class="panel-body noPadding">
					  	<div class="blueText xlargeText"><strong>{{ $getads_info->title }}</strong></div>
					  	<div class="xlargeText grayText"><strong>{{ number_format($getads_info->price) }} USD</strong></div>
					  	<div class="mediumText lightgrayText">Ad Rating
					  	@if($get_average_rate != 0)
					  	<span id="averageRate" class="mediumText adRating pull-right"></span>
					  	@else
					  	<span class="mediumText lightgrayText pull-right">N/A</span>
					  	@endif
					  	</div>
					  	@if(Auth::check())
					  	@if(!(Auth::user()->id == $getads_info->user_id))
				 		 <div class="row topPaddingB">
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><button class="btn fullSize normalText rateButton borderZero" id="btn-top_rate_product"><i class="fa fa-star rightMargin"></i>Rate Product</button></div>
					  			@if($watch_item)
					  				<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><a data-watchitem_id="{{ $watch_item->id }}" data-ad_id="{{ $getads_info->ads_id }}" data-user_id="{{ $getads_info->user_id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-remove-watchlist"><button class="btn fullSize normalText watchButton borderZero"><i class="fa fa-eye rightMargin"></i> Remove Watch Item</button></a></div>
					  			@else
					  				<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><a data-id="{{ $getads_info->id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-watchlist"><button class="btn fullSize normalText watchButton borderZero"><i class="fa fa-eye rightMargin"></i> Watch Item</button></a></div>
					  			@endif
					  		</div>
					  		<div class="col-md-12 noPadding"><button class="btn fullSize normalText blueButton borderZero"><i class="fa fa-comments"></i> Leave Comment</button></div>	  	  					
					    @endif
					  	@else
					   <div class="row topPaddingB">
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><button class="btn fullSize normalText rateButton borderZero"><i class="fa fa-star rightMargin"></i>Rate Product</button></div>
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding "><a href="#" class="btn-login" data-toggle="modal" data-target="#modal-login"><button class="btn fullSize normalText watchButton borderZero" data-title="{{ $getads_info->title }}" href="#" data-target="#modal-login"><i class="fa fa-eye rightMargin"></i> Watch Item</button></a></div>
					  		
					  		</div>
					  		<div class="col-md-12 noPadding"><button class="btn fullSize normalText blueButton borderZero"><i class="fa fa-comments rightMargin"></i> Leave Comment</button></div>	  	  					
					   	@endif
					  	<div class="col-md-12 blockTop noPadding">
					  	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body topPaddingB bgGray">
							  	<div class="col-xs-12 bottomPadding noPadding">
							  		<div class="profileBlock">
							  			<a class="panelRedTitle" href="{{ url('/user/vendor') . '/' . $getads_info->user_id . '/' .$getads_info->name  }}">
				                      <div class="profileImage">
				                        <img src="{{ url('uploads').'/'.$getads_info->user_photo}}" class="avatar" alt="user profile image" style="width: 85px; height: 85px;">
				                      </div>
				                 		 </a>
				                      <div class="profileText">
							  			<a class="panelRedTitle" href="{{ url('/user/vendor') . '/' . $getads_info->user_id }}">
				                        <div class="mediumText grayText"><strong>{{ $getads_info->name }}</strong></div></a>
				                    @if ($getads_info->ads_type_id != 2)
							  			<div class="normalText grayText"><i class="fa fa-envelope rightMargin"></i> {{ $getads_info->email }}</div>
					  	         		@if ($getads_info->telephone)
							  			    <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> {{ $getads_info->telephone }}</div>
							  			@else
							  			    <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> {{ $getads_info->mobile }}</div>
							  			@endif
							  		@else 
										@if($get_winner_bidder)
					 						@if($get_winner_bidder->user_id == Auth::user()->id)
												<div class="normalText grayText"><i class="fa fa-envelope rightMargin"></i> {{ $getads_info->email }}</div>
							  	         		@if ($getads_info->telephone)
								  			    <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> {{ $getads_info->telephone }}</div>
								  				@else 
								  			    <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> {{ $getads_info->mobile }}</div>
								  				@endif
											@else

											@endif
										@else
										@endif	
							  		@endif
					  	         	    @if ($getads_info->city || $getads_info->countryName)
							  			  <div class="normalText grayText"><i class="fa fa-map-marker rightMargin"></i> {{ $getads_info->city }}
							  			@if ($getads_info->city) 
							  			   , 
							  			@endif
							  				{{ $getads_info->countryName }}</div>
							  			@endif
				                      </div>
				                    </div>
							  	</div>
							  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding normalText textJustify">
							  		<article>
							  		<?php echo($rows->description); ?>
							  		Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.
							  		Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.
							  		Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.
							  		</article>
							  		</div>
							  </div>
							</div>
				        @if ($getads_info->ads_type_id != 2)
			       	     	@if ($getads_info->website || $getads_info->facebook_account || $getads_info->twitter_account)			
							<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body bgGray">
					  			@if ($getads_info->website)
					  			<div class="normalText grayText"><i class="fa fa-globe rightMargin"></i>{{ $getads_info->website}} </div>
					  			@endif
					  			@if ($getads_info->facebook_account)
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-facebook-square rightMargin"></i></span> {{ $getads_info->facebook_account }}</div>
					  			@endif
					  			@if ($getads_info->twitter_account)
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-twitter rightMargin"></i></span> {{ $getads_info->twitter_account }}</div>
							  	@endif
							  </div>
							</div>	
							@endif
						@else
							@if($get_winner_bidder)
						 		@if($get_winner_bidder->user_id == Auth::user()->id)
									@if ($getads_info->website || $getads_info->facebook_account || $getads_info->twitter_account)			
										<div class="panel panel-default bottomMarginLight removeBorder borderZero">
										  <div class="panel-body bgGray">
								  			@if ($getads_info->website)
								  			<div class="normalText grayText"><i class="fa fa-globe rightMargin"></i>{{ $getads_info->website}} </div>
								  			@endif
								  			@if ($getads_info->facebook_account)
								  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-facebook-square rightMargin"></i></span> {{ $getads_info->facebook_account }}</div>
								  			@endif
								  			@if ($getads_info->twitter_account)
								  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-twitter rightMargin"></i></span> {{ $getads_info->twitter_account }}</div>
										  	@endif
										  </div>
										</div>
									@endif
								@endif
							@endif	
						@endif
						</div>
					  	@if(Auth::check())
						  	@if(!(Auth::user()->id == $getads_info->user_id))
								<div class="col-md-12 noPadding topPaddingB"><button data-target = "#modal-send-msg" data-toggle="modal" class="btn fullSize mediumText redButton borderZero"><i class="fa fa-paper-plane"></i> Message {{$getads_info->name }}</button></div>				  
					 	 	 @else
								<div class="col-md-12 noPadding topPaddingB">
								<p class="alert alert-danger borderZero">It's your own listing, you can't contact the publisher.</p>
							</div>
						    @endif
					  	@else
							<div class="col-md-12 noPadding topPaddingB"><button  data-target = "#modal-login" data-toggle="modal" class="btn fullSize mediumText redButton borderZero"><i class="fa fa-paper-plane"></i> Message {{$getads_info->name }}</button></div>				  
				 	 	@endif


					  		
					  	@if($getads_info->ads_type_id == 2)
  		     			<div class="col-md-12 blockTop noPadding">
								<div class="panel panel-default bottomMarginLight removeBorder borderZero">
								  <div class="panel-body lineHeightC bgGray"> 	
				                 <div id="load-form_bidder" class="loading-pane hide">
				                      <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
				                  </div>
								  		<!--ad timer -->
              						   <div class="col-lg-12 col-md-12 col-xs-12 noPadding">

							         	<span class="norightPadding normalText redText" data-countdown="{{$getads_info->ad_expiration}}"></span> <span class="normalText redText">Left</span>
							          <span class="leftPadding normalText pull-right leftMargin">Bid cost: {{$getads_info->minimum_allowed_bid}}</span>
							      </div>

							          <div id="load-form_bidder" class="loading-pane hide">
							        		<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
									  </div>
									  <div id="refresh-bidders">
										</div>
									  <div id="bidders">
						  			<?php  $i = 1; ?>
								   	@foreach ($get_bidders as $bidders)
								   		@if($i == 1)
              						   <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   			<div class="mediumText grayText">
								   				<strong>
								   					<span class="col-lg-1 noPadding"> <div style="display:none;">{{$i++}}</div><img src="{{ url('/').'/uploads/'.$bidders->photo}}"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="{{ url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name }}">{{$bidders->name}}</a> </span><span class="noPadding pull-right">{{number_format($bidders->points)}} pts.</span>
								   				</strong>
								   			</div>
								   		</div>
								   			@if($getads_info->ad_expiration <= $current_time)
									  			@if($getads_info->ads_type_id == '2' && $getads_info->status == '3')
              						  				 <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
									  					<div class="blueText mediumText noPadding noMargin" style="text-align: center;">Bid Winner</div>
									  				</div>	
									  			@endif
									  		@endif
								   		@else
									  		@if($getads_info->status != '3')
              						   			<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   					<div class="normalText {{($getads_info->status == 3?'lightgrayText':'grayText')}}">
								   						<span class="col-lg-1 noPadding"><div style="display:none;">{{$i++}}</div> <img src="{{ url('/').'/uploads/'.$bidders->photo}}"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText {{($getads_info->status == 3?'lightgrayText':'grayText')}}" href="{{ url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name }}">{{$bidders->name}}</a> </span><span class="noPadding pull-right">{{number_format($bidders->points)}} pts.</span>
								   					</div>
								   				</div>
								   			@endif
								   		@endif

									@endforeach	
									@if(count($get_all_bidders) > 3)
								   		    <a href="#" class="news-expand pull-left normalText" data-toggle="modal" data-target="#modal-list-bidders" aria-controls="rmjs-1">View More</a>
								   	@elseif (count($get_all_bidders) == 0)
              						   			<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   						<center>No Bidders yet</center>
								   				</div>
								   	@else 

								   	@endif	    
									</div>
								  </div>
								</div>
										<input type="hidden" name="ads_id" id="ads_id" value="{{$getads_info->id}}">
									@if (Auth::check())
										@if (Auth::user()->id != $getads_info->user_id) 
										<button class="btn fullSize mediumText blueButton borderZero btn-add-biding" data-id="{{$getads_info->id}}" data-title="{{$getads_info->title}}"><i class="fa fa-money"></i> Bid</button>
						 	 			@endif
						 	 		@else
										<button class="btn fullSize mediumText blueButton borderZero" class="btn-login" data-toggle="modal" data-target="#modal-login"><i class="fa fa-money"></i> Bid</button>	
						 	 		@endif
						 	 	</div>
					  	
					  	@else
						 
					  	@endif

					  </div>
				</div>
				</div>
			</div>
 </div>
 </div>
 <div class="container-fluid bgGray">
	<div class="container blockMargin noPadding">
		<div class="col-md-12 noPadding">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<span class="panelTitle">Description</span>
				  	<p> {{ $getads_info->description }}</p>
				  </div>
				</div>
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				 <div class="panelTitle bottomPaddingB">Custom Attributes</div>
				  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  	<div class="row">
				  	<form class="form-horizontal" role="form">
				  	<div class="col-lg-6 col-sm-6 col-xs-12">
							<?php echo $custom_attributes;?>
					</div>
				  	<div class="col-md-6 col-sm-6 col-xs-12">
				  	</div>
				  	</form>
				  	</div>
				  </div>
				</div>
				</div>
				@if($getads_info->youtube)
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<span class="panelTitle">Video</span>
				  	<p><?php echo($getads_info->youtube) ?></p>
				  </div>
				</div>
				@endif
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<div class="panelTitle bottomPaddingB">Contact Information</div>
				@if($getads_info->ads_type_id != 2)  	
				  	@if($getads_info->address)
				  	<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText"> {{ $getads_info->address }}</span></div>
				  	@endif
				  	@if($getads_info->telephone)
				  	<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText"> {{ $getads_info->telephone }}</span></div>
				  	@endif
				  	@if($getads_info->email)
				  	<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText"> {{ $getads_info->email }}</span></div>
				  	@endif
				  	@if($getads_info->office_hours)
				  	<div class="mediumText grayText"><strong>Office Hours:</strong><span> {{ $getads_info->office_hours }}</span></div>
				  	@endif

				  	@if ($getads_info->facebook_account || $getads_info->twitter_account)
				  	<div class="panelTitle topPaddingB bottomPaddingB">Social Media</div>
				  	@endif
				  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  		@if($getads_info->facebook_account)
				  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-facebook-square"></i> {{ $getads_info->facebook_account }}</span></div>
				  		@endif
				  		@if($getads_info->twitter_account)
				  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-twitter"></i> {{ $getads_info->twitter_account }}</span></div>
						@endif
				  </div>
				@else
					 @if($get_winner_bidder)
					 	@if($get_winner_bidder->user_id == Auth::user()->id)
							@if($getads_info->address)
						  	<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText"> {{ $getads_info->address }}</span></div>
						  	@endif
						  	@if($getads_info->telephone)
						  	<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText"> {{ $getads_info->telephone }}</span></div>
						  	@endif
						  	@if($getads_info->email)
						  	<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText"> {{ $getads_info->email }}</span></div>
						  	@endif
						  	@if($getads_info->office_hours)
						  	<div class="mediumText grayText"><strong>Office Hours:</strong><span> {{ $getads_info->office_hours }}</span></div>
						  	@endif

						  	@if ($getads_info->facebook_account || $getads_info->twitter_account)
						  	<div class="panelTitle topPaddingB bottomPaddingB">Social Media</div>
						  	@endif
						  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
						  		@if($getads_info->facebook_account)
						  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-facebook-square"></i> {{ $getads_info->facebook_account }}</span></div>
						  		@endif
						  		@if($getads_info->twitter_account)
						  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-twitter"></i> {{ $getads_info->twitter_account }}</span></div>
								@endif
						  </div>
						@else	
							<p class="alert alert-danger borderZero">Contact information is hidden. It only shows to the Bid Winner.</p>
						@endif
					@else
							<p class="alert alert-danger borderZero">Contact information is hidden. It only shows to the Bid Winner.</p>

					@endif	
				@endif
				</div>
			</div>
			<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">

			  	<iframe width="100%" height="206px"frameborder="0" style="border:0"src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDuGKZMGH7Fo4H8SWyvNJWYelH5brT4bTg&q='{{ $getads_info->city }}'+'{{ $getads_info->countryName }}'" allowfullscreen></iframe>
				</div>
			</div>
			<div class="panel panel-default bottomMarginLight removeBorder nobottomPadding borderZero">
				  <div class="panel-body">

			  	<ul class="list-inline nobottomMargin">
				<li><a class="btn btn-block btn-social btn-facebook borderZero"><i class="fa fa-facebook"></i> Facebook</a></li>
				<li><a class="btn btn-block btn-social btn-google-plus borderZero"><i class="fa fa-google-plus"></i> Google</a></li>
				<li><a class="btn btn-block btn-social btn-twitter borderZero"><i class="fa fa-twitter"></i> Twitter</a></li>
          		<li><a class="btn btn-block btn-social btn-instagram borderZero"><i class="fa fa-instagram"></i> Instagram</a></li>
				</ul>
				</div>
			</div>
   </div>




   <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
          
      <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">

          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: 100%;">
          </div>
       </div>
      </div>

       <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{ url('/ads/view') . '/' . $featured_ad->id }}"> 
              		<div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div>
          		</a>
	          	<div class="rightPadding nobottomPadding lineHeight">
	          		<div class="mediumText grayText topPadding">{{ $featured_ad->title }}</div>
	                  <div class="normalText bottomPadding">
	                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id .'/'. $featured_ad->name }}">by {{$featured_ad->name}}</a>
	                  </div>
	                  <div class="mediumText bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
	                        <div class="mediumText bottomPadding">
	                         @if($featured_ad->city || $featured_ad->countryName)
	                          <i class="fa fa-map-marker rightMargin"></i>
	                          {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
	                         @else
	                          <i class="fa fa-map-marker rightMargin"></i> not available
	                         @endif
	                         </div>

	                          <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
	                  <div class="minPadding adcaptionMargin">
	                  	
	                  	 @if($featured_ad->average_rate != null)
	                       <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
	                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
	                     @else
	                       <span class="blueText rightMargin">No Rating</span>
	                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
	                     @endif	
	                  </div>
	        	</div>
              </div>
             </div>
             @endforeach

      <div class="panel panel-default removeBorder borderZero">
      </div>
         <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">PRODUCTS BY <a class="panelRedTitle" href="{{ url('/user/vendor') . '/' . $getads_info->user_id .'/'.$getads_info->name }}">{{ $getads_info->name}}</a></span>
        </div>
            @foreach ($vendor_ads as $vendor_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{ url('/ads/view') . '/' . $vendor_ad->id }}"> 
              		<div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$vendor_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div>
          		</a>
	          	<div class="rightPadding nobottomPadding lineHeight">
	          		<div class="mediumText grayText topPadding">{{ $vendor_ad->title }}</div>
                  <div class="normalText bottomPadding">
                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $vendor_ad->user_id .'/'.$vendor_ad->name}}">by {{$vendor_ad->name}}</a>
                  </div>
                  <div class="mediumText bottomPadding blueText"><strong>{{ number_format($vendor_ad->price) }} USD</strong></div>
                        <div class="mediumText bottomPadding">
                         @if($vendor_ad->city || $vendor_ad->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $vendor_ad->city }} @if ($vendor_ad->city),@endif {{ $vendor_ad->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                         </div>
                         <?php $i=0;?>
                               @if(count($vendor_ad->getAdvertisementComments)>0)
                                  @foreach($vendor_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                  <div class="minPadding adcaptionMargin">
                  	 @if($vendor_ad->average_rate != null)
                       <span id="vendor_ads_{{$vendor_ad->id}}" class="blueText pull-left rightMargin"></span>
                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                     @else
                       <span class="blueText rightMargin">No Rating</span>
                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                     @endif	
                  </div>
	        	</div>
<!--               <div class="row">
                <a href="{{ url('/ads/view') . '/' . $vendor_ad->id }}">
                <div class="col-md-6 col-sm-3 col-xs-3 norightPadding">
                    <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$vendor_ad->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
               </div>
                </div>
                </a>
                <div class="col-md-6 col-sm-9 col-xs-9 noleftPadding lineHeight">
                  <div class="mediumText grayText topPadding bottomPadding">{{ $vendor_ad->title }}</div>
                  <div class="normalText bottomPadding">
                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $vendor_ad->user_id }}">by {{$featured_ad->name}}</a>
                  </div>
                  <div class="mediumText blueText"><strong>{{ number_format($vendor_ad->price) }} USD</strong></div>
                        <div class="mediumText bottomPadding">
                         @if($vendor_ad->city || $vendor_ad->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $vendor_ad->city }} @if ($vendor_ad->city),@endif {{ $vendor_ad->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                         </div>
                  <div class="minPadding">
                  	 @if($vendor_ad->average_rate != null)
                       <span id="vendor_ads_{{$vendor_ad->id}}" class="blueText pull-left rightMargin"></span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @else
                       <span class="blueText rightMargin">No Rating</span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @endif	
                  </div>
              </div>
              </div> -->
              </div>
             </div>
             @endforeach

  </div>
  </div>
  </div>
 </div>
 <div class="container-fluid bgWhite borderBottom">
	<div class="container blockMargin">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 noPadding sideBlock">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panelTitle bottomPadding">Comments and reviews</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomPadding {{$comments == null ? '' : '' }}">
			<div class="panel panel-default borderZero removeBorder borderBottom noMargin" id="comment-pane">
		 <div class="panel-body" id="post-comment-pane">
					<?php echo $form;?>
				</div>
		
	   		</div>
   		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topMargin topBorder noPadding comments-container">
   			{!! $comments !!}
	   	</div>
  </div>
</div>
 </div>
</div>
			<div class="container-fluid bgGray">
				<div class="container blockTop">
					<div class="col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
			 		<div class="panel panel-default  removeBorder borderZero">
			      <div class="panel-heading panelTitleBarLight">
			        <span class="panelTitle">Related Ads</span>  <span class="hidden-xs">| Check out other ads with the same criteria.</span>
			      </div>
			      
			      	<div class="visible-xs visible-sm">
			      		<div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding blockBottom">

			        @foreach($related_ads_mobile as $row)
			        <div class="panel panel-default bottomMarginLight removeBorder borderZero">
			         <div class="panel-body noPadding">
			         	<a href="{{ url('/ads/view') . '/' . $row->id }}"> 
	              		<div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
	          			</div>
		          		</a>
			          	<div class="rightPadding nobottomPadding lineHeight">
			          		<div class="mediumText grayText topPadding bottomPadding">{{ $row->title }}</div>
			        		<div class="normalText redText bottomPadding">by {{ $row->name }}</div>
			        		<div class="bottomPadding mediumText blueText"><strong> {{ $row->price }} USD</strong></div>
			            <!-- <div class="mediumText bottomPadding">
                         @if($row->city || $row->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
				         </div>
 -->					  	<div class="mediumText bottomPadding">
				                @if($row->city || $row->countryName)
				                   <i class="fa fa-map-marker rightMargin"></i>
				                    {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
				                     @else
				                    	<i class="fa fa-map-marker rightMargin"></i> not available
				                    @endif
				          </div>
				                         <?php $i=0;?>
				                               @if(count($row->getAdvertisementComments)>0)
				                                  @foreach($row->getAdvertisementComments as $count)
				                                        <?php $i++;?>     
				                                   @endforeach
				                               @endif
				           <div class="minPadding adcaptionMargin">
				                  	 @if($row->average_rate != null)
				                       <span id="related_ads_mobile_{{$row->id}}" class="blueText pull-left rightMargin"></span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @else
				                       <span class="blueText rightMargin">No Rating</span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @endif	
				             </div>
			        	</div>
<!-- 			          <div class="row">
			        	<a href="{{ url('/ads/view') . '/' . $row->id }}">
			        	<div class="col-md-5 col-sm-5 col-xs-5">
			        		<div class="fill" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
			        	   </div>
			        	  </div>
			            </a>
			        	<div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
			        		<div class="mediumText grayText topPadding bottomPadding">{{ $row->title }}</div>
			        		<div class="normalText redText bottomPadding">by {{ $row->name }}</div>
			        		<div class="bottomPadding mediumText blueText"><strong> {{ $row->price }} USD</strong></div>
			        		<div class="mediumText bottomPadding">
                         @if($row->city || $row->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
				          </div>
			        		<div class="mediumText blueText">
			        			<i class="fa fa-star"></i>
			        			<i class="fa fa-star"></i>
			        			<i class="fa fa-star"></i>
			        			<i class="fa fa-star"></i>
			        			<i class="fa fa-star-half-empty"></i>
			        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
			        		</div>
			            </div>
			          </div> -->
				     </div>
				     </div>
				     @endforeach

				    <!--  </div> -->
						</div>
			      	</div>
			      	<div class="hidden-xs hidden-sm">
			      		<div class="panel-body">
			        <div id="relatedAds" class="carousel slide carousel-fade" data-ride="carousel">
			          <!-- Wrapper for slides -->
			          <div class="carousel-inner" role="listbox">
			            <div class="item active">
			            <div class="col-lg-12 col-md-12">
			           @foreach ($related_ads  as $related_ad)
			              <div class="col-lg-3 col-md-3 noPadding lineHeight">
			                <div class="sideMargin borderBottom">
			                  <a href="{{ url('/ads/view') . '/' . $related_ad->id }}">
			                  	<div class="adImage" style="background:  url({{ url('uploads/ads/thumbnail').'/'.$related_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
			                		</div>
			                	</a>
			                  <div class="minPadding nobottomPadding">
			                    <div class="mediumText noPadding topPadding bottomPadding">{{ $related_ad->title }}</div>
			                    <div class="normalText grayText bottomPadding bottomMargin">
			                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $related_ad->user_id .'/'. $related_ad->name}}"> {{ $related_ad->name }}</a>
			                      <span class="pull-right"><strong class="blueText">{{ $related_ad->price }} USD</strong>
			                      </span>
			                    </div>
			                    <!-- <div class="normalText bottomPadding">
			                         @if($related_ad->city || $related_ad->countryName)
			                          <i class="fa fa-map-marker rightMargin"></i>
			                          {{ $related_ad->city }} @if ($related_ad->city),@endif {{ $related_ad->countryName }}
			                         @else
			                          <i class="fa fa-map-marker rightMargin"></i> not available
			                         @endif
			                       <span class="pull-right ">
			                        <span class="blueText rightMargin">
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star-half-empty"></i>
			                        </span>
			                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
			                      </span>
			                    </div> -->
			                      <div class="normalText bottomPadding">
				                         @if($related_ad->city || $related_ad->countryName)
				                          <i class="fa fa-map-marker rightMargin"></i>
				                          {{ $related_ad->city }} @if ($related_ad->city),@endif {{ $related_ad->countryName }}
				                         @else
				                          <i class="fa fa-map-marker rightMargin"></i> not available
				                         @endif
				                         <span class="pull-right bottomPadding">
				                  	 @if($related_ad->average_rate != null)
				                       <span id="related_ads_{{$related_ad->id}}" class="blueText pull-left rightMargin"></span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @else
				                       <span class="blueText rightMargin">No Rating</span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @endif	
				                  </span>
				                  </div>
				                         <?php $i=0;?>
				                               @if(count($related_ad->getAdvertisementComments)>0)
				                                  @foreach($related_ad->getAdvertisementComments as $count)
				                                        <?php $i++;?>     
				                                   @endforeach
				                               @endif
			                  </div>
			                </div>
			              </div>
			              @endforeach
			             </div>
			            </div>

			            @if(count($related_adset2) != "0")
			            <div class="item">
			            <div class="col-lg-12 col-md-12">
			           @foreach ($related_adset2  as $row)
			              <div class="col-lg-3 col-md-3 noPadding ">
			                <div class="sideMargin borderBottom">
			                  <a href="{{ url('/ads/view') . '/' . $row->id }}">
			                  	<div class="adImage" style="background:  url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
			                		</div>
			                	</a>
			                  <div class="minPadding nobottomPadding">
			                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
			                    <div class="normalText grayText bottomPadding bottomMargin">
			                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id .'/'.$row->name }}"> {{ $row->name }}</a>
			                      <span class="pull-right"><strong class="blueText">{{ $row->price }} USD</strong>
			                      </span>
			                    </div>
			                    <!-- <div class="normalText bottomPadding">
			                         @if($row->city || $row->countryName)
			                          <i class="fa fa-map-marker rightMargin"></i>
			                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
			                         @else
			                          <i class="fa fa-map-marker rightMargin"></i> not available
			                         @endif
			                       <span class="pull-right ">
			                        <span class="blueText rightMargin">
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star-half-empty"></i>
			                        </span>
			                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
			                      </span>
			                    </div> -->
			                    <div class="normalText bottomPadding">
				                         @if($row->city || $row->countryName)
				                          <i class="fa fa-map-marker rightMargin"></i>
				                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
				                         @else
				                          <i class="fa fa-map-marker rightMargin"></i> not available
				                         @endif
				                         <span class="pull-right bottomPadding">
				                  	 @if($related_ad->average_rate != null)
				                       <span id="related_ads_{{$related_ad->id}}" class="blueText pull-left rightMargin"></span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @else
				                       <span class="blueText rightMargin">No Rating</span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @endif	
				                  </div>
				                         <?php $i=0;?>
				                               @if(count($row->getAdvertisementComments)>0)
				                                  @foreach($row->getAdvertisementComments as $count)
				                                        <?php $i++;?>     
				                                   @endforeach
				                               @endif
				                  <!-- <div class="minPadding adcaptionMargin">
				                  	 @if($row->average_rate != null)
				                       <span id="related_adset2_{{$row->id}}" class="blueText pull-left rightMargin"></span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @else
				                       <span class="blueText rightMargin">No Rating</span>
				                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
				                     @endif	
				                  </div> -->
			                  </div>
			                </div>
			              </div>
			              @endforeach
			             </div>
			            </div>
			            @endif
			          </div>
			          <!-- Left and right controls -->
			          <a class="left carousel-control" href="#relatedAds" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:90px; left:0px;">
			            <span class="fa fa-angle-left" aria-hidden="true"></span>
			            <span class="sr-only">Previous</span>
			          </a>
			          <a class="right carousel-control" href="#relatedAds" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:90px; right:0px;">
			            <span class="fa fa-angle-right" aria-hidden="true"></span>
			            <span class="sr-only">Next</span>
			          </a>
			          
			        </div>
			    </div>
			      </div>
			    </div>
				</div>
</div>
<?php echo $modal_related_popups;?>
       <div class="popup-box chat-popup" id="qnimate">
          <div class="popup-head">
        <div class="popup-head-left pull-left"> Send A Message </div>
            <div class="popup-head-right pull-right">
<!--             <div class="btn-group">
                      <button class="chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false">
                     <i class="glyphicon glyphicon-cog"></i> </button>
                    <ul role="menu" class="dropdown-menu pull-right">
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Clear Chat</a></li>
                    <li><a href="#">Email Chat</a></li>
                    </ul>
            </div> -->
            
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                      </div>
        </div>
      <div class="minPadding">
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <input class="topPadding fullSize" type="text" name="title" id="row-title" >
      <input class="topPadding" type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
      <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message"></textarea>
<!--       <div class="btn-footer">
      <button class="bg_none"><i class="glyphicon glyphicon-film"></i> </button>
      <button class="bg_none"><i class="glyphicon glyphicon-camera"></i> </button>
            <button class="bg_none"><i class="glyphicon glyphicon-paperclip"></i> </button>-->
<!--       <button class="bg_none pull-right"><i class="glyphicon glyphicon-send"></i> </button> -->
          <div class="clearfix bottomPadding">
        <div class="popup-messages-footer">
        <button id="" class="btn normalText redButton borderZero pull-right"><i class="fa fa-paper-plane"></i> Submit</button>
      </span>
        {!! Form::close() !!}
      </div>
    </div>
      </div>
    </div>
  <!--  <div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-message-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle"><i class="fa fa-plane"></i>Send Message</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<input class="fullSize" type="text" name="title" id="row-title" value="{{$getads_info->title.'(Inquiry)'}}">
				<textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
			</div>
			<div class="modal-footer">
				<input  type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
				<button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> -->
<div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-message-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title panelTitle"><i class="fa fa-plane rightPadding"></i>Send Message</h4>
          </div>
      <div class="modal-body">
        <div id="form-notice"></div>
        <!-- <input class="fullSize" type="text" name="title" id="row-title" value="Inquiry ({{$rows->name}})">
        <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea> -->
  <div class="">
    <label for="email">Title:</label>
    <input class="form-control borderZero" type="text" name="title" id="row-title" value="Inquiry ({{$getads_info->title}})">
  </div>
  <div class="">
    <label for="pwd">Message:</label>
    <textarea class="form-control borderZero" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
  </div>
      </div>
      <div class="modal-footer">
        <input  type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
        <button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
        <button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
</div>

<!-- </div> -->
@include('ads.form')
@stop
