@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
             window.location.href = "";   
        loading.addClass('hide');
    }
 $("#ads-category").on("change", function() { 
        var id = $("#ads-category").val();
        $("#custom-attribute-container").removeClass("hide");
        $_token = "{{ csrf_token() }}";
        var customAtt = $("#custom-attributes-pane");
        customAtt.html(" ");
        var subcategory = $("#ads-subcategory");
        if ($("#ads-category").val()) {
          $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
            customAtt.html(response);
          }, 'json');
          subcategory.html("");
          $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
            subcategory.html(response);
          }, 'json');

       }    
  });
 $("#ads-type").on("change", function() { 
        var id = $("#ads-type").val();
        if (id == 2) {
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#auction-type-pane").addClass("hide");
        }
  });  



$("#ad-post-content-wrapper").on("click", ".btn-post_not_allowed", function() {
    $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $("#modal-form").find('form').trigger("reset");
    $(".modal-title").html('<strong>Limit Reached </strong>');
    $("#modal-post_invalid").modal('show');    
});

    $("#client-country").on("change", function() { 
    $("#client-city2").attr('disabled', 'disabled');
    $("#client-city2").addClass("hide");
    $("#client-city").removeClass("hide");
}); 
                                    
    $("#ads-price").change(function() {                  
        $('#row-bid_limit_amount').val(this.value);                  
    });


</script>
@stop
@section('content')
<div class="container-fluid bgGray noPadding borderBottom" id="ad-post-content-wrapper">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 noPadding">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 noPadding sideBlock">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Create Ad </span>
          </div>
          <div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
            <div id="ads-notice"></div>
           {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_ads', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-type" name="ad_type">
                    @foreach ($ads_types as $ads_type)
                  <option value="{{ $ads_type->id }}">{{ $ads_type->name }}</option>
                  @endforeach
                </select> 
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title </h5>
              <div class="col-sm-9">
                <input type="text" name="title" class="borderZero form-control borderzero" id="ads-title" maxlength="60">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="ads-price" name="price" nkeypress="document.theform.bid_limit_amount.value = this.value">
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
               <select type="text" class="form-control borderZero" id="ads-category"  name="category">
                  <option class="hide">Select Category</option>
                  @foreach($category as $row)
                    @if($row->buy_and_sell_status == 3)
                      <option value="{{$row->id}}">{{$row->name}}</option>
                    @endif
                  @endforeach
               </select>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category</h5>
              <div class="col-sm-9">
               <select type="text" class="form-control borderZero" id="ads-subcategory"  name="subcategory">

               </select>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description 
               </h5>
              <div class="col-sm-9">
                <textarea  class="form-control borderZero" id="ads-description" rows="10"  name="description"></textarea>
              </div>
            </div>

              <?php $i= $get_user_plan_types->img_per_ad - 0; 
             for ($x = 1; $x <= $i; $x++) {
                 echo '
            <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email">'; if ($x == 1) { echo 'Ad Photos (Max 4)'; } echo '</h5>
               <div class="col-sm-9">
                <div class="uploader-pane">
                      <div class="change">
                        <input type="hidden" id="row-photo">
               <input name="photo'.$x.'" type="file" id="row-photo[1]" class="file form-control borderZero file_photo"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*" ';
              if ($x == 1) {
                 echo 'required="required"';
               }
               echo 'value="Upload a Photo">&nbsp;
                      </div>
                </div>
              </div>
              </div>
                 ';
              }
              ?>          

                  <div class="hide" id="auction-type-pane">
                    <h5 class="inputTitle borderbottomLight">Auction Information </h5>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-bid_limit_amount" name="bid_limit_amount">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-bid_start_amount" name="bid_start_amount">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid" placeholder="">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                      <div class="col-sm-9">
                        <div class="col-sm-6 noleftPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8">
                              <input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 norightPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8"> 
                              <select id="row-bid_duration_dropdown_id" name="bid_duration_dropdown_id" class="form-control borderZero inputBox fullSize">
                                <option value="" class="hide" selected>Select:</option>
                                 @foreach ($bid_duration_dropwdown as $value)
                                    <option value="{{$value->id}}">{{$value->value}}</option>
                                 @endforeach
                              </select>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <h5 class="inputTitle borderbottomLight bottomPadding hide" id="custom-attribute-container">Custom Attribute</h5>
              <div id="custom-attributes-pane">
              </div>

              <h5 class="inputTitle borderbottomLight bottomPadding">Additional Information </h5>
              <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero" id="client-country" name="country">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}" {{($get_user_country==$row->id ? 'selected' : '')}}>{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero hide" id="client-city" name="city">
                </select>


                   <select class="form-control borderZero" id="client-city2" name="city2">
                   @foreach($city as $row)
                    <option value="{{$row->id}}" {{($get_user_city==$row->id ? 'selected' : '')}}>{{$row->name}}</option>
                  @endforeach
                    </select>
              </div>
            </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Address</h5>
                        <div class="col-sm-9">
                              <input class="form-control borderZero" type="text" id="#"  name="address" value='{{ $get_user_address }}'>
                        </div>
                      </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Youtube</h5>
                        <div class="col-sm-9">
                            <input type="embed" name="youtube" pattern="<iframe.+" class="form-control borderZero">
                        </div>
                      </div>
     
            <div class="bordertopLight topPaddingB">
              <input type="hidden" name="rem_img_upload" value="<?php echo $i; ?>">
                    <?php echo $post_ad_btn;?>
            </div>
          {!! Form::close() !!}


          </div>
        </div>
        </div>
        </div>
      </div>

 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span class="pull-right"><?php echo($get_listing_available); ?></span></div>
            <hr>
            <div class=""><i class="fa fa-th"></i> Auction Ads Available <span class="pull-right"><?php echo($get_auction_ads_available); ?></span></div>
          </div>
        </div>
          
      <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
       </div>
      </div>


       <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
                <a href="{{ url('/ads/view') . '/' . $featured_ad->id }}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
              <div class="rightPadding nobottomPadding lineHeight">
                <div class="mediumText grayText topPadding">{{ $featured_ad->title }}</div>
                    <div class="normalText bottomPadding">
                          <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="mediumText bottomPadding">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>
                    <div class="minPadding">
                      
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                       @else
                         <span class="blueText rightMargin">No Rating</span>
                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
                </div>
              </div>
             </div>
             @endforeach
    </div>
  </div>
</div>
@include('ads.form')
@stop
