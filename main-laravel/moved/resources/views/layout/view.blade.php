@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">


function refresh(){

	$_token = "{{ csrf_token() }}";
	 var id = "{{ $ads_id }}";

	 var loading = $(".loading-pane");

	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
	 $.post("{{ url('ads/fetch-info/'.$ads_id) }}", { id: id, _token: $_token }, function(response) {
	 	
	 	$.each(response.rows, function(index, value) {
	 		//console.log(index+"="+value);
	 		var field = $("#rows-" + index);

 			if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	console.log(value);
             	 $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }
             
          }); 
      }, 'json');

	 loading.addClass("hide");
	}
</script>
@stop
@section('content')

{{$rows->price}}

<div class="container-fluid bgWhite noPadding borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-md-12 noPadding">
			<div class="col-md-9">
	<div class="col-md-12 noPadding blockBottom">
		<div class="fill" style="background: url({{ url('uploads/ads/').'/'.$rows->photo}}); background-repeat: no-repeat; background-size: cover; height: 400px; width: auto;">
		</div>
	</div>
	<div class="col-md-12 noPadding">
		<div class="panel panel-default bottomMarginLight removeBorder borderZero">
			<div class="panel-body bgGray">
				<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">

					<!-- Wrapper for slides -->
					<div class="carousel-inner" role="listbox">
						<div class="item active">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								@foreach ($thumbnails as $row)
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 noPadding ">
									<div class="sideMargin">
										><div class="adImage" style="height:80px; width:100%; background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
										</div>
									</div>
								</div>
								@endforeach
				<!-- 			<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 noPadding ">
								<div class="sideMargin">
									<a href="http://192.168.254.14/yakolak/public/ads/view/22"><div class="adImage" style="height:80px; width:100%; background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/Papawash.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div></a>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 noPadding ">
								<div class="sideMargin">
									<a href="http://192.168.254.14/yakolak/public/ads/view/21"><div class="adImage" style="height:80px; width:100%; background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/hoverboard.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div></a>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 noPadding ">
								<div class="sideMargin">
									<a href="http://192.168.254.14/yakolak/public/ads/view/19"><div class="adImage" style="height:80px; width:100%; background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/flower_macro_petals_blur_107395_3840x2160.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div></a>
								</div>
							</div> -->    
							</div>
						</div>
					</div>
					<!-- Left and right controls -->
					<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:12px;; left:-10px;">
					  <span class="fa fa-angle-left" aria-hidden="true"></span>
					  <span class="sr-only">Previous</span>
					</a>
					<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:12px;; right:-10px;">
					  <span class="fa fa-angle-right" aria-hidden="true"></span>
					  <span class="sr-only">Next</span>
					</a>      
				</div>
			</div>
		</div>
	</div>
</div>
			<div class="col-md-3 noPadding">
				<div class="col-md-12">
					<div class="panel panel-default bottomMarginLight removeBorder borderZero">
					  <div class="panel-body noPadding">
					  	<div class="blueText xlargeText"><strong>50% Off Every Second Night</strong></div>
					  	<div class="xlargeText grayText"><strong> {{$rows->price}} USD</strong></div>
					  	<div class="mediumText lightgrayText">Ad Rating
					  		<span class="mediumText adRating pull-right">
					  			<i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star"></i>
			            <i class="fa fa-star-half-empty"></i>
					  		</span>
					  	</div>
					  	<div class="row">
					  		<div class="col-md-6 col-sm-6 col-xs-12 bottomPadding"><button class="btn fullSize normalText rateButton borderZero"><i class="fa fa-star"></i> Rate Product</button></div>
					  		<div class="col-md-6 col-sm-6 col-xs-12 bottomPadding"><button class="btn fullSize normalText watchButton borderZero"><i class="fa fa-eye"></i> Watch Item</button></div>
					  	</div>
					  		<div class="col-md-12 noPadding"><button class="btn fullSize normalText blueButton borderZero"><i class="fa fa-comments"></i> Leave Comment</button></div>
					  	<div class="col-md-12 topMargin blockBottom noPadding">
					  	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body topPaddingB bgGray">
							  	<div class="row">
							  		<div class="col-md-4 col-sm-3 col-xs-12">
							  			<div class="adImage" style="height:85px; width:100%; background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/flower_macro_petals_blur_107395_3840x2160.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
								  		</div>
								  	</div>
							  		<div class="col-md-8 col-sm-9 col-xs-12">
							  			<div class="mediumText grayText"><strong>{{ $rows->name }}</strong></div>
							  			<div class="normalText grayText"><i class="fa fa-envelope rightMargin"></i> {{ $rows->email }}</div>
							  			<div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> {{ $rows->mobile }}</div>
							  			<div class="normalText grayText"><i class="fa fa-map-marker rightMargin"></i> Jeddah, {{ $rows->countryName }}</div>
							  		</div>
							  	</div>
							  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding normalText">Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.
							  		</div>
							  </div>
							</div>
							<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body bgGray">
					  			<div class="normalText grayText"><i class="fa fa-globe rightMargin"></i> shangrila.com</div>
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-facebook-square rightMargin"></i></span> facebook.com/shangrila</div>
					  			<div class="normalText grayText"><span class="blueText"><i class="fa fa-twitter rightMargin"></i></span> twitter.com/shangrila</div>
							  </div>
							</div>
						</div>
						<div class="col-md-12 noPadding"><button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-paper-plane"></i> Message Shangrila</button></div>
					  </div>
				</div>
				</div>
			</div>
    </div>
 </div>
 </div>
 <div class="container-fluid bgGray borderBottom">
	<div class="container blockMargin noPadding">
		<div class="col-md-12 noPadding">
			<div class="col-md-9 col-sm-12 col-xs-12">
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<span class="panelTitle">Description</span>
				  	<p> {{ $rows->description }}</p>
				  </div>
				</div>
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				 <div class="panelTitle bottomPaddingB">Custom Attributes</div>
				  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  	<div class="row">
				  	<div class="col-lg-6 col-sm-6 col-xs-12">
							<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
						</div>
				  	<div class="col-md-6 col-sm-6 col-xs-12">
				  		<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<div class="inputGroupContainer">
									<div class="input-group col-md-12 col-sm-12 col-xs-12">
										<input name="first_name" placeholder="Input"  class="form-control borderZero inputBox fullSize" type="text">
									</div>
								</div>
							</div>
				  	</div>
				  	</div>
				  </div>
				</div>
				</div>
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<span class="panelTitle">Video</span>
				  </div>
				</div>
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<div class="panelTitle bottomPaddingB">Contact Information</div>
				  	<div class="mediumText grayText"><strong>Address:</strong><span class="mediumText grayText"> {{ $rows->address }}</span></div>
				  	<div class="mediumText grayText"><strong>Phone No.:</strong><span class="mediumText grayText"> {{ $rows->telephone }}</span></div>
				  	<div class="mediumText grayText"><strong>Email Address:</strong><span class="mediumText grayText"> {{ $rows->email }}</span></div>
				  	<div class="mediumText grayText"><strong>Office Hours:</strong><span> {{ $rows->office_hours }}</span></div>
				  	<div class="panelTitle topPaddingB bottomPaddingB">Social Media</div>
				  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-facebook-square"></i> facebook.com/shangrila</span></div>
				  		<div class="col-md-4 col-sm-4 col-xs-6"><span class="rightMargin mediumText grayText"><i class="fa fa-twitter"></i> twiiter.com/shangrila</span></div>
				  </div>
				</div>
			</div>
			<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3850.9779215686144!2d120.58926331539807!3d15.159557167196674!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3396f273d427eb37%3A0xb9f5907c1809acdb!2sOceana+Commercial+Complex!5e0!3m2!1sen!2sph!4v1455856335967" width="100%" height="258px" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
   </div>




   <div class="col-md-3 col-sm-12 col-xs-12 noPadding">
   	<div class="col-md-12 col-sm-12 col-xs-12">
	   	<div class="panel panel-default removeBorder borderZero">
	        <div class="panel-heading panelTitleBarLightB">
	          <span class="panelRedTitle">ADVERTISEMENT</span>
	        </div>
	      <div class="panel-body noPadding">
          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
	     </div>
	   	</div>
 		</div>


 		<div class="col-md-12 col-sm-12 col-xs-12">
	   	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        @foreach ($featured_ads as $row)
    		<div class="panel-body adspanelPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7">
        		<div class="mediumText grayText">Featured Ads 2</div>
        		<div class="normalText lightgrayText">by Dell Distributor</div>
        		<div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
        @endforeach
	     </div>
	     <div class="panel panel-default removeBorder borderZero">
<!--     		<div class="panel-body adspanelPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7">
        		<div class="mediumText grayText">Featured Ads 1</div>
        		<div class="normalText lightgrayText">by Dell Distributor</div>
        		<div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div> -->
	     </div>
	   	</div>


	   	<div class="col-md-12 col-sm-12 col-xs-12">
	   		<div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">PRODUCTS BY {{ $rows->name}}</span>
        </div>
	   		@foreach ($vendor_ads as $row)
	   	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        
    		<div class="panel-body adspanelPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7">
        		<div class="mediumText grayText">{{ $row->title }}</div>
        		<div class="normalText lightgrayText">by {{ $row->name}}</div>
        		<div class="mediumText blueText"><strong>{{ $row->price }} USD</strong></div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
	     </div>
	     @endforeach
<!-- 	     <div class="panel panel-default removeBorder borderZero">
    		<div class="panel-body adspanelPadding">
        <div class="row">
        	<div class="col-md-5 col-sm-5 col-xs-5">
        		<div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
        	</div>
        	</div>
        	<div class="col-md-7 col-sm-7 col-xs-7">
        		<div class="mediumText grayText">Product 2</div>
        		<div class="normalText lightgrayText">by Dell Distributor</div>
        		<div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
        		<div class="mediumText blueText">
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star"></i>
        			<i class="fa fa-star-half-empty"></i>
        			<span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
        		</div>
        	</div>
        </div>
        </div>
	     </div> -->
	   	</div>
 		</div>
  </div>
 </div>
</div>
</div>
@stop
