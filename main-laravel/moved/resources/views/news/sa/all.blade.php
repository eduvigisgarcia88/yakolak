<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque" style="background-color: #ffffff !important;">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo sa-logo" src="{{ url('/') }}/uploads/logo/samaya-logo-ar.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-left" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">الرئيسية</a></li>
            <li><a class="smoothscroll" href="{{ url('/#about-us') }}">حول نحن</a></li>
            <!-- <li><a class="smoothscroll" href="{{ url('maxillofacial') }}">MAXILLOFACIAL</a></li> -->
            <li><a class="smoothscroll" href="{{ url('services') }}">خدمات العياده</a></li>
            <li><a class="smoothscroll" href="{{ url('profile/view') . '/'.$profiles->id  }}">لفريق الطبى</a></li>
            <li><a class="smoothscroll active" href="{{ url('/#services') }}">الفريق الطبي</a></li>
            <li><a class="smoothscroll" href="{{ url('career-page') }}">CAREER</a></li>
            <li><a class="smoothscroll" href="{{ url('contact-us') }}">CONTACT US</a></li>
        </ul>
        </div>
    </div>
</nav>

<section id="jumbotron" class="news">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">سمايا عيادة المتقدم تجميل الاسنان</p>
            {!! Form::open(array('url' => 'language', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <select name="language">
                    <option value="sa">عربي</option>
                    <option value="en">English</option>
                </select>
                <button type="submit" class="btn btn-xs">تغيير</button>
            {!! Form::close() !!}
        </div>
    </div>
	<div id="news-all" style="padding-top: 120px; padding-bottom: 60px;">
        <div class="container">
        @foreach ($rows as $row)
            <div class="row news-info">
                <a href="{{ url('news/view/' . $row->id )}}"><div class="pull-right news-img-ar" style="background-image: url({{ url('/') }}/uploads/news/thumbnail/{{ $row->photo }});"></div></a>
                <p class="news-title"><a href="{{ url('news/view/' . $row->id )}}">{{ $row->title_arabic }}</a></p>
                <p class="news-date">{{ date("F j, Y  h:i A", strtotime($row->created_at)) }}</p>
                @if (strlen($row->desc_arabic) > 310)
                <p class="news-article">{{ substr($row->desc_arabic, 0, 310) }}...</p>
                @else
                <p class="news-article">{{ $row->desc_arabic }}</p>
                @endif
                <p class="news-readmore"><a href="{{ url('news/view/' . $row->id )}}">READ MORE</a></p>
            </div>
        @endforeach
        </div>
        <div id="row-pages" class="text-center">{!! $pages !!}</div>
	</div>
</section>