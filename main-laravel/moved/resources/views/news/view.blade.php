@extends('layouts.frontend')

@section('scripts')

@stop

@section('content')

@if(Session::has('locale'))

    @if (Session::get('locale') == 'sa')

        @include('news.sa.view')

    @else

        @include('news.en.view')

    @endif

@else

    @include('news.en.view')

@endif

@stop