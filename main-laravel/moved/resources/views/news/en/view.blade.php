<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" src="{{ url('/') }}/uploads/logo/samaya-logo.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">HOME</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#about-us">ABOUT US</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#services">SERVICES</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#medical-team">MEDICAL TEAM</a></li>
            <li><a class="smoothscroll" href="{{ url('/') }}#news">NEWS</a></li>
            </ul>
        </div>
    </div>
</nav>

<section id="jumbotron" class="news">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">Samaya Clinic Advanced Cosmetic Dentistry</p>
            {!! Form::open(array('url' => 'language', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <select name="language">
                    <option value="en">English</option>
                    <option value="sa">Arabic</option>
                </select>
                <button type="submit" class="btn btn-xs">Change</button>
            {!! Form::close() !!}
        </div>
    </div>
	<div id="news-content" class="container">
		<img src="{{ url('/') }}/uploads/news/{{ $news->photo }}" onerror="this.style.display='none'">
		<h1>{{ $news->title_eng }}</h1>
		<span><?php echo($news->desc_eng) ?></span>
	</div>
</section>