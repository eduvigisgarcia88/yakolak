<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" src="{{ url('/') }}/uploads/logo/samaya-logo.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
         <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">HOME</a></li>
            <li><a class="smoothscroll" href="{{ url('/#about-us') }}">ABOUT US</a></li>
            <!-- <li><a class="smoothscroll" href="{{ url('maxillofacial') }}">MAXILLOFACIAL</a></li> -->
            <li><a class="smoothscroll" href="{{ url('/#services') }}">SERVICES</a></li>
             <li><a class="smoothscroll" href="{{ url('profile/view') . '/'.$profiles->id  }}">MEDICAL TEAM</a></li>
            <li><a class="smoothscroll active" href="{{ url('/#news') }}">NEWS</a></li>
            <li><a class="smoothscroll" href="{{ url('career-page') }}">CAREER</a></li>
            <li><a class="smoothscroll" href="{{ url('contact-us') }}">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</nav>


<section id="jumbotron" class="news">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">Samaya Clinic Advanced Cosmetic Dentistry</p>
            {!! Form::open(array('url' => 'language', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <select name="language">
                    <option value="en">English</option>
                    <option value="sa">Arabic</option>
                </select>
                <button type="submit" class="btn btn-xs">Change</button>
            {!! Form::close() !!}
        </div>
    </div>
	<div id="news-all">
        <div class="container">
        @foreach ($rows as $row)
            <div class="row news-info">
                <a href="{{ url('news/view/' . $row->id )}}"><div class="pull-left news-img" style="background-image: url({{ url('/') }}/uploads/news/thumbnail/{{ $row->photo }});"></div></a>
                <p class="news-title"><a href="{{ url('news/view/' . $row->id )}}">{{ $row->title_eng }}</a></p>
                <p class="news-date">{{ date("F j, Y  h:i A", strtotime($row->created_at)) }}</p>
                @if (strlen($row->desc_eng) > 310)
                <p class="news-article">{{ substr($row->desc_eng, 0, 310) }}...</p>
                @else
                <p class="news-article">{{ $row->desc_eng }}</p>
                @endif
                <p class="news-readmore"><a href="{{ url('news/view/' . $row->id )}}">READ MORE</a></p>
            </div>
        @endforeach
        </div>
        <div id="row-pages" class="text-center">{!! $pages !!}</div>
	</div>
</section>