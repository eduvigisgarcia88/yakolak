<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'config/news/save', 'role' => 'form', 'class' => 'form-horizontal', 'files' => true)) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Modal title</h4>
			</div>
			<div class="modal-body">
			  <div id="form-notice"></div>
				<div class="form-group hide" id="tester">
					<img style="max-width: 200px;" id="preview" src=""/>
				</div>
				</center>
				<div class="form-group">
					<label for="row-photo" class="col-sm-3 control-label font-color">Photo</label>
				<div class="col-sm-9">
					<input id="row-photo" type="file" class="file" name="photo" data-preview-file-type="text">
				</div>
				</div>
				<div class="form-group">
					<label for="row-title_eng" class="col-sm-3 control-label font-color">News Title (English)</label>
				<div class="col-sm-9">
					<input type="text" name="title_eng" class="form-control" id="row-title_eng" maxlength="100">
				</div>
				</div>
				<div class="form-group">
					<label for="row-title_arabic" class="col-sm-3 control-label font-color">News Title (Arabic)</label>
				<div class="col-sm-9">
					<input type="text" name="title_arabic" class="form-control" id="row-title_arabic" maxlength="100">
				</div>
				</div>
				<div class="form-group">
					<label for="row-desc_eng" class="col-sm-3 control-label font-color">Description (English)</label>
				<div class="col-sm-9">
					<textarea name="desc_eng" id="row-desc_eng" class="form-control" rows="10"></textarea>
				</div>
				</div>
				<div class="form-group">
					<label for="row-desc_arabic" class="col-sm-3 control-label font-color">Description (Arabic)</label>
				<div class="col-sm-9">
					<textarea name="desc_arabic" id="row-desc_arabic" class="form-control" rows="10"></textarea>
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Save changes</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>