@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

	// refresh the list
	function refresh() {
			var sort = urlData('s') || '';
			var order = urlData('o') || '';
			var page = urlData('page') || '';
			var loading = $("#load-list");
			var table = $("#rows");
			$_token = "{{ csrf_token() }}";
			var url = "{{ url('/') }}";
			loading.removeClass('hide');
			
			$.post("{{ url('config/news/refresh') }}", { s: sort, o: order, page: page, _token: $_token }, function(response) {
					// clear table
					table.html("");

					// populate table
					$.each(response.rows.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							table.append(
									'<tr data-id="' + row.id + '">' +
									'<td><img src="' + url + "/uploads/news/thumbnail/" + row.photo + '" onerror="this.style.display="none"" style="max-width: 300px; max-height: 300px;" class="img-responsive"></td>' +
									'<td>' + row.title_eng + '</td>' +
									// '<td>' + desc_eng + '</td>' +
									'<td>' + moment(row.created_at).format('LLL') + '</td>' +
									'<td class="text-right">' + 
										'<a target="_blank" href="' + url + '/news/view/' + row.id + '" class="btn btn-xs btn-success btn-view" title="View News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
										'&nbsp;<a href="' + url + '/config/news/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
										'&nbsp;<a class="btn btn-xs btn-danger btn-delete" title="Delete News" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// update links
					$("#row-pages").html(response.pages);

					loading.addClass('hide');
			}, 'json');
	}
	
	$(document).ready(function() {

		refresh();
		// delete news
		$("#news-lists").on("click", ".btn-delete", function() {
	  		var id = $(this).parent().parent().data('id');
	  		var name = $(this).parent().parent().find('td:nth-child(2)').html();

			$(".modal-header").removeAttr("class").addClass("modal-header delete-bg");

	  		dialog('News Deletion', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('config/news/delete') }}", id);
		});
	});

</script>
@stop

@section('content')

<div class="container">
    @if (Session::has("notice"))
        <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }} <a target="_blank" href="{{ Session::get('notice.url') }}">View Here</a>
        </div>
    @endif
    <div class="panel panel-default panelShadow">
	  <div class="panel-heading">
	  	<h4>NEWS
	  		<span class="pull-right"><a class="btn btn-addnew" href="{{ url('config/news/create') }}" style="margin: -6px 0 0px;"><i class="fa fa-plus"></i> Add New</a></span>
	  	</h4>
	  </div>
	  <div class="panel-body">
	  	<div class="table-responsive padding-none">
			<table id="news-lists" class="table panelShadow">
				<thead>
					<tr>
						<th>Photo</th>
						<th><a href="{{ $url . '?s=title_eng&amp;o=' . ($sort == 'title_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">NEWS TITLE{!! ($sort == 'title_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
						<th><a href="{{ $url . '?s=created_at&amp;o=' . ($sort == 'created_at' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">DATE{!! ($sort == 'created_at' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>			
						<th><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
					</tr>
				</thead>
				<tbody id="rows">
				</tbody>
			</table>
		</div>
	  </div>
	  	<div class="container-fluid marginTopZero">
	  	<div id="row-pages" class="text-right">{!! $pages !!}</div>
	  	</div>
	</div>
</div>
@stop