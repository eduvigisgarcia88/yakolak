@extends('layout.frontend')
@section('scripts')
	<script>
	 $_token = "{{ csrf_token() }}";
	 $("#ads-category").on("change", function() { 
	        var id = $("#ads-category").val();
	        console.log(id);
	        $("#custom-attribute-container").removeClass("hide");
	        $_token = "{{ csrf_token() }}";
	        var custom_attri = $("#custom-attributes-pane");
	        custom_attri.html(" ");
	        var subcategory = $("#ads-subcategory");
	        if ($("#ads-category").val()) {
	          $.post("{{ url('get-custom-attributes') }}", { id: id, _token: $_token }, function(response) {
	            custom_attri.html(response);
	          }, 'json');
	          	subcategory.html("");
	          $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
	            subcategory.html(response);
	          }, 'json');

	       }    
	  });
	</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding sideBlock">
				
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SEARCH OPTIONS</span>
					</div>
				{!! Form::open(array('url' => 'browse', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
					<div class="panel-body normalText">
						<input type="text" class="form-control borderZero inputBox bottomMargin" name="" placeholder="Search Value">
						<select class="form-control borderZero inputBox bottomMargin" id="ad-type" name="ad_type">
		                  <option class="hide">Select</option>
		                  @foreach($ad_type as $row)
		                    <option value="{{$row->id}}">{{$row->name}}</option>
		                  @endforeach
		                </select>

						 <select class="form-control borderZero inputBox bottomMargin" id="ads-category" name="category">
						 	<option class="hide">Select</option>
						 	@foreach($category as $row)
						 		@if($row->buy_and_sell_status == 3)
						 			<option value="{{$row->id}}">{{$row->name}}</option>
						 		@endif
						 	@endforeach
			             </select>
			             <select class="form-control borderZero inputBox bottomMargin" id="ads-subcategory" name="subcategory">
						 		<option value="">All</option>
			             </select>
						 <select class="form-control borderZero inputBox bottomMargin" id="client-country" name="country">
		                  <option class="hide">Select</option>
		                  @foreach($countries as $row)
		                    <option value="{{$row->id}}">{{$row->countryName}}</option>
		                  @endforeach
		                 </select>
						<select class="form-control borderZero inputBox bottomMargin" id="client-city" name="city">
              			</select>
					</div>
					<div class="panel-heading panelTitleBarLightB bordertopLight">
						<span class="panelTitleSub">(CATEGORY) ATTRIBUTES</span>
					</div>
					<div class="panel-body normalText">
						<div id="custom-attributes-pane">
					 	</div>
					</div>
					<div class="panel-heading panelTitleBarLightB bordertopLight">
						<span class="panelTitleSub">PRICE RANGE</span>
					</div>
					<div class="panel-body normalText">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin" name="price_from" placeholder="ie. 100" value="100">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding leftPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin" name="price_to" placeholder="ie. 10000" value="1000">
							</div>
						</div>
						<button class="btn blueButton borderZero noMargin fullSize" type="submit"><i class="fa fa-search"></i> Update Search</button>
					</div>
				</div>
			{!! Form::close() !!}	
<!-- 				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        <div class="panel-body noPadding">
        <div class="row">
          <div class="col-md-6 col-sm-3 col-xs-3">
            <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
          </div>
          </div>
          <div class="col-md-6 col-sm-9 col-xs-9 noleftPadding">
            <div class="mediumText grayText">Featured Ads 2</div>
            <div class="normalText lightgrayText">by Dell Distributor</div>
            <div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
            <div class="mediumText blueText">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-empty"></i>
              <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
            </div>
          </div>
        </div>
        </div>
       </div> -->
<div class="hidden-xs hidden-sm">
<div class="blockBottom">			
<div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{ url('/ads/view') . '/' . $featured_ad->id }}"> 
              		<div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div>
          		</a>
	          	<div class="rightPadding nobottomPadding lineHeight">
	          		<div class="mediumText grayText topPadding">{{ $featured_ad->title }}</div>
	                  <div class="normalText bottomPadding">
	                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id }}">by {{$featured_ad->name}}</a>
	                  </div>
	                  <div class="mediumText bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
	                        <div class="mediumText bottomPadding">
	                         @if($featured_ad->city || $featured_ad->countryName)
	                          <i class="fa fa-map-marker rightMargin"></i>
	                          {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
	                         @else
	                          <i class="fa fa-map-marker rightMargin"></i> not available
	                         @endif
	                         </div>
	                  <div class="minPadding">
	                  	 @if($featured_ad->average_rate != null)
	                       <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
	                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
	                     @else
	                       <span class="blueText rightMargin">No Rating</span>
	                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
	                     @endif	
	                  </div>
	        	</div>
<!--               <div class="row">
                <a href="{{ url('/ads/view') . '/' . $featured_ad->id }}">
                <div class="col-md-6 col-sm-3 col-xs-3 norightPadding">
                    <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
               </div>
                </div>
                </a>
                <div class="col-md-6 col-sm-9 col-xs-9 noleftPadding lineHeight">
                  <div class="mediumText grayText topPadding bottomPadding">{{ $featured_ad->title }}</div>
                  <div class="normalText bottomPadding">
                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id }}">by {{$featured_ad->name}}</a>
                  </div>
                  <div class="mediumText blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                        <div class="mediumText bottomPadding">
                         @if($featured_ad->city || $featured_ad->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                         </div>
                  <div class="minPadding">
                  	 @if($featured_ad->average_rate != null)
                       <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @else
                       <span class="blueText rightMargin">No Rating</span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @endif	
                  </div>
              </div>
              </div> -->
              </div>
             </div>
             @endforeach
         </div>
     </div>
       </div>
     </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				
				<div class="panel panel-default borderZero removeBorder">
				  <div class="panel-body noPadding">
				  	<img class="fill" src="{{ url('uploads').'/'.$banner_placement_top->photo }}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
				  </div>
				  <div class="panel-footer bgWhite">
				  	<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
				 	</div>
				</div>
				<!-- <div class="panel panel-default removeBorder borderZero ">
					<div class="panel-body">
					</div>
					<div class="panel-heading removeBorder panelTitleBarLightB">
						<div class="fill" style="background: url({{ url('img').'/'}}Banner-MainA.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        				</div>
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>

				</div> -->
				
			<div class="blockBottom">			
				<div class="panel-heading panelTitleBarLightB">
					<span class="panelBlueTitle">Search Result<span class="pull-right"><span class="leftMarginB normalText grayText">Sort by <i class="fa fa-sort"></i></span><span class="leftMarginB normalText grayText">List by  <i class="fa fa-th"></i> </span></span></span></span>         
       			 </div>
       			 	<div class="visible-xs visible-sm">
						@forelse($rows as $row)
									           <div class="panel panel-default bottomMarginLight removeBorder borderZero">
            							  <div class="panel-body noPadding">
								        @foreach($row->getAdvertisementPhoto as $photo_info)
											@if($photo_info->primary == 1)		
								         <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$photo_info->photo }}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
								          </div></a>
								         @endif
										@endforeach
								        <div class="rightPadding nobottomPadding">
								          <div class="mediumText noPadding topPadding bottomPadding">{{$row->title}}</div>
								          <div class="normalText redText bottomPadding">
								            D: 00 - H: 00 - M: 00
								          </div>
								          <div class="mediumText blueText bottomPadding">
								            <strong> {{ $row->price }}USD</strong>
								          </div>
								          <div class="mediumText bottomPadding">
								          <i class="fa fa-map-marker"></i> 
								             
								          </div>
								          <div class="mediumText blueText">
								            <i class="fa fa-star"></i>
								            <i class="fa fa-star"></i>
								            <i class="fa fa-star"></i>
								            <i class="fa fa-star"></i>
								            <i class="fa fa-star-half-empty"></i>
								            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
								          </div>
								        </div>
								      </div>
								    </div>
							 @empty
		                          <h3 class="text-center">No Result(s) Found</h3>
							 @endforelse
					  </div>

					<div class="panel panel-body hidden-xs hidden-sm">
						@forelse ($rows as $row)
              			   <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding ">
								<div class="sideMargin borderBottom bottomMarginB ">
									@foreach($row->getAdvertisementPhoto as $photo_info)
										@if($photo_info->primary == 1)					
											<a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$photo_info->photo }}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
											</div></a>
										@endif
									@endforeach
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">{{$row->title}}</div>
										<div class="mediumText blueText bottomPadding">
											<strong>{{$row->price}} USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
                          </div>
                          @empty
                          <h3 class="text-center">No Result(s) Found</h3>
						@endforelse
					</div>
					<div class="panel-footer borderTopB bgWhite">
						<nav class="noPadding">
							<span class="clearfix">
							<div class="col-md-6">
					<ul class="pagination noPadding noMargin">
						<li >
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-6">
					<span class="pull-right topPadding">
					<span class="redText normalText">1 - 12 of 1200 listings</span>
				</span>
			</div>
		</span>
				</nav>
					</div>
				</div>
				

				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-heading panelTitleBarLightB">
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>
					<div class="panel-body noPadding">
						<img class="fill" src="{{ url('uploads').'/'.$banner_placement_bottom->photo }}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
					</div>
				</div>

				<div class="visible-xs visible-sm">
<div class="blockBottom">			
<div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{ url('/ads/view') . '/' . $featured_ad->id }}"> 
              		<div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div>
          		</a>
	          	<div class="rightPadding nobottomPadding lineHeight">
	          		<div class="mediumText grayText topPadding">{{ $featured_ad->title }}</div>
	                  <div class="normalText bottomPadding">
	                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id }}">by {{$featured_ad->name}}</a>
	                  </div>
	                  <div class="mediumText bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
	                        <div class="mediumText bottomPadding">
	                         @if($featured_ad->city || $featured_ad->countryName)
	                          <i class="fa fa-map-marker rightMargin"></i>
	                          {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
	                         @else
	                          <i class="fa fa-map-marker rightMargin"></i> not available
	                         @endif
	                         </div>
	                  <div class="minPadding">
	                  	 @if($featured_ad->average_rate != null)
	                       <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
	                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
	                     @else
	                       <span class="blueText rightMargin">No Rating</span>
	                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
	                     @endif	
	                  </div>
	        	</div>
<!--               <div class="row">
                <a href="{{ url('/ads/view') . '/' . $featured_ad->id }}">
                <div class="col-md-6 col-sm-3 col-xs-3 norightPadding">
                    <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover;">
               </div>
                </div>
                </a>
                <div class="col-md-6 col-sm-9 col-xs-9 noleftPadding lineHeight">
                  <div class="mediumText grayText topPadding bottomPadding">{{ $featured_ad->title }}</div>
                  <div class="normalText bottomPadding">
                        <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $featured_ad->user_id }}">by {{$featured_ad->name}}</a>
                  </div>
                  <div class="mediumText blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                        <div class="mediumText bottomPadding">
                         @if($featured_ad->city || $featured_ad->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                         </div>
                  <div class="minPadding">
                  	 @if($featured_ad->average_rate != null)
                       <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @else
                       <span class="blueText rightMargin">No Rating</span>
                       <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                     @endif	
                  </div>
              </div>
              </div> -->
              </div>
             </div>
             @endforeach
         </div>
     </div>
				
        </div>
				
				
      </div>
    </div>
  </div>
</div>
@stop
