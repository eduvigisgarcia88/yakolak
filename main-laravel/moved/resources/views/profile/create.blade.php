@extends('layouts.backend')

@section('scripts')
  <script type="text/javascript">
  	    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2,
           toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });
  </script>
<script type="text/javascript">
	
    /* $('#row-photo').on('change', function() {
		var tmppath = URL.createObjectURL(event.target.files[0]);
    	$("img#preview").attr('src',tmppath);
    }); */
$("input.file").fileinput({
        maxFileCount: 1,
        maxFileSize: 1024,
        allowedFileTypes: ['image'],
		allowedFileExtensions: ['jpg', 'bmp', 'png', 'jpeg'],
    });
</script>
@stop

@section('content')
 
<div class="container">

@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
@endif

{!! Form::open(array('url' => 'config/profile/save', 'role' => 'form', 'class' => 'form-horizontal', 'files' => true)) !!}
	<!-- Nav tabs -->
  <ul class="myNavs nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a class="borderZero" href="#english" aria-controls="english" role="tab" data-toggle="tab">English</a></li>
    <li role="presentation"><a class="borderZero" href="#arabic" aria-controls="arabic" role="tab" data-toggle="tab">Arabic</a></li>
  </ul>

	<div class="tab-content myTabs">	
	<div class="pan panel panel-default borderZero">
  <div class="panel-body">
  	<div class="form-group hide" id="tester">
			<img style="max-width: 200px;" id="preview" src=""/>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-photo', 'Cover Photo', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::file('photo', ['class' => 'file', 'id' => 'row-photo', 'data-show-upload' => 'false', 'data-show-caption' => 'true', 'multiple' => 'true' ]) !!}
			
		</div>
		</div>
  </div>
</div>

	 <!-- Tab panes -->
  
  	<div role="tabpanel" class="tab-pane active" id="english">
    	<div class="container-fluid">
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-name_eng', 'Name (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('name_eng', null, ['class' => 'form-control', 'id' => 'row-name_eng']) !!}
		</div>
		</div>
	

		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-qualification_eng', 'Qualification (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('qualification_eng', null, ['class' => 'form-control', 'id' => 'row-qualification_eng']) !!}
		</div>
		</div>


		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-contact_no_eng', 'Contact no. (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('contact_no_eng', null, ['class' => 'form-control', 'id' => 'row-contact_no_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-specialty_eng', 'Specialty (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('specialty_eng', null, ['class' => 'form-control', 'id' => 'row-specialty_eng']) !!}
		</div>
		</div>

		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-education_eng', 'Education (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('education_eng', null, ['class' => 'form-control', 'id' => 'row-education_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-cur_pos_eng', 'Current Position (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('cur_pos_eng', null, ['class' => 'form-control', 'id' => 'row-cur_pos_eng']) !!}
		</div>
		</div>
		
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-about_me_eng', 'About Me (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::textarea('about_me_eng', null, ['class' => 'form-control summernote', 'id' => 'row-about_me_eng']) !!}
		</div>
		</div>
	
							<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-facebook_eng', 'Facbook Link (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('facebook_eng', null, ['class' => 'form-control', 'id' => 'row-facebook_eng']) !!}
		</div>
		</div>

			<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-twitter_eng', 'Twitter Link (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('twitter_eng', null, ['class' => 'form-control', 'id' => 'row-twitter_eng']) !!}
		</div>
		</div>
			<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-instagram_eng', 'Instagram Link (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('instagram_eng', null, ['class' => 'form-control', 'id' => 'row-instagram_eng']) !!}
		</div>
		</div>

	<!-- Eng Tab End -->
	</div>
	</div>

	<div role="tabpanel" class="tab-pane" id="arabic">
    <div class="container-fluid">
	<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-name_arabic', 'Name (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('name_arabic', null, ['class' => 'form-control', 'id' => 'row-name_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
				<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-qualification_arabic', 'Qualification (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('qualification_arabic', null, ['class' => 'form-control', 'id' => 'row-qualification_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
	<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-contact_no_arabic', 'Contact no. (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('contact_no_arabic', null, ['class' => 'form-control', 'id' => 'row-contact_no_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-specialty_arabic', 'Specialty (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('specialty_arabic', null, ['class' => 'form-control', 'id' => 'row-specialty_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
			<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-education_arabic', 'Education (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('education_arabic', null, ['class' => 'form-control', 'id' => 'row-education_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-cur_pos_arabic', 'Current Position (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('cur_pos_arabic', null, ['class' => 'form-control', 'id' => 'row-cur_pos_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
			<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-about_me_arabic', 'About Me (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::textarea('about_me_arabic', null, ['class' => 'form-control summernote', 'id' => 'row-about_me_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-facebook_arabic', 'Facbook Link (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('facebook_arabic', null, ['class' => 'form-control', 'id' => 'row-facebook_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-twitter_arabic', 'Twitter Link (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('twitter_arabic', null, ['class' => 'form-control', 'id' => 'row-twitter_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
			<div class="form-group">
		<div class="col-sm-3">
			{!! Form::label('row-instagram_arabic', 'Instagram Link (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-sm-9">
			{!! Form::text('instagram_arabic', null, ['class' => 'form-control', 'id' => 'row-instagram_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>

	<!-- arabic tab end -->
	</div>
	</div>
	
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Create profile</button>
		</div>
		</div>

<!-- Tab end -->
	</div>


	




{!! Form::close() !!}

</div>
@stop