@extends('layouts.frontend')

@section('scripts')

@stop

@section('content')

@if(Session::has('locale'))

    @if (Session::get('locale') == 'sa')

        @include('profile.sa.view')

    @else

        @include('profile.en.view')

    @endif

@else

    @include('profile.en.view')

@endif

@stop