@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

	// refresh the list
	function refresh() {
			var sort = urlData('s') || '';
			var order = urlData('o') || '';
			var page = urlData('page') || '';
			var loading = $("#load-list");
			var table = $("#rows");
			$_token = "{{ csrf_token() }}";
			var url = "{{ url('/') }}";
			loading.removeClass('hide');
			
			$.post("{{ url('config/profile/refresh') }}", { s: sort, o: order, page: page, _token: $_token }, function(response) {
					// clear table
					table.html("");

					// populate table
					$.each(response.rows.data, function(index, row) {

							table.append(
									'<tr data-id="' + row.id + '">' +
									'<td><img src="' + url + "/uploads/med-team/thumbnail/" + row.photo + '" onerror="this.style.display="none"" style="max-width: 100px; max-height: 100px;" class="img-responsive"></td>' +
									'<td>' + row.name_eng + '</td>' +
									'<td>' + row.specialty_eng + '</td>' +
									// '<td>' + row.education_eng + '</td>' +
									'<td>' + row.cur_pos_eng + '</td>' +
									'<td>' + row.about_me_eng + '</td>' +
									'<td>' + moment(row.created_at).format('LLL') + '</td>' +
									'<td class="col-lg-2 col-md-2 text-right">' + 
										'<a target="_blank" href="' + url + '/profile/view/' + row.id + '" class="btn btn-xs btn-success btn-view" title="View profile" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-eye"></i></a>' +
										'&nbsp;<a href="' + url + '/config/profile/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit profile" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
										'&nbsp;<a class="btn btn-xs btn-danger btn-delete" title="Delete profile" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-trash"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// update links
					$("#row-pages").html(response.pages);

					loading.addClass('hide');
			}, 'json');
	}
	
	$(document).ready(function() {

		refresh();
		// delete profile
		$("#profile-lists").on("click", ".btn-delete", function() {
	  		var id = $(this).parent().parent().data('id');
	  		var name = $(this).parent().parent().find('td:nth-child(2)').html();

			$(".modal-header").removeAttr("class").addClass("modal-header delete-bg");

	  		dialog('Profile Deletion', 'Are you sure you want to delete <strong>' + name + '</strong>?', "{{ url('config/profile/delete') }}", id);
		});
	});

</script>
@stop

@section('content')

<div class="container">
    @if (Session::has("notice"))
        <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }} <a target="_blank" href="{{ Session::get('notice.url') }}">View Here</a>
        </div>
    @endif
     <div class="panel panel-default panelShadow">
	  <div class="panel-heading">
	  	<h4>Medical Team
	  		<span class="pull-right" style="margin: -6px 0 0px;"><a class="btn btn-addnew" href="{{ url('config/profile/create') }}"><i class="fa fa-plus"></i> Add New</a></span>
	  	</h4>
	  </div>
	  <div class="panel-body">
	  	<div class="table-responsive padding-none">
			<table id="profile-lists" class="table panelShadow">
				<thead>
					<tr>
					<th>Photo</th>
					<th nowrap><a href="{{ $url . '?s=name_eng&amp;o=' . ($sort == 'title_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">NAME{!! ($sort == 'title_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
					<th nowrap><a href="{{ $url . '?s=specialty_eng&amp;o=' . ($sort == 'specialty_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">SPECIALTY{!! ($sort == 'specialty_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
					<!-- <th><a href="{{ $url . '?s=education_eng&amp;o=' . ($sort == 'education_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">EDUCATION{!! ($sort == 'education_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>	 -->		
					<th nowrap><a href="{{ $url . '?s=cur_pos_eng&amp;o=' . ($sort == 'cur_pos_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">CURRENT POSITION{!! ($sort == 'cur_pos_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
					<th nowrap><a href="{{ $url . '?s=about_me_eng&amp;o=' . ($sort == 'about_me_eng' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">ABOUT ME{!! ($sort == 'about_me_eng' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
					<th nowrap><a href="{{ $url . '?s=created_at&amp;o=' . ($sort == 'created_at' && $order == 'asc' ? 'desc' : 'asc') . '&amp;page=' . $rows->currentPage() }}">CREATED AT{!! ($sort == 'created_at' ? ' <i class="fa fa-sort-' . $order . '"></i>' : '') !!}</a></th>
					<th nowrap><i id="load-list" class="fa fa-refresh fa-spin pull-right hide"></i></th>
					</tr>
				</thead>
				<tbody id="rows">
				</tbody>
			</table>
		</div>
	  </div>
	  	<div class="container-fluid marginTopZero">
	  	<div id="row-pages" class="text-right">{!! $pages !!}</div>
	  	</div>
	</div>
</div>

@stop