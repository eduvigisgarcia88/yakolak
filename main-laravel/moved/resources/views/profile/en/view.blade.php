<!-- Navigation Bar -->
<nav id="navbar-main" class="navbar navbar-default navbar-fixed-top opaque">
    <div class="container">
<div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
      <a href="{{ url('/') }}" rel="home">
            <img id="main-logo" class="animated fadeIn img-responsive opaque-logo en-logo" src="{{ url('/') }}/uploads/logo/samaya-logo.png">
            </a>
        </div>
        <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            <li><a class="smoothscroll" href="{{ url('/') }}">HOME</a></li>
            <li><a class="smoothscroll" href="{{ url('/#about-us') }}">ABOUT US</a></li>
          <!--   <li><a class="smoothscroll" href="{{ url('maxillofacial') }}">MAXILLOFACIAL</a></li> -->
            <li><a class="smoothscroll" href="{{ url('services') }}">SERVICES</a></li>
            <li><a class="smoothscroll active" href="{{ url('profile/view') . '/'.$profiles->id  }}">MEDICAL TEAM</a></li>
            <li><a class="smoothscroll" href="{{ url('/#news') }}">NEWS</a></li>
            <li><a class="smoothscroll" href="{{ url('career-page') }}">CAREER</a></li>
            <li><a class="smoothscroll" href="{{ url('contact-us') }}">CONTACT US</a></li>
            </ul>
        </div>
    </div>
</nav>


<section id="jumbotron" class="profile">
 	<div id="change-language">
        <div class="container">
            <p class="hidden-xs" id="sub-title">Samaya Clinic Advanced Cosmetic Dentistry | <a href="{{ url('about-jeddah') }}">About Jeddah</a></p>
            {!! Form::open(array('url' => 'language', 'role' => 'form', 'class' => 'form-horizontal')) !!}
                <select name="language">
                    <option value="en">English</option>
                    <option value="sa">Arabic</option>
                </select>
                <button type="submit" class="btn btn-xs">Change</button>
            {!! Form::close() !!}
        </div>
    </div>

<div id="profile-content" class="container-fluid borderBottom">
    <div class="container">
        <div class="col-lg-12 col-md-12 noPadding">
      <div class="col-md-6">  
      <div class="col-md-6">       
        <div style="background-image: url({{ url('/') }}/uploads/med-team/{{ $profile->photo }})" class="img-view img-responsive" onerror="this.style.display='none'"> </div>  
            <h6><strong>SPECIALTY </strong><br>
            <span><?php echo($profile->about_me_eng) ?></span></h6>
            <h6><strong>EDUCATION </strong><br>
            <span><?php echo($profile->about_me_eng) ?></span></h6> 
            <h6><strong>ABOUT </strong><br>
            <span><?php echo($profile->about_me_eng) ?></span></h6>
                </div>
       <div class="col-md-6">
            <p><span><strong>NAME </strong> {{ $profile->name_eng }}</span><br>
            <p><span><strong>POSITION </strong>{{ $profile->specialty_eng }}</span></p>
            <p><span><strong>QUALIFICATIONS </strong> </span></p>
            <p><span><strong>CONTACT NO. </strong></span></p>
            <p><span><strong>FACEBOOK </strong></span></p>
            <p><span><strong>TWITTER </strong></span></p>
            <p><span><strong>INSTAGRAM </strong></span>
        </div>
      </div>
                
    <div class="col-lg-6 col-md-6 ">
<section id="medical-team" class="active">
    <div class="med-team-container">
    <h2>SAMAYA GROUPS MEDICAL TEAM</h2>
        @foreach ($rows as $row)
        <div class="col-md-3 col-sm-4 med-profile">
            <a href="{{ url('profile/view/' . $row->id )}}"><div class="med-img" style="background-image: url({{ url('/') . '/uploads/med-team/thumbnail/' . $row->photo }});"></div></a>
            <p class="med-name">{{ $row->name_eng}}</p>
            <p class="med-position">{{ $row->specialty_eng }}</p>
            <div class="med-btn">
                <a href="{{ url('profile/view/' . $row->id )}}" class="btn btn-profile">SEE PROFILE</a>
            </div>
        </div>
        @endforeach
    </div>
</section>
      </div>      
      </div>
    </div>
  </div>
</section>