@extends('layouts.backend')

@section('scripts')

@stop

@section('content')

    <div class="container">
    @if (Auth::user()->usertype_id == 1)
    <div class="col-md-6">
    @else
    <div class="col-md-offset-2 col-md-8">
    @endif
    	<div class="panel panel-default panelShadow panelShadow">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-laptop"></i> Inquiries
    		</div>
    		<div class="panel-body">
    		    @foreach($inquiries as $row)
        		  	<strong>{{ $row->name }}</strong> 
                    @if ($row->status == "Pending")
                    <span class="label label-default pull-right">{{ $row->status}}</span>
                    @elseif ($row->status == "Scheduled")
                    <span class="label label-primary pull-right">{{ $row->status}}</span> @ {{ date("M t, o h:i A", strtotime($row->date_sched)) }}
                    @elseif ($row->status == "Finished")
                    <span class="label label-success pull-right">{{ $row->status}}</span>
                    @else    
                    <span class="label label-danger pull-right">{{ $row->status}}</span>
                    @endif
                    <hr>
    		    @endforeach
        		<center><a href="{{ url('config/inquiry') }}" class="btn btn-view_more">VIEW MORE</a></center>
    		</div>
    	</div>
    </div>

    @if (Auth::user()->usertype_id == 1)
    <div class="col-md-6">
    	<div class="panel panel-default panelShadow">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-user-md"></i> Medical Team
    		</div>
    		<div class="panel-body">
    		  @foreach($profiles as $row)
    		  	<strong>{{ $row->name_eng }}</strong> - 
                {{ $row->specialty_eng }}
                <hr>
    		  @endforeach
        		<center><a href="{{ url('config/profile') }}" class="btn btn-view_more">VIEW MORE</a></center>
    		</div>
    	</div>
    </div>
    <div class="col-md-6">
    	<div class="panel panel-default panelShadow">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-star-o"></i> Featured
    		</div>
    		<div class="panel-body">
    		  @foreach($promos as $row)
    		  	<strong>{{ $row->title_eng }}</strong> - 
    		  	@if (strlen($row->desc_eng) > 50)
                {{ substr($row->desc_eng, 0, 50) }}...
                @else
                {{ $row->desc_eng }}
                @endif
                <hr>
    		  @endforeach
        		<center><a href="{{ url('config/promo') }}" class="btn btn-view_more">VIEW MORE</a></center>
    		</div>
    	</div>
    </div>
    <div class="col-md-6">
    	<div class="panel panel-default panelShadow">
    		<div class="panel-heading panel-dashboard">
    			<i class="fa fa-newspaper-o"></i> News
    		</div>
    		<div class="panel-body">
    		  @foreach($news as $row)
    		  	<strong>{{ $row->title_eng }}</strong> - 
    		  	@if (strlen($row->desc_eng) > 50)
                {{ substr($row->desc_eng, 0, 50) }}...
                @else
                {{ $row->desc_eng }}
                @endif
                <hr>
    		  @endforeach
        		<center><a href="{{ url('config/news') }}" class="btn btn-view_more">VIEW MORE</a></center>
    		</div>
    	</div>
    </div>
    </div>
    @endif
@stop