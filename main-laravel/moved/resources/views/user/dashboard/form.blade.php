<div class="modal fade" id="modal-ads-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/remove', 'role' => 'form', 'class' => 'form-horizontal', 'style'=>'margin-bottom: 0', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Delete Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete? 
				</div>
				</div>
			</div>
			<div class="modal-footer bordertopLight borderBottom">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn  redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
        	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-enable-ads" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/enable', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Enable Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to enable this ads?
				</div>
				</div>
			</div>
			<div class="modal-footer bordertopLight borderBottom">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn  redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
        	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<!-- modal feature -->
<div class="modal fade" id="modal-feature-ads" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
			{!! Form::open(array('url' => 'ad/feature', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Feature Ad</h4>
	        </div>
			<div class="modal-body">
			 <div id="feature_ad_points">
				<div id="load-form-feature" class="loading-pane hide">
	    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
	  			</div>
				<div id="form-notice"></div>
				<p>Do you want to make this a feature ad?</p>
				<p>Duration:</p>
				<p><input class="form-control borderZero" type="number" name="duration" id="row-featured_duration" onkeyup="computeDuration()">

					<select class="form-control borderZero" id="row-featured_duration_metric" name="duration_metric" onchange="computeDuration()">
						@foreach($feature_price_matrix as $row)
							<option value="{{$row->id}}">{{$row->name}}</option>
						@endforeach
					</select>
				</p>
				<p>Payment Method</p>
				<p>
				   <select class="form-control borderZero" id="row-payment_method" name="payment_method" onchange="computeDuration()">
						<option value="1">Reward Points</option>
						<option value="2">Paypal</option>
					</select>
				</p>
				<p><sup class="redText hide" id="feature-error">* Insufficient Points</sup></p>
				<p id="total-point-label">Cost: <span id="total-point-cost">0</span> <span id="payment_unit"></span><span id="current_points" class="pull-right"> You have {{Auth::user()->points}} points available.</span></p>
		
				<input type="hidden" name="id" id="row-id" value="">
				<input type="hidden" name="title" id="row-title" value="">
				<input type="hidden" name="cost_points" id="row-cost_points" value="">
		    	<div class="modal-footer bordertopLight borderBottom" id="buttons_for_reward_points">
				 <span>
					<button type="button" class="btn btn-submit blueButton borderZero" disabled="disabled" id="btnFeature">Submit</button>
					<button type="button" class="btn  redButton borderZero" data-dismiss="modal">Close</button>
				</span>
               {!! Form::close() !!}
        	</div>
        	</div>
        	</div>
        	<div class="modal-footer bordertopLight borderBottom hide" id="buttons_for_paypal">
        		<span>
				  <form class="noMargin" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypalForm">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="hosted_button_id" value="W3QQ6C546WHF4">
                        <INPUT TYPE="hidden" NAME="return" id="return_url">
                        <input type="hidden" name="business" id="element-to-hide" value="yakolakseller@gmail.com">
                        <input type="hidden" name="item_name" id="paypal-item-name">
                        <input type="hidden" name="item_number" id="paypal-item-number">
                        <input type="hidden" name="cm" value="">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="amount" id="paypal-amount">
                        <div class="form-group-sm noMargin">
<!--                         <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label> -->
                        <!-- <div class="input-group col-md-9 col-sm-12 col-xs-12 "> -->
                        <input type="submit" value="Add To Cart" style="padding:7px 15px !important;" name="submit" title="PayPal - The safer, easier way to pay online!" class="paypal_btn" id="btnPaypalFeature">
                        <button type="button" class="btn  redButton borderZero" data-dismiss="modal">Close</button>
<!--                         <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->

                        <!-- </div> -->
                        </div>
                    </form>
                </span>
        	</div>
		
		</div>
	</div>
</div>




<div class="modal fade" id="modal-ads-disable" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/disable', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Disable Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to Disable? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-ads-disable-list" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/disablelist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_ads_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Disable Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to Disable? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-disable-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-auction-disable" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/disable', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">End Auction</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to end the auction? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit blueButton borderZero" id="disableYes"><!-- <i class="fa fa-trash"></i> -->Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"> No</button>
<!-- 				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button> -->
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


<div class="modal fade" id="modal-ads-enable" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/enable', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Enable Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to enable? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>





<div class="modal fade" id="modal-ads-enable-alert" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/enable', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Enable Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					This Ad is disabled, do you want to enable? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>




<div class="modal fade" id="modal-watchlist-remove" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'dashboard/remove-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-remove_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Remove Ads</h4>
	        </div>
			<div class="modal-watchlist-body modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12 modal-body">
					Remove Ad from your Watchlist?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="watchlist_id" id="row-watchlist_id">
				<input type="hidden" name="watchlist_type" id="row-watchlist_type">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-plan-subscription" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'subscription/upgrade', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_subscription_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Upgrade Plan</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to Upgrade your Subscription? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="user_id" id="row-user_id" value="">
				<input type="hidden" name="plan_id" id="row-plan_id" value="">
				<button type="submit" class="btn btn-success borderZero modal-plan-subscription" name="months_subscribed" id="row-months" value="6"><i class="fa fa-check"></i> 6 MONTHS</button>
				<button type="submit" class="btn btn-success borderZero modal-plan-subscription" name="months_subscribed" id="row-months" value="12"><i class="fa fa-check"></i> 12 MONTHS</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-form-purchase-banner" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">PURCHASE ADVERTISEMENT</h4>
	        </div>
			<div class="modal-body nobottomPadding">
          <div id="banner-load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
          </div>
          <div id="form-banner_notice"></div>
			{!! Form::open(array('url' => 'banner/payment/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form_banner', 'files' => true)) !!}
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Title</h5>
				    <div class="col-sm-9">
				      <input type="text" class="form-control borderZero" id="row-title" name="title" placeholder="">
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Start Date</h5>
				    <div class="col-sm-9">
				    <!--   <input type="text" class="form-control borderZero" id="" placeholder=""> -->
				          <div class="input-group date" id="row-date_picker_start">
                        {!! Form::text('start_date', null, ['class' => 'form-control borderZero', 'id' => 'row-start_date']) !!}
                        <span class="borderZero input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                      </div>
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Country</h5>
				    <div class="col-sm-9">
				      <select name="country_id" class="form-control borderZero">
				      	@foreach ($countries as $row)
				      		<option value="{{$row->id}}" {{($rows->country == $row->id ? 'selected':'')}}> {{ $row->countryName }}</option>
				      	@endforeach
				      </select>
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Language</h5>
				    <div class="col-sm-9">
				 	 <select name="language" class="form-control borderZero">
				      	@foreach ($languages as $row)
				      		<option value="{{$row->id}}"> {{ $row->languageName }}</option>
				      	@endforeach
				      </select>
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Page</h5>
				    <div class="col-sm-9">
				    <select class="form-control borderZero" name="page" id="row-page">
				    	<option class="hide">Select:</option>
				    	@foreach($banner_placement_pages as $row)
				    		<option value="{{$row->id}}">{{$row->name}}</option>
				    	@endforeach
				    	<!-- <option value="1">Home </option>
				    	<option value="2">Home > Category</option>
				    	<option value="3">Home > Search</option>
				    	<option value="4">Home > Ads > View</option>
				        <option value="5">Home > User Public Page</option>
				        <option value="6">Home > Category > All</option> -->
				    </select>
				    </div>
				  </div>
				  <div class="form-group hide" id="banner-category-pane">
				    <h5 class="normalText col-sm-3" for="email">Category</h5>
				    <div class="col-sm-9">
				<select type="text" class="form-control borderZero" id="ads-category"  name="category">
                  <option class="hide">Select Category</option>
                  @foreach($category as $row)
                      <option value="{{$row->unique_id}}">{{$row->name}}</option>
                  @endforeach
             	</select>
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Placement</h5>
				    <div class="col-sm-9">
				      <select name="banner_placement_id" id="row-banner_placement_id" class="form-control borderZero">
				      		<option value="" selected> Select:</option>
				      </select>
				      <sub class="redText topmarginveryLight hide" id="error-placement-slot">* No availble slot for this placement</sub>
				    </div>

				  </div>
				<!--   <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Duration</h5>
				    <div class="col-sm-9">
				    	<div class="row">
				    		<div class="col-xs-6">
						      <input type="text" class="form-control borderZero" name="banner_duration" id="row-banner-duration" placeholder="">
							</div>
							<div class="col-xs-6">
							  <select class="form-control borderZero" id="sel1">
							    <option>Days</option>
							    <option>Month</option>
							    <option>Years</option>
							  </select>
							</div>
						</div>
				    </div>
				  </div> -->
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Placement Price</h5>
				    <div class="col-sm-9">
				        <select class="form-control borderZero" id="banner_placement_plan_id" name="banner_placement_plan_id" disabled>
            		    </select>
				    </div>
				  </div>
				  <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Expiration Date</h5>
				    <div class="col-sm-9">
				    	 <input type="text" class="form-control borderZero" name="expiration_date" readonly id="row-expiration-date" placeholder="">
				    </div>
				  </div>
				  <div class="row borderBottomDarkLight blockBottom">                  
          </div>
          <div class="form-group">
				    <h5 class="normalText col-sm-3" for="email">Link</h5>
				    <div class="col-sm-9">
				      <input type="url" class="form-control borderZero" name="link" id="row-link" placeholder="">
				    </div>
				  </div>
				  


				  <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"> Image</h5>
               <div class="col-sm-9">
               <div id="preview" class="hidden-xs">
				 <img id="row-img" class="hide bottomPadding img-responsive" src="#" alt="your image" />
				</div>
				<div id="preview" class="center visible-xs">
				 <img id="row-img-mobile" class="hide bottomPadding noMargin img-responsive" src="#" alt="your image" / style="width:300px;height:300px">
				</div>
				<div class="uploader-pane">
			      <div class="change">
			    <div class="input-group input-group error-photo'.$x.' error-photo-type'.$x.'">
			      <input type="text" name="file_name" id="file_name" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
			      <span class="input-group-btn">
			        <label class="btn-file btn blueButton borderZero cursor">



			        Browse <input name="photo" type="file" id="row-photo" data-id="" class="photo btn photoName borderZero form-control borderZero cursor"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*" required="required" value="Upload a Photo">
			        </label> 
			      </span>
			      <span class="input-group-btn">
			        <button data-id="" type="button" class="btn removeImage redButton borderZero leftMargin"><span style="padding: 1px;">Delete</span></button>
			      </span>
			    </div>
			     <sup class="sup-errors redText"></sup>
			  </div>
			  </div>

<!--                 <div class="uploader-pane">
                      <div class="change">
                        <input type="hidden" id="row-photo" name="image">
               <input name="photo" type="file" id="row-photo" class="file form-control borderZero file_photo"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*" required="required" value="Upload a Photo">&nbsp;
                      </div>
                </div> -->
              </div>
              </div>
        </div>
			<div class="modal-footer borderTopDarkB">
				<button type="submit" class="btn btn-sm blueButton borderZero noMargin" id="btnPurchaseBanner">Purchase</button>	
			</div>
			{!! Form::close() !!}	

		</div>
	</div>
</div>




<div class="modal fade" id="modal-make-auction" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/dashboard/listing/save/make-auction', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_make_auction_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle"></h4>
	        </div>
			<div class="panel-body">
				<div id="form-make_auction_notice" class=""></div>
					<div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Bid Limit Amount</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" id="ads-price" name="bid_limit_amount" placeholder="">
		              </div>
		            </div>
		            <div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Bid Start Amount</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" name="bid_start_amount" placeholder="">
		              </div>
		            </div>
		            <div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Minimum Allowed Bid</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" name="minimum_allowed_bid" placeholder="">
		              </div>
		            </div>
					<div class="col-lg-6 col-md-6 col-xs-6 noPadding">
						<div class="form-group">
							<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
							<div class="col-sm-8">
								<input type="text" class="form-control borderZero" name="bid_duration_start" placeholder="" required="required">
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-6 noPadding">
						<div class="form-group">
							<h5 class="col-sm-4 normalText" for="register4-email"> &nbsp;&nbsp;Bid Duration</h5>
							<div class="col-sm-8">
								<select name="bid_duration_dropdown_id" class="form-control borderZero fullSize" required="required">
									<option value="" selected>Select:</option>
									<option value="3">Months</option>
									<option value="2">Weeks</option>
									<option value="1">Days</option>
								</select>
							</div>
						</div>
					</div>	


			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<input type="hidden" name="type_id" id="row-type_id" value="2">
				<button type="submit" class="btn btn-success borderZero"><i class="fa fa-check"></i> Update</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-start-auction" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/dashboard/listing/save/make-auction', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_start_auction_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle"></h4>
	        </div>
			<div class="panel-body">
				<div id="form-start_auction_notice" class=""></div>
					<div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Bid Limit Amount</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" id="ads-price" name="bid_limit_amount" placeholder="">
		              </div>
		            </div>
		            <div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Bid Start Amount</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" name="bid_start_amount" placeholder="">
		              </div>
		            </div>
		            <div class="form-group">
		               <h5 class="col-sm-3 normalText" for="price">Minimum Allowed Bid</h5>
		              <div class="col-sm-9">
		                <input class="form-control borderZero" type="text" name="minimum_allowed_bid" placeholder="">
		              </div>
		            </div>
					<div class="col-lg-6 col-md-6 col-xs-6 noPadding">
						<div class="form-group">
							<h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
							<div class="col-sm-8">
								<input type="text" class="form-control borderZero" name="bid_duration_start" placeholder="" required="required">
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-xs-6 noPadding">
						<div class="form-group">
							<h5 class="col-sm-4 normalText" for="register4-email"> &nbsp;&nbsp;Bid Duration</h5>
							<div class="col-sm-8">
								<select name="bid_duration_dropdown_id" class="form-control borderZero fullSize" required="required">
									<option value="" selected>Select:</option>
									<option value="3">Months</option>
									<option value="2">Weeks</option>
									<option value="1">Days</option>
								</select>
							</div>
						</div>
					</div>	


			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-auction_id" value="">
				<input type="hidden" name="type_id" id="row-type_id" value="2">
				<button type="submit" class="btn btn-success borderZero"><i class="fa fa-check"></i> Update</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-make-basic-ad" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-make-auction-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/dashboard/ad/make-basic-ad', 'role' => 'form', 'class' => 'form-horizontal', 'style'=>'margin-bottom: 0', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Make Basic Ad</h4>
	        </div>
			<div class="modal-body">
				<div id="form-make_baisc_ad_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Are you sure you want to change this to Basic Ad? 
				</div>
				</div>
			</div>
			<div class="modal-footer bordertopLight borderBottom">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn  redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
        	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>





<div class="modal fade" id="modal-send_referral" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/dashboard/referral/send', 'role' => 'form', 'class' => 'form-horizontal', 'style'=>'margin-bottom: 0', 'id' => 'form-fereral_invitation_form', 'files' => true)) !!}

			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-header panelTitle"></h4>
	        </div>
			<div class="panel-body">
				<div id="form-send_referral_invitation_notice" class=""></div>
					<div class="form-group">
		              <div class="col-sm-12 col-lg-12">
                              <select name="email[]" id="example-filterBehavior" multiple="multiple" class="form-control input-md borderzero  multiselect" style="width: 100%;display: inline !important;">
				                @foreach ($contact_lists as $contact_list) 
				               <option value="{{$contact_list->email}}">{{$contact_list->email}}</option>
				                @endforeach
				              </select>
		              </div>
		            </div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<input type="hidden" name="type_id" id="row-type_id" value="2">
				<button type="submit" class="btn btn-success borderZero"><i class="fa fa-check"></i> Send</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Cancel</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- Modal -->
<div id="inviteUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content borderZero">
     <div id="load-form" class="loading-pane hide">
    	<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
     </div>
      <div class="modal-header modalTitleBar">
    <button type="button" class="close closeButton" data-dismiss="modal">×</button>
      <h4 class="modal-title panelTitle">INVITE USERS</h4>
    </div>
      <div class="modal-body clearfix">
      	<div class="col-sm-12 bottomPaddingC noPadding">
      		<div class="row">

	        <div class="col-sm-4" style="padding-bottom:2px;"> <a href="{{url('/').'/referral/import'}}"<button type="submit" class="btn redButton borderZero fullSize">IMPORT GMAIL</button></a></div>
	        <div class="col-sm-4" style="padding-bottom:2px;"><button type="button" class="btn redButton borderZero fullSize">IMPORT HOTMAIL</button></div>
	        <div class="col-sm-4" style="padding-bottom:2px;"><button type="button" class="btn redButton borderZero fullSize">IMPORT YAHOO</button></div>
	    </div>
	    </div>
{{-- 	    <div class="row" style="padding-top: 50px !important;">
	    	<div class="borderTopDarkB topPaddingB"></div>
	    </div> --}}
	   	{!! Form::open(array('url' => 'user/dashboard/referral/send', 'role' => 'form', 'class' => 'form-group', 'style'=>'margin-bottom: 0', 'id' => 'modal-send-referral', 'files' => true)) !!}
	   	<div class="col-md-12">
		  <div class="form-group text-center">
			<label class="radio-inline"><input type="radio" id="contact_src" value="auto" checked name="optradio">Select from Contacts</label>
		    <label class="radio-inline" style="padding-right: 59px;"><input type="radio" id="contact_src" value="manual" name="optradio">Manual Input</label>
		  </div>
		</div>
		<div class="col-md-12 col-xs-12 col-sm-12">
	      <div class="form-group hide" id="manual-pane">
		    <span for="email" class="grayText mediumText">Email Address</span>
			<input type="text" name="email[]" id="referred_email" class="borderZero inputBox fullSize">
		  </div>
		  </div>
		  <div class="col-md-12 col-xs-12 col-sm-12">
		  <div class="" id="auto-pane">
		    <span for="email" class="grayText mediumText">Email Address</span>
		   	 <select name="email[]" id="row-email_id" multiple="multiple" class="input-md borderzero email_id multiselect" style="width: 100%; border-color: #ccc;border: none !important; border-radius: 0px !important;">
		   	 	@foreach($contact_lists as $row)
                  <option value="{{ $row->id }}">{{$row->email}}</option>
                @endforeach
		   	 </select>	
		  </div>
		  </div>
		  <div class="col-md-12">
		  <div class="form-group">
		    <span for="email" class="grayText mediunText">Message</span>
		    <textarea class="form-control borderZero inputBox fullSize topMarginB" rows="5" id="comment"></textarea>
		  </div>
		  </div>
      </div>
      	<!-- <div class="borderTopDarkB"></div> -->
      <div class="clearfix"></div>
      <div class="modal-footer borderTopDarkB">
      		<input type="hidden" id="contact-type" name="contact_type" value="1">
            <button type="submit" class="btn blueButton borderZero">SEND</button>
      </div>
    </div>
    </div>
   	{!! Form::close() !!}
</div>

<div id="purchaseBid" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content borderZero">
      <div class="modal-header modalTitleBar">
    <button type="button" class="close closeButton" data-dismiss="modal">×</button>
      <h4 class="modal-title panelTitle">Purchase Bid</h4>
    </div>
      <div class="modal-body">
        <p>Would you like to purchase bid points?</p>
      </div>
      <div class="modal-footer">
      	<button type="button" class="btn blueButton borderZero" data-dismiss="modal">Yes</button>
        <button type="button" class="btn redButton borderZero" data-dismiss="modal">No</button>
      </div>
    </div>

  </div>
</div>
