@extends('layout.frontend')
@section('scripts')
<script>


$_token = "{{ csrf_token() }}";
function refresh(){
	 var id = "{{ $user_id }}";
	 var loading = $(".loading-pane");
	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
   console.time('jquery');
  
	 $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
	 	$.each(response.rows, function(index, value) {
	 		var field = $("#client-" + index);
            console.log(index+"="+value);
 			    if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	 $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }
            if(index == "date_of_birth"){
              $("#client-date_of_birth").val(moment(value).format('MM/DD/YYYY'));
            }
            if(index == "company_start_date"){
              $("#client-company_start_date").val(moment(value).format('MM/DD/YYYY'));
            }
           	if(index == "usertype_id"){
           		if(value == "1"){
           			$("#accountBranchContainer").addClass("hide");
                $("#founded-date-pane").addClass("hide");
                $("#location-pane").removeClass("hide");
           		}else{
           			$("#accountBranchContainer").removeClass("hide");
                $("#founded-date-pane").removeClass("hide");
                $("#location-pane").addClass("hide");
           		}
           	}
       
            if(index == "alias"){
              $("#vanity-url").attr("href","{{URL::to('/')}}"+value);
              $("#customer-alias").val(value);
            }
            if(index == "country"){
               $("#client-country").html(response.user_country);
            }
            if(index == "city"){
              $("#client-city").html(response.user_city);
            }
            console.timeEnd('jquery');
          }); 

      }, 'json');
  
	 loading.addClass("hide");
	}
  function refreshSettings(){
   var id = "{{ $user_id }}";
   var loading = $(".loading-pane");
   loading.removeClass("hide");
   var url = "{{ url('/') }}";
   $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
    $.each(response.rows, function(index, value) {

            if(index == "usertype_id"){
              if(value == "1"){
                $("#accountBranchContainer").addClass("hide");
                $("#founded-date-pane").addClass("hide");
                $("#location-pane").removeClass("hide");
              }else{
                $("#accountBranchContainer").removeClass("hide");
                $("#founded-date-pane").removeClass("hide");
                $("#location-pane").addClass("hide");
              }
            }
            if(index == "alias"){
              $("#vanity-url").prop("href","{{URL::to('/')}}"+value);
              $("#customer-alias").val(value);
            }
            if(index == "country"){
               $("#client-country").html(response.user_country);
            }
            if(index == "city"){
              $("#client-city").html(response.user_city);
            }
            
          }); 

      }, 'json');
   loading.addClass("hide");
  }

    function refreshWatchlist(){
          $_token = "{{ csrf_token() }}"; 
          $(".watchlist-container").addClass("hide");
          var panel_watchlists = $("#watchlist-panel");
          panel_watchlists.html(" ");
            $.post("{{ url('user/dashboard/watchlist/refresh') }}", { _token: $_token }, function(response) {
              panel_watchlists.html(response);
            }, 'json');       
    }

    function refreshVendorAdsActiveList(){
          $("#form-make_auction_notice").addClass("hide");
          var ads = $("#listings");
          ads.html(" ");
            $.post("{{ url('user/dashboard/listing/active/refresh') }}", { _token: $_token }, function(response) {
              ads.html(response);
            }, 'json');    
           
    }
    $("#ad-listings").on("click", function() {
        var listing = $("#listings");
        listing.html(" ");
          $.post("{{ url('user/dashboard/listing/active/refresh') }}", { _token: $_token }, function(response) {
            listing.html(response);
          }, 'json');
      
          
    });
    function refreshVendorAuctionActive(){
          $_token = "{{ csrf_token() }}";
          var auctionRefresh = $("#auctionAds");
          auctionRefresh.html(" ");
            $.post("{{ url('user/dashboard/auction/active/refresh') }}", { _token: $_token }, function(response) {
              auctionRefresh.html(response);
            }, 'json');    
    }
    $("#auction-listings").on("click", function() {
        var auction = $("#auctionAds");
        auction.html(" ");
          $.post("{{ url('user/dashboard/auction/active/refresh') }}", { _token: $_token }, function(response) {
            auction.html(response);
          }, 'json');
    });
      
    function refreshMessages(){
      var url = "{{ url('/') }}";
      var id = $("#row-message_id").val();
      var container = $("#message_container");
      $.post("{{ url('user/send/message/vendor/refresh') }}", { id: id, _token: $_token }, function(response) {
            container.html(response.message_content);
        }, 'json');

    }
    function refreshUserBanner(){
      var url = "{{ url('/') }}";
      var loading = $("#banner-pane").find('#load-form');
      loading.removeClass("hide");
      var container = $("#banner-container");
      $.post("{{ url('user/banner/refresh') }}", {_token: $_token }, function(response) {
            container.html(response.banner);
            if(response.banner != ""){
              $('#advertising-indicator').addClass("hide");
            }else{
              $('#advertising-indicator').removeClass("hide");
            }
            loading.addClass("hide");
      },'json');
  
    }
    function refreshMessageContainer(){
      var url = "{{ url('/') }}";
      var container = $("#message-tbody");
      $("#message_container").html("");
      $("#modal-send-message").addClass("hide");
      $.post("{{ url('user/message/refresh') }}", {_token: $_token }, function(response) {
            container.html(response.messages_header);
      }, 'json');
    }

    function refreshNewMeesage(){
      var url = "{{ url('/') }}";
      var container = $("#message-tbody");
      $("#message_container").html("");
      $("#modal-send-message").addClass("hide");
      $.post("{{ url('user/message/newMessage') }}", {_token: $_token }, function(response) {
            container.html(response.messages_header);
      }, 'json');
    }

    function purchaseBannerPlacement(){
      var url = "{{ url('/') }}";
      var title = $("#row-title").val();
      $.get("{{ url('banner/payment') }}", { title:title, _token: $_token}, function(response) {
        }, 'json');
       window.location.assign('banner/payment');
    }

    $("#content-wrapper").on("click", ".btn-delete-ads", function() {
    	  var id = $(this).data('id');
    	  var title = $(this).data('title');
    	  $("#row-id").val("");
  		  $("#row-id").val(id);
  		  $("#row-title").val(title);
  		  $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Delete Ad '+ title +' </strong>', title);
        $("#modal-ads-delete").modal('show');  
    });
    $('#banner_placement_plan_id').change(function() {
        var start_date = $("#row-start_date").val();
        var plan_id = $(this).val();
        $.post("{{ url('generate-expiration-date') }}", { plan_id: plan_id,start_date:start_date, _token: $_token }, function(response) {
          $("#row-expiration-date").val(response.generated_expiration_date);
        }, 'json');
       
    });
     $('#row-page').change(function() {
        var id = $(this).val();
        if(id == 1){
          $("#banner-category-pane").addClass("hide");
        }else{
          $("#banner-category-pane").removeClass("hide");
        }
        var banner_placement = $("#row-banner_placement_id");
        $.post("{{ url('get-available-placement-plan') }}", { id:id, _token: $_token }, function(response) {
          banner_placement.html(response.rows);
        }, 'json');
    });
    function computeDuration(){
      var duration = $("#row-featured_duration").val();
      var duration_metric  = $("#row-featured_duration_metric").val();
      var payment_method  = $("#row-payment_method").val();
      var id  = $("#modal-feature-ads").find("#row-id").val();
      var title  = $("#modal-feature-ads").find("#row-title").val();

        $("#total-point-cost").text('');
        $.post("{{ url('exchange-point-checker') }}", { payment_method:payment_method,duration: duration,duration_metric:duration_metric,_token :$_token }, function(response) {
            if(payment_method == 1){
               $("#payment_unit").html("points");
               $("#current_points").removeClass("hide");
               $("#buttons_for_reward_points").removeClass("hide");
               $("#buttons_for_paypal").addClass("hide");
            }else{

               var url = "{{url('payment/ad-feature').'/'}}"+'1016'+'/'+id+'/'+duration+'/'+duration_metric;
               $("#return_url").val(url);
               $("#row-title").val(title);
               $("#payment_unit").html("USD");
               $("#current_points").addClass("hide");
               $("#buttons_for_reward_points").addClass("hide");
               $("#buttons_for_paypal").removeClass("hide");

            }
          if(response.status == 1){
             $("#total-point-cost").text(response.computed_cost);
             $("#feature-error").addClass("hide");
             $("#btnFeature").removeAttr("disabled","disabled");
             $("#row-cost_points").val(response.computed_cost);
          }else{
             $("#btnFeature").attr("disabled","disabled");
             $("#feature-error").removeClass("hide");
             $("#total-point-cost").text(response.computed_cost);
          } 
          if($("#row-cost_points").val() == 0){
            $("#btnPaypalFeature").addClass("hide"); 
          }else{
            $("#btnPaypalFeature").removeClass("hide");
            $("#paypal-item-name").val("Feature Ad -"+title);
            $("#paypal-amount").val(response.computed_cost);
          }
         
        }, 'json');
    } 
    $("#content-wrapper").on("click", ".btn-enable-ads", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Enable Ads '+ title +' </strong>', title);
        $("#modal-enable-ads").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-ads-alert", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Disable Ad '+ title +' </strong>', title);
        $("#modal-ads-disable").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-ads-alert-list", function() {
      console.log(id);
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-disable-id").val("");
        $("#row-disable-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Disable Ad '+ title +' </strong>', title);
        $("#modal-ads-disable-list").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-auction-alert", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>End Auction '+ title +' </strong>', title);
        $("#modal-auction-disable").modal('show');    
    });

    $('#accordion a').click(function() {
        $(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
    });
    $(function () {
        $('#row-date_picker_start').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
        $("#message-pane").addClass("hide");
        $("#content-wrapper").find(".readonly-view").attr("disabled","disabled");
    });
        $(function () {
        $('#row-date_picker_end').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY h:MM A'
        });
    });
   @if(Auth::user()->status == 3)
    dialogMessage('Yakolak', '<strong>Your account has been mark as spam if you believe this is a mistake kindly contact us</strong>');
   @endif
    @if(Auth::user()->status == 2)
    dialogMessage('Yakolak', '<strong>Your account is not yet verified.</strong>');
   @endif
   $(".btn-remove-message-dialog").click(function () {
          $("#modal-dialog-message").modal("hide");
   });
   $("#content-wrapper").on("click", "#btn-change-mode", function() {

      if($(this).text() == "Edit"){
        $("#content-wrapper").find("form").find(".readonly-view").removeAttr("disabled","disabled");
        $("#customer-alias").removeAttr("disabled","disabled");
        $("#btn-save_button").removeClass("hide");
        $(this).text("Read-only");
      }else{
        $(this).text("Edit");
        $("#content-wrapper").find("form").find(".readonly-view").attr("disabled","disabled");
        $("#customer-alias").attr("disabled","disabled");
        $("#btn-save_button").addClass("hide");
      }
   });
   $("#content-wrapper").on("click", ".btn-enable-ads-alert", function() {
    	  var id = $(this).data('id');
    	  var title = $(this).data('title');
    	  $("#row-id").val("");
		    $("#row-id").val(id);
		    $("#row-title").val(title);
		    $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Enable Ad '+ title +' </strong>', title);
        $("#modal-ads-enable-alert").modal('show');    
  });
  
  $("#content-wrapper").on("click", ".btn-purchase-ads", function() {
    $("#modal-form-purchase-banner").find("form")[0].reset();
    $("#modal-form-purchase-banner").find(".alert").remove();
    $("#modal-form-purchase-banner").modal("show");
  });  
  $("#content-wrapper").on("click", ".btn-send-msg", function() {
    var id = $(this).data('id');
    var title = $(this).data('title');
    $("#modal-send-msg").modal("show");
    $("#row-reciever_id").val(id);
    $("input#row-title").val("Inquiry for "+title);
  });
  
	 $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });
  $('.btn-remove').on('click', function() {
      var id = {{ $rows->id }};
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Remove Photo', 'Are you sure you want to remove your photo</strong>?', "{{ url('profile/photo/remove') }}", id);
  });

   $("#content-wrapper").on('click','.btn-purchase-bid', function() {
      var id = $(this).data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Purchase Bid', 'Do you to purchase this bid point?</strong>?', "{{ url('bid/purchase') }}", id);
  });

  $("#content-wrapper").on("click", "#btn-feature", function() {
    var id = $(this).data('feature-id');
    var title = $(this).data('ad-title');
    $('#modal-feature-ads').find("#row-id").val(id);
    $('#modal-feature-ads').find("#row-title").val(title);
    $('#modal-feature-ads').modal("show"); 
      // $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      // dialog('Feature Ad', 'Make this a feature ad</strong>?', "{{ url('ad/feature') }}", id);
  });
  $("#content-wrapper").on("click", "#btn-unfeature", function() { 
      var id = $(this).data('feature-id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnFeature Ad', 'Unfeature this ad</strong>?', "{{ url('ad/unfeature') }}", id);
  });
  $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $("#get_tel_prefix").val("");
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
            $("#client-tel_no_prefix").val(response.tel_prefix);
            $("#client-mobile_no_prefix").val(response.tel_prefix);
          }, 'json');
       }
  });
$("#client-geo-country").on("change", function() { 
        var id = $("#client-geo-country").val();
        $("#get_tel_prefix").val("");
        $_token = "{{ csrf_token() }}";
        var city = $("#client-geo-city");
        city.html(" ");
        if ($("#client-geo-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
            $("#client-tel_no_prefix").val(response.tel_prefix);
            $("#client-mobile_no_prefix").val(response.tel_prefix);
          }, 'json');
       }
  });
$("#client-bid-point-package").on("change", function() { 
        $_token = "{{ csrf_token() }}";
        var id = $(this).val();
        var container = $("#paypal-button-container");
        container.html(" ");
        if ($(this).val()) {
          $.post("{{ url('bid/payment') }}", { id: id, _token: $_token }, function(response) {
            container.append(response.form);
         
          }, 'json');
       }
  });
 $("#accountBranchContainer").on("change", "#account-company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().parent().parent().find("#account-company_city");

        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
 

 var branchAccount = $("#accountBranch").html();
 $(".btn-add-account-branch").click(function() {
       $("#accountBranchContainer").append("<div id='accountBranch'>"+branchAccount+"</div>");
       var country = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_country").val();
       var city = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_city").html();
       $("#branchContainer").find('#accountBranch:last-child').find("#account-company_country").val(country);
       $("#branchContainer").find('#accountBranch:last-child').find("#account-company_city").html(city);
       $("#accountBranchContainer").find('#accountBranch:last-child').find(".btn-add-account-branch").removeClass("btn-add-account-branch").addClass("btn-del-account-branch").html("<i class='fa fa-minus redText'></i>");
 	   $("#accountBranchContainer").find('#accountBranch:last-child input:text').val("");
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#branch_id").prop("value","0");
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_country option:eq(0)").attr('selected','selected');
 	   $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_city option:eq(0)").attr('selected','selected');     	
 });

 $("#accountBranchContainer").on("click", ".btn-del-account-branch", function() { 
   var current =  $(this).parent().parent().parent();
   var branch_id = current.find('#branch_id').val();
   current.find('#branch_status').val("2")
   current.addClass("hide");
 });
  

  $("#nav div a[href^='#']").on('click', function(e) {
   // prevent default anchor click behavior
   e.preventDefault();
   // store hash
   var hash = this.hash;
   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top - 100
     }, 1000, function(){
       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });
    });

    $("#replyBlock").hide();
    $(".btn-message-back").hide();
    $(".btn-message-back").on("click", function() { 
            $("#messageBlock").show();
            $("#replyBlock").hide();
            $(".btn-message-back").hide();          
    });
    


$("#content-wrapper").on("click", ".view-message", function() {
      // location.reload("{{$messages_counter}}");
      // $('#message-count-container').load("{{$messages_counter}}");
      $("#messageBlock").hide();
      $(".btn-message-back").show();
      $("#replyBlock").show();
      var id = $(this).data('header-id');
      $("#row-message_id").val(id);
      $_token = "{{ csrf_token() }}";
      $("#message-pane").removeClass("hide");
      $("#modal-send-message").removeClass("hide");
      $(this).find('i').removeClass('fa-envelope').addClass('fa-envelope-o');
      var container = $("#message_container");
      var container_title = $("#message-title-pane");
      $('#message-pane').find(".loading-pane").removeClass("hide");
      container.html("");
      $('#messageInboxPane').addClass('bottomPaddingC').removeClass('nobottomPadding');
      $.post("{{ url('user/messages') }}", { id: id, _token: $_token }, function(response) {
      container.html(response.message_content);
      container_title.text(response.message_title);
      $('#message-pane').find(".loading-pane").addClass('hide');
    }, 'json');
      $('#view_more_inbox').hide();
      $(this)
     $.post("{{ url('user/send/message/vendor/status') }}", { id: id, _token: $_token }, function(response) {
      $("#message-count-container").text(response.updated_count);
      
     }, 'json');

}); 
      $("#myTableMessage").on("click", "#delete-message", function() {
          var id = $(this).data('id');
          $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
          dialog('Delete Message', 'Are you sure you want to delete this message</strong>?', "{{ url('user/message/delete') }}", id);
          
      }); 

      $('#back_button_inbox').click(function(){
        $('#view_more_inbox').show();
        $('#messageInboxPane').removeClass('bottomPaddingC').addClass('nobottomPadding');

      });

      $("#content-wrapper").on("click", ".btn-plan-subscription-upgrade", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        var plan_id = $(this).data('plan');
        var user_id = $(this).data('user_id');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#row-plan_id").val(plan_id);
        $("#row-user_id").val(user_id);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong> Upgrade to '+ title +' </strong>', title);
        $("#modal-plan-subscription").modal('show');
      });

      $("#row-banner_placement_id").on("change", function() { 
        var id = $(this).val();
        if(id == 5 || id == 6 || id == 7){
           $.post("{{ url('get-available-banner-slot') }}", { id: id, _token: $_token }, function(response) {
              if(response == 1){
                $("#error-placement-slot").addClass("hide");
                $("#btnPurchaseBanner").removeAttr("disabled","disabled");
              }else{
                $("#error-placement-slot").removeClass("hide");
                $("#btnPurchaseBanner").attr("disabled","disabled");
              }
          }, 'json');
        }
        $("#banner_placement_plan_id").removeAttr("disabled", "disabled");
      });  


     $("#row-banner_placement_id").on("change", function() { 
        var id = $("#row-banner_placement_id").val();
        $_token = "{{ csrf_token() }}";
        var banner = $("#banner_placement_plan_id");
        banner.html(" ");
        if ($("#row-banner_placement_id").val()) {
          $.post("{{ url('get-bannerplacementplans') }}", { id: id, _token: $_token }, function(response) {
            banner.append(response);
          }, 'json');
             }
      });

  $("#client-usertype_id").on("change", function() { 
      if($(this).val() == 1){
          $("#account-setting").find(".individual-pane-one").removeClass("hide");
          $("#individual-pane-two").removeClass("hide");
          $("#accountBranchContainer").addClass("hide");
          $("#account-setting").find(".founded-date-pane").addClass("hide");
        
      }else if($(this).val() == 2){
          $("#account-setting").find(".individual-pane-one").addClass("hide");
          $("#individual-pane-two").addClass("hide");
          $("#accountBranchContainer").removeClass("hide");
          $("#account-setting").find(".founded-date-pane").removeClass("hide");
        
      }
  });

  $("#inviteUser").on("click", "#contact_src", function() {
    $(this).find("form").find("#row-email_id").multiselect('refresh');
    if( $(this).val() =="auto"){
      $("#auto-pane").removeClass("hide");
      $('#manual-pane').addClass("hide");
      $('#auto-pane').find("#row-email_id").attr('name','email[]');
      $("#contact-type").val("1");
    }else{
      $("#auto-pane").addClass("hide");
      $('#manual-pane').removeClass("hide");
      $('#manual-pane').find("input[type=text]").attr('name','email[]');
      $("#contact-type").val("2");
    }
  });
  $("#content-wrapper").on("click", ".btn-banner-view", function() {
        var id = $(this).parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
              $.post("{{ url('dasboard/banner/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Ads ['+response.title+']</strong>');
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);                
                          }
                      });
                      $("#row-ads_type_id").attr('disabled','disabled');
                      $("#row-ads_category").attr('disabled','disabled');
                      $("#row-title").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-countryName").attr('disabled','disabled');
                      $("#row-city").attr('disabled','disabled');
                      $("#row-address").attr('disabled','disabled');
                      $("#row-youtube").attr('disabled','disabled');
                      $("#row-price").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form-purchase-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');  
      });



 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
   });
 });

 $("#content-wrapper").on("click", ".btn-make-auction", function() {
    var ads_id = $(this).data('ads_id');
    var title = $(this).data('title');
    $("#row-ads_id").val(ads_id);
    $("#row-title").val(title);
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
 
    $.post("{{ url('user/dashboard/listing/form/make-auction') }}", { ads_id: ads_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-title").html('<strong>Make Auction  '+ title +' </strong>', title);
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
                  if(index=="price"){
                    $("#ads-price").attr('value', +value);
                 }
              });
              // show form
               $("#modal-form").find('form').trigger("reset");
               $("#modal-make-auction").modal('show');

            
          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });

 $("#content-wrapper").on("click", ".btn-start-auction", function() {
    var ads_id = $(this).data('ads_id');
    var title = $(this).data('title');
    $("#row-auction_id").val(ads_id);
    $("#row-title").val(title);
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
 
    $.post("{{ url('user/dashboard/listing/form/make-auction') }}", { ads_id: ads_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-title").html('<strong>Make Auction  '+ title +' </strong>', title);
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
                  if(index=="price"){
                    $("#ads-price").attr('value', +value);
                 }
              });
              // show form
               $("#modal-form").find('form').trigger("reset");
               $("#modal-start-auction").modal('show');

          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });




 $("#content-wrapper").on("click", ".btn-remove-watchlist",function() { 
        var watchlist_id = $(this).parent().parent().parent().data('watchlist_id');
        var watchlist_type = $(this).parent().parent().parent().data('watchlist_type');
        var title = $(this).parent().parent().parent().data('title');
        var btn = $(this);
        $("#row-watchlist_id").val(watchlist_id);
        $("#row-watchlist_type").val(watchlist_type);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        if(watchlist_type == 1 || watchlist_type == 3) {
           $(".modal-title").html('<i class="fa fa-trash"></i> <strong>Remove Ads ['+ title +']</strong>');
           $(".modal-watchlist-body").html('Are you sure you want to remove this ad?');
        } else if (watchlist_type == 2) {
           $(".modal-title").html('<i class="fa fa-trash"></i> <strong>Remove Profile ['+ title +']</strong>');
           $(".modal-body").html('Are you sure you want to remove this profile?');
        }
        $("#modal-watchlist-remove").modal('show');
        // User Id Disabled
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
              $.post("{{ url('dashboard/favorite-profile/manipulate') }}", { watchlist_type: watchlist_type, watchlist_id: watchlist_id, _token: $_token }, function(response) {
                  if(!response.error) {
                     // output form data
                      $.each(response, function(index, value) {
                          var field = $("#watch-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);                
                          }
                      });
                      // show form
                     
                  } else {
                      status(false, response.error, 'alert-danger');
                  }
                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');  
      });



$("#content-wrapper").on("click", ".btn-send-a-referral", function() {
    var user_id = $(this).data('user_id');
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $.post("{{ url('user/dashboard/referral/manipulate') }}", { user_id: user_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-header").html('<strong>Send Referral  </strong>');
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
              });
              // show form
              $("#modal-send_referral").find('form').trigger("reset");
              $("#modal-send_referral").modal('show');


          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });

    function sortByDate(){

      var url = "{{ url('/') }}";
      var loading = $("#messageInboxPane").find("#load-form");
      loading.removeClass('hide');
      var field = 'created_at';
      var order = $("#msg-sort").val();
      var container = $("#message-container");
      console.log(field+"="+container);
      $.post("{{ url('user/message/refresh') }}", { field:field,order:order,_token: $_token }, function(response) {
            container.html(response.messages_header);
             loading.addClass('hide');
        }, 'json');
       if($("#msg-sort").val() == "asc"){
        $("#msg-sort").val("desc");
        $("#sort-indicator").removeClass("fa-sort-amount-asc").addClass("fa-sort-amount-desc");
       }else{
        $("#msg-sort").val("asc");
        $("#sort-indicator").removeClass("fa-sort-amount-desc").addClass("fa-sort-amount-asc");
       }
    }
    function sortByStatus(){
      
      var url = "{{ url('/') }}";
      var loading = $("#messageInboxPane").find("#load-form");
      loading.removeClass('hide');
      var field = 'read_status';
      var order = $("#msg-sort").val();
      var container = $("#message-container");
      $.post("{{ url('user/message/refresh') }}", { field:field,order:order,_token: $_token }, function(response) {
            container.html(response.messages_header);
             loading.addClass('hide');
        }, 'json');


       if($("#msg-sort").val() == "asc"){
        $("#msg-sort").val("desc");
        $("#sort-indicator").removeClass("fa-sort-amount-asc").addClass("fa-sort-amount-desc");
       }else{
        $("#msg-sort").val("asc");
         $("#sort-indicator").removeClass("fa-sort-amount-desc").addClass("fa-sort-amount-asc");
       }
    }
     $('#email_id').multiselect();
     $('#row-email_id').multiselect({
      maxHeight: 400,
      enableCaseInsensitiveFiltering: true,
      selectAllValue: 'multiselect-all',
      selectAllText: 'Select All',
      includeSelectAllOption: true,
      numberDisplayed: 2
    });
    $("#referred_email").select2({
      minimumResultsForSearch: Infinity,
      tags: true,
      tokenSeparators: [',', ' ']
    });
  

google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Page and Ads Visits',     {{ $no_of_page_and_ad_visits }}],
          ['Reviews',      {{ $no_of_reviews }}],
          ['Comments placed for the ads',  {{ $no_of_comments_for_ads }}],
          ['Comments placed for the users', {{ $no_of_comments_for_user }}],
        ]);

        var options = {
          pieSliceText: 'value'
          ,colors: ['#3498DB', '#5DADE2', '#85C1E9', '#AED6F1']
          ,fontSize: 16
          ,chartArea:{left:20,top:0,width:"100%",height:"100%"}
          ,height: 350
          ,width: 350
          ,legend: 'none'
          ,pieHole: 0.4
          ,tooltip : {
          trigger: 'none'
          }
};

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

      /* CLICK LINKS START */
    var url = window.location.href;
    var hash = url.substring(url.indexOf('#'));
    $(document).ready(function(){
      if(hash == "#messages"){
          $("#messages-link").click();
         }else if(hash== "#listings"){
           $("#listings-link").click();
         }else if(hash== "#watchlist"){
           $("#watchlist-link").click();
         }else if(hash== "#settings"){
           $("#settings-link").click();
         }else if(hash== "#auction"){
           $("#auction-link").click();
         }else if(hash== "#subscription"){
           $("#subscription-link").click();
         }else if(hash == "#comments"){
           $(".comClicked").click();
         }else if(hash == "#reports"){
           $(".repClicked").click();
         }else if(hash == "#rewards"){
           $(".rewClicked").click();
         }else if(hash == "#alerts"){
           $(".aleClicked").click();
         }else if(hash == "#invoice"){
           $(".invClicked").click();
         }else if(url){
           $("#account-link").click();
         }
        function mobileSidebarFold(){
            $("#mobilesidebarAccount").removeClass("active");
            $("#mobilesidebarAccount").find('nav').removeClass("active");
            $("#wrapper").removeClass("active");
        }
        $(".mesClick").click(function() {
           $(".mesClicked").click();
            mobileSidebarFold();
        });
        $(".aleClick").click(function() {
           $(".aleClicked").click();
            mobileSidebarFold();
        });
        $(".adlClick").click(function() {
           $("#listings-link").click();
            mobileSidebarFold();
        });
        $(".aucClick").click(function() {
           $("#auction-link").click();
            mobileSidebarFold();
        });
        $(".comClick").click(function() {
           $(".comClicked").click();
             mobileSidebarFold();
        });
        $(".repClick").click(function() {
           $(".repClicked").click();
             mobileSidebarFold();
        });
         $(".setClick").click(function() {
           $("#settings-link").click();
             mobileSidebarFold();
        });
         $(".rewClick").click(function() {
           $(".rewClicked").click();
             mobileSidebarFold();
        });

        $("#messages-id").click(function() {
           $("#messages-link").click();
        });
        $("#listings-id").click(function() {
           $("#listings-link").click();
        });
        $("#auclistings-id").click(function() {
           $("#auction-link").click();
        });
        $("#subscription-id").click(function() {
           $("#subscription-link").click();
        });
        $("#watchlist-id").click(function() {
           $("#watchlist-link").click();
        });
         $("#settings-id").click(function() {
           $("#settings-link").click();
        });

        var id = $("#client-bid-point-package").val();
        var container = $("#paypal-button-container");
        container.html(" ");
        if ($("#client-bid-point-package").val()) {
          $.post("{{ url('bid/payment') }}", { id: id, _token: $_token }, function(response) {
            container.append(response.form);
          }, 'json');
       }
    });

    
   /* CLICK LINKS END */
    // $(".").on("click", function(event){
    //     $("#row-photo_upload").val("");
    //     $("#file_name").val("");
    // });

//   $("#panel-profile").on("click", ".btn-img-delete", function() { 
//   $(".modal-header").removeAttr("class").addClass("modal-header modalTitleBar");
//   dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Delete Profile Picture</strong></h4>', 'Are you sure you want to delete your profile picture?');
//   $.post("{{ url('profile/photo/remove') }}",{id:id, _token: $_token}, function(response) {
//           console.log(response);
//          // $("#profile-photo").attr('src', url + '/uploads/' ++ '?' + new Date().getTime());  
//     }, 'json');
  
// });

  $("#panel-profile").on("click", ".btn-img-delete", function() { 
      var id = {{Auth::user()->id}};
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Delete Profile Picture</strong></h4>', 'Are you sure you want to delete your profile picture?', "{{ url('profile/photo/remove') }}", id);
  });

   /* Disabled Input, Textarea, and Dropdowns set to cursor 'default' START */
    $("input.readonly-view, select.readonly-view, textarea.readonly-view, label.readonly-view, label.radio-inline").css("cursor","default");
   /* Disabled Input, Textarea, and Dropdowns set to cursor 'default' END */
   // /* White background page loader to prevent broken pages from showing - START*/
   //  $(window).load(function() {
   //    $(".loader").fadeOut("slow");
   //  });
   // /* White background page loader to prevent broken pages from showing - END*/
   @if(Auth::check())
    var id = "{{Auth::user()->id}}";
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city-for-dashboard') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
    var photo = "{{$rows->photo}}";
    if(photo == "default_image.png"){
      // $(".btn-img-delete").attr('disabled','disabled')
      $(".btn-img-delete").addClass('grayButtonB')
    }else{
      // $(".btn-img-delete").removeAttr('disabled','disabled')
      $(".btn-img-delete").removeClass('grayButtonB')
    }  
   @endif

   var plan_expiry = '{{$rows->plan_expiration}}';
   var user_plan = '{{$rows->user_plan_types_id}}';
   var current_time = "<?php $date = new DateTime(); echo $date->format('Y-m-d H:i:s'); ?>";
   var new_plan_expiry = new Date(plan_expiry);
   var new_current_time = new Date(current_time);
   var time_diff = new_plan_expiry - new_current_time;
   var time_diff_in_seconds = time_diff / 1000;
     if(user_plan != 1 || user_plan != 5  || plan_expiry != ''){
     /* SUBSCRIPTION */
      $('.planIdentify').countdown(new_plan_expiry)
      .on('update.countdown', function(event){
        var format = '%-H h and %-M min';
          if(event.offset.totalDays > 0){
            format = '%-d day%!d';
          }else{
            format = 'Upgrade Plan';
          }
          if(event.offset.weeks > 0){
            format = '%-m month%!m';
          }else{
            format = 'Upgrade Plan';
          }
        $(this).text('('+event.strftime(format)+')');
          if(event.offset.totalDays > 0){
            $('.durationPlan').text('');
          }else{
            format = 'Upgrade Plan';
          }
          if(event.offset.weeks > 0){
            $('.durationPlan').text(event.strftime(format));
          }else{
            $('.durationPlan').text('');
          }
        })
      .on('finish.countdown', function(event) {
        $(this).html('(Upgrade Plan)');
        $('.durationPlan').text('Expired');
      });
     /* SUBSCRIPTION */
     $('a#subscription-link').click(function(){
       if(time_diff_in_seconds < 604800){ //604800 seconds = 7 days
        $('.expiry-countdown').countdown(new_plan_expiry)
          .on('update.countdown', function(event){
            var format = '%-H h and %-M min';
            if(event.offset.totalDays > 0){
              format = '%-d day%!d';
            }
            if(event.offset.weeks > 0){
              format = '%m week%!w';
            }
            $(this).text('Your plan will expire in '+event.strftime(format));
            })
          .on('finish.countdown', function(event) {
            $(this).html('Plan expired!');
          });
        var array = $('.plan_discount').map(function(){
               return $.trim('<div class="col-sm-4"><div class="panel panel-default borderZero removeBorder nobottomMargin"><div class="panel-heading planTitleBar borderbottomPlan text-center discounts_contain"><span class="panelTitle blueText plan_name">'+$(this).data('plan_name')+'</span></div><div class="panel-body "><div class="xlargeText xxlargeText blueText text-center"><b class="plan_price">$'+$(this).data('plan_price')+'/mo</b></div></div></div></div>');
            }).get();
        $('div.discounted_container').html(array.join(''));
        var discount_desc = $('div.discount_description').data('discount_description');
        console.log(discount_desc);
        $('div.discounts_contain').children().each(function(){
            $("span.plan_name:contains('{{strtoupper($current_user_plan_type)}}')").parent().parent().remove();
            // $("span.plan_name:contains('BRONZE')").remove();
        });
        $('#modalExpiration').modal('show');
        $("button.yesUpgrade").click(function(){
        $('html, body').animate({
            scrollTop: $("#subscription-upgrade").offset().top
            }, 320);
        });
       }
    })
    }else{
      $('.planIdentify').html("Free");
    }

   $(".uploader-pane").on("change", ".photoName", function() {
// $('.photoName').change(function() {         
  // var id = $(this).data('id');
  // var filename = "";
  filename = $(this).val().split("\\").pop();
   $("#file_name").val(filename);
   $("#preview");
   $("#row-img").attr("src");
   $("#row-img-mobile").attr("src");
   $("#preview").removeClass("hide");
});

   function readURL(input, id) {
    // var id = $(this).data('id');
    // console.log(id);
    // console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#row-img").attr('src', e.target.result);
            $("#row-img").removeClass('hide');
            $("#row-img-mobile").attr('src', e.target.result);
            $("#row-img-mobile").removeClass('hide');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".photoName").change(function(){
    readURL(this, id);
});

$(".uploader-pane").on("click", ".removeImage", function() { 
    var id = $(this).data('id');
    $("#preview").addClass("hide");
    $("#file_name").val("");
    $("#row-img").removeAttr("src");
    $("#row-img-mobile").removeAttr("src");
    $(".photo").val("");
});
var account_status = "{{$rows->sms_verification_status}}";
if(account_status == 0){
  $('span.account_status').click(function(){
    $('.modal_verification').text('VERIFY YOUR ACCOUNT');
    $('#modalVerify').modal('show');
  });
}
$('#modalVerify .close').click(function(){
  $('.alert-danger').hide();
});
$('#modalVerify .cancel').click(function(){
  $('.alert-danger').hide();
});
$('#sms_send_code').submit(function(e){
  e.preventDefault();
  var mobile_number = $('.mobile_no_verify').val();
  var _token = "{{csrf_token()}}";
  $.ajax({
    type: 'POST',
    url: 'dashboard/sms-send-code',
    data: {mobile_number: mobile_number, _token: _token},
    beforeSend: function(){$('button.sms_verify_btn').attr('disabled','disabled').html('Sending <i></i>').children().addClass('fa fa-spinner fa-spin');},
    complete: function(){$('button.sms_verify_btn').removeAttr('disabled').html('Sent <i></i>').children().addClass('fa fa-check');},
    success: function(data){$('.form_container').children().remove();$('.form_container').html(data);}
  });
});

function startTimer(){
  var counter = 180;
  setInterval(function() {
    counter--;
    if (counter >= 0) {
      $('.newSMSCode').text('Code sent. Wait: '+counter+'s');
    }
    if (counter === 0) {
        $('.newSMSCode').html('New code <i></i>').removeAttr('disabled');;
        clearInterval(counter);
    }
  }, 1000);
}

$(document).ready(function(){
  $('button.multiselect').css('border-radius','0');
  $('button.multiselect').css('height','34px');
});
$(document).ready(function(){
  $('.alerts_table_color').find('td').find('span.alerts_table_font').children().each(function() {
  $(this).addClass('grayTextB').css('font-size','14px').hover(function(e) { 
    $(this).css("color",e.type === "mouseenter"?"#999999":"#999999");
  })
});
});

 $("#modal-feature-ads").on("click", "#btnFeature", function(e) { 
    var loading = $("#load-form-feature");
      // stop form from submitting
       e.preventDefault();
     var form = $('#modal-feature-ads').find("form");
    loading.removeClass('hide');
        // push form data
    $.ajax({
      type: "post",
      url: '{{url('ad/feature')}}',
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        }  else {

         $("#modal-feature-ads").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refreshVendorAdsActiveList();
          refreshVendorAuctionActive();
        }

        loading.addClass('hide');

      }
      });
  });
   
</script>

@stop
@section('content')
<!-- <div style="display: none;">Hidden by default</div> -->
<!-- <div class="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 99;background-color: #FFFFFF;"></div> -->
<div class="container-fluid noPadding bgGray borderBottom planType" id="content-wrapper">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 noPadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        			<div id="change-photo-notice"></div>

					<div class="panel panel-default removeBorder borderZero borderBottom" id="panel-profile">
							<div class="panel-body normalText minPaddingB">	
								  <div id="load-form" class="loading-pane hide">
							             <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
							     </div>
								<img src ="{{url('uploads').'/'.$rows->photo}}" class="img-responsive" id="profile-photo" width="100%">
					                <div class="minPadding noleftPadding norightPadding">
				            {!! Form::open(array('url' => 'profile/photo/save', 'role' => 'form', 'class' => 'form-horizontal','files'=>'true','id'=>'modal-save_photo')) !!} 
				                  	<div class="uploader-pane">
					                      <div class='change error-photo'>
					                        <!-- <input name='photo' id='row-photo_upload' type='file' class='file borderZero form-control file_photo' data-show-upload='false' placeholder='Upload a photo'> -->
					                       <sup class="sup-errors redText"></sup>
                          </div>

<div class="row">
<div class="col-xs-12">
<div class="input-group input-group-sm">
  <span class="input-group-addon redText bgWhite borderZero" id="sizing-addon1"><i class="fa fa-upload" aria-hidden="true"></i></span>
  <input input type="text" name="file_name" id="file_name" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
  <span class="input-group-btn">
   <label class="btn-file btn redButton borderZero btn-sm">
    Browse <input  name='photo' id='row-photo_upload' type="file" style="display: none;" class='' data-show-upload='false' placeholder='Upload a photo' onchange="document.getElementById('file_name').value = this.value.split('\\').pop().split('/').pop()">
</label> 
        <!-- <button type="button" class="btn-file btn redButton borderZero btn-sm">Browse</button> -->
      </span>
        <span class="input-group-btn">
        <button type="button" class="btn btn-img-delete redButton borderZero btn-sm leftMargin"><span style="padding: 1px;"><i class="fa fa-trash" aria-hidden="true"></i></span></button>
      </span>
</div>
</div>
<!-- <div class="col-xs-3">
<!-- <button type="button" class="btn redButton borderZero btn-sm">Browse</button> -->
<!-- <button type="button" class="pull-right btn redButton borderZero btn-sm leftMargin" ><span style="padding: 1px;"><i class="fa fa-trash" aria-hidden="true"></i></span></button>
</div> -->
</div>
					                  </div>
				                    <!-- <button class="btn redButton borderZero" type="button">Browse</button> -->
				                </div>
				                <input type="hidden" name="id" value="{{$rows->id}}">
								<button class="btn blueButton borderZero noMargin fullSize imageUpload" type="submit" style="height:35px;"><i class="fa fa-picture-o"></i> Upload Photo</button>
						   </div>
						   {!! Form::close() !!}
					 </div>
					
					<div class="panel panel-default removeBorder borderZero borderBottom hidden-xs hidden-sm">
							<div class="panel-body normalText">
								<ul id="dashboardTab" class="nav nav-pills nav-stacked noPadding" role="tablist">
									<li role="presentation"><a id="account-link" href="#account" aria-controls="account" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i> Account<i class="fa fa-angle-right pull-right"></i></a></li>
                  <!-- <li role="presentation"><a href="#additionalInformation" aria-controls="information" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user-plus rightPadding"></i> Additional Information<i class="fa fa-angle-right pull-right"></i></a></li> -->
									<li role="presentation"><a id="subscription-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-barcode  rightPadding"></i> Subscription <span class="redText planIdentify">(Upgrade Plan)</span><i class="fa fa-angle-right pull-right"></i></a></li>
									<li role="presentation" id="auction-listings"><a id="auction-link" href="#auctionAds" aria-controls="auctionAds" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bar-chart rightPadding"></i> Auction Listings<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation" id="ad-listings"><a id="listings-link" href="#listings" aria-controls="listings" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-th rightPadding"></i> Ads Listings<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a id="alerts-link" href="#alerts" aria-controls="alerts" role="tab" data-toggle="tab" class="aleClicked borderbottomLight grayText"><i class="fa fa-bell rightPadding"></i> Alerts<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a id="messages-link" href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="mesClicked borderbottomLight grayText"><i class="fa fa-envelope rightPadding"></i> Messages<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a href="#commentsandReviews" aria-controls="commentsandReviews" role="tab" data-toggle="tab" class="comClicked borderbottomLight grayText"><i class="fa fa-comments rightPadding"></i> Comments and Reviews<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a id="watchlist-link" href="#watchlist" aria-controls="watchlist" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-eye rightPadding"></i> Watchlist<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a href="#rewardsandreferrals" aria-controls="rewardsandreferals" role="tab" data-toggle="tab" class="rewClicked borderbottomLight grayText"><i class="fa fa-link  rightPadding"></i> Rewards and Referrals<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a href="#advertisingplans" aria-controls="advertisingplans" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bullhorn  rightPadding"></i> Banner Ads<i class="fa fa-angle-right pull-right"></i></a></li>

									<li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab" class="repClicked borderbottomLight grayText"><i class="fa fa-area-chart rightPadding"></i> Reports<i class="fa fa-angle-right pull-right"></i></a></li>
                  <li role="presentation"><a href="#invoice" aria-controls="invoice" role="tab" data-toggle="tab" class="invClicked borderbottomLight grayText"><i class="fa fa-list rightPadding" aria-hidden="true"></i> Invoice<i class="fa fa-angle-right pull-right"></i></a></li>
                  <!-- <li role="presentation"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bar-chart rightPadding" aria-hidden="true"></i> Statistics<i class="fa fa-angle-right pull-right"></i></a></li> -->
                  <li role="presentation"><a id="settings-link" href="#profileSettings" aria-controls="profileSettings" role="tab" onclick="refresh()" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-cog rightPadding"></i>  Settings<i class="fa fa-angle-right pull-right"></i></a></li>
                </ul>
							</div>
						</div>

        </div>
      </div>			
     <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12 noPadding">
			<div class="tab-content">
		    <div role="tabpanel" class="tab-pane active" id="account">
		    	<div class="panel panel-default removeBorder borderZero borderBottom nobottomMargin">
		    		<div id="account-profile-notice"></div>
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Account Setting</span>
              <span class="pull-right"><button class="btn redButton borderZero btn-sm" id="btn-change-mode" style="position: relative;bottom: 5px;">Edit</button></span>
						</div>
						<div class="panel-body" id="panel-account-profile">
               {!! Form::open(array('url' => 'register/save', 'role' => 'form', 'class' => '','files'=>'true','id'=>'form-profile_save')) !!} 
              <div id="load-account-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
              </div> 
              <div class="normalText grayText">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              VANITY URL
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                  <div class="form-group bottomMargin">
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-12 col-sm-12 col-xs-12 ">
                     {{ URL::to('/').'/' }} <input class="borderZero inputBox readonly-view" type="text" name="alias" id="customer-alias" value="{{$rows->alias}}">
                      </div>
                    </div>
                  </div>
              </div>
              </div>
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding redText ">
							BASIC INFORMATION
              <span class="pull-right redText">
                ACCOUNT STATUS:<span class="form-group bottomMargin" style="vertical-align:middle;">
                <!-- <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label> -->
                @if($rows->sms_verification_status == 1)
                <span class="label label-success borderZero" style="vertical-align:middle;">Verified</span>
                @else
                <span class="account_status label label-danger borderZero redbutton" style="cursor:pointer; vertical-align:middle;">Verify</span>
                @endif
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
                  </div>
                </div>
              </span>
              </span>
							</div>
						 	   
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin" id="account-setting">
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Usertype</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <select class="form-control inputBox readonly-view" id="client-usertype_id" name="usertype">
                          @foreach($usertypes as $row)
                            <option value="{{$row->id}}"{{($rows->usertype_id == $row->id ? 'selected' : '')}}>{{$row->type_name}}</option>
                          @endforeach  
                        </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input name="username" placeholder="Input" value="{{$rows->username}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Profile Name</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input name="name" placeholder="Input" value="{{$rows->name}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 founded-date-pane {{$rows->usertype_id == 1 ? 'hide':''}}">
                 <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Founded Date</label>
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <div class="input-group date" id="date-company_start_date">
                         <input type="text" name="company_start_date" value="{{ date("m/d/Y", strtotime($rows->company_start_date)) }}" class="form-control borderZero inputBox readonly-view" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div> 
                    </div>
                  </div> 
              </div> 
              <div class="col-sm-6 individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                        <input type="text" class="form-control borderZero inputBox readonly-view" value="{{$rows->address}}"name="address" id="client-address">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <input name="email" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize readonly-view" type="email">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                          <input name="website" class="form-control borderZero inputBox fullSize readonly-view" type="url" value="{{$rows->website}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin" id= "client-country-pane">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                  <div class="input-group col-md-8 col-sm-12 col-xs-12">
                    <select class="form-control borderZero inputBox readonly-view" id="client-country" name="country">
                      <option class="hide">Select</option>
                      @foreach($countries as $row)
                        <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':''}}>{{$row->countryName}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin" id="client-city-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-city" name="city">
                      </select>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Phone Number</label>
                  <div class="input-group flex">
                    <input class="form-control borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;" placeholder="+63">
                    <input class="form-control leftMargin borderZero inputBox fullSize readonly-view" type="number" name="telephone_no" value="{{$rows->telephone}}" id="client-tel_no">
                  </div>  
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Mobile Number</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group flex">
                       <input class="form-control borderZero inputBox readonly-view" type="text" name="mobile_no_prefix" id="client-mobile_no_prefix" style="width:50px;" placeholder="+63">
                      <input class="form-control leftMargin borderZero inputBox fullSize readonly-view" type="number" name="mobile" value = "{{$rows->mobile}}" id="client-mobile">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Birthdate</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                        <input type="text" name="date_of_birth" class="form-control borderZero inputBox readonly-view" id="client-date_of_birth" value="{{ date("m/d/Y", strtotime($rows->date_of_birth)) }}" placeholder="MM/DD/YYYY">
                        <span class="input-group-addon borderZero inputBox">
                          <span><i class="fa fa-calendar"></i></span>
                       </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">Gender</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group topPadding col-md-8 col-sm-12 col-xs-12">
                       <label class="radio-inline"><input class="readonly-view" type="radio" name="gender" id="client-gender-male"  {{$rows->gender == 'm' ? 'checked':''}} value="m">Male</label>
                       <label class="radio-inline"><input class="readonly-view" type="radio" name="gender" id="client-gender-female" {{$rows->gender == 'f' ? 'checked':''}} value="f">Female</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </span>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-lg-12 col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Description</label>
                      <div class="inputGroupContainer">
                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <textarea class="form-control inputBox lineHeight readonly-view" rows="5" id="comment" name="description">{{$rows->description}}</textarea>
                        </div>
                      </div>
                    </div>
                </div>
        </div>
<!-- 								<div class="col-lg-6 col-md-6 col-xs-12">
										<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
													<input name="username" placeholder="Input" value="{{$rows->username}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Profile Name</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
													<input name="name" placeholder="Input" value="{{$rows->name}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
													<input name="email" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
												</div>
											</div>
										</div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Usertype</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
													  <select class="form-control inputBox readonly-view" id="client-usertype_id" name="usertype">
													  	@foreach($usertypes as $row)
													  		<option value="{{$row->id}}"{{($rows->usertype_id == $row->id ? 'selected' : '')}}>{{$row->type_name}}</option>
													  	@endforeach  
													  </select>
												</div>
											</div>
										</div> -->
								<!-- 		<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
															<input name="password" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
															<input name="password_confirmation" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
												</div>
											</div>
										</div> -->
<!--                   <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12">
                              <input name="website" class="form-control borderZero inputBox fullSize readonly-view" type="text" value="{{$rows->website}}">
                        </div>
                      </div>
                    </div> -->
<!--                 @if($rows->usertype_id == 2)
                 <div class="form-group bottomMargin" id="founded-date-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Founded Date</label>
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <div class="input-group date" id="date-company_start_date">
                         <input type="text" name="company_start_date" value="{{ date("m/d/Y", strtotime($rows->company_start_date)) }}" class="form-control borderZero inputBox readonly-view" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div> 
                    </div>
                  </div> 
                 @endif  
								</div>
                <div class="col-sm-12 noPadding {{$rows->usertype_id == 1 ? '':'hide'}}" id="individual-pane-one">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group bottomMargin" id= "client-country-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-country" name="country">
                        <option class="hide">Select</option>
                        @foreach($countries as $row)
                          <option value="{{$row->id}}">{{$row->countryName}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group bottomMargin" id="client-city-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-city" name="city">
                      </select>
                    </div>
                  </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Phone Number</label>
                    <div class="input-group flex">
                      <input class="input-group-addon borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;">
                      <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="telephone_no" id="client-tel_no">
                    </div>   -->
<!--                     <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12">
                          <input class="form-control borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;">
                          <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="telephone_no" id="client-tel_no">
                      </div>
                    </div> -->
<!--                   </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Mobile Number</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12">
                            <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="mobile" id="client-mobile">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                          <input type="text" class="form-control borderZero inputBox readonly-view" name="address" id="client-address" placeholder="">
                      </div>
                    </div>
                  </div> -->
<!--                   <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Birthdate</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                          <input type="text" name="date_of_birth" class="form-control borderZero inputBox readonly-view" id="client-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div>
                    </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">I am a</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group topPadding col-md-8 col-sm-12 col-xs-12">
                         <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-male" value="m">Male</label>
                         <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-female" value="f">Female</label>
                         <input type="hidden" name="gender" id="client-gender">
                      </div>
                    </div>
                  </div> -->
						</div>
					<div id="accountBranchContainer"  class="{{ ($rows->usertype_id == 1 ? 'hide': '') }}">	
						<?php echo($branches) ?>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
							<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding topMarginB redText">
								SOCIAL MEDIA
							</span>
              <div class="row">
              <div class="col-sm-6">
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Facebook</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
												<input name="facebook_account" placeholder="" value="{{$rows->facebook_account}}" class="form-control borderZero inputBox fullSize readonly-view" type="url">
									       <sup class="sup-errors redText"></sup>
                  </div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Twitter</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
												<input name="twitter_account" placeholder="" value="{{$rows->twitter_account}}" class="form-control borderZero inputBox fullSize readonly-view" type="url">
									</div>
								</div>
							</div>
            </div>
            <div class="col-sm-6">
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Instagram</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
												<input name="instagram_account" placeholder="" value="{{$rows->instagram_account}}" class="form-control borderZero inputBox fullSize readonly-view" type="url">
									</div>
								</div>
							</div>
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">LinkedIn</label>  
								<div class="inputGroupContainer">
									<div class="input-group col-md-8 col-sm-12 col-xs-12 ">
												<input name="linkedin_account" placeholder="" value="{{$rows->linkedin_account}}" class="form-control borderZero inputBox fullSize readonly-view" type="url">
									</div>
								</div>
							</div>
						</div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
              
<!--               <div class="row">
              <div class="col-sm-6">
              <span class="form-group bottomMargin">
                @if($rows->sms_verification_status == 1)
                <span class="label label-success">Verified <i class="fa fa-check"></i></span>
                @else
                <span class="account_status label label-danger" style="cursor:pointer;">Verify</span>
                @endif
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
                  </div>
                </div>
              </span>
            </div>
          </div> -->
          </div>
          <br>
					<!-- 	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
							<span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding topMarginB redText">
								NEWSLETTER
							</span>
							<div class="col-xs-12 noPadding">
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Weekly</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 noPadding">
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Monthly</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
						</div>
						<div class="col-xs-12 noPadding">
							<div class="form-group bottomMargin">
								<label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Promotions</label>  
								<div class="inputGroupContainer">
									<div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
											<input type="checkbox" value="">
									</div>
								</div>
							</div>
						</div>
					</div> -->
						
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordertopLight noPadding topPaddingB">
						<input type="hidden" name="id" id="client-id" value="{{$rows->id}}">
							<button class="btn blueButton borderZero noMargin pull-right hide" id="btn-save_button" type="submit">Update</button>
						</div>
					{!! Form::close() !!}
						</div>
					</div>
        </div>
<!--        <div role="tabpanel" class="tab-pane" id="additionalInformation">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Additional Information</span>
            </div>
            <div class="panel-body">
              <div class="normalText grayText">
                <form class="form-horizontal" role="form">
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Phone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="telephone_no" id="client-tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Mobile Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="mobile" id="client-mobile" placeholder="">
              </div>
            </div>
            <div id="gender" class="form-group">
              <h5 class="normalText col-sm-3" for="email">I am a</h5>
              <div class="col-sm-9">
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-male" value="m">Male</label>
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-female" value="f">Female</label>
               <input type="hidden" name="gender" id="client-gender">
              </div>
            </div>
            <div id="startDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Founded Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-company_start_date">
                   <input type="text" name="company_start_date" class="form-control borderZero inputBox" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div class="form-group" id= "client-country-pane">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-country" name="country">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group" id="client-city-pane">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-city" name="city">
                </select>
              </div>
            </div>
            <div class="form-group" id="client-address-pane">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="address" id="client-address" placeholder="">
              </div>
            </div>
            <div id="birthDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Birth Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-date_of_birth">
                   <input type="text" name="date_of_birth" class="form-control borderZero inputBox" id="client-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div id="officeHours" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Youtube</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" namee="youtube_account" id="client-youtube" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Website</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="website" id="client-website" placeholder="">
              </div>
            </div>
            <div id="branchContainer">
            <section id="addBranch" class="container-fluid">
            <h5 class="grayText borderbottomLight bottomPadding">
              BRANCH<span class="notrequiredText"> ( Not Required )</span><button type="button" value="" class="hide-view btn-add-branch pull-right borderZero inputBox"><i class="fa fa-plus redText"></i></button>
            </h5>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Telephone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_tel_no[]" id="client-company_tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Cellphone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_mobile[]" id="client-company_mobile" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox company_country" id="client-company_country" name="company_country[]">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                 <select class="form-control borderZero inputBox" id="client-company_city" name="company_city[]">
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_address[]" id="client-company_address" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_email[]" id="client-company_email" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Office Hours</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_office_hours[]" id="client-company_office_hours" placeholder="">
              </div>
            </div>
            <input type="hidden" name="company_branch_id[]">
            </section>
            </div>
          </form>
            </div>
            </div>
          </div>
        </div> -->
		   <div role="tabpanel" class="tab-pane" id="subscription">
        <div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">CURRENT PLAN SUBCRIPTION</span>
            </div>
            <div class="panel-body">
              <div class="lineHeightC normalText grayText">
                  {!!$plan!!}
                  <div class="col-md-12 noPadding">
              <hr class="nobottomMargin notopMargin" style="padding-bottom:8px">
              </div>
              </div>
                      <div class="form-group">
                      
                      <div class="mediumText normalWeight bottomPadding">Purchase Bid Points</div>
                      <div class="row">
                      <div class="col-sm-10">
                      <select class="form-control borderZero grayText normalText normalWeight" id="client-bid-point-package">
                      @foreach ($bid_points as $row) 
                        <option class="grayText normalText normalWeight" value="{{$row->id}}">{{$row->points}} Bid Points - {{$row->price == 0 ? 'FREE':$row->price}}{{$row->price == 0 ? '':'$'}}</option>
                      @endforeach
                      </select>
                      </div>
                      <div class="col-sm-2" id="paypal-button-container">
                      </div>
                      </div>
                    </div>
            </div>
          </div>
       </div>
     </div>
   </div>
   <div role="tabpanel" class="tab-pane" id="auctionAds">
      
              
    </div>
		 
  <div role="tabpanel" class="tab-pane" id="listings">
  </div>

		    <div role="tabpanel" class="tab-pane" id="alerts">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Alerts</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortByDate()"> Title</a></li>
                          <li><a href="#" onclick="sortByStatus()"> Description</a></li>
                          <li><a href="#" onclick="sortByDate()"> Date</a></li>
                        </ul>
                      </li>
                    </ul>
                  </span>
						</div>
						<div class="panel-body" style="padding-top: 3px; padding-bottom:0px;">
							<div class="normalText grayText table-responsive">

                <table id="myTable" class="table">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Description</th>
                      <th class="pull-right">Date</th>
                    </tr>
                  </thead>
<tbody class="bodyA">
  @if($notificationsList)
    @foreach($notificationsList as $notificationsList)
      <tr class="alerts_table_color">
        <td><?php echo($notificationsList->type); ?></td>
        <td><span class="normalWeight normalText grayTextB alerts_table_font"><?php echo ($notificationsList->message); ?></span></td>
        <td><span class="normalWeight normalText grayTextB"><?php echo date("m/d/y g:i A", strtotime($notificationsList->created_at)); ?></span></td>
<!--         <td class="minPadding btn-viewAlerts cursor">
          <div class="notificationBlock"> -->
          <!-- <div class="notificationImage">
          <img src="{{url('uploads').'/'.$notificationsList->photo}}" class="avatar img-circle topMargin" alt="user profile image" style="width: 40px; height: 40px;">
          </div> -->
          <!-- <span class="normalText"><?php echo($notificationsList->message); ?></span> -->
<!--             <div class="grayText normalText topPadding"><?php echo($notificationsList->type); ?>
              <span class="lightgrayText pull-right">
                <?php echo date("F j, Y g:i A", strtotime($notificationsList->created_at)); ?></div>
              </span>
            </div>
          </div>
        </td> 
        <td><span class="normalText"><?php echo($notificationsList->message); ?></span></td>
        <td><span class="lightgrayText pull-right">
                <?php echo date("F j, Y g:i A", strtotime($notificationsList->created_at)); ?></div>
              </span></td> -->
      </tr>
<!--       <tr>
        <td class="minPadding btn-viewAlerts cursor">
          <div class="notificationBlock">
          <div class="notificationImage">
          <img src="{{url('uploads').'/'.$notificationsList->photo}}" class="avatar img-circle topMargin" alt="user profile image" style="width: 40px; height: 40px;">
          </div>
          <span class="normalText"><?php echo($notificationsList->message); ?></span>
          </div>
        </td> 
      </tr> -->
    @endforeach
  @endif
</tbody>
                </table>   
                  
                </div>
                <div class="text-center" style="margin-right:-15px; margin-left:-15px;">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:100%">View More</button>
                  </div>
						</div>
					</div>
		    </div>
		 <div role="tabpanel" class="tab-pane" id="messages">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Message Inbox</span>
						</div>
						<div class="panel-body notopPadding nobottomPadding" id="messageInboxPane">
                 <div id="load-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 
							<div class="col-md-12 noPadding">
                  <div id="messageBlock" class="table-responsive">
                <table id="myTableMessage" class="table">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th class="pull-right">
<!--          <button class="bgWhite removeBorder borderZero normalText grayText dropdown-toggle hidden-sm  hidden-xs" type="" data-toggle="dropdown">Sort by
            <span class="caret"></span>
          </button> -->
          <ul class="nav navbar-nav sortDropdown">
        <li class="dropdown">
          <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
<!--           <div href="#" class="dropdown-toggle grayText cursor noBg noPadding" data-toggle="dropdown"><span id="bidding-count-container">Sort by </span> <span class="caret"></span></div> -->
          <ul class="dropdown-menu sortDrop">
            <li><a href="#"  onclick="sortByDate()"> Date</a></li>
            <li><a href="#"  onclick="sortByStatus()"> Unread</a></li>
          </ul>
        </li>
      </ul>
                       <!-- <span class="dropdown">
                        <button class="noBackground removeBorder borderZero normalText grayText dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown" aria-expanded="false"><i id="sort-indicator" class="redText fa fa-sort-amount-asc"></i>
                        </button>
                          <ul class="dropdown-menu borderZero normalText" style="margin-left:-128px;margin-top:11px;">
                            <li><a href="#"  onclick="sortByDate()"> Date</a></li>
                            <li><a href="#"  onclick="sortByStatus()"> Unread</a></li>
                          </ul>
                        </span> --> <!-- <a href="#" class="btn-sort-msg"></a> --></th>
                    </tr>
                  </thead>
                  <tbody id="message-container">
                    <?php echo $messages_header;?>
                    <!-- <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> System Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> System Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> Inquiry Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr> -->
                  </tbody>
                </table>
                <input type="hidden" id="msg-field" value="date">
                <input type="hidden" id="msg-sort" value="desc">
                
              </div>
              <div id="view_more_inbox" class="text-center" style="margin-left: -15px; margin-right: -15px;">
                <button class="btn blueButton borderZero" type="button" data-toggle="modal" data-target="#" style="width:100%;">View More </button>
              </div>
              
<!-- 								<div class="col-md-6">
									<div class="panel panel-default borderZero removeBorder">
								    <div class="panel-heading messageTitle nosidePadding">TITLE <span class="pull-right"><span class="normalText">Sort</span> <i class="redText fa fa-sort-amount-asc"></i></span></div>
								    <div class="panel-body noPadding">
								    	<table class="table removeBorder" id="message-table">
										    <tbody class="normalText bgGray grayText" id="message-tbody">
										      <?php echo $messages_header;?>
										    </tbody>
										  </table>
								    </div>
								  </div>
							</div> -->
							<div id="replyBlock">
								<div class="panel removeBorder borderZero panel-default noMargin" id="message-pane">
								   <div class="panel-heading messageTitle noPadding bottomPaddingB nosidePadding" id="message-title-pane">MESSAGE TITLE</div>
								   <div class="panel-body noPadding">
                    <div class="loading-pane hide">
                        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                    </div>
									 <table class="table bottomMarginB removeBorder">
                    
								    <tbody class="normalText grayText" id="message_container">
								     
								    </tbody>
								  </table>
                       {!! Form::open(array('url' => 'user/send/message', 'role' => 'form', 'class' => 'form-horizontal hide', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      								  <textarea class="borderZero form-control linHeight" rows="3" id="row-message" name="message" required></textarea>
                        <span class="pull-left topPadding">
                          <button id="back_button_inbox" class="btn redButton borderZero btn-message-back">Back</button>
                        </span>
      								  <span class="pull-right topPadding">
                          <input type="hidden" name="message_id" id="row-message_id">      								  	
                          <span><button type="submit" class="btn borderZero blueButton">Reply</button></span>
      								  </span>
                       {!! Form::close() !!}
								   </div>
								 </div>
								</div>
							</div>
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="commentsandReviews">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Comments and Reviews</span>
						</div>
						<div class="panel-body">
              <div class="table-responsive">

                
                    <?php echo $review_summary;?>
              </div>
           
					    <!-- <div class="col-sm-12 bgWhite bottomPaddingB">
								<div class="panelTitle bottomPaddingB">Add title comment and review</div>
								<div class="commentBlock">
	            	<div class="commenterImage">
		               <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
		              </div>
	                <div class="commentText">
	                <div class="panelTitleB bottomPadding">Username</div>
	                <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
	                <div class="subText">
					          <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
										<span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
										<span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
									</div>
	               	</div>
	            </div>
							</div> -->
              <!--
							<div class="col-sm-12 bgGray bottomPaddingB borderBottom">
								<div class="panelTitle topPaddingB bottomPaddingB">Add title comment and review</div>
								<div class="commentBlock">
            		<div class="commenterImage">
	               <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
	              </div>
                <div class="commentText">
                <div class="panelTitleB bottomPadding">Username</div>
                <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                <div class="subText">
				          <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
									<span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
									<span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
								</div>
               	</div>
            	</div>
							</div>
							<div class="col-sm-12 bgWhite topPaddingB bottomPaddingB">
								<div class="panelTitle bottomPaddingB">if vendor page show vendor name</div>
								<div class="commentBlock">
	            	<div class="commenterImage">
		               <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
		              </div>
	                <div class="commentText">
	                <div class="panelTitleB bottomPadding">Username</div>
	                <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
	                <div class="subText">
					          <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
										<span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
										<span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
									</div>
	               	</div>
	            </div>
							</div> -->
						</div>
					</div>
		    </div>




<div role="tabpanel" class="tab-pane" id="watchlist">
  <div id="watchlist-panel"></div>
  <div class="watchlist-container">
  <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
         
        <!-- Mobile View -->
        <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
            <div class="panel-heading panelTitleBarLightB">
                <span class="panelTitle">Listings</span>  <span class="hidden-xs hidden-sm">| Check out our members latest bids and start bidding now!</span>
                <!--   <span class="pull-right"><a href="http://192.168.254.14/yakolak/public/category/list" class="redText">view more</a></span> -->
               </div>
            <div class="col-sm-12 col-xs-12 blockBottom noPadding">
             <div class="panel-body noPadding">
            </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="panel panel-default removeBorder borderZero hidden-xs hidden-sm">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Watchlist</span>
              </div>
                 <div id="load-watchlist-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 
          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 noPadding"> 
           <!--    <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse9">
                <strong>Auction Bidded </strong><span class="{{count($auction_biddeds) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($auction_biddeds) == 0 ? 'bgGrayC':'bgRed'}} badge">{{count($auction_biddeds)}} </span> Bidded<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse9" class="panel-collapse collapse {{(count($auction_biddeds) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">
     
             @foreach ($auction_biddeds as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="3">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by {{$row->name}}<span class="pull-right"><strong class="blueText">
                                   {{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  <span class="pull-right">
                                       @if($row->average_rate != null)
                                         <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @else
                                         <span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @endif 
                                  </span>
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @endforeach 

               </div>
            </div>
          </div>
        </div>
        </div>
        </div> -->
              <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body notopPadding nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse7">
                <strong>Favorite Listings </strong><span class="{{count($watchlistads) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($watchlistads) == 0 ? 'bgGrayC':'bgRed'}} badge">{{ count($watchlistads) }}</span> Favorite Ads<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse7" class="panel-collapse collapse {{(count($watchlistads) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">
                @forelse ($watchlistads as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="1" data-title="{{$row->title}}">
                    <div class="panel panel-default borderZero removeBorder bottomMarginB">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{ url('/ads/view') . '/' . $row->id }}">
                              <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                                if(strlen($row->title) >= 37){echo "..";} ?>
                              </div>
                              <div class="visible-md"><?php echo substr($row->title, 0, 25);
                                if(strlen($row->title) >= 37){echo "..";} ?>
                              </div>
                            </a>
                            <div class="normalText grayText bottomPadding bottomMargin"> 
                              <div class="row">
                                <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}"> 
                                  <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                                </div>
                                <div class="col-sm-6">
                                  <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="normalText bottomPadding">
                              <div class="row">
                                <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                                  @if($row->city || $row->countryName)
                                    <i class="fa fa-map-marker rightMargin"></i>
                                    {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                                  @else  
                                  @endif
                                    <?php $i=0;?>
                                  @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                    <?php $i++;?>     
                                  @endforeach   
                                  @endif
                                </div>
                                <div class="col-sm-6">
                                  <span class="pull-right">
                                  @if($row->average_rate != null)
                                    <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                    <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                                  @else
                                    <span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                                    <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                                  @endif 
                                  </span>
                                </div>
                              </div>
                            </div>   
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @empty
                 <p class="text-center redText topPadding bottomPadding">Empty</p>

                @endforelse 

              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding notopPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder bottomMarginB">
            <div class="row panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse91">
                <strong>Favorite Auctions </strong><span class="lightgrayText pull-right"><span class="bgGrayC badge">0</span> Favorite Auctions<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse91" class="panel-collapse collapse">
              <div class="panel-body borderBottom row removeBorder center">
              {{-- <div class="redText normalText text-center">Empty</div> --}}
              <p class="text-center redText topPadding bottomPadding">Empty</p>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
         <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding notopPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse8">
                <strong>Favorite Profile </strong><span class="{{count($favorite_profiles) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($favorite_profiles) == 0 ? 'bgGrayC':'bgRed'}} badge">{{count($favorite_profiles)}}</span> Favorite Users<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse8" class="panel-collapse collapse {{(count($favorite_profiles) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">

                        @forelse ($favorite_profiles as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="2">
                    <div class="panel panel-default borderZero removeBorder bottomMarginB">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">                
                                <div class="mediumText grayText ">
                                  <a class="grayText" href="{{URL::to('/'.$row->alias)}}"><span class="fa fa-user" aria-hidden="true"></span><span class="leftPadding">{{$row->name}}</span></a>
<!--                                   <i class="fa fa-user" aria-hidden="true"></i> <span class="leftPadding">{{$row->name}}</span><span class="pull-right">
                                  </span> -->
                                </div>
                                  <div class="normalText topPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @empty
                   <p class="text-center redText topPadding bottomPadding">Empty</p>
                @endforelse 
               </div>
            </div>
          </div>
        </div>
        </div>
        </div> <!-- <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                <div class="panel-body nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse91">
                <strong>Auction Bidded </strong><span class="leftMargin lightgrayText">|  Free account type can only have 4 active listings you must disable active first.</span><span class="redText pull-right">{{count($auction_biddeds)}} Bidded<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse91" class="panel-collapse collapse ">
              <div class="panel-body borderBottom row removeBorder">
     
             @foreach ($auction_biddeds as $row)               
               <div class="col-lg-3 col-md-3" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="3">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by {{$row->name}}<span class="pull-right"><strong class="blueText">
                                   {{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  <span class="pull-right">
                                       @if($row->average_rate != null)
                                         <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @else
                                         <span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @endif 
                                  </span>
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText blueText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @endforeach 

               </div>
            </div>
            <div class="row borderBottom"></div>
          </div>
        </div>
        </div>
        </div> --></div>
          <!-- Mobile Listing -->
    </div>
  </div>
</div>
</div>
		    <div role="tabpanel" class="tab-pane" id="rewardsandreferrals">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Rewards and Referrals</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortByDate()"> Username</a></li>
                          <li><a href="#" onclick="sortByStatus()"> Email</a></li>
                          <li><a href="#" onclick="sortByDate()"> Date</a></li>
                          <li><a href="#" onclick="sortByDate()"> Point</a></li>
                        </ul>
                      </li>
                    </ul>
                  </span>
						</div>
						<div class="panel-body">
              <div class="xxlargeText grayText">Total Accumulated Points<span class="pull-right">{{$referral_points}}</span></div>
<!--                 <div class="row">Email Address
                  <div class="borderBottom"></div>
                </div> -->
              <div class="normalText topPaddingB grayText">
                <div class="form-inline">
                  <label for="text" class="mediumText rightPaddingB">Referral ID</label>
                  <input type="text" class="form-control borderZero inputBox fullSize" id="email" readonly value="{{$rows->referral_code}}">
                  <span class="normalText lightgrayText pull-right topPaddingB hidden-xs hidden-sm"><span class="hidden-xs hidden-sm">*This code is autogenerated and cannot be altered</span></span>
                  <div class="normalText lightgrayText topPaddingB visible-xs visible-sm"><span class="visible-xs visible-sm">*This code is autogenerated and cannot be altered</span></div>
                </div>
                <!-- <div class="bordertopLightB blockMargin"></div> -->
                <hr class="nobottomMargin">
                <div class="table-responsive">
                <table id="myTable" class="table">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Date</th>
                      <th>Point</th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse ($get_referral_info as $row)
                        <tr>
                          <td>{{$row->getReferralType->name}}</td>
                          <td>{{$row->getUserInfo->name}}</td>
                          <td>{{$row->getUserInfo->email}}</td>
                          <td>{{date("F j Y", strtotime($row->created_at))}}</td>
                          <td>{{$row->referral_points}}</td>
                        </tr>
                  @empty
                       <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center">No Available Referrals</td>
                        <td></td>
                        <td></td>
                       </tr>
                  @endforelse 
                  </tbody>
                </table>
                <div class="text-center pull-right hidden-xs">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" data-toggle="modal" data-target="#inviteUser" style="width:200px;"><i class="fa fa-plus" aria-hidden="true"></i> SEND INVITES </button>
                  </div>
              </div>
              <div class="text-center visible-xs>
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero fullSize" type="button" data-toggle="modal" data-target="#inviteUser">SEND INVITES </button>
                  </div>
              </div>
								  <!-- <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a data-toggle="tab" href="#send-referrals">Send Referrals</a></li>
                    <li><a data-toggle="tab" href="#successful-referrals">Successful Referrals</a></li>
                    <li><a data-toggle="tab" href="#accumulated-points">Accumulated Points</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="accumulated-points" class="tab-pane fade">
                     <h3>1500 points</h3>
                    </div>
                    <div id="successful-referrals" class="tab-pane fade">
                      <h3>Menu 2</h3>
                       <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Email</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>john@example.com</td>
                          </tr>
                          <tr>
                            <td>mary@example.com</td>
                          </tr>
                          <tr>
                            <td>july@example.com</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div id="send-referrals" class="tab-pane fade in active">
                      <h3>Contacts</h3>
                      <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 mediumText inputLabel noPadding ">Emails</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                        
                        </div>
                      </div>
                    </div>

                          <div class="text-right">
                            <a href="{{url('/').'/referral/import'}}"<button type="submit" class="btn btn-default btn-success borderZero"><i class="fa fa-plane"></i> Import Contacts</button></a>
                            <button type="submit" data-user_id="{{Auth::user()->id}}" class="btn btn-default btn-success borderZero btn-send-a-referral"><i class="fa fa-plane"></i> Send A Referral</button>
                          </div>
  
                    
                    </div>
                  </div> -->
							</div>
						</div>
					</div>
		    <div role="tabpanel" class="tab-pane" id="advertisingplans">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Advertising</span>
						</div>
						<div class="panel-body" id="banner-pane">
                <div id="load-form" class="loading-pane hide">
                <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
              </div>
              <div class="grayText"><strong>Purchases</strong><span class="lightgrayText"> | You can purchase banner ads</span><span class="pull-right"><button class="btn btn-sm blueButton borderZero noMargin btn-purchase-ads"><i class="fa fa-plus" aria-hidden="true"></i> Purchase Ads</button></div>
                <div class="table-responsive fullSize topPaddingB">
                <table id="myTable" class="table noMargin normalText">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th>Placement</th>
                      <th>Plan</th>
                      <th>Start</th>
                      <th>End</th>
                      <th class="">Status</th>
                      <th class="text-right"></th>
            <!--           <th class="text-right">Tools</th> -->
                    </tr>
                  </thead>
                  <tbody id="banner-container">
                    @foreach ($get_user_banners as $row)
                    <tr data-id="{{ $row->id}}">
                      <td class="btn-banner-view">{{ $row->title }}</td>
                      <td class="grayText">{{ $row->getBannerPlacement->name}}</td>
                      <td class="grayText">${{ $row->getBannerPlacementPlan->price }} USD / {{$row->getBannerPlacementPlan->period}} {{($row->getBannerPlacementPlan->period > 1 ? ' months':' month')}}</td>
                      <td class="grayText">{{ $row->start_date }}</td>
                      <td class="grayText">{{ $row->expiration }}</td>
                      <td class="grayText">{{$row->banner_status==1 ? 'Pending':'' }}{{$row->banner_status==2 ? 'Approved':'' }}{{$row->banner_status==3 ? 'Rejected':'' }}{{$row->banner_status==4 ? 'Active':'' }}</td>
                      <td class="pull-right">
                        <form class="noMargin" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="hosted_button_id" value="W3QQ6C546WHF4">
                        <INPUT TYPE="hidden" NAME="return" value="{{url('payment/banner').'/'.$row->id}}">
                        <input type="hidden" name="business" id="element-to-hide" value="yakolakseller@gmail.com">
                        <input type="hidden" name="item_name" value="${{ $row->getBannerPlacementPlan->price }} USD / {{$row->getBannerPlacementPlan->period}} months (Banner Plan)">
                        <input type="hidden" name="item_number" value="">
                        <input type="hidden" name="cm" value="${{ $row->getBannerPlacementPlan->price }} USD / {{$row->getBannerPlacementPlan->period}} {{($row->getBannerPlacementPlan->period > 1 ? ' months':' month')}}">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="amount" value="{{$row->getBannerPlacementPlan->price}}">
                        <div class="form-group-sm noMargin">
<!--                         <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label> -->
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                        <input type="submit" value="Add to Cart" name="submit" title="PayPal - The safer, easier way to pay online!" class="paypal_btn {{$row->banner_status == 2 ? '':'hide'}} {{$row->banner_status == 4 ? 'hide':''}}">
<!--                         <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->
                        </div></div>
                        </form>
                        </div>
                     </td>
                    </tr>
                    @endforeach
                   <!--  <tr>
                      <td>Ad Name</td>
                      <td class="lightgrayText">En > Home</td>
                      <td class="lightgrayText">30 Days</td>
                      <td class="lightgrayText">Jan 1 2016</td>
                      <td class="lightgrayText">Jan 30 2016</td>
                      <td class="lightgrayText text-right">Expired</td>;pppppp
                    </tr>
                    <tr>
                      <td>Ad Name</td>
                      <td class="lightgrayText">En > Home</td>
                      <td class="lightgrayText">30 Days</td>
                      <td class="lightgrayText">Jan 1 2016</td>
                      <td class="lightgrayText">Jan 30 2016</td>
                      <td class="lightgrayText text-right">Pending</td>
                    </tr -->
                  </tbody>
                </table>
                @if (count($get_user_banners) == 0)
                <div class="lightgrayText normalText normalWeight text-center topPaddingD" id="advertising-indicator">No Banner Available</div>
                @endif
              </div>
						</div>
					</div>
		    </div>
		    <div role="tabpanel" class="tab-pane" id="reports">
		    	<div class="panel panel-default removeBorder borderZero borderBottom ">
			    	<div class="panel-heading panelTitleBar">
							<span class="panelTitle">Reports</span>
						</div>
						<div class="panel-body" style="padding-bottom: 0px;">
              <div class="normalText grayText">
                <div class="row">
                <div class="col-sm-12 noPadding">
                  <div class="col-sm-12">

                   <div class="row">
                    <div class="col-sm-12 col-md-6">
                   <div id="piechart" class="chart"></div>
                 </div>
                 <div class="col-sm-12 col-md-6 lineHeightC topPaddingD">
                   <div class="col-sm-12 normalText text-uppercase"><i style="color:#3498db" class="fa fa-square" aria-hidden="true"></i> number of page and ads visits</div>
                   <div class="col-sm-12 normalText text-uppercase"><i style="color:#5dade2" class="fa fa-square" aria-hidden="true"></i> number of reviews</div>
                   <div class="col-sm-12 normalText text-uppercase"><i style="color:#85c1e9" class="fa fa-square" aria-hidden="true"></i> number of comments placed for the ads</div>
                   <div class="col-sm-12 normalText text-uppercase"><i style="color:#aed6f1" class="fa fa-square" aria-hidden="true"></i> number of comments placed for the users</div>
                  </div>
                </div>
                 </div>
<!--                   <div class="col-sm-12 col-md-6">   
                    <div class="statisticBox">
                      <span class="statisticIcon bgAqua"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Page and Ads Visits</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_page_and_ad_visits }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgRed"><i class="fa fa-newspaper-o" aria-hidden="true"></i></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Reviews</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_reviews }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgGreen"><i class="fa fa-comment" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the ads</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_ads }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgYellow"><i class="fa fa-comments" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the users</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_user }}</b></span>
                      </div>
                    </div>
                  </div> -->
                </div>
              </div>
              <div class="table-responsive topPaddingC">
                <table id="myTable" class="table">
                  <thead>
                    <tr>
                      <th class="noWrap">Ad Title</th>
                      <th class="noWrap">Page Visit</th>
                      <th class="noWrap">Rating</th>
                      <th class="noWrap">Total Comments and Reviews</th>
                      <!-- <th class="noWrap">Total Posted Ads</th>
                      <th class="noWrap">Total Posted Auction</th>
                      <th class="noWrap">Total Pictures Uploaded</th>
                      <th class="noWrap">Plan Expiration</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>{{ count($total_ad_posted) }}</td> 
                      <td>{{ count($total_auction_posted) }}</td> 
                      <td>{{ count($total_auction_posted) }} </td> 
                      <td>{{ $no_of_reviews + $no_of_comments_for_ads + $no_of_comments_for_user }}</td> 
                    </tr>
                  </tbody>
                </table>  
                </div> 
                            
                </div>
                <div class="text-center" style="margin-left:-15px; margin-right:-15px;">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:100%;">View More</button>
                  </div>     
<!-- 							<div class="normalText grayText">
                Total posted ads: {{ count($total_ad_posted) }} <br>
                Total posted auction ads: {{ count($total_auction_posted) }} <br>
                Total pictures uploaded: {{ count($total_uploaded_photos) }} <br>
                Plan will expire on : {{ $rows->plan_expiration }} <br>
							</div> -->
						</div>
					</div>
		    </div>
<!--         <div role="tabpanel" class="tab-pane" id="statistics">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Statistics</span>
            </div>
            <div class="panel-body">
              <div class="normalText grayText">
                <div class="col-sm-12 noPadding">
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgAqua"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Page and Ads Visits</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_page_and_ad_visits }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgRed"><i class="fa fa-newspaper-o" aria-hidden="true"></i></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Reviews</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_reviews }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgGreen"><i class="fa fa-comment" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the ads</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_ads }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgYellow"><i class="fa fa-comments" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the users</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_user }}</b></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div role="tabpanel" class="tab-pane" id="invoice">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Invoice</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortByDate()"> Invoice no.</a></li>
                          <li><a href="#" onclick="sortByStatus()"> Transaction Type</a></li>
                          <li><a href="#" onclick="sortByDate()"> Details</a></li>
                          <li><a href="#" onclick="sortByDate()"> Cost</a></li>
                          <li><a href="#" onclick="sortByDate()"> Date</a></li>
                        </ul>
                      </li>
                    </ul>
                  </span>
            </div>
            <div class="panel-body" style="padding-bottom: 0px;">
              <div class="normalText grayText">
              <div class="table-responsive">
                <table id="invoiceTable" class="table">
                  <thead>
                    <tr>
                      <th class="">Invoice No.</th>
                      <th class="">Transaction Type</th>
                      <th class="">Details</th>
                      <th class="">Cost</th>
                      <th class="">Date</th>
                      <th class="">Options</th>
                      <!-- <th class="noWrap">Total Posted Ads</th>
                      <th class="noWrap">Total Posted Auction</th>
                      <th class="noWrap">Total Pictures Uploaded</th>
                      <th class="noWrap">Plan Expiration</th> -->
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>432</td> 
                      <td>Plan Upgrades</td> 
                      <td>Ads</td> 
                      <td>5 USD</td> 
                      <td>11/11/16</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-print"></i></button>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-file-pdf-o"></i></button>
                      </td> 
                    </tr>
                    <tr>
                      <td>432</td> 
                      <td>Bid Points</td> 
                      <td>Ads</td> 
                      <td>5 USD</td> 
                      <td>11/11/16</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-print"></i></button>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-file-pdf-o"></i></button>
                      </td> 
                    </tr>
                    <tr>
                      <td>432</td> 
                      <td>Banner Advertising</td> 
                      <td>Ads</td> 
                      <td>5 USD</td> 
                      <td>11/11/16</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-print"></i></button>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-file-pdf-o"></i></button>
                      </td> 
                    </tr>
                    <tr>
                      <td>432</td> 
                      <td>Featured Ads</td> 
                      <td>Ads</td> 
                      <td>5 USD</td> 
                      <td>11/11/16</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-print"></i></button>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-file-pdf-o"></i></button>
                      </td> 
                    </tr>
                    <tr>
                      <td>432</td> 
                      <td>Featured Bid</td> 
                      <td>Ads</td> 
                      <td>5 USD</td> 
                      <td>11/11/16</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-print"></i></button>
                        <button class="borderZero removeborder noBackground mediumText" ><i class="fa fa-file-pdf-o"></i></button>
                      </td> 
                    </tr>
                  </tbody>
                </table>  
                </div> 
                            
                </div>
                <div class="text-center" style="margin-left:-15px; margin-right:-15px;">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:100%;">View More</button>
                  </div>     
<!--              <div class="normalText grayText">
                Total posted ads: {{ count($total_ad_posted) }} <br>
                Total posted auction ads: {{ count($total_auction_posted) }} <br>
                Total pictures uploaded: {{ count($total_uploaded_photos) }} <br>
                Plan will expire on : {{ $rows->plan_expiration }} <br>
              </div> -->
            </div>
          </div>
        </div>

        <d iv role="tabpanel" class="tab-pane" id="profileSettings">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Settings</span>
            </div>
            <div class="panel-body">
              <div id="load-settings-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
              </div>
              <div id="settings-form-notice"></div> 
              <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              PASSWORD
              </div>
               
            {!! Form::open(array('url' => 'setting/save', 'role' => 'form', 'class' => '','id'=>'form-settings_save')) !!} 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
                <div class="col-lg-12 col-md-12 col-xs-12">
<!--                     <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Current Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                          <input name="current_password" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div> -->
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">New Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="password" class="form-control borderZero inputBox fullSize" type="password" placeholder="Leave blank for unchanged">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Re-Type Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="password_confirmation"  class="form-control borderZero inputBox fullSize" type="password" placeholder="Leave blank for unchanged">
                        </div>
                      </div>
                    </div>
                </div>
<!-- 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    
                   <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                              <input name="password" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                              <input name="password_confirmation" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div>   
                </div> -->
            </div>

              </div>
<!--                 <div class="normalText grayText {{$rows->usertype_id == 1 ? '':'hide'}}" id="location-pane" >
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              LOCATION
              </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                          <select name="country" id="client-country" class="form-control borderZero inputBox fullSize">
                              <option class="hide">Select</option>
                               @foreach($countries as $row)
                                <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':'' }}>{{$row->countryName}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                     <div class="form-group bottomMargin">
                      <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                          <select name="city" id="client-city" class="form-control borderZero inputBox fullSize">
                               @foreach($cities as $row)
                                <option value="{{$row->id}}" {{$rows->city == $row->id ? 'selected':'' }}>{{$row->name}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
              </div> -->
                <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              NEWSLETTER
              </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="form-horizontal bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="weekly_newsletter" value="1" {{$rows->weekly_newsletter == "1" ? 'checked':''}}> Receive Weekly</label>  
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="monthly_newsletter" value="1" {{$rows->monthly_newsletter == "1" ? 'checked':''}}> Receive Monthly</label>  
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="promotion_newsletter" value="1" {{$rows->promotion_newsletter == "1" ? 'checked':''}}> Receive Promotion</label>  
                    </div>
<!--                     <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">Receive Monthly</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                         <input type="checkbox" value="">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Receive Promotions</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                         <input type="checkbox" value="">
                        </div>
                      </div>
                    </div> -->
                </div>
            </div>
              </div>

               <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              LOCATION & LANGUAGE
              </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <select name="country" id="client-geo-country" class="form-control borderZero inputBox fullSize">
                              <option class="hide">Select</option>
                               @foreach($countries as $row)
                                <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':'' }}>{{$row->countryName}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                     <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <select name="city" id="client-geo-city" class="form-control borderZero inputBox fullSize">
                               @foreach($cities as $row)
                                <option value="{{$row->id}}" {{$rows->city == $row->id ? 'selected':'' }}>{{$row->name}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="form-group bottomMargin">
                          <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Localization</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                            <select class="form-control borderZero inputBox fullSize" id="sel1">
                              <option>English</option>
                            </select>
                            </div>
                          </div>
                        </div>
<!--                         <div class="form-group bottomMargin">
                          <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding">Arabic</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                             <input type="radio" value="" name="language">
                            </div>
                          </div>
                        </div>
                        <div class="form-group bottomMargin">
                          <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Chinese</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                             <input type="radio" value="" name="language">
                            </div>
                          </div>
                        </div> -->
                    </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordertopLight noPadding topPaddingB ">
            <input type="hidden" name="id" id="client-id" value="{{$user_id}}">
              <button class="btn blueButton borderZero noMargin pull-right" type="submit">Update</button>
            </div>
             {!! Form::close() !!}

              </div>
            </div>
          </div>
        </div>
		 </div>
		</div>
      </div>
    </div>
  </div>
		
<!-- Remove Modal -->
  <div id="modal-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-remove-header" class="modal-header modal-yeah">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-remove-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-remove-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-remove-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-remove-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
    </div>
    <!-- modal send msg -->
                <div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
                      <div class="modal-dialog">
                        <div class="modal-content borderZero">
                            <div id="load-message-form" class="loading-pane hide">
                              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                            </div>
                          {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
                          <div class="modal-header modalTitleBar">
                              <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title panelTitle"><i class="fa fa-plane rightPadding"></i>Send Message</h4>
                              </div>
                          <div class="modal-body">
                            <div id="form-notice"></div>
<!--                             <input class="fullSize" type="text" name="title" id="row-title" value="Inquiry">
                            <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea> -->
                      <div class="">
                        <label for="email">Title:</label>
                        <input class="form-control borderZero" type="text" name="title" id="row-title">
                      </div>
                      <div class="">
                        <label for="pwd">Message:</label>
                        <textarea class="form-control borderZero" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
                      </div>
                          </div>
                          <div class="modal-footer">
                            <input type="hidden" name="reciever_id" id="row-reciever_id" value="">
                            <button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
                            <button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>

                    <div id="modalExpiration" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content borderZero">
                          <div class="modal-header modalTitleBar">
                        <button type="button" class="close closeButton" data-dismiss="modal">×</button>
                          <h4 class="modal-title panelTitle expiry-countdown">Your Plan is Expiring soon!</h4>
                        </div>
                          <div class="modal-body">
                            <p>Your current plan is <span class="text-capitalize">{{strtoupper($current_user_plan_type)}}</span>, Upgrade now and get a discount, Would you like to extend?</p>
                            <div class="discounted_container">

                            </div>
                            <!-- <div class='text-capitalize discounted_container'></div> -->
                          </div>
                          <div class="clearfix"></div>
                          <div class="modal-footer">
                            <button type="button" class="btn blueButton borderZero yesUpgrade" data-dismiss="modal">Yes</button>
                            <button type="button" class="btn redButton borderZero" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @if($rows->sms_verification_status == 0)
                    <div id="modalVerify" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content borderZero">
                          <div class="modal-header modalTitleBar">
                        <button type="button" class="close closeButton" data-dismiss="modal">×</button>
                          <h4 class="modal_verification modal-title panelTitle">VERIFY YOUR ACCOUNT</h4>
                        </div>
                          <div class="modal-body form_container">
                          <form role="form" class="form-horizontal" id="sms_send_code">
                            <p>Verify Your Account, Enter your mobile number.</p>
<!--                             <span>Mobile Number: <input type="tel" class="mobile_no_verify" value="{{$rows->mobile}}" name="mobile_number" required="required"></span>
                            <button type="submit" class="sms_verify_btn btn blueButton borderZero verify_account_sms">Send Code</button> -->
                            <div class="input-group">
                              <input type="tel" class="form-control borderZero mobile_no_verify" value="{{$rows->mobile}}" name="mobile_number" placeholder="Enter your mobile number..."  required="required">
                              <span class="input-group-btn">
                                <button class="btn btn-code borderZero sms_verify_btn verify_account_sms" type="submit">Send</button>
                              </span>
                            </div>
                          </form>
                          </div>
                          <div class="clearfix"></div>
                          <div class="modal-footer">
                            <button type="button" class="btn redButton borderZero cancel" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endif


@stop
@include('user.dashboard.form')