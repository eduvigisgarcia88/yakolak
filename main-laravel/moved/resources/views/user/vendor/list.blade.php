@extends('layout.frontend')
@section('scripts')
<script>
$_token = "{{ csrf_token() }}";
$('#stats_block').remove();
function refresh(){
        location.reload();
}

$(function () {
          $("#rateYo").rateYo({
            starWidth: "12px",
            fullStar: true,
            ratedFill: "#5da4ec",
            normalFill: "#cccccc",
          });
          $("#averageRate").rateYo({
             starWidth: "12px",
             rating: {{$get_average_rate}},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @foreach ($vendorbids as $row)  
          $("#vendor_bids_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          @foreach ($lastestads as $row)  
          $("#latest_ads_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          @foreach ($featured_ads as $row) 

          $("#featured_ads_{{$row->id}}").rateYo({
             starWidth: "14px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          @foreach ($vendorbidsmobile as $row) 

          $("#vendor_bids_mobile_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach

          @foreach ($latestadsmobile as $row) 
          $("#latest_ads_mobile_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach

          @foreach ($related_ads_mobile as $row) 
          $("#related_ads_mobile_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          @foreach ($related_ads as $row) 
          $("#related_ads_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          @foreach ($related_adset2 as $row) 
          $("#related_adset2_{{$row->id}}").rateYo({
             starWidth: "12px",
             rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
             readOnly: true,
             ratedFill: "#5da4ec",
             normalFill: "#cccccc",
          });
          @endforeach
          var starWidth = $("#rateYo").rateYo("option", "starWidth"); //returns 40px

          $("#rateYo").rateYo("option", "starWidth", "15px"); //returns a jQuery Element
          $("#rateYo").rateYo()
                  .on("rateyo.set", function (e, data) {
                   
              var rating = data.rating;

                      if(rating == 0){
                        $(".btn-rate_product").attr('disabled','disabled');
                      }else{
                        $(".btn-rate_product").removeAttr('disabled','disabled');
                      }
                  });
                 
            });
// $(".nameChar").text(function(index, currentText) {
    
//     var x =  currentText;
//     console.log(x);
//     // console.log(x.length);
//     if(x.length > 12) {
//       $("#badge").removeClass("pull-right");  
//     }else{
//        $("#badge").addClass("pull-right");  
//     }
// });
$(document).ready(function(){
    $(".btn-rate_product").attr('disabled','disabled');
    $(".btn-save_comment").attr('disabled','disabled');
    $(".submitReply ").attr('disabled','disabled');
    var x =  $("#get-user-name").val();
    // console.log(x.length);
    if(x.length > 15) {
       $("#badge").removeClass("pull-right");  
    }else{
       $("#badge").addClass("pull-right");  
    }
});

$("#review-product-pane").on("click", ".btn-rate_product", function() { 
  var $rateYo = $("#rateYo").rateYo();
  var rate = $rateYo.rateYo("rating");
  var id = {{ $user_id }}; 
  var vendor_id = {{ $user_id }}; 
  $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
  dialog('Rate Vendor', 'Are you sure you want to rate this vendor</strong>?', "{{ url('user/rate') }}",id,rate,vendor_id);
});

$("#comment-pane").on("keyup", "#ad-comment", function() {  
  var comment = $("#ad-comment").val();
   if((jQuery.trim( comment )).length==0 ){
    $(".btn-save_comment").attr('disabled','disabled');
   }else{
    $(".btn-save_comment").removeAttr('disabled','disabled');
  } 

});
$(".comments-container").on("keyup", "#reply-message", function() {  
  var reply_message = $(this).parent().parent().parent().parent().parent().find('#reply-message').val();
   if(reply_message == ""){
    $(this).parent().parent().parent().parent().parent().find('.submitReply').attr('disabled','disabled');
   }else{
     $(this).parent().parent().parent().parent().parent().find('.submitReply').removeAttr('disabled','disabled');
  } 

});


$(".comments-container").on("keyup", "#main_comment_reply", function() {  
  var main_reply = $(this).parent().parent().parent().find('#main_comment_reply').val();
   if(main_reply == ""){
    $(this).parent().parent().parent().parent().find('.parentSubmit').attr('disabled','disabled');
   }else{
     $(this).parent().parent().parent().parent().find('.parentSubmit').removeAttr('disabled','disabled');
  } 

});
$(".comments-container").on("click", "#show", function() {
 $(this).parent().parent().parent().find('.inputReply').toggleClass("hidden");

  var get_name  = $(this).data('name');
   if(get_name != undefined){
     var parse_name = 'To '+get_name+' :';
     $(this).parent().parent().parent().parent().find('#main_comment_reply').val(parse_name);
   }else{
    parse_name = "";
   }
 });


$(".comments-container").on("click", "#show_reply_box", function() {
   $(this).parent().parent().parent().parent().parent().find('.inputReplyBox').toggleClass("hidden");

   var get_name  = $(this).data('name');
   if(get_name != undefined){
     var parse_name = 'To '+get_name+' :';
     $(this).parent().parent().parent().parent().parent().find('#reply-message').val(parse_name);
   }else{
    parse_name = "";
   }
 });
 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   $this.countdown(finalDate, function(event) {
     $this.html(event.strftime('D: %D - H: %H - M: %M'));
   });
 });
  $(function(){
 
    $('#ad-comment').keyup(function()
    {
      var yourInput = $(this).val();
      re = /[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
      var isSplChar = re.test(yourInput);
      if(isSplChar)
      {
        var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
        $(this).val(no_spl_char);
      }
    });

      $(function(){
        $("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
              $('#qnimate').removeClass('popup-box-on');
            });
  })
 
  });

  $("#content-wrapper").on("click", ".btn-favorite-profile", function() {
  var name = $(this).data('name');
  var profile_id = $(this).data('profiles_id');
    $("#row-profile_id").val("");
    $("#row-profile_id").val(profile_id);
    $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $("#modal-form").find('form').trigger("reset");
    $(".modal-title").html('<strong>Adding '+ name +'</strong>', profile_id);
    $("#modal-favorite-profile").modal('show');    
});

    $("#content-wrapper").on("click", ".btn-remove-favorite-profile", function() {
  
  var name = $(this).data('name');
  var profiles_id = $(this).data('profile_id');
  var watchlist_id = $(this).data('watchlist_id');
    $("#row-profiles_id").val("");
    $("#row-profiles_id").val(profiles_id);
    $("#row-watchlist_id").val(watchlist_id);
    $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $("#modal-form").find('form').trigger("reset");
    $(".modal-title").html('<strong>Remove '+ name +'</strong>', profiles_id);
    $("#modal-remove-favorite-profile").modal('show');    
});
    $("#panel-bids-main").on("click", "#btn-view-more-bids", function() {

          var last_id = $("#latest-bids-container:last").children().last().data('id');
          var user_id = $("#content-wrapper").data("user-id");
          var btn = $(this);
          btn.html("");
          btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
          var loading = $("#panel-bids").find(".loading-pane");
          var skip = $("#skip").val();
          var take = $("#take").val();
          var no_of_results = $("#no_of_results").val();
          var container = $("#latest-bids-container");
          var body = "";

          $.post("{{url('get-user-bids-pagination')}}", {user_id:user_id,last_id:last_id,skip:skip,take:take,_token: $_token}, function(response) {
                $.each(response.rows, function(index, value) {
                  body += '<div class="col-lg-4 col-md-4 noPadding lineHeight" data-id="'+value.id+'">'+
                      '<div class="sideMargin borderBottom">'+
                        '<a href="{{ url('ads/view') }}/'+value.id+'" target="_blank"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo + '); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                      '</div></a>'+
                        '<div class="minPadding nobottomPadding">'+
                          '<div class="mediumText noPadding topPadding bottomPadding">'+
                                '<div class="visible-lg">'+ value.title +
                               '</div>'+
                                '<div class="visible-md">'+ value.title +
                                '</div>'+
                          '</div>'+
                          '<div class="normalText grayText bottomPadding bottomMargin">'+ 
                           '<a class="normalText lightgrayText" href="{{ url('/') }}/'+value.alias+'"> by '+value.name+'</a>'+
                            '<span class="pull-right"><strong class="blueText"> '+value.price+' USD</strong>'+
                           '</span>'+
                          '</div>'+
                          '<div class="normalText bottomPadding">'+
                             '<i class="fa fa-map-marker rightMargin"></i>'+value.city+' , '+value.countryName+      
                            '<span class="pull-right">'+
                                   (value.average_rate != null ? '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' : '<span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>')+
                                   '<span class="lightgrayText pull-right"><i class="fa fa-comment"></i></span>'+
                            '</span>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class="sideMargin minPadding text-center nobottomPadding redText normalText">'+       
                      '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.get_ads_id+'"></div>'+
                      '</div>'+
                      '</div>';

                    $("#no_of_results").val(response.no_of_results);
                  });
            container.append(body);
            btn.html("");
            btn.html("View More");
            var last =  $("#latest-bids-container:last").children().last().data('id');
            if(response.last_id == last ){
                  $("#btn-view-more-bids").remove();
            }
            // loading.addClass("hide");
             $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('D: %D - H: %H - M: %M'));
               });
             });
            $.each(response.rows, function(index, value) {      
               $("#ads_latest_bids_"+value.id).rateYo({
                   starWidth: "12px",
                   rating:(value.average_rate != null ? value.average_rate:0),
                   ratedFill: "#5da4ec",
                   readOnly: true,
               });
            });
          }, 'json'); 
    });
   $("#panel-ads-main").on("click", "#btn-view-more-ads", function() {
            var last_id = $("#latest-ads-container:last").children().last().data('id');
            var user_id = $("#content-wrapper").data("user-id");
            var loading = $("#panel-ads").find(".loading-pane");
            var btn = $(this);
            btn.html("");
            btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
            var container = $("#latest-ads-container");
            var body = "";
            $.post("{{url('get-user-ads-pagination')}}", {user_id:user_id,last_id:last_id,_token: $_token}, function(response) {
                  
                  $.each(response.rows, function(index, value) {
                    // if(response.last_id == value.id){
                    //   $("#btn-view-more-ads").remove();
                    // }else{
                          
                    body += '<div class="col-lg-4 col-md- noPadding lineHeight" data-id="'+value.id+'">'+
                        '<div class="sideMargin borderBottom bottomMarginB">'+
                          '<a href="{{ url('ads/view') }}/'+value.id+'" target="_blank"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo + '); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                        '</div></a>'+
                          '<div class="minPadding nobottomPadding">'+
                            '<div class="mediumText noPadding topPadding bottomPadding">'+
                                  '<div class="visible-lg">'+ value.title +
                                 '</div>'+
                                  '<div class="visible-md">'+ value.title +
                                  '</div>'+
                            '</div>'+
                            '<div class="normalText grayText bottomPadding bottomMargin">'+ 
                             '<a class="normalText lightgrayText" href="{{ url('/') }}/'+value.alias+'"> by '+value.name+'</a>'+
                              '<span class="pull-right"><strong class="blueText"> '+value.price+' USD</strong>'+
                             '</span>'+
                            '</div>'+
                            '<div class="normalText bottomPadding">'+
                               '<i class="fa fa-map-marker rightMargin"></i>'+value.city+' , '+value.countryName+      
                              '<span class="pull-right">'+
                                     (value.average_rate != null ? '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' : '<span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>')+
                                     '<span class="lightgrayText pull-right"><i class="fa fa-comment"></i></span>'+
                              '</span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '</div>';

                      $("#no_of_results").val(response.no_of_results);
                      
                       
                
                    });
                    
                    
              container.append(body);
               btn.html("");
               btn.html("View More");
               var last =  $("#latest-ads-container:last").children().last().data('id');
               if(response.last_id == last ){
                  $("#btn-view-more-ads").remove();
               }
               $('[data-countdown]').each(function() {
                 var $this = $(this), finalDate = $(this).data('countdown');
                 var ads_id = $(this).data('ads_id');
                 $this.countdown(finalDate, function(event) {
                 $this.html(event.strftime('D: %D - H: %H - M: %M'));
                 });
               });
              $.each(response.rows, function(index, value) {      
                 $("#ads_latest_bids_"+value.id).rateYo({
                     starWidth: "12px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     readOnly: true,
                 });
              });
            }, 'json'); 
      });
    var jurl = window.location.href;
    var hash = jurl.substring(jurl.indexOf('#'));
    var urlprod = jurl.substring(jurl.indexOf('|')+1);
    var productx = urlprod.replace(/\|/g, ' ');
    var prodname = productx.toUpperCase();
    if(/#direct/i.test(hash)){
      $('button[data-target=#modal-send-msg]').click();
      $('input#row-title').val('Inquiry for ('+prodname+')');
    }


$('#description-body').filter(function() {
        return $.trim($(this).text()) === ''
}).hide()

</script>
@stop
@section('content')
<div class="container-fluid bgWhite borderbottomLight">
  <div class="container bannerJumbutron" id="content-wrapper" data-user-id = "{{$rows->id}}">
    <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-md-3 col-sm-5 col-xs-12">
      <div class="panel panel-default removeBorder borderZero">
        <div class="panel-body noPadding">
        <div>
          <img src="{{url('uploads').'/'.$rows->photo}}" class="img-responsive" alt="user profile image">
        </div>
        <div class="topPadding">
        <button type="button" class="btn redButton borderZero fullSize ellipsis">Message {{$rows->name}} (offline)</button>
        </div>
        <div class="topPaddingB">
                <span class="grayTextB">Vendor Rating:</span>
             @if($get_average_rate != 0)<span id="rate" class="normalText grayText">
                <span id="averageRate" class="mediumText adRating"></span></span>
             @else
                <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
             @endif
                <span class="grayTextB pull-right rightPaddingB">2 <i class="fa fa-comment" aria-hidden="true"></i></span>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding normalText ellipsis wordBreak {{ $rows->website == "" && $rows->facebook_account == "" && $rows->twitter_account == ""? '' : ''}}">
                <div class="normalText grayText ellipsis"><i class="fa fa-globe rightMargin"></i>{{ $rows->website  == ""  ? 'N/A': $rows->website }}</div>
                <div class="normalText grayText ellipsis"><span class="grayText"><i class="fa fa-facebook-square rightMargin"></i></span>{{ $rows->facebook_account == ""  ? 'N/A': $rows->facebook_account }}</div>
                <div class="normalText grayText ellipsis"><span class="grayText"><i class="fa fa-twitter rightMargin"></i></span>{{ $rows->twitter_account == ""  ? 'N/A': $rows->twitter_account }}</div> 
              </div>
        </div>
      </div>
<!--         <div class="panel panel-default bottomMarginLight removeBorder borderZero">
            <div class="panel-body noPadding">
              <div class="col-md-12 col-sm-12 noPadding">
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
                <div class="panel-body minPaddingB">
                  <div class="col-xs-12 noPadding">
                    <div class="profileBlock">
                      <div class="profileImage">
                        <img src="{{url('uploads').'/'.$rows->photo}}" class="avatar" alt="user profile image" style="width: 85px; height: 85px;">
                      </div>
                      <div class="profileText lineHeightA">
                        
                        <div class="mediumText"><strong class="nameChar noleftPadding norightPadding mediumText grayText ellipsis text-uppercase" data-toggle="popover" title="{{$rows->name}}" data-content="">{{$rows->name}}</strong>
                          <span id="badge" class="pull-right break">
                              <span class="smallText normalWeight badge 
                                {{strToLower($current_user_plan_type) == 'free' ? 'bgGrayC':''}} 
                                {{ strToLower($current_user_plan_type) == 'silver' ? 'bgGrayC':''}} 
                                {{ strToLower($current_user_plan_type) == 'bronze' ? 'bgGrayC':''}}
                                {{ strToLower($current_user_plan_type) == 'gold' ? 'bgGrayC':''}}"">
                                {{$current_user_plan_type}}
                              </span>
                          
                        @if(Auth::check())
                          @if(Auth::user()->id == $rows->id)
                        
                          @else
                            @if($get_favorite_profiles == null)
                              <i class="fa fa-bookmark lightgrayText cursor btn-favorite-profile" data-name="{{$rows->name}}" data-profiles_id="{{$rows->id}}"></i>
                                @else>
                                <i class="fa fa-bookmark blueText cursor btn-remove-favorite-profile" data-watchlist_id="{{$get_favorite_profiles->id}}" data-name="{{$rows->name}}" data-profile_id="{{$rows->id}}"></i>
                            @endif

                          @endif
                            @else
                            <i class="fa fa-bookmark lightgrayText cursor" data-target = "#modal-login" data-toggle="modal"></i>
                        @endif 
                        </span>        
                        </div>
                        <div class="normalText grayText wordBreakB"><i class="fa fa-envelope rightMargin"></i> {{$rows->email}}</div>
                        <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> <span class="skype_c2c_print_container notranslate">{{$rows->mobile}}</span><span id="skype_c2c_container" class="skype_c2c_container notranslate" dir="ltr" tabindex="-1" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" data-numbertocall="+639178238577" data-numbertype="paid" data-isfreecall="false" data-isrtl="false" data-ismobile="false"><span class="skype_c2c_highlighting_inactive_common" dir="ltr" skypeaction="skype_dropdown"><span class="skype_c2c_textarea_span" id="non_free_num_ui"><img width="0" height="0" class="skype_c2c_logo_img" src="chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/call_skype_logo.png"><span class="skype_c2c_text_span">+639178238577</span><span class="skype_c2c_free_text_span"></span></span></span></span></div>
                        <div class="normalText grayText"><i class="fa fa-map-marker rightMargin"></i> Jeddah, UAE</div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding normalText ellipsis wordBreak {{ $rows->website == "" && $rows->facebook_account == "" && $rows->twitter_account == ""? 'hide' : ''}}">
                    <div class="normalText grayText ellipsis {{ $rows->website  == ""  ? 'hide': '' }}"><i class="fa fa-globe rightMargin"></i> {{$rows->website}}</div>
                    <div class="normalText grayText ellipsis {{ $rows->facebook_account == ""  ? 'hide': '' }}"><span class="blueText"><i class="fa fa-facebook-square rightMargin"></i></span> {{ $rows->facebook_account }} </div>
                    <div class="normalText grayText ellipsis {{ $rows->twitter_account == ""  ? 'hide': '' }}"><span class="blueText"><i class="fa fa-twitter rightMargin"></i></span> {{ $rows->twitter_account }}</div> 
                  </div>
                </div>
              </div>
            </div>
            @if(Auth::check())
              @if(Auth::user()->id == $rows->id)
              
              @else
              <div class="col-md-12 noPadding"><button data-target = "#modal-send-msg" data-toggle="modal" class="btn fullSize mediumText redButton borderZero ellipsis"><i class="fa fa-paper-plane"></i> Message {{$rows->name}} <span>{{ $rows->online_status == "1" ? '(online)':'(offline)' }}</span></button></div>
              @endif
            @else
              <div class="col-md-12 noPadding"><button  data-target = "#modal-login" data-toggle="modal" class="btn fullSize mediumText redButton borderZero ellipsis"><i class="fa fa-paper-plane"></i> Message {{$rows->name}} <span>{{ $rows->online_status == "1" ? '(online)': '(offline)' }}</span></button></div>
            @endif
            </div>
        </div> -->
      </div>
       <input type="hidden" id="get-user-name" value="{{$rows->name}}">
      <div class="col-md-9 col-sm-7 col-xs-12 sideBlock noPadding">
      <div class="panel panel-default removeBorder noMargin borderZero">
        <div class="panel-body nobottomPadding">
          <div class="xlargeText grayText">
          <b>{{$rows->name}}</b>
          <span class="pull-right grayTextB">
          <b class="rightPadding">{{$current_user_plan_type}}</b>
          @if(Auth::check())
                        @if(Auth::user()->id == $rows->id)
                      
                        @else
                          @if($get_favorite_profiles == null)
                            <i class="fa fa-bookmark lightgrayText cursor btn-favorite-profile" data-name="{{$rows->name}}" data-profiles_id="{{$rows->id}}"></i>
                              @else>
                              <i class="fa fa-bookmark blueText cursor btn-remove-favorite-profile" data-watchlist_id="{{$get_favorite_profiles->id}}" data-name="{{$rows->name}}" data-profile_id="{{$rows->id}}"></i>
                          @endif

                        @endif
                          @else
                          <i class="fa fa-bookmark lightgrayText cursor" data-target = "#modal-login" data-toggle="modal"></i>
                      @endif
          </span>
          </div>

        </div>
      </div>
        <div class="panel panel-default nobottomMargin removeBorder borderZero">
          <div class="panel-body" id="description-body"><!-- style="min-height: 100px;" -->

                <div class="mediumText wordBreak lineHeight textJustify"><?php echo $rows->description;?></div> 

                  <div class="topPaddingC">
                  <div class="normalText grayText wordBreakB"><i class="fa fa-envelope rightMargin"></i> {{$rows->email}}</div>
                  <div class="normalText grayText"><i class="fa fa-phone rightMargin"></i> <span class="skype_c2c_print_container notranslate">{{$rows->mobile}}</span><!-- <span id="skype_c2c_container" class="skype_c2c_container notranslate" dir="ltr" tabindex="-1" onmouseover="SkypeClick2Call.MenuInjectionHandler.showMenu(this, event)" onmouseout="SkypeClick2Call.MenuInjectionHandler.hideMenu(this, event)" onclick="SkypeClick2Call.MenuInjectionHandler.makeCall(this, event)" data-numbertocall="+639178238577" data-numbertype="paid" data-isfreecall="false" data-isrtl="false" data-ismobile="false"><span class="skype_c2c_highlighting_inactive_common" dir="ltr" skypeaction="skype_dropdown"><span class="skype_c2c_textarea_span" id="non_free_num_ui"><img width="0" height="0" class="skype_c2c_logo_img" src="chrome-extension://lifbcibllhkdhoafpjfnlhfpfgnpldfl/call_skype_logo.png"><span class="skype_c2c_text_span">+639178238577</span><span class="skype_c2c_free_text_span"></span></span></span></span> --></div>
                  <div class="normalText grayText"><i class="fa fa-map-marker rightMargin"></i> Jeddah, UAE</div>
                  </div>
                <?php echo $branches_public?>
            <!-- <br>
            <div class="mediumText">Cras ultrices risus sit amet ex sagittis posuere. Praesent euismod dignissim ipsum, eget varius nulla egestas tincidunt. Praesent porttitor erat id leo ornare fringilla. Fusce efficitur egestas tellus vitae eleifend. Pellentesque et felis facilisis, ultrices neque vitae, pretium quam. Ut eget justo at libero commodo mollis. Nulla rhoncus metus enim, sit amet gravida est eleifend nec. Nunc eget convallis mi.</div> -->
          </div>
        </div>          
<!--         <div class="panel panel-default removeBorder borderZero bgGray noMargin">
          <div class="panel-body grayTextB" style="height: 35px;padding: 8px;"><div class="mediumText grayTextB" style="display:inline;">Vendor Rating:
            
             @if($get_average_rate != 0)<span id="rate" class="normalText grayText">
                <span id="averageRate" class="mediumText adRating"></span></span>
             @else
                <i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i>
             @endif
                <span class="grayTextB pull-right">2 <i class="fa fa-comment" aria-hidden="true"></i></span></div></div>
        </div> -->
      </div>
    </div>
 </div>
 </div>
<!--  <div class="container-fluid bgWhite">
    <?php echo $branches_public?> 
</div> -->
<div class="container-fluid bgGray borderBottom">
  <div class="container blockMargin">
    <div class="col-md-12 noPadding">
      <div class="col-md-9">



<div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding">  
    <div class="noPadding">
              <div class="panel-heading panelTitleBarLightB">
                   <span class="panelTitle">VENDOR BIDS</span>
                  <!--  <span class="redText pull-right cursor">View More</span> -->
               </div>
            @foreach ($vendorbidsmobile as $row)
      <div class="panel panel-default removeBorder borderZero bottomMarginLight ">
        <div class="panel-body rightPaddingB noPadding">
        <span class="label label-featured {{$row->feature == 0 ? 'hide':''}}">FEATURED</span>
         <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
        <div class="rightPadding nobottomPadding lineHeight">
          <div class="mediumText topPadding noPadding bottomPadding">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                <div class="visible-sm"><?php echo substr($row->title, 0, 35); 
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
                <div class="visible-xs"><?php echo substr($row->title, 0, 15);
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
            </a>
          <div class="normalText redText bottomPadding">
                 <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}">by {{$row->name}}</a>
          </div>
          </div>
          <div class="normalText redText bottomPadding">
          <div data-countdown="{{$row->ad_expiration}}"></div>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="normalText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <?php $i=0;?>
                               @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
          <div style="padding-top:8px;">
              @if($row->average_rate != null)
               <span id="vendor_bids_mobile_{{$row->id}}" class="blueText rightMargin pull-left"></span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
             @else
               <span class="blueText rightMargin">No Rating</span>
                        <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
              @endif 
           <!--  <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span> -->
          </div>
        </div>
        </div>
      </div>
      @endforeach
             <div class="panel-footer bgWhite text-center removeBorder">
            <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:240px;"><i class=""></i>View More</button>
          </div>
        </div>
          <div class="panel panel-default removeBorder borderZero">
          </div>
  </div>


        <div class="col-md-12 col-sm-12 hidden-xs hidden-sm noPadding">
        <div class="panel panel-default  removeBorder borderZero borderBottom {{count($vendorbids) == 0 ? 'hide':''}}" id="panel-bids-main">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Vendor Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
          <!--   <span class="redText pull-right cursor">View More</span> -->
          </div>
            <div class="panel-body" id="panel-bids">
                  <div class="col-lg-12 col-md-12 col-sm-12" id="main-bid-container">
                     <div id="latest-bids-container">
                  @foreach ($vendorbids as $row)  
                    <div class="col-lg-4 col-md-4 col-sm-4 noPadding" data-id="{{$row->id}}" id="bid-container">
                        <div class="sideMargin borderBottom">
                        <span class="label label-featured {{$row->feature == 0 ? 'hide':''}}">FEATURED</span>
                          <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                          </div></a>
                          <div class="minPadding nobottomPadding">
                            <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                              <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                                  if(strlen($row->title) >= 37){echo "..";} ?>
                             </div>
                              <div class="visible-md"><?php echo substr($row->title, 0, 25);
                                  if(strlen($row->title) >= 37){echo "..";} ?>
                              </div>
                            </a>
                            <div class="normalText grayText bottomPadding bottomMargin"> 
                            <div class="row">
                              <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}"> 
                               <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                             </div>
                             <div class="col-sm-6">
                                <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                                </span>
                              </div>
                            </div>
                            </div>
                            <div class="normalText bottomPadding">
                              <div class="row">
                                    <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                                     @if($row->city || $row->countryName)
                                     <i class="fa fa-map-marker rightMargin"></i>
                                     {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                                     @else
                                     <i class="fa fa-map-marker rightMargin"></i> not available
                                     @endif
                                     <?php $i=0;?>
                                      @if(count($row->getAdvertisementComments)>0)
                                          @foreach($row->getAdvertisementComments as $count)
                                               <?php $i++;?>     
                                          @endforeach   
                                      @endif
                                    </div>
                                  <div class="col-sm-6">
                                    <span class="pull-right">
                                     @if($row->average_rate != null)
                                       <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                                     @else
                                       <span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                                       <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                                     @endif 
                                    </span>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                             <div data-countdown="{{$row->ad_expiration}}"></div>
                        </div>
                      </div>
                    @endforeach
                        </div>
                      </div>
                  </div>
<!--                   <div class="text-center bottomPaddingB">
                    <input type="hidden" name="user_id" id="client-user_id" value="{{$rows->id}}">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:92.75px;"><i class=""></i>View More</button>
                  </div> -->
                </div>
            </div>


 <div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding">  
    <div class="noPadding">
              <div class="panel-heading panelTitleBarLightB">
                   <span class="panelTitle">LATEST ADS</span>
                  <!--  <span class="redText pull-right cursor">View More</span> -->
               </div>
            @foreach ($latestadsmobile as $row)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              <span class="label label-featured {{$row->feature == 0 ? 'hide':''}}">FEATURED</span>
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
              <div class="rightPadding nobottomPadding lineHeight">
                <div class="mediumText topPadding noPadding bottomPadding">
                      <div class="visible-sm"><?php echo substr($row->title, 0, 37); 
                       if(strlen($row->title) >= 37){echo "..";} ?>
                    </div>
                    <div class="visible-xs"><?php echo substr($row->title, 0, 17);
                       if(strlen($row->title) >= 37){echo "..";} ?>
                    </div>
                </div>
                    <div class="normalText redText bottomPadding">
                          <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}">by {{$row->name}}</a>
                    </div>
                    <div class="mediumText bottomPadding blueText"><strong>{{ number_format($row->price) }} USD</strong></div>
                          <div class="mediumText bottomPadding">
                           @if($row->city || $row->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>
                              <?php $i=0;?>
                               @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                                  
                    <div class="minPadding">
                       @if($row->average_rate != null)
                         <span id="latest_ads_mobile_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="blueText rightMargin">No Rating</span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
             <div class="panel-footer bgWhite text-center removeBorder">
            <button class="btn blueButton borderZero" type="button" id="btn-view-more-bids" style="width:240px;"><i class=""></i>View More</button>
          </div>
        </div>
               <div class="panel panel-default removeBorder borderZero">
              </div>
  </div>


  <div class="col-md-12 col-lg-12 hidden-xs hidden-sm noPadding">
    <div class="panel panel-default removeBorder borderZero borderBottom {{count($lastestads) == 0 ? 'hide':''}}" id="panel-ads-main">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our latest listings now!</span>
       <!--  <span class="redText pull-right cursor">View More</span> -->
      </div>
        <div class="panel-body" id="panel-ads">
           <div class="col-lg-12" id="main-ads-container">
            <div id="latest-ads-container">
            @foreach ($lastestads as $row)
            <div class="col-lg-4 col-md-4 col-sm-3 bottomMarginB noPadding" data-id="{{$row->id}}">
                <div class="sideMargin borderBottom">
                <span class="label label-featured {{$row->feature == 0 ? 'hide':''}}">FEATURED</span>
                  <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div></a>
                  <div class="minPadding nobottomPadding">
                    <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                      <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                        if(strlen($row->title) >= 37){echo "..";} ?>
                      </div>
                      <div class="visible-md"><?php echo substr($row->title, 0, 25);
                        if(strlen($row->title) >= 37){echo "..";} ?>
                      </div>
                    </a>
                  <div class="normalText grayText bottomPadding bottomMargin"> 
                    <div class="row">
                      <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}"> 
                        <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                      </div>
                      <div class="col-sm-6">
                        <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="normalText bottomPadding">
                    <div class="row">
                      <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                        @if($row->city || $row->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                        @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                        @endif
                          <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                          @foreach($row->getAdvertisementComments as $count)
                          <?php $i++;?>     
                          @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-6">
                        <span class="pull-right">
                          @if($row->average_rate != null)
                            <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                            <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                            <span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                            <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                        </span>
                      </div>
                    </div>
                  </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
            </div>
            <div class="text-center">
            <input type="hidden" id="no_of_results" value="0">
            <button class="btn blueButton borderZero topMargin" type="button" id="btn-view-more-bids" style="width:240px;"><i class=""></i>View More</button>
        </div>
<!--             <nav aria-label="Page navigation">
  <ul class="pagination noMargin">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav> -->
<!--             <nav class="noPadding">
          <ul class="pagination noPadding noMargin">
            <li>
              <a href="#" aria-label="Previous">
                <span aria-hidden="true">«</span>
              </a>
            </li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li>
              <a href="#" aria-label="Next">
                <span aria-hidden="true">»</span>
              </a>
            </li>
          </ul>
        </nav> -->
          </div>

<!--           <div class="text-center bottomPaddingB">
            <input type="hidden" name="user_id" id="client-user_id" value="{{$rows->id}}">
             <button class="btn blueButton borderZero" type="button" id="btn-view-more-ads" style="width:92.75px;">View More</button>
           </div> -->
        </div>
      </div>
    </div>

 <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 sideBlock noPadding">
          
      <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
          <div class="fill" style="background: url({{ url('uploads/banner/advertisement/desktop').'/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg':$banner_placement_right->image}}); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
       </div>
      </div>
       <div class="panel-heading panelTitleBarLightB {{count($featured_ads) == 0  ? 'hide':''}}">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
                <a href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
              <div class="ellipsis"><a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a></div>
                    <div class="normalText bottomPadding bottomMargin">
                          <a class="normalText lightgrayText" href="{{ url('/') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
        </div>
  </div>




    </div>
  </div>
</div>
 <div class="container-fluid bgWhite borderBottom">
  <div class="container blockMargin">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 noPadding sideBlock">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panelTitle bottomPadding">Comments and reviews</div>
      </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomPadding {{$comments == null ? '' : '' }}">
      <div class="panel panel-default borderZero removeBorder noMargin" id="comment-pane">
      <div class="panel-body nobottomPadding">
     <?php echo $form?>
        </div>
        </div>
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topBorder noPadding comments-container">


        
      <?php if($comments == ""){ echo "<h4 class='text-center lightgrayText normalText borderBottom noComments''>No current comments and reviews</h4>";}else{echo $comments;}?>
       <!--  <div class="panel panel-default bgGray borderZero borderBottom removeBorder">
          <div class="panel-body nobottomPadding">
            <div class="commentBlock">
                    <div class="commenterImage">
                       <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                      </div>
                      <div class="commentText">
                      <div class="panelTitle bottomPadding">Username</div>
                      <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                      <div class="subText">
                        <span class="blueText normalText"><i class="fa fa-comments"></i> Reply</span>
                        <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                        <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                      </div>
                  <div class="panel panel-default bgGray borderZero removeBorder replyBlock bordertopLight blockTop">
                      <div class="panel-body norightPadding nobottomPadding">
                        <div class="commentBlock">
                          <div class="commenterImage">
                             <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                            </div>
                            <div class="commentText">
                            <div class="panelTitle bottomPadding">Username</div>
                            <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                            <div class="subText">
                              <span class="blueText normalText"><i class="fa fa-comments"></i> Reply</span>
                              <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                              <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                            </div>
                            </div>
                        </div>
                      </div>
                  </div>
                </div>
              </div>
            </div>
        </div> -->
       <!--  <div class="panel panel-default bgWhite borderZero removeBorder">
          <div class="panel-body nobottomPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                 <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                </div>
                <div class="commentText">
                <div class="panelTitle bottomPadding">Username</div>
                <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                <div class="subText">
                  <a id="show" role="button" class="blueText normalText"><i class="fa fa-comments"></i> Reply</a>
                  <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                  <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                </div>
              <div id="" class="panel panel-default borderZero removeBorder replyBlock bordertopLight blockTop inputReply hidden">
                  <div class="panel-body norightPadding nobottomPadding">
                    <div class="commentBlock">
                      <textarea class="form-control borderZero fullSize" rows="2" id="ad-comment" name="comment"></textarea>
                    </div>
                    <span class="pull-right">
                      <button class="btn normalText blueButton submitReply borderZero topMargin"> Submit</button>
                    </span>
                  </div>
              </div>
          </div>
        </div>
      </div>
        </div> -->
      </div>
  </div>
</div>
</div>


      <div class="container-fluid bgGray">
        <div class="container blockTop">
          <div class="col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
          <div class="panel panel-default  removeBorder borderZero">
            <div class="panel-heading panelTitleBarLight">
              <span class="panelTitle">Related Ads</span>  <span class="hidden-xs">| Check out other ads with the same criteria.</span>
            <!--   <span class="redText pull-right cursor">View More</span> -->
            </div>
            
              <div class="visible-xs visible-sm">
                <div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding blockBottom">

              @foreach($related_ads_mobile as $row)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
               <div class="panel-body noPadding">
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
              <div class="rightPadding nobottomPadding lineHeight">
                <div class="mediumText grayText topPadding bottomPadding">{{ $row->title }}</div>
                  <div class="normalText redText bottomPadding">by {{ $row->name }}</div>
                  <div class="bottomPadding mediumText blueText"><strong> {{ $row->price }} USD</strong></div>
                   <div class="mediumText bottomPadding">
                           @if($row->city || $row->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>
                                <?php $i=0;?>
                               @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding">
                       @if($row->average_rate != null)
                         <span id="related_ads_mobile_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="blueText rightMargin">No Rating</span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
                  <!-- <div class="mediumText bottomPadding">
                         @if($row->city || $row->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                  </div>
                  <div class="mediumText blueText">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-empty"></i>
                    <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
                  </div> -->
            </div>
<!--                 <div class="row">
                <a href="{{ url('/ads/view') . '/' . $row->id }}">
                <div class="col-md-5 col-sm-5 col-xs-5">
                  <div class="fill" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
                   </div>
                  </div>
                  </a>
                <div class="col-md-7 col-sm-7 col-xs-7 noleftPadding">
                  <div class="mediumText grayText topPadding bottomPadding">{{ $row->title }}</div>
                  <div class="normalText redText bottomPadding">by {{ $row->name }}</div>
                  <div class="bottomPadding mediumText blueText"><strong> {{ $row->price }} USD</strong></div>
                  <div class="mediumText bottomPadding">
                         @if($row->city || $row->countryName)
                          <i class="fa fa-map-marker rightMargin"></i>
                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                         @else
                          <i class="fa fa-map-marker rightMargin"></i> not available
                         @endif
                  </div>
                  <div class="mediumText blueText">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star-half-empty"></i>
                    <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
                  </div>
                  </div>
                </div> -->
             </div>
             </div>
             @endforeach

            <!--  </div> -->
            </div>
              </div>
              <div class="hidden-xs hidden-sm">
                <div class="panel-body">
              <div id="relatedAds" class="carousel slide carousel-fade" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                  <div class="item active">
                  <div class="col-lg-12 col-md-12">
                 @foreach ($related_ads  as $related_ad)
                    <div class="col-lg-3 col-md-3 noPadding">
                      <div class="sideMargin borderBottom">
                      <span class="label label-featured {{$related_ad->feature == 0 ? 'hide':''}}">FEATURED</span>
                        <a href="{{url('/').'/'.$related_ad->country_code.'/'.$related_ad->ad_type_slug.'/'.$related_ad->parent_category_slug.'/'.$related_ad->category_slug.'/'.$related_ad->slug}}">
                          <div class="adImage" style="background:  url({{ url('uploads/ads/thumbnail').'/'.$related_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                          </div>
                        </a>
                        <div class="minPadding nobottomPadding">
                           <div class="row">
                      <div class="col-sm-12 ellipsis cursor">
                          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                             {{ $related_ad->title }}
                          </a>
                        </div>
                      </div>
<!--                           <div class="mediumText noPadding topPadding bottomPadding">{{ $related_ad->title }}</div> -->
                          <div class="normalText grayText bottomPadding bottomMargin">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                     <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
                         <!--  <div class="normalText bottomPadding">
                               @if($related_ad->city || $related_ad->countryName)
                                <i class="fa fa-map-marker rightMargin"></i>
                                {{ $related_ad->city }} @if ($related_ad->city),@endif {{ $related_ad->countryName }}
                               @else
                                <i class="fa fa-map-marker rightMargin"></i> not available
                               @endif
                             <span class="pull-right ">
                              <span class="blueText rightMargin">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star-half-empty"></i>
                              </span>
                              <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                            </span>
                          </div> -->
                           <div class="normalText bottomPadding">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                        <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-6">
                      <span class="pull-right">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                             <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                    </div>
                                <?php $i=0;?>
                               @if(count($related_ad->getAdvertisementComments)>0)
                                  @foreach($related_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                          <!-- <div class="minPadding">
                             @if($related_ad->average_rate != null)
                               <span id="related_ad_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                               <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                             @else
                               <span class="blueText rightMargin">No Rating</span>
                               <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                             @endif 
                          </div> -->
                        </div>
                      </div>
                    </div>
                    @endforeach
                   </div>
                  </div>

                  @if(count($related_adset2) != "0")
                  <div class="item">
                  <div class="col-lg-12 col-md-12">
                 @foreach ($related_adset2  as $row)
                    <div class="col-lg-3 col-md-3 noPadding ">
                      <div class="sideMargin borderBottom">
                        <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                          <div class="adImage" style="background:  url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                          </div>
                        </a>
                        <div class="minPadding nobottomPadding">
                          <div class="row">
                      <div class="col-sm-12 ellipsis cursor">
                          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                             {{ $related_ad->title }}
                          </a>
                        </div>
                      </div>
<!--                           <div class="mediumText noPadding topPadding bottomPadding">{{ $related_ad->title }}</div> -->
                          <div class="normalText grayText bottomPadding bottomMargin">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                     <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
                         <!--  <div class="normalText bottomPadding">
                               @if($related_ad->city || $related_ad->countryName)
                                <i class="fa fa-map-marker rightMargin"></i>
                                {{ $related_ad->city }} @if ($related_ad->city),@endif {{ $related_ad->countryName }}
                               @else
                                <i class="fa fa-map-marker rightMargin"></i> not available
                               @endif
                             <span class="pull-right ">
                              <span class="blueText rightMargin">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star-half-empty"></i>
                              </span>
                              <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                            </span>
                          </div> -->
                           <div class="normalText bottomPadding">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                        <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-6">
                      <span class="pull-right">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="lightgrayText rightMargin"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></span>
                             <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                    </div>
                                <?php $i=0;?>
                               @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                          <!-- <div class="minPadding">
                             @if($row->average_rate != null)
                               <span id="related_adset2_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                               <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                             @else
                               <span class="blueText rightMargin">No Rating</span>
                               <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                             @endif 
                          </div> -->
                         <!--  <div class="normalText bottomPadding">
                               @if($row->city || $row->countryName)
                                <i class="fa fa-map-marker rightMargin"></i>
                                {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                               @else
                                <i class="fa fa-map-marker rightMargin"></i> not available
                               @endif
                             <span class="pull-right ">
                              <span class="blueText rightMargin">
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star"></i>
                              <i class="fa fa-star-half-empty"></i>
                              </span>
                              <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                            </span>
                          </div> -->
                        </div>
                      </div>
                    </div>
                    @endforeach
                   </div>
                  </div>
                  @endif
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#relatedAds" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; left:-10px;">
                  <span class="fa fa-angle-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#relatedAds" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; right:-10px;">
                  <span class="fa fa-angle-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
                
              </div>
          </div>
            </div>
          </div>
        </div>
        <div class="popup-box chat-popup" id="qnimate">
          <div class="popup-head">
        <div class="popup-head-left pull-left"> Send A Message </div>
            <div class="popup-head-right pull-right">
<!--             <div class="btn-group">
                      <button class="chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false">
                     <i class="glyphicon glyphicon-cog"></i> </button>
                    <ul role="menu" class="dropdown-menu pull-right">
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Clear Chat</a></li>
                    <li><a href="#">Email Chat</a></li>
                    </ul>
            </div> -->
            
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                      </div>
        </div>
      <div class="minPadding">
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <input class="topPadding fullSize" type="text" name="title" id="row-title" placeholder="Title here..">
      <input class="topPadding" type="hidden" name="reciever_id" id="row-reciever_id" value="{{$rows->id}}">
      <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message"></textarea>
<!--       <div class="btn-footer">
      <button class="bg_none"><i class="glyphicon glyphicon-film"></i> </button>
      <button class="bg_none"><i class="glyphicon glyphicon-camera"></i> </button>
            <button class="bg_none"><i class="glyphicon glyphicon-paperclip"></i> </button>-->
<!--       <button class="bg_none pull-right"><i class="glyphicon glyphicon-send"></i> </button> -->
          <div class="clearfix bottomPadding">
        <div class="popup-messages-footer">
        <button id="" class="btn normalText redButton borderZero pull-right"><i class="fa fa-paper-plane"></i> Submit</button>
      </span>
        {!! Form::close() !!}
      </div>
    </div>
      </div>
    </div>
</div>
<div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-message-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title panelTitle"><i class="fa fa-plane rightPadding"></i>Send Message</h4>
          </div>
      <div class="modal-body">
        <div id="form-notice"></div>
        <!-- <input class="fullSize" type="text" name="title" id="row-title" value="Inquiry ({{$rows->name}})">
        <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea> -->
  <div class="">
    <label for="email">Title:</label>
    <input class="form-control borderZero" type="text" name="title" id="row-title" value="Inquiry ({{$rows->name}})">
  </div>
  <div class="">
    <label for="pwd">Message:</label>
    <textarea class="form-control borderZero" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
  </div>
      </div>
      <div class="modal-footer">
        <input  type="hidden" name="reciever_id" id="row-reciever_id" value="{{$rows->id}}">
        <button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
        <button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
@include('user.vendor.form')
@stop