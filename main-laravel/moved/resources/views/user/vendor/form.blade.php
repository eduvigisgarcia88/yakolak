<div class="modal fade" id="modal-favorite-profile" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'add-favorite-profile', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_favorite_profile_form', 'files' => true)) !!}
			<div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Add to Favorite Profile</h4>
			</div>
			<div class="modal-body">
				<div id="form-favorite_prof_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Are you sure you want to add to Favourite Profiles?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="profile_id" id="row-profile_id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>



<div class="modal fade" id="modal-remove-favorite-profile" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'dashboard/favorite-profile-remove', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-remove_favorite_profile_form', 'files' => true)) !!}
			<div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Remove from Favorite Profile</h4>
			</div>
			<div class="modal-body">
				<div id="form-remove-favorite_prof_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Are you sure you want to remove from Favourite Profiles?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="profiles_id" id="row-profiles_id" value="">
				<input type="hidden" name="watchlist_type" id="row-watchlist_type" value="2">
				<input type="hidden" name="watchlist_id" id="row-watchlist_id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

