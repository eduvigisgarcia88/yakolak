@extends('layout.frontend')
@section('scripts')

@stop
@section('content')
<section id="profile">
<div class="container">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
			<div id="userSidebar">
				<div class="panel panel-default borderzero">
				  <div class="panel-heading pnl-heading">
				  		<div class="">
							<img src="{{ url('img').'/'}}avatar.jpg" height="" class="img-responsive">
						</div>
				  </div>
				<div class="panel-body">
					<span class="">
				  <button type="button" class="borderzero btn btn-success"><i class="fa fa-exchange visible-sm visible-xs"></i> <span class="hidden-sm hidden-xs">Change</span></button>
					</span>
				  <span class="pull-right">
					<button type="button" class="borderzero btn btn-danger"><i class="fa fa-remove visible-sm visible-xs"></i> <span class="hidden-sm hidden-xs">Remove</span></button>
					</span>
				</div>
				</div>
				<div class="panel panel-default borderzero">
				  <div class="panel-heading pnl-heading">
				  	<h4><span id="profile-name"></span><span class="pull-right"><a href="{{url('profile/view/'.Auth::user()->id)}}" target="_blank"><i class="fa fa-external-link"></i></a></span></h4>
				  </div>
				  <div class="panel-body panelBodyPaddingZero">
				  	<ul>
						  <li><a href="{{ url('profile/'.Auth::user()->id.'/ads') }} "><i class="fa fa-th"></i> Listings</a></li>
						  <li><a href="#news"><i class="fa fa-bell"></i> Alerts</a></li>
						  <li><a href="#contact"><i class="fa fa-user"></i> Account</a></li>
						  <li><a href="#about"><i class="fa fa-heart"></i> Watchlist</a></li>
						  <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out"></i> Logout</a></li>
						</ul>
					</div>
				</div>
				<button type="button" class="fullSize borderzero btn btn-danger"><i class="fa fa-trash"></i> Delete Account</button>
			</div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
		</div>

</div>
</section>

@stop