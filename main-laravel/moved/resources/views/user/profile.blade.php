@extends('layout.frontend')
@section('scripts')
<script>
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})
function refresh(){

	$_token = "{{ csrf_token() }}";
	 var id = "{{ $user_id }}";

	 var loading = $(".loading-pane");

	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
	 $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
	 	
	 	$.each(response.rows, function(index, value) {
	 		//console.log(index+"="+value);
	 		var field = $("#customer-" + index);

 			if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	 $("#profile-photo").attr('src', url + '/img/' + value + '?' + new Date().getTime());
             }
             
          }); 
      }, 'json');

	 loading.addClass("hide");
	}

	$(document).ready(function() {
	});

	 $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });
     $('.btn-remove').on('click', function() {
      var id = {{ $rows->id }};

      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      dialog('Remove Photo', 'Are you sure you want to remove your photo</strong>?', "{{ url('profile/photo/remove') }}", id);

     });

</script>
@stop
@section('content')
<!-- account start -->
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 noPadding">
      <div class="col-lg-3 col-md-3">
        <div class="col-lg-12 noPadding">
					
					<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBarLight">
								<span class="panelTitle">DASHBOARD</span>
							</div>
						
							<div class="panel-body normalText">
                <div class="input-group minPadding noleftPadding norightPadding">
                  <span class="input-group-btn">
                    <button class="btn redButton borderZero" type="button">Browse</button>
                  </span>
                  <input type="text" class="form-control borderZero inputBox" placeholder="Select photo">
                </div>
								<button class="btn blueButton borderZero noMargin fullSize" type="button">Upload Photo</button>
							</div>
						</div>
					
					<div class="panel panel-default removeBorder borderZero borderBottom">
							<div class="panel-body normalText">
								<ul class="nav nav-pills nav-stacked noPadding">
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i> Account<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-diamond  rightPadding"></i> Subscription <span class="redText">(Upgrade Plan)</span><i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-th rightPadding"></i> Listings<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-flag rightPadding"></i> Alerts<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-envelope rightPadding"></i> Messages<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-comments rightPadding"></i> Comments and Reviews<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-eye rightPadding"></i> Watchlist<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-ticket  rightPadding"></i> Rewards and Referrals<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-barcode  rightPadding"></i> Advertising Plans<i class="fa fa-angle-right pull-right"></i></a></li>
									<li><a href="#" class="borderbottomLight grayText"><i class="fa fa-area-chart rightPadding"></i> Reports<i class="fa fa-angle-right pull-right"></i></a></li>
								</ul>
							</div>
						</div>

        </div>
      </div>
			
			
      <div class="col-lg-9 col-md-9 ">
				

				
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBar">
						<span class="panelTitle">Account Settings</span>
					</div>
					<div class="panel-body">
						<div class="col-lg-12 borderbottomLight noPadding bottomPadding redText ">
							BASIC INFORMATION
						</div>
						
						<div class="col-lg-12 topPaddingB noPadding bottomMargin">
								<div class="col-lg-6 ">
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="first_name" placeholder="Input" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Profile Name</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="first_name" placeholder="Input" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="first_name" placeholder="Input" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
								</div>

								<div class="col-lg-6 ">
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Usertype</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
														<select class="form-control inputBox" id="sel1">
													    <option>1</option>
													    <option>2</option>
													    <option>3</option>
													    <option>4</option>
													  </select>
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="first_name" placeholder="Input" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>
										<div class="form-group bottomMargin">
											<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
											<div class="inputGroupContainer">
												<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
															<input name="first_name" placeholder="Input" class="form-control borderZero inputBox fullSize" type="text">
												</div>
											</div>
										</div>

								</div>
								<div class="col-lg-12">
										<div class="form-group bottomMargin">
											<label class="col-lg-12 col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Description</label>
											<div class="inputGroupContainer">
												<div class="input-group col-lg-12 col-md-9 col-sm-12 col-xs-12 ">
														<textarea class="form-control inputBox" rows="5" id="comment"></textarea>
												</div>
											</div>
										</div>
								</div>
						</div>	
						
						<div class="col-lg-12  borderbottomLight noPadding bottomPadding topMarginB bottomMargin redText">
							BRAND INFORMATION
							<span class="pull-right">ADD BRANCH</span>
						</div>
						
						
						<div class="col-lg-12 bordertopLight noPadding topPaddingB ">
							<button class="btn blueButton borderZero noMargin pull-right " type="button">Update</button>
						</div>
					</div>

				</div>
      </div>
				
				
      </div>
    </div>
  </div>
		
<!-- Remove Modal -->
  <div id="modal-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-remove-header" class="modal-header modal-yeah">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-remove-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-remove-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-remove-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-remove-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
@stop