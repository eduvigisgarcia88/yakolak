@extends('layouts.backend')

@section('scripts')
<script type="text/javascript">

	// refresh the list
	function refresh() {
			var loading = $(".load-refresh");
			var edit = $(".edit-contact");
			var services = $("#rows-services");
			var about = $("#rows-about");
			var header = $("#rows-header");
			var contact = $("#rows-contact");
			var contactpage = $("#rows-contactpage");
			var aboutjeddah = $("#rows-aboutjeddah");
			var maxillofacial = $("#rows-maxillofacial");
			var careerpage = $("#rows-careerpage");
			var servicespage = $("#rows-servicespage");
			$_token = "{{ csrf_token() }}";
			var url = "{{ url('/') }}";
			loading.removeClass('hide');
			
			$.post("{{ url('config/other/refresh') }}", { _token: $_token }, function(response) {
					// clear services table
					services.html("");
					about.html("");
					contact.html("");
					header.html("");

					// populate about us table
					$.each(response.header.data, function(index, row) {

							header.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + row.title_eng + '</td>' +
									'<td>' + row.desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/header/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});


					$.each(response.aboutjeddah.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							aboutjeddah.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + 'About Jeddah' + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/aboutjeddah/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit pull-right" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});
					$.each(response.contactpage.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							contactpage.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + 'Contact Page' + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/contactpage/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit pull-right" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									 '</td>' +
									'</tr>'
							);
					});
					$.each(response.maxillofacial.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							maxillofacial.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + 'Maxillofacial' + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/maxillofacial/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit pull-right" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									 '</td>' +
									'</tr>'
							);
					});
					$.each(response.careerpage.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							careerpage.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + 'Career Page' + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/careerpage/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit pull-right" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									 '</td>' +
									'</tr>'
							);
							careerpage.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + 'Services Page' + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/servicespage/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit pull-right" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									 '</td>' +
									'</tr>'
							);
					});

					// populate services table
					$.each(response.services.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							services.append(
									'<tr data-id="' + row.id + '">' +
									'<td><i class="fa fa-' + row.icon + ' fa-3x"></i></td>' +
									'<td>' + row.title_eng + '</td>' +
									'<td>' + desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/service/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit Services" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// populate about us table
					$.each(response.about.data, function(index, row) {

							var desc_eng;

							if (row.desc_eng.length < 200) {
								desc_eng = row.desc_eng;
							} else {
								desc_eng = row.desc_eng.substring(0, 200) + '...';
							}

							about.append(
									'<tr data-id="' + row.id + '">' +
									'<td>' + desc_eng + '</td>' +
									'<td>' +
										'<a href="' + url + '/config/other/about/' + row.id + '/edit" class="btn btn-xs btn-primary btn-edit" title="Edit About Us" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a>' +
									'</td>' +
									'</tr>'
							);
					});

					// populate contact information table
					$.each(response.contact.data, function(index, row) {

							contact.append(
									'<tr data-id="' + row.id + '">' +
									'<td>Address (English)</td>' +
									'<td>' + row.address_eng + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Address (Arabic)</td>' +
									'<td>' + row.address_arabic + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Phone Number</td>' +
									'<td>' + row.phone + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Email Address</td>' +
									'<td>' + row.email + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Clinic Hours (English)</td>' +
									'<td>' + row.clinic_eng + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Clinic Hours (Arabic)</td>' +
									'<td>' + row.clinic_arabic + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Facebook Link</td>' +
									'<td>' + row.facebook_link + '</td>' +
									'</tr>' +
									'<tr data-id="' + row.id + '">' +
									'<td>Twitter Link</td>' +
									'<td>' + row.twitter_link + '</td>' +
									'</tr>'
							);
					});

					loading.addClass('hide');
					edit.removeClass('hide');

			}, 'json');
	}
	
	$(document).ready(function() {

		refresh();

	});

</script>
@stop

@section('content')

<div class="container">
    @if (Session::has("notice"))
        <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <i class="fa fa-fw fa-exclamation-triangle"></i> {{ Session::get('notice.msg') }} <a target="_blank" href="{{ Session::get('notice.url') }}">View Here</a>
        </div>
    @endif
</div>
		<div class="container">

			<h3>HEADER</h3>
			<div class="table-responsive padding-none">
				<table id="header-lists" class="table">
					<thead>
						<tr>
							<th>TITLE</th>
							<th>DESCRIPTION</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-header">
					</tbody>
				</table>
			</div>

			<h3>ABOUT SAMAYA GROUP</h3>
			<div class="table-responsive padding-none">
				<table id="about-lists" class="table">
					<thead>
						<tr>
							<th>BODY</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-about">
					</tbody>
				</table>
			</div>

			<hr>
			<h3>SAMAYA GROUP SERVICES (HOME PAGE)</h3>
			<div class="table-responsive padding-none">
				<table id="service-lists" class="table">
					<thead>
						<tr>
							<th>ICON</th>
							<th>SERVICE TITLE</th>
							<th>DESCRIPTION</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-services">
					</tbody>
				</table>
			</div>

			<hr>
			<h3>CONTACT INFORMATION <i class="fa fa-refresh fa-spin load-refresh"></i><a href="{{ url('config/other/contact/' . $contact_id .'/edit') }}" class="btn btn-xs btn-primary btn-edit edit-contact hide" title="Edit Contact Information" style="width: 25px; margin-bottom: 5px;"><i class="fa fa-pencil"></i></a></h3>
			<div class="table-responsive padding-none">
				<table id="contact-lists" class="table">
					<tbody id="rows-contact">
					</tbody>
				</table>
			</div>

	<div class="table-responsive padding-none">
				<table id="contact-lists" class="table">
					<thead>
						<tr>
							<th>CUSTOM PAGES</th>
							<th><i class="fa pull-right fa-refresh fa-spin load-refresh"></i></th>
						</tr>
					</thead>
					<tbody id="rows-aboutjeddah">
					</tbody>
					<tbody id="rows-contactpage">
					</tbody>
					<tbody id="rows-maxillofacial">
					</tbody>
					<tbody id="rows-careerpage">
					</tbody>
				</table>
			</div>
		</div>

@stop