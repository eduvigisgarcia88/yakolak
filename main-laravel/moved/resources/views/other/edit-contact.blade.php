@extends('layouts.backend')

@section('scripts')

@stop

@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($contact, ['action' => ['OtherController@updateContact', $contact->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<div class="well">
		<a class="btn btn-goback" href="{{ url('config/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-address_eng', 'Address (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('address_eng', null, ['class' => 'form-control', 'id' => 'row-address_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-address_arabic', 'Address (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('address_arabic', null, ['class' => 'form-control', 'id' => 'row-address_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-phone', 'Phone Number', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('phone', null, ['class' => 'form-control', 'id' => 'row-phone']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-email', 'Email Address', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('email', null, ['class' => 'form-control', 'id' => 'row-email']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-clinic_eng', 'Clinic Hours (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('clinic_eng', null, ['class' => 'form-control', 'id' => 'row-clinic_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-clinic_arabic', 'Clinic Hours (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('clinic_arabic', null, ['class' => 'form-control', 'id' => 'row-clinic_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-facebook_link', 'Facebook Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('facebook_link', null, ['class' => 'form-control', 'id' => 'row-facebook_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-twitter_link', 'Twitter Link', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('twitter_link', null, ['class' => 'form-control', 'id' => 'row-twitter_link']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Contact Info</button>
		</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>

@stop