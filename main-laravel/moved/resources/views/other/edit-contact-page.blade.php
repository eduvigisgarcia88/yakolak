@extends('layouts.backend')

@section('scripts')
  <script type="text/javascript">
   	    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2,
           toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });
  </script>
@stop

@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($contactpage, ['action' => ['OtherController@updateContactPage', $contactpage->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<div class="well">
		<a class="btn btn-goback" href="{{ url('config/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-desc_eng', 'Body (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::textarea('desc_eng', null, ['class' => 'form-control summernote', 'id' => 'row-desc_eng summernote']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-desc_arabic', 'Body (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::textarea('desc_arabic', null, ['class' => 'form-control summernote', 'id' => 'row-desc_eng summernote', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Contact Page</button>
		</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>

@stop