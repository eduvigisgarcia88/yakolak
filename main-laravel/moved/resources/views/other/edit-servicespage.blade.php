@extends('layouts.backend')

@section('scripts')
  <script type="text/javascript">
 	    $(document).ready(function() {
      $('.summernote').summernote({
        height: 250,
        tabsize: 2,
           toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });
  </script>
@stop
@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($servicespage, ['action' => ['OtherController@updateServicesPage', $servicespage->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<!-- Nav tabs -->
  <ul class="myNavs nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a class="borderZero" href="#english" aria-controls="english" role="tab" data-toggle="tab">English</a></li>
    <li role="presentation"><a class="borderZero" href="#arabic" aria-controls="arabic" role="tab" data-toggle="tab">Arabic</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content myTabs">
    <div role="tabpanel" class="tab-pane active" id="english">
    	<div class="container-fluid">
  		<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-desc_eng_col1', 'Body Column 1 (English)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
				{!! Form::textarea('desc_eng_col1', null, ['class' => 'form-control summernote', 'id' => 'row-desc_eng_col1 summernote']) !!}
			</div>
			</div>
				<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-desc_eng_col2', 'Body Column 2 (English)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
				{!! Form::textarea('desc_eng_col2', null, ['class' => 'form-control summernote', 'id' => 'row-desc_eng_col2 summernote']) !!}
			</div>
			</div>
	    </div>
	  </div>
    <div role="tabpanel" class="tab-pane" id="arabic">
    	<div class="container-fluid">
  		<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-desc_arabic_col1', 'Body Column 1 (Arabic)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
				{!! Form::textarea('desc_arabic_col1', null, ['class' => 'form-control summernote', 'id' => 'row-desc_arabic_col1 summernote', 'style' => 'direction: rtl;']) !!}
			</div>
			</div>
			<div class="form-group">
			<div class="col-sm-3">
				{!! Form::label('row-desc_arabic_col2', 'Body Column 2 (Arabic)', ['class' => 'control-label font-color']) !!}
			</div>
			<div class="col-sm-9">
				{!! Form::textarea('desc_arabic_col2', null, ['class' => 'form-control summernote', 'id' => 'row-desc_arabic_col2 summernote', 'style' => 'direction: rtl;']) !!}
			</div>
			</div>
	    </div>
    </div>
  </div>
<div class="pan panel panel-default borderZero">
  <div class="panel-body">
  	<div class="row">
  		<div class="col-lg-6">
  			<a class="btn btn-goback btn-warning borderZero" href="{{ url('config/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
  		</div>
  		<div class="col-lg-6 text-right">
  			<button type="submit" class="btn btn-submit btn-primary borderZero"><i class="fa fa-save"></i> Update Services Page</button>
  		</div>
  	</div>
  </div>
</div>
</div>

	{!! Form::close() !!}
</div>

@stop