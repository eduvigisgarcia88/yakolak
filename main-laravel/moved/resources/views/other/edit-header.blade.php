@extends('layouts.backend')

@section('scripts')

@stop

@section('content')
 
<div class="container">
	@if ($errors->any())
	<div class="alert alert-dismissable alert-danger">
        <button type="button" class="close" data-dismiss="alert">×</button>
		@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
    </div>
	@endif
	{!! Form::model($header, ['action' => ['OtherController@updateHeader', $header->id], 'role' => 'form', 'method' => 'PATCH', 'class' => 'form-horizontal', 'files' => true]) !!}
	<div class="well">
		<a class="btn btn-goback" href="{{ url('config/other') }}"><i class="fa fa-arrow-left"></i> Go Back</a>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-title_eng', 'Title (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('title_eng', null, ['class' => 'form-control', 'id' => 'row-title_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-title_arabic', 'Title (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::text('title_arabic', null, ['class' => 'form-control', 'id' => 'row-title_arabic', 'style' => 'direction: rtl;']) !!}
		</div>
		</div><div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-desc_eng', 'Body (English)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::textarea('desc_eng', null, ['class' => 'form-control', 'id' => 'row-desc_eng']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-md-3 text-right">
			{!! Form::label('row-desc_arabic', 'Body (Arabic)', ['class' => 'control-label font-color']) !!}
		</div>
		<div class="col-md-9">
			{!! Form::textarea('desc_arabic', null, ['class' => 'form-control', 'id' => 'row-desc_eng', 'style' => 'direction: rtl;']) !!}
		</div>
		</div>
		<div class="form-group">
		<div class="col-sm-offset-3 col-sm-9">
			<button type="submit" class="btn btn-submit"><i class="fa fa-save"></i> Update Header</button>
		</div>
		</div>
	</div>
	{!! Form::close() !!}
</div>

@stop