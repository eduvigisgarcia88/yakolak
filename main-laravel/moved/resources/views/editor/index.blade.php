<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">

  <title>Editor</title>

  <!-- include jquery -->
   <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 

  <!-- include libs stylesheets -->
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" />
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" />

  <!-- include summernote -->
  <link rel="stylesheet" href="plugins/summernote.css">
  <script type="text/javascript" src="plugins/summernote.js"></script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('.summernote').summernote({
        height: 300,
        tabsize: 2
      });
    });
  </script>
</head>
<body>
<div class="container">
  <h4>Lately library
    <span class="label label-info">Bootstrap v3.3.5</span>
    <span class="label label-success">font-awesome v4.3.0</span>
  </h4>
  <form action="post.php" method="post">
  <textarea name="body" class="summernote"></textarea>
  <input type="submit" value="submit">
</form>
</div>
</body>
</html>
