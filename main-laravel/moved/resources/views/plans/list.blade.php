@extends('layout.frontend')
@section('scripts')
<script>
  $("#nav div a[href^='#']").on('click', function(e) {
   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top - 100
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 


      $("#content-wrapper").on("click", ".btn-plan-subscription-upgrade", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        var plan_id = $(this).data('plan');
        $("#row-id").val("");
      $("#row-id").val(id);
      $("#row-title").val(title);
      $("#row-plan_id").val(plan_id);
      $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
          $("#modal-form").find('form').trigger("reset");
          $(".modal-title").html('<strong> Upgrade to '+ title +' </strong>', title);
          $("#modal-plan-subscription").modal('show');    
    });


    $("#plan-price").on("change", function() { 
        // $("#btn-get-started").removeAttr('disabled','disabled');
        // $("#btn-get-started-below").removeAttr('disabled','disabled');
    });
    $("div a[href^='#']").on('click', function(e) {

       // prevent default anchor click behavior
       e.preventDefault();

       // animate
       $('html, body').animate({
           scrollTop: $(this.hash).offset().top - 100
         }, 400, function(){
   
           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = this.hash;
         });

    });
</script>
@stop
@section('content')
<div data-spy="scroll" data-target=".navbar" data-offset="50">
<div class="container-fluid bgGray">
  <div class="container bannerJumbutron nobottomMargin">
    <div class="col-sm-12 noPadding">
    <div class="jumbotron borderZero">
    <h1 class="blueText text-center">Sleek, Simple And Secure Yakolak Plans
      <div class="text-center"><a class="btn btn-lg blueButton borderZero noMargin btn-login scroll" href="#section1">CHOOSE PLANS</a></div>
    </h1>   
      <div class="fill" style="background: url({{url('uploads/tronImage.png')}}); background-position: center center; background-repeat: no-repeat; background-size: contain; height: 390px; width: 100%;">
  </div>
</div>
</div>
</div>
<div class="container-fluid bgWhite">
  <div class="container">
     <div id="Free" class="col-md-12">
          <div class="panel panel-default removeBorder borderZero borderBottom">
            <div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>FREE</strong></span></div><div class="panel-body"><div class="bottomPaddingB xxlargeText"><strong><span class="blueText">FREE</span> <span class="lightgrayText"> ZERO COST FOREVER</span></strong><span class="pull-right"><!-- <a class="btn btn-sm blueButton borderZero noMargin btn-login scroll" href="#section1">CHOOSE PLANS</a> --></span> </div>
            <div class="row borderbottomLight">                  
            </div>
             <div class="blockTop">
             <div class="lineHeightC  normalText grayText bottomPaddingC">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi congue leo eu erat elementum placerat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam at dui dui. Nunc rhoncus tellus eu neque ultrices molestie. Vivamus eget rutrum leo. Nulla dapibus ligula quis massa faucibus sollicitudin sed ac purus. Maecenas egestas rhoncus nunc, sit amet vestibulum lectus iaculis nec. Pellentesque at ipsum id odio suscipit commodo a a dui. Nam turpis nibh, imperdiet sed urna quis, auctor maximus quam. Cras scelerisque metus tempus ligula gravida blandit. Praesent dictum tempor quam varius laoreet. Morbi quis libero ut erat egestas lacinia laoreet nec mauris. Ut nec imperdiet lectus, at ullamcorper orci. Aenean ultrices iaculis augue at tempor.
              </div>
              <div class="col-lg-12 lineHeightC  noPadding">
              <div class="col-lg-4 noleftPadding">
              <div class="smallText normalText grayText">
                <span class=""> No. of Ads <span class="pull-right blueText bold">2</span></span>
              </div>
              <div class="smallText normalText grayText">
                <span class=""> No. of Auctions <span class="pull-right blueText bold">2</span></span>
              </div>
              <div class="smallText normalText grayText">
                <span class="">  No. of Image per Ad <span class="pull-right blueText bold">4</span></span>
              </div>
              <div class="smallText normalText grayText">
                <span class="">  No. of Videos per Ad <span class="pull-right blueText bold">1</span></span>
              </div>
              </div>
              <div class="col-lg-4">
              <div class="smallText normalText grayText">
                <span class=""> Total Free Points <span class="pull-right blueText bold">10 </span></span>
              </div>
              <div class="smallText normalText grayText">
                <span class=""> Total Bids <span class="pull-right blueText bold">10 </span></span>
              </div>
              <div class="smallText normalText grayText">
                <span class=""> Free SMS Notification <span class="pull-right blueText bold">23 </span></span>
              </div>
              </div>
              </div>
            </div>
            </div>
        </div>
      </div>
    <?php echo($plan); ?>
  </div>
</div>
<div class="container-fluid bgGray topPaddingC">
  <div class="container">
    <?php echo($plan_description); ?>
  </div>
</div>
</div>
@stop
@include('plans.form')

