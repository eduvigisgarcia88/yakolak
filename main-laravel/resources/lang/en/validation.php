<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'error-:attribute|*The :attribute must be accepted.',
    'active_url'           => 'error-:attribute|*The :attribute is not a valid URL.',
    'after'                => 'error-:attribute|*The :attribute must be a date after :date.',
    'alpha'                => 'error-:attribute|*The :attribute may only contain letters.',
    'alpha_dash'           => 'error-:attribute|*The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'error-:attribute|*The :attribute may only contain letters and numbers.',
    'array'                => 'error-:attribute|*The :attribute must be an array.',
    'before'               => 'error-:attribute|*The :attribute must be a date before :date.',
    'between'              => [
        'numeric' => 'error-:attribute|*The :attribute must be between :min and :max.',
        'file'    => 'error-:attribute|*The :attribute must be between :min and :max kilobytes.',
        'string'  => 'error-:attribute|*The :attribute must be between :min and :max characters.',
        'array'   => 'error-:attribute|*The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'error-:attribute|*The :attribute field must be true or false.',
    'confirmed'            => 'error-:attribute|*The :attribute confirmation does not match.',
    'date'                 => 'error-:attribute|*The :attribute is not a valid date.',
    'date_format'          => 'error-:attribute|*The :attribute does not match the format :format.',
    'different'            => 'error-:attribute|*The :attribute and :other must be different.',
    'digits'               => 'error-:attribute|*The :attribute must be :digits digits.',
    'digits_between'       => 'error-:attribute|*The :attribute must be between :min and :max digits.',
    'email'                => 'error-:attribute|*The :attribute must be a valid email address.',
    'exists'               => 'error-:attribute|*The selected :attribute is invalid.',
    'filled'               => 'error-:attribute|*The :attribute field is required.',
    // 'image'                => 'error-:attribute|*The :attribute must be an image.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'error-:attribute|*The selected :attribute is invalid.',
    'integer'              => 'error-:attribute|*The :attribute must be an integer.',
    'ip'                   => 'error-:attribute|*The :attribute must be a valid IP address.',
    'json'                 => 'error-:attribute|*The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'Field may not be greater than :max.',
        // 'file'    => 'error-:attribute|*Field may not be greater than :max kilobytes.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'error-:attribute|*Field may not be greater than :max characters.',
        'array'   => 'error-:attribute|*Field may not have more than :max items.',
    ],
    'mimes'                => 'error-:attribute|*The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'error-:attribute|*Field must be at least :min.',
        'file'    => 'error-:attribute|*Field must be at least :min kilobytes.',
        'string'  => 'error-:attribute|*The :attribute must be at least :min characters.',
        'array'   => 'error-:attribute|*Field must have at least :min items.',
    ],
    'not_in'               => 'error-:attribute|*The selected :attribute is invalid.',
    'numeric'              => 'error-:attribute|*The :attribute must be a number.',
    'regex'                => 'error-:attribute|*The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    // 'required'             => 'error-:attribute|*Field required',
    'required_if'          => 'error-:attribute|*The :attribute field is required when :other is :value.',
    'required_with'        => 'error-:attribute|*The :attribute field is required when :values is present.',
    'required_with_all'    => 'error-:attribute|*The :attribute field is required when :values is present.',
    'required_without'     => 'error-:attribute|*The :attribute field is required when :values is not present.',
    'required_without_all' => 'error-:attribute|*The :attribute field is required when none of :values are present.',
    'same'                 => 'error-:attribute|*The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'error-:attribute|*The :attribute must be :size.',
        'file'    => 'error-:attribute|*The :attribute must be :size kilobytes.',
        'string'  => 'error-:attribute|*The :attribute must be :size characters.',
        'array'   => 'error-:attribute|*The :attribute must contain :size items.',
    ],
    'string'               => 'error-:attribute|*The :attribute must be a string.',
    'timezone'             => 'error-:attribute|*The :attribute must be a valid zone.',
    'unique'               => 'error-:attribute|*The :attribute has already been taken.',
    'url'                  => 'error-:attribute|*The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
