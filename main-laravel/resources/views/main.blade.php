@extends('layout.frontend')
@section('scripts')
@stop
@section('content')


<!-- latest bids start -->
<div class="container-fluid bgGray">
<div class="container blockMargin noPadding">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
      <span class="pull-right"><a href="{{url('category/list')}}" class="redText">view more</a></span>
    </div>

    
    <div class="col-sm-12 col-xs-12 blockBottom noPadding">
    <div class="panel-body noPadding">
      @foreach ($adslatestbidsmobile as $row)
      <div class="panel panel-default removeBorder borderZero bottomMarginLight">
        <div class="panel-body noPadding">
         <a href="{{ url('/ads/view') . '/' . $row->id }}"> <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div></a>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}
            <div class="normalText redText bottomPadding">
                 <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
          </div>
          </div>
          <div class="normalText redText bottomPadding">
            D: 00 - H: 00 - M: 00
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="mediumText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <div>
           
           <!--  <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span> -->
          </div>
        </div>
        </div>
      </div>
      @endforeach
    </div>
    </div>
  </div>
</div>

  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs" >
    <div class="panel panel-default  removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
        <span class="pull-right"><a href="{{url('category/list')}}" class="redText">view more</a></span>
      </div>
      <div class="panel-body">
        <div id="bidsCarousel" class="carousel slide carousel-fade" data-ride="carousel">


          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
        
            <div class="item active">
            <div class="col-lg-12 col-md-12">
              @foreach ($adslatestbids as $row)
              <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">

                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div>
                @endforeach
            </div>
            </div>
             



             <div class="item">
            <div class="col-lg-12 col-md-12">
            @foreach ($adslatestbids_panel2 as $row)
              <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div>
              </div>
              @endforeach     
            </div>         
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#bidsCarousel" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; left:-3px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#bidsCarousel" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; right:-3px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
  </div>
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default bottomMarginLight removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Ads</span> 
      <span class="pull-right"><a href="{{url('category/list')}}" class="redText">view more</a></span>
    </div>
     
     <div class="col-sm-12 col-xs-12 noPadding">
      <div class="panel-body noPadding">
     @foreach ($latestadsmobile as $row)
        <div class="panel panel-default borderZero removeBorder bottomMarginLight">
          <div class="panel-body noPadding">
              <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
          <div class="normalText redText bottomPadding ellipsis">
                 <a class="normalText lightgrayText" href="{{ url('/') . '/' . $row->alias }}"> by {{ $row->name }}</a>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="mediumText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
          </div>
        </div>
         @endforeach
    </div>
    </div>
  </div>
<!--   <div class="panel panel-default marginbottomHeavy removeBorder borderZero">
    <div class="panel-body noPadding">
      <div class="col-sm-12 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            by Dell Distributor
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div> -->
</div>
<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
    <div class="panel panel-default removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our members latest ads now!</span>
        <span class="pull-right"><a href="{{url('category/list')}}" class="redText">view more</a></span>
      </div>
      <div class="panel-body">
        <div id="adsCarousel" class="carousel slide carousel-fade" data-ride="carousel">
          <!-- Indicators -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
            
            <div class="item active">
            <div class="col-lg-12 col-md-12">
              @foreach ($latestads as $row)
              <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin bottomMarginB borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
              </div>
                @endforeach
            </div>
            </div>


           <div class="item">
            <div class="col-lg-12 col-md-12">
            @foreach ($latestads_panel2 as $row)
              <div class="col-lg-3 col-md-3 noPadding ">
                <div class="sideMargin borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">{{ $row->title }}</div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">
                        <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                      </span>
                    </div>
                  </div>
                </div>
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
              </div>
              @endforeach     
            </div>         
            </div>
      </div>











          <!-- Left and right controls -->
          <a class="left carousel-control" href="#adsCarousel" role="button" data-slide="prev" style="background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; left:-3px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#adsCarousel" role="button" data-slide="next" style="background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; right:-3px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
      </div>
    </div>
    </div>
  </div>
<!-- latest end -->
@stop
