@extends('layout.frontend')
@section('scripts')
<script>

$_token = "{{ csrf_token() }}";
function refresh(){
   var id = "{{ $user_id }}";
   var loading = $(".loading-pane");
   loading.removeClass("hide");
   var url = "{{ url('/') }}";
   console.time('jquery');
  
   $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
    $.each(response.rows, function(index, value) {
      var field = $("#client-" + index);
            console.log(index+"="+value);
          if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
               $("#profile-name").text(value)
             }
             if(index=="photo"){
               $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }
            if(index == "date_of_birth"){
              $("#client-date_of_birth").val(moment(value).format('MM/DD/YYYY'));
            }
            if(index == "company_start_date"){
              $("#client-company_start_date").val(moment(value).format('MM/DD/YYYY'));
            }
            if(index == "usertype_id"){
              if(value == "1"){
                $("#accountBranchContainer").addClass("hide");
                $("#founded-date-pane").addClass("hide");
                $("#location-pane").removeClass("hide");
              }else{
                $("#accountBranchContainer").removeClass("hide");
                $("#founded-date-pane").removeClass("hide");
                $("#location-pane").addClass("hide");
              }
            }
       
            if(index == "alias"){
              $("#vanity-url").attr("href","{{URL::to('/')}}"+value);
              $("#customer-alias").val(value);
            }
            if(index == "country"){
               $("#client-country").html(response.user_country);
            }
            if(index == "city"){
              $("#client-city").html(response.user_city);
            }
          }); 

      }, 'json');
  
   loading.addClass("hide");
  }
  function refreshSettings(){
   var id = "{{ $user_id }}";
   var loading = $("#profileSettings").find(".loading-pane");
   loading.removeClass("hide");
   var url = "{{ url('/') }}";
   $.post("{{ url('profile/fetch-info/'.$user_id) }}", { id: id, _token: $_token }, function(response) {
    $.each(response.rows, function(index, value) {
            if(index == "country"){
               $("#profileSettings").find("#client-geo-country").val(value);
               $("#client-country").val(value);
            }
            if(index == "city"){
               $("#profileSettings").find("#client-geo-city").val(value);
               $("#client-city").val(value);
            }
            if(index == "localization"){
              $("#profileSettings").find("#client-geo-localization").val(response.localization);
            }
            if(index == "weekly_newsletter"){

            }
            if(index == "monthly_newsletter"){

            }
            if(index == "promotion_newsletter"){

            }
            if(index == "usertype_id"){
              if(value == "1"){
                $("#accountBranchContainer").addClass("hide");
                $("#founded-date-pane").addClass("hide");
                $("#location-pane").removeClass("hide");
              }else{
                $("#accountBranchContainer").removeClass("hide");
                $("#founded-date-pane").removeClass("hide");
                $("#location-pane").addClass("hide");
              }
            }
            if(index == "alias"){
              $("#vanity-url").prop("href","{{URL::to('/')}}"+value);
              $("#customer-alias").val(value);
            }
          
            
          }); 
        location.reload();
        loading.addClass("hide");
      }, 'json');
  
  }

    function refreshWatchlist(){
          $_token = "{{ csrf_token() }}"; 
          $(".watchlist-container").addClass("hide");
          var panel_watchlists = $("#watchlist-panel");
          panel_watchlists.html(" ");
            $.post("{{ url('user/dashboard/watchlist/refresh') }}", { _token: $_token }, function(response) {
              panel_watchlists.html(response);
            }, 'json');       
    }

    function refreshVendorAdsActiveList(){
          $("#form-make_auction_notice").addClass("hide");
          var ads = $("#listings");
          ads.html(" ");
            $.post("{{ url('user/dashboard/listing/active/refresh') }}", { _token: $_token }, function(response) {
              ads.html(response);
            }, 'json');    
           
    }
    $("#ad-listings").on("click", function() {
        var listing = $("#listings");
        listing.html(" ");
          $.post("{{ url('user/dashboard/listing/active/refresh') }}", { _token: $_token }, function(response) {
            listing.html(response);
          }, 'json');
      
          
    });
    
    function refreshVendorAuctionActive(){
          $_token = "{{ csrf_token() }}";
          var auctionRefresh = $("#auctionAds");
          auctionRefresh.html(" ");
            $.post("{{ url('user/dashboard/auction/active/refresh') }}", { _token: $_token }, function(response) {
              auctionRefresh.html(response);
            }, 'json');    
    }

    $("#auction-listings").on("click", function() {
        var auction = $("#auctionAds");
        auction.html(" ");
          $.post("{{ url('user/dashboard/auction/active/refresh') }}", { _token: $_token }, function(response) {
            auction.html(response);
          }, 'json');
    });
      
    function refreshMessages(){
      var url = "{{ url('/') }}";
      var id = $("#row-message_id").val();
      var container = $("#message_container");
      $.post("{{ url('user/send/message/vendor/refresh') }}", { id: id, _token: $_token }, function(response) {
            container.html(response.message_content);
        }, 'json');

    }
    function viewMore(){
      var url = "{{ url('/') }}";
      var loading = $("#banner-pane").find('#load-form');
      loading.removeClass("hide");
      var search = $("#row-search-banner").val();
      var sort = $("#row-sort-banner").val();
      var order = $('#row-current-order-banner').val();
      console.log("viewmore = "+order);
      var container = $("#banner-container");
      var increment = $("#increment").val();
      $.post("{{ url('user/banner/refresh') }}",{sort:sort,order:order,search: search,increment: increment,_token: $_token }, function(response) {
            container.html(response.banner);
            if(response.banner != ""){
              $('#advertising-indicator').addClass("hide");
            }else{
              $('#advertising-indicator').removeClass("hide");
            }
            loading.addClass("hide");
            $("#count").val(response.total_banner_count);
            $("#increment").val(response.increment);
            var currentCount = $("#count").val();
            var currentInc = $("#increment").val();
            if(parseInt(currentInc) >= parseInt(currentCount)) {
              $("#btn-view-more-banners").addClass("hide");
            }else{
              $("#btn-view-more-banners").removeClass("hide");
            }
      },'json');
    }
     $(".banner-user-table").on("click", "#row-sort", function() {

          var loading = $("#banner-pane").find('#load-form');
          loading.removeClass('hide');
          $increment = 0;
          $count = 0;
          $page = $("#row-page").val();
          $order = $("#row-next-order-banner").val();
          console.log("sort = "+$order);
          $sort =  $(this).data('sort');   
          $search = $("#row-search-banner").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $(".banner-user-table").find("i").removeClass('fa-caret-up');
          $(".banner-user-table").find("i").removeClass('fa-caret-down');
          var table = $("#rows");
          var increment = $("#increment").val();
          var count = $("#count").val();
          var container = $("#banner-container");
          var sort_function = 1;

            $.post("{{ url('user/banner/refresh') }}",{sort:$sort,order:$order,search: $search,increment:increment,sort_function:sort_function,_token: $_token }, function(response) {
                    container.html(response.banner);
                    $('#row-pages').html(response.pages);
                    loading.addClass("hide");
                   
            }, 'json');

           if(parseInt(increment) >= parseInt(count)) {
              $("#btn-view-more-banners").addClass("hide");
            }else{
              $("#btn-view-more-banners").removeClass("hide");
           }
              $("#row-sort-banner").val($sort);
           if($order == "asc"){
                $("#row-next-order-banner").val('desc');
                $("#row-current-order-banner").val('asc');
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
            if($order == "desc"){
                $("#row-next-order-banner").val('asc');
                $("#row-current-order-banner").val('desc');
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');          
           }   
    }); 
    function refreshUserBanner(){

      var url = "{{ url('/') }}";
      var loading = $("#banner-pane").find('#load-form');
      loading.removeClass("hide");
      var search = $("#row-search-banner").val();
      var sort = $("#row-sort-banner").val();
      var order = $('#row-current-order-banner').val();
      var container = $("#banner-container");
      var increment = $("#increment").val();
      var save_function = 1;
      $(".banner-user-table").find("i").removeClass('fa-caret-up');
      $(".banner-user-table").find("i").removeClass('fa-caret-down');
      $.post("{{ url('user/banner/refresh') }}",{sort:sort,order:order,search: search,increment: increment,save_function:save_function,_token: $_token }, function(response) {
            container.html(response.banner);
            if(response.banner != ""){
              $('#advertising-indicator').addClass("hide");
            }else{
              $('#advertising-indicator').removeClass("hide");
            }
            loading.addClass("hide");
            $("#count").val(response.total_banner_count);

            var currentCount = $("#count").val();
            var currentInc = $("#increment").val();
            $("#increment").val(response.increment);
            if(parseInt(currentInc) >= parseInt(currentCount)) {
              $("#btn-view-more-banners").addClass("hide");
            }else{
              $("#btn-view-more-banners").removeClass("hide");
            }
      },'json');

    }
    
   $('#row-search-banner').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          var url = "{{ url('/') }}";
          var loading = $("#banner-pane").find('#load-form');
          loading.removeClass("hide");
          var search = $("#row-search-banner").val();
          var sort = $("#row-sort-banner").val();
          var order = $('#row-current-order-banner').val();
          var container = $("#banner-container");
          var increment = 5;
          var search_function = 1;

          $("#count").val(5);
          $(".banner-user-table").find("i").removeClass('fa-caret-up');
          $(".banner-user-table").find("i").removeClass('fa-caret-down');
          
          $.post("{{ url('user/banner/refresh') }}",{sort:sort,order:order,search_function:search_function,search: search,increment: increment,_token: $_token }, function(response) {
                container.html(response.banner);
                if(response.banner != ""){
                  $('#advertising-indicator').addClass("hide");
                }else{
                  $('#advertising-indicator').removeClass("hide");
                }
                loading.addClass("hide");
                $("#count").val(response.total_banner_count);
                $("#increment").val(response.increment);
                var currentCount = $("#count").val();
                var currentInc = $("#increment").val();
                if(parseInt(currentInc) >= parseInt(currentCount)){
                  $("#btn-view-more-banners").addClass("hide");
                }else{
                  $("#btn-view-more-banners").removeClass("hide");
                }
          },'json');
      }

   });

   $(".banner-user-table").on("click", ".btn-paypal-banner", function() {

      var id = $(this).parent().parent().data('id');
      var amount = $(".banner-user-table").find("#paypal-form").data('amount');
      $.post("{{ url('user/banner/validate') }}",{id:id,amount:amount,_token: $_token }, function(response) {
           $(".banner-user-table").find(".btn-paypal-banner").parent().find("#paypal-form").html(response.rows);
           $(".banner-user-table").find(".btn-paypal-banner").parent().find("#paypal-form").find(".paypal_btn").trigger('click');
      },'json');
      
   });

   function refreshMessageContainer(){
      var url = "{{ url('/') }}";
      var container = $("#message-tbody");
      $("#message_container").html("");
      $("#modal-send-message").addClass("hide");
      $.post("{{ url('user/message/refresh') }}", {_token: $_token }, function(response) {
            container.html(response.messages_header);
      }, 'json');
    }

    function refreshNewMeesage(){
      var url = "{{ url('/') }}";
      var container = $("#message-tbody");
      $("#message_container").html("");
      $("#modal-send-message").addClass("hide");
      $.post("{{ url('user/message/newMessage') }}", {_token: $_token }, function(response) {
            container.html(response.messages_header);
      }, 'json');
    }

    function purchaseBannerPlacement(){
      var url = "{{ url('/') }}";
      var title = $("#row-title").val();
      $.get("{{ url('banner/payment') }}", { title:title, _token: $_token}, function(response) {
        }, 'json');
       window.location.assign('banner/payment');
    }

    $("#content-wrapper").on("click", ".btn-delete-ads", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Delete Ad '+ title +' </strong>', title);
        $("#modal-ads-delete").modal('show');  
    });
    $('#banner_placement_plan_id').change(function() {
        var start_date = $("#row-start_date").val();
        var plan_id = $(this).val();
        $.post("{{ url('generate-expiration-date') }}", { plan_id: plan_id,start_date:start_date, _token: $_token }, function(response) {
          $("#row-expiration-date").val(response.generated_expiration_date);
        }, 'json');
       
    });
    // $('#row-page').change(function() {
    //     var id = $(this).val();
    //     if(id == 1){
    //       $("#banner-category-pane").addClass("hide");
    //     }else{
    //       $("#banner-category-pane").removeClass("hide");
    //     }
    //     var banner_placement = $("#row-banner_placement_id");
    //     $.post("{{ url('get-available-placement-plan') }}", { id:id, _token: $_token }, function(response) {
    //       banner_placement.html(response.rows);
    //     }, 'json');
    // });
    function computeDuration(){
      var duration = $("#row-featured_duration").val();
      var duration_metric  = $("#row-featured_duration_metric").val();
      var payment_method  = $("#row-payment_method").val();
      var id  = $("#modal-feature-ads").find("#row-id").val();
      var title  = $("#modal-feature-ads").find("#row-title").val();

        $("#total-point-cost").text('');
        $.post("{{ url('exchange-point-checker') }}", { payment_method:payment_method,duration: duration,duration_metric:duration_metric,_token :$_token }, function(response) {
            if(payment_method == 1){
               $("#payment_unit").html("points");
               $("#current_points").removeClass("hide");
               $("#buttons_for_reward_points").removeClass("hide");
               $("#buttons_for_paypal").addClass("hide");
            }else{

               var url = "{{url('payment/ad-feature').'/'}}"+'1016'+'/'+id+'/'+duration+'/'+duration_metric+'/'+"{{\Crypt::encrypt(Request::url())}}";
               $("#return_url").val(url);
               $("#row-title").val(title);
               $("#payment_unit").html("USD");
               $("#current_points").addClass("hide");
               $("#buttons_for_reward_points").addClass("hide");
               $("#buttons_for_paypal").removeClass("hide");

            }
          if(response.status == 1){
             $("#total-point-cost").text(response.computed_cost);
             $("#feature-error").addClass("hide");
             $("#btnFeature").removeAttr("disabled","disabled");
          }else{
             $("#btnFeature").attr("disabled","disabled");
             if (payment_method == 1) {
                $("#feature-error").removeClass("hide");
              } else {
                $("#feature-error").addClass("hide");
              }
             $("#total-point-cost").text(response.computed_cost);
          } 
          if($("#row-cost_points").val() == 0){
            $("#btnPaypalFeature").addClass("hide"); 
          }else{
            $("#btnPaypalFeature").removeClass("hide");
            $("#paypal-item-name").val("Feature Ad -"+title);
            $("#paypal-amount").val(response.computed_cost);
          }
          
         $("#row-cost_points").val(response.computed_cost);

        }, 'json');
    } 
    $("#content-wrapper").on("click", ".btn-enable-ads", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Enable Ads '+ title +' </strong>', title);
        $("#modal-enable-ads").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-ads-alert", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Disable Ad '+ title +' </strong>', title);
        $("#modal-ads-disable").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-ads-alert-list", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-disable-id").val("");
        $("#row-disable-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Disable Ad '+ title +' </strong>', title);
        $("#modal-ads-disable-list").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-disable-auction-alert", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>End Auction '+ title +' </strong>', title);
        $("#modal-auction-disable").modal('show');    
    });

    $("#content-wrapper").on("click", ".btn-edit-banner", function() {
         var id = $(this).parent().parent().data("id");
         $('#modal-form-purchase-banner').find("form").find(".alert").remove();
         // $(this).find("i").removeClass('fa-pencil').addClass('fa-circle-o-notch fa-spin');
         $.post("{{ url('user/dashboard/get-banner-info') }}", { id: id, _token: $_token }, function(response) {
            if(response.error){
       
               status('sample','Action not allowed', 'alert-danger'); 
            }else{
              $.each(response.row, function(index, value) {
              var field = $("#row-" + index);
                if(field.length > 0) {
                    field.val(value);
                }
                $("#modal-form-purchase-banner").find("#row-img").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+value+'?'+$.now());
                $("#modal-form-purchase-banner").find(".uploader-pane").addClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").attr("src","{{url('uploads/banner/advertisement/mobile').'/'}}"+response.row.image_mobile+'?'+$.now());
                $("#modal-form-purchase-banner").find("#row-img-desktop").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+response.row.image_desktop+'?'+$.now());
                $("#modal-form-purchase-banner").find("#change-photo-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#change-photo-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#preview").removeClass("hide");

                if(index == "id"){
                    $("#modal-form-purchase-banner").find("#row-id").val(value);
                }
                if(index=="amount"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val(value);
                    $("#modal-form-purchase-banner").find("#placement-price").val(value);
                }
                if(index=="metric_duration"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_duration").val(value);
                }
                if(response.page_location == "1"){
                    $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide");
                }else{
                    $("#modal-form-purchase-banner").find("#banner-category-pane").removeClass("hide");
                    $("#modal-form-purchase-banner").find("#ads-category").html(response.categoriesContainer);
                }
                $("#modal-form-purchase-banner").find("#row-title").val(response.row.title);
                $("#banner_price_pane").removeClass("hide");
                $("#modal-form-purchase-banner").find("#placement-price").html(response.banner_placement_price_container);
                $("#row-banner_placement_id").html(response.placementContainer);
                // $(this).find("i").removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                $('#modal-form-purchase-banner').find(".disable-view").removeAttr('disabled','disabled');
                $('#modal-form-purchase-banner').find(".alert").remove();
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").removeClass("hide");
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").text("Save");
                $('#modal-form-purchase-banner').find("#btnClose").addClass("hide");
                $('#modal-form-purchase-banner').find("#row-photo").removeAttr("required");
                $('#modal-form-purchase-banner').find("form").find("#btnClose").addClass("hide");
                $("#modal-form-purchase-banner").modal("show");
            });
            }
            
      }, 'json');

    });
    $("#content-wrapper").on("click", ".btn-view-banner", function() {
         var id = $(this).parent().parent().data("id");
         var func = 2;
         $('#modal-form-purchase-banner').find("form").find(".alert").remove();
         // $(this).find("i").removeClass('fa-pencil').addClass('fa-circle-o-notch fa-spin');
         $.post("{{ url('user/dashboard/get-banner-info') }}", { id: id, func:func,_token: $_token }, function(response) {
            $.each(response.row, function(index, value) {
              var field = $("#row-" + index);
                if(field.length > 0) {
                    field.val(value);
                }
                $("#modal-form-purchase-banner").find("#row-img").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+value+'?'+$.now());
                $("#modal-form-purchase-banner").find(".uploader-pane").addClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").attr("src","{{url('uploads/banner/advertisement/mobile').'/'}}"+response.row.image_mobile+'?'+$.now());
                $("#modal-form-purchase-banner").find("#row-img-desktop").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+response.row.image_desktop+'?'+$.now());
                $("#modal-form-purchase-banner").find("#change-photo-desktop").addClass("hide");
                $("#modal-form-purchase-banner").find("#change-photo-mobile").addClass("hide");
                $("#modal-form-purchase-banner").find("#preview").removeClass("hide");

                if(index == "id"){
                    $("#modal-form-purchase-banner").find("#row-id").val(value);
                }
                if(index=="amount"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val(value);
                    $("#modal-form-purchase-banner").find("#placement-price").val(value);
                }
                if(index=="metric_duration"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_duration").val(value);
                }
                if(response.page_location == "1"){
                    $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide");
                }else{
                    $("#modal-form-purchase-banner").find("#banner-category-pane").removeClass("hide");
                    $("#modal-form-purchase-banner").find("#ads-category").html(response.categoriesContainer);
                }
                $("#modal-form-purchase-banner").find("#row-title").val(response.row.title);
                $("#banner_price_pane").removeClass("hide");
                $("#modal-form-purchase-banner").find("#placement-price").html(response.banner_placement_price_container);
                $("#row-banner_placement_id").html(response.placementContainer);
                $('#modal-form-purchase-banner').find("form").find(".disable-view").attr('disabled','disabled');
                $('#modal-form-purchase-banner').find(".alert").remove();
                // $(this).find("i").removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").addClass("hide");
                $('#modal-form-purchase-banner').find("#btnClose").removeClass("hide");
                $("#modal-form-purchase-banner").modal("show");

            });
      }, 'json');
    });
    $('#accordion a').click(function() {
        $(this).find('i').toggleClass('fa-chevron-down fa-chevron-up');
    });
    $("#modal-form-purchase-banner").on("click", "#change-photo-mobile", function() {
        $(this).parent().parent().find(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
    });
    $("#modal-form-purchase-banner").on("click", "#change-photo-desktop", function() {
        $(this).parent().parent().find(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
    });
    $("#modal-form-purchase-banner").on("change", "#row-duration", function() {
        var duration = $(this).val();
        var amount = $("#placement-price").val();
        var price = $(".placement_amount").val();
        var total_amount = 0;
        var duration_metric = $(".placement_amount").find(':selected').data('metric');
        $("#row-placement_actual_amount").val(price);
        $("#row-placement_actual_duration").val(duration_metric);
        total_amount = parseFloat(duration) * parseFloat(amount);
        $("#row-total_cost").val(total_amount);
    });
    $("#modal-form-purchase-banner").on("change", ".placement_amount", function() {

       var price = $(this).val();
       var duration_metric = $(this).find(':selected').data('metric');
       console.log(price);
       console.log(duration_metric);
       var duration = $("#row-duration").val();
       $("#row-placement_actual_amount").val(price);
       $("#row-placement_actual_duration").val(duration_metric);

        // var duration = $("#row-duration").val();
        // var amount = $("#row-placement_actual_amount").val();
        var total_amount = 0;
        total_amount = parseFloat(duration) * parseFloat(price);
        $("#row-total_cost").val(total_amount);
    });  
    $(function () {
        $('#row-date_picker_start').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
        $("#message-pane").addClass("hide");
        // $("#content-wrapper").find(".readonly-view").attr("disabled","disabled");
    });
        $(function () {
        $('#row-date_picker_end').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY h:MM A'
        });
    });

   $(".btn-remove-message-dialog").click(function () {
          $("#modal-dialog-message").modal("hide");
   });
   // $("#content-wrapper").on("click", "#btn-change-mode", function() {

   //    if($(this).text() == "Edit"){
   //      $("#content-wrapper").find("form").find(".readonly-view").removeAttr("disabled","disabled");
   //      $("#customer-alias").removeAttr("disabled","disabled");
   //      $("#btn-save_button").removeClass("hide");
   //      $(this).text("Read-only");
   //    }else{
   //      $(this).text("Edit");
   //      $("#content-wrapper").find("form").find(".readonly-view").attr("disabled","disabled");
   //      $("#customer-alias").attr("disabled","disabled");
   //      $("#btn-save_button").addClass("hide");
   //    }
   // });
   $("#content-wrapper").on("click", ".btn-enable-ads-alert", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong>Enable Ad '+ title +' </strong>', title);
        $("#modal-ads-enable-alert").modal('show');    
  });
  
  $("#content-wrapper").on("click", ".btn-purchase-ads", function() {
    var generated =Math.random().toString(36).substr(2, 9);
    $("#modal-form-purchase-banner").find("#change-photo-desktop").addClass("hide");
    $("#modal-form-purchase-banner").find("#change-photo-mobile").addClass("hide");
    $("#modal-form-purchase-banner").find("#row-img-desktop").addClass("hide");
    $("#modal-form-purchase-banner").find("#row-img-mobile").addClass("hide");
    $("#modal-form-purchase-banner").find("#row-img-label-desktop").addClass("hide");
    $("#modal-form-purchase-banner").find("#row-img-label-mobile").addClass("hide");
    $("#modal-form-purchase-banner").find(".uploader-pane").removeClass("hide");
    $('#modal-form-purchase-banner').find("form").find(".alert").remove();
    $("#modal-form-purchase-banner").find("form")[0].reset();
    $("#modal-form-purchase-banner").find(".alert").remove();
    $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val('');  
    $("#modal-form-purchase-banner").find("#banner_price_pane").addClass("hide"); 
    $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide"); 
    $("#modal-form-purchase-banner").find("#row-id").val("");
    $('#modal-form-purchase-banner').find("form").find(".disable-view").removeAttr('disabled','disabled');
    $('#modal-form-purchase-banner').find(".disable-view").removeAttr('disabled','disabled');
    $('#modal-form-purchase-banner').find("#btnPurchaseBanner").removeClass("hide");
    $('#modal-form-purchase-banner').find("#btnClose").addClass("hide");
    $('#modal-form-purchase-banner').find("#btnPurchaseBanner").text("Purchase");
    $("#modal-form-purchase-banner").find("#row-order_no").val(generated);
    $("#modal-form-purchase-banner").modal("show");
  });  
  // $("#modal-form-purchase-banner").on("change", "#row-country_id", function() {
  //   console.log("dsdsd");

  // });   
  $("#content-wrapper").on("click", ".btn-send-msg", function() {
    var id = $(this).data('id');
    var title = $(this).data('title');
    $("#modal-send-msg").modal("show");
    $("#row-reciever_id").val(id);
    $("input#row-title").val("Inquiry for "+title);
  });
  
   $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });
  $('.btn-remove').on('click', function() {
      var id = {{ $rows->id }};
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Remove Photo', 'Are you sure you want to remove your photo</strong>?', "{{ url('profile/photo/remove') }}", id);
  });

   $("#content-wrapper").on('click','.btn-purchase-bid', function() {
      var id = $(this).data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Purchase Bid', 'Do you to purchase this bid point?</strong>?', "{{ url('bid/purchase') }}", id);
  });

  $("#content-wrapper").on("click", "#btn-feature", function() {
    var id = $(this).data('feature-id');
    var title = $(this).data('ad-title');
    $('#modal-feature-ads').find("#row-id").val(id);
    $('#modal-feature-ads').find("#row-title").val(title);
    $('#modal-feature-ads').modal("show"); 
      // $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      // dialog('Feature Ad', 'Make this a feature ad</strong>?', "{{ url('ad/feature') }}", id);
  });
  $("#content-wrapper").on("click", "#btn-unfeature", function() { 
      var id = $(this).data('feature-id');
      $(".modal-header").addClass("modalTitleBar");
      $(".modal-title").removeAttr("class").addClass("modal-title-bidder panelTitle");
      dialog('UnFeature Ad', 'Warning:</strong> Unfeaturing a listing will not refund the transaction.Do you want to continue?', "{{ url('ad/unfeature') }}", id);
  });
  $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $("#get_tel_prefix").val("");
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
            $("#client-tel_no_prefix").val(response.tel_prefix);
            $("#client-mobile_no_prefix").val(response.tel_prefix);
          }, 'json');
       }
  });
  $("#row-country_id").on("change", function() { 
        var id = $("#row-country_id").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#row-city");
        city.html(" ");
        if ($("#row-country_id").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
 });
 $("#modal-form-purchase-banner").on("change","#row-country_id",function(){
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $("#modal-form-purchase-banner").find("#row-city");
        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
  });
$("#client-geo-country").on("change", function() { 
        var id = $("#client-geo-country").val();
        $("#get_tel_prefix").val("");
        $_token = "{{ csrf_token() }}";
        var city = $("#client-geo-city");
        city.html(" ");
        if ($("#client-geo-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
            $("#profileSettings").find("#client-geo-localization").val(response.client_localization);
            $("#client-tel_no_prefix").val(response.tel_prefix);
            $("#client-mobile_no_prefix").val(response.tel_prefix);
          }, 'json');
       }
  });
 $("#client-bid-point-package").on("change", function() { 
        $_token = "{{ csrf_token() }}";
        var id = $(this).val();
        var container = $("#paypal-button-container");
        container.html(" ");
        if ($(this).val()) {
          $.post("{{ url('bid/payment') }}", { id: id, _token: $_token }, function(response) {
            container.append(response.form);
         
          }, 'json');
       }
 });
 $("#accountBranchContainer").on("change", "#account-company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().parent().parent().find("#account-company_city");

        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.html(response.rows);
          }, 'json');
       }
  });
$("#modal-form-purchase-banner").on("change","#row-page",function(){
     var id = $("#row-page").val(); 
     $("#homepage-guide").addClass("hide");
     $("#browse-guide").addClass("hide");
     $("#listings-guide").addClass("hide");
     if(id == 1){
      $("#homepage-guide").removeClass("hide");
      $(".banner-category-pane").addClass("hide");
     }
     if(id == 2){
      $("#browse-guide").removeClass("hide");
      $(".banner-category-pane").removeClass("hide");
     }
     if(id == 3){
      $("#listings-guide").removeClass("hide");
      $(".banner-category-pane").removeClass("hide");
     }
     var country_id = $("#row-country_id").val();
     var containerPlacement = $("#row-banner_placement_id");
 
     var containerCategory = $("#ads-category");
         containerPlacement.html(" ");
         containerCategory.html(" ");
         // containerPlacementPrice.html(" ");
       if ($(this).val()) {
          $.post("{{ url('get-banner-placements') }}", { id: id,country_id:country_id, _token: $_token }, function(response) {
            containerPlacement.append(response.banner_placement_container);
            
            // containerCategory.append(response.categories);
            // containerPlacementPrice.append(response.banner_placement_price_container);
          }, 'json');
      }
});
$("#modal-form-purchase-banner").on("change","#row-banner_placement_id",function(){
    var id = $("#row-page").val();
    var containerPlacementPrice = $("#placement-price");
      containerPlacementPrice.html("");
       if ($(this).val()) {
          $.post("{{ url('get-banner-placements-price') }}", { id: id, _token: $_token }, function(response) {
            // containerPlacement.append(response.banner_placement_container);
            // containerCategory.append(response.categories);
            containerPlacementPrice.append(response.banner_placement_price_container);
            $("#banner_price_pane").removeClass("hide");
          }, 'json');
      }
});

 var branchAccount = $("#accountBranch").html();
 $(".btn-add-account-branch").click(function() {
       $("#accountBranchContainer").append("<div id='accountBranch'>"+branchAccount+"</div>");
       var country = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_country").val();
       var city = $("#accountBranchContainer").find('accountBranch:first-child').find("#account-company_city").html();
       $("#branchContainer").find('#accountBranch:last-child').find("#account-company_country").val(country);
       $("#branchContainer").find('#accountBranch:last-child').find("#account-company_city").html(city);
       $("#accountBranchContainer").find('#accountBranch:last-child').find(".btn-add-account-branch").removeClass("btn-add-account-branch").addClass("btn-del-account-branch").html("<i class='fa fa-minus redText'></i>");
     $("#accountBranchContainer").find('#accountBranch:last-child input:text').val("");
     $("#accountBranchContainer").find('#accountBranch:last-child').find("#branch_id").prop("value","0");
     $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_country option:eq(0)").attr('selected','selected');
     $("#accountBranchContainer").find('#accountBranch:last-child').find("#account-company_city option:eq(0)").attr('selected','selected');       
 });

 $("#accountBranchContainer").on("click", ".btn-del-account-branch", function() { 
   var current =  $(this).parent().parent().parent();
   var branch_id = current.find('#branch_id').val();
   current.find('#branch_status').val("2")
   // current.addClass("hide");
   current.remove();
   $("#accountBranchContainer").append('<input type="hidden" name="delete_branch[]" value="'+branch_id+'">');
 });
  

  $("#nav div a[href^='#']").on('click', function(e) {
   // prevent default anchor click behavior
   e.preventDefault();
   // store hash
   var hash = this.hash;
   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top - 100
     }, 1000, function(){
       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });
    });

    $("#replyBlock").hide();
    $(".btn-message-back").hide();
    $(".btn-message-back").on("click", function() { 
            $("#messageBlock").show();
            $("#replyBlock").hide();
            $(".btn-message-back").hide();          
    });
    


$("#content-wrapper").on("click", ".view-message", function() {
      // location.reload("{{$messages_counter}}");
      // $('#message-count-container').load("{{$messages_counter}}");
      $(this).parent().data('unread', 1);
      $("#messageBlock").hide();
      $(".btn-message-back").show();
      $("#replyBlock").show();
      var id = $(this).data('header-id');
      $("#row-message_id").val(id);
      $_token = "{{ csrf_token() }}";
      $("#message-pane").removeClass("hide");
      $("#modal-send-message").removeClass("hide");
      $(this).find('i').attr('class', 'fa fa-envelope-o grayTextB rightPadding').next('span').attr('class', 'mediumText grayTextB');
      $(this).parent().find('span.message_date').attr('class', 'message_date rightPadding grayTextB normalWeight');
      var container = $("#message_container");
      var container_title = $("#message-title-pane");
      $('#message-pane').find(".loading-pane").removeClass("hide");
      container.html("");
      $('#messageInboxPane').addClass('bottomPaddingC').removeClass('nobottomPadding');
      $.post("{{ url('user/messages') }}", { id: id, _token: $_token }, function(response) {
      container.html(response.message_content);
      container_title.text(response.message_title);
      $('#message-pane').find(".loading-pane").addClass('hide');
    }, 'json');
      $('#view_more_inbox').hide();
      $(this)
     $.post("{{ url('user/send/message/vendor/status') }}", { id: id, _token: $_token }, function(response) {
      var message_count_container = $("#message-count-container");
      if(response.updated_count == 0){
        message_count_container.next('i').attr('class', 'fa fa-envelope-o');
      } else {
        message_count_container.next('i').attr('class', 'fa fa-envelope');
      }

      message_count_container.text(response.updated_count);
      
      
     }, 'json');

}); 
      $("#myTableMessage").on("click", "#delete-message", function() {
          var id = $(this).data('id');
          $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
          dialog('Delete Message', 'Are you sure you want to delete this message</strong>?', "{{ url('user/message/delete') }}", id);
          
      }); 

      $('#back_button_inbox').click(function(){
        $('#view_more_inbox').show();
        $('#messageInboxPane').removeClass('bottomPaddingC').addClass('nobottomPadding');

      });

      $("#content-wrapper").on("click", ".btn-plan-subscription-upgrade", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        var plan_id = $(this).data('plan');
        var user_id = $(this).data('user_id');
        $("#row-id").val("");
        $("#row-id").val(id);
        $("#row-title").val(title);
        $("#row-plan_id").val(plan_id);
        $("#row-user_id").val(user_id);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong> Upgrade to '+ title +' </strong>', title);
        $("#modal-plan-subscription").modal('show');
      });

      // $("#row-banner_placement_id").on("change", function() { 
      //   var id = $(this).val();
      //   if(id == 5 || id == 6 || id == 7){
      //      $.post("{{ url('get-available-banner-slot') }}", { id: id, _token: $_token }, function(response) {
      //         if(response == 1){
      //           $("#error-placement-slot").addClass("hide");
      //           $("#btnPurchaseBanner").removeAttr("disabled","disabled");
      //         }else{
      //           $("#error-placement-slot").removeClass("hide");
      //           $("#btnPurchaseBanner").attr("disabled","disabled");
      //         }
      //     }, 'json');
      //   }
      //   $("#banner_placement_plan_id").removeAttr("disabled", "disabled");
      // });  


     $("#row-banner_placement_id").on("change", function() { 
        var id = $("#row-banner_placement_id").val();
        $_token = "{{ csrf_token() }}";
        var banner = $("#banner_placement_plan_id");
        banner.html(" ");
        if ($("#row-banner_placement_id").val()) {
          $.post("{{ url('get-bannerplacementplans') }}", { id: id, _token: $_token }, function(response) {
            banner.append(response);
          
          }, 'json');
         }
           $("#banner_placement_plan_id").removeAttr("disabled","disabled");
      });

  $("#content-wrapper").on("change", "#plan_term", function() {
      var id = $(this).val();
      var loading = $(this).parent().parent().parent().parent().find("#load-price");
      $(this).parent().parent().parent().parent().find("#row-price_id").val(id);
      loading.removeClass("hide");
      var price_container = $(this).parent().parent().parent().parent().find("#plan_price_container");
      price_container.html("");
       if (id) {
          $.post("{{ url('user/get-prices') }}", { id: id, _token: $_token }, function(response) {
            price_container.html(response);
            loading.addClass("hide");
          }, 'json');
      }
  });   
  $("#client-usertype_id").on("change", function() { 
      if($(this).val() == 1){
          $("#account-setting").find(".individual-pane-one").removeClass("hide");
          $("#individual-pane-two").removeClass("hide");
          $("#accountBranchContainer").addClass("hide");
          $("#account-setting").find(".founded-date-pane").addClass("hide");
          $('#profile_name_change').text('Vendor Name');
          $('#user_type_text').text('/vendor/');
        
      }else if($(this).val() == 2){
          $("#account-setting").find(".individual-pane-one").addClass("hide");
          $("#individual-pane-two").addClass("hide");
          $("#accountBranchContainer").removeClass("hide");
          $("#account-setting").find(".founded-date-pane").removeClass("hide");
          $('#profile_name_change').text('Company Name');
          $('#user_type_text').text('/company/');
      }
  });

  $("#inviteUser").on("click", "#contact_src", function() {
    $(this).find("form").find("#row-email_id").multiselect('refresh');
    if( $(this).val() =="auto"){
      $("#auto-pane").removeClass("hide");
      $('#manual-pane').addClass("hide");
      $('#auto-pane').find("#row-email_id").attr('name','email[]');
      $("#contact-type").val("1");
    }else{
      $("#auto-pane").addClass("hide");
      $('#manual-pane').removeClass("hide");
      $('#manual-pane').find("input[type=text]").attr('name','email[]');
      $("#contact-type").val("2");
    }
  });
  $("#content-wrapper").on("click", ".btn-banner-view", function() {
        var id = $(this).parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
              $.post("{{ url('dasboard/banner/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Ads ['+response.title+']</strong>');
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);                
                          }
                      });
                      $("#row-ads_type_id").attr('disabled','disabled');
                      $("#row-ads_category").attr('disabled','disabled');
                      $("#row-title").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-countryName").attr('disabled','disabled');
                      $("#row-city").attr('disabled','disabled');
                      $("#row-address").attr('disabled','disabled');
                      $("#row-youtube").attr('disabled','disabled');
                      $("#row-price").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form-purchase-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');  
      });



 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
   });
 });

 $("#content-wrapper").on("click", ".btn-make-auction", function() {
    var ads_id = $(this).data('ads_id');
    var title = $(this).data('title');
    $("#row-ads_id").val(ads_id);
    $("#row-title").val(title);
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
 
    $.post("{{ url('user/dashboard/listing/form/make-auction') }}", { ads_id: ads_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-title").html('<strong>Make Auction  '+ title +' </strong>', title);
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
                  if(index=="price"){
                    $("#ads-price").attr('value', +value);
                 }
              });
              // show form
               $("#modal-form").find('form').trigger("reset");
               $("#modal-make-auction").modal('show');

            
          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });

 $("#content-wrapper").on("click", ".btn-start-auction", function() {
    var ads_id = $(this).data('ads_id');
    var title = $(this).data('title');
    $("#row-auction_id").val(ads_id);
    $("#row-title").val(title);
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
 
    $.post("{{ url('user/dashboard/listing/form/make-auction') }}", { ads_id: ads_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-title").html('<strong>Make Auction  '+ title +' </strong>', title);
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
                  if(index=="price"){
                    $("#ads-price").attr('value', +value);
                 }
              });
              // show form
               $("#modal-form").find('form').trigger("reset");
               $("#modal-start-auction").modal('show');

          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });




 $("#content-wrapper").on("click", ".btn-remove-watchlist",function() { 
        var watchlist_id = $(this).parent().parent().parent().data('watchlist_id');
        var watchlist_type = $(this).parent().parent().parent().data('watchlist_type');
        var title = $(this).parent().parent().parent().data('title');
        var btn = $(this);
        $("#row-watchlist_id").val(watchlist_id);
        $("#row-watchlist_type").val(watchlist_type);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        if(watchlist_type == 1 || watchlist_type == 3) {
           $(".modal-title").html('<i class="fa fa-trash"></i> <strong>Remove Ads ['+ title +']</strong>');
           $(".modal-watchlist-body").html('Are you sure you want to remove this ad?');
        } else if (watchlist_type == 2) {
           $(".modal-title").html('<i class="fa fa-trash"></i> <strong>Remove Profile ['+ title +']</strong>');
           $(".modal-body").html('Are you sure you want to remove this profile?');
        }
        $("#modal-watchlist-remove").modal('show');
        // User Id Disabled
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
              $.post("{{ url('dashboard/favorite-profile/manipulate') }}", { watchlist_type: watchlist_type, watchlist_id: watchlist_id, _token: $_token }, function(response) {
                  if(!response.error) {
                     // output form data
                      $.each(response, function(index, value) {
                          var field = $("#watch-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);                
                          }
                      });
                      // show form
                     
                  } else {
                      status(false, response.error, 'alert-danger');
                  }
                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');  
      });



$("#content-wrapper").on("click", ".btn-send-a-referral", function() {
    var user_id = $(this).data('user_id');
    $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $.post("{{ url('user/dashboard/referral/manipulate') }}", { user_id: user_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
            $(".modal-header").html('<strong>Send Referral  </strong>');
           
              // output form data
              $.each(response.row, function(index, value) {
                  var field = $("#ads-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
              });
              // show form
              $("#modal-send_referral").find('form').trigger("reset");
              $("#modal-send_referral").modal('show');


          } else {
              status(false, response.error, 'alert-danger');
          }
      }, 'json');   
  });

    function sortByDate(){

      var url = "{{ url('/') }}";
      var loading = $("#messageInboxPane").find("#load-form");
      loading.removeClass('hide');
      var field = 'created_at';
      var order = $("#msg-sort").val();
      var container = $("#message-container");
      console.log(field+"="+container);
      $.post("{{ url('user/message/refresh') }}", { field:field,order:order,_token: $_token }, function(response) {
            container.html(response.messages_header);
             loading.addClass('hide');
        }, 'json');
       if($("#msg-sort").val() == "asc"){
        $("#msg-sort").val("desc");
        $("#sort-indicator").removeClass("fa-sort-amount-asc").addClass("fa-sort-amount-desc");
       }else{
        $("#msg-sort").val("asc");
        $("#sort-indicator").removeClass("fa-sort-amount-desc").addClass("fa-sort-amount-asc");
       }
    }
    function sortByStatus(){
      
      var url = "{{ url('/') }}";
      var loading = $("#messageInboxPane").find("#load-form");
      loading.removeClass('hide');
      var field = 'read_status';
      var order = $("#msg-sort").val();
      var container = $("#message-container");
      $.post("{{ url('user/message/refresh') }}", { field:field,order:order,_token: $_token }, function(response) {
            container.html(response.messages_header);
             loading.addClass('hide');
        }, 'json');


       if($("#msg-sort").val() == "asc"){
        $("#msg-sort").val("desc");
        $("#sort-indicator").removeClass("fa-sort-amount-asc").addClass("fa-sort-amount-desc");
       }else{
        $("#msg-sort").val("asc");
         $("#sort-indicator").removeClass("fa-sort-amount-desc").addClass("fa-sort-amount-asc");
       }
    }
     $('#email_id').multiselect();
     $('#row-email_id').multiselect({
      maxHeight: 400,
      enableCaseInsensitiveFiltering: true,
      selectAllValue: 'multiselect-all',
      selectAllText: 'Select All',
      includeSelectAllOption: true,
      numberDisplayed: 2
    });
    $("#referred_email").select2({
      minimumResultsForSearch: Infinity,
      tags: true,
      tokenSeparators: [',', ' ']
    });
  


// google.charts.load("current", {packages:['corechart']});
//     google.charts.setOnLoadCallback(drawChart);
//     function drawChart() {
//       var data = google.visualization.arrayToDataTable([
//         ["Element", "", { role: "style" } ],
//         ["Total Comments", {{ $total_comments_vendor }}, "#3498DB"],
//         ["Total Page Visit", {{ $total_page_visits }}, "#5DADE2"],
//       ]);

//       var view = new google.visualization.DataView(data);
//       view.setColumns([0, 1,
//                        { calc: "stringify",
//                          sourceColumn: 1,
//                          type: "string",
//                          role: "annotation" },
//                        2]);

//       var options = {
//         title: "",
//         width: 600,
//         height: 400,
//         chartArea:{left:0,top:0,width:'100%',height:'80%'},
//         bar: {groupWidth: "95%"},
//         legend: { position: "none" },
//       };
//       var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
//       chart.draw(view, options);
//   }

      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      // google.charts.setOnLoadCallback(function(){
      //   drawChart();
      // });














      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Hits','Comments'],
          ['{{Carbon\Carbon::today()->subDays(3)->toFormattedDateString()}}', {{$total_visits_three}}, {{$total_comments_three}}],
          ['{{Carbon\Carbon::today()->subDays(2)->toFormattedDateString()}}', {{$total_visits_two}}, {{$total_comments_two}}],
          ['{{Carbon\Carbon::today()->subDays(1)->toFormattedDateString()}}', {{$total_visits_one}}, {{$total_comments_one}}],
          ['{{Carbon\Carbon::today()->toFormattedDateString()}}', {{$total_visits_today}}, {{$total_comments_today}}]
        ]);

        var options = {
          hAxis: {title: '',  titleTextStyle: {color: '#5da4ec'}},
          vAxis: {maxValue: {{$highest_visit_count == 0 ? 4 : $highest_visit_count}}, minValue: {{$lowest_visit_count}}, format: "0", gridlines: { count: 4 },},
          legend: { position: "bottom" },
          chartArea:{left:85, top:10, bottom:60, width:'80%',height:'80%'},
          colors: ['#5da4ec','#ff6d6d']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('columnchart_values'));
        chart.draw(data, options);
      }

      google.charts.setOnLoadCallback(drawChartMobile);
      // google.charts.setOnLoadCallback(function(){
      //   drawChart();
      // });

      function drawChartMobile() {
        var data = google.visualization.arrayToDataTable([
          ['Date', 'Hits','Comments'],
          ['{{Carbon\Carbon::today()->subDays(3)->toFormattedDateString()}}', {{$total_visits_three}}, {{$total_comments_three}}],
          ['{{Carbon\Carbon::today()->subDays(2)->toFormattedDateString()}}', {{$total_visits_two}}, {{$total_comments_two}}],
          ['{{Carbon\Carbon::today()->subDays(1)->toFormattedDateString()}}', {{$total_visits_one}}, {{$total_comments_one}}],
          ['{{Carbon\Carbon::today()->toFormattedDateString()}}', {{$total_visits_today}}, {{$total_comments_today}}]
        ]);

        var options = {
          hAxis: {title: '',  titleTextStyle: {color: '#5da4ec'}},
          vAxis: {maxValue: {{$highest_visit_count == 0 ? 4 : $highest_visit_count}}, minValue: {{$lowest_visit_count}}, format: "0", gridlines: { count: 4 },},
          legend: { position: "bottom" },
          chartArea:{width:'inherit',height:'inherit'},
          colors: ['#5da4ec','#ff6d6d']
        };

        var chart = new google.visualization.AreaChart(document.getElementById('columnchart_values_mobile'));
        chart.draw(data, options);
      }

      $(window).resize(function(){
        drawChart();
        drawChartMobile();
      });

      $('.repClicked').click(function(){
        setTimeout(function(){ drawChart(); }, 10);
      });
 

      /* CLICK LINKS START */
    var url = window.location.href;
    var hash = url.substring(url.indexOf('#'));
    $(document).ready(function(){
        var id = "{{$rows->country }}";
        $_token = "{{ csrf_token() }}";
        var city = $("#modal-form-purchase-banner").find("#row-city");
        city.html(" ");
        if (id) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
      if(hash == "#messages"){
          $("#messages-link").click();
         }else if(hash== "#listings"){
           $("#listings-link").click();
         }else if(hash== "#watchlist"){
           $("#watchlist-link").click();
         }else if(hash== "#settings"){
           $("#settings-link").click();
         }else if(hash== "#auction"){
           $("#auction-link").click();
         }else if(hash== "#subscription"){
           $("#subscription-link").click();
         }else if(hash == "#comments"){
           $(".comClicked").click();
         }else if(hash == "#reports"){
           $(".repClicked").click();
         }else if(hash == "#rewards"){
           $(".rewClicked").click();
         }else if(hash == "#alerts"){
           $(".aleClicked").click();
         }else if(hash == "#invoice"){
           $(".invClicked").click();
         }else if(url){
           $("#account-link").click();
         }
        function mobileSidebarFold(){
            $("#mobilesidebarAccount").removeClass("active");
            $("#mobilesidebarAccount").find('nav').removeClass("active");
            $("#wrapper").removeClass("active");
        }
        $(".mesClick").click(function() {
           $(".mesClicked").click();
            mobileSidebarFold();
            $('#panel-profile').hide();
        });
        $(".aleClick").click(function() {
           $(".aleClicked").click();
            mobileSidebarFold();
            $('#panel-profile').hide();
        });
        $(".adlClick").click(function() {
           $("#listings-link").click();
            mobileSidebarFold();
            $('#panel-profile').hide();
        });
        $(".aucClick").click(function() {
           $("#auction-link").click();
            mobileSidebarFold();
            $('#panel-profile').hide();
        });
        $(".comClick").click(function() {
           $(".comClicked").click();
             mobileSidebarFold();
             $('#panel-profile').hide();
        });
        $(".repClick").click(function() {
           $(".repClicked").click();
             mobileSidebarFold();
             $('#panel-profile').hide();
        });
        $(".invClick").click(function() {
           $(".invClicked").click();
             mobileSidebarFold();
             $('#panel-profile').hide();
        });
         $(".setClick").click(function() {
           $("#settings-link").click();
             mobileSidebarFold();
             $('#panel-profile').hide();
        });
         $(".rewClick").click(function() {
           $(".rewClicked").click();
             mobileSidebarFold();
             $('#panel-profile').hide();
        });

        $("#messages-id").click(function() {
           $("#messages-link").click();
        });
        $("#listings-id").click(function() {
           $("#listings-link").click();
        });
        $("#auclistings-id").click(function() {
           $("#auction-link").click();
        });
        $("#subscription-id").click(function() {
           $("#subscription-link").click();
        });
        $("#watchlist-id").click(function() {
           $("#watchlist-link").click();
        });
         $("#settings-id").click(function() {
           $("#settings-link").click();
        });

        var id = $("#client-bid-point-package").val();
        var container = $("#paypal-button-container");
        container.html(" ");
        if ($("#client-bid-point-package").val()) {
          $.post("{{ url('bid/payment') }}", { id: id, _token: $_token }, function(response) {
            container.append(response.form);
          }, 'json');
       }
    });

    
   /* CLICK LINKS END */
    // $(".").on("click", function(event){
    //     $("#row-photo_upload").val("");
    //     $("#file_name").val("");
    // });

//   $("#panel-profile").on("click", ".btn-img-delete", function() { 
//   $(".modal-header").removeAttr("class").addClass("modal-header modalTitleBar");
//   dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Delete Profile Picture</strong></h4>', 'Are you sure you want to delete your profile picture?');
//   $.post("{{ url('profile/photo/remove') }}",{id:id, _token: $_token}, function(response) {
//           console.log(response);
//          // $("#profile-photo").attr('src', url + '/uploads/' ++ '?' + new Date().getTime());  
//     }, 'json');
  
// });

  $("#panel-profile").on("click", ".btn-img-delete", function() { 
      var id = {{Auth::user()->id}};
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Delete Profile Picture</strong></h4>', 'Are you sure you want to delete your profile picture?', "{{ url('profile/photo/remove') }}", id);
  });

   /* Disabled Input, Textarea, and Dropdowns set to cursor 'default' START */
    $("input.readonly-view, select.readonly-view, textarea.readonly-view, label.readonly-view, label.radio-inline").css("cursor","default");
   /* Disabled Input, Textarea, and Dropdowns set to cursor 'default' END */
   // /* White background page loader to prevent broken pages from showing - START*/
   //  $(window).load(function() {
   //    $(".loader").fadeOut("slow");
   //  });
   // /* White background page loader to prevent broken pages from showing - END*/
    @if(Auth::check())
    var id = "{{Auth::user()->id}}";
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city-for-dashboard') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
    var photo = "{{$rows->photo}}";
    if(photo == "default_image.png"){
      // $(".btn-img-delete").attr('disabled','disabled')
      $(".btn-img-delete").addClass('grayButtonB')
    }else{
      // $(".btn-img-delete").removeAttr('disabled','disabled')
      $(".btn-img-delete").removeClass('grayButtonB')
    }  
   @endif

   var plan_expiry = '{{$rows->plan_expiration}}';
   var user_plan = '{{$rows->user_plan_types_id}}';
   var current_time = "<?php $date = new DateTime(); echo $date->format('Y-m-d H:i:s'); ?>";
   var new_plan_expiry = new Date(plan_expiry);
   var new_current_time = new Date(current_time);
   var time_diff = new_plan_expiry - new_current_time;
   var time_diff_in_seconds = time_diff / 1000;
     if(user_plan != 1 || user_plan != 5  || plan_expiry != ''){
     /* SUBSCRIPTION */
      $('.planIdentify').countdown(new_plan_expiry)
      .on('update.countdown', function(event){
        var format = '%DD-%HH-%MM';
        
//           if(event.offset.totalDays > 0 && event.offset.totalDays < 11){
//             format = '%-d day%!d';
//           }else{
//             format = 'Upgrade Plan';
//           }
        
//           if(event.offset.weeks > 0){
//             format = '%-m month%!m';
//           }else{
//             format = 'Upgrade Plan';
//           }
        
//           if(event.offset.months > 0){
//             format = '%-w month%!w';
//           }else{
//             format = 'Upgrade Plan';
//           }
        
        if(/NaN/i.test(event.strftime(format)) == false){
          $(this).text('('+event.strftime(format)+')');
        }
//         console.log(!isNaN(event.strftime(format)));
//           if(event.offset.totalDays > 0){
//             $('.durationPlan').text('');
//           }else{
//             format = 'Upgrade Plan';
//           }
//           if(event.offset.weeks > 0){
//             $('.durationPlan').text(event.strftime(format));
//           }else{
//             $('.durationPlan').text('');
//           }
        })
      .on('finish.countdown', function(event) {
        $(this).html('(Upgrade Plan)');
        $('.durationPlan').text('Expired');
      });
     /* SUBSCRIPTION */
     $('a#subscription-link').click(function(){
       if(time_diff_in_seconds < 604800){ //604800 seconds = 7 days
        $('.expiry-countdown').countdown(new_plan_expiry)
          .on('update.countdown', function(event){
            var format = '%-H h and %-M min';
            if(event.offset.totalDays > 0){
              format = '%-d day%!d';
            }
            if(event.offset.weeks > 0){
              format = '%m week%!w';
            }
            $(this).text('Your plan will expire in '+event.strftime(format));
            })
          .on('finish.countdown', function(event) {
            $(this).html('Plan expired!');
          });
        var array = $('.plan_discount').map(function(){
               return $.trim('<div class="col-sm-4"><div class="panel panel-default borderZero removeBorder nobottomMargin"><div class="panel-heading planTitleBar borderbottomPlan text-center discounts_contain"><span class="panelTitle blueText plan_name">'+$(this).data('plan_name')+'</span></div><div class="panel-body "><div class="xlargeText xxlargeText blueText text-center"><b class="plan_price">$'+$(this).data('plan_price')+'/mo</b></div></div></div></div>');
            }).get();
        $('div.discounted_container').html(array.join(''));
        var discount_desc = $('div.discount_description').data('discount_description');
        console.log(discount_desc);
        $('div.discounts_contain').children().each(function(){
            $("span.plan_name:contains('{{strtoupper($current_user_plan_type)}}')").parent().parent().remove();
            // $("span.plan_name:contains('BRONZE')").remove();
        });
        $('#modalExpiration').modal('show');
        $("button.yesUpgrade").click(function(){
        $('html, body').animate({
            scrollTop: $("#subscription-upgrade").offset().top
            }, 320);
        });
       }
    })
    }else{
      $('.planIdentify').html("Free");
    }
    
   function readURL(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
       
        reader.onload = function (e) {
            if(id =="desktop"){
               $("#row-img-desktop").attr('src', e.target.result);
               $("#row-img-desktop").removeClass('hide');
              
            }else{
               $("#row-img-mobile").attr('src', e.target.result);
               $("#row-img-mobile").removeClass('hide');
              
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

  $("#modal-form-purchase-banner").on("change", ".photoName", function() {
    var id = $(this).data('id');
    readURL(this, id);
     var filenameDesktop = "";
     var filenameMobile = "";
     if(id =="desktop"){
        filenameDesktop = $(this).val().split("\\").pop();
        $("#row-file_name_desktop").val(filenameDesktop);
     }else{
         filenameMobile = $(this).val().split("\\").pop();
        $("#row-file_name_mobile").val(filenameMobile);
     }
});

$(".uploader-pane").on("click", ".removeImage", function() { 
    var id = $(this).data('id');
    if(id=="desktop"){
      $("#row-img-desktop").removeAttr("src");
      $("#row-img-desktop").addClass("hide");
      $("#row-file_name_desktop").val("");
    }else{
      $("#row-img-mobile").removeAttr("src");
      $("#row-img-mobile").addClass("hide");
      $("#row-file_name_mobile").val("");
    }
});
var account_status = "{{$rows->sms_verification_status}}";
if(account_status == 0){
  $('span.account_status').click(function(){
    $('.modal_verification').text('VERIFY YOUR ACCOUNT');
    $('#modalVerify').modal('show');
  });
}
$('#modalVerify .close').click(function(){
  $('.alert-danger').hide();
});
$('#modalVerify .cancel').click(function(){
  $('.alert-danger').hide();
});
$('#sms_send_code').submit(function(e){
  e.preventDefault();
  var mobile_number = $('.mobile_no_verify').val();
  var _token = "{{csrf_token()}}";
  $.ajax({
    type: 'POST',
    url: 'dashboard/sms-send-code',
    data: {mobile_number: mobile_number, _token: _token},
    beforeSend: function(){$('button.sms_verify_btn').attr('disabled','disabled').html('Sending <i></i>').children().addClass('fa fa-spinner fa-spin');},
    complete: function(){$('button.sms_verify_btn').removeAttr('disabled').html('Sent <i></i>').children().addClass('fa fa-check');},
    success: function(data){$('.form_container').children().remove();$('.form_container').html(data);}
  });
});

function startTimer(){
  var counter = 180;
  setInterval(function() {
    counter--;
    if (counter >= 0) {
      $('.newSMSCode').text('Code sent. Wait: '+counter+'s');
    }
    if (counter === 0) {
        $('.newSMSCode').html('New code <i></i>').removeAttr('disabled');;
        clearInterval(counter);
    }
  }, 1000);
}

$(document).ready(function(){
  $('button.multiselect').css('border-radius','0');
  $('button.multiselect').css('height','34px');
 
});
$(document).ready(function(){
  $('.alerts_table_color').find('td').find('span.alerts_table_font').children().each(function() {
  $(this).addClass('grayTextB').css('font-size','14px').hover(function(e) { 
    $(this).css("color",e.type === "mouseenter"?"#999999":"#999999");
  })
});
});

 $("#modal-feature-ads").on("click", "#btnFeature", function(e) { 
    var loading = $("#load-form-feature");
      // stop form from submitting
       e.preventDefault();
     var form = $('#modal-feature-ads').find("form");
    loading.removeClass('hide');
        // push form data
    $.ajax({
      type: "post",
      url: '{{url('ad/feature')}}',
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          // var errors = '<ul>';

          // $.each(response.error, function(index, value) {
          //   errors += '<li>' + value + '</li>';
            
          // });

          // errors += '</ul>';

          // status('Please correct the following:', errors, 'alert-danger', '#form-notice');
          $('#feature-error').text(response.error[0]);
          $('#btnFeature').prop('disabled', true);
        }  else {

         $("#modal-feature-ads").modal('hide');
          status(response.title, response.body, 'alert-success');

          // refresh the list
          refreshVendorAdsActiveList();
          refreshVendorAuctionActive();
        }

        loading.addClass('hide');

      }
      });
  });
  
   $("#dashboard_rating").rateYo({
         starWidth: "28px",
         rating: {{ (round($total_final_rating,1) != null ? round($total_final_rating,1):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  $("#content-wrapper").on("click", ".btn-disable-banner", function() {
     var id = $(this).parent().parent().data('id');
     $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Banner', 'Are you sure you want to disable the Advertisement? <br>This will not refund the purchase', "{{ url('user/dashboard/banner/disable') }}",id);
  });
  $("#content-wrapper").on("click", ".btn-enable-banner", function() {
     var id = $(this).parent().parent().data('id');
      $.post("{{ url('banner/verify') }}",{id:id,_token: $_token }, function(response) {
                if(response.errorVerify) {
                   status('sample','Action not allowed', 'alert-danger'); 
                }else{
                  $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
                  dialog('Enable Banner', 'Are you sure you want to enable this banner</strong>?', "{{ url('user/dashboard/banner/enable') }}",id);
                }
      },'json');
    
  });

  function put_to_bottom(tbody_id) {
    $(document).find(tbody_id+' tr.hide').each(function() {
      $(this).appendTo(tbody_id);
    });
  }
  function sortByUnread(table, order, by) {
    var available_unread = $(document).find(".alerts_table_color[data-unread='1']").not('.hide').length;
    var current_visible = $(document).find(".alerts_table_color").not('.hide').length;
    var for_desc = $(document).find(".alerts_table_color[data-unread='1']").get();
    var for_asc = $(document).find(".alerts_table_color[data-unread='1']").get().reverse();
    var tbody = table.find('tbody');
      // sortTable(table, order, by);
      if (order == 'asc') {
        $(for_asc).each(function(){
          $(this).prependTo('#alerts_tbody');
          $('#sort_alerts_unread').val('desc');
        });
      } else {
        $(for_desc).each(function(){
          $(this).appendTo('#alerts_tbody');
          $('#sort_alerts_unread').val('asc');
        }); 
      }

      $(document).find(".alerts_table_color").addClass('hide');

      $(document).find(".alerts_table_color").each(function(k, v){
        if ((k+1) <= current_visible) {
          $(this).removeClass('hide');
        }
      });

      // put_to_bottom('#'+tbody.attr('id'));
  }

  function sortTable(table, order, by) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');

    $(document).find(tbody).find('tr').sort(function(a, b) {
        if (asc) {

          return $('td:first', a).parent('tr').data(by).toString().localeCompare($('td:first', b).parent('tr').data(by));

        } else {

          return $('td:first', b).parent('tr').data(by).toString().localeCompare($('td:first', a).parent('tr').data(by));
        }
    }).appendTo(tbody);
    
    put_to_bottom('#'+tbody.attr('id'));
    if(order == 'asc') {

      if(tbody.attr('id') == 'message-container') {
        if(by == 'date') {
          $('#sort_messages_date').val('desc');
        } else {
          $('#sort_messages_unread').val('desc');
        }        
      } else {
        // if(by == 'date') {
          $('#sort_alerts_date').val('desc');
        // } else {
          $('#sort_alerts_title').val('desc');
          $('#sort_alerts_description').val('desc');
          // $('#sort_alerts_unread').val('desc');
        // }        
      }

      
    } else {
      if(tbody.attr('id') == 'message-container') {
        // if(by == 'date') {
          $('#sort_messages_date').val('asc');
        // } else {
          $('#sort_messages_unread').val('asc');
        // }
      } else {
        // if(by == 'date') {
          $('#sort_alerts_date').val('asc');
        // } else {
          $('#sort_alerts_description').val('asc');
          $('#sort_alerts_title').val('asc');
          // $('#sort_alerts_unread').val('asc');
        // }
      }

    }
$('#sort_alerts_unread').val('asc');
  }


function sortInvoice(table, order, by) {
    var asc   = order === 'asc',
        tbody = table.find('tbody');

    $(document).find(tbody).find('tr').sort(function(a, b) {
      if (by == 'transaction_type') {
        if (asc) {
          return $('td:first', a).parent('tr').data(by).toString().localeCompare($('td:first', b).parent('tr').data(by));
        } else {
          return $('td:first', b).parent('tr').data(by).toString().localeCompare($('td:first', a).parent('tr').data(by));
        }
      } else {
        if (asc) {
          return parseInt($('td:first', a).parent('tr').data(by)) < parseInt($('td:first', b).parent('tr').data(by)) ? 1 : -1;
        } else {
          return parseInt($('td:first', b).parent('tr').data(by)) < parseInt($('td:first', a).parent('tr').data(by)) ? 1 : -1;
        }
      }
    }).appendTo(tbody);

    if (order == 'asc') {
      
      $('#sort_invoice_transaction_type').val('desc');
      $('#sort_invoice_cost').val('desc');
      $('#sort_invoice_date').val('desc');

    } else {

      $('#sort_invoice_transaction_type').val('asc');
      $('#sort_invoice_cost').val('asc');
      $('#sort_invoice_date').val('asc');

    }
    
    put_to_bottom('#'+tbody.attr('id'));
}

function sortRatings(table, order, by) {

    console.log('ratings sort');

    var asc   = order === 'asc',
        tbody = table.find('tbody');

    $(document).find(tbody).find('tr').sort(function(a, b) {
      if (by == 'title') {
        if (asc) {
          return $('td:first', a).parent('tr').data(by).toString().localeCompare($('td:first', b).parent('tr').data(by));
        } else {
          return $('td:first', b).parent('tr').data(by).toString().localeCompare($('td:first', a).parent('tr').data(by));
        }
      } else {
        if (asc) {
          return parseInt($('td:first', a).parent('tr').data(by)) < parseInt($('td:first', b).parent('tr').data(by)) ? 1 : -1;
        } else {
          return parseInt($('td:first', b).parent('tr').data(by)) < parseInt($('td:first', a).parent('tr').data(by)) ? 1 : -1;
        }
      }
    }).appendTo(tbody);

    if (order == 'asc') {

      $('#sort_ratings_title').val('desc');
      $('#sort_ratings_visits').val('desc');
      $('#sort_ratings_ratings').val('desc');
      $('#sort_ratings_comments').val('desc');

    } else {

      $('#sort_ratings_title').val('asc');
      $('#sort_ratings_visits').val('asc');
      $('#sort_ratings_ratings').val('asc');
      $('#sort_ratings_comments').val('asc');

    }
    
    put_to_bottom('#'+tbody.attr('id'));
}



$(document).ready(function () {

  var size_li = $(document).find("#alerts_tbody").find('tr').size();
  var x = 10;
  $('#btn-view-more-alerts').click(function(){
    
    x = (x+3 <= size_li) ? x+3 : size_li;
    $(document).find('#alerts_tbody tr:lt('+x+')').removeClass('hide');
    var hidden_alerts = $(document).find('#alerts_tbody').find('tr.hide').length;
    if(hidden_alerts == 0) {
      $('#div-btn-alerts').remove();
    }
  });

  var size_m = $(document).find("#message-container").find('tr').size();
  var y = 10;
  $('#view_more_inbox').click(function(){
    
    y = (y+3 <= size_m) ? y+3 : size_m;
    $(document).find('#message-container tr:lt('+y+')').removeClass('hide');

    var hidden_message = $(document).find('#message-container').find('tr.hide').length;
    if(hidden_message == 0) {
      $('#view_more_inbox').remove();
    }
  });

  var message_count = $(document).find('#message-container').find('tr').length;
  console.log(message_count);
  if(message_count <= 10) {
    $('#view_more_inbox').remove();
  }

//invoice
  var size_i = $(document).find("#invoice_tbody").find('tr').size();
  var z = 10;
  $('#invoice_vmbtn').click(function(){
    
    z = (z+3 <= size_i) ? z+3 : size_i;
    $(document).find('#invoice_tbody tr:lt('+z+')').removeClass('hide');

    var hidden_invoice = $(document).find('#invoice_tbody').find('tr.hide').length;
    if(hidden_invoice == 0) {
      $('#view_more_invoice').remove();
    }
  });

  var invoice_count = $(document).find('#invoice_tbody').find('tr').length;
  console.log(invoice_count);
  if(invoice_count <= 10) {
    $('#view_more_invoice').remove();
  }

  var size_r = $(document).find("#ratings_tbody").find('tr').size();
  var r = 10;
  $('#btn-view-more-ratings').click(function(){
    
    r = (r+3 <= size_r) ? r+3 : size_r;
    $(document).find('#ratings_tbody tr:lt('+r+')').removeClass('hide');

    var hidden_invoice = $(document).find('#ratings_tbody').find('tr.hide').length;
    if(hidden_invoice == 0) {
      $('#view_more_ratings').remove();
    }
  });

  var invoice_count = $(document).find('#ratings_tbody').find('tr').length;
  console.log(invoice_count);
  if(invoice_count <= 10) {
    $('#view_more_ratings').remove();
  }

});

function isUrlValid(url) {
    return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
}

  $('.alerts_table_color').click(function(){

    var this_tr = $(this);
    var id = this_tr.data('id');
    var type = this_tr.data('type');

    $.post("{{ url('user/read/notifications') }}", { id: id, type: type, _token: $_token }, function(response) {
      if(response == 1) {
        this_tr.attr('data-unread', '2');
        if (this_tr.children().find('span').hasClass('bold')) {
          this_tr.children().find('span').toggleClass('bold grayTextB');
        }
      }
    });



    try {
      var href = $(this_tr.data('href')).attr('href');
      if (isUrlValid(href) == true) {
        window.open(href, '_blank');
      }
    } catch (e) {
      // console.log(e);
    }
  });


  $(document).ready(function(){
    $('.alerts_table_color').each(function(k, v){
      var this_tr = $(this);
      try {
        var href = $(this_tr.data('href')).attr('href');
        if (isUrlValid(href) == true) {
          this_tr.attr('style', 'cursor: pointer !important').children().attr('style', 'cursor: pointer !important');
        } else {
          this_tr.attr('style', 'cursor: default !important').children().attr('style', 'cursor: default !important');
        }
      } catch (e) {
        this_tr.attr('style', 'cursor: default !important').children().attr('style', 'cursor: default !important');
      }
      
    });
  });
  
  
  $('.btn-direct-msg').click(function() {
    var receiver = $(this).data('receiver');
    var title = $(this).data('title');
    var form = $('#modal-direct-msg');

    form.find('#row-reciever_id').val(receiver);
    form.find('#row-title').val(title);

    form.modal('toggle');


  });
 $(document).ready(function(){
   // $( "#placement_guide" ).tooltip({ content: '<img src =http://yakolak.xerolabs.co/uploads/default/homepage_placement_guide.jpg/>' });

   $('#panelPrecios [data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'bottom',
    html: true
});
 });

 $("#ads-type").on("change", function() { 
   var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
        var id = $("#ads-type").val();
        $("#category-pane").addClass("hide");
        $("#subcategory-pane").addClass("hide");
        $("#subcategorythree-pane").addClass("hide");
        if (id == 2) {
            $("#category-ads-pane").addClass("hide");
            $("#category-auction-pane").removeClass("hide");
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#category-ads-pane").removeClass("hide");
            $("#category-auction-pane").addClass("hide");
            $("#auction-type-pane").addClass("hide");
        }
        $("#ads-main-category").html("");
        $("#ads-category-auction").html("");
        $.post("{{ url('get-cat-type-for-post') }}", { id: id, _token: $_token }, function(response) {
          if(response.cat_type == "undefined"){
            $("#ads-main-category").html('<option value="all" readOnly>All Categories</option>');
            $("#ads-category-auction").html('<option value="all" readOnly>All Categories</option>');
          }else{
            $("#ads-main-category").html(response.cat_type);
            $("#ads-category-auction").html(response.cat_type);
          }
          touch_custom_attr();
       }, 'json');
  });  
$("#ads-category").on("change", function() { 
          var id = $("#ads-category").val();
           $("#custom-attribute-container").removeClass("hide");
          if(id=="Y2F0LTI4" || id == "Y2F0LTI5"){
            $("#price-range-container-title").addClass("hide");
            $('#price-range-container').addClass("hide");
          }else{
            $("#price-range-container-title").removeClass("hide");
            $('#price-range-container').removeClass("hide");
          }
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          var subcategory = $("#ads-subcategory");
          if(id){
            $('#custom-attributes-pane').addClass('hide');
            
            
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                $("#custom-attribute-pane-title").addClass("hide");
                $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                    custom_attri.html(response.rows);
              }     
            }, 'json').complete(stick);
            
            
              subcategory.html("");
            if(id !="all"){
              $.post("{{ url('get-sub-category-for-post') }}", { id: id, _token: $_token }, function(response) {
                if(response == "undefined"){
                  $("#subcategory-pane").addClass("hide");
                  $("#subcategorythree-pane").addClass("hide");
                }else{
                  $("#subcategory-pane").removeClass("hide");
                   subcategory.html(response);
                }
               
              }, 'json');
            }
         }    
    });
 $("#ads-subcategory").on("change", function() { 
      var id = $("#ads-subcategory").val();
      $("#custom-attribute-container").removeClass("hide");
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          if(id){
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                  $("#custom-attribute-pane-title").addClass("hide");
                  $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                  custom_attri.html(response.rows);
              }     
          }, 'json');
          }
            var container = $("#ads-subSubcategory");
          $.post("{{ url('get-sub-category-three-for-post') }}", { id: id, _token: $_token }, function(response) {
            if(response == "undefined"){
               $("#subcategorythree-pane").addClass("hide");
            }else{
               $("#subcategorythree-pane").removeClass("hide");
               container.html(response);
            }
            touch_custom_attr();
          }, 'json');
   });
   $("#ads-main-category").on("change", function() {
        var id = $("#ads-main-category").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
//                     $("#ads-subcategory").addClass("hide");
                  }else{
//                     $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  }
             touch_custom_attr();
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
//                     $("#custom-attribute-pane-title").addClass("hide");
//                     $("#custom-attribute-container").addClass("hide");
                }else{
//                     $("#custom-attribute-pane-title").removeClass("hide");
//                     $("#custom-attribute-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }  
                touch_custom_attr();
            }, 'json');
       }
   });
 $("#ads-category-auction").on("change", function() {
        var id = $("#ads-category-auction").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
                    $("#ads-subcategory").addClass("hide");
                  }else{
                    $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  } 
//              touch_custom_attr();
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
                    // $("#custom-attribute-pane-title").addClass("hide");
                    // $("#custom-attribute-pane-container").addClass("hide");
//                     $("#custom-attribute-container").addClass("hide");
                }else{
                    // $("#custom-attribute-pane-title").removeClass("hide");
                    // $("#custom-attribute-pane-container").removeClass("hide");
//                     $("#custom-attribute-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }   
                touch_custom_attr();
            }, 'json');
       }
   });  
  
  
  $('#modal-send-referral').submit(function(e){
    e.preventDefault();
    
    var form = $(this)[0];
    var formData = new FormData(form);
    
    $.ajax({
        url: $('#modal-send-referral').attr('action'),
        data: formData,
        type: 'POST',
        contentType: false,
        processData: false,
        beforeSend: function() {
          $('#load-form-invite').removeClass('hide');
        },
        success: function(response) {
          $('#load-form-invite').addClass('hide');
          console.log(response);
          if(response == 1) {
            status('Invitation Sent', 'Referral Invitation Sent', 'alert-success');
            $('#inviteUser').modal('toggle');
          } else {
            status('Invitation Sent', 'Referral Invitation Failed', 'alert-danger');
          }
          form.trigger('reset');
        }
    });
    
  });
  
  $(document).ready(function(){

        $('.print-invoice').click(function(){
            var invoice_html = '{{url("pdfinvoice")}}?invoice_no=' + $(this).parent().parent().data('invoice_no');
            $('#loaderFrame').attr('src', invoice_html);
        });
    });
  
  function isLoaded()
  {
    if ($("#loaderFrame").attr('src')) {
      $("#loaderFrame").get(0).contentWindow.print();
    }
    
  }
  
</script>

@stop
@section('content')
<!-- <div style="display: none;">Hidden by default</div> -->
<!-- <div class="loader" style="position: fixed;left: 0px;top: 0px;width: 100%;height: 100%;z-index: 99;background-color: #FFFFFF;"></div> -->
<div class="container-fluid noPadding bgGray borderBottom planType" id="content-wrapper">
  <div class="container bannerJumbutron">
  @if(Auth::check())
   @if(Auth::user()->status == 2)
{{--     <div class="alert alert-danger text-center" style="border: -1px solid #ffcccc; border-radius: 0; background-color: #fcf3f3; margin-left: 15px; margin-right: 15px;">
      <span id="resend_email" class="grayText" style="font-size:14px;"> Account is not yet verified kindly click <a id="resend_mail" class="grayText" href="#"><u>here</u></a> to resend the confirmation email</span>
    </div> --}}
    @elseif(Auth::user()->status == 3)
{{--     <div class="alert alert-danger text-center" style="border: -1px solid #ffcccc; border-radius: 0; background-color: #fcf3f3; margin-left: 15px; margin-right: 15px;">
      <span class="grayText" style="font-size:14px;"> This account has been block due to violation of site policy you cannot use the sites features. If you think this is a mistake kindly <a class="grayText" href="mailto:admin@yakolak.com"><u>contact</u></a> us.</span>
    </div> --}}
    @endif
  @endif
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 noPadding">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div id="change-photo-notice"></div>

          <div class="panel panel-default removeBorder borderZero borderBottom" id="panel-profile">
              <div class="panel-body normalText minPaddingB"> 
                  <div id="load-form" class="loading-pane hide">
                           <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                   </div>
                <img src ="{{URL::route('uploads', array(), false).'/'.$rows->photo}}" class="img-responsive" id="profile-photo" width="100%">
                          <div class="minPadding noleftPadding norightPadding">
                    {!! Form::open(array('url' => 'profile/photo/save', 'role' => 'form', 'class' => 'form-horizontal','files'=>'true','id'=>'modal-save_photo')) !!} 
                            <div class="uploader-pane">
                                <div class='change error-photo'>
                                  <!-- <input name='photo' id='row-photo_upload' type='file' class='file borderZero form-control file_photo' data-show-upload='false' placeholder='Upload a photo'> -->
                                 <sup class="sup-errors redText"></sup>
                          </div>

<div class="row">
<div class="col-xs-12">
<div class="input-group input-group-sm">
  <span class="input-group-addon redText bgWhite borderZero" id="sizing-addon1"><i class="fa fa-upload" aria-hidden="true"></i></span>
  <input input type="text" name="file_name" id="file_name" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
  <span class="input-group-btn">
   <label class="btn-file btn redButton borderZero btn-sm">
    Browse <input  name='photo' id='row-photo_upload' type="file" style="display: none;" class='' data-show-upload='false' placeholder='Upload a photo' onchange="document.getElementById('file_name').value = this.value.split('\\').pop().split('/').pop()">
</label> 
        <!-- <button type="button" class="btn-file btn redButton borderZero btn-sm">Browse</button> -->
      </span>
        <span class="input-group-btn">
        <button type="button" class="btn btn-img-delete redButton borderZero btn-sm leftMargin"><span style="padding: 1px;"><i class="fa fa-trash" aria-hidden="true"></i></span></button>
      </span>
</div>
</div>
<!-- <div class="col-xs-3">4fr
<!-- <button type="button" class="btn redButton borderZero btn-sm">Browse</button> -->
<!-- <button type="button" class="pull-right btn redButton borderZero btn-sm leftMargin" ><span style="padding: 1px;"><i class="fa fa-trash" aria-hidden="true"></i></span></button>
</div> -->
</div>
                            </div>
                            <!-- <button class="btn redButton borderZero" type="button">Browse</button> -->
                        </div>
                        <input type="hidden" name="id" value="{{$rows->id}}">
                <button class="btn blueButton borderZero noMargin fullSize imageUpload" type="submit" style="height:35px;"><i class="fa fa-picture-o"></i> Upload Photo</button>
               </div>
               {!! Form::close() !!}
           </div>
          
          <div class="panel panel-default removeBorder borderZero borderBottom hidden-xs hidden-sm">
              <div class="panel-body normalText">
                <ul id="dashboardTab" class="nav nav-pills nav-stacked noPadding" role="tablist">
                  <li role="presentation"><a id="account-link" href="#account" aria-controls="account" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i> Account<i class="fa fa-angle-right pull-right"></i></a></li>
                  <!-- <li role="presentation"><a href="#additionalInformation" aria-controls="information" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user-plus rightPadding"></i> Additional Information<i class="fa fa-angle-right pull-right"></i></a></li> -->
                  <li role="presentation"><a id="subscription-link" href="#subscription" aria-controls="subscription" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-barcode  rightPadding"></i> Subscription <span class="redText planIdentify">(Upgrade Plan)</span><i class="fa fa-angle-right pull-right"></i></a></li>
                  <li role="presentation" id="auction-listings"><a id="auction-link" href="#auctionAds" aria-controls="auctionAds" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bar-chart rightPadding"></i> Auction Listings<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation" id="ad-listings"><a id="listings-link" href="#listings" aria-controls="listings" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-th rightPadding"></i> Ads Listings<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a id="alerts-link" href="#alerts" aria-controls="alerts" role="tab" data-toggle="tab" class="aleClicked borderbottomLight grayText"><i class="fa fa-bell rightPadding"></i> Notifications<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a id="messages-link" href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="mesClicked borderbottomLight grayText"><i class="fa fa-envelope rightPadding"></i> Messages<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a href="#commentsandReviews" aria-controls="commentsandReviews" role="tab" data-toggle="tab" class="comClicked borderbottomLight grayText"><i class="fa fa-comments rightPadding"></i> Comments and Reviews<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a id="watchlist-link" href="#watchlist" aria-controls="watchlist" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-eye rightPadding"></i> Watchlist<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a href="#rewardsandreferrals" aria-controls="rewardsandreferals" role="tab" data-toggle="tab" class="rewClicked borderbottomLight grayText"><i class="fa fa-link  rightPadding"></i> Reward Points <span class="redText">({{Auth::user()->points}} points)</span><i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a href="#advertisingplans" aria-controls="advertisingplans" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bullhorn  rightPadding"></i> Banner Ads<i class="fa fa-angle-right pull-right"></i></a></li>

                  <li role="presentation"><a href="#reports" aria-controls="reports" role="tab" data-toggle="tab" class="repClicked borderbottomLight grayText"><i class="fa fa-area-chart rightPadding"></i> Reports<i class="fa fa-angle-right pull-right"></i></a></li>
                  <li role="presentation"><a href="#invoice" aria-controls="invoice" role="tab" data-toggle="tab" class="invClicked borderbottomLight grayText"><i class="fa fa-list rightPadding" aria-hidden="true"></i> Invoice<i class="fa fa-angle-right pull-right"></i></a></li>
                  <!-- <li role="presentation"><a href="#statistics" aria-controls="statistics" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-bar-chart rightPadding" aria-hidden="true"></i> Statistics<i class="fa fa-angle-right pull-right"></i></a></li> -->
                  <li role="presentation"><a id="settings-link" href="#profileSettings" aria-controls="profileSettings" role="tab" onclick="refresh()" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-cog rightPadding"></i>  Settings<i class="fa fa-angle-right pull-right"></i></a></li>
                </ul>
              </div>
            </div>

        </div>
      </div>      
     <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12 noPadding">
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="account">
          <div class="panel panel-default removeBorder borderZero borderBottom nobottomMargin">
            <div id="account-profile-notice"></div>
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Account Setting</span>
              <!-- <span class="pull-right"><button class="btn redButton borderZero btn-sm" id="btn-change-mode" style="position: relative;bottom: 5px;">Edit</button></span> -->
            </div>
            <div class="panel-body" id="panel-account-profile">
               {!! Form::open(array('url' => 'register/save', 'role' => 'form', 'class' => '','files'=>'true','id'=>'form-profile_save')) !!} 
              <div id="load-account-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
              </div> 
              <div class="normalText grayText">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              VANITY URL
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                  <div class="form-group bottomMargin">
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-12 col-sm-12 col-xs-12 ">
                     {{ URL::to('/') }}<span id="user_type_text">/{{$rows->usertype_id == 1 ? 'vendor' : 'company'}}/</span> <input class="borderZero inputBox readonly-view" type="text" name="alias" id="customer-alias" value="{{$rows->alias}}">
                      </div>
                    </div>
                  </div>
              </div>
              </div>
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding redText ">
              BASIC INFORMATION
              <span class="pull-right redText">
                ACCOUNT <span class="hidden-xs">STATUS</span>:<span class="form-group bottomMargin" style="vertical-align:middle;">
                <!-- <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label> -->
                @if($rows->sms_verification_status == 1)
                <span class="label label-success borderZero" style="vertical-align:middle;">Verified</span>
                @else
                <span class="account_status label label-danger borderZero redbutton" style="cursor:pointer; vertical-align:middle;">Verify</span>
                @endif
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
                  </div>
                </div>
              </span>
              </span>
              </div>
                 
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin" id="account-setting">
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">User Type</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <select class="form-control inputBox" id="client-usertype_id" name="usertype">
                          @foreach($usertypes as $row)
                            <option value="{{$row->id}}"{{($rows->usertype_id == $row->id ? 'selected' : '')}}>{{$row->type_name}}</option>
                          @endforeach  
                        </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input name="username" placeholder="Input" value="{{$rows->username}}" class="form-control borderZero inputBox fullSize" type="text">
                    </div>
                  </div>
                </div>
              </div>
            </div>
              <div class="row">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding " id="profile_name_change"> @if(Auth::user()->usertype_id == 1) Vendor Name @elseif(Auth::user()->usertype_id == 2) Company Name @else Profile Name @endif</label>
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input name="name" placeholder="Input" value="{{$rows->name}}" class="form-control borderZero inputBox fullSize" type="text">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6 founded-date-pane {{$rows->usertype_id == 1 ? 'hide':''}}">
                 <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Founded Date</label>
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <div class="input-group date" id="date-company_start_date">
                         <input type="text" name="company_start_date" value="{{ date("m/d/Y", strtotime($rows->company_start_date)) }}" class="form-control borderZero inputBox" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div> 
                    </div>
                  </div> 
              </div> 
              <div class="col-sm-6 individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                        <input type="text" class="form-control borderZero inputBox readonly-view" value="{{$rows->address}}" name="address" id="client-address">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <input name="email" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize readonly-view" type="email">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                          <input name="website" class="form-control borderZero inputBox fullSize readonly-view" type="url" value="{{$rows->website}}">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin" id= "client-country-pane">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                  <div class="input-group col-md-8 col-sm-12 col-xs-12">
                    <select class="form-control borderZero inputBox readonly-view" id="client-country" name="country">
                      <option class="hide">Select</option>
                      @foreach($countries as $row)
                        <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':''}}>{{$row->countryName}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin" id="client-city-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-city" name="city">
                      </select>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Phone Number</label>
                  <div class="input-group flex">
                    <input class="form-control borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;" placeholder="+63">
                    <input class="form-control leftMargin borderZero inputBox fullSize readonly-view" type="number" name="telephone_no" value="{{$rows->telephone}}" id="client-tel_no">
                  </div>  
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Mobile Number</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group flex">
                       <input class="form-control borderZero inputBox readonly-view" type="text" name="mobile_no_prefix" id="client-mobile_no_prefix" style="width:50px;" placeholder="+63">
                      <input class="form-control leftMargin borderZero inputBox fullSize readonly-view" type="number" name="mobile" value = "{{$rows->mobile}}" id="client-mobile">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row individual-pane-one {{$rows->usertype_id == 1 ? '':'hide'}}">
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Birthdate</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                        <input type="text" name="date_of_birth" class="form-control borderZero inputBox readonly-view" id="client-date_of_birth" value="{{ date("m/d/Y", strtotime($rows->date_of_birth)) }}" placeholder="MM/DD/YYYY">
                        <span class="input-group-addon borderZero inputBox">
                          <span><i class="fa fa-calendar"></i></span>
                       </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group bottomMargin">
                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">Gender</label>  
                  <div class="inputGroupContainer">
                    <div class="input-group topPadding col-md-8 col-sm-12 col-xs-12">
                       <label class="radio-inline"><input class="readonly-view" type="radio" name="gender" id="client-gender-male"  {{$rows->gender == 'm' ? 'checked':''}} value="m">Male</label>
                       <label class="radio-inline"><input class="readonly-view" type="radio" name="gender" id="client-gender-female" {{$rows->gender == 'f' ? 'checked':''}} value="f">Female</label>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </span>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-lg-12 col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Description</label>
                      <div class="inputGroupContainer">
                        <div class="input-group col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <textarea class="form-control inputBox lineHeight" rows="5" id="comment" name="description">{{$rows->description}}</textarea>
                        </div>
                      </div>
                    </div>
                </div>
        </div>
<!--                <div class="col-lg-6 col-md-6 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Username</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="username" placeholder="Input" value="{{$rows->username}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Profile Name</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="name" placeholder="Input" value="{{$rows->name}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="email" placeholder="Input" value="{{$rows->email}}" class="form-control borderZero inputBox fullSize readonly-view" type="text">
                        </div>
                      </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Usertype</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                            <select class="form-control inputBox readonly-view" id="client-usertype_id" name="usertype">
                              @foreach($usertypes as $row)
                                <option value="{{$row->id}}"{{($rows->usertype_id == $row->id ? 'selected' : '')}}>{{$row->type_name}}</option>
                              @endforeach  
                            </select>
                        </div>
                      </div>
                    </div> -->
                <!--    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                              <input name="password" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                              <input name="password_confirmation" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div> -->
<!--                   <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12">
                              <input name="website" class="form-control borderZero inputBox fullSize readonly-view" type="text" value="{{$rows->website}}">
                        </div>
                      </div>
                    </div> -->
<!--                 @if($rows->usertype_id == 2)
                 <div class="form-group bottomMargin" id="founded-date-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Founded Date</label>
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <div class="input-group date" id="date-company_start_date">
                         <input type="text" name="company_start_date" value="{{ date("m/d/Y", strtotime($rows->company_start_date)) }}" class="form-control borderZero inputBox readonly-view" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div> 
                    </div>
                  </div> 
                 @endif  
                </div>
                <div class="col-sm-12 noPadding {{$rows->usertype_id == 1 ? '':'hide'}}" id="individual-pane-one">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group bottomMargin" id= "client-country-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-country" name="country">
                        <option class="hide">Select</option>
                        @foreach($countries as $row)
                          <option value="{{$row->id}}">{{$row->countryName}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group bottomMargin" id="client-city-pane">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>  
                    <div class="input-group col-md-8 col-sm-12 col-xs-12">
                      <select class="form-control borderZero inputBox readonly-view" id="client-city" name="city">
                      </select>
                    </div>
                  </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Phone Number</label>
                    <div class="input-group flex">
                      <input class="input-group-addon borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;">
                      <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="telephone_no" id="client-tel_no">
                    </div>   -->
<!--                     <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12">
                          <input class="form-control borderZero inputBox readonly-view" type="text" name="telephone_no_prefix" id="client-tel_no_prefix" style="width:50px;">
                          <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="telephone_no" id="client-tel_no">
                      </div>
                    </div> -->
<!--                   </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Mobile Number</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group col-md-8 col-sm-12 col-xs-12">
                            <input class="form-control borderZero inputBox fullSize readonly-view" type="text" name="mobile" id="client-mobile">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                          <input type="text" class="form-control borderZero inputBox readonly-view" name="address" id="client-address" placeholder="">
                      </div>
                    </div>
                  </div> -->
<!--                   <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Birthdate</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group date col-md-8 col-sm-12 col-xs-12" id="date-date_of_birth">
                          <input type="text" name="date_of_birth" class="form-control borderZero inputBox readonly-view" id="client-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                          <span class="input-group-addon borderZero inputBox">
                            <span><i class="fa fa-calendar"></i></span>
                         </span>
                      </div>
                    </div>
                  <div class="form-group bottomMargin">
                    <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">I am a</label>  
                    <div class="inputGroupContainer">
                      <div class="input-group topPadding col-md-8 col-sm-12 col-xs-12">
                         <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-male" value="m">Male</label>
                         <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-female" value="f">Female</label>
                         <input type="hidden" name="gender" id="client-gender">
                      </div>
                    </div>
                  </div> -->
            </div>
          <div id="accountBranchContainer"  class="{{ ($rows->usertype_id == 1 ? 'hide': '') }}"> 
            <?php echo($branches) ?>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
              <span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding topMarginB redText">
                SOCIAL MEDIA
              </span>
              <div class="row">
              <div class="col-sm-6">
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Facebook</label>  
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
                        <input name="facebook_account" placeholder="" value="{{$rows->facebook_account}}" class="form-control borderZero inputBox fullSize" type="url">
                         <sup class="sup-errors redText"></sup>
                  </div>
                </div>
              </div>
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Twitter</label>  
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <input name="twitter_account" placeholder="" value="{{$rows->twitter_account}}" class="form-control borderZero inputBox fullSize" type="url">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Instagram</label>  
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <input name="instagram_account" placeholder="" value="{{$rows->instagram_account}}" class="form-control borderZero inputBox fullSize" type="url">
                  </div>
                </div>
              </div>
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">LinkedIn</label>  
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                        <input name="linkedin_account" placeholder="" value="{{$rows->linkedin_account}}" class="form-control borderZero inputBox fullSize" type="url">
                  </div>
                </div>
              </div>
            </div>
          </div>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
              
<!--               <div class="row">
              <div class="col-sm-6">
              <span class="form-group bottomMargin">
                @if($rows->sms_verification_status == 1)
                <span class="label label-success">Verified <i class="fa fa-check"></i></span>
                @else
                <span class="account_status label label-danger" style="cursor:pointer;">Verify</span>
                @endif
                <div class="inputGroupContainer">
                  <div class="input-group col-md-8 col-sm-12 col-xs-12 error-password">
                  </div>
                </div>
              </span>
            </div>
          </div> -->
          </div>
          <br>
          <!--  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
              <span class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noPadding bottomMarginB bottomPadding topMarginB redText">
                NEWSLETTER
              </span>
              <div class="col-xs-12 noPadding">
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Weekly</label>  
                <div class="inputGroupContainer">
                  <div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input type="checkbox" value="">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 noPadding">
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Monthly</label>  
                <div class="inputGroupContainer">
                  <div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input type="checkbox" value="">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-xs-12 noPadding">
              <div class="form-group bottomMargin">
                <label class="col-md-4 col-sm-3 col-xs-12 normalText inputLabel noPadding ">Receive Promotions</label>  
                <div class="inputGroupContainer">
                  <div class="topPadding input-group col-md-8 col-sm-12 col-xs-12 ">
                      <input type="checkbox" value="">
                  </div>
                </div>
              </div>
            </div>
          </div> -->
            
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordertopLight noPadding topPaddingB">
            <input type="hidden" name="id" id="client-id" value="{{$rows->id}}">
              <button class="btn blueButton borderZero noMargin pull-right" id="btn-save_button" type="submit">Update</button>
            </div>
          {!! Form::close() !!}

            </div>
          </div>
        </div>
<!--        <div role="tabpanel" class="tab-pane" id="additionalInformation">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Additional Information</span>
            </div>
            <div class="panel-body">
              <div class="normalText grayText">
                <form class="form-horizontal" role="form">
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Phone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="telephone_no" id="client-tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Mobile Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="mobile" id="client-mobile" placeholder="">
              </div>
            </div>
            <div id="gender" class="form-group">
              <h5 class="normalText col-sm-3" for="email">I am a</h5>
              <div class="col-sm-9">
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-male" value="m">Male</label>
               <label class="radio-inline"><input type="radio" name="temp_gender" id="client-gender-female" value="f">Female</label>
               <input type="hidden" name="gender" id="client-gender">
              </div>
            </div>
            <div id="startDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Founded Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-company_start_date">
                   <input type="text" name="company_start_date" class="form-control borderZero inputBox" id="client-company_start_date" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div class="form-group" id= "client-country-pane">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-country" name="country">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group" id="client-city-pane">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox" id="client-city" name="city">
                </select>
              </div>
            </div>
            <div class="form-group" id="client-address-pane">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="address" id="client-address" placeholder="">
              </div>
            </div>
            <div id="birthDate" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Birth Date</h5>
              <div class="col-sm-9">
                <div class="input-group date" id="date-date_of_birth">
                   <input type="text" name="date_of_birth" class="form-control borderZero inputBox" id="client-date_of_birth" maxlength="28" placeholder="MM/DD/YYYY">
                    <span class="input-group-addon borderZero inputBox">
                      <span><i class="fa fa-calendar"></i></span>
                   </span>
                </div> 
              </div>
            </div>
            <div id="officeHours" class="form-group">
              <h5 class="normalText col-sm-3" for="email">Youtube</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" namee="youtube_account" id="client-youtube" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Website</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="website" id="client-website" placeholder="">
              </div>
            </div>
            <div id="branchContainer">
            <section id="addBranch" class="container-fluid">
            <h5 class="grayText borderbottomLight bottomPadding">
              BRANCH<span class="notrequiredText"> ( Not Required )</span><button type="button" value="" class="hide-view btn-add-branch pull-right borderZero inputBox"><i class="fa fa-plus redText"></i></button>
            </h5>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Telephone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_tel_no[]" id="client-company_tel_no" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Cellphone Number</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_mobile[]" id="client-company_mobile" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9">
                <select class="form-control borderZero inputBox company_country" id="client-company_country" name="company_country[]">
                  <option class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}">{{$row->countryName}}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9">
                 <select class="form-control borderZero inputBox" id="client-company_city" name="company_city[]">
                </select>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Address</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_address[]" id="client-company_address" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_email[]" id="client-company_email" placeholder="">
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Office Hours</h5>
              <div class="col-sm-9">
                <input type="text" class="form-control borderZero inputBox" name="company_office_hours[]" id="client-company_office_hours" placeholder="">
              </div>
            </div>
            <input type="hidden" name="company_branch_id[]">
            </section>
            </div>
          </form>
            </div>
            </div>
          </div>
        </div> -->
       <div role="tabpanel" class="tab-pane" id="subscription">
        <div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">CURRENT PLAN SUBCRIPTION</span>
              <span class="panelTitle redText pull-right {{$remainingDiscountDays == 0 ? 'hide':''}}">{{$remainingDiscountDays}} days remaining for {{ $discount }} % OFF</span>
            </div>
            <div class="panel-body">
              <div class="lineHeightC normalText grayText">
                  {!!$plan!!}
                  <div class="col-md-12 noPadding">
              <hr class="nobottomMargin notopMargin" style="padding-bottom:8px">
              </div>
              </div>
                      <div class="form-group">
                      
                      <div class="mediumText normalWeight bottomPadding">Purchase Bid Points <span class="lightgrayText {{$bid_discount == 0 ? 'hide':''}}">( {!!$bid_discount!!}% OFF )</span></div>
                      <div class="row">
                      <div class="col-sm-10">
                      <select class="form-control borderZero grayText normalText normalWeight" id="client-bid-point-package">
                      {!!$bid_points!!}
                      </select>
                      </div>
                      <div class="col-sm-2" id="paypal-button-container">
                      </div>
                      </div>
                    </div>
            </div>
          </div>
       </div>
     </div>
   </div>
   <div role="tabpanel" class="tab-pane" id="auctionAds">
      
              
    </div>
     
  <div role="tabpanel" class="tab-pane" id="listings">
  </div>

        <div role="tabpanel" class="tab-pane" id="alerts">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Notifications</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <input type="hidden" id="sort_alerts_title" value="asc">
                        <input type="hidden" id="sort_alerts_description" value="asc">
                        <input type="hidden" id="sort_alerts_date" value="asc">
                        <input type="hidden" id="sort_alerts_unread" value="asc">
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortTable($('#myTable'), $('#sort_alerts_title').val(), 'title')"> Title</a></li>
                          <li><a href="#" onclick="sortTable($('#myTable'), $('#sort_alerts_description').val(), 'description')"> Description</a></li>
                          <li><a href="#" onclick="sortTable($('#myTable'), $('#sort_alerts_date').val(), 'date')"> Date</a></li>
                          <li><a href="#" onclick="sortByUnread($('#myTable'), $('#sort_alerts_unread').val(), 'unread')"> Unread</a></li>
                        </ul>
                      </li>
                    </ul>
                  </span>
            </div>
            <div class="panel-body" style="padding-top: 0px; padding-bottom:0px;">
              <div class="normalText grayText table-responsive">
                <table id="myTable" class="table">
                  <thead>
                    <tr>
                      <th onclick="sortTable($('#myTable'), $('#sort_alerts_title').val(), 'title')">Title</th>
                      <th onclick="sortTable($('#myTable'), $('#sort_alerts_description').val(), 'description')">Description</th>
                      <th onclick="sortTable($('#myTable'), $('#sort_alerts_date').val(), 'date')" class="pull-right">Date</th>
                    </tr>
                  </thead>
<tbody class="bodyA" id="alerts_tbody">
  @if($user_alerts)
    @foreach($user_alerts as $key => $notificationsList)
      <tr class="alerts_table_color {{ $key > 9 ? 'hide' : ''}}" data-description="{{ strip_tags($notificationsList['alert_message']) }}" data-date="{{ strtotime($notificationsList['alert_date']) }}" data-title="{{ (str_replace(['[',']'], '', $notificationsList['alert_title']) != 'System Message' && !is_null($notificationsList['feature_type']) ? $notificationsList['feature_type'] : str_replace(['[',']'], '', $notificationsList['alert_title'])) }}" data-href="{{ $notificationsList['alert_message'] }}" data-type="{{ isset($notificationsList['n_id']) ? 'notifications' : 'messages' }}" data-id="{{ isset($notificationsList['n_id']) ? $notificationsList['n_id'] : $notificationsList['m_id'] }}" data-unread="{{ $notificationsList['read_status'] == 1 ? '1' : '2' }}" data-order="{{ $notificationsList['read_status'] == 1 ? $key : '' }}">
        <td style="min-width: 108px; overflow: hidden; display: inline-block; white-space: nowrap;"><span class="normalText {{ $notificationsList['read_status'] == 1 ? 'bold grayText' : 'grayTextB' }}" style="font-size: 14px;">{{ (str_replace(['[',']'], '', $notificationsList['alert_title']) != 'System Message' && !is_null($notificationsList['feature_type']) ? $notificationsList['feature_type'] : str_replace(['[',']'], '', $notificationsList['alert_title'])) }}</span></td>
        <td title="{{ strlen(strip_tags($notificationsList['alert_message'])) > 65 ? strip_tags($notificationsList['alert_message']) : '' }}"><span class="normalText {{ $notificationsList['read_status'] == 1 ? 'bold grayText' : 'grayTextB' }}" style="font-size: 14px;">{{ mb_strimwidth(strip_tags($notificationsList['alert_message']), 0, 65, '...') }} </span></td>
        <td><span class="normalText {{ $notificationsList['read_status'] == 1 ? 'bold grayText' : 'grayTextB' }}" style="font-size: 14px;">{{ date("m/d/y g:i A", strtotime($notificationsList['alert_date'])) }}</span></td>
      </tr>
    @endforeach
  @endif
</tbody>
                </table>   
                  
                </div>
                <div id="div-btn-alerts" class="text-center {{ $user_alerts_count <= 10 ? 'hide' : '' }}" style="margin-right:-15px; margin-left:-15px; cursor: default;">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-alerts" style="width:100%; cursor: default;">View More</button>
                  </div>
            </div>
          </div>
        </div>
     <div role="tabpanel" class="tab-pane" id="messages">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Message Inbox</span>
              <span class="pull-right">
                <ul class="nav navbar-nav sortDropdown">
        <li class="dropdown">
        <input type="hidden" id="sort_messages_date" value="asc">
        <input type="hidden" id="sort_messages_unread" value="asc">
          <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
          <ul class="dropdown-menu sortDrop">
            <li><a href="#"  onclick="sortTable($('#myTableMessage'), $('#sort_messages_date').val(), 'date')"> Date</a></li>
            <li><a href="#"  onclick="sortTable($('#myTableMessage'), $('#sort_messages_unread').val(), 'unread')"> Unread</a></li>
          </ul>
        </li>
      </ul>
              </span>
            </div>
            <div class="panel-body notopPadding nobottomPadding" id="messageInboxPane">
                 <div id="load-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 
              <div class="col-md-12 noPadding">
                  <div id="messageBlock" class="table-responsive">
                <table id="myTableMessage" class="table">
                  <thead>
                    <tr>
                      <th>Title</th>
                      <th class="pull-right">
<!--          <button class="bgWhite removeBorder borderZero normalText grayText dropdown-toggle hidden-sm  hidden-xs" type="" data-toggle="dropdown">Sort by
            <span class="caret"></span>
          </button> -->
          
                       <!-- <span class="dropdown">
                        <button class="noBackground removeBorder borderZero normalText grayText dropdown-toggle hidden-sm hidden-xs" data-toggle="dropdown" aria-expanded="false"><i id="sort-indicator" class="redText fa fa-sort-amount-asc"></i>
                        </button>
                          <ul class="dropdown-menu borderZero normalText" style="margin-left:-128px;margin-top:11px;">
                            <li><a href="#"  onclick="sortByDate()"> Date</a></li>
                            <li><a href="#"  onclick="sortByStatus()"> Unread</a></li>
                          </ul>
                        </span> --> <!-- <a href="#" class="btn-sort-msg"></a> --></th>
                    </tr>
                  </thead>
                  <tbody id="message-container">
                    <?php echo $messages_header;?>
                    <!-- <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> System Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> System Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-envelope-o grayTextB rightPadding"></i> Inquiry Messages</td>
                      <td>07/18/16 11:35 AM <i class="fa fa-trash"></i></td>
                    </tr> -->
                  </tbody>
                </table>
                <input type="hidden" id="msg-field" value="date">
                <input type="hidden" id="msg-sort" value="desc">
                
              </div>
              <div id="view_more_inbox" class="text-center" style="margin-left: -15px; margin-right: -15px;">
                <button class="btn blueButton borderZero" type="button" data-toggle="modal" data-target="#" style="width:100%;">View More </button>
              </div>
              
<!--                <div class="col-md-6">
                  <div class="panel panel-default borderZero removeBorder">
                    <div class="panel-heading messageTitle nosidePadding">TITLE <span class="pull-right"><span class="normalText">Sort</span> <i class="redText fa fa-sort-amount-asc"></i></span></div>
                    <div class="panel-body noPadding">
                      <table class="table removeBorder" id="message-table">
                        <tbody class="normalText bgGray grayText" id="message-tbody">
                          <?php echo $messages_header;?>
                        </tbody>
                      </table>
                    </div>
                  </div>
              </div> -->
              <div id="replyBlock">
                <div class="panel removeBorder borderZero panel-default noMargin" id="message-pane">
                   <div class="panel-heading messageTitle noPadding bottomPaddingB nosidePadding" id="message-title-pane">MESSAGE TITLE</div>
                   <div class="panel-body noPadding">
                    <div class="loading-pane hide">
                        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                    </div>
                   <table class="table bottomMarginB removeBorder">
                    
                    <tbody class="normalText grayText" id="message_container">
                     
                    </tbody>
                  </table>
                       {!! Form::open(array('url' => 'user/send/message', 'role' => 'form', 'class' => 'form-horizontal hide', 'id' => 'modal-send-message', 'files' => 'true')) !!}
                        <textarea class="borderZero form-control linHeight" rows="3" id="row-message" name="message" required></textarea>
                        <span class="pull-left topPadding">
                          <button id="back_button_inbox" class="btn redButton borderZero btn-message-back">Back</button>
                        </span>
                        <span class="pull-right topPadding">
                          <input type="hidden" name="message_id" id="row-message_id">                         
                          <span><button type="submit" class="btn borderZero blueButton">Reply</button></span>
                        </span>
                       {!! Form::close() !!}
                   </div>
                 </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="commentsandReviews">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Comments and Reviews</span>
            </div>
            <div class="panel-body">
              <div class="table-responsive">

                
                    <?php echo $review_summary;?>
              </div>
           
              <!-- <div class="col-sm-12 bgWhite bottomPaddingB">
                <div class="panelTitle bottomPaddingB">Add title comment and review</div>
                <div class="commentBlock">
                <div class="commenterImage">
                   <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                  </div>
                  <div class="commentText">
                  <div class="panelTitleB bottomPadding">Username</div>
                  <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                  <div class="subText">
                    <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
                    <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                    <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                  </div>
                  </div>
              </div>
              </div> -->
              <!--
              <div class="col-sm-12 bgGray bottomPaddingB borderBottom">
                <div class="panelTitle topPaddingB bottomPaddingB">Add title comment and review</div>
                <div class="commentBlock">
                <div class="commenterImage">
                 <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                </div>
                <div class="commentText">
                <div class="panelTitleB bottomPadding">Username</div>
                <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                <div class="subText">
                  <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
                  <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                  <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                </div>
                </div>
              </div>
              </div>
              <div class="col-sm-12 bgWhite topPaddingB bottomPaddingB">
                <div class="panelTitle bottomPaddingB">if vendor page show vendor name</div>
                <div class="commentBlock">
                <div class="commenterImage">
                   <img src="http://bootdey.com/img/Content/user_1.jpg" class="avatar" alt="user profile image" style="width: 52px; height: 52px;">
                  </div>
                  <div class="commentText">
                  <div class="panelTitleB bottomPadding">Username</div>
                  <p class="normalText">Hello this is a test comment and this comment is particularly very long and it goes on and on and on.</p>
                  <div class="subText">
                    <span class="rightPaddingB  blueText normalText"><i class="fa fa-comments"></i> Reply</span>
                    <span class="redText normalText"><i class="fa fa-paper-plane"></i> Direct Message</span>
                    <span class="lightgrayText  topPadding normalText pull-right"> December 1, 2016 5:00 PM</span>
                  </div>
                  </div>
              </div>
              </div> -->
            </div>
          </div>
        </div>




<div role="tabpanel" class="tab-pane" id="watchlist">
  <div id="watchlist-panel"></div>
  <div class="watchlist-container">
  <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
         
        <!-- Mobile View -->
        <div class="col-xs-12 col-sm-12 noPadding visible-sm visible-xs">
          <div class="panel panel-default removeBorder borderZero">
            <div class="panel-heading panelTitleBarLightB">
                <span class="panelTitle">Listings</span>  <span class="hidden-xs hidden-sm">| Check out our members latest bids and start bidding now!</span>
                <!--   <span class="pull-right"><a href="http://192.168.254.14/yakolak/public/category/list" class="redText">view more</a></span> -->
               </div>
            <div class="col-sm-12 col-xs-12 blockBottom noPadding">
             <div class="panel-body noPadding">
            </div>
            </div>
           </div>
          </div>
         </div>
        </div>
        <div class="panel panel-default removeBorder borderZero hidden-xs hidden-sm">
              <div class="panel-heading panelTitleBar">
                <span class="panelTitle">Watchlist</span>
              </div>
                 <div id="load-watchlist-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                 </div> 
          <div class="panel-body noPadding">
            <div class="col-lg-12 col-md-12 noPadding"> 
           <!--    <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse9">
                <strong>Auction Bidded </strong><span class="{{count($auction_biddeds) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($auction_biddeds) == 0 ? 'bgGrayC':'bgRed'}} badge">{{count($auction_biddeds)}} </span> Bidded<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse9" class="panel-collapse collapse {{(count($auction_biddeds) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">
     
             @foreach ($auction_biddeds as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="3">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by {{$row->name}}<span class="pull-right"><strong class="blueText">
                                   {{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  <span class="pull-right">
                                       @if($row->average_rate != null)
                                         <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @else
                                         <span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @endif 
                                  </span>
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @endforeach 

               </div>
            </div>
          </div>
        </div>
        </div>
        </div> -->
              <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body notopPadding nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse7">
                <strong>Favorite Listings </strong><span class="{{count($watchlistads) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($watchlistads) == 0 ? 'bgGrayC':'bgRed'}} badge">{{ count($watchlistads) }}</span> Favorite Ads<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse7" class="panel-collapse collapse {{(count($watchlistads) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">
                @forelse ($watchlistads as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="1" data-title="{{$row->title}}">
                    <div class="panel panel-default borderZero removeBorder bottomMarginB">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{ url('/ads/view') . '/' . $row->id }}">
                              <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                                if(strlen($row->title) >= 37){echo "..";} ?>
                              </div>
                              <div class="visible-md"><?php echo substr($row->title, 0, 25);
                                if(strlen($row->title) >= 37){echo "..";} ?>
                              </div>
                            </a>
                            <div class="normalText grayText bottomPadding bottomMargin"> 
                              <div class="row">
                                <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}"> 
                                  <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                                </div>
                                <div class="col-sm-6">
                                  <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="normalText bottomPadding">
                              <div class="row">
                                <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                                  @if($row->city || $row->countryName)
                                    <i class="fa fa-map-marker rightMargin"></i>
                                    {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                                  @else  
                                  @endif
                                    <?php $i=0;?>
                                  @if(count($row->getAdvertisementComments)>0)
                                  @foreach($row->getAdvertisementComments as $count)
                                    <?php $i++;?>     
                                  @endforeach   
                                  @endif
                                </div>
                                <div class="col-sm-6">
                                  <span class="pull-right">
                                    {!! app('App\Http\Controllers\FrontEndUserController')->create_stars(isset($row->getAdRatings) ? $row->getAdRatings->avg('rate') : 0) !!}
                                  </span>
                                </div>
                              </div>
                            </div>   
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @empty
                 <p class="text-center redText topPadding bottomPadding">Empty</p>

                @endforelse 

              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
        <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding notopPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder bottomMarginB">
            <div class="row panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse91">
                <strong>Favorite Auctions </strong><span class="lightgrayText pull-right"><span class="bgGrayC badge">0</span> Favorite Auctions<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse91" class="panel-collapse collapse">
              <div class="panel-body borderBottom row removeBorder center">
              {{-- <div class="redText normalText text-center">Empty</div> --}}
              <p class="text-center redText topPadding bottomPadding">Empty</p>
              </div>
            </div>
          </div>
        </div>
        </div>
        </div>
         <div class="panel panel-default removeborder borderZero nobottomMargin">
                <div class="panel-body nobottomPadding notopPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse8">
                <strong>Favorite Profile </strong><span class="{{count($favorite_profiles) == 0 ? 'lightgrayText':'redText'}} pull-right"><span class="{{count($favorite_profiles) == 0 ? 'bgGrayC':'bgRed'}} badge">{{count($favorite_profiles)}}</span> Favorite Users<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse8" class="panel-collapse collapse {{(count($favorite_profiles) == 0 ? '':'in')}}">
              <div class="panel-body borderBottom row removeBorder">

                        @forelse ($favorite_profiles as $row)               
               <div class="col-lg-4 col-md-4" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="2">
                    <div class="panel panel-default borderZero removeBorder bottomMarginB">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">                
                                <div class="mediumText grayText ">
                                  <a class="grayText" href="{{URL::to('/'.$row->alias)}}"><span class="fa fa-user" aria-hidden="true"></span><span class="leftPadding">{{$row->name}}</span></a>
<!--                                   <i class="fa fa-user" aria-hidden="true"></i> <span class="leftPadding">{{$row->name}}</span><span class="pull-right">
                                  </span> -->
                                </div>
                                  <div class="normalText topPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText redText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @empty
                   <p class="text-center redText topPadding bottomPadding">Empty</p>
                @endforelse 
               </div>
            </div>
          </div>
        </div>
        </div>
        </div> <!-- <div class="panel panel-default removeborder borderBottom borderZero nobottomMargin">
                <div class="panel-body nobottomPadding">
                  <div class="panel-group nobottomMargin" id="accordion">
          <div class="panel panel-default removeBorder">
            <div class="row notopPadding panel-heading removeBorder bgWhite">
                <a id="watch" class="grayText" data-toggle="collapse" href="#collapse91">
                <strong>Auction Bidded </strong><span class="leftMargin lightgrayText">|  Free account type can only have 4 active listings you must disable active first.</span><span class="redText pull-right">{{count($auction_biddeds)}} Bidded<i class="leftPadding fa fa-chevron-down" aria-hidden="true"></i></span></a></div>
            <div class="row borderbottomLight"></div>
            <div id="collapse91" class="panel-collapse collapse ">
              <div class="panel-body borderBottom row removeBorder">
     
             @foreach ($auction_biddeds as $row)               
               <div class="col-lg-3 col-md-3" id="ads-list" data-watchlist_id="{{$row->watchlist_id}}" data-watchlist_type="3">
                    <div class="panel panel-default borderZero removeBorder">
                      <div class="panel-body noPadding">
                          <div class="">
                          <a href="{{url('/').'/ads/view/'.$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                        </div></a>
                          <div class="minPadding nobottomPadding">
                            <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                            </div>
                                <div class="normalText grayText bottomPadding bottomMargin">
                                  by {{$row->name}}<span class="pull-right"><strong class="blueText">
                                   {{ number_format($row->price) }} USD</strong>
                                  </span>
                                </div>
                                  <div class="normalText bottomPadding">
                                   @if($row->city || $row->countryName)
                                   <i class="fa fa-map-marker rightMargin"></i>
                                   {{ $row->city }} @if ($row->city),@endif 
                                   @if($row->countryName) 
                                   {{ $row->countryName }}@else None @endif
                                   @else
                                   <i class="fa fa-map-marker rightMargin"></i> not available
                                   @endif
                                  <span class="pull-right">
                                       @if($row->average_rate != null)
                                         <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @else
                                         <span class="blueText rightMargin">No Rating</span>
                                         <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                                       @endif 
                                  </span>
                                </div>
                             
                            </div>
                           </div>
                      </div>
                      <div class="panel-footer">
                       <a href="#" class="btn-remove-watchlist"><span class="normalText blueText"> <i class="fa fa-trash"></i> Remove</span></a>
                      </div>
                    </div>
                 </div>
                @endforeach 

               </div>
            </div>
            <div class="row borderBottom"></div>
          </div>
        </div>
        </div>
        </div> --></div>
          <!-- Mobile Listing -->
    </div>
  </div>
</div>
</div>
        <div role="tabpanel" class="tab-pane" id="rewardsandreferrals">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Rewards and Referrals</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortByDate()"> Username</a></li>
                          <li><a href="#" onclick="sortByStatus()"> Email</a></li>
                          <li><a href="#" onclick="sortByDate()"> Date</a></li>
                          <li><a href="#" onclick="sortByDate()"> Point</a></li>
                        </ul>
                      </li>
                    </ul>
                  </span>
            </div>
            <div class="panel-body nobottomPadding">
              <div class="xxlargeText grayText">Total Available Points<span class="pull-right">{{Auth::user()->points}}</span></div>
<!--                 <div class="row">Email Address
                  <div class="borderBottom"></div>
                </div> -->
              <div class="normalText topPaddingB grayText">
                <div class="form-inline">
                  <label for="text" class="mediumText rightPaddingB">Referral ID</label>
                  <input type="text" class="form-control borderZero inputBox fullSize" id="email" readonly value="{{$rows->referral_code}}">
                  <span class="normalText lightgrayText pull-right topPaddingB hidden-xs hidden-sm"><span class="hidden-xs hidden-sm">*This code is autogenerated and cannot be altered</span></span>
                  <div class="normalText lightgrayText topPaddingB visible-xs visible-sm"><span class="visible-xs visible-sm">*This code is autogenerated and cannot be altered</span></div>
                </div>
                <!-- <div class="bordertopLightB blockMargin"></div> -->
                <hr class="nobottomMargin">
                <div class="table-responsive">
                <table id="myTable" class="table">
                  <thead>
                    <tr>
                      <th>Action</th>
                      <th>Username</th>
                      <th>Email</th>
                      <th>Date</th>
                      <th>Point</th>
                    </tr>
                  </thead>
                  <tbody>
                  @forelse ($get_referral_info as $row)
                        <tr>
                          <td>{{$row->getReferralType->name}}</td>
                          <td>{{$row->getUserInfo->name}}</td>
                          <td>{{$row->getUserInfo->email}}</td>
                          <td>{{date("F j Y", strtotime($row->created_at))}}</td>
                          <td>{{$row->referral_points}}</td>
                        </tr>
                  @empty
                       <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center">No Available Referrals</td>
                        <td></td>
                        <td></td>
                       </tr>
                  @endforelse 
                  </tbody>
                </table>
              </div>
              <div class="row text-center">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero fullSize" type="button" data-toggle="modal" data-target="#inviteUser">SEND INVITES </button>
                  </div>
              </div>
                  <!-- <ul class="nav nav-tabs nav-justified">
                    <li class="active"><a data-toggle="tab" href="#send-referrals">Send Referrals</a></li>
                    <li><a data-toggle="tab" href="#successful-referrals">Successful Referrals</a></li>
                    <li><a data-toggle="tab" href="#accumulated-points">Accumulated Points</a></li>
                  </ul>

                  <div class="tab-content">
                    <div id="accumulated-points" class="tab-pane fade">
                     <h3>1500 points</h3>
                    </div>
                    <div id="successful-referrals" class="tab-pane fade">
                      <h3>Menu 2</h3>
                       <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th>Email</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>john@example.com</td>
                          </tr>
                          <tr>
                            <td>mary@example.com</td>
                          </tr>
                          <tr>
                            <td>july@example.com</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div id="send-referrals" class="tab-pane fade in active">
                      <h3>Contacts</h3>
                      <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 mediumText inputLabel noPadding ">Emails</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                        
                        </div>
                      </div>
                    </div>

                          <div class="text-right">
                            <a href="{{url('/').'/referral/import'}}"<button type="submit" class="btn btn-default btn-success borderZero"><i class="fa fa-plane"></i> Import Contacts</button></a>
                            <button type="submit" data-user_id="{{Auth::user()->id}}" class="btn btn-default btn-success borderZero btn-send-a-referral"><i class="fa fa-plane"></i> Send A Referral</button>
                          </div>
  
                    
                    </div>
                  </div> -->
              </div>
            </div>
          </div>
        <div role="tabpanel" class="tab-pane" id="advertisingplans">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Advertising</span>
            </div>
            <div class="panel-body" id="banner-pane" style="padding-bottom:0px;">
                <div id="load-form" class="loading-pane hide">
                <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
              </div>
              <div class="grayText"><strong>Purchases</strong><span class="lightgrayText"> | You can purchase banner ads</span><span class="pull-right"><button class="btn btn-sm blueButton borderZero noMargin btn-purchase-ads"><i class="fa fa-plus" aria-hidden="true"></i> Purchase Ads</button>
              <!-- <button class="btn btn-sm blueButton borderZero noMargin" onclick="refreshUserBanner()"><i class="fa fa-plus" aria-hidden="true"></i>Refresh</button> -->
              </div>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="padding:10px 16px 0px 16px;">
                        <div class="input-group borderZero">
                          <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                          <input type="text" class="form-control borderZero" id="row-search-banner">
                        </div>
                    </div>
                </div>
                <div class="table-responsive fullSize topPaddingB">
                <table id="myTable" class="table noMargin normalText banner-user-table">
                  <thead>
                    <tr>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-order="desc" data-sort="banner_advertisement_subscriptions.banner_status" id="row-sort"><small><i class="fa fa"></i> Status</small></div></th>
                       <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_advertisement_subscriptions.id" id="row-sort"><small><i class="fa fa"></i> ID</small></div></th>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_advertisement_subscriptions.title" id="row-sort"><small><i class="fa"></i> Order No</div></small></th>
             <!--     <th>Placement</th> -->
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.countryCode" id="row-sort"><small><i class="fa"></i> Country Code</small></div></th>

                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_location.name" id="row-sort"><small><i class="fa"></i> Plan</div></small></th>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.name" id="row-sort"><small><i class="fa"></i> Category</div></small></th>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="amount" id="row-sort"><small><i class="fa"></i> Placement Cost</small></div></th>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="duration" id="row-sort"><small><i class="fa"></i> Duration</small></div></th>
                      <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="total_cost" id="row-sort"><small><i class="fa"></i> Total Cost</small></div></th>
                      <th class="text-right">Tools</th>
                    </tr>
                  </thead>
                  <tbody id="banner-container">
                    @foreach ($get_user_banners as $row)
                   <tr data-id="{{ $row->id}}" data-amount="{{$row->total_cost}}" class="{{$row->banner_status == 6 ? 'disabledColor':''}}">
                      <td class="grayText"><small>{{$row->banner_status==1 ? 'Pending':'' }}{{$row->banner_status==2 ? 'Approved':'' }}{{$row->banner_status==3 ? 'Rejected':'' }}{{$row->banner_status==4 ? 'Active':'' }}{{$row->banner_status==6 ? 'Disabled':'' }}</small></td>
                      <td><small>{{ $row->id }}</small></td>
                      <td><small><center>{{ $row->order_no }}</center></small></td>
                      <td><small><center>{{ $row->countryCode}}</center></small></td>
                      <td><small>{{ $row->placement_name}}</small></td>
                      <td><small>{{ $row->category_name != null ?  $row->category_name :'N/A'}}</small></td>
                      <td><small><center>{{ $row->amount }}</center></small></td>
                   <!--    <td class="grayText">{{$row->placement}}</td> -->
                      <td class="grayText"><small>{{$row->duration}} {{ $row->metric_duration == "d" ? 'days':''}}{{ $row->metric_duration == "m" ? 'months':''}}{{ $row->metric_duration == "y" ? 'years':''}}</small></td>
                      <td><small><center>${{$row->total_cost}}</center></small></td>
                      <td class="grayText">
                         <button class="btn btn-default btn-xs borderZero pull-right btn-paypal-banner {{$row->banner_status == 2 ? '':'hide'}} {{$row->banner_status == 4 ? 'hide':''}}" style="margin-right:3px;"><i class="fa fa-paypal" aria-hidden="true"></i></button>
                        @if($row->banner_status == 6)
                          <button class="btn btn-default btn-xs borderZero pull-right btn-enable-banner" disabled style="margin-right:3px;"><i class="fa fa-unlock"></i></button> 
                        @else
                          <button class="btn btn-default btn-xs borderZero pull-right btn-disable-banner" style="margin-right:3px;"><i class="fa fa-lock"></i></button>
                        @endif
                          <button class="btn btn-default btn-xs borderZero pull-right btn-view-banner" style="margin-right:3px;"><i class="fa fa-eye"></i></button>
                          <button class="btn btn-default btn-xs borderZero pull-right btn-edit-banner" style="margin-right:3px;" {{$row->banner_status == 4 ? 'disabled':''}} ><i class="fa fa-pencil"></i></button>
                          <form id="paypal-form" class="noMargin" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
                                
                          </form>
                      </td>
                    </tr> 
                    @endforeach
                  </tbody>
                </table>
                @if (count($get_user_banners) == 0)
                <div class="lightgrayText normalText normalWeight text-center topPaddingD bottomPaddingB" id="advertising-indicator">No Banner Available</div>
                @endif
              </div>
            </div>
            <input type="hidden" id="row-sort-banner">
            <input type="hidden" id="row-next-order-banner" value="desc">
            <input type="hidden" id="row-current-order-banner" value="desc">
            <input type="hidden" id="count" value="{{$get_user_banners_count}}">
            <input type="hidden" id="increment" value="5">
           <button class="btn blueButton borderZero {{ $get_user_banners_count <= 5 ? 'hide':''}}" id="btn-view-more-banners" type="button" onclick="viewMore()" style="width:100%;"><i class=""></i>View More</button>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane" id="reports">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Reports</span>
            </div>
            <div class="panel-body" style="padding-bottom: 0px;">
              <div class="normalText grayText">
                <div class="row">
                <div class="col-sm-12 noPadding">
                <div class="col-sm-12">
                <div class="panel panel-default borderZero removeBorder noMargin">
                  <div class="panel-body text-left noPadding">
                    <h3 style="margin-top:0px;">Average Rating</h3>
                    {{-- <h1>{{round($total_final_rating, 1)}}</h1> --}}
                    <span title="{{round($total_final_rating,1)}}" id="dashboard_rating" class="blueText noPadding pull-left"></span>
                  </div>
                </div>
                <hr>
                </div>
                <div class="col-sm-12 text-center noPadding hidden-sm hidden-xs">
                    <div id="columnchart_values" class="chart noPadding"></div>
                </div>
                <div class="col-sm-12 text-center noPadding visible-xs visible-sm">
                    <div id="columnchart_values_mobile" class="chart noPadding"></div>
                </div>
                <hr>

<!--                   <div class="col-sm-12 col-md-6">   
                    <div class="statisticBox">
                      <span class="statisticIcon bgAqua"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Page and Ads Visits</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_page_and_ad_visits }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgRed"><i class="fa fa-newspaper-o" aria-hidden="true"></i></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Reviews</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_reviews }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgGreen"><i class="fa fa-comment" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the ads</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_ads }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgYellow"><i class="fa fa-comments" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the users</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_user }}</b></span>
                      </div>
                    </div>
                  </div> -->
                </div>
              </div>
              <div class="table-responsive">
              <hr class="nobottomMargin">
              <input type="hidden" id="sort_ratings_title" value="asc">
              <input type="hidden" id="sort_ratings_visits" value="asc">
              <input type="hidden" id="sort_ratings_ratings" value="asc">
              <input type="hidden" id="sort_ratings_comments" value="asc">
                <table id="myTable" class="table ratings_table">
                  <thead>
                    <tr>
                      <th class="noWrap" onclick="sortRatings($('.ratings_table'), $('#sort_ratings_title').val(), 'title')">Ad Title</th>
                      <th class="noWrap" onclick="sortRatings($('.ratings_table'), $('#sort_ratings_visits').val(), 'visits')">Page Visit</th>
                      <th class="noWrap" onclick="sortRatings($('.ratings_table'), $('#sort_ratings_ratings').val(), 'ratings')">Rating</th>
                      <th class="noWrap" onclick="sortRatings($('.ratings_table'), $('#sort_ratings_comments').val(), 'comments')">Total Comments and Reviews</th>
                    </tr>
                  </thead>
                  
                  <tbody id="ratings_tbody">
                    @foreach($reports as $key => $report)
                    <tr class="{{ $key > 9 ? 'hide' : ''}}" data-title="{{$report->title}}" data-visits="{{$report->overall_visits}}" data-ratings="{{round($report->overall_rating,1)}}" data-comments="{{$report->overall_comments}}">
                      <td>{{$report->title}}</td> 
                      <td>{{$report->overall_visits}}</td> 
                      <td>{{round($report->overall_rating,1)}}</td> 
                      <td>{{$report->overall_comments}}</td> 
                    </tr>
                     @endforeach
                  </tbody>
                 
                </table>  
                </div> 
                            
                </div>
                <div id="view_more_ratings" class="text-center" style="margin-left:-15px; margin-right:-15px;">
                    <input type="hidden" id="no_of_results" value="0">
                    <button class="btn blueButton borderZero" type="button" id="btn-view-more-ratings" style="width:100%;">View More</button>
                  </div>     
<!--              <div class="normalText grayText">
                Total posted ads: {{ count($total_ad_posted) }} <br>
                Total posted auction ads: {{ count($total_auction_posted) }} <br>
                Total pictures uploaded: {{ count($total_uploaded_photos) }} <br>
                Plan will expire on : {{ $rows->plan_expiration }} <br>
              </div> -->
            </div>
          </div>
        </div>
<!--         <div role="tabpanel" class="tab-pane" id="statistics">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Statistics</span>
            </div>
            <div class="panel-body">
              <div class="normalText grayText">
                <div class="col-sm-12 noPadding">
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgAqua"><i class="fa fa-tachometer" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Page and Ads Visits</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_page_and_ad_visits }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgRed"><i class="fa fa-newspaper-o" aria-hidden="true"></i></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Reviews</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_reviews }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgGreen"><i class="fa fa-comment" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the ads</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_ads }}</b></span>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="statisticBox">
                      <span class="statisticIcon bgYellow"><i class="fa fa-comments" aria-hidden="true"></i></span>
                      <div class="statisticContent">
                        <span class="mediumText text-uppercase">Number of Comments placed for the users</span>
                        <br>
                        <span class="xlargeText"><b>{{ $no_of_comments_for_user }}</b></span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div> -->
        <div role="tabpanel" class="tab-pane" id="invoice">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Invoice</span>
              <span class="pull-right" id="sort-param">
              <ul class="nav navbar-nav sortDropdown">
                      <li class="dropdown open">
                        <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder" data-toggle="dropdown" aria-expanded="true"><span id="bidding-count-container" style="font-size:14px;">Sort by </span> <span class="caret"></span></button>
                        <ul class="dropdown-menu sortDrop">
                          <li><a href="#" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_transaction_type').val(), 'transaction_type')"> Transaction Type</a></li>
                          <li><a href="#" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_cost').val(), 'cost')"> Cost</a></li>
                          <li><a href="#" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_date').val(), 'date')"> Date</a></li>
                          <input type="hidden" id="sort_invoice_transaction_type" value="asc">
                          <input type="hidden" id="sort_invoice_cost" value="asc">
                          <input type="hidden" id="sort_invoice_date" value="asc">
                        </ul>
                      </li>
                    </ul>
                  </span>
            </div>
            <div class="panel-body" style="padding-bottom: 0px;">
              <div class="normalText grayText">
              <div class="table-responsive">
                <table id="invoiceTable" class="table">
                  <thead>
                    <tr>
                      <th class="">Invoice No.</th>
                      <th class="" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_transaction_type').val(), 'transaction_type')">Transaction Type</th>
                      <th class="">Details</th>
                      <th class="" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_cost').val(), 'cost')">Cost</th>
                      <th class="" onclick="sortInvoice($('#invoiceTable'), $('#sort_invoice_date').val(), 'date')">Date</th>
                      <th class="">Options</th>
                      <!-- <th class="noWrap">Total Posted Ads</th>
                      <th class="noWrap">Total Posted Auction</th>
                      <th class="noWrap">Total Pictures Uploaded</th>
                      <th class="noWrap">Plan Expiration</th> -->
                    </tr>
                  </thead>
                  <tbody id="invoice_tbody">
                    @foreach($invoices as $key => $invoice)
                    <tr data-invoice_no='{{$invoice->invoice_no}}' data-cost="{{$invoice->cost}}" data-date="{{strtotime($invoice->created_at)}}" data-transaction_type="{{$invoice->transaction_type}}" class="{{$key > 9 ? 'hide' : ''}}">
                      <td>{{$invoice->invoice_no}}</td> 
                      <td>{{$invoice->transaction_type}}</td> 
                      <td>{{$invoice->details}}</td> 
                      <td>{{$invoice->cost}} {{$invoice->currency}}</td> 
                      <td>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $invoice->created_at)->format('m/d/y') }}</td> 
                      <td>
                        <button class="borderZero removeborder noBackground mediumText print-invoice"><i class="fa fa-print"></i></button>
                        <a target="_blank" style="color: #999999;" role="button" href='{{url("pdfinvoice")}}?invoice_no={{$invoice->invoice_no}}' class="borderZero removeborder noBackground mediumText"><i class="fa fa-file-pdf-o"></i></a>
                      </td> 
                    </tr>
                    @endforeach
                  </tbody>
                </table>  
                </div> 
                            
                </div>
                <iframe onload="isLoaded()" id="loaderFrame" style="visibility: hidden; height: 1px; width: 1px;"></iframe>
                <div class="text-center" style="margin-left:-15px; margin-right:-15px;" id="view_more_invoice">
                    <button id="invoice_vmbtn" class="btn blueButton borderZero" type="button" style="width:100%;">View More</button>
                  </div>     
<!--              <div class="normalText grayText">
                Total posted ads: {{ count($total_ad_posted) }} <br>
                Total posted auction ads: {{ count($total_auction_posted) }} <br>
                Total pictures uploaded: {{ count($total_uploaded_photos) }} <br>
                Plan will expire on : {{ $rows->plan_expiration }} <br>
              </div> -->
            </div>
          </div>
        </div>

        <d iv role="tabpanel" class="tab-pane" id="profileSettings">
          <div class="panel panel-default removeBorder borderZero borderBottom ">
            <div class="panel-heading panelTitleBar">
              <span class="panelTitle">Settings</span>
            </div>
            <div class="panel-body noBottomPadding">
              <div id="load-settings-form" class="loading-pane hide">
                      <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
              </div>
              <div id="settings-form-notice"></div> 
              <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              PASSWORD
              </div>
               
            {!! Form::open(array('url' => 'setting/save', 'role' => 'form', 'class' => '','id'=>'form-settings_save')) !!} 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
<!--                     <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Current Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                          <input name="current_password" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div> -->
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">New Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="password" class="form-control borderZero inputBox fullSize" type="password" placeholder="Leave blank for unchanged">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Re-Type Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <input name="password_confirmation"  class="form-control borderZero inputBox fullSize" type="password" placeholder="Leave blank for unchanged">
                        </div>
                      </div>
                </div>
<!-- 
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    
                   <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                              <input name="password" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Confirm Password</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-9 col-sm-12 col-xs-12 ">
                              <input name="password_confirmation" placeholder="Leave blank for unchanged" class="form-control borderZero inputBox fullSize" type="password">
                        </div>
                      </div>
                    </div>   
                </div> -->
            </div>

              </div>
<!--                 <div class="normalText grayText {{$rows->usertype_id == 1 ? '':'hide'}}" id="location-pane" >
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              LOCATION
              </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                <div class="col-lg-12 col-md-12 col-xs-12">
                    <div class="form-group bottomMargin">
                      <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                          <select name="country" id="client-country" class="form-control borderZero inputBox fullSize">
                              <option class="hide">Select</option>
                               @foreach($countries as $row)
                                <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':'' }}>{{$row->countryName}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                     <div class="form-group bottomMargin">
                      <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                          <select name="city" id="client-city" class="form-control borderZero inputBox fullSize">
                               @foreach($cities as $row)
                                <option value="{{$row->id}}" {{$rows->city == $row->id ? 'selected':'' }}>{{$row->name}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
              </div> -->
                <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText borderbottomLight noleftPadding topPaddingD bottomMarginB bottomPadding redText ">
              NEWSLETTER
              </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
                    <div class="form-horizontal bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="weekly_newsletter" id="client-weekly_newsletter" value="1" {{$rows->weekly_newsletter == "1" ? 'checked':''}}> Receive Weekly</label>  
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="monthly_newsletter" id="client-monthly_newsletter" value="1" {{$rows->monthly_newsletter == "1" ? 'checked':''}}> Receive Monthly</label>  
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding "><input class="checkboxAlign" type="checkbox" name="promotion_newsletter" id="client-promotion_newsletter" value="1" {{$rows->promotion_newsletter == "1" ? 'checked':''}}> Receive Promotion</label>  
                    </div>
<!--                     <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding">Receive Monthly</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                         <input type="checkbox" value="">
                        </div>
                      </div>
                    </div>
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Receive Promotions</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                         <input type="checkbox" value="">
                        </div>
                      </div>
                    </div> -->
            </div>
              </div>

               <div class="normalText grayText">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 normalText topPaddingD borderbottomLight noleftPadding bottomMarginB bottomPadding redText ">
              LOCATION & LANGUAGE
              </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomMargin">
                    <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <select name="country" id="client-geo-country" class="form-control borderZero inputBox fullSize">
                              <option class="hide">Select</option>
                               @foreach($countries as $row)
                                <option value="{{$row->id}}" {{$rows->country == $row->id ? 'selected':'' }}>{{$row->countryName}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                     <div class="form-group bottomMargin">
                      <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City</label>  
                      <div class="inputGroupContainer">
                        <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                          <select name="city" id="client-geo-city" class="form-control borderZero inputBox fullSize">
                               @foreach($cities as $row)
                                <option value="{{$row->id}}" {{$rows->city == $row->id ? 'selected':'' }}>{{$row->name}}</option>
                               @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                        <div class="form-group bottomMargin">
                          <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Localization</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                 <input name="text" name="localization" value="{{$localization}}" id="client-geo-localization" class="form-control borderZero inputBox fullSize" readonly type="text">
                            </div>
                          </div>
<!--                         <div class="form-group bottomMargin">
                          <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding">Arabic</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                             <input type="radio" value="" name="language">
                            </div>
                          </div>
                        </div>
                        <div class="form-group bottomMargin">
                          <label class="col-md-8 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Chinese</label>  
                          <div class="inputGroupContainer">
                            <div class="input-group col-md-4 col-sm-12 col-xs-12 ">
                             <input type="radio" value="" name="language">
                            </div>
                          </div>
                        </div> -->
                    </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bordertopLight noPadding topPaddingB ">
            <div class="row">
            <input type="hidden" name="id" id="client-id" value="{{$user_id}}">
              <button class="btn blueButton borderZero noMargin fullW  pull-right" type="submit">Update</button>
              </div>
            </div>
             {!! Form::close() !!}

              </div>
            </div>
          </div>
        </div>
     </div>
    </div>
      </div>
    </div>
  </div>
    
<!-- Remove Modal -->
  <div id="modal-remove" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content borderzero">
        <div id="modal-remove-header" class="modal-header modal-yeah">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-remove-title"></h4>
        </div>
        <div class="modal-body">
            <p class="modal-remove-body"></p> 
        </div>
        <div class="modal-footer">
          <input type="hidden" id="row-remove-hidden">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal" id="modal-remove-button">Yes</button>
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
    </div>
    <!-- modal send msg -->
                <div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
                      <div class="modal-dialog">
                        <div class="modal-content borderZero">
                            <div id="load-message-form" class="loading-pane hide">
                              <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                            </div>
                          {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
                          <div class="modal-header modalTitleBar">
                              <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title panelTitle"><i class="fa fa-plane rightPadding"></i>Send Message</h4>
                              </div>
                          <div class="modal-body">
                            <div id="form-notice"></div>
<!--                             <input class="fullSize" type="text" name="title" id="row-title" value="Inquiry">
                            <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea> -->
                      <div class="">
                        <label for="email">Title:</label>
                        <input class="form-control borderZero" type="text" name="title" id="row-title">
                      </div>
                      <div class="">
                        <label for="pwd">Message:</label>
                        <textarea class="form-control borderZero" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
                      </div>
                          </div>
                          <div class="modal-footer">
                            <input type="hidden" name="reciever_id" id="row-reciever_id" value="">
                            <button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
                            <button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                          </div>
                          {!! Form::close() !!}
                        </div>
                      </div>
                    </div>

                    <div id="modalExpiration" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content borderZero">
                          <div class="modal-header modalTitleBar">
                        <button type="button" class="close closeButton" data-dismiss="modal">×</button>
                          <h4 class="modal-title panelTitle expiry-countdown">Your Plan is Expiring soon!</h4>
                        </div>
                          <div class="modal-body">
                            <p>Your current plan is <span class="text-capitalize">{{strtoupper($current_user_plan_type)}}</span>, Upgrade now and get a discount, Would you like to extend?</p>
                            <div class="discounted_container">

                            </div>
                            <!-- <div class='text-capitalize discounted_container'></div> -->
                          </div>
                          <div class="clearfix"></div>
                          <div class="modal-footer">
                            <button type="button" class="btn blueButton borderZero yesUpgrade" data-dismiss="modal">Yes</button>
                            <button type="button" class="btn redButton borderZero" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @if($rows->sms_verification_status == 0)
                    <div id="modalVerify" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content borderZero">
                          <div class="modal-header modalTitleBar">
                        <button type="button" class="close closeButton" data-dismiss="modal">×</button>
                          <h4 class="modal_verification modal-title panelTitle">VERIFY YOUR ACCOUNT</h4>
                        </div>
                          <div class="modal-body form_container">
                          <form role="form" class="form-horizontal" id="sms_send_code">
                            <p>Verify Your Account, Enter your mobile number.</p>
<!--                             <span>Mobile Number: <input type="tel" class="mobile_no_verify" value="{{$rows->mobile}}" name="mobile_number" required="required"></span>
                            <button type="submit" class="sms_verify_btn btn blueButton borderZero verify_account_sms">Send Code</button> -->
                            <div class="input-group">
                              <input type="tel" class="form-control borderZero mobile_no_verify" value="{{$rows->mobile}}" name="mobile_number" placeholder="Enter your mobile number..."  required="required">
                              <span class="input-group-btn">
                                <button class="btn btn-code borderZero sms_verify_btn verify_account_sms" type="submit">Send</button>
                              </span>
                            </div>
                          </form>
                          </div>
                          <div class="clearfix"></div>
                          <div class="modal-footer">
                            <button type="button" class="btn redButton borderZero cancel" data-dismiss="modal">Cancel</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    @endif


@stop
@include('user.dashboard.form')
