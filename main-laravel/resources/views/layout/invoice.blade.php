<html>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
</style>
<body>
<table>
  <tr>
    <td>
      <img src="http://yakolak.xerolabs.co/img/yakolaklogo.png" style="padding-bottom: 10px;">
    </td>
    <td style="line-height: 1; text-align: right;">
      <small><small>Yakolak</small></small><br>
      <small><small></small>{{App\ContactDetails::pluck('address_line')}}</small><br>
      <small><small>{{App\ContactDetails::pluck('city')}}, {{App\ContactDetails::pluck('zip')}}</small></small><br>
      <small><small>{{App\ContactDetails::pluck('tel')}}</small></small><br><br>
      <small><small>Invoice Status: Paid</small></small><br>
      <small><small>{{$date}}</small></small>
    </td>
  </tr>
</table>
<br>
<table class="table-main">

    <thead class="thead-table">
    <tr>
        <th style="background-color: #ffffff; color: #000000;">Email Address</th>
        <th style="background-color: #ffffff; color: #000000;">Name</th>
    </tr>
    </thead>

    <tbody class="tbody-table">
    <tr>
        <td>{{$email}}</td>
        <td style="text-align: center;">{{$name}}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
    </tr>
    </tbody>

    <thead class="thead-table">
    <tr>
        <th style="background-color: #ffffff; color: #000000;">Payment Method</th>
        <th style="background-color: #ffffff; color: #000000;">Currency</th>
    </tr>
    </thead>

    <tbody class="tbody-table">
    <tr>
        <td>Paypal</td>
        <td style="text-align: center;">{{$currency}}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
    </tr>
    </tbody>

    <thead class="thead-table">
    <tr>
        <th style="background-color: #ffffff; color: #000000;">Items</th>
        <th style="background-color: #ffffff; color: #000000;">Amount</th>
    </tr>
    </thead>

    <tbody class="tbody-table">
    <tr>
        <td>{{$transaction_type}} ({{$extended_details}})</td>
        <td style="text-align: center;">{{$cost}} {{$currency}}</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td></td>
    </tr>
    <tr>
        <td><strong>TOTAL</strong></td>
        <td style="text-align: center;"><strong>{{$cost}} {{$currency}}</strong></td>
    </tr>
    </tbody>
</table>

</div>
</body>
</html>