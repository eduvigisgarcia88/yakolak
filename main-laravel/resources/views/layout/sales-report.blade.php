<html>
<style>
body
{
  margin: 15px;
  background-color: #ffffff;
  font-family: 'Helvetica';
}
table {
  width: 100%;
}
table.table-main {
  background-color: #1e2631;
}
thead.thead-table {
  background-color: #26629A;
  color: #ffffff;
}
tbody.tbody-table {
  background-color: #F9F9F9;
}
h3 {
  margin: 10px;
  text-decoration: underline;
}
</style>
<body>
<table>
  <tr>
    <td>
      <img src="http://yakolak.xerolabs.co/img/yakolaklogo.png" style="padding-bottom: 10px;">
    </td>
    <td style="line-height: 1; text-align: right;">
      <small><small>Yakolak</small></small><br>
      <small><small></small>{{App\ContactDetails::pluck('address_line')}}</small><br>
      <small><small>{{App\ContactDetails::pluck('city')}}, {{App\ContactDetails::pluck('zip')}}</small></small><br>
      <small><small>{{App\ContactDetails::pluck('tel')}}</small></small><br><br>
    </td>
  </tr>
</table>
<br>
<table class="table-main">

    <thead class="thead-table">
    <tr>
        <th style="background-color: #ffffff; color: #000000;">INVOICE NO.</th>
        <th style="background-color: #ffffff; color: #000000;">USER</th>
        <th style="background-color: #ffffff; color: #000000;">EMAIL</th>
        <th style="background-color: #ffffff; color: #000000;">TYPE</th>
        <th style="background-color: #ffffff; color: #000000;">DETAILS</th>
        <th style="background-color: #ffffff; color: #000000;">COST</th>
        <th style="background-color: #ffffff; color: #000000;">DATE</th>
    </tr>
    </thead>

    <tbody class="tbody-table">
      {!! $table_data !!}
    </tbody>
</table>

</div>
</body>
</html>