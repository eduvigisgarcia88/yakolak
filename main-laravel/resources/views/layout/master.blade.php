<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

 <!-- For editor -->
  <!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" />
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
  <link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.css" /> -->


    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{ isset($title) ? $title : 'Yakolak' }}</title>

    <!-- Bootstrap -->
   {!! HTML::style('/plugins/css/jquery.rateyo.min.css') !!}
   {!! HTML::style('/css/bootstrap.min.css') !!}
   {!! HTML::style('/font-awesome/css/font-awesome.min.css') !!}
   {!! HTML::style('/css/styles.css') !!}
   {!! HTML::style('/css/common.css') !!}
   {!! HTML::style('/css/backend.css') !!}
   {!! HTML::style('/css/xerolabs/backend/custom.css') !!}
   {!! HTML::style('/plugins/css//toastr.min.css') !!}
   {!! HTML::style('/plugins/css//fileinput.min.css') !!}
   {!! HTML::style('/plugins/css/bootstrap-switch.css') !!}
   {!! HTML::style('/plugins/css/select2-bootstrap.css') !!}
   {!! HTML::style('/plugins/css/select2.css') !!}
   {!! HTML::style('/plugins/css/bootstrap-multiselect.css') !!}
   {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
   <!-- Editor -->
   {!! HTML::style('/plugins/css/summernote.css') !!}


   {!! HTML::style('/css/xerolabs/common.css') !!}



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
          <nav class="navbar navbar-default noMargin">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('admin/dashboard/main')}}"><img alt="Brand" src="/img/yakolaklogo.png" height="70" width="100" class="img-responsive"></a>
              </div>
              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Users</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/users/front-end' ? ' class=active' : '' }}><a href="{{ url('admin/users/front-end') }}" ><span id="hide" data-toggle="tooltip"  data-placement="right">Front-End Users</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/users/administrators' ? ' class=active' : '' }}><a href="{{ url('admin/users/administrators') }}" ><span id="hide" data-toggle="tooltip"  data-placement="right">Administrators</span></a></li>
                      <!-- <li><a href="#">Back-End Users</a></li> -->
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Listings</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/listings/ads' ? ' class=active' : '' }}><a href="{{url('admin/listings/ads')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Ads</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/listings/auctions' ? ' class=active' : '' }}><a href="{{url('admin/listings/auctions')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Auction</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/listings/featured-ad' ? ' class=active' : '' }}><a href="{{url('admin/listings/featured-ad')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Featured</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/listings/comments-and-reviews' ? ' class=active' : '' }}><a href="{{url('admin/listings/comments-and-reviews')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Comments And Reviews</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/listings/spam-and-bots' ? ' class=active' : '' }}><a href="{{url('admin/listings/spam-and-bots')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Spam and Bots</span></a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Marketing</a>
                    <ul class="dropdown-menu">
                    <li{{ Route::current()->getPath() == 'admin/marketing/newsletter' ? ' class=active' : '' }}><a href="{{url('admin/marketing/newsletter')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Newsletter</span></a></li> 

                    <li{{ Route::current()->getPath() == 'admin/marketing/plans-and-promotions' ? ' class=active' : '' }}><a href="{{url('admin/marketing/plans')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Listing Plans</span></a></li>

                    <li{{ Route::current()->getPath() == 'admin/marketing/bids' ? ' class=active' : '' }}><a href="{{url('admin/marketing/bids')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Bid Plans</span></a></li>

                    <li{{ Route::current()->getPath() == 'admin/marketing/banners' ? ' class=active' : '' }}><a href="{{url('admin/marketing/banners')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Banners</span></a></li>

                    <li{{ Route::current()->getPath() == 'admin/marketing/banner-settings' ? ' class=active' : '' }}><a href="{{url('admin/marketing/banner-settings')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Banner Rates</span></a></li>

                    <li{{ Route::current()->getPath() == 'admin/marketing/banner-default' ? ' class=active' : '' }}><a href="{{url('admin/marketing/banner-default')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Banner Default</span></a></li>

                    <li{{ Route::current()->getPath() == 'admin/marketing/recent-searches' ? ' class=active' : '' }}><a href="{{url('admin/marketing/recent-searches')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Recent Searches</span></a></li>
                    
                    <!--   <li{{ Route::current()->getPath() == 'admin/dashboard/email-template' ? ' class=active' : '' }}><a href="{{url('admin/dashboard/email-template')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Template Settings</span></a></li>
                      <li><a href="{{url('admin/plan/vendor')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Plans</span></a></li>
                      <li><a href="{{url('admin/plan-rates/vendor')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Plan Rates</span></a></li>
                       <li{{ Route::current()->getPath() == 'admin/dashboard/banner' ? ' class=active' : '' }}><a href="{{url('admin/dashboard/banner')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Banners</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/banners/rates' ? ' class=active' : '' }}><a href="{{url('admin/banner/rates')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Banner Rates</span></a></li> -->
                    </ul>
                  </li>
                   </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Reports</a>
                    <ul class="dropdown-menu">
                      <li {{ Route::current()->getPath() == 'admin/report' ? ' class=active' : '' }}><a href="{{url('admin/report')}}"><span id="hide" data-toggle="tooltip" data-placement="right">User Registrations</span></a></li>
                      <li {{ Route::current()->getPath() == 'admin/report-ads' ? ' class=active' : '' }}><a href="{{url('admin/report-ads')}}"><span id="hide" data-toggle="tooltip" data-placement="right">Ads/Bids</span></a></li>
                      <li {{ Route::current()->getPath() == 'admin/report-sales' ? ' class=active' : '' }}><a href="{{url('admin/report-sales')}}"><span id="hide" data-toggle="tooltip" data-placement="right">Sales</span></a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Themes</a>
                    <ul class="dropdown-menu">
                     <li{{ Route::current()->getPath() == 'admin/settings/theme/plan' ? ' class=active' : '' }}><a href="{{url('admin/settings/theme/plan')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Plan</span></a></li>
                     <li{{ Route::current()->getPath() == 'admin/settings/theme/country' ? ' class=active' : '' }}><a href="{{url('admin/settings/theme/country')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Countries</span></a></li>
                    </ul>
                  </li>
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Settings</a>
                    <ul class="dropdown-menu">
                      <li {{ Route::current()->getPath() == 'admin/settings/pages' ? ' class=active' : '' }}><a href="{{url('admin/settings/pages')}}"><span id="hide" data-toggle="tooltip" data-placement="right">Page Contents</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/settings/categories' ? ' class=active' : '' }}><a href="{{url('admin/settings/categories')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Categories</span></a></li>
                       <li{{ Route::current()->getPath() == 'admin/settings/custom-attributes' ? ' class=active' : '' }}><a href="{{url('admin/settings/custom-attributes')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Custom Attributes</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/settings/countries' ? ' class=active' : '' }}><a href="{{url('admin/settings/countries')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Countries</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/settings/localization' ? ' class=active' : '' }}><a href="{{url('admin/settings/localization')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Localization</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/settings/configuration' ? ' class=active' : '' }}><a href="{{url('admin/settings/configuration')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Configuration</span></a></li>
                    </ul>
                  </li>
                
                <!--   <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Moderation Management</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/moderation/banner' ? ' class=active' : '' }}><a href="{{url('admin/moderation/banner')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Banner Advertisements</span></a></li>
                    </ul>
                  </li> -->

                  <!--  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Plan Management</a>
                    <ul class="dropdown-menu">
                      <
                    </ul>
                  </li> -->
        <!--           <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Category Management</a>
                    <ul class="dropdown-menu">
                     
                    </ul>
                  </li> -->
           <!--        <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Banner Management</a>
                    <ul class="dropdown-menu">
                     
                    </ul>
                  </li> -->
              <!--     <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Settings</a>
                    <ul class="dropdown-menu">
                      <li{{ Route::current()->getPath() == 'admin/dashboard/countries' ? ' class=active' : '' }}><a href="{{url('admin/dashboard/countries')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Countries</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/dashboard/cities' ? ' class=active' : '' }}><a href="{{url('admin/dashboard/cities')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Cities</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/dashboard/category' ? ' class=active' : '' }}><a href="{{url('admin/dashboard/category')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Categories</span></a></li>
                      <li{{ Route::current()->getPath() == 'admin/category/custom-attributes' ? ' class=active' : '' }}><a href="{{url('admin/category/custom-attributes')}}"><span id="hide" data-toggle="tooltip"  data-placement="right">Manage Custom Attributes</span></a></li>
                    </ul> -->
                   <!--  <ul class="dropdown-menu">
                      <li><a href="#"></a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul> -->
                  <!-- </li> -->
     <!--               <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Promotion Management</a>
                    <ul class="dropdown-menu">
                  
                    </ul> -->
                   <!--  <ul class="dropdown-menu">
                      <li><a href="#"></a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul> -->
       <!--            </li> -->
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="img-circle" src="/img/avatar.jpg" width="20" height="20"> Hi {{ Auth::user()->name }}! <span class="badge">5</span> <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li><a href="#"></a></li>
                        <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li>
                      <!--   <li role="separator" class="divider"></li>
                        <li><a href="#">Separated link</a></li> -->
                        <li role="separator" class="divider"></li>
                        <li><a href="{{ url('admin/dashboard/logout')}}"><i class="fa fa-sign-out"></i> Logout</a></li>
                      </ul>
                    </li>
                  </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
        <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderZero" data-url="" data-id="" id="dialog-confirm-backend">Yes</button>
                <button type="button" class="btn btn-default borderZero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>
      <div id="content">
        @yield('content')
      </div>
    </div>
      <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}
    {!! HTML::script('/js/form.js') !!}
    {!! HTML::script('/plugins/js/toastr.min.js') !!}
    <!-- {!! HTML::script('/plugins/js/jquery.jscroll.min.js') !!} -->
    {!! HTML::script('/plugins/js/fileinput.min.js') !!}
    {!! HTML::script('/plugins/js/bootstrap-switch.js') !!}
    {!! HTML::script('/plugins/js/jquery.countdown.js') !!}
    {!! HTML::script('/plugins/js/jquery.countdown.min.js') !!}
    {!! HTML::script('/plugins/js/select2.min.js') !!}
    {!! HTML::script('/plugins/js/loader.js') !!}
    {!! HTML::script('/plugins/js/jquery.rateyo.min.js') !!}
    {!! HTML::script('/plugins/js/bootstrap-multiselect.js') !!}
    {!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
    {!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
     @yield('scripts')
    <!-- Editor -->
    {!! HTML::script('/plugins/js/summernote.js') !!}

    <script type="text/javascript">
      $('[data-toggle="tooltip"]').tooltip(); 
    </script>
  </body>

</html>