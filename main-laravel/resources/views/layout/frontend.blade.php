<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    {{-- //ADS VIEW --}}
@if(Request::route()->getName() == "SEO_URL" && isset($getads_info))
    <meta property="og:title" content="{{ $getads_info->title.' ('.$getads_info->city.', '.$getads_info->countryName .') '.'- Yakolak' }}" />
    <meta property="og:description" content="{{ mb_strimwidth(str_replace('-', ' ', $getads_info->parent_category_slug).', '.$getads_info->description, 0, 150, '...') }}" />
    <meta property="og:image" content="{{URL::route('uploads', array(), false).'/ads/'.$primary_photos->photo}}" />
    <meta property="og:url" content="{{Request::url()}}" />
    <meta property="og:site_name" content="Yakolak" /> 
    <meta property="og:type" content="website" />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@yakolak" />
    <meta name="twitter:title" content="{{ $getads_info->title.' ('.$getads_info->city.', '.$getads_info->countryName .') '.'- Yakolak' }}" />
    <meta name="twitter:description" content="{{ mb_strimwidth(str_replace('-', ' ', $getads_info->parent_category_slug).', '.$getads_info->description, 0, 150, '...') }}" />
    <meta name="twitter:image" content="{{URL::route('uploads', array(), false).'/ads/'.$primary_photos->photo}}" />
    <meta name="Robots" content="index, follow" />
    <meta name="title" content="{{ $getads_info->title.' ('.$getads_info->city.', '.$getads_info->countryName .') '.'- Yakolak' }}" />
    <meta name="description" content="{{ mb_strimwidth(str_replace('-', ' ', $getads_info->parent_category_slug).', '.$getads_info->description, 0, 150, '...') }}" />
@endif
    {{-- //GLOBAL SEARCH --}}
@if(Request::route()->getName() == "CAT_URL")
@if(strlen(Request::route()->parameters()['country_code']) == 2)
    <meta name="Robots" content="index, follow" />
    <meta name="title" content="{{ mb_strimwidth(request()->get('search').(request()->route('country_code') == 'all' ? '' : (request()->get('search') != '' ? ' ' : '').(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))).' - Yakolak', 0, 60, '...') }}" />
    <meta name="description" content="{{ mb_strimwidth('Showing results for '.request()->get('search').(request()->route('country_code') == 'all' ? '' : ' in '.(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))), 0, 150, '...') }}" />
@else
    <meta name="Robots" content="nofollow" />
@endif
@endif
    {{-- //SEARCH with CATEGORY --}}
@if(Request::route()->getName() == "CAT_NEW_URL")
    
    
    @if(request()->url().'?listing='.Request::get('listing') == url().'/'.Session::get('default_location').'/all-cities/all-categories?listing=buy-and-sell')
      <link rel="canonical" href="{{url(Session::get('default_location').'/all-cities/buy-and-sell')}}" />
    @endif

    @if(request()->url().'?listing='.Request::get('listing') == url().'/'.Session::get('default_location').'/all-cities/all-categories?listing=auctions')
      <link rel="canonical" href="{{url(Session::get('default_location').'/all-cities/auctions')}}" />
    @endif
           
    <meta name="Robots" content="index, follow" /> 
    <meta name="title" content="{{ mb_strimwidth(request()->get('search').(request()->route('country_code') == 'all' ? '' : (request()->get('search') != '' ? ' ' : '').(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))).' '.(str_replace('-', ' ', request()->route('slug'))).' - Yakolak', 0, 60, '...') }}" />
    <meta name="description" content="{{ mb_strimwidth('Showing results'.(request()->get('search') != '' ? ' for '.request()->get('search') : '').(request()->route('country_code') == 'all' ? '' : ' in '.(request()->route('city') == 'all-cities' ? '' : str_replace('-', ' ', request()->route('city')).', ').(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))).' of '.str_replace('-', ' ',request()->route('slug')).(request()->route('q') != '' ? ' '.str_replace('-',' ', request()->route('q')) : ''), 0, 150, '...') }}" />
@endif

@if(Request::route()->getName() == "SEO_URL" && !isset($getads_info))
    <meta name="Robots" content="index, follow" /> 
    <meta name="title" content="{{ mb_strimwidth(request()->get('search').(request()->route('country_code') == 'all' ? '' : (request()->get('search') != '' ? ' ' : '').(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))).' '.(str_replace('-', ' ', request()->route('parent_category_slug'))).' - Yakolak', 0, 60, '...') }}" />
    <meta name="description" content="{{ mb_strimwidth('Showing results'.(request()->get('search') != '' ? ' for '.request()->get('search') : '').(request()->route('country_code') == 'all' ? '' : ' in '.(request()->route('ad_type_slug') == 'all-cities' ? '' : str_replace('-', ' ', request()->route('ad_type_slug')).', ').(App\Country::where('countryCode', request()->route('country_code'))->pluck('countryName'))).' of '.str_replace('-', ' ',request()->route('parent_category_slug')).(request()->route('category_slug') != '' ? ' '.str_replace('-',' ', request()->route('category_slug')) : ''), 0, 150, '...') }}" />
@endif
    
@if(Route::current()->getPath() == '{vanity}' && Request::route('vanity') == 'support')
    <meta name="Robots" content="index, follow" />
    <meta name="title" content="Support - Yakolak" />
    <meta name="description" content="{{mb_strimwidth($contact_details->description, 0, 150, '...')}}" />
@endif
    
@if(Route::current()->getPath() == '{country_code}/all-categories')
    <meta name="Robots" content="index, follow" />
    <meta name="title" content="All Categories - Yakolak" />
    <meta name="description" content="All Categories" />
@endif

    <!-- ^ Alkane Code -->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Yakolak</title>

    <!-- Bootstrap -->
     {!! HTML::style('css/bootstrap/bootstrap.min.css') !!}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

      <!-- Font Awesome -->
      {!! HTML::style('font-awesome/css/font-awesome.min.css') !!}
      <!-- Glyphicons -->
     <!--  {!! HTML::style('font/font-awesome.min.css') !!} -->


      <!-- Plugins -->
      {!! HTML::style('/plugins/css/fileinput.min.css') !!}
      {!! HTML::style('/plugins/css/toastr.min.css') !!}
      {!! HTML::style('/plugins/css/jquery.rateyo.min.css') !!}
      {!! HTML::style('/plugins/css/morris.css') !!}
      {!! HTML::style('/plugins/css/select2-bootstrap.css') !!}
      {!! HTML::style('/plugins/css/select2.css') !!}
      {!! HTML::style('https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/2.7.0/css/flag-icon.min.css') !!}
       <!-- Editor -->
      <!-- Xerolabs -->
      {!! HTML::style('/css/xerolabs/style.css') !!}
     
      {!! HTML::style('/css/xerolabs/common.css') !!}
      {!! HTML::style('/css/xerolabs/frontend/custom.css') !!}
   
 
      {!! HTML::style('/plugins/components/datetimepicker/build/css/bootstrap-datetimepicker.min.css') !!}
      {!! HTML::style('/css/plugins/bootstrap-multiselect.css') !!}
</head>
<body class="scrollbar" id="style-1">

@if (Auth::guest())
<div class="modal fade" id="modal-ads" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content borderZero">
      <div class="modal-header modal-warning">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Warning</h4>
        </div>
        <div class="modal-body">
        <h5>Your not logged in!</h5>
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default borderZero" data-dismiss="modal">Close</button>
          <a href="{{ url('auth/login') }}"><button type="submit" class="btn btn-warning borderZero" data-dismiss="modal">Login</button></a>

        </div>
      </div>
    </div>
  </div>
 @else
  <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close closeButton" data-dismiss="modal"><span  aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn blueButton borderZero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn redButton borderZero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>

     <!-- message Dialog -->
        <div class="modal" id="modal-dialog-message" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <!-- <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> -->
                <h4 id="dialog-message-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-message-body"></div>
              <div class="modal-footer">
                <!-- <button type="button" class="btn blueButton borderZero" data-url="" data-id="" id="dialog-confirm">Yes</button> -->
                <button type="button" class="btn redButton borderZero btn-remove-message-dialog">Close</button>
              </div>
            </div>
          </div>
        </div>

 @endif

<div id="mobilesidebarAccount" class="visible-sm visible-xs">

<div class="panel-group" id="mobileAccordion">
   <li class="whiteText sidebarTitle">
    <div class="mediumText whiteText bottomPaddingB"><b>@if(Auth::check()) Hi {{Auth::user()->username}}@else Welcome @endif</b></div>
    @if(Auth::check())
    <div class="sidebarProfile">
      <div class="commenterImage">
        <img src="{{ URL::route('uploads', array(), false).'/'.Auth::user()->photo}}" class="avatar" alt="user profile image" style="width: 50px; height: 50px;">
      </div>
      <div class="statisticcommentText">
        <p class="normalText leftPaddingB bottomMarginB">10 Active Ads</p>
        <p class="normalText leftPaddingB bottomMarginB">10 Active Auctions</p>
      </div>
    </div>
     @endif
  </li>
 
  @if (!Auth::check()) 
  <li class="modalSidebar"><a class="whiteText normalText" href="#"  data-toggle="modal" data-target="#modal-login"><i class="fa fa-sign-in rightPadding" aria-hidden="true"></i> Login</a></li>
  <li class="modalSidebar"><a class="whiteText normalText" href="#" class="modalSidebar" data-toggle="modal" data-target="#modal-register"><i class="fa fa-pencil-square-o rightPadding" aria-hidden="true"></i> Register</a></li>
  @else
  <li><a class="whiteText normalText normalText" href="{{url('dashboard')}}"><i class="fa fa-tachometer rightPadding" aria-hidden="true"></i> Dashboard <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>

  <li><a class="mesClick whiteText normalText" href="{{ url('dashboard')}}#messages"><i class="fa fa-envelope rightPadding" aria-hidden="true"></i> Messages <span class="badge leftMargin"> {{$message_count}}</span><span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="aleClick whiteText normalText" href="{{ url('dashboard')}}#alerts"><i class="fa fa-bell rightPadding"></i> Alerts <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="adlClick whiteText normalText" href="{{url('dashboard')}}#listings"><i class="fa fa-th rightPadding"></i>AD Listings <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="aucClick whiteText normalText" href="{{url('dashboard')}}#auction"><i class="fa fa-bar-chart rightPadding"></i> Auction Listings <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="comClick whiteText normalText" href="{{url('dashboard')}}#comments"><i class="fa fa-comments rightPadding"></i> Comments and Reviews <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="rewClick whiteText normalText" href="{{url('dashboard')}}#rewards"><i class="fa fa-link  rightPadding"></i> Rewards and Referrals <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="repClick whiteText normalText" href="{{url('dashboard')}}#reports"><i class="fa fa-area-chart rightPadding"></i> Reports <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
    <li><a class="invClick whiteText normalText" href="{{url('dashboard')}}#invoice"><i class="fa fa-list rightPadding"></i> Invoice <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="setClick whiteText normalText" href="{{url('dashboard')}}#settings"><i class="fa fa-cog rightPadding"></i> Settings <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
<!--   <li><a class="whiteText normalText" href="{{ url('ads/post') }}"><i class="fa fa-newspaper-o rightPadding" aria-hidden="true"></i> Post Ads <span class="pull-right"><i class="fa fa-angle-right"></i></a></li> -->

  @endif

  <div class="myAccordion" data-parent="">
  <li><div data-toggle="collapse" data-target="#categories" class="cursor"><i class="fa fa-tags rightPadding" aria-hidden="true"></i> Categories<span class="pull-right"><i class="fa fa-angle-right"></i></div>
      <ul id="categories" class="noMargin collapse">
        @foreach($categories_per as $row)
      <li class=""><a href="{{url('/').'/'.Session::get('default_location').'/'.$row->slug}}" class="grayText"><span class="rightMargin"><?php echo $row->icon; ?></span> {{$row->name}}<i class="fa fa-angle-right pull-right"></i></a></li>
  @endforeach
        <li class=""><a href="{{url('/').'/'.Session::get('default_location').'/all/categories'}}" class="whiteText"><i class="fa fa-bars rightMargin" aria-hidden="true"></i>All Categories <i class="fa fa-angle-right pull-right"></i></a></li>
      </ul>
    </li>
  <li><div data-toggle="collapse" data-target="#support" class="cursor"><i class="fa fa-life-ring rightPadding" aria-hidden="true"></i> Support <span class="pull-right"><i class="fa fa-angle-right"></i></div>
      <ul id="support" class="noMargin collapse">
        <li><a class="whiteText" id="contact-us" href="{{url('support').'#contactus'}}">Contact Us <i class="fa fa-angle-right pull-right"></i></a></li>
        <li><a class="whiteText" id="about-us" href="{{url('support').'#aboutus'}}">About us <i class="fa fa-angle-right pull-right"></i></a></li>
        <li><a class="whiteText" id="call-us" href="{{url('support').'#callus'}}">Call us <i class="fa fa-angle-right pull-right"></i></a></li>
        <li><a class="whiteText" id="terms-and-conditions" href="{{url('support').'#termAndconditions'}}">Terms and Conditions <i class="fa fa-angle-right pull-right"></i></a></li>
        <li><a class="whiteText" id="faq" href="{{url('support').'#faq'}}">FAQ's <i class="fa fa-angle-right pull-right"></i></a></li>
        <li><a class="whiteText" id="advertising" href="{{url('support').'#advertising'}}">Advertising <i class="fa fa-angle-right pull-right"></i></a></li>
      </ul>
    </li>
<!--     <li><div data-toggle="collapse" data-target="#language" class="cursor"><i class="fa fa-language rightPadding" aria-hidden="true"></i> Language <span class="pull-right"><i class="fa fa-angle-right"></i></div>
      <ul id="language" class="noMargin collapse">
        <li><a href="#">English</a></li>
        <li><a href="#">Chines</a></li>
        <li><a href="#">Arabic</a></li>
      </ul>
    </li> -->
      <li class="modalSidebar"><a class="whiteText normalText" href="#"  data-toggle="modal" data-target="#country-modal"><i class="fa fa-globe rightPadding" aria-hidden="true"></i> Country: {{strtoupper(Session::get('default_location'))}}</a></li>
  </div>
<!--   <li><a class="whiteText normalText" href="{{url('dashboard/watchlist')}}"><i class="fa fa-tags rightPadding" aria-hidden="true"></i> Categories <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  <li><a class="whiteText normalText" href="{{url('dashboard/watchlist')}}"><i class="fa fa-life-ring rightPadding" aria-hidden="true"></i> Support <span class="pull-right"><i class="fa fa-angle-right"></i></a></li> -->
<!--   <li><a class="whiteText normalText" href="{{url('dashboard/watchlist')}}"><i class="fa fa-language rightPadding" aria-hidden="true"></i> Language <span class="pull-right"><i class="fa fa-angle-right"></i></a></li> -->
  @if (Auth::guest()) 
  @else
  <li><a class="whiteText normalText" href="{{url('logout')}}"><i class="fa fa-sign-out rightPadding" aria-hidden="true"></i> Logout <span class="pull-right"><i class="fa fa-angle-right"></i></a></li>
  @endif
</div>
</div>
<!-- header start -->
<nav class="navbar navbar-default navbar-fixed-top bgWhite borderBottom">
  <div class="container noPadding">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 leftPadding noPaddingXs">
        <div class="minPadding hidden-md hidden-lg noleftPadding toggleSize">
          <button class="btn noBGButton menuToggle borderZero sidebar-btn" type="button"> <i class="fa fa-bars"></i> </button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-9 col-xs-5 leftPaddingMinXs">
            @if(Session::get('default_location') == 'ALL' or '')
             <a class="navbar-brand logo" href="{{ url('/') }}">
            @else
              <a class="navbar-brand logo" href="{{ url('/').'/'.Session::get('default_location') }}">
            @endif
               <img src="{{url('img').'/yakolaklogo.png'}}" class="img-responsive responsiveImgXS">
             </a>
        </div>
      </div>
      
      <div class="col-lg-9 col-md-9 col-sm-9 col-xs-7 pull-right">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" style="padding-top:3px">
            {!! Form::open(array('url' => strtolower(!is_null(Session::get('default_location')) ? Session::get('default_location') : 'all').'/q', 'role' => 'form', 'method' => 'GET', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
                <div class="input-group minPadding noleftPadding norightPadding text-right" >
                  <input type="text" class="form-control borderZero inputBox inputSearch" id="keyword" value="{{Session::get('keyword')}}" onkeyup="passKeyword()"  name="search" placeholder="Search here...">
                  <span class="input-group-btn">
                    <button class="btn blueButton borderZero hidden-xs" type="submit"><i class="fa fa-search"></i> Search</button>
                    <button class="btn blueButton borderZero hidden-sm hidden-md hidden-lg" type="submit" style="height:34px;"><i class="fa fa-search"></i></button>
                   @if(!Auth::check())
                        <button class="btn mediumText redButton borderZero postAds hidden-xs hidden-sm" type="button" class="#" data-toggle="modal" data-target="#modal-login">Post Ads</button>
                   @else
                      @if(Auth::check())
                          @if(Auth::user()->usertype_id == 3)
                           
                          @else
                             <a href="{{ url('post/ads') }}" class="hidden-sm hidden-xs"><button class="btn mediumText redButton borderZero postAds" {{Auth::user()->status == 2 || Auth::user()->status == 3 ? 'disabled':''}} type="button">Post Ads</button></a>
                               @endif
                          @endif
                   @endif
                  </span>
                </div>
             {!! Form::close() !!}   
        </div>
        <div class="collapse navbar-collapse col-lg-12 col-md-12 col-sm-12 col-xs-12 bottomMargin pull-right" id="div-status">
                <ul class="nav navbar-nav hidden-sm">
              @foreach($categories_per as $row)
                @if($categories_per->first() == $row)
                  <li><a href="{{url('/').'/'.strtolower(Session::get('default_location')).'/all-cities/buy-and-sell'}}">Buy</a></li>
                @endif
              @endforeach
            
              @if(Auth::check())
                @if(Auth::user()->status == 2 || Auth::user()->status == 3)
                @else
                    <li id="sell_btn_r"><a href="{{ url('post/ads') }}">Sell</a></li>
                @endif
              @else
                    <li><a href="#" data-toggle="modal" data-target="#modal-login">Sell</a></li>
              @endif

              <li id="bid_btn_r"><a href="{{url('/').'/'.strtolower(Session::get('default_location')).'/all-cities/auctions'}}">Bid</a></li>
                  <!-- <li class=""><a href="#" class="button-plans">Plans <i class="fa fa-angle-down leftMargin"></i></a></li> -->
                  <li class="cursor"><a class="button-categories">Categories <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <!-- <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Categories
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('category/list')}}">Buy and Sell</a></li>
                      <li><a href="#">Live Auctions</a></li>
                      <li><a href="#">Cars and Vehicles</a></li> 
                      <li><a href="#">Clothing</a></li>
                      <li><a href="#">Events</a></li>
                      <li><a href="#">Jobs</a></li>
                      <li><a href="#">Real Estate</a></li>
                      <li><a href="#">Services</a></li>
                      <li><a href="#">All Categories</a></li>
                    </ul>
                  </li> -->
                  <li class="cursor"><a class="button-support">Support <i class="fa fa-angle-down leftMargin"></i></a></li>
                  <!-- <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Support
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="{{url('support')}}" onclick="selectContactUs()">Contact us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAboutUs()">About us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectCallUs()">Call us</a></li>
                      <li><a href="{{url('support')}}" onclick="selectFaqs()">FAQ's</a></li>
                      <li><a href="{{url('support')}}" onclick="selectAdvertising()">Advertising</a></li>
                    </ul>
                  </li> -->
                  @if(!Auth::check())
                  <li class="borderleftLight"><a href="#" class="btn-login" id="btn-login">Login</a></li>
                  <li class="borderleftLight"><a href="#" class="#" id="btn-register">Register</a></li>
                  @else
                  <li class="borderleftLight"></li>
                  <li class=""><a href="#" data-toggle="modal" class="button-user">
                    <img src="{{ URL::route('uploads', array(), false).'/'.Auth::user()->photo}}" class="avatar" alt="user profile image" style="width: 25px; height: 25px;">
                     {{Auth::user()->username}} <i class="fa fa-angle-down leftMargin"></i></a>
                  </li>
                  <li class="borderleftLight"><a class="blueText envelopeClick" href="{{ url('dashboard')}}#messages"><span id="message-count-container">{{$message_count}}</span> <i class="{{$message_count == 0 ? 'fa fa-envelope-o':'fa fa-envelope'}}"></i></a></li>
                  <li class="dropdown">
                    <!-- <a href="#" class="dropdown-toggle blueText" data-toggle="dropdown"><span id="bidding-count-container">{{$notifications_global->count()}}</span> <i class="fa fa-flag"></i></a> -->
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a id="testt2" href="#" class="dropdown-toggle blueText noBg" data-toggle="dropdown"><span id="bidding-count-container">{{$notifications_global->count()}}</span> <i class="fa {{$notifications_global->count() == 0 ? 'fa-flag-o' : 'fa-flag'}}"></i></a>
          <ul class="dropdown-menu notiDropdown">
            <div class="panel panel-default removeBorder borderZero noMargin">
                        <div class="panel-heading normalText bgWhite minPadding" id="noti_list">{{$notifications_global->count() == 0 ? 'No new notifications':'Notifications'}} </div>
                        <div class="panel-body noPadding"><!-- scrollbar -->
                        <div class="notificationArea" id="style-1">
                          @if($notifications_global)
                              @foreach($notifications_global as $notificationsList)
                                <li class="borderbottomLight leftPadding rightPaddingB btnRead" data-href='{{$notificationsList->message}}' data-id="{{$notificationsList->id}}" style="cursor: pointer; min-height: 59px;">
                                  <div class="notificationBlock">
                                    <div class="notificationImage">
                                      <img src="{{URL::route('uploads', array(), false).'/'.(is_null($notificationsList->photo) ? 'default_image.png' : $notificationsList->photo)}}" class="avatar img-circle topMargin" alt="user profile image" style="width: 40px; height: 40px;">
                                    </div>
                                    <div class="notificationText">
                                      <span class="noti_text_height"><small style="word-break: break-word;" title="{{ (strlen(strip_tags($notificationsList->message)) > 60 ? strip_tags($notificationsList->message) : '') }}">{{ mb_strimwidth(strip_tags($notificationsList->message), 0, 60, '...')}}</small></span>

                                      <div class="lightgrayText smallText add_padding">
                                      <span class="pull-left">
                                        {{ mb_strimwidth($notificationsList->type, 0, 25, '...') }}
                                      </span>
                                      <span class="pull-right">
                                        {{ date("M j, Y g:i A", strtotime($notificationsList->created_at)) }}
                                      </span>
                                    </div>
                                    </div>
                                    
                                  </div>
                                </li>
                              @endforeach
                            @endif
                            </div>
                        </div>
                      </div>
          </ul>
        </li>
      </ul>
<!--                     <ul class="dropdown-menu notiUL  borderZero" >
                      <div class="panel panel-default removeBorder borderZero noMargin">
                        <div class="panel-heading normalText minPadding">Notifications</div>
                        <div class="panel-body noPadding scrollbar" id="style-1">
                          @if($notifications_global)
                              @foreach($notifications_global as $notificationsList)
                                <li class="borderbottomLight leftPadding rightPaddingB btnRead">
                                  <div class="notificationBlock">
                                    <div class="notificationImage">
                                      <img src="{{URL::route('uploads', array(), false).'/'.$notificationsList->photo}}" class="avatar img-circle topMargin" alt="user profile image" style="width: 40px; height: 40px;">
                                    </div>
                                    <div class="notificationText">
                                      <span><small><?php echo($notificationsList->message); ?></small></span>
                                      <div class="lightgrayText smallText topPadding"><?php echo($notificationsList->type); ?>
                                        <span class="pull-right">
                                          <?php echo date("F j, Y g:i A", strtotime($notificationsList->created_at)); ?></div>
                                        </span>
                                      </div>
                                      <div class="lightgrayText smallText">
                                    </div>
                                  </div>
                                </li>
                              @endforeach
                            @endif
                        </div>
                      </div>
                    </ul> -->
                  </li>
<!--                   <li><a href="#" class="blueText"><span id="bidding-count-container">{{$notifications_global->count()}}</span> <i class="fa fa-flag"></i></a></li> -->
                   @endif
                   <li class="borderleftLight cursor"><a type="button" class="" data-toggle="modal" data-target="#country-modal">@if(Session::get('default_location') == 'ALL' or '')All @else{{strtoupper(Session::get('default_location'))}} @endif</a></li>
                  <!-- <li class="borderleftLight dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">EN
                    <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">English</a></li>
                      <li><a href="#">Chinese</a></li>
                      <li><a href="#">Arabic</a></li>
                    </ul>
                  </li> -->
                </ul>
        </div>
      </div>

    </div>
  </div>
</nav>
@if(Auth::check())
  @if(Auth::user()->status == 2)
  <nav id="accountStatus" class="navbar navbar-default navbar-account navbar-fixed-top">  
    @if($verification_interval)
    <div class="container" id="notice_of_verification">
      <span class="whiteText" style="font-size:14px;">Please wait up to 24 hours for email confirmation. You can request another verification email after <span id="time_here" data-countdown_v="{{$verification_interval->created_at}}"></span>.</span>
    </div>
    @else
    <div class="container" id="notice_of_verification">
      <span id="resend_email" class="whiteText" style="font-size:14px;"> Account is not yet verified kindly click <a id="resend_mail" class="whiteText" href="#"><u>here</u></a> to resend the confirmation email. </span>
    </div>
    @endif
      {{-- <a type="button" class="close" id="close_notice">&times;</a> --}}
  </nav>
  @elseif(Auth::user()->status == 3)
  <nav id="accountStatus" class="navbar navbar-default navbar-account navbar-fixed-top">
    <div class="container">
      <span class="whiteText" style="font-size:14px;"> This account has been block due to violation of site policy you cannot use the sites features. If you think this is a mistake kindly <a class="whiteText" href="mailto:admin@yakolak.com"><u>contact us</u>.</a> </span>
      <!-- <a type="button" class="close" id="close_notice">&times;</a> -->
    </div>
  </nav>
  @endif
@endif
  @if (!(Auth::guest()))
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding userPanel">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 12px 10px 10px 10px;"><strong class="whiteText">Account Navigation:</strong></li>
        <li><a class="whiteText" href="{{url('dashboard')}}">Dashboard</a></li>
        <li><a class="whiteText" id="vanity-url" href="{{url(Auth::user()->usertype_id == 1 ? 'vendor' : 'company').'/'.Auth::user()->alias}}">Public Page</a></li>
        <li><a class="whiteText" id="messages-id" href="{{url('dashboard').'#messages'}}">Messages</a></li>
        <li><a class="whiteText" id="listings-id" href="{{url('dashboard').'#listings'}}">Listings</a></li>
        <li><a class="whiteText" id="auclistings-id" href="{{url('dashboard'.'#auction')}}">Bids</a></li>
        <li><a class="whiteText" id="watchlist-id" href="{{url('dashboard').'#watchlist'}}">Watchlist</a></li>
        <li><a class="whiteText" id="subscription-id" href="{{url('dashboard'.'#subscription')}}">Plans</a></li>
        <li><a class="whiteText" id="settings-id" href="{{url('dashboard').'#settings'}}">Settings</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('logout') }}">Logout</a></li>
      </ul>
      </div>
    </div>
@endif
<!--     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs plan">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 12px 10px 10px 10px;"><strong class="whiteText">Plans:</strong></li>
        <li><a class="whiteText" href="{{url('plans')}}">Bronze</a></li>
        <li><a class="whiteText" href="{{url('plans')}}">Silver</a></li>
        <li><a class="whiteText" href="{{url('plans')}}">Gold</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{url('plans')}}">All Plans</a></li>
      </ul>
      </div>
    </div> -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding quickSupport">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 12px 10px 10px 10px;"><strong class="whiteText">Quick Support:</strong></li>
        <li><a class="whiteText" id="contact-us" href="{{url('support').'#contactus'}}" onclick="$('#contact-us-link').click();">Contact Us</a></li>
        <li><a class="whiteText" id="about-us" href="{{url('support').'#aboutus'}}" onclick="$('#company-link').click();">About us</a></li>
        <li><a class="whiteText" id="call-us" href="{{url('support').'#callus'}}" onclick="$('#call-us-link').click();">Call us</a></li>
        <li><a class="whiteText" id="terms-and-conditions" href="{{url('support').'#termAndconditions'}}" onclick="$('#terms-and-conditions-link').click();">Terms and Conditions</a></li>
        <li><a class="whiteText" id="faq" href="{{url('support').'#faq'}}" onclick="$('#faq-link').click();">FAQ's</a></li>
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{ url('support') }}">Support Page</a></li>
      </ul>
      </div>
    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden-sm hidden-xs noPadding categoriesNav">
    <div class="container">
      <ul class="normalText nav navbar-nav navbar-left leftPaddingB">
        <li style="padding: 12px 10px 10px 10px;"><strong class="whiteText">Categories:</strong></li>
        <li><a class="whiteText" href="{{url('/').'/'.strtolower(Session::get('default_location')).'/'}}all-cities/buy-and-sell">Buy and Sell</a></li>
        <li><a class="whiteText" href="{{url('/').'/'.strtolower(Session::get('default_location')).'/'}}all-cities/auctions">Live Auction</a></li>
        @foreach($categories_per as $row)
            <li><a class="whiteText" href="{{url('/').'/'.strtolower(Session::get('default_location')).'/'.'all-cities'.'/'.$row->slug}}">{{$row->name}}</a></li>
        @endforeach
      </ul>
      <ul class="normalText nav navbar-nav navbar-left pull-right rightPaddingB">
        <li class="pull-right"><a class="whiteText" href="{{url('/').'/'.strtolower(Session::get('default_location')).'/all-categories'}}">All Categories</a></li>
      </ul>
      </div>
    </div>

<!-- <div id="mobilesidebarCategory" class="visible-sm visible-xs">
  <h4 class="whiteText leftMarginB">Categories</h4>
  <div class="form-group" id="accordion">
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" class="whiteText" data-toggle="collapse" data-parent="#accordion" href="#buyandsell">Buy and Sell</a>
      <div id="buyandsell" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#bidding">Bidding</a>
      <div id="bidding" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#carsandvehicles">Cars and Vehicles</a>
      <div id="carsandvehicles" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#clothings">Clothings</a>
      <div id="clothings" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#events">Events</a>
      <div id="events" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#jobs">Jobs</a>
      <div id="jobs" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#realestate">Real Estate</a>
      <div id="realestate" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
     <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#services">Services</a>
      <div id="services" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
    <div class="panel panel-default">
          <a class="whiteText leftMarginB" data-toggle="collapse" data-parent="#accordion" href="#all">All Categories</a>
      <div id="all" class="panel-collapse collapse normalText panelBody">
        <li><a>Item 1</a></li>
        <li><a>Item 2</a></li>
      </div>
    </div>
  </div>
  </div> -->
<!-- header end -->
<div id="page-content" class="">
<div id="wrapper" class="">
 <div id="content">
     @yield('content')
     @include('layout.form') 
 </div>
@if(Route::current()->getPath() == '{country_code}/all-categories' || Route::current()->getPath() == '{country_code}/{city}/{slug}/{q?}' || Route::current()->getPath() == 'post/ads' || Route::current()->getPath() == 'profile' || Route::current()->getPath() == 'search' || Route::current()->getPath() == 'plans' || Route::current()->getPath() == 'support' || Request::url() == url('dashboard') || Route::current()->getPath() == 'buy' || Route::current()->getPath() == 'bid' || Route::current()->getPath() == 'sell' || Route::current()->getPath() == 'support/contact-us' || Route::current()->getPath() == 'support/call-us' || Route::current()->getPath() == 'support/about-us' || Route::current()->getPath() == 'support/faqs' || Route::current()->getPath() == 'support/advertising' || Route::current()->getPath() == 'support/help-us' || Route::current()->getPath() == '{country_code}/{ad_type_slug}/{parent_category_slug}/{category_slug}/{slug}' ||  Route::current()->getPath() == 'dashboard/messages' || Route::current()->getPath() == 'dashboard/listing' || Route::current()->getPath() == 'dashboard/bids' || Route::current()->getPath() == 'dashboard/watchlist' || Route::current()->getPath() == 'dashboard/settings' || Route::current()->getPath() == 'user/vendor/{id}' || Route::current()->getPath() == 'ads/edit/{id}' || Route::current()->getPath() == '{country_code}/{slug}'|| Route::current()->getPath() == 'browse' || Route::current()->getPath() == '{country_code}/all/categories' || Route::current()->getPath() == 'error/access/denied' || Route::current()->getPath() == '{country_code}/{slug}/{q?}' || Route::current()->getPath() == '{country_code}/{cat}/{slug}/{q?}' || Route::current()->getPath() == '{country_code}/q' || Route::current()->getPath() == '{country_code}/{category}/q')

@else
<!-- main information start -->
  
<div class="container-fluid bgGray {{(Route::current()->getPath() == '{username}' || Route::current()->getPath() == '{country_code}/all/categories' || Route::current()->getPath() == '{country_code}/{slug}' || Route::current()->getPath() == 'custom/search') ? 'hide':''}} ">
  <div class="container noPadding" id="stats_block">
    <div class="container-fluid col-lg-12 col-md-12 noPadding">

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom" style="min-height: 312px;">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Yakolak Statistic</strong></span>
        </div>
        <div class="panel-body lineHeight">
          <p>Total Ads</p>
          <h4 class="blueText"><b>{{$total_ads}}</b></h4>
          <p>Total Bids</p>
          <h4 class="blueText"><b>{{$total_auctions}}</b></h4>
          <p>Total Vendors</p>
          <h4 class="blueText"><b>{{$total_vendors}}</b></h4>
          <p>Total Companies</p>
          <h4 class="blueText"><b>{{$total_companies}}</b></h4>
        </div>
      </div>  
      </div>

      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Top Members</strong></span>
        </div>
        <div class="panel-body lineHeight">
        @foreach($top_mem_per as $row)
          @if($row->total_listings > 0)
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
            <div class="commenterImage"><a href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"><img src="{{URL::route('uploads', array(), false).'/'.$row->photo}}" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"></a>
            </div>
            <div class="statisticcommentText">
            <div class="panelTitleB bottomPadding"><a href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}" class="blueText">{{$row->name}}</a></div><p class="normalText" style="margin-left:46px;">{{$row->ad_listings}} Ads & {{$row->auction_listings}} Bids Listings</p>
            </div>
            </div>
            </div>
          @endif
        @endforeach
        </div>
      </div>
      </div>

      <div class="clearfix visible-xs-block visible-sm-block"></div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Latest Comments</strong></span>
        </div>
        <div class="panel-body lineHeight">

          @foreach($latest_comments_per as $row)
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <a href="{{ url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}"><img src="{{ URL::route('uploads', array(), false).'/'.$row->photo }}" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"></a>
              </div>
              <div class="statisticcommentText">
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="panelTitleB bottomPadding ellipsis">{{ $row->title }}</div></a>
                <p class="normalText ellipsis"><a class="" style="color:#333;" href="{{ url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}">by: <span class="">{{ $row->name }}</span></a></p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
      <div class="panel panel-default removeBorder borderZero borderBottom minHeight">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Latest Reviews</strong></span>
        </div>
        <div class="panel-body lineHeight">
        @foreach($latest_reviews_per as $row)
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding"><a class="blueText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><p class="ellipsis" style="margin-bottom:-7px;padding:0;">{{ $row->title }}</p></a></div>
            <p class="" style="margin-bottom:-4px;padding:0;">
              {{-- @if($row->rate == 1.0)
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
              @elseif($row->rate == 2.0)
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
              @elseif($row->rate == 3.0)
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><i class="fa fa-star-o"></i><i class="fa fa-star-o"></i>
              @elseif($row->rate == 4.0)
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><i class="fa fa-star-o"></i>
              @elseif($row->rate == 5.0)
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span>
              @endif --}}
              {!! app('App\Http\Controllers\FrontEndUserController')->create_stars($row->rate) !!}
            </p>
          </div>
          <div class="col-xs-12 noPadding" style="display:inline-block;">
            <div class="normalText"><a style="color:#333;" href="{{ url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}"><p class="ellipsis" style="margin-bottom:2px !important; margin:0;padding:0;">Reviewed by: {{ $row->name }}</p></a></div>
          </div>
        </div>
        @endforeach

        <!-- 
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding">Listing Name
              <span class="blueText pull-right rightMargin hidden-xs">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <i class="fa fa-star-half-empty"></i>
              </span>
            </div>
            <div class="blueText bottomPadding rightMargin visible-xs">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <i class="fa fa-star-half-empty"></i>
              </div>
          </div>
          <div class="col-xs-12 noPadding">
            <div class="normalText">Fast transaction will do it again.</div>
            <div class="smallText lightgrayText">Reviewed by: Username</div>
          </div>
        </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="col-xs-12 noPadding">
            <div class="panelTitleB bottomPadding">Listing Name
              <span class="blueText pull-right rightMargin hidden-xs">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <i class="fa fa-star-half-empty"></i>
              </span>
            </div>
            <div class="blueText bottomPadding rightMargin visible-xs">
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                <i class="fa fa-star-half-empty"></i>
              </div>
          </div>
          <div class="col-xs-12 noPadding">
            <div class="normalText">Fast transaction will do it again.</div>
            <div class="smallText lightgrayText">Reviewed by: Username</div>
          </div>
        </div>
        </div>
      </div> -->
      </div>

    </div>
  </div>
</div>
</div>
<!-- main information end -->
@endif

              
<!-- main footer start -->
<section id="mainFooter">
<div class="container-fluid bgWhite borderBottom ">
  <div class="container noPadding ">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Customer Service</strong></span>
        </div>
        <div class="panel-body normalText">
          <p><a href="{{url('support').'#helpcenter'}}" class="grayText clickH">Help Center</a></p>
          <p><a href="{{url('support').'#contactus'}}" class="grayText clickC">Contact Us</a></p>
          <p><a href="{{url('support').'#termAndconditions'}}" class="grayText clickT">Terms and Conditions</a></p>
          <p><a href="{{url('support').'#faq'}}" class="grayText clickF">Frequently Asked Questions</a></p>
          <p>&nbsp;</p>
          <p><a href="https://fb.me" target="_blank" class="grayText">Facebook Page</a></p>
          <p><a href="https://twitter.com" target="_blank" class="grayText">Twitter</a></p>
          <p><a href="https://google.com/+" target="_blank" class="grayText">Google Plus</a></p>
        </div>
      </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>About Yakolak</strong></span>
        </div>
        <div class="panel-body normalText">
          <p><a class="grayText clickCI" href="{{ url('support').'#aboutus' }}">Company Information</a></p>
          <p><a class="grayText clickMAV" href="{{ url('support').'#missionandvision' }}">Mission and Vision</a></p>
          <p>&nbsp;</p>
<!--           <p><a href="{{url('support').'#advertising'}}" class="grayText clickAdv">Advsertising</a></p> -->
          <p><a class="grayText clickSM" href="{{ url('support').'#sitemap' }}">Sitemap</a></p>
        </div>
      </div>
      </div>

      <div class="clearfix visible-xs-block visible-sm-block"></div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Sell on Yakolak</strong></span>
        </div>
        <div class="panel-body normalText">
          @if(!Auth::check())
          <p><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modal-register" class="grayText">Create an account</a></p>
          @endif
          <p><a href="{{url('post/ads')}}" class="grayText">Publish Ads</a></p>
          @if(!Auth::check())
          <button class="btn redButton borderZero noMargin fullSize" type="button" data-toggle="modal" data-target="#modal-register">LIST AD FOR FREE</button>
          @else
          <a role="button" class="btn redButton borderZero noMargin fullSize" type="button" href="{{url('post/ads')}}">LIST AD FOR FREE</a>
          @endif
          <p>&nbsp;</p>
          @if(!Auth::check())
          <p><a href="#" data-toggle="modal" data-target="#modal-login" class="grayText">Account Upgrade</a></p>
          @else
          <p><a href="{{url('dashboard/').'#subscription'}}" class="grayText clickAccUpg">Account Upgrade</a></p>
          @endif
        </div>
      </div>
      </div>
      
      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 sidePadding">
      <div class="panel panel-default removeBorder borderZero nobottomMargin">
        <div class="panel-heading panelTitleBar">
          <span class="mediumText grayText"><strong>Subscribe to our newsletter</strong></span>
        </div>
        <div class="panel-body normalText">
          <form id="newsletter_subs">
            <input id="sub_email" type="email" class="form-control borderZero inputBox bottomMargin" placeholder="Email Address" required>
            <input id="sub_first" type="text" class="form-control borderZero inputBox bottomMargin" placeholder="First Name" required>
            <input id="sub_last" type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Last Name" required>
            <button id="sub_submit" class="btn blueButton borderZero noMargin fullSize" type="submit">SUBSCRIBE</button>
          </form>
        </div>
      </div>
      </div>

    </div>
  </div>
</div>
</section>

<div class="hidden-xs hidden-sm hide">
@if(Auth::check())
<ul class="list-inline chatArea" id="chatbox-container">
    @for($x=1;$x<=3;$x++)
      <li id="chatPanel" class="col-sm-4 pop chatpanelBottom">
      <div class="panel panel-default noMargin borderZero">
        <div class="panel-heading clickPop text-capitalize cursor">Username <button type="button" class="close" data-target="#chatPanel" data-dismiss="alert">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button></div>
            <div class="panel-body chatboxBody">
              <div id="chatbox"></div>
            </div>
            <div class="panel-footer bgWhite">
            <div class="input-group">
                        <input id="btn-input" type="text" class="form-control input-sm borderZero" placeholder="Type your message here...">
                        <span class="input-group-btn">
                            <button class="btn blueButton borderZero btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>
           <!--  <div class="row">
              <div class="textArea" contentEditable="true"></div>
              </div> -->
              </div>
            </div>
      </li>
@endfor
</ul>
@endif
</div>
<!-- @if(Auth::check())
<ul class="list-inline chatArea" id="chatbox-container">
    @for($x=1;$x<=3;$x++)
      <li id="chatPanel" class="pop chatpanelBottom">
      <div class="panel panel-default noMargin borderZero">
        <div class="panel-heading clickPop text-capitalize">Username <button type="button" class="close" data-target="#chatPanel" data-dismiss="alert">
        <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
        </button></div>
            <div class="panel-body chatboxBody">
              <div id="chatbox"></div>
            </div>
            <div class="panel-footer bgWhite">
            <div class="row">
              <div class="textArea" contentEditable="true"></div>
              </div>
              </div>
            </div>
      </li>
@endfor
</ul>
@endif -->
      <!--  <li id="chatPanel1">
   <div class="panel panel-default noMargin borderZero pop popChat">
    <div class="panel-heading clickPop text-capitalize">Username <button type="button" class="close" data-target="#chatPanel" data-dismiss="alert">
    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
    </button></div>
        <div class="panel-body chatboxBody">
          <div id="chatbox"></div>
        </div>
        <div class="panel-footer bgWhite">
        <div class="row">
          <textarea class="form-control borderZero" rows="1" id="comment"></textarea>
          </div>
          </div>
        </div>
  </li> -->
<!--   <li>
  <div class="panel panel-default noMargin">
    <div class="panel-heading">USERNAME</div>
    <div class="panel-body chatboxBody">
      <div id="chatbox"></div>
    </div>
    <div class="panel-footer">
      <textarea class="form-control" rows="1" id="comment"></textarea>
      <button type="button" class="btn blueButton borderZero">Submit</button>
    </div>
  </div>
  </li>
  <li>
  <div class="panel panel-default noMargin">
    <div class="panel-heading">USERNAME</div>
    <div class="panel-body chatboxBody">
      <div id="chatbox"></div>
    </div>
    <div class="panel-footer">
      <textarea class="form-control" rows="1" id="comment"></textarea>
      <button type="button" class="btn blueButton borderZero">Submit</button>
    </div>
  </div>
  </li> -->

<!--   <div class="chatBox">
    <div class="panel panel-default removeBorder borderZero">
      <div class="panel-heading chatboxHeader cursor openChat">Username
        <span class="pull-right">
            <i class="fa fa-power-off"></i>
        </span>
      </div>
    <div class="panel-body chatboxBody">
      <div id="chatbox"></div>
    </div>
    <div class="panel-footer chatboxTextarea bgWhite">
      <div class="col-sm-12 noPadding">
        <div class="row">
        <textarea name="textArea" id="textArea" class="form-control borderZero removeBorder" rows="1" id="message" onkeyup="textAreaAdjust(this)" style="overflow:hidden"></textarea>
        </div>
      </div>
    </div>
    </div>
  </div> -->


<div class="bgGray container-fluid">
<div class="noPadding container">
<div class="col-lg-12 ">
  <div class="col-lg-12 minPadding noleftPadding lightgrayText">© 2016 Yakolak.com. All rights reserved.</div>
</div>
</div>
</div>
</div>

<!-- main footer end -->

<!-- search start -->

<!-- sea    
        
      </div>
    </div>
  </div>
</div>
<!-- account end -->


<!-- NAVIGATOR -->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
   <!-- jQuery -->
{!! HTML::script('/js/jquery.js') !!}
{!! HTML::script('/js/form.js') !!}
{!! HTML::script('/js/tooltip.js') !!}
{!! HTML::script('/plugins/js/jquery.rateyo.min.js') !!}
{!! HTML::script('/plugins/js/loader.js') !!}
{!! HTML::script('/plugins/js/morris.min.js') !!}
{!! HTML::script('/plugins/js/moment.min.js') !!}
{!! HTML::script('/plugins/js/raphael-min.js') !!}
{!! HTML::script('/plugins/js/timeago.js') !!}
{!! HTML::script('/plugins/js/toastr.min.js') !!}
{!! HTML::script('/plugins/js/fileinput.min.js') !!}
{!! HTML::script('/plugins/js/jquery.countdown.js') !!}
{!! HTML::script('/plugins/js/jquery.countdown.min.js') !!}
{!! HTML::script('/plugins/js/select2.min.js') !!}
{!! HTML::script('/plugins/components/moment/min/moment.min.js') !!}
{!! HTML::script('/plugins/components/datetimepicker/build/js/bootstrap-datetimepicker.min.js') !!}
{!! HTML::script('/plugins/readmore.js') !!}
<!-- {!! HTML::script('/plugins/arrive.min.js') !!} -->
{!! HTML::script('js/plugins/bootstrap-multiselect.js') !!}

    <!-- Bootstrap -->
{!! HTML::script('/js/bootstrap.min.js') !!}
    <!-- Editor -->
{!! HTML::script('/plugins/js/summernote.js') !!}

{!! HTML::script('/plugins/js/purl.js') !!}
  
 @yield('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.actual/1.0.19/jquery.actual.min.js"></script>
<script src="http://html2canvas.hertzen.com/dist/html2canvas.min.js"></script>
<script type="text/javascript">
 // $_token = '{{ csrf_token() }}';

    //   var CaptchaCallback = function(){
    //     grecaptcha.render('RecaptchaField1', {'sitekey' : '6LeMqBYTAAAAAEOhsIBOdqDtkxT67Gu192cUZdp4'});
    //     grecaptcha.render('RecaptchaField2', {'sitekey' : '6LeMqBYTAAAAAEOhsIBOdqDtkxT67Gu192cUZdp4'});
    // };

    var CaptchaCallback = function() {
        grecaptcha.render('login_captcha', {'sitekey' : '{{Session::get('siteKey')}}'});
        grecaptcha.render('register_captcha', {'sitekey' : '{{Session::get('siteKey')}}'});
    };


$(document).ready(function() {

  var branch = $("#branchContainer").html();
    // $(".btn-add-branch").click(function() {
       $(".btn-add-branch").on("click", function() {
        $("#branchContainer").append(branch);
        var country = $("#branchContainer").find('section:first').find("#client-company_country").val();
        var city = $("#branchContainer").find('section:first').find("#client-company_city").html();

        $("#branchContainer").find('section:last').find("#client-company_country").val(country);
        $("#branchContainer").find('section:last').find("#client-company_city").html(city);
        $("#branchContainer").find('section:last').find(".btn-add-branch").removeClass("btn-add-branch").addClass("btn-del-branch").html("<i class='fa fa-minus redText'></i>");
        //$("#branchContainer").find('section:last').find(".btn-add-branch").html("<button type='button' value='' class='hide-view btn-add-branch pull-right borderZero inputBox'></button>");
     });
 $("#chatbox-container").on("click", ".btnChangeMessageStatus", function(){
         var id = $(this).data('id');
         console.log(id);
         $_token = "{{ csrf_token() }}";
          $.post("{{ url('user/send/message/vendor/status') }}", { id: id, _token: $_token }, function(response) {
              $("#message-count-container").text(response.updated_count);
              
       }, 'json');
  });
 $("#client-country").on("change", function() {
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
  $(".modal").on("change", ".company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        console.log("dsdsd");
        var city = $(this).parent().parent().parent().find("#client-company_city");
        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
 $(".modal").on("click", ".btn-del-branch", function() { 
    $(this).parent().parent().remove();
 });

  $(".sidebar-btn").click(function() {
    $("#mobilesidebarAccount").toggleClass("active");
    $("#wrapper").toggleClass("active");
    $("#page-content").toggleClass("active");
    // $("#content").toggleClass("active");
    $(".navbar-fixed-top").toggleClass("active");
    // // console.log($(window).width());
    // if ($(".navbar-fixed-top").hasClass("active")) {
    // }
  });
   $(".modalSidebar").click(function() {
    $("#page-content").toggleClass("active");
  });

  $("#page-content").click(function() {
  // $("body").on('click', '#page-content', function() {
    // console.log('asdasd');
    if ($("#mobilesidebarAccount").hasClass("active")) {
      $(".sidebar-btn").click();
    }
  });

  $(".navbar-fixed-top").attr('style', 'max-width: ' + $(window).width() + 'px;');

  $(window).resize(function() {
    $(".navbar-fixed-top").attr('style', 'max-width: ' + $(window).width() + 'px;');
  });

  $(".btnRead").on('click', function(event) {
    var btn = $(this);
    var id = btn.data('id');
  // alert(id);

  console.log(id + 'seen');

  $.post("{{ url('user/read/notifications/vendor/status') }}", { id: id, _token: $_token }, function(response) {

    $("#bidding-count-container").html(response.updated_noti_count);
    
    var li_count = response.updated_noti_count;
    if(li_count <= 0){
      $('#noti_list').text('No new notifications');
      $('#bidding-count-container').next('i').attr('class', 'fa fa-flag-o');
    }
    btn.remove();

    var redirect_to = $(btn.data('href')).attr('href');
    window.location.replace(redirect_to);

  }, 'json');

  });

  $("#button-reply").on('click', function(event) {
    var html = $(this).parent().parent().find('#reply-pane:first').html();
    $(".comments-container").find('#reply-pane:last').find("#ad-replies-containter").html(html);
  });

  $(".modalSidebar").click(function() {
    $("#mobilesidebarAccount").toggleClass("active");
    $("#wrapper").toggleClass("active");
    $(".navbar-fixed-top").toggleClass("active");
  });

  $('#bidsCarousel').carousel({
    pause: true,
    interval: false
  });

  $('#adsCarousel').carousel({
    pause: true,
    interval: false
  });

  $('.carousel').carousel({
    pause: true,
    interval: false
  });

  $('[data-toggle=offcanvas]').click(function() {
    $('#sidebar-wrapper').toggleClass('active');
  });
   $(function () {

        $('.date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
        $('.start-date').datetimepicker({
            locale: 'en',
            format: 'MM/DD/YYYY'
        });
  });
// $('#row-start_date').datetimepicker()
//     .on("input change", function (e) {
// });
$(".button-plans").click(function(){
    $(".plan").toggleClass("active");
    $(".userPanel").removeClass("active");
    $(".categoriesNav").removeClass("active");
    $(".quickSupport").removeClass("active");
    $(".availableRegion").removeClass("active");
});

// $(".button-user").click(function() {
// if ($("div").hasClass("accountVerified")) {
// $(".accountVerified").addClass("hide");
// $(".userPanel").toggleClass("active");
// } else if ($("div").not("active")){
// $(".accountVerified").removeClass("hide");
// }
// });

$(".button-user").click(function(){
    $(".userPanel").toggleClass("active");
    $(".categoriesNav").removeClass("active");
    $(".plan").removeClass("active");
    $(".quickSupport").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-categories").click(function(){
    $(".categoriesNav").toggleClass("active");
    $(".plan").removeClass("active")
    $(".quickSupport").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-support").click(function(){
    $(".quickSupport").toggleClass("active");
    $(".plan").removeClass("active")
    $(".categoriesNav").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".availableRegion").removeClass("active");
});
$(".button-region").click(function(){
    $(".availableRegion").toggleClass("active");
    $(".plan").removeClass("active")
    $(".quickSupport").removeClass("active");
    $(".userPanel").removeClass("active");
    $(".categoriesNav").removeClass("active");   
});

$("#modal-register").on("change", "#client-usertype", function() { 
      $.each($("input[name='usertype']:checked"), function(){            
           var x =  $("input[name='usertype']:checked").val();
            if($(this).val()=="2"){
               $(".common-name-pane").text("Company Name");
            }else{
               $(".common-name-pane").text("Individual's Name");
            }
      });
})
$(".btn-nav").click(function(){
    $("#navULxs").toggleClass("active");
    $("#loginULxs").removeClass("active");
});
$(".btn-prof").click(function(){
    $("#loginULxs").toggleClass("active");
    $("#navULxs").removeClass("active");
});
$("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $(".button-pane").addClass('hide');

});
$("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide");
        $(".photo-pane").removeClass("hide");
        $(".button-pane").removeClass('hide');
});
$("#btn-register").on("click", function() {
     $("#modal-register").find(".alert").remove();
     console.log( $("#modal-register").html());  
     $("#modal-register").modal("show");
});
$("#btn-login").on("click", function() {
     $("#modal-login").find(".alert").remove();
     $("#modal-login").modal("show");
});
@if(Session::has('notice'))
  $("#modal-login").modal("show");
@endif
@if(Session::has('referral_code'))
   $("#modal-register").modal("show");
@endif

// $("#modal-login").on("hidden.bs.modal", function (event) {
//   $("", function (event) {
//     $("#modal-login .alert").remove();

//     });
// });
// $("#modal-ads-need-login").on("hidden.bs.modal", function (event) {
//  $("#modal-login .alert").remove();
// });

// $("#modal-register").on("hidden.bs.modal", function (event) {
//   $("#modal-register .alert").remove();
//   $("#modal-register").find("form")[0].reset();
// });
 
  $('#client-gender-male').change(function() {
      if($(this).is(":checked")) {
        $("#client-gender").val("m");
      }
  });
  $('#client-gender-female').change(function() {
      if($(this).is(":checked")) {
         $("#client-gender").val("f");
      }
  });
 
  @if(session('status'))
      status("Error", "Account Validated", 'alert-success');
  @endif
  @if(session('error'))
      status("Error", "Account is already validated", 'alert-danger');
  @endif
  
  @if(session('unsub_success'))
      status("Error", "Unsubscribed", 'alert-success');
  @endif
  @if(session('unsub_failed'))
      status("Error", "No existing subscription", 'alert-danger');
  @endif
  
  @if(session('status_referral'))
      status("Error", "Invitation Sent", 'alert-success');
  @endif
  @if(session('error_referral'))
      status("Error", "Invitation Failed", 'alert-danger');
  @endif
  @if(Auth::check())

         @if(session('payment_success') == 1)
           status("Error", "Your plan has been upgraded", 'alert-success');
         @endif
         @if(session('payment_failed') == 1)
           status("Error", "Insufficient points", 'alert-danger');
         @endif
         @if(session('payment_success_for_bid_points') == 1)
            status("Error", "Purchased Successful", 'alert-success');
         @endif
         @if(session('payment_success_for_banner_plan') == 1)
            status("Error", "Banner Purchased Successful", 'alert-success');
         @endif
         @if(session('payment_success_for_feature_ad') == 1)
           status("Error", "Feature Ad Request Sent", 'alert-success');
         @endif
      

  @endif
         
   // @if(Auth::check())
   //       @if(session('payment_success') == 1)
   //         status("Error", "Your plan has been upgraded", 'alert-success');
   //       @endif
   //       @if(Auth::user()->user_plan_types_id == "1")
   //       $('.planType').addClass("bgGray");
   //       @elseif(Auth::user()->user_plan_types_id == "2" || Auth::user()->user_plan_types_id == "6")
   //       $('.planType').addClass("bgBronze");
   //       $('.planType').removeClass("bgGray");
   //       $('.planType').removeClass("bgSilver");
   //       $('.planType').removeClass("bgGold");
   //       @elseif(Auth::user()->user_plan_types_id == "3" || Auth::user()->user_plan_types_id == "7")
   //       $('.planType').addClass("bgSilver");
   //       $('.planType').removeClass("bgGray");
   //       $('.planType').removeClass("bgBronze");
   //       $('.planType').removeClass("bgGold");
   //       @elseif(Auth::user()->user_plan_types_id == "4" || Auth::user()->user_plan_types_id == "8")
   //       $('.planType').addClass("bgGold");
   //       $('.planType').removeClass("bgGray");
   //       $('.planType').removeClass("bgSilver");
   //       $('.planType').removeClass("bgBronze");
   //       @endif
   // @endif

  $("#getting-started").countdown("2017/01/01", function(event) {
     $(this).text(
       event.strftime('D: %D - H: %H - M:%M - S:%S')
     );
  });

          //readmore
  $('#info').readmore({
      moreLink: '<a href="#">Usage, examples, and options</a>',
      collapsedHeight: 600,
      afterToggle: function(trigger, element, expanded) {
        if(! expanded) { // The "Close" link was clicked
          $('html, body').animate({scrollTop: element.offset().top}, {duration: 600});
        }
      }
    });
    $('article').readmore({speed: 500});

     $('#example-filterBehavior').multiselect({
            enableFiltering: true,
            filterBehavior: 'value'
     });

    // $("#modal-register").on("click", ".btn-close-register", function() { 
    //  $("#modal-register").addClass('hide');
    //  $(".modal-backdrop").addClass('hide');
    // });

//   $(function(){
// $("#addClass").click(function () {
//           $('#qnimate').addClass('popup-box-on');
//             });
          
//             $("#removeClass").click(function () {
//           $('#qnimate').removeClass('popup-box-on');
//             });
//   })

  $(function(){
      $(".openChat").click(function () {
          $('.chatBox').toggleClass('open');
          $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle')
      });
        
  })

  $(function(){
    $("#chatbox-container").on("click", ".clickPop", function() { 
          $(this).closest('li').toggleClass('chatpanelBottom chatpanelUp')
          // $(".pop").toggleClass('chatpanelBottom chatpanelUp')
       });
          
  })

  
});
/* Footer Tab Clicker Start */
  $(".clickC").click(function() {
      $("#contact-us-link").click();
      console.log("Contact Us clicked");
  });
   $(".clickT").click(function() {
     $("#terms-and-conditions-link").click();
     console.log("Terms and Conditions clicked");
  });
  $(".clickF").click(function() {
     $("#faq-link").click();
     console.log("FAQ clicked");
  });
  $(".clickH").click(function() {
     $("#helpcenter-link").click();
     console.log("Help Center clicked")
  });
  $(".envelopeClick").click(function() {
     $("#messages-link").click();
     console.log("Messages clicked");
  });
  $(".clickAccUpg").click(function() {
     $("#subscription-link").click();
     console.log("Subscription clicked");
  });
  $(".clickBid").click(function() {
     $("#auction-link").click();
     console.log("Auction clicked");
  });
  $(".clickCI").click(function() {
     $("#company-link").click();
     console.log("CI clicked.");
  });
  $(".clickSM").click(function() {
     $("#sitemap-link").click();
     console.log("SM clicked.");
  });
  $(".clickMAV").click(function() {
     $("#missionandvision-link").click();
     console.log("MAV clicked.");
  });
  $(".clickAdv").click(function() {
     $("#advertise-link").click();
     console.log("Advertising clicked.");
  });
/* Footer Tab Clicker END */

  $(document).ready(function() {
  $(".modal").on("hidden.bs.modal", function() {
    $(".modal-body1").html("Where did he go?!?!?!");
  });
});

  var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].onclick = function(){
        this.classList.toggle("active");
        this.nextElementSibling.classList.toggle("show");
    }
}
function textAreaAdjust(o) {
    o.style.height = "1px";
    o.style.height = (15+o.scrollHeight)+"px";
}
$(function() {
    $("#textArea").keypress(function (e) {
        if(e.which == 13) {
            //submit form via ajax, this is not JS but server side scripting so not showing here
            $("#chatbox").append($(this).val() + "<br/>");
            $(this).val("");
            e.preventDefault();
        }
    });
});
$('#textArea').bind('keypress', function (event) {
    var regex = new RegExp("^[a-zA-Z0-9]+$");
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
    
});
// @if(Auth::check())
//   $(document).ready(function(){
//   function auto_load(){
          
//           $.ajax({
//             url: "{{ url('user/message/notification/refresh')}}",
//             data: { _token: '{{ csrf_token() }}' },
//             type: "POST",
//             complete: function(){
//               auto_load();
//               $("#chatbox-container").stop();
             
//             },
//             success: function(response) {

//               if (!response.logout) {
//                 $("#message-count-container").html(response.count);
//                 $("#chatbox-container").html(response.chat_box);
//                 $("#chatbox-container").stop();
//               } else {
//                   location.reload();

//               }
//               }
            
//           });

//   }
//     auto_load(); //Call auto_load() function when DOM is Ready
// });


@endif

$(document).ready(function(){
  $('#modal-save_form').submit(function(){
    $('#keyword:text').each(
    function(i,el) {
        if (!el.value || el.value == '') {
            el.placeholder = 'Fill this field';
            /* or:
            el.placeholder = $('label[for=' + el.id + ']').text();
            */
        }
    });
  });
});

$('div.panelTitleB.bottomPadding').addClass('ellipsis');
$('a.location-switch').click(function(){
  var data_code = $(this).data('code');
  var destination_url = "{{url('/')}}";
  $.ajax({
    type: "POST",
    url: "{{url('locale/change/location')}}",
    data: { data_code: data_code, _token: "{{csrf_token()}}" },
    success: function(response){
      if(response.code == 1)
      {
        if(response.code == '')
        {
          $(location).attr('href',destination_url+'/');
        }
        else
        {
          $(location).attr('href',destination_url+'/'+response.country_code);
        }
      }
      else
      {
        $(location).attr('href',destination_url);
      }
    },
  });
});

$('#close_notice').click(function(){
  $('#accountStatus').remove();
  var _token = "{{csrf_token()}}";
  $.ajax({
      url: "{{url('verify/close')}}",
      type: "post",
      data: {_token: _token},
      success:function(response){

      }
  });
});

@if(Auth::check())
$(document).on('click', '#resend_email',function(){
    $('#notice_of_verification').html('<span class="whiteText" style="font-size:14px;"> Account is not yet verified kindly click <a id="resend_mail" class="whiteText"><u>here</u></a> (you can only send one every 24 hours) to resend the confirmation email. </span>');
    var _token = "{{csrf_token()}}";
    var id = "{{Auth::user()->id}}";
    $.ajax({
      url: "{{url('resend/email/verification')}}",
      type: "post",
      data: {id: id, _token: _token},
      success:function(response){
        if(response == 1){
          status('Success','Email verification sent.','alert-success');
          setTimeout(function()
          {
            location.reload();
          },1500);
        }
      },
    });
   });

if($("#accountStatus").length)
{
  $('#content').addClass('marginBlock');
}
else
{
  $('#content').removeClass('marginBlock');
}


var count_v = $('#time_here').data('countdown_v');
$('#time_here').countdown(count_v)
.on('update.countdown', function(event) {
    // if(event.offset.totalDays > 0)
    // {
    //   var format = '%-D days';
    // }
    // else
    // {
      var format = '%-H hours %-M minutes %-S seconds';
    // }
  $(this).html(event.strftime(format));
})
.on('finish.countdown', function(event) {
  $('#notice_of_verification').html('<span id="resend_email" class="whiteText" style="font-size:14px;"> Account is not yet verified kindly click <a id="resend_mail" class="whiteText" href="#"><u>here</u></a> to resend the confirmation email. </span>');
  var _token = "{{csrf_token()}}";
  var id = "{{Auth::user()->id}}";
  $.ajax({
      url: "{{url('clear/email/verification')}}",
      type: "post",
      data: {id: id, _token: _token},
      success:function(response){
        console.log('vclear');
      },
    });
});
@endif
$(document).ready(function(){
  $('#testt2').click(function(){
    $('.noti_text_height').each(function(){
      var elem = $(this);
      var height = elem.actual('height');
      if(height <= 19){
        elem.next('div.add_padding').css('padding-top', '19px');
      }
    });
  });
});

$('#newsletter_subs').submit(function(e){
  e.preventDefault();
  var email = $('#sub_email').val(),
      firstname = $('#sub_first').val(),
      lastname = $('#sub_last').val(),
      _token = "{{csrf_token()}}";
  
  $.ajax({
    url: "{{url('subscribe/email')}}",
    type: "post",
    data: {email: email, firstname: firstname, lastname: lastname, _token: _token},
    success: function(response) {
      console.log(response);
      status('Success', 'Subscribed', 'alert-success');
    },
  });
  
});


</script>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
</div>
</body>
</html>