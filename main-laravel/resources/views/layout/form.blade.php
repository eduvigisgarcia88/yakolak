<!-- Modal -->
<div id="country-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content borderZero removeBorder">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title panelTitle">Please select your country / region</h4>
      </div>
      <div class="modal-body">
        <div class="col-xs-12 noPadding bottomPaddingC text-justify">
          <ul class="list-unstyled lineHeightC split-list noPadding">
          <div class="col-md-3 col-sm-6">
          @if(isset($global_country_lists[0]))
          @foreach($global_country_lists[0] as $row => $name)
            @if($name == 'All')
              <li><a style="cursor:pointer;" data-code="{{strtolower($row)}}" class="grayText location-switch">{{$name}}</a></li>
            @endif
          @endforeach
          @foreach($global_country_lists[0] as $row => $name)
            @if($name != 'All')
              <li><a style="cursor:pointer;" data-code="{{strtolower($row)}}" class="grayText location-switch">{{$name}}</a></li>
            @endif
          @endforeach
          @endif
          </div>
          <div class="col-md-3 col-sm-6">
          @if(isset($global_country_lists[1]))
          @foreach($global_country_lists[1] as $row => $name)
            <li><a style="cursor:pointer;" data-code="{{strtolower($row)}}" class="grayText location-switch">{{$name}}</a></li>
          @endforeach
          @endif
          </div>
          <div class="col-md-3 col-sm-6">
          @if(isset($global_country_lists[2]))
          @foreach($global_country_lists[2] as $row => $name)
            <li><a style="cursor:pointer;" data-code="{{strtolower($row)}}" class="grayText location-switch">{{$name}}</a></li>
          @endforeach
          @endif
          </div>
          <div class="col-md-3 col-sm-6">
          @if(isset($global_country_lists[3]))
          @foreach($global_country_lists[3] as $row => $name)
            <li><a style="cursor:pointer;" data-code="{{strtolower($row)}}" class="grayText location-switch">{{$name}}</a></li>
          @endforeach
          @endif
          </div>
        </ul>
      </div>
      </div>
      <div class="clearfix"></div>
    </div>

  </div>
</div>

<!--modal login -->
<div class="modal fade" id="modal-login" role="dialog">
  <div class="modal-dialog modal-sm" id="modal-login_form">
    <div class="modal-content removeBorder loginSize borderZero">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
          <h4 class="modal-title panelTitle">Login</h4>
        </div>
        <div class="modal-body">

            {!! Form::open(array('url' => 'login', 'role' => 'form', 'id' => 'myForm', 'class' => 'form-horizontal')) !!} 
            @if(Session::has('notice'))
                  <sup class="sup-errors redText">{!! Session::get('notice.msg.invalid_login') !!}</sup>
                @endif
              <div class="form-group">
                <h5 class="col-sm-3 normalText" for="register4-email">Username</h5>
                <div class="col-sm-9 error-username">
                  <input class="form-control borderZero inputBox" type="text" id="register4-email" name="username" placeholder="Enter your username..">
                  @if(Session::has('notice'))
                  <sup class="sup-errors redText">{!! Session::get('notice.msg.username_required') !!}</sup>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <h5 class="col-sm-3 normalText" for="register4-password">Password</h5>
                <div class="col-sm-9 error-password">          
                   <input class="form-control borderZero inputBox" type="password" id="register4-password" name="password" placeholder="Enter your password..">
                  @if(Session::has('notice'))
                  <sup class="sup-errors redText">{!! Session::get('notice.msg.password_required') !!}</sup>
                  @endif
                </div>
              </div>
              <div class="form-group">
                      <h5 class="col-xs-12 col-sm-12 normalText" for="register4-password">Security</h5>
                      <div class="col-xs-12 col-sm-12">           
              <!--  <div id="RecaptchaField1" class="col-md-12" required="required"></div> -->
              <!--  {!! app('captcha')->display(); !!}

               <div class="g-recaptcha" data-theme="light" data-sitekey="{{Session::get('siteKey')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
 -->            
               <div id="login_captcha"></div>
               @if(Session::has('notice'))
                  <sup class="sup-errors redText" style="margin-left: 102px;">{!! Session::get('notice.msg.captcha_required') !!}</sup>
                @endif
                </div>
              </div>
              <div class="row">
              <div class="borderTopDarkB modalSocialMedia">
                <a class="btn btn-block facebookButton borderZero" href="{{url('facebook/login')}}">
                  <i class="fa fa-facebook"></i> Login with Facebook
                </a>
                <a class="btn btn-block twitterButton borderZero" href="{{url('twitter/login')}}">
                  <i class="fa fa-twitter"></i> Login with Twitter
                </a>
                <a class="btn btn-block googleButton borderZero" href="{{url('google/login')}}">
                  <i class="fa fa-google-plus"></i> Login with Google
                </a>
              </div>
            </div>
        </div>
        <div class="modal-footer borderTopDarkB borderBottom">
          <span class="pull-left normalText redText topPadding">Not yet a member? 
            <a href="#" class="normalText redText topPadding" data-dismiss="modal" data-toggle="modal" data-target="#modal-register">Register Now!</a>
          </span>
          <span class="pull-right">
            <button type="submit" class="btn blueButton borderZero submit-btn">Login</button>
          </span>
        </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
<!--modal register -->
@if(Route::current()->getPath() == 'register/{referral_code}')
<div class="modal fade in" id="modal-register" role="dialog" data-backdrop="static" style="display: block; padding-right: 17px;">
@else
<div class="modal fade" id="modal-register" role="dialog" data-backdrop="static">
@endif
  <div class="modal-dialog modal-md">
    <div class="modal-content removeBorder borderZero">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close closeButton btn-close-register" data-dismiss="modal">&times;</button>
          <h4 class="modal-title panelTitle">Register</h4>
        </div>
        <div class="modal-body">
          <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
          </div>
          <div id="register-notice"></div>
          <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
          {!! Form::open(array('url' => 'register/save', 'role' => 'form', 'class' => 'form-horizontal','files'=>'true','id'=>'modal-register_form')) !!} 
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Username</h5>
              <div class="col-sm-9 error-username">
                <input type="text" class="form-control borderZero inputBox" name="username" id="client-username" placeholder="">
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Email</h5>
              <div class="col-sm-9 error-email_address">
                <input type="email" class="form-control borderZero inputBox" name="email" id="client-email" placeholder="">
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Password</h5>
              <div class="col-sm-9 error-password">
                <input type="password" class="form-control borderZero inputBox" name="password" id="client-password" placeholder="">
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Confirm Password</h5>
              <div class="col-sm-9 error-password_confirmation">
                <input type="password" class="form-control borderZero inputBox" name="password_confirmation" placeholder="">
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">User Type</h5>
              <div class="col-sm-9 erros-usertype">
               <!--  <select class="form-control borderZero inputBox text-uppercase" id="client-usertype" name="usertype"> -->
                  @foreach($user_types_global as $row)
                    <label class="radio-inline"><input type="radio" id="client-usertype" name="usertype" value="{{$row->id}}" {{$row->id == 1 ? 'checked':''}}>{{strToUpper($row->type_name)}}</label>
                  @endforeach
               <!--  </select> -->
               <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3 common-name-pane" for="email">Individual Name</h5>
              <div class="col-sm-9 error-name">
                <input type="text" class="form-control borderZero inputBox" name="name" id="client-name" placeholder="">
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group ">
              <h5 class="normalText col-sm-3" for="email">Referral Code (Optional)</h5>
              <div class="col-sm-9 error-name">
                <input type="text" class="form-control borderZero inputBox" value="{{Session::get('referral_code')}}" name="referral_code" id="client-referral-code" {{Session::has('referral_code')?'readonly="readonly"':''}} placeholder="">
<!--                 <sup class="sup-errors redText"></sup> -->
              </div>
            </div>
            <div class="form-group">
                      <h5 class="col-xs-3 col-sm-3 normalText" for="register4-password">Security</h5>
                      <div class="col-xs-3 col-sm-9 error-captcha">           
                      <div id="register_captcha"></div>
             <!--         <div class="g-recaptcha" data-theme="light" data-sitekey="{{Session::get('siteKey')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div> -->
                        <sup class="sup-errors redText"></sup>
                     </div>
           </div>
        </div>
        <div class="modal-footer bordertopLight borderBottom">
          <span class="pull-left normalText redText topPadding">Already a member? 
            <a href="#" class="normalText redText topPadding" data-dismiss="modal" data-toggle="modal" data-target="#modal-login">Login Now!</a>
          </span>
          <span class="pull-right">
            <button type="submit" class="btn blueButton borderZero">Register</button>
          </span>
        </div>
      </div>
    </div>
      {!! Form::close() !!}
</div>
  </div>

  
