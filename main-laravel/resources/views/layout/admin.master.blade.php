<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{$title}}/title>

    <!-- Bootstrap -->
    {!! HTML::script('css/bootstrap.min.css') !!}
    {!! HTML::script('css/xerolabs/backend/custom.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="#">
                  <img alt="Brand" src="...">
                </a>
              </div>
            </div>
          </nav>
        </div>
        <!-- Confirmation Dialog -->
        <div class="modal" id="modal-dialog" tabindex="-1" role="dialog" aria-labelledBy="dialog-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog">
            <div class="modal-content borderZero">
                <div id="load-dialog" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                </div>
              <div class="modal-header">
                <input id="token" class="hide" name="_token" value="{{ csrf_token() }}">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 id="dialog-title" class="modal-title"></h4>
              </div>
              <div class="modal-body" id="dialog-body"></div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default borderZero" data-url="" data-id="" id="dialog-confirm">Yes</button>
                <button type="button" class="btn btn-default borderZero" data-dismiss="modal">No</button>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6">
          </div>
          <div class="col-lg-6">
          </div>
        </div>
      </div>
      <div id="content">
        @yield('content')
      </div>
    </div>
      <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
  </body>
</html>