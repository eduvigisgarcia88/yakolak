@extends('layout.email')
@section('content')
<p>
	<?php
	if($action == 'expired') {
		echo "Your ad ".$ad_name." feature status has been ".$action.".";
	} else {
		echo "Your ad ".$ad_name." feature request has been ".$action.".";
	}
	?>
</p>
@stop