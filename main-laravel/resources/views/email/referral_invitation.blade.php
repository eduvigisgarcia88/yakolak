@extends('layout.email')
@section('scripts')
@stop
@section('content')
<link rel="stylesheet" href="sample.css" type="http://yakolak.xerolabs.co/plugins/css/summernote.css" />

<?php
 $search =  array('{REFERRAL_CODE}', '{CONTACT_NAME}','{USER_NAME}','{USER_EMAIL}','{USER_PHONE}');
 $replace = array($referral_code, $contact_name, $user_name, $user_email, $user_phone);                        
 $body = str_replace($search, $replace, $newsletter->body);
 echo ($body);
?>

<script src="http://yakolak.xerolabs.co/plugins/summernote.js"></script>
@stop
