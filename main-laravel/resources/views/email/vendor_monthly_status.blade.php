@extends('layout.email')
@section('scripts')
@stop
@section('content')
<p><?php
 $search =  array('{TOTAL_AD_POSTED}','{TOTAL_BID_POSTED}','{TOTAL_REFERRAL_POINTS}','{SMS}','{BID_POINTS}','{CURRENT_PLAN}');
 $replace = array($ad_posted, $bid_posted, $referral_points, $sms,$bid_points,$current_plan);                        
 $body = str_replace($search, $replace, $newsletter->body);
echo ($body);
 ?></p>
 <p style="margin-top: 20px;">Thank you!</p>
@stop
