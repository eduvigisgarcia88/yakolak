{{-- <!DOCTYPE html>
<html>
    <head>
        <title>Access denied</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        {!! HTML::style('/css/xerolabs/style.css') !!}
     
          {!! HTML::style('/css/xerolabs/common.css') !!}
          {!! HTML::style('/css/xerolabs/frontend/custom.css') !!}
          {!! HTML::style('css/bootstrap/bootstrap.min.css') !!}

        <style>

        </style>
    </head>
    <body>
    <nav class="navbar navbar-default navbar-fixed-top bgWhite borderBottom" style="height: 80px;">
  <div class="container noPadding">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      
      <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 leftPadding noPaddingXs">
        <div class="minPadding hidden-md hidden-lg noleftPadding toggleSize">
          <button class="btn noBGButton menuToggle borderZero sidebar-btn" type="button"> <i class="fa fa-bars"></i> </button>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-9 col-xs-5 leftPaddingMinXs">
                          <a class="navbar-brand logo" href="http://yakolak.xerolabs.co/ph">
                           <img src="http://yakolak.xerolabs.co/img/yakolaklogo.png" class="img-responsive responsiveImgXS">
             </a>
        </div>
      </div>

    </div>
  </div>
</nav>

        <div class="container-fluid">
            <div class="content" style="display: table; height: 97vh; width: 100%;">
                <div class="title" style="display: table-cell; width: 100%; vertical-align: middle; text-align: center; color: #B0BEC5; font-size: 50px;">Access denied</div>
            </div>
        </div>
        <div class="bgGray container-fluid">
        <div class="noPadding container">
        <div class="col-lg-12 ">
          <div class="col-lg-12 minPadding noleftPadding lightgrayText">© 2016 Yakolak.com. All rights reserved.</div>
        </div>
        </div>
        </div>
    </body>
</html> --}}



@extends('layout.frontend')
@section('scripts')
<script>
 $_token = "{{ csrf_token() }}";
$(function () {

  @foreach($adslatestbids as $row)
    $("#ads_latest_bids_{{$row->id}}").rateYo({
         starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbids_panel2 as $row)
    $("#ads_latest_bids_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads as $row)
    $("#latest_ads_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads_panel2 as $row)
    $("#latest_ads_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestadsmobile as $row)
    $("#latest_ads_mobile_{{$row->id}}").rateYo({
         starWidth: "14px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbidsmobile as $row)
    $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
         starWidth: "14px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
});

 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   var ads_id = $(this).data('ads_id');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('%DD-%HH-%MM-%SS'));
   });
 });


 $('#flyout_menu').each(function() {
            $.get("{{ url('user/plan/expiring') }}", { _token: $_token}, function(response) {
                append(response);   
    }, 'json');

 });
  $('#flyout_menu').each(function() {
            $.get("{{ url('user/plan/expired') }}", { _token: $_token}, function(response) {
                append(response);   
    }, 'json');

 });
   @if(Auth::check())
            $("#latest-ads-wrapper").on("click", "#btn-user-view", function() {
                var ad_id = $(this).data("id"); 
                var user_id = "{{Auth::user()->id}}";
                var post_url = "{{ url('user/save/ad/view') }}";
                $.post(post_url, { user_id: user_id,ad_id:ad_id,_token: $_token}, function(response) { 
                }, 'json'); 
             });
    @endif

      
    $("#main-container").on("click", "#btn-view-more-bids", function() {
          var last_id = $("#latest-bids-container:last").children().last().data('id');
          var btn = $(this);
          btn.html("");
          btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
          var loading = $("#panel-bids").find(".loading-pane");
          // loading.removeClass("hide");
          var skip = $("#skip").val();
          var take = $("#take").val();
          var get_current_no_bids = $("#per_bids").val();
          var per = parseInt(get_current_no_bids) + 4;
          var no_of_results = $("#no_of_results").val();
          var container = $("#latest-bids-container");
          var mobile_container = $("#bids-mobile-container");
          var body = "";
          var mobile_body="";
          $.post("{{url('get-bids-pagination')}}", {per:per,_token: $_token}, function(response) {
                $.each(response.rows, function(index, value) {
                    console.log(response.rows);
                   var concat_country = value.city+' , '+value.countryName;
                    var len = concat_country.length;
                    if(len > 13){
                    var res = concat_country.substr(0,13);
                    var ellipsible_location = res+'...';
                    }else{
                      ellipsible_location = concat_country;
                    }
                    var len = value.name.length;
                    if(len > 13){
                    var res = value.name.substr(0,13);
                    var ellipsible_name = res+'...';
                    }else{
                      ellipsible_name= value.name;
                    }
                  body += '<div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="'+value.id+'">'+
                      '<div class="sideMargin borderBottom">'+
                      '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                        '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo + '); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                      '</div></a>'+
                        '<div class="minPadding nobottomPadding">'+
                          '<div class="mediumText noPadding topPadding bottomPadding ellipsis cursor">'+
                                '<div class="visible-lg ellipsis">'+ value.title +
                               '</div>'+
                                '<div class="visible-md ellipsis">'+ value.title +
                                '</div>'+
                          '</div>'+
                          '<div class="normalText grayText bottomPadding bottomMargin ellipsis">'+ 
                           '<a class="normalText lightgrayText" href="{{ url('/').'/' }}'+value.alias+'"> by '+ellipsible_name+'</a>'+
                            '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' USD</strong>'+
                           '</span>'+
                          '</div>'+
                          '<div class="normalText bottomPadding">'+
                             '<i class="fa fa-map-marker rightMargin"></i> '+ellipsible_location+    
                            '<span class="pull-right">'+
                                   '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' +
                                   '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                            '</span>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class="sideMargin minPadding text-center nobottomPadding redText normalText bottomMarginC">'+       
                      '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.get_ads_id+'"></div>'+
                      '</div>'+
                      '</div>';

                   mobile_body +='<div class="panel panel-default removeBorder borderZero bottomMarginLight ">'+
                              '<div class="panel-body rightPaddingB noPadding">'+
                              '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                              '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"> <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo +'); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                                '</div></a>'+
                              '<div class="rightPadding nobottomPadding lineHeight">'+
                               '<div class="mediumText topPadding noPadding bottomPadding">'+
                                '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                      '<div class="visible-sm">'+ value.title +
                                      '</div>'+
                                      '<div class="visible-xs">'+ value.title +
                                      '</div>'+
                                '</a>'+
                                '<div class="normalText redText bottomPadding ellipsis">'+
                                       '<a class="normalText lightgrayText" href="{{ url('/').'/' }}'+value.alias+'"> by '+value.name+'</a>'+
                                '</div>'+
                                '</div>'+
                                '<div class="normalText redText bottomPadding">'+
                                '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.get_ads_id+'"></div>'+
                                '</div>'+
                                '<div class="mediumText blueText bottomPadding">'+
                                  '<strong>'+(value.price).toLocaleString()+'USD</strong>'+
                                '</div>'+
                                '<div class="normalText bottomPadding">'+
                                  '<i class="fa fa-map-marker rightMargin"></i>'+ concat_country +
                                '</div>'+
                                '<div style="padding-top:8px;">'+
                                      '<span id="ads_latest_bids_mobile_'+value.id+'" class="pull-left blueText rightMargin noPadding"></span>' +
                                      '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                                '</div>'+
                              '</div>'+
                              '</div>'+
                            '</div>';

                    $("#no_of_results").val(response.no_of_results);

                  });
          
            $("#per_bids").val(per);

            var per_bids = $("#per_bids").val();
            var max_bids = $("#max_bids").val();
            var parse_per_bids = parseInt(per_bids);
            var parse_max_bids = parseInt(max_bids);
            if(parse_per_bids >= parse_max_bids){
              $("#btn-view-more-bids").remove();
            }

          
            container.html(body);
            mobile_container.html(mobile_body);
            btn.html("");
            btn.html("View More");
  
             $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
             });
            $.each(response.rows, function(index, value) {      
               $("#ads_latest_bids_"+value.id).rateYo({
                   starWidth: "12px",
                   rating:(value.average_rate != null ? value.average_rate:0),
                   ratedFill: "#5da4ec",
                   normalFill: "#cccccc",
                   readOnly: true,
               });
            });
            $.each(response.rows, function(index, value) {      
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                   starWidth: "14px",
                   rating:(value.average_rate != null ? value.average_rate:0),
                   ratedFill: "#5da4ec",
                   normalFill: "#cccccc",
                   readOnly: true,
               });
            });

          }, 'json'); 
    });
   $("#main-container").on("click", "#btn-view-more-ads", function() {
            var last_id = $("#latest-ads-container:last").children().last().data('id');
            var loading = $("#panel-ads").find(".loading-pane");
            var get_current_no_ads= $("#per_ads").val();
            var per = parseInt(get_current_no_ads) + 4;
            // loading.removeClass("hide");
            var btn = $(this);
            btn.html("");
            btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
            var container = $("#latest-ads-container");
            var mobile_container = $("#ads-mobile-container");
            var body = "";
            var mobile_body ="";
            $.post("{{url('get-ads-pagination')}}", {per:per,_token: $_token}, function(response) {
                  
                  $.each(response.rows, function(index, value) {
                    var concat_country = value.city+' , '+value.countryName;
                    var len = concat_country.length;
                    if(len > 13){
                    var res = concat_country.substr(0,13);
                    var ellipsible_location = res+'...';
                    }else{
                      ellipsible_location = concat_country;
                    }
                    var len = value.name.length;
                    if(len > 13){
                    var res = value.name.substr(0,13);
                    var ellipsible_name = res+'...';
                    }else{
                      ellipsible_name= value.name;
                    }
                    console.log(response.rows);
                          
                    body += '<div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="'+value.id+'">'+
                        '<div class="sideMargin bottomMarginC borderBottom">'+
                        '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                          '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo + '); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                        '</div></a>'+
                          '<div class="minPadding nobottomPadding">'+
                            '<div class="mediumText noPadding topPadding bottomPadding ellipsis cursor">'+
                                  '<div class="visible-lg ellipsis">'+ value.title +
                                 '</div>'+
                                  '<div class="visible-md ellipsis">'+ value.title +
                                  '</div>'+
                            '</div>'+
                            '<div class="normalText grayText bottomPadding bottomMargin ellipsis">'+ 
                             '<a class="normalText lightgrayText" href="{{ url('/').'/' }}'+value.alias+'"> by '+ ellipsible_name +'</a>'+
                              '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' USD</strong>'+
                             '</span>'+
                            '</div>'+
                            '<div class="normalText bottomPadding">'+
                               '<i class="fa fa-map-marker rightMargin"></i> '+ellipsible_location+
                              '<span class="pull-right">'+
                                     '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' +
                                     '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                              '</span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '</div>';


                    mobile_body +='<div class="panel panel-default removeBorder borderZero bottomMarginLight ">'+
                              '<div class="panel-body rightPaddingB noPadding">'+
                              '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                              '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"> <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail') }}/' + value.photo +'); background-repeat: no-repeat; background-size: cover; background-position:center center;">'+
                                '</div></a>'+
                              '<div class="rightPadding nobottomPadding lineHeight">'+
                               '<div class="mediumText topPadding noPadding bottomPadding">'+
                                '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                      '<div class="visible-sm">'+ value.title +
                                      '</div>'+
                                      '<div class="visible-xs">'+ value.title +
                                      '</div>'+
                                '</a>'+
                                '<div class="normalText redText bottomPadding ellipsis">'+
                                       '<a class="normalText lightgrayText" href="{{ url('/').'/' }}'+value.alias+'"> by '+value.name+'</a>'+
                                '</div>'+
                                '</div>'+
                                '<div class="mediumText blueText bottomPadding">'+
                                  '<strong>'+(value.price).toLocaleString()+'USD</strong>'+
                                '</div>'+
                                '<div class="normalText bottomPadding">'+
                                  '<i class="fa fa-map-marker rightMargin"></i>'+ concat_country +
                                '</div>'+
                                '<div style="padding-top:8px;" class="topPaddingE">'+
                                      '<span id="latest_ads_mobile_'+value.id+'" class="pull-left blueText noPadding rightMargin"></span>' +
                                      '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                                '</div>'+
                              '</div>'+
                              '</div>'+
                            '</div>';  
                    });
              $("#per_ads").val(per);
              var per_ads = $("#per_ads").val();
              var max_ads = $("#max_ads").val();
            
              if(parseInt(per_ads) >= parseInt(max_ads)){
                $("#main-container").find("#btn-view-more-ads").remove();
              }
              container.html(body);
              mobile_container.html(mobile_body);
               btn.html("");
               btn.html("View More");
             
                
               $('[data-countdown]').each(function() {
                 var $this = $(this), finalDate = $(this).data('countdown');
                 var ads_id = $(this).data('ads_id');
                 $this.countdown(finalDate, function(event) {
                 $this.html(event.strftime('%DD-%HH-%MM-%SS'));
                 });
               });
              $.each(response.rows, function(index, value) {      
                 $("#ads_latest_bids_"+value.id).rateYo({
                     starWidth: "12px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                 });
              });
              $.each(response.rows, function(index, value) {      
                 $("#latest_ads_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                 });
              });
            }, 'json'); 
      });

$('#stats_block').remove();
</script>
@stop
@section('content')
<div class="container-fluid">
      <div class="content bgGray" style="padding: 115px 30px 30px 30px; display: table; height: 665px; width: 100%;">
          <div class="title bgWhite" style="display: table-cell; width: 100%; vertical-align: middle; text-align: center; font-size: 50px;">Access denied</div>
      </div>
  </div>
@stop
