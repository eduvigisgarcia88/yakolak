@extends('layout.frontend')
@section('scripts')
  <script>
   $_token = "{{ csrf_token() }}";

   $(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".allcategoryNav").addClass("navCollapse");
        
    } else {
        $(".allcategoryNav").removeClass("navCollapse");
      
    }
    });

   $("#allcategory a[href^='#']").on('click', function(e) {

       // prevent default anchor click behavior
       e.preventDefault();

       // animate
       $('html, body').animate({
           scrollTop: $(this.hash).offset().top - 220
         }, 400, function(){
   
           // when done, add hash to url
           // (default click behaviour)
           window.location.hash = this.hash;
         });

    });

   $('.allcategoryNav a').click(function(e) {
        e.preventDefault();
        $('a').removeClass('active');
        $(this).addClass('active');
    });

 //   var current_url = "{{Request::url()}}";
 //   var allcateg_url = "{{url('category/all')}}";
 //   if(current_url == allcateg_url){
 //    $('a.btn-login').attr('data-toggle','modal');
 //    $('a.btn-login').attr('data-target','#login-modal');
 //    $('a.btn-login').removeAttr('id','btn-login');
 //    $('a#btn-register').attr('data-toggle','modal');
 //    $('a#btn-register').attr('data-target','#register-modal');
 //    $('a#btn-register').removeAttr('id','btn-register');
  // $('[data-target="#modal-login"]').each(function(){
 //     $(this).attr('data-target', '#login-modal');
  // });
  // $('[data-target="#modal-register"]').each(function(){
 //     $(this).attr('data-target', '#modal-register');
  // });
 //   }

   $("#modal-register_form").submit(function(e) {

     var form = $("#modal-register").find("form");
     var loading = $("#modal-register").find("#load-form");
     // stop form from submitting
     e.preventDefault();

     loading.removeClass('hide');
     // console.log(new FormData($("#modal-register_form")[0]));
     // push form data
      $.ajax({
           type: "post",
           url: form.attr('action'),
           //data: form.serialize(),
           data: new FormData($("#modal-register_form")[0]),
           dataType: "json",   
           async: true,
           processData: false,
           contentType: false,
     success: function(response) {

           if (response.unauthorized) {
             window.location.href = response.unauthorized;
           }
       else if(response.error) {
         $(".sup-errors").html("");
         $(".form-control").removeClass("required");
         $.each(response.error, function(index, value) {
           var errors = value.split("|");
           $("." + errors[0].replace(/ /g,"_") + " .form-control").addClass("required");
           $("." + errors[0].replace(/ /g,"_") + " .sup-errors").html(errors[1]);
         });
       } else {
         $("#modal-form").modal('hide');
         status(response.title, response.body, 'alert-success');
         
         // refresh the list
         
         refresh();
       }

       loading.addClass('hide');

     }
     });
   });
   $(function($) {
    var num_cols = 3,
    container = $('.split-list'),
    listItem = 'li',
    listClass = 'col-sm-4';
    container.each(function() {
        var items_per_col = new Array(),
        items = $(this).find(listItem),
        min_items_per_col = Math.floor(items.length / num_cols),
        difference = items.length - (min_items_per_col * num_cols);
        for (var i = 0; i < num_cols; i++) {
            if (i < difference) {
                items_per_col[i] = min_items_per_col + 1;
            } else {
                items_per_col[i] = min_items_per_col;
            }
        }
        for (var i = 0; i < num_cols; i++) {
            $(this).append($('<div ></div>').addClass(listClass));
            for (var j = 0; j < items_per_col[i]; j++) {
                var pointer = 0;
                for (var k = 0; k < i; k++) {
                    pointer += items_per_col[k];
                }
                $(this).find('.' + listClass).last().append(items[j + pointer]);
            }
        }
    });
});
  </script>
@stop
@section('content')
<div class="container-fluid bgGray" id="allcategory" data-spy="scroll" data-target=".allcategoryNav" data-offset="50">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <nav class="navbar-fixed-top allcategoryNav hide">
        <div class="container">
          <div class="col-sm-12 norightPadding">
          <div class="panel panel-default removeBorder borderZero">
          <div class="panel-body noPadding">
            @foreach($categories as $row)
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#{{strTolower( str_replace(' ', '', $row->name))}}">{!! $row->icon_big !!} <span class="leftPadding">{{$row->name}}</span></a>
            </div>
            @endforeach
            
        <!--    <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#liveauctions"><i class="fa-2x fa fa-gavel" aria-hidden="true"></i> <span class="leftPadding">Live Auctions</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#carsandvehicles"><i class="fa-2x fa fa-car" aria-hidden="true"></i> <span class="leftPadding">Cars and Vehicles</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#clothing"><i class="fa-2x fa fa-female" aria-hidden="true"></i> <span class="leftPadding">Clothing</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#events"><i class="fa-2x fa fa-calendar" aria-hidden="true"></i> <span class="leftPadding">Events</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#jobs"><i class="fa-2x fa fa-suitcase" aria-hidden="true"></i> <span class="leftPadding">Jobs</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#realestate"><i class="fa-2x fa fa-home" aria-hidden="true"></i> <span class="leftPadding">Real Estate</span></a>
            </div>
            <div class="col-sm-12 col-md-3 categoryBlock noPadding">
              <a class="grayText" href="#services"><i class="fa-2x fa fa-shield" aria-hidden="true"></i> <span class="leftPadding">Services</span></a>
            </div> -->
          </div>
        </div>
      </div>
      </nav>
      </div>
      </nav>
      <div class="col-sm-7 col-md-9">
        @foreach($categories as $row)
        <div id="{{strTolower( str_replace(' ', '',$row->name))}}" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle"><a class="blueText" href="{{url('/').'/'.strtolower($country_code_session).'/'.'all-cities/'.$row->slug}}">{{$row->name}}</a></span></div>
          <div class="panel-body">
            <div class="col-lg-12 noleftPadding">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                @if($row->cat_type == 2)
                    @foreach($categories as $rows)
                      @if($rows->biddable_status == 1)
                         @foreach($rows->subcategoryinfo as $subCat)
                          <li><a href="{{url('/').'/'.strtolower($country_code_session).'/'.'all-cities/'.$row->slug.'/'.$subCat->slug}}/q?listing=auctions" class="grayText">{{$subCat->name}}</a></li>
                         @endforeach
                      @endif
                    @endforeach
                @elseif($row->cat_type == 1)
                  @if(count($row->subcategoryinfo) > 0)
                    @foreach($row->subcategoryinfo as $subCat)
                      <li><a href="{{url('/').'/'.strtolower($country_code_session).'/'.'all-cities/'.$row->slug.'/'.$subCat->slug}}" class="grayText">{{$subCat->name}}</a></li>
                    @endforeach
                  @endif
                @endif

       <!--          <li><a href="/press/" class="grayText">Clothing, Shoes & Jewelry</a></li>
                <li><a href="/about/careers/" class="grayText">Health, Beauty & Pharmacy</a></li>
                <li><a href="/blog/" class="grayText">Sewing, Craft & Party Supplies</a></li>
                <li><a href="/legal/terms/" class="grayText">Movies, Music & Books</a></li>
                <li><a href="/legal/privacy/" class="grayText">Baby & Toddler</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Sports, Fitness & Outdoors</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Home, Furniture & Patio</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Toys & Video Games</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Auto & Tires</a></li> -->
              </ul>
          </div>
          </div>
        </div>
        @endforeach
      <!--  <div id="liveauctions" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Live Auctions</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
               <li><a href="/press/" class="grayText">Mark McGwire's 70th-home-run baseball</a></li>
                <li><a href="/about/careers/" class="grayText">Wittelsbach diamond</a></li>
                <li><a href="/blog/" class="grayText">Badminton Cabinet</a></li>
                <li><a href="/legal/terms/" class="grayText">1957 Ferrari 250 Testa Rossa</a></li>
                <li><a href="/legal/privacy/" class="grayText">Roman-era statue, Artemis and the Stag</a></li>
              </ul>
          </div>
        </div>
        <div id="carsandvehicles" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Cars and Vehicles</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">Oil Filters</a></li>
                <li><a href="/press/" class="grayText">Car Lighting</a></li>
                <li><a href="/about/careers/" class="grayText">Auto Repair Tools</a></li>
                <li><a href="/blog/" class="grayText">Antifreeze & Coolants</a></li>
                <li><a href="/legal/terms/" class="grayText">Motor Oil</a></li>
                <li><a href="/legal/privacy/" class="grayText">Anti-Theft Devices</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Car Battery Chargers & Accessories</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">In-Dash Stereos</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Car Washes & Cleaners</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Car Wax & Polish</a></li>
                <li><a href="/about/" class="grayText">Cleaning Tools/a></li>
                <li><a href="/press/" class="grayText">Detailing Kits</a></li>
                <li><a href="/about/careers/" class="grayText">Pressure Washers</a></li>
                <li><a href="/blog/" class="grayText">Cargo Management</a></li>
                <li><a href="/legal/terms/" class="grayText">Exterior Car Decoration</a></li>
                <li><a href="/legal/privacy/" class="grayText">Floor Mats</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Seat Covers</a></li>
              </ul>
          </div>
        </div>
        <div id="clothing" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Clothing</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">Underwear</a></li>
                <li><a href="/press/" class="grayText">Socks</a></li>
                <li><a href="/about/careers/" class="grayText">Sleepwear</a></li>
                <li><a href="/blog/" class="grayText">Personalized Jewelry</a></li>
                <li><a href="/legal/terms/" class="grayText">Wedding Rings</a></li>
                <li><a href="/legal/privacy/" class="grayText">Watches</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Fashion Jewelry</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Fine Jewelry</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Baby Girls (0-24M)</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Baby Boys (0-24M)</a></li>
              </ul>
          </div>
        </div>
        <div id="events" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Events</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">World Pyro Olympics</a></li>
                <li><a href="/press/" class="grayText">Caracol Festival</a></li>
                <li><a href="/about/careers/" class="grayText">Pangisdaan Festival </a></li>
                <li><a href="/blog/" class="grayText">Chinese New Year</a></li>
                <li><a href="/legal/terms/" class="grayText">Travel Tour Expo</a></li>
                <li><a href="/legal/privacy/" class="grayText">Pasig Summer Music Festival</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Parade of Festivals</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Manila International Auto Show </a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Aliwan Fiesta </a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Ka-Angkan Festival</a></li>
                <li><a href="/about/" class="grayText">Flores de Mayo </a></li>
                <li><a href="/press/" class="grayText">Feast of Nuestra Señora de Guia</a></li>
                <li><a href="/about/careers/" class="grayText">Manila Music Festival</a></li>
                <li><a href="/blog/" class="grayText">Sunduan Festival</a></li>
                <li><a href="/legal/terms/" class="grayText">Manila Day </a></li>
                <li><a href="/legal/privacy/" class="grayText">Philippine International Jazz and Arts Festival</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Fête de la Musique </a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Subli-an sa Maynila</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Cinemanila International Film Festival </a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Cinemalaya Philippine Independent Film Festival</a></li>
                <li><a href="/about/" class="grayText">Anniversary of Cry of Pugadlawin </a></li>
                <li><a href="/press/" class="grayText">Nagsabado Festival</a></li>
                <li><a href="/about/careers/" class="grayText">Manila International Literary Festival</a></li>
                <li><a href="/blog/" class="grayText">Manila Salsa Congress</a></li>
                <li><a href="/legal/terms/" class="grayText">Marikina Christmas Festival </a></li>
              </ul>
          </div>
       </div>
        <div id="jobs" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Jobs</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">Orthodontist</a></li>
                <li><a href="/press/" class="grayText">Dentist</a></li>
                <li><a href="/about/careers/" class="grayText">Computer Systems Analyst</a></li>
                <li><a href="/blog/" class="grayText">Nurse Anesthetist</a></li>
                <li><a href="/legal/terms/" class="grayText">Physician Assistant</a></li>
                <li><a href="/legal/privacy/" class="grayText">Sub Category 6</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Nurse Practitioner</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Psychiatrist</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Pediatrician</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Anesthesiologist</a></li>
              </ul>
          </div>
        </div>
        <div id="realestate" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Real Estate</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">House Facts Realty</a></li>
                <li><a href="/press/" class="grayText">Knoxville Fine Homes</a></li>
                <li><a href="/about/careers/" class="grayText">The Haverkate Group</a></li>
                <li><a href="/blog/" class="grayText">Briscoe Real Estate</a></li>
                <li><a href="/legal/terms/" class="grayText">Gene Northup</a></li>
                <li><a href="/legal/privacy/" class="grayText">Bronx Experts Realty</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Janet DeBusk Hensley</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Greg Noonan</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Sally Forster Jones</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Laura Marie</a></li>
              </ul>
          </div>
        </div>
        <div id="services" class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBar"><span class="panelTitle">Services</span></div>
          <div class="panel-body">
              <ul class="list-unstyled lineHeightC split-list noPadding">
                <li><a href="/about/" class="grayText">Architects & Building Design</a></li>
                <li><a href="/press/" class="grayText">Builders - Garages/Barns/Sheds</a></li>
                <li><a href="/about/careers/" class="grayText">Carpet Sales/Installation/Repair</a></li>
                <li><a href="/blog/" class="grayText">Drain Pipe Installation - Exterior</a></li>
                <li><a href="/legal/terms/" class="grayText">Energy Efficiency Auditing</a></li>
                <li><a href="/legal/privacy/" class="grayText">Furniture - Refinishing & Repair</a></li>
                <li><a href="/i/trust-safety-mission/" class="grayText">Gutter Repair & Replacement</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Hardware & Home Improvement Stores</a></li>
                <li><a href="http://elance-odesk.com/online-work-report-global" class="grayText">Plumbing - Drain Cleaning</a></li>
                <li><a href="/hiring/" target="_blank" class="grayText">Snow Removal</a></li>
              </ul>
          </div>
        </div> -->
        </div>
      <div class="col-sm-5 col-md-3 noPadding">
        <div class="panel panel-default removeBorder bottomMarginB borderZero">
          <div class="panel-heading panelTitleBarLightB"> 
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
          <!-- <img class="fill" src="" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;"> -->
{{--           <div class="fill" style="background: url({{ url('uploads/banner/advertisement/desktop').'/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg':$banner_placement_right->image}}); background-position: center center; background-repeat: no-repeat; background-size: initial; height: 390px; width: 100%;">
          </div> --}}
          <img src="{{ url('uploads/banner/advertisement/desktop').'/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg':$banner_placement_right->image}}" style="height:390px; width:100%;">
       </div>
      </div>
        <div class="panel-heading panelTitleBarLightB {{count($featured_ads_per) == 0 ? 'hide' : ''}}">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads_per as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
                <a href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                  </div>
              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a>
                    <div class="normalText bottomPadding bottomMargin">
                          <a class="normalText lightgrayText" href="{{ url($featured_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
      
      </div>
</div>
</div>
</div>
@stop
