@extends('layout.frontend')
@section('scripts')

@stop

@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
				
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
				<div class="panel panel-default removeBorder borderZero borderBottom ">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SEARCH OPTIONS</span>
					</div>
					<div class="panel-body normalText">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Search Value">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Ad Type (Ads / Bids)">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Category">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Country">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="City / State">
					</div>
					<div class="panel-heading panelTitleBarLightB bordertopLight">
						<span class="panelTitleSub">(CATEGORY) ATTRIBUTES</span>
					</div>
					<div class="panel-body normalText">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
						<input type="text" class="form-control borderZero inputBox bottomMargin" placeholder="Custom Attribute">
					</div>
					<div class="panel-heading panelTitleBarLightB bordertopLight">
						<span class="panelTitleSub">PRICE RANGE</span>
					</div>
					<div class="panel-body normalText">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin " placeholder="ie. 100">
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding leftPadding">
								<input type="text" class="form-control borderZero inputBox bottomMargin " placeholder="ie. 10000">
							</div>
						</div>
						<button class="btn blueButton borderZero noMargin fullSize" type="button">Update Search</button>
					</div>
				</div>
					
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        <div class="panel-body noPadding">
        <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-5">
            <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
          </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-7">
            <div class="mediumText grayText">Featured Ads 2</div>
            <div class="normalText lightgrayText">by Dell Distributor</div>
            <div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
            <div class="mediumText blueText">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-empty"></i>
              <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
            </div>
          </div>
        </div>
        </div>
       </div>
					
        </div>


      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
				
				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-body">
					</div>
					<div class="panel-heading removeBorder panelTitleBarLightB">
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>

				</div>
				
				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelBlueTitle">Bid list</span>
					</div>
					<div class="panel-body noPadding visible-xs visible-sm">
							<div class="col-sm-12 col-xs-12 noPadding">
					        <div class="">
					         <a href="http://192.168.254.14/yakolak/public/ads/view/30"> <div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/air-jordan-shoes-15.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
					          </div></a>
					        <div class="rightPadding nobottomPadding">
					          <div class="mediumText noPadding topPadding bottomPadding">Jordan Shoesddd</div>
					          <div class="normalText redText bottomPadding">
					            D: 00 - H: 00 - M: 00
					          </div>
					          <div class="mediumText blueText bottomPadding">
					            <strong> USD</strong>
					          </div>
					          <div class="mediumText bottomPadding">
					          <i class="fa fa-map-marker"></i> 
					             
					          </div>
					          <div class="mediumText blueText">
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star-half-empty"></i>
					            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
					          </div>
					        </div>
					      </div>
					    </div>
					    <div class="col-sm-12 col-xs-12 noPadding">
					        <div class="">
					         <a href="http://192.168.254.14/yakolak/public/ads/view/30"> <div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/air-jordan-shoes-15.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
					          </div></a>
					        <div class="rightPadding nobottomPadding">
					          <div class="mediumText noPadding topPadding bottomPadding">Jordan Shoesddd</div>
					          <div class="normalText redText bottomPadding">
					            D: 00 - H: 00 - M: 00
					          </div>
					          <div class="mediumText blueText bottomPadding">
					            <strong> USD</strong>
					          </div>
					          <div class="mediumText bottomPadding">
					          <i class="fa fa-map-marker"></i> 
					             
					          </div>
					          <div class="mediumText blueText">
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star"></i>
					            <i class="fa fa-star-half-empty"></i>
					            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
					          </div>
					        </div>
					      </div>
					    </div>
					  </div>
						<div class="panel-body hidden-xs hidden-sm">
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url(http://192.168.254.14/yakolak/public/uploads/ads/thumbnail/air-jordan-shoes-15.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
              <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding ">
								<div class="sideMargin borderBottom">
									<div class="adImage" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
									</div>
									<div class="minPadding nobottomPadding">
										<div class="largeText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
										<div class="mediumText blueText bottomPadding">
											<strong>1,000,000 USD</strong>
											<span class="pull-right">
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star"></i>
												<i class="fa fa-star-half-empty"></i>
											</span>
										</div>
										<div class="normalText  bottomPadding">
											<i class="fa fa-map-marker"></i> Jeddah, UAE<span class="pull-right lightgrayText">27 <i class="fa fa-comment"></i></span>
										</div>
									</div>
								</div>
              </div>
						
					</div>
				</div>
				
				<nav class="noPadding">
					<ul class="pagination noPadding noMargin blockBottom">
						<li >
							<a href="#" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						</li>
						<li><a href="#">1</a></li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#">5</a></li>
						<li>
							<a href="#" aria-label="Next">
								<span aria-hidden="true">&raquo;</span>
							</a>
						</li>
					</ul>
				</nav>
				
				<div class="panel panel-default removeBorder borderZero ">
					<div class="panel-heading panelTitleBarLightB">
						<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="redText pull-right normalText">view rates</span>
					</div>
					<div class="panel-body">
					</div>
				</div>
				
        </div>
				
				
      </div>
    </div>
  </div>
@stop