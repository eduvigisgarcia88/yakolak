  @extends('layout.frontend')
@section('scripts')
  <script>
   $_token = "{{ csrf_token() }}";
   $("#ads-category").on("change", function() {
       var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
          var id = $("#ads-category").val();
          var main_category_id = $("#ads-main-category").val();
          var category_id = $("#ads-category").val();
          var sub_cat_one_id = $("#ads-subcategory").val();
          var sub_cat_two_id = $("#ads-subSubcategory").val();
          var category_auction_id = $("#ads-category-auction").val();
          var ad_type = $("#ad-type").val();
          if($("#ads-main-category").val() == "Y2F0LTI1"){
         $("#ads-category-auction").removeClass("hide");  
          }else{
             $("#ads-category-auction").addClass("hide"); 
          }
          var custom_attri = $("#custom-attributes-pane");
          var category_title = $("#category-title");
          category_title.html("");
          custom_attri.html("");
          var subcategory = $("#ads-subcategory");
          if(id){
             $.post("{{ url('get-custom-attributes') }}", { category_id:category_id,sub_cat_one_id:sub_cat_one_id,sub_cat_two_id:sub_cat_two_id,main_category_id:main_category_id,ad_type:ad_type ,category_auction_id:category_auction_id, id:id,_token: $_token }, function(response) {
             if(response.rows == ""){
                $("#custom-attribute-pane-title").addClass("hide");
                $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                custom_attri.html(response.rows);
                category_title.html(response.category_name);
              }
            }, 'json');
            if(id !="all"){
              $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
            if(response == "undefined"){
              $("#ads-subcategory").addClass("hide");
          }else{
              subcategory.html(response);
              $("#ads-subcategory").removeClass("hide");      
           }          
              }, 'json');
            }else{
              $("#ads-subcategory").addClass("hide");
            }
         }
        $("#ads-subcategory").val("all");
        $("#ads-subSubcategory").val("all");    
    });
   $("#ads-category-auction").on("change", function() {
          var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
        var id = $("#ads-category-auction").val();
          var category_auction_id = $("#ads-category-auction").val();
          console.log(category_auction_id);
          var main_category_id = $("#ads-main-category").val();
          var category_id = $("#ads-category").val();
          var sub_cat_one_id = $("#ads-subcategory").val();
          var sub_cat_two_id = $("#ads-subSubcategory").val();
          var ad_type = $("#ad-type").val();
          var custom_attri = $("#custom-attributes-pane");
          $("#ads-category").addClass("hide");
          custom_attri.html("");
          var category_title = $("#category-title");
          category_title.html("");

          var subcategory = $("#ads-subcategory");
             if(id == "Y2F0LTI4"){ // for events
            $("#range-from-container").html('<div class="input-group date error-price" id="date-event-date">'+
                                  '<input type="text" name="date_from" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY">'+
                                  '<span class="input-group-addon borderZero">'+
                                    '<span><i class="fa fa-calendar"></i></span>'+
                                '</span>'+
                          '</div>');
            $("#range-to-container").html('<div class="input-group date error-price" id="date-event-date">'+
                                  '<input type="text" name="date_to" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY">'+
                                  '<span class="input-group-addon borderZero">'+
                                    '<span><i class="fa fa-calendar"></i></span>'+
                                '</span>'+
                          '</div>');

            $("#range-title").html('<span class="panelTitleSub">DATE RANGE</span>');

          }else if(id == "Y2F0LTI5"){ // for jobs

            $("#range-from-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_from" placeholder="ie. 100">');
            $("#range-to-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_to" placeholder="ie. 100">');
              $("#range-title").html('<span class="panelTitleSub">SALARY RANGE</span>');

          }else{ // for ads

            $("#range-from-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="price_from" placeholder="ie. 100">');
            $("#range-to-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="price_to" placeholder="ie. 100">');
              $("#range-title").html('<span class="panelTitleSub">PRICE RANGE</span>');
              
          }
          if(id){
             $.post("{{ url('get-custom-attributes') }}", { category_id:category_id,sub_cat_one_id:sub_cat_one_id,sub_cat_two_id:sub_cat_two_id,main_category_id:main_category_id,ad_type:ad_type ,category_auction_id:category_auction_id, id:id,_token: $_token }, function(response) {
             if(response.rows == ""){
                $("#custom-attribute-pane-title").addClass("hide");
                $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                  category_title.html(response.category_name);
                custom_attri.html(response.rows);
              }
            }, 'json');

              
            if(id !="all"){
               var container = $("#ads-category");
           $.post("{{ url('get-main-subcategory') }}", { id: id, _token: $_token }, function(response) {
                  console.log(response);
                  if(response == "undefined"){
                    $("#ads-category").addClass("hide");
                  }else{
                    container.html(response);
                    $("#ads-category").removeClass("hide");                   
                  }
           }, 'json'); 

            }
         }

          $('.date').datetimepicker({
              locale: 'en',
              format: 'MM/DD/YYYY'
          });
    
    });
   $("#ads-subcategory").on("change", function() { 
        var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
        var id = $("#ads-subcategory").val();
        var main_category_id = $("#ads-main-category").val();
          var category_id = $("#ads-category").val();
          var sub_cat_one_id = $("#ads-subcategory").val();
          var sub_cat_two_id = $("#ads-subSubcategory").val();
          var category_auction_id = $("#ads-category-auction").val();
            var ad_type = $("#ad-type").val();
        // $("#ads-category-auction").addClass("hide");  
      $("#custom-attribute-container").removeClass("hide");
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
           var category_title = $("#category-title");
          category_title.html("");
            
          if(id){
              $.post("{{ url('get-sub-subcategory') }}", { id: id, _token: $_token }, function(response) {
            if(response.subsub_cat == "undefined"){
              $("#ads-subSubcategory").addClass("hide");
            }else{
              $("#ads-subSubcategory").removeClass("hide");
              $("#ads-subSubcategory").html(response.subsub_cat); 
            }
          }, 'json');
             $.post("{{ url('get-custom-attributes') }}", { category_id:category_id,sub_cat_one_id:sub_cat_one_id,sub_cat_two_id:sub_cat_two_id,main_category_id:main_category_id,ad_type:ad_type ,category_auction_id:category_auction_id, id:id,_token: $_token }, function(response) {
                 if(response.rows == ""){
                    $("#custom-attribute-pane-title").addClass("hide");
                    $("#custom-attribute-pane-container").addClass("hide");
                  }else{
                      $("#custom-attribute-pane-title").removeClass("hide");
                      $("#custom-attribute-pane-container").removeClass("hide");
                    custom_attri.html(response.rows);
                      category_title.html(response.category_name);
                  }     
               }, 'json');
          }
        $("#ads-subSubcategory").val("all");
   });
   $("#ad-price_from").on("keyup", function() {
    if($(this).val() == 0 || $(this).val() == "" ){
      $("#ad-price_to").removeAttr('required','required');
      $("#ad-price_to").val('');
    }else{
      $("#ad-price_to").attr('required','required');  
    }
   });
   $("#ad-price_to").on("change", function() {
    var price_to = $("#ad-price_to").val();
    if(price_to ==0){
      $(this).attr("required","required");
    }
   });
   $("#ad-type").on("change", function() {
    var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
    var id = $(this).val();
      $("#ads-category").addClass("hide");
      $("#ads-subcategory").addClass("hide");
      $("#ads-category-auction").addClass("hide");
      $("#ads-subSubcategory").addClass("hide");
      
    // if(id == "4"){
    //  // $("#ads-main-category").addClass("hide");
     //   $("#ads-category").val("all");
     //   $("#ads-subcategory").val("all");
     //   $("#ads-category-auction").val("all");
     //   $("#ads-subSubcategory").val("all");
     //   $("#ads-main-category").val("all"); 
    // }else{
      $("#ads-main-category").removeClass("hide");
      $("#ads-main-category").html("");
      $.post("{{ url('get-cat-type') }}", { id: id, _token: $_token }, function(response) {
          if(response.cat_type == "undefined"){
            $("#ads-main-category").html('<option value="all" readOnly>All</option>');  
          }else{
            $("#ads-main-category").html(response.cat_type);     
          } 
       }, 'json').complete(function(){
        @if(isset($direct_first_cat))
//          var direct_first_cat = '{{$direct_first_cat}}';
//          var if_opt_exist = $("#ads-main-category option[value='{{$direct_first_cat}}']").length > 0;
//        if(if_opt_exist)
//        {
//          $('#ads-main-category').val(direct_first_cat).change();
          
//        }
//        else
//        {
//          $('#ads-main-category').val('all').change();
//        }
          
          
          
      @endif
         
         @if(Request::route()->getName() == "SEO_URL" && !isset($getads_info))
            
              var cat = '{{ request()->route('parent_category_slug') }}';
             
            @elseif(Request::route()->getName() == "CAT_NEW_URL")
              
              var cat = '{{ request()->route('slug') }}';
             
            @endif
            
         var search_cat = $('#ads-main-category').children('[data-slug="'+cat+'"]').val();
         if(search_cat)
           {
             $('#ads-main-category').val(search_cat).change();
           }
          else
            {
              $('#ads-main-category').val('all').change();
            }
         console.log(cat);
         console.log('WTFWTF');
                 
         
       });
       
    // }
    $("#custom-attribute-pane-title").addClass("hide");
    $("#custom-attribute-pane-container").addClass("hide"); 
   });




   $("#ads-main-category").on("change", function() {
    var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
    $("#ads-category").val("all");
    $("#ads-subcategory").val("all");
    $("#ads-subSubcategory").val("all");
    $("#ads-category").addClass("hide");
    $("#ads-subcategory").addClass("hide");
    $("#ads-category-auction").addClass("hide");
    $("#ads-subSubcategory").addClass("hide");
    var category_title = $("#category-title");
    category_title.html("");

        var id = $("#ads-main-category").val();
        var main_category_id = $("#ads-main-category").val();
          var category_id = $("#ads-category").val();
          var sub_cat_one_id = $("#ads-subcategory").val();
          var sub_cat_two_id = $("#ads-subSubcategory").val();
          var category_auction_id = $("#ads-category-auction").val();
            var ad_type = $("#ad-type").val();
          if(id == "Y2F0LTI4"){ // for events
            $("#range-from-container").html('<div class="input-group date error-price" id="date-event-date">'+
                                  '<input type="text" name="date_from" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY">'+
                                  '<span class="input-group-addon borderZero">'+
                                    '<span><i class="fa fa-calendar"></i></span>'+
                                '</span>'+
                          '</div>');
            $("#range-to-container").html('<div class="input-group date error-price" id="date-event-date">'+
                                  '<input type="text" name="date_to" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY">'+
                                  '<span class="input-group-addon borderZero">'+
                                    '<span><i class="fa fa-calendar"></i></span>'+
                                '</span>'+
                          '</div>');

            $("#range-title").html('<span class="panelTitleSub">DATE RANGE</span>');

          }else if(id == "Y2F0LTI5"){ // for jobs

            $("#range-from-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_from" placeholder="ie. 100">');
            $("#range-to-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_to" placeholder="ie. 100">');
              $("#range-title").html('<span class="panelTitleSub">SALARY RANGE</span>');

          }else{ // for ads

            $("#range-from-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="price_from" placeholder="ie. 100">');
            $("#range-to-container").html('<input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="price_to" placeholder="ie. 100">');
              $("#range-title").html('<span class="panelTitleSub">PRICE RANGE</span>');
              
          }
          // $("#ads-subcategory").html("");
          var container = $("#ads-category");
         if($("#ads-main-category").val() != "Y2F0LTI1"){
           $.post("{{ url('get-main-subcategory') }}", { id: id, _token: $_token }, function(response) {
                  console.log(response);
                  if(response == "undefined"){
                    $("#ads-category").addClass("hide");
                  }else{
                    container.html(response);
                    $("#ads-category").removeClass("hide");                   
                  }
           }, 'json').complete(function(){
            @if(isset($direct_second_cat))
//              var direct_second_cat = '{{$direct_second_cat}}';
//              var if_opt_exist = $("#ads-category option[value='{{$direct_second_cat}}']").length > 0;
//              if(if_opt_exist)
//              {
//                $('#ads-category').val(direct_second_cat).change(); 
//              }
//              else
//              {
//                $('#ads-category').val('all').change(); 
//              }
              
//              refresh();
//              $('#update_search_load').remove();
            @endif
            
            @if(Request::route()->getName() == "SEO_URL" && !isset($getads_info))
            
              var cat = '{{ request()->route('parent_category_slug') }}';
              var identify = '{{ request()->route('category_slug') }}';
              
              if(identify != 'q'){
                 var sub = '{{ request()->route('category_slug') }}';
               }else
                   {
                     var sub = 'all';
                   }
             
            @elseif(Request::route()->getName() == "CAT_NEW_URL")
              
              var cat = '{{ request()->route('slug') }}';
              var identify = '{{ request()->route('q') }}';
               if(identify != 'q'){
                 var sub = '{{ request()->route('q') }}';
               }else
                   {
                     var sub = 'all';
                   }
             
            @endif
            
//                var search_cat = $('#ads-main-category').children('[data-slug="'+cat+'"]').val();
//                $('#ads-main-category').val(search_cat);
           if(sub || sub != 'all'){
               var set = $('#ads-category').children('[data-slug="'+sub+'"]').val();
             if(set){
               $('#ads-category').val(set).change();
             }else
               {
                 $('#ads-category').val('all').change();
               }
                          
             }
           else{
             $('#ads-category').val('all').change();
           }
//                $('#ads-category').children('[data-slug="'+cat+'"]').val((sub ? sub : 'all')).change();
                refresh();
                $('#update_search_load').remove();
             
             console.log(sub);
             console.log('TFTF');
            
           }); 
         var custom_attri = $("#custom-attributes-pane");
              custom_attri.html("");
              $.post("{{ url('get-custom-attributes') }}", { category_id:category_id,sub_cat_one_id:sub_cat_one_id,sub_cat_two_id:sub_cat_two_id,main_category_id:main_category_id,ad_type:ad_type ,category_auction_id:category_auction_id, id:id,_token: $_token }, function(response) {
               if(response.rows == ""){
                  $("#custom-attribute-pane-title").addClass("hide");
                  $("#custom-attribute-pane-container").addClass("hide");
                }else{
                  custom_attri.html(response.rows);
                    $("#custom-attribute-pane-title").removeClass("hide");
                    $("#custom-attribute-pane-container").removeClass("hide");
                    category_title.html(response.category_name);    
                }     
            }, 'json');
        }else if(id == 'all'){
          $("#ads-category-auction").val('all');
          $("#ads-category").val('all');
          $("#ads-subcategory").val('all');

        }else{
          var container_auction = $("#ads-category-auction");
          $.post("{{ url('get-live-auction-category') }}", { id: id, _token: $_token }, function(response) {
                  console.log(response);
                  if(response == "undefined"){
                    $("#ads-category-auction").addClass("hide");  
                  }else{
                    container_auction.html(response.auction_cat);
                      $("#ads-category-auction").removeClass("hide");             
                  }
           }, 'json'); 
            var auction_id = $("#ads-category-auction").val();
            var custom_attri = $("#custom-attributes-pane");
            var category_title = $("#category-title");
            category_title.html("");
              custom_attri.html("");
              $.post("{{ url('get-custom-attributes') }}", { category_id:category_id,sub_cat_one_id:sub_cat_one_id,sub_cat_two_id:sub_cat_two_id,main_category_id:main_category_id,ad_type:ad_type ,category_auction_id:category_auction_id, id:id,_token: $_token }, function(response) {
               if(response.rows == ""){
                  $("#custom-attribute-pane-title").addClass("hide");
                  $("#custom-attribute-pane-container").addClass("hide");
                }else{
                  custom_attri.html(response.rows);
                    $("#custom-attribute-pane-title").removeClass("hide");
                    $("#custom-attribute-pane-container").removeClass("hide");
                    category_title.html(response.category_name);  
                }     
            }, 'json');

        }
    
          $('.date').datetimepicker({
              locale: 'en',
              format: 'MM/DD/YYYY'
          });

   });

   $("#client-country").on("change", function(t) {
    var check_event = t;
      var prev_main_id  = $("#previous-main-cat-id").val();
        if(prev_main_id == ""){
          $("#update-status").val(0);
        }else{
          $("#update-status").val(2);
        }
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city-for-search') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json').complete(function(){
            @if(isset($search_city))
              var if_opt_exist = $("#client-city option[value='{{$search_city}}']").length > 0;
              var cntry = $('#client-country').val();
              if(if_opt_exist == true && cntry != 'all')
              {
                $('#client-city').val({{$search_city}}).change();
                if (check_event.originalEvent === undefined)
            {
              refresh();
            }
              }
              else if(if_opt_exist == false)
              {
                $('#client-city').val('all').change();
                if (check_event.originalEvent === undefined)
            {
              refresh();
            }
              }
            @else
            if (check_event.originalEvent === undefined)
        {
          refresh();
        }
      @endif
          });
          
       }
   });

   function stars() {
    @foreach($rows as $row)
      $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
   }

   @foreach($rows as $row)
      $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach

    @foreach($rows as $row)
      $("#latest_ads_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#mobile_ads_{{$row->id}}").rateYo({
           starWidth: "14px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#grid_view_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#ad_rating_list_{{$row->id}}").rateYo({
           starWidth: "14px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#ad_rating_grid_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#mobile_view_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#list_view_{{$row->id}}").rateYo({
           starWidth: "12px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
    @foreach($rows as $row)
      $("#latest_ads_mobile_{{$row->id}}").rateYo({
           starWidth: "12",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
     @foreach($featured_ads as $row)
      $("#featured_ads_cat_search_{{$row->id}}").rateYo({
           starWidth: "14px",
           rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
           readOnly: true,
      });
    @endforeach
     $('#change-result-view').click(function(){
         $('#list-view').toggleClass('hide');
         $('#grid-view').toggleClass('hide');
   });
   var $divs_grid = $("div.sort-box-grid");
   var $divs_list = $("div.sort-box-list");
   $('#sort-by-name').on('click', function () {
        var alphabeticallyOrderedDivsGrid = $divs_grid.sort(function (a, b) {
              return $(a).find("h1").text().toLowerCase() > $(b).find("h1").text().toLowerCase();
          });
          $("#sort-grid-container").html(alphabeticallyOrderedDivsGrid);
           var alphabeticallyOrderedDivsList = $divs_list.sort(function (a, b) {
              return $(a).find("h1").text().toLowerCase() > $(b).find("h1").text().toLowerCase();
          });
          $("#sort-list-container").html(alphabeticallyOrderedDivsList);
   });
   $('#sort-by-price').on('click', function () {
         var numericallyOrderedDivsGrid = $divs_grid.sort(function (a, b) {
              return $(a).find("h2").text() > $(b).find("h2").text();
          });
          $("#sort-grid-container").html(numericallyOrderedDivsGrid);
          var numericallyOrderedDivsList = $divs_list.sort(function (a, b) {
              return $(a).find("h2").text() > $(b).find("h2").text();
          });
          $("#sort-list-container").html(numericallyOrderedDivsList);
   }); 
    $('[data-countdown]').each(function() {
     var $this = $(this), finalDate = $(this).data('countdown');
     var ads_id = $(this).data('ads_id');
     $this.countdown(finalDate, function(event) {
     $this.html(event.strftime('%DD-%HH-%MM-%SS'));
     });
   });

  function get_url_end(str, chr){
      var index = str.indexOf(chr);
      if (index != -1) {
          return(str.substring(0, index)+'=');
      }
      return("");
  }

  function format_url(str) {
    str = (str + '').toString();
    return encodeURIComponent(str)
      .replace(/!/g, '%21')
      .replace(/'/g, '%27')
      .replace(/\(/g, '%28')
      .replace(/\)/g, '%29')
      .replace(/\*/g, '%2A')
      .replace(/%20/g, '+');
  }

   function refresh(){
      $("#update-status").val(1);
       var order = $("#order").val();
       var sort=$("#sort").val();
       var no_of_ads = $("#row-no_of_ads").val();
       var loading = $("#search-container").find(".loading-pane");
       var grid_container = $("#sort-grid-container");
       var list_container = $("#sort-list-container");
       var mobile_container = $("#mobile_ui_refresh"); 
       var mobile_content = "";
       
       var grid_content = "";
       var list_content = "";
       $("#breadcrumb").html("");
       if(no_of_ads == ""){
         $("#per").val("9");
       }else{
         $("#per").val(no_of_ads);
       }
      
       // grid_container.html("");
       // list_container.html("");
      loading.removeClass("hide");
      var form = $("#search-form").find("form");
      $.ajax({
              type: "post",
          url: form.attr('action'),
          data: form.serialize(),
          dataType: "json",
      success: function(response) {
            try
        {
          //check if ipv4 or a real domain (regex)
          let isIPV4 = RegExp([
              '^https?:\/\/([a-z0-9\\.\\-_%]+:([a-z0-9\\.\\-_%])+?@)?',
              '((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4',
              '][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])?',
              '(:[0-9]+)?(\/[^\\s]*)?$'
          ].join(''), 'i');
          var protocol = window.location.protocol + '//';
          var host = window.location.host;
          var search_val = $('#search-keyword').val();
          var ip_bool = isIPV4.test(window.location.protocol + '//' + window.location.host);
          var selected_country = $('#client-country :selected').data('code');
          var category_slug = ($('#ads-main-category :selected').data('slug') == undefined ? 'all-categories' : $('#ads-main-category :selected').data('slug'));
          var listing_no = $('#ad-type').val();
          var get_delimeter = (search_val == '' ? '?' : '&');
          var listing_name = ((listing_no == 2) ? get_delimeter + 'listing=auctions' : (listing_no == 1) ? get_delimeter + 'listing=buy-and-sell' : '');
          var ads_subcat = $('#ads-category :selected').data('slug');
          var sub_category_slug = (ads_subcat == 'all' || ads_subcat == undefined ? '' : '/' + ads_subcat);
          var city = ($('#client-city :selected').data('slug') ? $('#client-city :selected').data('slug') : 'all-cities') + '/';
          var search_query = (search_val == '' ? '' : '/q?search=' + format_url(search_val));
          var synology_add_on = '/' + window.location.pathname.split('/')[1] + '/' + window.location.pathname.split('/')[2] + '/';
          var synology_url = (ip_bool == true ? synology_add_on : '/');
          var country_pos = (ip_bool == true ? 3 : 1);
          var current_country = window.location.pathname.split('/')[country_pos];
          var final_country = current_country.replace(current_country, selected_country) + '/';

          //final SEF URL
          var seo_url = protocol + host + synology_url + final_country + city + category_slug + sub_category_slug + search_query + listing_name;
          
          window.history.pushState('', '', seo_url);
          
        }
        catch(e)
        {
          console.log(e);
        }

        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        }else if(response.rows=="") {
             $("#sort-param").addClass("hide");
             $("#btn-view-more-bids").addClass("hide");
             $(".viewMoreList").addClass("hide");
             grid_content += '<h3 class="text-center">No Result(s) found<h3>';
             grid_container.html(grid_content); 
             mobile_content += '<h3 class="text-center">No Result(s) found<h3><div class="clearfix"></div>';
             mobile_container.html(mobile_content);
        }else {
          $("#sort-param").removeClass("hide");

// alert(response.url);
          var top_image_desktop_url = '{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}'+response.banner_top.image_desktop;
          var top_image_mobile_url = '{{URL::route('uploads', array(), false).'/banner/advertisement/mobile/'}}'+response.banner_top.image_mobile;  

          var side_image_a_url =  '{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}'+response.banner_left_A.image_desktop;
          var side_image_b_url =  '{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}'+response.banner_left_B.image_desktop;

          var bottom_image_desktop_url =  '{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}'+response.banner_bottom.image_desktop;
          var bottom_image_mobile_url =  '{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}'+response.banner_bottom.image_mobile;


          $("#top_image_desktop").attr('src',top_image_desktop_url).parent('a').attr('href', response.banner_top.url);
          $("#top_image_mobile").attr('src',top_image_mobile_url).parent('a').attr('href', response.banner_top.url);

          $("#side_image_A").attr('src',side_image_a_url).parent('a').attr('href', response.banner_left_A.url);
          $("#side_image_B").attr('src',side_image_b_url).parent('a').attr('href', response.banner_left_B.url);

          $("#bottom_image_desktop").attr('src',bottom_image_desktop_url).parent('a').attr('href', response.banner_bottom.url);
          $("#bottom_image_mobile").attr('src',bottom_image_mobile_url).parent('a').attr('href', response.banner_bottom.url);
          

          $.each(response.rows, function(index, value) {



               grid_content += '<div class="col-lg-4 col-md-4 noPadding box" data-id="'+value.id+'">'+
                          '<div class="sideMargin  bottomMarginC">'+
                          '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span><span class="label '+(value.ads_type_id != 1 ? '':'hide')+'" data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'" style="background-color: #ff6d6d; font-size: 14px; border-radius: 0; position: absolute; margin: 130px 0 0 5px;"></span>'+
                            '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                            '<div class="minPadding borderBottom nobottomPadding">'+
                              '<div class="mediumText noPadding ellipsis cursor">'+
                                  '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                   '<div class="visible-lg ellipsis">'+ value.title +'</div>'+
                                   '<div class="visible-md ellipsis">'+ value.title +'</div>'+
                                 '</a>'+
                              '</div>'+
                              '<div class="normalText grayText bottomPadding">'+ 
                  '<div class="row">'+
                    '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.name +'>'+
                      '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ value.name +'</a>'+
                    '</div>'+
                    '<div class="col-sm-6">'+
                      '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                              '<div class="normalText bottomPaddingB">'+
                                 '<div class="row">'+
                  '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.countryName +'>'+
                                 '<i class="fa fa-map-marker rightMargin"></i> ' + value.city +' , '+ value.countryName +    
                                 '</div>'+
                                 '<div class="col-sm-6">'+
                                '<span class="pull-right">'+
                                       (value.average_rate != null ? '<span id="ad_rating_grid_'+value.id+'" class="pull-left blueText"></span>' : '<span class="lightgrayText normalText rightPadding"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                                       '<span class="lightgrayText pull-right normalText"> '+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';            
         
            
             grid_content += '</div></div>';


        list_content += '<div class="sort-box-list"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">'+
                  '<div class="panel panel-default removeBorder borderZero bottomMarginLight">'+
                    '<div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">'+
                     '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+       
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImageB"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImageB" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                      '<div class="nobottomPadding lineHeight parent" style="padding-left: 130px;">'+
                      '<div class="mediumText grayText topPaddingB">'+
                        '<a class="grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"><span class="text-capitalize">'+value.title+'</span></a>'+
                        '<span class="pull-right blueText mediumText">'+
                          '<strong>'+value.price+' '+value.currency+'</strong>'+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-6 noleftPadding">'+
      '<div class="normalText redText">';
              if(value.ads_type_id == 2){
            list_content +=  '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'"></div>';
                     } 

              list_content += '</div>'+   
                      '<div class="normalText bottomPaddingB">'+
                        '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-6 norightPadding">'+
                        '<span class="pull-right normalText">'+
                        (value.average_rate != null ? '<span id="ad_rating_list_'+value.id+'" class="pull-left blueText norightMargin norightPadding"></span>' : '<span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-12 noPadding text-justify">'+
                      '<div class="normalText bottomPadding bottomMarginD"><span class="more normalText grayText text-capitalize" style="word-break: break-word;">'+value.description+
                      '</span></div></div>'+
                        '<div class="normalText bottomPaddingB child">'+
                          '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+
                          '<span class="pull-right normalText">'+
                           '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +'<i class="fa fa-comment leftPadding"></i></span>'+
                          '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';
                 
             list_content +='</div> ';

             mobile_content += '<div class="panel panel-default bottomMarginLight removeBorder borderZero">'+
                      '<div class="panel-body rightPaddingB noPadding">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                    '<div class="rightPadding nobottomPadding lineHeight">'+
                    '<div class="mediumText topPadding noPadding bottomPadding">'+
                    '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                    '<div class="visible-sm ellipsis">'+value.title+'</div>'+
                    '<div class="visible-xs ellipsis">'+value.title+'</div>'+
                    '</a>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<a class="normalText lightgrayText ellipsis" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                    '</div>'+
                    '</div>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<div style="'+(value.ads_type_id == 2 ? '' : 'opacity: 0;')+'" data-countdown="'+value.ad_expiration+'"></div>'+
                    '</div>'+
                    '<div class="mediumText blueText bottomPadding">'+
                    '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                    '</div>'+
                    '<div class="normalText bottomPadding">'+
                    '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+'</div>'+
                    '<div style="padding-top:8px;">'+
                    '<span id="ads_latest_bids_mobile_'+value.id+'" class="blueText rightMargin pull-left"></span>'+
                    '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';

                      });
          
          $("#breadcrumb").html(response.history);
          $("#per").val(response.per);
          $("#count").val(response.count);
          $("#total-count").text(response.count);
          
          $("#previous-main-category").val(response.previous_main_category);
            $("#previous-main-cat-id").val(response.previous_main_cat_id);
            $("#previous-category").val(response.previous_category);
            $("#previous-category-auction").val(response.previous_category_auction);
            $("#previous-subcategory").val(response.previous_subcategory);
            $("#previous-subSubCategory").val(response.previous_subSubCategory);
            $("#previous-country").val(response.previous_country);
            $("#previous-city").val(response.previous_city);
            $("#previous-ad-type").val(response.previous_ad_type);
            $("#previous-keyword").val(response.previous_keyword);
            // $("#previous-salary-from").val(response.previous_category);
            $("#previous-salary-to").val(response.previous_salary_to);
            $("#previous-price-from").val(response.previous_price_from);
            // $("#previous-price-to").val(response.previous_category);
            // $("#previous-date-from").val(response.previous_category);
            // $("#previous-date-to").val(response.previous_category);

          if(response.count < 10){
            $("#change-no-of-ads").addClass("hide");
          }else{
            $("#change-no-of-ads").removeClass("hide");
          }
          if(response.per > response.count){
               $("#btn-view-more-bids").addClass("hide").parent().addClass("hide");
               $('[id="btn-view-more-list"]').parent().addClass("hide");
            }else{
               $('[id="btn-view-more-list"]').parent().removeClass("hide");
               $("#btn-view-more-bids").removeClass("hide").parent().removeClass("hide");
            }
                grid_container.html(grid_content);
                list_container.html(list_content);
                mobile_container.html(mobile_content);
                if(grid_content.length > 0)
                {
                  $('#change-no-of-ads').removeClass('hide');
            $('#bidding-count-container').parent().parent().removeClass('hide');
            $('#change-result-view').parent().removeClass('hide');
                }
                else
                {
                  $('#change-no-of-ads').addClass('hide');
            $('#bidding-count-container').parent().parent().addClass('hide');
            $('#change-result-view').parent().addClass('hide');
                }

        }
           $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ad_rating_list_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

           $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

          $.each(response.rows, function(index, value) {
               $("#ad_rating_grid_"+value.id).rateYo({
                     starWidth: "12px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });
           $('[data-countdown]').each(function() {
             var $this = $(this), finalDate = $(this).data('countdown');
             var ads_id = $(this).data('ads_id');
             $this.countdown(finalDate, function(event) {
             $this.html(event.strftime('%DD-%HH-%MM-%SS'));
             });
           });
           $(function () {
  // Configure/customize these variables.
  var showChar = 365;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more";
  var lesstext = "Show less";


    $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

    var c = content.substr(0, showChar);
    var h = content.substr(showChar, content.length - showChar);

    var html = c + ellipsestext ;

    $(this).html(html);
    }

  });
});
        loading.addClass('hide');

      }
      });
      
   }
   function maxAds(){
       var order = $("#order").val();
       var sort=$("#sort").val();
       var no_of_ads = $("#row-no_of_ads").val();
       var loading = $("#search-container").find(".loading-pane");
       var grid_container = $("#sort-grid-container");
       var list_container = $("#sort-list-container");
       var grid_content = "";
       var list_content = "";
       var mobile_container = $("#mobile_ui_refresh"); 
       var mobile_content = "";
       $("#breadcrumb").html("");
       if(no_of_ads == ""){
         $("#per").val("9");
       }else{
         $("#per").val(no_of_ads);
       }
      $("#client-country").val("all");
      $("#client-city").val("all");
       // grid_container.html("");
       // list_container.html("");
       loading.removeClass("hide");
      var form = $("#search-form").find("form");
      $.ajax({
              type: "post",
          url: form.attr('action'),
          data: form.serialize(),
          dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        }else if(response.rows=="") {
             $("#sort-param").addClass("hide");
             $("#btn-view-more-bids").addClass("hide");
             grid_content += '<h3 class="text-center">No Result(s) found<h3>';
             grid_container.html(grid_content); 
             mobile_content += '<h3 class="text-center">No Result(s) found<h3><div class="clearfix"></div>';
             mobile_container.html(mobile_content);
        }else {
          $("#sort-param").removeClass("hide");
          $.each(response.rows, function(index, value) {
                      grid_content += '<div class="col-lg-4 col-md-4 noPadding box" data-id="'+value.id+'">'+
                          '<div class="sideMargin  bottomMarginC">'+
                          '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span><span class="label '+(value.ads_type_id != 1 ? '':'hide')+'" data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'" style="background-color: #ff6d6d; font-size: 14px; border-radius: 0; position: absolute; margin: 130px 0 0 5px;"></span>'+
                            '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                            '<div class="minPadding borderBottom nobottomPadding">'+
                              '<div class="mediumText noPadding ellipsis cursor">'+
                                  '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                   '<div class="visible-lg ellipsis">'+ value.title +'</div>'+
                                   '<div class="visible-md ellipsis">'+ value.title +'</div>'+
                                 '</a>'+
                              '</div>'+
                              '<div class="normalText grayText bottomPadding">'+ 
                  '<div class="row">'+
                    '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.name +'>'+
                      '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ value.name +'</a>'+
                    '</div>'+
                    '<div class="col-sm-6">'+
                      '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                              '<div class="normalText bottomPaddingB">'+
                                 '<div class="row">'+
                  '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.countryName +'>'+
                                 '<i class="fa fa-map-marker rightMargin"></i> ' + value.city +' , '+ value.countryName +    
                                 '</div>'+
                                 '<div class="col-sm-6">'+
                                '<span class="pull-right">'+
                                       (value.average_rate != null ? '<span id="ad_rating_grid_'+value.id+'" class="pull-left blueText"></span>' : '<span class="lightgrayText normalText rightPadding"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                                       '<span class="lightgrayText pull-right normalText"> '+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';             
            
             grid_content += '</div></div>';


                        list_content += '<div class="sort-box-list"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">'+
                  '<div class="panel panel-default removeBorder borderZero bottomMarginLight">'+
                    '<div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">'+
                     '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+       
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImageB"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImageB" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                      '<div class="nobottomPadding lineHeight parent" style="padding-left: 130px;">'+
                      '<div class="mediumText grayText topPaddingB">'+
                        '<a class="grayText" href ="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"><span class="text-capitalize">'+value.title+'</span></a>'+
                        '<span class="pull-right blueText mediumText">'+
                          '<strong>'+(value.price)+' '+value.currency+'</strong>'+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-6 noleftPadding">'+
      '<div class="normalText redText">';
              if(value.ads_type_id != 1){
            list_content +=  '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'"></div>';
                     } 

              
              list_content += '</div>'+   
                      '<div class="normalText bottomPaddingB">'+
                        '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-6 norightPadding">'+
                        '<span class="pull-right normalText">'+
                        (value.average_rate != null ? '<span id="ad_rating_list_'+value.id+'" class="pull-left blueText norightMargin norightPadding"></span>' : '<span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-12 noPadding text-justify">'+
                      '<div class="normalText bottomPadding bottomMarginD"><span class="more normalText grayText text-capitalize" style="word-break: break-word;">'+value.description+
                      '</span></div></div>'+
                        '<div class="normalText bottomPaddingB child">'+
                          '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+
                          '<span class="pull-right normalText">'+
                           '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +'<i class="fa fa-comment leftPadding"></i></span>'+
                          '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';
                 
             list_content +='</div> ';

             mobile_content += '<div class="panel panel-default bottomMarginLight removeBorder borderZero">'+
                      '<div class="panel-body rightPaddingB noPadding">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                    '<div class="rightPadding nobottomPadding lineHeight">'+
                    '<div class="mediumText topPadding noPadding bottomPadding">'+
                    '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                    '<div class="visible-sm ellipsis">'+value.title+'</div>'+
                    '<div class="visible-xs ellipsis">'+value.title+'</div>'+
                    '</a>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<a class="normalText lightgrayText ellipsis" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                    '</div>'+
                    '</div>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<div style="'+(value.ads_type_id == 2 ? '' : 'opacity: 0;')+'" data-countdown="'+value.ad_expiration+'"></div>'+
                    '</div>'+
                    '<div class="mediumText blueText bottomPadding">'+
                    '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                    '</div>'+
                    '<div class="normalText bottomPadding">'+
                    '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+'</div>'+
                    '<div style="padding-top:8px;">'+
                    '<span id="ads_latest_bids_mobile_'+value.id+'" class="blueText rightMargin pull-left"></span>'+
                    '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';


                      });
          $("#breadcrumb").html(response.history);
          $("#per").val(response.per);
          $("#count").val(response.count);
          $("#total-count").text(response.count);
          if(response.count < 10){
            $("#change-no-of-ads").addClass("hide");
          }else{
            $("#change-no-of-ads").removeClass("hide");
          }
          if(response.per > response.count){
               $("#btn-view-more-bids").addClass("hide");
               $('[id="btn-view-more-list"]').addClass("hide");
            }else{
               $('[id="btn-view-more-list"]').removeClass("hide");
               $("#btn-view-more-bids").removeClass("hide");
            }
                grid_container.html(grid_content);
                list_container.html(list_content);
        }
            $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ad_rating_list_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

            $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });
          
          $.each(response.rows, function(index, value) {
               $("#ad_rating_grid_"+value.id).rateYo({
                     starWidth: "12px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });
           $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
             });

           $(function () {
    // Configure/customize these variables.
    var showChar = 100;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more";
    var lesstext = "Show less";
    

    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span><a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});
        loading.addClass('hide');

      }
      });
      
   }
    function sortByPrice(){
       var order = $("#order").val();
       var sort=$("#sort").val("price");
        if(order =="asc"){
          $("#order").val("desc");
         }else{
          $("#order").val("asc")
        }
       var loading = $("#search-container").find(".loading-pane");
       var grid_container = $("#sort-grid-container");
       var list_container = $("#sort-list-container");
       var grid_content = "";
       var list_content = "";
       var mobile_container = $("#mobile_ui_refresh"); 
       var mobile_content = "";
       // grid_container.html("");
       // list_container.html("");
       loading.removeClass("hide");
      var form = $("#search-form").find("form");
      $.ajax({
              type: "post",
          url: form.attr('action'),
          data: form.serialize(),
          dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', '#form-notice');
        } else {
          $.each(response.rows, function(index, value) {
                            grid_content += '<div class="col-lg-4 col-md-4 noPadding box" data-id="'+value.id+'">'+
                          '<div class="sideMargin  bottomMarginC">'+
     
     '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+                        '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                            '<div class="minPadding borderBottom nobottomPadding">'+
                              '<div class="mediumText noPadding ellipsis cursor">'+
                                  '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                   '<div class="visible-lg ellipsis">'+ value.title +'</div>'+
                                   '<div class="visible-md ellipsis">'+ value.title +'</div>'+
                                 '</a>'+
                              '</div>'+
                              '<div class="normalText grayText bottomPadding">'+ 
                  '<div class="row">'+
                    '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.name +'>'+
                      '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ value.name +'</a>'+
                    '</div>'+
                    '<div class="col-sm-6">'+
                      '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                              '<div class="normalText bottomPaddingB">'+
                                 '<div class="row">'+
                  '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.countryName +'>'+
                                 '<i class="fa fa-map-marker rightMargin"></i> ' + value.city +' , '+ value.countryName +    
                                 '</div>'+
                                 '<div class="col-sm-6">'+
                                '<span class="pull-right">'+
                                       (value.average_rate != null ? '<span id="ad_rating_grid_'+value.id+'" class="pull-left blueText"></span>' : '<span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                                       '<span class="lightgrayText pull-right normalText"> '+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';             
         
            
             grid_content += '</div></div>';

                       list_content += '<div class="sort-box-list"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">'+
                  '<div class="panel panel-default removeBorder borderZero bottomMarginLight">'+
                    '<div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+        
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImageB"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImageB" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                      '<div class="nobottomPadding lineHeight parent" style="padding-left: 130px;">'+
                      '<div class="mediumText grayText topPaddingB">'+
                        '<span class="text-capitalize">'+value.title+'</span>'+
                        '<span class="pull-right blueText mediumText">'+
                          '<strong>'+(value.price)+' '+value.currency+'</strong>'+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-6 noleftPadding">'+
      '<div class="normalText redText">';
              if(value.ads_type_id != 1){
            list_content +=  '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'"></div>';
                     } 

              list_content += '</div>'+   
                      '<div class="normalText bottomPadding parent">'+
                        '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-6 norightPadding">'+
                        '<span class="pull-right normalText">'+
                        (value.average_rate != null ? '<span id="ad_rating_list_'+value.id+'" class="pull-left blueText norightMargin norightPadding"></span>' : '<span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-12 noPadding text-justify">'+
                      '<div class="normalText topPadding bottomMarginD bottomPadding"><span class="more normalText grayText text-capitalize" style="word-break: break-word;">'+value.description+
                      '</span></div></div>'+
                        '<div class="normalText bottomPaddingB child">'+
                          '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+
                          '<span class="pull-right normalText">'+
                           '<span class="lightgrayText pull-right normalText">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                          '</span>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';
                 
             list_content +='</div> ';

             mobile_content += '<div class="panel panel-default bottomMarginLight removeBorder borderZero">'+
                      '<div class="panel-body rightPaddingB noPadding">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                    '<div class="rightPadding nobottomPadding lineHeight">'+
                    '<div class="mediumText topPadding noPadding bottomPadding">'+
                    '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                    '<div class="visible-sm ellipsis">'+value.title+'</div>'+
                    '<div class="visible-xs ellipsis">'+value.title+'</div>'+
                    '</a>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<a class="normalText lightgrayText ellipsis" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                    '</div>'+
                    '</div>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<div style="'+(value.ads_type_id == 2 ? '' : 'opacity: 0;')+'" data-countdown="'+value.ad_expiration+'"></div>'+
                    '</div>'+
                    '<div class="mediumText blueText bottomPadding">'+
                    '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                    '</div>'+
                    '<div class="normalText bottomPadding">'+
                    '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+'</div>'+
                    '<div style="padding-top:8px;">'+
                    '<span id="ads_latest_bids_mobile_'+value.id+'" class="blueText rightMargin pull-left"></span>'+
                    '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';

                
                      });
          
                grid_container.html(grid_content);
                list_container.html(list_content);
                mobile_container.html(mobile_content);
        }
        $.each(response.rows, function(index, value) {
          $("#ad_rating_list_"+value.id).rateYo({
            starWidth: "14px",
            rating:(value.average_rate != null ? value.average_rate:0),
            ratedFill: "#5da4ec",
            normalFill: "#cccccc",
            readOnly: true,
          });

        });
        $.each(response.rows, function(index, value) {
          $("#ad_rating_grid_"+value.id).rateYo({
            starWidth: "12px",
            rating:(value.average_rate != null ? value.average_rate:0),
            ratedFill: "#5da4ec",
            normalFill: "#cccccc",
            readOnly: true,
          });

        });
        $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
          });

    $(function () {
  // Configure/customize these variables.
  var showChar = 365;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more";
  var lesstext = "Show less";


    $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

    var c = content.substr(0, showChar);
    var h = content.substr(showChar, content.length - showChar);

    var html = c + ellipsestext ;

    $(this).html(html);
    }

  });
});
        loading.addClass('hide');

      }

    });
  }
    function sortByName(){
           var order = $("#order").val();
           var sort=$("#sort").val("title");
           if(order =="asc"){
            $("#order").val("desc");
           }else{
            $("#order").val("asc")
           }
           var loading = $("#search-container").find(".loading-pane");
           var grid_container = $("#sort-grid-container");
           var list_container = $("#sort-list-container");
           var grid_content = "";
           var list_content = "";
           var mobile_container = $("#mobile_ui_refresh"); 
           var mobile_content = "";
           // grid_container.html("");
           // list_container.html("");
           loading.removeClass("hide");
          var form = $("#search-form").find("form");
          $.ajax({
                  type: "post",
              url: form.attr('action'),
              data: form.serialize(),
              dataType: "json",
          success: function(response) {
            if(response.error) {
              var errors = '<ul>';

              $.each(response.error, function(index, value) {
                errors += '<li>' + value + '</li>';
                
              });

              errors += '</ul>';

              status('Please correct the following:', errors, 'alert-danger', '#form-notice');
            } else {
              $.each(response.rows, function(index, value) {
                        grid_content += '<div class="col-lg-4 col-md-4 noPadding box" data-id="'+value.id+'">'+
                          '<div class="sideMargin  bottomMarginC">'+
                          '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span><span class="label '+(value.ads_type_id != 1 ? '':'hide')+'" data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'" style="background-color: #ff6d6d; font-size: 14px; border-radius: 0; position: absolute; margin: 130px 0 0 5px;"></span>'+  
                            '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                            '<div class="minPadding borderBottom nobottomPadding">'+
                              '<div class="mediumText noPadding ellipsis cursor">'+
                                  '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                   '<div class="visible-lg ellipsis">'+ value.title +'</div>'+
                                   '<div class="visible-md ellipsis">'+ value.title +'</div>'+
                                 '</a>'+
                              '</div>'+
                              '<div class="normalText grayText bottomPadding">'+ 
                  '<div class="row">'+
                    '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.name +'>'+
                      '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ value.name +'</a>'+
                    '</div>'+
                    '<div class="col-sm-6">'+
                      '<span class="pull-right"><strong class="blueText">'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                              '<div class="normalText bottomPaddingB">'+
                                 '<div class="row">'+
                  '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.countryName +'>'+
                                 '<i class="fa fa-map-marker rightMargin"></i> ' + value.city +' , '+ value.countryName +    
                                 '</div>'+
                                 '<div class="col-sm-6">'+
                                '<span class="pull-right">'+
                                       (value.average_rate != null ? '<span id="ad_rating_grid_'+value.id+'" class="pull-left blueText"></span>' : '<span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                                       '<span class="lightgrayText pull-right normalText"> '+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';             
         
            
             grid_content += '</div></div>';

                             list_content += '<div class="sort-box-list"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">'+
                  '<div class="panel panel-default removeBorder borderZero bottomMarginLight">'+
                    '<div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+        
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImageB"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImageB" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                      '<div class="nobottomPadding lineHeight parent" style="padding-left: 130px;">'+
                      '<div class="mediumText grayText topPaddingB">'+
                        '<span class="text-capitalize">'+value.title+'</span>'+
                        '<span class="pull-right blueText mediumText">'+
                          '<strong>'+(value.price)+' '+value.currency+'</strong>'+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-6 noleftPadding">'+
      '<div class="normalText redText">';
              if(value.ads_type_id != 1){
            list_content +=  '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'"></div>';
                     } 

              list_content += '</div>'+   
                      '<div class="normalText bottomPadding parent">'+
                        '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-6 norightPadding">'+
                        '<span class="pull-right normalText">'+
                        (value.average_rate != null ? '<span id="ad_rating_list_'+value.id+'" class="pull-left blueText norightMargin norightPadding"></span>' : '<span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-12 noPadding text-justify">'+
                      '<div class="normalText topPadding bottomMarginD bottomPadding"><span class="more normalText grayText text-capitalize" style="word-break: break-word;">'+value.description+
                      '</span></div></div>'+
                        '<div class="normalText bottomPaddingB child">'+
                          '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+
                          '<span class="pull-right normalText">'+
                           '<span class="lightgrayText pull-right normalText">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                          '</span>'+
                      '</div>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';
                 
             list_content +='</div> ';

             mobile_content += '<div class="panel panel-default bottomMarginLight removeBorder borderZero">'+
                      '<div class="panel-body rightPaddingB noPadding">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                    '<div class="rightPadding nobottomPadding lineHeight">'+
                    '<div class="mediumText topPadding noPadding bottomPadding">'+
                    '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                    '<div class="visible-sm ellipsis">'+value.title+'</div>'+
                    '<div class="visible-xs ellipsis">'+value.title+'</div>'+
                    '</a>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<a class="normalText lightgrayText ellipsis" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                    '</div>'+
                    '</div>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<div style="'+(value.ads_type_id == 2 ? '' : 'opacity: 0;')+'" data-countdown="'+value.ad_expiration+'"></div>'+
                    '</div>'+
                    '<div class="mediumText blueText bottomPadding">'+
                    '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                    '</div>'+
                    '<div class="normalText bottomPadding">'+
                    '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+'</div>'+
                    '<div style="padding-top:8px;">'+
                    '<span id="ads_latest_bids_mobile_'+value.id+'" class="blueText rightMargin pull-left"></span>'+
                    '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';


                    
                          });
              
                    grid_container.html(grid_content);
                    list_container.html(list_content);
                    mobile_container.html(mobile_content);
            }
            $.each(response.rows, function(index, value) {
               $("#ad_rating_list_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

            });

            $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

            $.each(response.rows, function(index, value) {
                 $("#ad_rating_grid_"+value.id).rateYo({
                       starWidth: "12px",
                       rating:(value.average_rate != null ? value.average_rate:0),
                       ratedFill: "#5da4ec",
                       normalFill: "#cccccc",
                       readOnly: true,
                  });

            });

            $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

            $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
          });

        $(function () {
  // Configure/customize these variables.
  var showChar = 365;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more";
  var lesstext = "Show less";


    $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

    var c = content.substr(0, showChar);
    var h = content.substr(showChar, content.length - showChar);

    var html = c + ellipsestext ;

    $(this).html(html);
    }

  });
});
            loading.addClass('hide');

          }
        });
    }

    function viewMore(){
           var order = $("#order").val();
           var sort=$("#sort").val();
           var per = $("#per").val();
           var x = 0;
           var parsePer = parseInt(per);
           if($("#per").val() == 0){
            $("#per").val(3);
           }else{
            $("#per").val(parsePer+3);

           }
           // var loading = $("#search-container").find(".loading-pane");
           var grid_container = $("#sort-grid-container");
           var list_container = $("#sort-list-container");
           var grid_content = "";
           var list_content = "";
           var mobile_content = "";
           var mobile_container = $("#mobile_ui_refresh");
           var btn = $("#btn-view-more-bids");
           var btn_list = $('[id="btn-view-more-list"]');
           btn_list.html("");
                     btn_list.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
           btn.html("");
                     btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
           // grid_container.html("");
           // list_container.html("");
           // loading.removeClass("hide");
          var form = $("#search-form").find("form");
          $.ajax({
                  type: "post",
              url: form.attr('action'),
              data: form.serialize(),
              dataType: "json",
          success: function(response) {
            if(response.error) {
              var errors = '<ul>';

              $.each(response.error, function(index, value) {
                errors += '<li>' + value + '</li>';
                
              });

              errors += '</ul>';

              status('Please correct the following:', errors, 'alert-danger', '#form-notice');
            } else {
              $.each(response.rows, function(index, value) {
                console.log(value);
                 grid_content += '<div class="col-lg-4 col-md-4 noPadding box" data-id="'+value.id+'">'+
                          '<div class="sideMargin  bottomMarginC">'+
                          '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span><span class="label '+(value.ads_type_id != 1 ? '':'hide')+'" data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'" style="background-color: #ff6d6d; font-size: 14px; border-radius: 0; position: absolute; margin: 130px 0 0 5px;"></span>'+
                            '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                            '<div class="minPadding borderBottom nobottomPadding">'+
                              '<div class="mediumText noPadding ellipsis cursor">'+
                                  '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                   '<div class="visible-lg ellipsis">'+ value.title +'</div>'+
                                   '<div class="visible-md ellipsis">'+ value.title +'</div>'+
                                 '</a>'+
                              '</div>'+
                              '<div class="normalText grayText bottomPadding">'+ 
                  '<div class="row">'+
                    '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.name +'>'+
                      '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ value.name +'</a>'+
                    '</div>'+
                    '<div class="col-sm-6">'+
                      '<span class="pull-right"><strong class="blueText">'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                      '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
                              '<div class="normalText bottomPaddingB">'+
                                 '<div class="row">'+
                  '<div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title='+ value.countryName +'>'+
                                 '<i class="fa fa-map-marker rightMargin"></i> ' + value.city +' , '+ value.countryName +    
                                 '</div>'+
                                 '<div class="col-sm-6">'+
                                '<span class="pull-right">'+
                                       (value.average_rate != null ? '<span id="ad_rating_grid_'+value.id+'" class="pull-left blueText"></span>' : '<span class="lightgrayText normalText rightPadding"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                                       '<span class="lightgrayText pull-right normalText"> '+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                                '</span>'+
                                '</div>'+
                                '</div>'+
                              '</div>'+
                            '</div>';             
         
            
             grid_content += '</div></div>';

                        list_content += '<div class="sort-box-list"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">'+
                  '<div class="panel panel-default removeBorder borderZero bottomMarginLight">'+
                    '<div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">'+
                     '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+       
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImageB"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImageB" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                      '<div class="nobottomPadding lineHeight parent" style="padding-left: 130px;">'+
                      '<div class="mediumText grayText topPaddingB">'+
                        '<a class="grayText" href ="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"><span class="text-capitalize">'+value.title+'</span></a>'+
                        '<span class="pull-right blueText mediumText">'+
                          '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-6 noleftPadding">'+
      '<div class="normalText redText">';
              if(value.ads_type_id != 1){
            list_content +=  '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.id+'"></div>';
                     } 

              list_content += '</div>'+   
                      '<div class="normalText bottomPaddingB">'+
                        '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                        '</div>'+
                        '</div>'+
                        '<div class="col-sm-6 norightPadding">'+
                        '<span class="pull-right normalText">'+
                        (value.average_rate != null ? '<span id="ad_rating_list_'+value.id+'" class="pull-left blueText norightMargin norightPadding"></span>' : '<span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>')+
                        '</span>'+
                        '</div>'+
                        '<div class="col-sm-12 noPadding text-justify">'+
                      '<div class="normalText bottomPadding bottomMarginD"><span class="more normalText grayText text-capitalize" style="word-break: break-word;">'+value.description+
                      '</span></div></div>'+
                        '<div class="normalText bottomPaddingB child">'+
                          '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+
                          '<span class="pull-right normalText">'+
                           '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +'<i class="fa fa-comment leftPadding"></i></span>'+
                          '</span>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>';
                 
             list_content +='</div> ';
                    
             mobile_content += '<div class="panel panel-default bottomMarginLight removeBorder borderZero">'+
                      '<div class="panel-body rightPaddingB noPadding">'+
                    '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                    '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div></a>'+
                    '<div class="rightPadding nobottomPadding lineHeight">'+
                    '<div class="mediumText topPadding noPadding bottomPadding">'+
                    '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                    '<div class="visible-sm ellipsis">'+value.title+'</div>'+
                    '<div class="visible-xs ellipsis">'+value.title+'</div>'+
                    '</a>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<a class="normalText lightgrayText ellipsis" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'">by '+value.name+'</a>'+
                    '</div>'+
                    '</div>'+
                    '<div class="normalText redText bottomPadding">'+
                    '<div style="'+(value.ads_type_id == 2 ? '' : 'opacity: 0;')+'" data-countdown="'+value.ad_expiration+'"></div>'+
                    '</div>'+
                    '<div class="mediumText blueText bottomPadding">'+
                    '<strong>'+(value.price).toLocaleString()+' '+value.currency+'</strong>'+
                    '</div>'+
                    '<div class="normalText bottomPadding">'+
                    '<i class="fa fa-map-marker rightMargin"></i>'+value.city+','+value.countryName+'</div>'+
                    '<div style="padding-top:8px;">'+
                    '<span id="ads_latest_bids_mobile_'+value.id+'" class="blueText rightMargin pull-left"></span>'+
                    '<span class="lightgrayText pull-right">'+ value.get_advertisement_comments.length +' <i class="fa fa-comment"></i></span>'+
                    '</div>'+
                    '</div>'+
                    '</div>'+
                    '</div>';


                          });
              
                    grid_container.html(grid_content);
                    list_container.html(list_content);
                    mobile_container.html(mobile_content);
                    $("#count").val(response.count);
                    
              $("#previous-main-category").val(response.previous_main_category);
                $("#previous-main-cat-id").val(response.previous_main_cat_id);
                $("#previous-category").val(response.previous_category);
                $("#previous-category-auction").val(response.previous_category_auction);
                $("#previous-subcategory").val(response.previous_subcategory);
                $("#previous-subSubCategory").val(response.previous_subSubCategory);
                $("#previous-country").val(response.previous_country);
                $("#previous-city").val(response.previous_city);
                $("#previous-ad-type").val(response.previous_ad_type);
                $("#previous-keyword").val(response.previous_keyword);
                // $("#previous-salary-from").val(response.previous_category);
                $("#previous-salary-to").val(response.previous_salary_to);
                $("#previous-price-from").val(response.previous_price_from);
                // $("#previous-price-to").val(response.previous_category);
                // $("#previous-date-from").val(response.previous_category);
                // $("#previous-date-to").val(response.previous_category);

                    if(response.per > response.count){
                   $("#btn-view-more-bids").addClass("hide");
                   $('[id="btn-view-more-list"]').addClass("hide");
                }else{
                   $('[id="btn-view-more-list"]').removeClass("hide");
                   $("#btn-view-more-bids").removeClass("hide");
                }

                    btn.html("");
                            btn.html("View More");
                            btn_list.html("");
                            btn_list.html("View More")
            }
             $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ad_rating_list_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

            });


              $.each(response.rows, function(index, value) {
              console.log(value.average_rate);
              
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                });

          });

            $.each(response.rows, function(index, value) {
                 $("#ad_rating_grid_"+value.id).rateYo({
                       starWidth: "12px",
                       rating:(value.average_rate != null ? value.average_rate:0),
                       ratedFill: "#5da4ec",
                       normalFill: "#cccccc",
                       readOnly: true,
                  });

            });
             $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
             });

             $(function () {
  // Configure/customize these variables.
  var showChar = 365;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more";
  var lesstext = "Show less";


    $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

    var c = content.substr(0, showChar);
    var h = content.substr(showChar, content.length - showChar);

    var html = c + ellipsestext ;

    $(this).html(html);
    }

  });
});
            // loading.addClass('hide');

          }
        });
    }
$(function () {
  // Configure/customize these variables.
  var showChar = 365;  // How many characters are shown by default
  var ellipsestext = "...";
  var moretext = "Show more";
  var lesstext = "Show less";


    $('.more').each(function() {
    var content = $(this).html();

    if(content.length > showChar) {

    var c = content.substr(0, showChar);
    var h = content.substr(showChar, content.length - showChar);

    var html = c + ellipsestext ;

    $(this).html(html);
    }

  });
});

// $(document).ready(function(){
//  $('select[name="ad_type"]').change();
// });

  var search_this_country = '{{$search_country}}';
  if(search_this_country == '')
  {
    $('#client-country').val('all').change();
  }
  else
  {
    $('#client-country').val({{$search_country}}).change();
  }
    
    
    @if(Request::route()->getName() == "SEO_URL" && !isset($getads_info))
            
      var slug_cat = '{{ request()->route('parent_category_slug') }}';

    @elseif(Request::route()->getName() == "CAT_NEW_URL")

      var slug_cat = '{{ request()->route('slug') }}';

    @endif
     

    $(document).ready(function(){
      @if(Request::get('listing') == 'auctions')
        $('#ad-type').val(2).change();
      @elseif(Request::get('listing') == 'buy-and-sell')
        $('#ad-type').val(1).change();
      @else
        if(slug_cat == 'buy-and-sell')
          {
            $('#ad-type').val(1).change();
          }
       else if(slug_cat == 'auctions')
         {
           $('#ad-type').val(2).change();
         }
       else
         {
          $('#ad-type').val(4).change();
         }
      @endif
    });
  </script>
@stop
@section('content')
<div class="container-fluid bgGray">

  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding sidePaddingXs">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" id="search-form">
        <div class="panel panel-default removeBorder noMargin borderZero borderBottom col-xs-12 noPadding">
        @if(isset($direct_second_cat))
          <div id="update_search_load" class="loading-pane"><div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div></div>
        @endif
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">SEARCH OPTIONS </span>
          </div>
          {!! Form::open(array('url' => 'custom-search', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form')) !!}

          <div class="panel-body normalText">
            <input type="text" class="form-control borderZero inputBox bottomMargin" value="{{ Request::get('search') == null ? '' : Request::get('search') }}" name="keyword" id="search-keyword" placeholder="Search Value">
            <select class="form-control borderZero inputBox bottomMargin" id="ad-type" name="ad_type">
                      @foreach($ad_type as $row)
                        <option  value="{{$row->id}}" {{ $row->id == Session::get('listing_name') ? 'selected':''}}>{{$row->name == "All" ? 'Listings' : $row->name }}</option>
                      @endforeach
                    </select>
                    <select class="form-control borderZero inputBox bottomMargin" id="ads-main-category" name="main_category">
                        <option data-slug="all-categories" value="all">All Categories</option>
                      @foreach($categories as $row)
                        <option data-slug="{{$row->slug}}" value="{{$row->unique_id}}" {{Session::get('session_cat_id') == $row->unique_id ? 'selected':''}}>{{$row->name}}</option>
                      @endforeach
                    </select>
                 <!--    <select class="form-control borderZero inputBox bottomMargin {{$cat_type == '1' ? 'hide':''}}" id="ads-category-auction" name="category_auction">
                    <option value="all">All Sub Categories</option>
                    @foreach($preloaded_categories as $row)
                      <option value="{{$row->unique_id}}">{{$row->name}}</option>
                    @endforeach
                  </select> -->
            <select class="form-control borderZero inputBox bottomMargin {{$cat_type == '2' ? 'hide':''}} {{count($sub_category) == 0 ? 'hide' : ''}}" id="ads-category" name="category">
              <option value="all">All Sub Categories</option>
              @foreach($sub_category as $row)
              <option data-slug="{{$row->slug}}" value="{{$row->unique_id}}">{{$row->name}}</option>
              @endforeach
                  </select>
                  <select class="form-control borderZero inputBox bottomMargin hide" id="ads-subcategory" name="subcategory">
                <option value="all">All</option>
                  </select>
                  <select class="form-control borderZero inputBox bottomMargin hide" id="ads-subSubcategory" name="subSubCategory">
                <option value="all">All</option>
                  </select>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding">
                <select class="form-control borderZero inputBox bottomMargin" id="client-country" name="country">
                    <option data-code="all" value="all">All Countries</option>
                    @if(Auth::check())
                      @foreach($countries as $row)
                      <option data-code="{{strtolower($row->countryCode)}}" value="{{$row->id}}" {{Auth::user()->country == $row->id ? 'selected':''}}>{{$row->countryName}}</option>
                      @endforeach
                    @else
                      @foreach($countries as $row)
                      <option data-code="{{strtolower($row->countryCode)}}" value="{{$row->id}}">{{$row->countryName}}</option>
                      @endforeach
                    @endif
                </select>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding leftPadding">
                <select class="form-control borderZero inputBox bottomMargin" id="client-city" name="city">
                    <?php echo $container_city;?>
                </select>
              </div>
            </div>
            </div>
            <div class="panel-heading panelTitleBarLightB bordertopLight {{$preloaded_custom_attributes == '' ? 'hide':''}}" id="custom-attribute-pane-title">
              <span class="panelTitleSub" id="category-title">{!!$category_name!!}</span>
            </div>
            <div class="panel-body normalText {{$preloaded_custom_attributes == '' ? 'hide':''}}" id="custom-attribute-pane-container">
              <div id="custom-attributes-pane">
                {!! $preloaded_custom_attributes !!}
              </div>
            </div>
            <div class="panel-heading panelTitleBarLightB bordertopLight" id="range-title">
              @if(Session::get('range_indicator') == 1)
              <span class="panelTitleSub">SALARY RANGE</span>
              @elseif(Session::get('range_indicator') == 2)
              <span class="panelTitleSub">DATE RANGE</span>
              @elseif(Session::get('range_indicator') == 3)
              <span class="panelTitleSub">PRICE RANGE</span>
              @endif
            </div>
            <div class="panel-body normalText" id="price-range-container">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding" id="range-from-container">
                  @if(Session::get('range_indicator') == 1)
                  <input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_from" placeholder="ie. 100">
                  @elseif(Session::get('range_indicator') == 2)
                  <div class="input-group date error-price" id="date-event-date">
                                  <input type="text" name="date_from" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY">
                                  <span class="input-group-addon borderZero">
                                    <span><i class="fa fa-calendar"></i></span>
                                 </span>
                          </div>
                  @elseif(Session::get('range_indicator') == 3)
                  <input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="price_from" placeholder="ie. 100">
                  @endif
                  
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 noPadding leftPadding" id="range-to-container">
                  @if(Session::get('range_indicator') == 1)
                  <input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_to" placeholder="ie. 100">
                  @elseif(Session::get('range_indicator') == 2)
                  <div class="input-group date error-price" id="date-event-date">
                                  <input type="text" name="date_to" class="form-control borderZero" id="ads-date_of_birth" placeholder="MM/DD/YY" value="">
                                  <span class="input-group-addon borderZero">
                                    <span><i class="fa fa-calendar"></i></span>
                                 </span>
                          </div>
                  @elseif(Session::get('range_indicator') == 3)
                  <input type="number" class="form-control borderZero inputBox bottomMargin" id="ad-price_from" name="salary_to" placeholder="ie. 100">
                  @endif
                </div>
              </div>
            </div> 
            <input type="hidden" id="order" name="order" value="desc">
            <input type="hidden" id="sort" name="sort">
            <input type="hidden" id="per" value="{{$per}}" name="per">
            <input type="hidden" id="count" value="{{$count}}" name="count">

            <input type="hidden" id="update-status" value="0" name="update_status">
            <input type="hidden" id="past-category" value="{{$past_category}}" name="past_category">
            <input type="hidden" id="past-ad_type" value="{{$past_ad_type}}" name="past_ad_type">

            <input type="hidden" id="previous-main-category" name="previous_main_category">
              <input type="hidden" id="previous-ad-type" name="previous_ad_type">
              <input type="hidden" id="previous-keyword" name="previous_keyword">
            <input type="hidden" id="previous-main-cat-id" name="previous_main_cat_id">
            <input type="hidden" id="previous-category"  name="previous_category">
            <input type="hidden" id="previous-category-auction"  name="previous_category_auction">
            <input type="hidden" id="previous-subcategory"  name="previous_subcategory">
            <input type="hidden" id="previous-subSubCategory" name="previous_subSubCategory">
            <input type="hidden" id="previous-country" name="previous_country">
            <input type="hidden" id="previous-city"  name="previous_city">
            <input type="hidden" id="previous-salary-from" name="previous-salary_from">
            <input type="hidden" id="previous-salary-to"  name="previous_salary_to">
            <input type="hidden" id="previous-price-from"  name="previous_price_from">
            <input type="hidden" id="previous-price-to"  name="previous_price_to">
            <input type="hidden" id="previous-date-from"  name="previous_date_from">
            <input type="hidden" id="previous-date-to" name="previous_date_to">
          </div>
      {!! Form::close() !!}
          <div class="bottomPaddingD">
          <button class="btn blueButton borderZero noMargin fullSize" onclick ="refresh()" type="submit">Update Search</button>
        </div>  
      <span class="hidden-xs">    
        <div class="panel panel-default removeBorder bottomMarginB borderZero">
          <div class="panel-heading panelTitleBarLightB"> 
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding" id="image-left-one-container">
           <!-- <img class="fill" src="" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;"> -->
             <a href="{{$banner_placement_left_one->url}}" target="_blank">
              <img id="side_image_A" src="{{URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$banner_placement_left_one->image_desktop}}" style="height: 390px; width: 100%;">
          </a>
       </div>
      </div>
      <div class="panel panel-default removeBorder bottomMarginB borderZero">
          <div class="panel-heading panelTitleBarLightB"> 
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding" id="image-left-two-container">
             <!-- <img class="fill" src="" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;"> -->
             <a href="{{$banner_placement_left_two->url}}" target="_blank">
           <img id="side_image_B" src="{{URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$banner_placement_left_two->image_desktop}}" style="height: 390px; width: 100%;">
          </a>
       </div>
      </div>

     </span>
          
        </div>


      </div>
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        
<div class="panel panel-default borderZero removeBorder">
          <div class="panel-body noPaddingXs" id="category-image-top-container">

            <a href="{{$banner_placement_top->url}}" target="_blank">

            <img id="top_image_desktop" class="fill hidden-xs" src="{{ URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$banner_placement_top->image_desktop}}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">

            <img id="top_image_mobile" class="fill visible-xs" src="{{ URL::route('uploads', array(), false).'/banner/advertisement/mobile'.'/'}}{{$banner_placement_top->image_mobile}}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
            </a>
          </div>
          <div class="panel-footer bgWhite">
            <span class="panelRedTitle">ADVERTISEMENT</span>
            <span class="pull-right normalText"><a class="redText" href="{{url('support').'#faq'}}">view rates</a></span>
          </div>
        </div>
        
<div class="blockBottom" id="search-container">
    <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
     </div>     
  <div class="panel-heading panelTitleBarLightB">
    <span class="panelBlueTitle">
        <span id="breadcrumb" class="normalText">
          Search Result for <span class="redText" id="searched_keyword"> "{{ Session::get('keyword') }}"</span>  
        </span>
        <span id="breadcrumb-updated" class="normalText hide">
        </span>
        <span class="pull-right" id="sort-param">
<!--          <button class="bgWhite removeBorder borderZero normalText grayText dropdown-toggle hidden-sm  hidden-xs" type="" data-toggle="dropdown">Sort by
            <span class="caret"></span>
          </button> --> 
      <span class="pull-right" id="sort-param">
<!--          <button class="bgWhite removeBorder borderZero normalText grayText dropdown-toggle hidden-sm  hidden-xs" type="" data-toggle="dropdown">Sort by
            <span class="caret"></span>
          </button> --> 
          <ul class="list-inline sortDropdown">
            <li class="hidden-xs hidden-sm rightPadding grayText {{$count < 10 ? 'hide':''}} {{ count($rows) == 0 ? 'hide' : '' }}" id="change-no-of-ads">
          <span class="grayText normalWeight rightPadding hidden-xs">No of Ads</span>
          <select name="no_of_ads" id="row-no_of_ads" class="grayText cursor noBg form-contro noPadding normalWeight hidden-xs" onchange="maxAds()">
            <option class="hide" value="">Select</option>
            <option value="9">9</option>
            <option value="15">15</option>
            <option value="24">24</option>
            <option value="30">30</option>
            <option value="1000">debug</option>
          </select>
    </li>
            <li class="hidden-xs hidden-sm dropdown {{ count($rows) == 0 ? 'hide' : '' }}">
          <button class="dropdown-toggle grayText cursor noBg noPadding removeBorder normalWeight" data-toggle="dropdown"><span id="bidding-count-container">Sort by </span> <span class="caret"></span></button>
<!--           <div href="#" class="dropdown-toggle grayText cursor noBg noPadding" data-toggle="dropdown"><span id="bidding-count-container">Sort by </span> <span class="caret"></span></div> -->
          <ul class="dropdown-menu sortDrop">
            <li><a href="#" onclick="sortByName()">AD Name</a></li>
      <li><a href="#" onclick="sortByPrice()">Price</a></li>
          </ul>
        </li>
            <li class="hidden-xs hidden-sm {{ count($rows) == 0 ? 'hide' : '' }}"><button id="change-result-view" type="" class="bgWhite removeBorder borderZero normalText grayText hidden-sm normalWeight">List by  <i class="leftPadding fa fa-th"></i></button></li>
          </ul>
      </span>
    </span>        
  </div>
  
<div class="visible-xs visible-sm">
<div id="mobile_ui_refresh">
  @forelse($rows as $row)
  <!-- <div class="panel panel-default bottomMarginLight removeBorder borderZero">
  <div class="panel-body rightPaddingB noPadding">
  <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
  <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"> <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
  </div></a>

  <div class="rightPadding nobottomPadding lineHeight">
  <div class="mediumText topPadding noPadding bottomPadding">
  <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
      <div class="visible-sm ellipsis"><?php echo substr($row->title, 0, 37); 
         if(strlen($row->title) >= 37){echo "..";} ?>
      </div>
      <div class="visible-xs ellipsis"><?php echo substr($row->title, 0, 17);
         if(strlen($row->title) >= 37){echo "..";} ?>
      </div>
  </a>
  <div class="normalText redText bottomPadding ellipsis">
       <a class="normalText lightgrayText ellipsis" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
  </div>
  </div>
  <div class="normalText redText bottomPadding">
  <div data-countdown="{{$row->ad_expiration}}"></div>
  </div>
  <div class="mediumText blueText bottomPadding">
  <strong>{{ number_format($row->price) }} USD</strong>
  </div>
  <div class="normalText bottomPadding">
  @if($row->city || $row->countryName)
  <i class="fa fa-map-marker rightMargin"></i>
  {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
  @else
  <i class="fa fa-map-marker rightMargin"></i> not available
  @endif
  </div>
  <div style="padding-top:8px;">
    @if($row->average_rate != null)
     <span id="ads_latest_bids_mobile_{{$row->id}}" class="blueText rightMargin pull-left"></span>
     <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
   @else
     <span class="blueText rightMargin"><span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span></span>
     <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
    @endif 

  </div>
  </div>
  </div>
  <div class="clearfix"></div>
  </div> -->
  @empty
  <h3 class="text-center bgWhite noMargin topPaddingC bottomPaddingC">No Result(s) Found</h3>
  @endforelse
  </div>
  
  <div class="text-center topPaddingB bgWhite viewMoreList hide">
        <button class="btn blueButton borderZero" onclick="viewMore()" type="button" id="btn-view-more-list" style="width:100%;"><i class=""></i>View More</button>
    </div>
</div>

<div id="grid-view">
  <div class="panel panel-body hidden-xs hidden-sm nobottomPadding">
    <div id="sort-grid-container">
    @forelse ($rows as $row)
      <!-- <div class="sort-box-grid">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding box" id="grid-result">
          <div class="sideMargin bottomMarginC">
          <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
          @foreach($row->getAdvertisementPhoto as $photo_info)
          @if($photo_info->primary == 1)          
          <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$photo_info->photo }}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
            </div>
          </a>
          @endif
          @endforeach
          <div class="minPadding borderBottom nobottomPadding">
            <div class="row">
              <div class="col-sm-12 ellipsis cursor">
                  <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                    <?php echo substr($row->title, 0, 35); 
                      if(strlen($row->title) >= 37){echo "..";} ?>
                  </a>
                </div>
              </div>
          <div class="normalText grayText bottomPadding">
            <div class="row">
              <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
              </div>
              <div class="col-sm-6">
                <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                </span>
              </div>
            </div>
          </div>
          <div class="normalText bottomPaddingB">
            <div class="row">
              <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                @if($row->city || $row->countryName)
                <i class="fa fa-map-marker rightMargin"></i>
                {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                @else
                <i class="fa fa-map-marker rightMargin"></i> not available
                @endif
              </div>
              <div class="col-sm-6">
                <span class="pull-right">
                @if($row->average_rate != null)
                <span id="ad_rating_grid_{{$row->id}}" class="pull-left blueText"></span>
                <span class="lightgrayText">{{count($row->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                @else
                <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                <span class="lightgrayText pull-right normalText">{{count($row->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                @endif 
                </span>
              </div>
            </div>
          </div>
          </div>
          @if($row->ads_type_id != 1)
            <div class="sideMargin minPadding text-center nobottomPadding redText normalText bottomMarginB">       
                      <div data-countdown="{{$row->ad_expiration}}" data-currenttime="{{ $current_time}}" data-ads_id="{{ $row->get_ads_id }}"></div>
                    </div>
               @endif
          </div>
        </div>
      </div> -->
      @empty
      <h3 class="text-center noPadding bottomPaddingC">No Result(s) Found</h3>
      @endforelse
    </div>
    <div class="clearfix"></div>
    <div class="text-center hide" style="margin-left:-15px;margin-right:-15px;">
            <button class="btn blueButton borderZero" onclick="viewMore()" type="button" id="btn-view-more-bids" style="width:100%;"><i class=""></i>View More</button>
        </div>
  </div>
</div>
<div id="list-view" class="hide clearfix hidden-sm hidden-xs">
  <div id="sort-list-container">
  @forelse ($rows as $row)
    <!-- <div class="sort-box-list">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="panel panel-default removeBorder borderZero bottomMarginLight">
        <div class="panel-body notopPadding noleftPadding nobottomPadding rightPaddingB">
        <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">hahahahhahhaahahh</span>
        @foreach($row->getAdvertisementPhoto as $photo_info)
        @if($photo_info->primary == 1)
        <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$photo_info->photo }}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        </a>
        @endif
        @endforeach
          <div class="parent nobottomPadding lineHeight" style="padding-left: 130px;">
            <div class="mediumText grayText topPaddingB">
            <span class="text-capitalize">{{$row->title}}</span>
            <span class="pull-right blueText mediumText">
              <strong>{{$row->price}} USD</strong>
            </span>
            </div>            
            <div class="col-sm-6 noleftPadding">
            @if($row->ads_type_id != 1)
      <div class="normalText redText">
              <div data-countdown="{{$row->ad_expiration}}" data-currenttime="{{ $current_time}}" data-ads_id="{{ $row->get_ads_id }}"></div>
            </div>
         @endif
          <div class="normalText bottomPadding">
            <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}">by {{$row->name}}</a>
            </div>
            </div>
            <div class="col-sm-6 norightPadding">
            <span class="pull-right normalText">
            @if($row->average_rate != null)
            <span id="ad_rating_list_{{$row->id}}" class="blueText norightPadding"></span>
            @else
            <span class="lightgrayText mediumText"><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span><span class="glyphicon glyphicon-star"></span></span>
            @endif 
            </span>
            </div>
          <div class="col-sm-12 noPadding">
          <div class="normalText topPadding bottomMarginD bottomPadding adDesciption text-justify">
           <span class="more normalText grayText text-capitalize lineHeightB">
        {{$row->description}}
      </span>
          </div>
          </div>
      <div class="normalText bottomPaddingB child">
              @if($row->city || $row->countryName)
              <i class="fa fa-map-marker rightMargin"></i>
              {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
              @else
              <i class="fa fa-map-marker rightMargin"></i> not available
              @endif
              <span class="pull-right normalText">
                @if($row->average_rate != null)
                <span class="lightgrayText pull-right normalText">{{count($row->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                @else
                <span class="lightgrayText pull-right normalText">{{count($row->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                @endif 
              </span>
          </div>
          </div>

        </div>
      </div>
    </div>
    </div>  -->
  @empty
  <h3 class="text-center noPadding bottomPaddingC">No Result(s) Found</h3>
  @endforelse
  </div>
  <div class="clearfix"></div>
  <div class="text-center">
        <button class="btn blueButton borderZero {{$count<=9 ? 'hide':''}}" onclick="viewMore()" type="button" id="btn-view-more-list" style="width:100%;"><i class=""></i>View More</button>
    </div>
</div>
</div>
        <div class="panel panel-default removeBorder borderZero ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
            <span class="pull-right normalText"><a class="redText" href="{{url('support').'#faq'}}">view rates</a></span>
          </div>
          <div class="panel-body noPaddingXs" id="image-bottom-container">
              <a href="{{$banner_placement_bottom->url}}" target="_blank">
              <img id="bottom_image_desktop" class="fill hidden-xs" src="{{ URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$banner_placement_bottom->image_desktop}}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">

              <img id="bottom_image_mobile" class="fill visible-xs" src="{{ URL::route('uploads', array(), false).'/banner/advertisement/mobile'.'/'}}{{$banner_placement_bottom->image_mobile}}" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
            </a>
          </div>
        </div>

{{-- <span class="visible-xs">    
         <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
                <a href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}"> 
                  <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div>
              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a>
                    <div class="normalText bottomPadding bottomMargin">
                          <a class="normalText lightgrayText" href="{{ url('/') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                    <div class="minPadding nobottomPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{count($featured_ad->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{count($featured_ad->getAdvertisementComments)}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
     </span> --}}
        
        </div>
        
        
      </div>
    </div>
  </div>
</div>

@stop