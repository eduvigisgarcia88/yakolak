@extends('layout.frontend')
@section('scripts')
<script>
  $("#nav div a[href^='#']").on('click', function(e) {
   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').animate({
       scrollTop: $(hash).offset().top - 100
     }, 1000, function(){

       // when done, add hash to url
       // (default click behaviour)
       window.location.hash = hash;
     });

}); 

    $("#content-wrapper").on("click", ".btn-plan-subscription-upgrade", function() {
        var id = $(this).data('id');
        var title = $(this).data('title');
        var plan_id = $(this).data('plan');
        $("#row-id").val("");
     	$("#row-id").val(id);
        $("#row-title").val(title);
        $("#row-plan_id").val(plan_id);
        $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
        $("#modal-form").find('form').trigger("reset");
        $(".modal-title").html('<strong> Upgrade to '+ title +' </strong>', title);
        $("#modal-plan-subscription").modal('show');    
    });
	 var url = window.location.href;
	 var hash = url.substring(url.indexOf('#'));

	 if(hash == "#contactus"){
	 	$("#contact-us-link").click();
	 }else if(hash== "#aboutus"){
	 	 $("#company-link").click();
	 }else if(hash== "#callus"){
	 	 $("#call-us-link").click();
	 }else if(hash== "#termAndconditions"){
	 	 $("#terms-and-conditions-link").click();
	 }else if(hash== "#faq"){
	 	 $("#faq-link").click();
	 }else if(hash== "#helpcenter"){
	 	 $("#helpcenter-link").click();
	 }else if(hash== "#advertising"){
	 	 $("#advertise-link").click();
	 }else if(hash== "#missionandvision"){
	 	 $("#missionandvision-link").click();
	 }else if(hash== "#sitemap"){
	 	 $("#sitemap-link").click();
	 }else if(url){
	 	 $("#contact-us-link").click();
	 }


  $("#contact-us").click(function() {
      $("#contact-us-link").click();
  });
  $("#companyinfos").click(function() {
     $("#company-link").click();
  });
  $("#call-us").click(function() {
   	 $("#call-us-link").click();
  });
   $("#terms-and-conditions").click(function() {
   	 $("#terms-and-conditions-link").click();
  });
  $("#faq").click(function() {
   	 $("#faq-link").click();
  });
  $("#helpcenter").click(function() {
   	 $("#helpcenter-link").click();
  });
  $("#advertising").click(function() {
   	 $("#advertise-link").click();
  });

$(document).ready(function() {
$('a[role="tab"]').click(function () {
    $('li').removeClass("active");
    $(this).addClass("active");
    console.log("Working"); 
});
});

$('#contact_yakolak').submit(function(e){
	e.preventDefault();
	$.ajax({
		type: "POST",
		url: "{{url('contact/yakolak')}}",
		data: new FormData(this),
		processData: false,
        contentType: false,
        beforeSend: function(){
        	$('div.validation-messages').remove();
        },
		success: function(response){
			if(response.code == 0){
				$('#validation-errors').prepend("<div class='validation-messages alert alert-danger alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> </div>");
				 $.each(response.errors, function(i, v){
                  $('div.validation-messages').append('<p class="errors_p">'+v+'</p>');
                });
			}
			else
			{
				// $('#validation-errors').prepend("<div class='validation-messages alert alert-success alert-dismissible'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> Message succesfully sent! Thank you.</div>");
				status('Message success', 'Thank you for your response.', 'alert-success');
				setTimeout(function(){location.reload();},1500)
			}
		}
	});
});

</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
	<div class="container bannerJumbutron nobottomMargin">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
			<div class="col-sm-3">
				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">SUPPORT</span>
					</div>
					<div id="supportTab" class="panel-body normalText">
						<ul class="nav nav-pills nav-stacked noPadding" role="tablist" id="supportLinks">
							<li role="presentation contactus"><a href="#contactUs" id="contact-us-link" aria-controls="contactUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-user rightPadding"></i>  Contact us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#helpCenter" id="helpcenter-link" aria-controls="helpCenter" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question rightPadding"></i>  Help Center <i class="fa fa-angle-right pull-right"></i></a></li>
							<!-- <li role="presentation"><a href="#aboutUs" id="about-us-link" aria-controls="aboutUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-info rightPadding"></i>  About us<i class="fa fa-angle-right pull-right"></i></a></li> -->
							<li role="presentation"><a href="#callUs" id="call-us-link" aria-controls="callUs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-phone rightPadding"> </i> Call us<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#termsandCondition" id="terms-and-conditions-link" aria-controls="termsandCondition" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-file-text-o rightPadding"></i> Terms and Conditions<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#faqs" id="faq-link" aria-controls="faqs" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-question-circle rightPadding"></i> FAQ's<i class="fa fa-angle-right pull-right"></i></a></li>

<!-- 							<li role="presentation"><a href="#advertise" id="advertise-link" aria-controls="advertise" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-volume-up rightPadding"></i> Advertising<i class="fa fa-angle-right pull-right"></i></a></li> -->
							<!-- <li role="presentation"><a href="#advertising" id="advertising-link" aria-controls="advertising" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-volume-up rightPadding"></i> Advertising<i class="fa fa-angle-right pull-right"></i></a></li> -->
						</ul>
					</div>
				</div>

				<div class="panel panel-default removeBorder borderZero borderBottom">
					<div class="panel-heading panelTitleBarLight">
						<span class="panelTitle">ABOUT</span>
					</div>
					<div id="supportTab" class="panel-body normalText">
						<ul class="nav nav-pills nav-stacked noPadding" role="tablist" id="supportLinks">
							<li role="presentation contactus"><a href="#companyinformation" id="company-link" aria-controls="companyinformation" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-info rightPadding"></i>  Company Information<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#missionandvision" id="missionandvision-link" aria-controls="missionandvision" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-eye rightPadding"></i>  Mission and Vision<i class="fa fa-angle-right pull-right"></i></a></li>
							<li role="presentation"><a href="#sitemap" id="sitemap-link" aria-controls="sitemap" role="tab" data-toggle="tab" class="borderbottomLight grayText"><i class="fa fa-sitemap rightPadding"></i>  Sitemap<i class="fa fa-angle-right pull-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-9 noPadding sideBlock">
				<div class="tab-content noPadding">
					<div role="tabpanel" class="tab-pane active" id="contactUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Contact Us</span>
							</div>
							<div class="panel-body">
							<div id="validation-errors">
							</div>
								<p>
								{!! str_replace('  ', ' &nbsp;', nl2br(htmlentities($contact_details->description))); !!}
								</p>
								<iframe src="https://www.google.com/maps?q={{ $contact_details->address }}&output=embed" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								<hr>
								<form class="borderbottomLight" id="contact_yakolak">
								{{ csrf_field() }}
								<div class="form-group error-name">
								   <label for="email" class="grayText normalText">Name</label>
								   <input type="text" name="name" class="form-control borderZero inputBox" id="usr">
								   <sup class="sup-errors redText"></sup>
								 </div>
								<div class="form-group">
								   <label for="email" class="grayText normalText">Email</label>
								   <input type="email" name="email" class="form-control borderZero inputBox" id="usr">
								 </div>
								<div class="form-group">
								   <label for="email" class="grayText normalText">Message</label>
								   <textarea name="message" class="form-control borderZero inputBox" rows="5" id="comment"></textarea>
								 </div>
								 <div class="form-group">
								   <label for="email" class="grayText normalText">Security</label>
								    {!! app('captcha')->display(); !!}

<!-- 					             	<div class="g-recaptcha" data-theme="light" data-sitekey="{{Session::get('siteKey')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div> -->
								  </div>
								   <div class="form-group pull-right topPaddingB noMargin">
								  <button type="submit" class="btn blueButton borderZero">Submit</button>
								  </div>
								</form>
							</div>
						</div>
					</div>
					

					<div role="tabpanel" class="tab-pane" id="helpCenter">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Help Center</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
										{!! html_entity_decode($page_help_center->content)  !!}

								</div>
							</div>
						</div>
					</div>
					
					<div role="tabpanel" class="tab-pane" id="callUs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Call us</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									{!! html_entity_decode($page_call_us->content) !!}
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="termsandCondition">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Terms and Conditions</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
										{!! html_entity_decode($page_terms_and_conditions->content) !!}
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="faqs">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">FAQ's</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
										{!! html_entity_decode($page_faq->content) !!}
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="advertising">
						<div class="panel panel-default removeBorder borderZero borderBottom " id="content-wrapper">
							<div id="nav" data-spy="scroll" data-target=".navbar" data-offset="10">


							
							</div>
						</div>
					</div>


					<div role="tabpanel" class="tab-pane" id="companyinformation">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Company Information</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									{!! html_entity_decode($page_company_information->content) !!}
								</div>
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="missionandvision">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Mission and Vision</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
								   {!! html_entity_decode($page_mission_and_vision->content) !!}
								</div>
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane" id="sitemap">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">Sitemap</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									{!! html_entity_decode($page_sitemap->content) !!}
								</div>
							</div>
						</div>
					</div>
	
<!-- 					<div role="tabpanel" class="tab-pane" id="advertise">
						<div class="panel panel-default removeBorder borderZero borderBottom ">
							<div class="panel-heading panelTitleBar">
								<span class="panelTitle">advertise</span>
							</div>
							<div class="panel-body">
								<div class="normalText grayText">
									<?php echo $bannerRates; ?>
								</div>
							</div>
						</div>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</div>
@stop
