<!-- modal add user -->
<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/user/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD USER</h4>
              </div>
              <div class="modal-body">
                <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#basic-info" id="basic-tab">Basic Information</a></li>
                  <li><a data-toggle="tab" href="#additional-info" id="additional-tab">Additional Information</a></li>
                  <li><a data-toggle="tab" href="#company-branch-info" id="company-tab">Company Branch Information</a></li>
                </ul>
               <div id="form-notice"></div>
               <div class="tab-content">
                  <div id="basic-info" class="tab-pane fade in active">
                 <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                    <img src="#" id="row-photo" height="100" width="100">
                    <button type="button" class="btn btn-primary pull-right btn-change"><i class="fa fa-camera"></i> Change Photo</button>
                    <button type="button" class="btn btn-primary pull-right btn-cancel">Cancel</button>
                    <div class="uploader-pane">
                        <div class='change'>
                            <input name='photo' id='row-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo'>
                        </div>
                     </div>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="name" class="form-control disable-view" id="row-name" maxlength="30" placeholder="Enter your last name">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="email" name="email" class="form-control disable-view" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-usertype_id" class="col-sm-3 col-xs-5 font-color">User Type</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control disable-view" id="row-usertype_id" name="usertype_id">
                      <option class="hide">Select</option>
                      @foreach($usertypes as $row)
                      <option value="{{ $row->id}}">{{ strtoupper($row->type_name)}}</option>
                      @endforeach
                  </select>
                </div>
                </div>
                  </div>
                  <div id="additional-info" class="tab-pane fade">
                   <div class="form-group">
                        <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Country</label>
                    <div class="col-sm-9 col-xs-7">
                       <select class="form-control disable-view" id="row-country" name="country">
                          <option class="hide">Select</option>
                          @foreach($country as $row)
                          <option value="{{ $row->id}}">{{ $row->countryName }}</option>
                          @endforeach
                      </select>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">City</label>
                    <div class="col-sm-9 col-xs-7">
                        <select class="form-control disable-view" id="row-city" name="city">

                        </select>
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Address</label>
                    <div class="col-sm-9 col-xs-7">
                      <input type="text" name="address" class="form-control disable-view" id="row-address" maxlength="30" placeholder="Enter your contact number">
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="row-mobile" class="col-sm-3 col-xs-5 font-color">Contact Number</label>
                    <div class="col-sm-9 col-xs-7">
                      <input type="text" name="telephone" class="form-control disable-view" id="row-telephone" maxlength="30" placeholder="Enter your contact number">
                    </div>
                    </div>
                    <div class="form-group">
                        <label for="row-website" class="col-sm-3 col-xs-5 font-color">Website</label>
                    <div class="col-sm-9 col-xs-7">
                      <input type="url" name="website" class="form-control disable-view" id="row-website" maxlength="30" placeholder="Enter your contact number">
                    </div>
                    </div>
                  </div>
                  <div id="company-branch-info" class="tab-pane fade">
                      <div id="company-branch-container">
                          <div id ="accountBranch"><div class="col-lg-12  borderbottomLight normalText noPadding bottomPadding topMarginB bottomMargin redText">
                                BRANCH INFORMATION
                                <span class="pull-right normalText"><button type="button" class="hide-view btn-add-account-branch pull-right borderZero inputBox noBackground"><i class="fa fa-plus redText"></i> ADD BRANCH</button></span>
                              </div>
                              <div class="col-lg-6 ">
                               <div class="form-group bottomMargin">
                                 <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Telephone No.</label>
                                  <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                      <input name="company_tel_no[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                   </div>
                                  </div>
                               </div>
                                <div class="form-group bottomMargin">
                                <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Country</label>
                                  <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                      <select id="account-company_country" name="company_country[]" class="form-control borderZero inputBox fullSize">
                                      <option class="hide">Select</option>
                                        @foreach($country as $row)
                                          <option value="{{$row->id}}">{{$row->countryName}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group bottomMargin">
                                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Address</label>
                                  <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                         <input name="company_address[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group bottomMargin">
                                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Office Hours</label>
                                  <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                          <input name="company_office_hours[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                    </div>
                                  </div>
                                </div>
                            </div>
                           <div class="col-lg-6 ">
                                <div class="form-group bottomMargin">
                                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Email Address</label>
                                  <div class="inputGroupContainer">
                                   <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                         <input name="company_email[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group bottomMargin">
                                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Cellphone</label>
                                  <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                         <input name="company_mobile[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                    </div>
                                  </div>
                                </div>
                                 <div class="form-group bottomMargin">
                                  <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">City or State</label>
                                 <div class="inputGroupContainer">
                                    <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                      <select id="account-company_city" name="company_city[]" class="form-control borderZero inputBox fullSize">
                                  
                                     </select>
                                  </div>
                                  </div>
                                </div>
                                 <div class="form-group bottomMargin">
                                 <label class="col-md-4 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Website</label> 
                                   <div class="inputGroupContainer">
                                     <div class="input-group col-md-8 col-sm-12 col-xs-12 ">
                                           <input name="company_website[]" placeholder="Input" value="" class="form-control borderZero inputBox fullSize" type="text">
                                     </div>
                                   </div>
                                 </div>
                                <input type="hidden" name="company_branch_id[]" id="branch_id">
                                <input type="hidden" name="company_branch_status[]" id="branch_status">
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
               
              </div>
              <div class="clearfix"></div>
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>