@extends('layout.master')
@section('scripts')

<script type="text/javascript">
   $_token = '{{ csrf_token() }}';

function activate_tooltip(){
  $(document).find('[data-toggle="tooltip"]').tooltip();
}



function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val(); 
      $order = $("#row-order").val();
      // $sort = $("#front-end-users-table").data('sort');
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, usertype_id:$usertype_id,order: $order, search: $search, status: $status, country:$country ,per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class= "'+(row.status == 3 ? 'blockedColor': '')+''+(row.status == 2 ? 'disabledColor': '')+'">' + 
              '<td><small><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a>' + (row.status == 1 ? 'Active'  : ''  || row.status == 2 ? 'Disabled'  : '' || row.status == 3 ? 'Blocked'  : '' ) + '</small></td>' +
              '<td><small><center>' + row.type_name + '</small></center></td>' +
              '<td><small><center>' + row.plan_name + '</small></center></td>' +
              '<td><small>' + row.username + '</small></td>' +
              '<td><small>' + row.email + '</small></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/all') }}/'+ row.id +'" target="_blank">' + row.ad_listings + '</a></small></center></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/flagged') }}/'+ row.id +'" target="_blank">' + row.ad_flagged + '</a></small></center></td>' +
              '<td><small>' + row.created_at + '</small></td>' +
              '<td><small>' + (row.last_login == null ? 'N/A': row.last_login) + '</small></td>' +
              '<td class="rightalign">'+
                 '<button data-toggle="tooltip" title="Delete" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +

                 (row.status !=3 ? '<button data-toggle="tooltip" title="Block" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="modal" data-target="#"><i class="fa fa-shield "></i></button>':'<button data-toggle="tooltip" title="Unblock" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="modal" data-target="#"><i class="fa fa-shirtsinbulk "></i></button>') +

                 '<button data-toggle="tooltip" title="Verify" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" '+(row.status == 3 ? 'disabled':'')+' '+(row.status == 1 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-paper-plane"></i></button>'+

                 (row.status !=2? '<button data-toggle="tooltip" title="Disable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" '+(row.status == 3 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>':'<button data-toggle="tooltip" title="Enable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>') +

                 '<a href="{{ url("/") }}' + '/' + (row.usertype_id == 1 ? 'vendor' : 'company') + '/' + row.alias + '"  data-toggle="tooltip" title="View" target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right"><i class="fa fa-eye"></i></a>' +
                 '<button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>' +
        '</td></tr>';
            

        });
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        activate_tooltip();
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        $("#row-order").val(response.order);
        loading.addClass("hide");

      }, 'json');
    } 
    function maxAds() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $sort = $("#front-end-users-table").find("#row-sort").data('sort');
      $order = $("#row-order").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, usertype_id:$usertype_id,order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
         $.each(response.rows.data, function(index, row) {
          console.log(row.id);
            body += '<tr data-id="' + row.id + '" class= "'+(row.status == 3 ? 'blockedColor': '')+''+(row.status == 2 ? 'disabledColor': '')+'">' + 
              '<td><small><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a>' + (row.status == 1 ? 'Active'  : ''  || row.status == 2 ? 'Disabled'  : '' || row.status == 3 ? 'Blocked'  : '' ) + '</small></td>' +
              '<td><small><center>' + row.type_name + '</small></center></td>' +
              '<td><small><center>' + row.plan_name + '</small></center></td>' +
              '<td><small>' + row.username + '</small></td>' +
              '<td><small>' + row.email + '</small></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/all') }}/'+ row.id +'" target="_blank">' + row.ad_listings + '</a></small></center></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/flagged') }}/'+ row.id +'" target="_blank">' + row.ad_flagged + '</a></small></center></td>' +
              '<td><small>' + row.created_at + '</small></td>' +
              '<td><small>' + (row.last_login == null ? 'N/A': row.last_login) + '</small></td>' +
              '<td class="rightalign">'+

                 '<button data-toggle="tooltip" title="Delete" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                 
                (row.status !=3 ? '<button data-toggle="tooltip" title="Block" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="modal" data-target="#"><i class="fa fa-shield "></i></button>':'<button data-toggle="tooltip" title="Unblock" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="modal" data-target="#"><i class="fa fa-shirtsinbulk "></i></button>') +


               '<button data-toggle="tooltip" title="Verify" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" '+(row.status == 3 ? 'disabled':'')+' '+(row.status == 1 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-paper-plane"></i></button>'+

                (row.status !=2? '<button data-toggle="tooltip" title="Disable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" '+(row.status == 3 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>':'<button data-toggle="tooltip" title="Enable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>') +

                 '<a href="{{ url("/") }}' + '/' + (row.usertype_id == 1 ? 'vendor' : 'company') + '/' + row.alias + '"  data-toggle="tooltip" title="View" target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right"><i class="fa fa-eye"></i></a>' +
                 '<button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>' +
        '</td></tr>';
            

        });
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        activate_tooltip();
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        $("#row-order").val(response.order);
        loading.addClass("hide");

      }, 'json');
      
    } 

    function search() {
      
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { usertype_id:$usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
      table.html("");
      
      var body = "";
      console.log(response);

         $.each(response.rows.data, function(index, row) {
          console.log(row.id);
           body += '<tr data-id="' + row.id + '" class= "'+(row.status == 3 ? 'blockedColor': '')+''+(row.status == 2 ? 'disabledColor': '')+'">' + 
              '<td><small><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a>' + (row.status == 1 ? 'Active'  : ''  || row.status == 2 ? 'Disabled'  : '' || row.status == 3 ? 'Blocked'  : '' ) + '</small></td>' +
              '<td><small><center>' + row.type_name + '</small></center></td>' +
              '<td><small><center>' + row.plan_name + '</small></center></td>' +
              '<td><small>' + row.username + '</small></td>' +
              '<td><small>' + row.email + '</small></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/all') }}/'+ row.id +'" target="_blank">' + row.ad_listings + '</a></small></center></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/flagged') }}/'+ row.id +'" target="_blank">' + row.ad_flagged + '</a></small></center></td>' +
              '<td><small>' + row.created_at + '</small></td>' +
              '<td><small>' + (row.last_login == null ? 'N/A': row.last_login) + '</small></td>' +
              '<td class="rightalign">'+
                 '<button data-toggle="tooltip" title="Delete" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +

                (row.status !=3 ? '<button data-toggle="tooltip" title="Block" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="modal" data-target="#"><i class="fa fa-shield "></i></button>':'<button data-toggle="tooltip" title="Unblock" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="modal" data-target="#"><i class="fa fa-shirtsinbulk "></i></button>') +

                '<button data-toggle="tooltip" title="Verify" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" '+(row.status == 3 ? 'disabled':'')+' '+(row.status == 1 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-paper-plane"></i></button>'+

                  (row.status !=2? '<button data-toggle="tooltip" title="Disable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" '+(row.status == 3 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>':'<button data-toggle="tooltip" title="Enable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>') +

                 '<a href="{{ url("/") }}' + '/' + (row.usertype_id == 1 ? 'vendor' : 'company') + '/' + row.alias + '"  data-toggle="tooltip" title="View" target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right"><i class="fa fa-eye"></i></a>' +
                 '<button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>' +
        '</td></tr>';
            

        });
      
      table.html(body);
      activate_tooltip();
      $('#row-pages').html(response.pages);
      $("#row-order").val(response.order);
      loading.addClass("hide");

      }, 'json');
    
    }

     $(".content-wrapper").on('click', '.pagination a', function (event) {
           event.preventDefault();
        if ( $(this).attr('href') != '#' ) {
          $("html, body").animate({ scrollTop: 0 }, "fast");
          $("#row-page").val($(this).html());

          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort = $("#row-sort").val();
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $per = $("#row-per").val();
          $usertype_id = $("#row-filter_usertype_id").val();

          var loading = $(".loading-pane");
          var table = $("#rows");

          $.post("{{ $refresh_route }}", { usertype_id:$usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          table.html("");
          
          var body = "";
          console.log(response);

            $.each(response.rows.data, function(index, row) {
          console.log(row.id);
            body += '<tr data-id="' + row.id + '" class= "'+(row.status == 3 ? 'blockedColor': '')+''+(row.status == 2 ? 'disabledColor': '')+'">' + 
              '<td><small><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a>' + (row.status == 1 ? 'Active'  : ''  || row.status == 2 ? 'Disabled'  : '' || row.status == 3 ? 'Blocked'  : '' ) + '</small></td>' +
              '<td><small><center>' + row.type_name + '</small></center></td>' +
              '<td><small><center>' + row.plan_name + '</small></center></td>' +
              '<td><small>' + row.username + '</small></td>' +
              '<td><small>' + row.email + '</small></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/all') }}/'+ row.id +'" target="_blank">' + row.ad_listings + '</a></small></center></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/flagged') }}/'+ row.id +'" target="_blank">' + row.ad_flagged + '</a></small></center></td>' +
              '<td><small>' + row.created_at + '</small></td>' +
              '<td><small>' + (row.last_login == null ? 'N/A': row.last_login) + '</small></td>' +
              '<td class="rightalign">'+
                 '<button data-toggle="tooltip" title="Delete" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                (row.status !=3 ? '<button data-toggle="tooltip" title="Block" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="modal" data-target="#"><i class="fa fa-shield "></i></button>':'<button data-toggle="tooltip" title="Unblock" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="modal" data-target="#"><i class="fa fa-shirtsinbulk "></i></button>') +

                 '<button data-toggle="tooltip" title="Verify" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" '+(row.status == 3 ? 'disabled':'')+' '+(row.status == 1 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-paper-plane"></i></button>'+

                (row.status !=2? '<button data-toggle="tooltip" title="Disable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" '+(row.status == 3 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>':'<button data-toggle="tooltip" title="Enable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>') +

                 '<a href="{{ url("/") }}' + '/' + (row.usertype_id == 1 ? 'vendor' : 'company') + '/' + row.alias + '"  data-toggle="tooltip" title="View" target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right"><i class="fa fa-eye"></i></a>' +
                 '<button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>' +
        '</td></tr>';
            

        });
          
          table.html(body);
          activate_tooltip();
          $('#row-pages').html(response.pages);
          $("#row-order").val(response.order);
          loading.addClass("hide");

          }, 'json');
          
      }
    });
    $("#front-end-users-table").on("click", "#row-sort", function() {
      $('.loading-pane').removeClass('hide');
      $order = $("#row-order").val();
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      // $order = $("#row-order").val();
      $sort =  $(this).data('sort');
      $("#front-end-users-table").find("i").removeClass('fa-caret-up');
      $("#front-end-users-table").find("i").removeClass('fa-caret-down');
      $(this).find("i").addClass('fa-caret-down');
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, usertype_id:$usertype_id,order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
          $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class= "'+(row.status == 3 ? 'blockedColor': '')+''+(row.status == 2 ? 'disabledColor': '')+'">' + 
              '<td><small><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a>' + (row.status == 1 ? 'Active'  : ''  || row.status == 2 ? 'Disabled'  : '' || row.status == 3 ? 'Blocked'  : '' ) + '</small></td>' +
              '<td><small><center>' + row.type_name + '</small></center></td>' +
              '<td><small><center>' + row.plan_name + '</small></center></td>' +
              '<td><small>' + row.username + '</small></td>' +
              '<td><small>' + row.email + '</small></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/all') }}/'+ row.id +'" target="_blank">' + row.ad_listings + '</a></small></center></td>' +
              '<td><small><center><a href = "{{ url('admin/users/advertisement/list/flagged') }}/'+ row.id +'" target="_blank">' + row.ad_flagged + '</a></small></center></td>' +
              '<td><small>' + row.created_at + '</small></td>' +
              '<td><small>' + (row.last_login == null ? 'N/A': row.last_login) + '</small></td>' +
              '<td class="rightalign">'+
                 '<button data-toggle="tooltip" title="Delete" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>' +
                 (row.status !=3 ? '<button data-toggle="tooltip" title="Block" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="modal" data-target="#"><i class="fa fa-shield "></i></button>':'<button data-toggle="tooltip" title="Unblock" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="modal" data-target="#"><i class="fa fa-shirtsinbulk "></i></button>') +

                 '<button data-toggle="tooltip" title="Verify" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" '+(row.status == 3 ? 'disabled':'')+' '+(row.status == 1 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-paper-plane"></i></button>'+
                
                 (row.status !=2? '<button data-toggle="tooltip" title="Disable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" '+(row.status == 3 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>':'<button data-toggle="tooltip" title="Enable" type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>') +

                 '<a href="{{ url("/") }}' + '/' + (row.usertype_id == 1 ? 'vendor' : 'company') + '/' + row.alias + '"  data-toggle="tooltip" title="View" target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right"><i class="fa fa-eye"></i></a>' +
                 '<button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>' +
        '</td></tr>';
            

        });
        if(response.order == "asc"){
            $("#row-order").val("desc");
            console.log("asc");
            $(this).html("<i class='fa fa-caret-down'></i>");
        }else{
           console.log("desc");
            $('#row-order').val("asc");
            $(this).html("<i class='fa fa-caret-up'></i>");
        } 
       
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        activate_tooltip();
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
        console.log($sort);
         if($order == "asc"){
            if($sort != "created_at"){
              $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');
            }else{
              $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
         }else{
            if($sort != "created_at"){
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
             }else{
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down'); 
            }
          
           
         }
    });
    $(".content-wrapper").on("click", ".btn-add", function() {
    	     $("#row-name").removeAttr('disabled','disabled');
           $("#row-email").removeAttr('disabled','disabled');
           $("#row-password").removeAttr('disabled','disabled');
           $("#row-password_confirmation").removeAttr('disabled','disabled');
           $("#row-mobile").removeAttr('disabled','disabled');
           $("#row-usertype_id").removeAttr('disabled','disabled');
           $(".uploader-pane").addClass("hide");
           $("#modal-form").find('form').trigger("reset");
           $("#button-save").removeClass("hide");
           $(".modal-title").html('<i class="fa fa-user-plus"></i><strong>Add User</strong>');
           $("#modal-form").modal('show');
           $(".btn-change").addClass("hide");
           $(".btn-cancel").addClass("hide");
           $("#row-photo").addClass("hide");
           $(".uploader-pane").removeClass("hide");
           $("#company-tab").addClass("hide");
           $("#modal-form").find(".hide-view").removeClass("hide");
           // $("#modal-form").find("form").trigger("reset");
           $("#modal-form").find("form")[0].reset();
           $("#modal-form").find(".disable-view").removeAttr("disabled",'disabled');
           $("#row-password").attr("placeholder","Enter password");
           $("#row-password_confirmation").attr("placeholder","Enter password confirmation");
   	});
     $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        console.log(id);
        // reset all form fields
        // $("#company-branch-info").html(""); 
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".btn-cancel").addClass('hide');
        $(".btn-change").removeClass("hide");
        $("#row-photo").removeClass("hide");
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/user/edit') }}", { id: id, _token: $_token }, function(response) {

                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.row.name+']</strong>');

                      // output form data
                      $.each(response.row, function(index, value) {
                          console.log(index);
                          var field = $("#row-" + index);

                         if(index == "photo"){
                            $("#row-photo").attr("src","{{url('uploads').'/'}}"+value);

                          }
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                         if(index == "city"){
                          $("#row-city").append(response.city);
                         }
                         if(index =="usertype_id"){
                            if(value != 2){
                                $("#company-tab").addClass("hide"); 
                                
                            }else{
                                $("#company-tab").removeClass("hide");
                            }
                         }
                         if(index == "get_user_company_branch"){
                              $("#company-branch-container").html(response.branch);
                         }
                         if(index == "status"){
                           if(value == 1){
                              $("#modal-form").find(".btn-blocked").removeClass("hide");
                              $("#modal-form").find(".btn-disable").removeClass("hide");
                              $("#modal-form").find(".btn-remove").removeClass("hide");
                           }else if(value == 2){
                              $("#modal-form").find(".btn-blocked").removeClass("hide");
                              $("#modal-form").find(".btn-enable").removeClass("hide");
                              $("#modal-form").find(".btn-remove").removeClass("hide");
                              $("#modal-form").find(".btn-verify").removeClass("hide");
                           }else if(value== 3){
                              $("#modal-form").find(".btn-unblocked").removeClass("hide");
                              $("#modal-form").find(".btn-remove").removeClass("hide");
                           }
                         }
                        $("#modal-form").find(".hide-view").removeClass("hide");
                        $("#modal-form").find(".disable-view").removeAttr("disabled",'disabled');
                        $("#row-password").attr("placeholder","Leave blank for unchanged");
                        $("#row-password_confirmation").attr("placeholder","Leave blank for unchanged");
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-email").removeAttr('disabled','disabled');
                      $("#row-password").removeAttr('disabled','disabled');
                      $("#row-password_confirmation").removeAttr('disabled','disabled');
                      $("#row-mobile").removeAttr('disabled','disabled');
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
 	 $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        $("#row-photo").removeClass("hide");

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        // $(".change").addClass('hide');
        $('.btn-change').addClass('hide');
        $('.btn-cancel').addClass('hide');
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/user/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit User ['+response.row.name+']</strong>');

                      // output form data
                      $.each(response.row, function(index, value) {
                          console.log(index);
                          var field = $("#row-" + index);
                          
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                         if(index == "city"){
                          $("#row-city").append(response.city);
                         }
                         if(index == "photo"){
                            $("#row-photo").attr("src","{{url('uploads').'/'}}"+value);
                         }
                         if(index =="usertype_id"){
                            if(value != 2){
                                $("#company-tab").addClass("hide"); 
                                
                            }else{
                                $("#company-tab").removeClass("hide");
                            }
                         }
                         if(index == "get_user_company_branch"){
                             $("#company-branch-container").html(response.branch);
                         }
                        $("#modal-form").find(".hide-view").addClass("hide");
                        $("#modal-form").find(".disable-view").attr("disabled",'disabled');
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
        
                      $("#row-name").attr('disabled','disabled');
                      $("#row-email").attr('disabled','disabled');
                      $("#row-password").attr('disabled','disabled');
                      $("#row-password_confirmation").attr('disabled','disabled');
                      $("#row-mobile").attr('disabled','disabled');
                      $("#row-usertype_id").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

      $("#row-country").on("change", function() { 
            var id = $("#row-country").val();
            $_token = "{{ csrf_token() }}";
            var city = $("#row-city");
            city.html(" ");
            if ($("#row-country").val()) {
              $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
                city.append(response.rows);
              }, 'json');
           }
      });

        $("#row-usertype_id").on("change", function() { 
          if($(this).val() == 2){
              $("#company-tab").removeClass("hide");
          }else{
              $("#company-tab").addClass("hide");
          }
        });

    $(".btn-change").click(function(){
        $("#row-photo").addClass("hide");
        $(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
        $(".btn-cancel").removeClass("hide");
         
    });
    $(".btn-cancel").click(function(){
        $("#row-photo").removeClass("hide");
        $(".uploader-pane").addClass("hide");
        $(this).addClass("hide");
        $(".btn-change").removeClass("hide");
    });
    $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
   });
     $("#company-branch-container").on("change", "#branch-country", function() { 
        var id = $("#branch-country").val();
        $_token = "{{ csrf_token() }}";
        // var city = $("#branch-city");
        var city = $(this).parent().parent().parent().find("#branch-city");
        console.log(city);
        city.html(" ");
        if ($("#branch-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
    });
 $("#company-branch-container").on("change", "#account-company_country", function() { 
        var id = $(this).val();
        $_token = "{{ csrf_token() }}";
        var city = $(this).parent().parent().parent().parent().parent().find("#account-company_city");

        city.html(" ");
        if ($(this).val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response);
          }, 'json');
       }
  });
 

 
 // $(".btn-add-account-branch").click(function() {
  $("#company-branch-container").on("click", ".btn-add-account-branch", function() {

     var branchAccount =$("#accountBranch:first-child").html();
     var country = $('#accountBranch:first').find("#branch-country").val();
     var city = $('#accountBranch:first').find("#branch-city").html();

     $("#company-branch-container").append("<div id='accountBranch'>"+branchAccount+"</div>");
     $('#accountBranch:last-child').find("#branch-country").val(country);
     $('#accountBranch:last-child').find("#branch-city").html(city);
     $('#accountBranch:last-child').find(".btn-add-account-branch").removeClass("btn-add-account-branch").addClass("btn-del-account-branch").html("<i class='fa fa-minus redText'> REMOVE</i>");
     $('#accountBranch:last-child input:text').val("");
     $('#accountBranch:last-child').find("#branch_id").prop("value","0");
     $('#accountBranch:last-child').find("#account-company_country option:eq(0)").attr('selected','selected');
     $('#accountBranch:last-child').find("#account-company_city option:eq(0)").attr('selected','selected');
    
 });
    $loading = false;
    $selectedID = 0;

 $(".content-wrapper").on("click", ".btn-expand", function() {
  $button = $(this);
  $button.prop('disabled', true);
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');

        if ($loading){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return;
        }
        $selectedID = $id;
        $(".loading-pane").removeClass("hide");
        $.post("{{ url('get-user-info') }}", { id: $id, _token: $_token }, function(response) {
        $tr.after('<tr><td colspan="10" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="expand-table row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');
        $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $(".loading-pane").addClass("hide");
        }).complete(function(){ $button.prop('disabled', false); });
 });

 $("#company-branch-container").on("click", ".btn-del-account-branch", function() { 
   var current =  $(this).parent().parent().parent();
   var branch_id = current.find('#branch_id').val();
   current.find('#branch_status').val("2")
   current.addClass("hide");
 });
 $("#modal-form").on("click",'.btn-blocked',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
    dialog('Block User', 'Are you sure you want to block this user</strong>?', "{{ url('admin/users/block') }}",id);
 });
 $("#modal-form").on("click",'.btn-unblocked',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
    dialog('UnBlock User', 'Are you sure you want to unblock this user</strong>?', "{{ url('admin/users/unblock') }}",id);
 });
 $("#modal-form").on("click",'.btn-disable',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
    dialog('Disable User', 'Are you sure you want to disable this user</strong>?', "{{ url('admin/users/disable') }}",id);
 });
 $("#modal-form").on("click",'.btn-enable',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
    dialog('Enable User', 'Are you sure you want to enable this user</strong>?', "{{ url('admin/users/enable') }}",id);
 });
 $("#modal-form").on("click",'.btn-remove',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
    dialog('Remove User', 'Are you sure you want to remove this user</strong>?', "{{ url('admin/users/remove') }}",id);
 });
 $("#modal-form").on("click",'.btn-verify',function(){
    var id = $("#row-id").val();
    $("#modal-form").modal("hide");
   dialog('Resend Verification Email', 'Are you sure you want to resend the verification email to this user</strong>?', "{{ url('admin/users/verify-email') }}",id);
 });



 $(".content-wrapper").on("click", ".btn-blocked", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Block User', 'Are you sure you want to block this user</strong>?', "{{ url('admin/users/block') }}",id);
 });
 $(".content-wrapper").on("click", ".btn-unblocked", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Unblock User', 'Are you sure you want to unblock this user</strong>?', "{{ url('admin/users/unblock') }}",id);
 });
 $(".content-wrapper").on("click", ".btn-disable", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Disable User', 'Are you sure you want to disable this user</strong>?', "{{ url('admin/users/disable') }}",id);
 });
  $(".content-wrapper").on("click", ".btn-enable", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Enable User', 'Are you sure you want to enable this user</strong>?', "{{ url('admin/users/enable') }}",id);
 });
 $(".content-wrapper").on("click", ".btn-removed", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Remove User', 'Are you sure you want to remove this user</strong>?', "{{ url('admin/users/remove') }}",id);
 });
 $(".content-wrapper").on("click", ".btn-verify", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Resend Verification Email', 'Are you sure you want to resend the verification email to this user</strong>?', "{{ url('admin/users/verify-email') }}",id);
 });
 $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
 });
 $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
 });


</script>
@stop
@section('content')
<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
				<h3 class="noMargin"><i class="fa fa-users"></i> {{$title}}</h3>
			</div>
			<div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
			 <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
       <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>  	
			</div>
      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
        <div class="input-group borderZero">
        <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
        <input type="text" class="form-control borderZero" id="row-search">
        </div>
        </div>
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
              <div class="input-group borderZero">
              <span class="input-group-addon borderZero" id="basic-addon1">Usertype</span>
                  <select class="form-control borderZero" id="row-filter_usertype_id" onchange="refresh()">
                          <option value="">All</option>
                        @foreach($usertypes as $row)
                          <option value="{{$row->id}}">{{ucfirst($row->type_name)}}</option>
                        @endforeach
                  </select>
              </div>
         </div>
         <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
             <div class="input-group borderZero">
              <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
              <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                      <option value="">All</option>
                    @foreach($country as $row)
                      <option value="{{$row->id}}">{{$row->countryName}}</option>
                    @endforeach
              </select>
              </div>
         </div>
    </div>
		</div>
		<div class="table-responsive col-xs-12 noPadding">
			<div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
			<table class="table table-condensed noMarginBottom backend-generic-table" id="front-end-users-table">
			    <thead>
			      <tr>
            <!--   <th>Photo</th> -->
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="status" id="row-sort"><i class="fa"></i>  Status</div></th>
			        <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="usertype_id" id="row-sort"><center><i class="fa"></i>  Type</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="plan_name" id="row-sort"><center><i class="fa"></i>  Plan Type</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="username" id="row-sort"><i class="fa"></i>  Username</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="email" id="row-sort"><i class="fa"></i>  Email</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_listings" id="row-sort"><center><i class="fa"></i>  Listings</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_flagged" id="row-sort"><center><i class="fa"></i>  Flagged</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="created_at" id="row-sort"><i class="fa fa-caret-up"></i>  Create Date</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="last_login" id="row-sort"><i class="fa"></i>  Last Login</div></th>
			        <th class="text-right">Tools</th>
			      </tr>
			    </thead>
			    <tbody id="rows">
			    	@foreach($rows as $row)
			    		<tr data-id="{{$row->id}}" class="{{$row->status == 3 ? 'blockedColor':''}}{{$row->status == 2 ? 'disabledColor':''}}">
              <!--   <td><img src="{{ url('uploads/').'/'.$row->photo}}" height="30" width="30" class="img-responsive"></td> -->
                <td><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="{{ $row->id }}"><i class="fa fa-plus"></i></button> {{($row->status == 1 ? 'Active':'')}}{{($row->status == 2 ? 'Disabled':'')}}{{($row->status == 3 ? 'Blocked':'')}}</small></td>
                <td><center><small>{{$row->type_name}}</small></center></td>
			    			<td><center><small>{{$row->plan_name}}</small></center></td>
			    			<td><small>{{$row->username}}</small></td>
                <td><small>{{$row->email}}</small></td>
                <td><center><small><a href="{{url('admin/users/advertisement/list/all').'/'.$row->id}}" target="_blank">{{$row->ad_listings}}</a></small></center></td>
                <td><center><small><a href="{{url('admin/users/advertisement/list/flagged').'/'.$row->id}}" target="_blank">{{$row->ad_flagged}}</a></small></center></td>
                <td><small>{{$row->created_at}}</small></td>
                <td><small>{{$row->last_login == null ? 'N/A': $row->last_login}}</small></td>
			    			 <td>
                  <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-removed" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                    @if($row->status != 3)
                    <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-blocked" data-toggle="tooltip" title="Block"><i class="fa fa-shield" aria-hidden="true"></i></button>
                    @else
                    <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-unblocked" data-toggle="tooltip" title="Unblock"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i></button>
                    @endif
                    <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-verify" data-toggle="tooltip" title="Verify" {{$row->status == 3 ? 'disabled':''}} {{$row->status == 1 ? 'disabled':''}}><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    @if($row->status != 2)
                    <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-disable" data-toggle="tooltip" title="Disable"{{$row->status == 3 ? 'disabled':''}}><i class="fa fa-lock" aria-hidden="true"></i></button>
                    @else
                    <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-enable" data-toggle="tooltip" title="Enable"><i class="fa fa-unlock" aria-hidden="true"></i></button>
                    @endif

					       		<a href="{{url('/').'/'.($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"  target="_blank" role="button" class="btn btn-default btn-xs btn-tools pull-right" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
					   			  <button type="button" class="btn btn-default btn-xs btn-tools pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
					   		  
                </td>	
			    		</tr>
			    	@endforeach
		 	 </table>
		 </div>
      <input type="hidden" id="row-page">
      <input type="hidden" id="row-order" value="">
      <input type="hidden" id="row-sort" value="">
      <input type="hidden" id="row-counter" value="1">
	</div>
     <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">  
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class='form-control borderZero' id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
</div>
@stop
@include('admin.user-management.front-end-users.form')