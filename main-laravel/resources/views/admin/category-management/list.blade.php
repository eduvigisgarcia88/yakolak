@extends('layout.master')
@section('scripts')

<script type="text/javascript">
   $loading = false;
   $selectedID =0;
   $_token = '{{ csrf_token() }}';
function refresh() {
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $("#row-filter_status").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.sequence + '</small></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small><center>' + row.country_code + '</center></small></td>' +
              '<td><small><center>'+(row.biddable_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.buy_and_sell_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.home == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>'
              '</td></tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
     function maxAds() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.sequence + '</small></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small><center>' + row.country_code + '</center></small></td>' +
              '<td><small><center>'+(row.biddable_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.buy_and_sell_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.home == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>'
              '</td></tr>';
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
     function search() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
         body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.sequence + '</small></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small><center>' + row.country_code + '</center></small></td>' +
              '<td><small><center>'+(row.biddable_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.buy_and_sell_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.home == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>'
              '</td></tr>';
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    } 

 $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
         body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.sequence + '</small></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small><center>' + row.country_code + '</center></small></td>' +
              '<td><small><center>'+(row.biddable_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.buy_and_sell_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.home == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>'
              '</td></tr>';
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

      }
    });
  $("#category-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#category-table").find("i").removeClass('fa-caret-up');
          $("#category-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
           if($order == "asc"){
              $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
           }else{
              $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
           } 
          
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
         body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.sequence + '</small></td>' +
              '<td><small>' + row.name + '</small></td>' +
              '<td><small><center>' + row.country_code + '</center></small></td>' +
              '<td><small><center>'+(row.biddable_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.buy_and_sell_status == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td><small><center>'+(row.home == 1 ? 'No':'Yes')+'</center></small></td>'+
              '<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>'
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
            
          }else{
              $('#row-order').val("asc");
          }
       
      }, 'json');
    });
    $(".content-wrapper").on("click", ".btn-add", function() {
           $("#modal-form").find('form').trigger("reset");
           // $(".category-pane").removeClass("hide");
           $(".sequence_pane").addClass("hide");
           $("#button-save").removeClass("hide");
           $("#category_type_one").removeAttr('checked','checked');
           $("#category_type_two").removeAttr('checked','checked');
           $("#row-name").removeAttr('disabled','disabled');
           $("#row-sequence").removeAttr('disabled','disabled');
           $("#category_type_one").removeAttr('disabled','disabled');
           $("#category_type_two").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#row-custom_attributes").removeAttr('disabled','disabled');           
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Category</strong>');
           $("#row-id").val("");
           $("#modal-add-category").find('form')[0].reset();
           $("#modal-add-category").find("#form-notice").find('.alert').remove();
           $("#modal-add-category").modal('show');
    });
    $(".content-wrapper").on("click", ".btn-add-subcat", function() { 
           var id = $(this).parent().parent().data('id');
           $("#modal-sub-category").find("form").find(".edit-view").attr("disabled","disabled");
           $("#modal-sub-category").find("form").find('#row-cat_id').val(id).attr("selected", "selected");
           $("#modal-sub-category").find("form").find('#row-form-cat-id').val(id);
           $("#modal-sub-category").find("form").find('#row-name').val("");
           $("#modal-sub-category").find("form").find('#row-description').val("");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Sub Category</strong>');
           $("#modal-sub-category").modal('show'); 
    });
    $(".content-wrapper").on("click", ".btn-add-subcat-two", function() { 
           var id = $(this).parent().parent().data('id');
           $("#modal-sub-category-two").find("form").find(".edit-view").attr("disabled","disabled");
           $("#modal-sub-category-two").find("form").find('#row-cat_id').val(id).attr("selected", "selected");
           $("#modal-sub-category-two").find("form").find('#row-form-cat-id').val(id);
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Sub Category</strong>');
           $("#modal-sub-category-two").modal('show');
    });
    $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $(".uploader-pane").addClass("hide");
        $("#modal-add-category").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-add-category").find('form')[0].reset();
        $("#modal-add-category").find("#form-notice").find('.alert').remove();
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        // User Id Disabled
        $('#modal-add-category').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          console.log(index+'='+value);
                         if(index == "user_type") {
                          $("#row-usertype_id-static").val(value.type_name);
                         }
                     
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                                           
                      
                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                            $("#row-buy_and_sell_status").val(value);
                          }
                          if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                             $("#row-biddable_status").val(value);
                          }

                          if(index=="home"){
                            if(value=="2"){
                              $("#category_home_option").attr("checked","checked");
                            }else{
                              $("#category_home_option").removeAttr("checked","checked");
                            }
                            $("#row-home").val(value);
                          }
                      });
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-add-category").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                        
                          if(field.length > 0) {
                              field.val(value);
                            
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                            $("#row-buy_and_sell_status").val(value);
                          }
                          if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                             $("#row-biddable_status").val(value);
                          }

                          if(index=="home"){
                            if(value=="2"){
                              $("#category_home_option").attr("checked","checked");
                            }else{
                              $("#category_home_option").removeAttr("checked","checked");
                            }
                            $("#row-home").val(value);
                          }
                      });
                      $(".category-pane").addClass("hide");
                      $("#row-name").attr('disabled','disabled');
                      $("#row-sequence").attr('disabled','disabled');
                      $("#category_type_one").attr('disabled','disabled');
                      $("#category_type_two").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-custom_attributes").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-view-sub-cat", function() {
        var id = $(this).parent().parent().data('subcat-id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $("#modal-form").find('form')[0].reset();
        $("#modal-form").find("#form-notice").find('.alert').remove();
        $('#row-cat_id').removeAttr("selected", "selected");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/subcategory/manipulate') }}", { id: id, _token: $_token }, function(response) {

                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                           if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }

                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          } 
                      });
                      $(".category-pane").addClass("hide");
                      $("#row-name").attr('disabled','disabled');
                      $("#row-sequence").attr('disabled','disabled');
                      $("#category_type_one").attr('disabled','disabled');
                      $("#category_type_two").attr('disabled','disabled');
                      $("#row-description").attr('disabled','disabled');
                      $("#row-custom_attributes").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-edit-sub-cat", function() {
        var id = $(this).parent().parent().data('id');
        var parent_id = $(this).parent().parent().data('parent-id');
        var btn = $(this);
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#modal-sub-category-two").find("#form-notice").find('.alert').remove();
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/subcategory/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data
                      // console.log(response.name);
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      //     if(index=="biddable_status"){
                      //       if(value=="2"){
                      //         $("#category_type_one").attr("checked","checked");
                      //       }else{
                      //         $("#category_type_one").removeAttr("checked","checked");
                      //       }
                      //     }
                      //     if(index=="buy_and_sell_status"){
                      //       if(value=="2"){
                      //         $("#category_type_two").attr("checked","checked");
                      //       }else{
                      //         $("#category_type_two").removeAttr("checked","checked");
                      //       }
                      //     }
                      });
                      $("#modal-sub-category").find('#row-cat_id').val(parent_id);
                      $("#modal-sub-category").find('#row-cat_id').attr("disabled","disabled");
                      $("#modal-sub-category").find('#row-name').val(response.name);
                      $("#modal-sub-category").find('#row-description').val(response.description);
                      $("#modal-sub-category").find('#row-id').val(response.id);
                      $(".category-pane").removeClass("hide");
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-sub-category").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-edit-sub-cat-two", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        // $("#modal-sub-category").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        // $("#modal-sub-category").find('form')[0].reset();
        $("#modal-sub-category-two").find("#form-notice").find('.alert').remove();
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/subcategory/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Category ['+response.name+']</strong>');

                      // output form data

                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          if(field.length > 0) {
                              field.val(value);
                            
                          }

                          if(index=="biddable_status"){
                            if(value=="2"){
                              $("#category_type_one").attr("checked","checked");
                            }else{
                              $("#category_type_one").removeAttr("checked","checked");
                            }
                          }
                          if(index=="buy_and_sell_status"){
                            if(value=="2"){
                              $("#category_type_two").attr("checked","checked");
                            }else{
                              $("#category_type_two").removeAttr("checked","checked");
                            }
                          }
                      });
                      $(".category-pane").removeClass("hide");
                
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-sub-category-two").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $("#row-cat_id").on("change", function() { 
          var id = $("#row-cat_id").val();
          var order= $("#row-sequence").val();

          $(".sequence_pane").addClass("hide");
          $_token = "{{ csrf_token() }}";
          if(id == "parent"){
            $(".sequence_pane").addClass("hide");
          }else if(id != "parent"){
            $(".sequence_pane").removeClass("hide");
  
          }
          
        });
       // $("#row-country-code").on("change", function() { 
       //     var country_code = $(this).val();
       //     $(".category-pane").removeClass("hide");
       //      $.post("{{ url('admin/get-country-categories') }}", { country_code:country_code,_token: $_token }, function(response) {
       //            $("#row-cat_id").html(response.rows);
       //      }, 'json');
       // });
      $("#row-sequence").on("keyup", function() {
          var id = $("#row-cat_id").val();
          var order= $("#row-sequence").val();
          if(id == "parent"){


            $.post("{{ url('admin/category/get-cat-sequence') }}", { order:order,_token: $_token }, function(response) {
              $.each(response.rows, function(index, row) {
             
                    if(row.sequence == order){        
                      status('','<ul><li> Selected display number already in used</li></ul>','alert-danger', '#form-notice');
                    }         
              });
            }, 'json');

          }else{

            $.post("{{ url('admin/category/get-subcat-sequence') }}", { id: id, order:order,_token: $_token }, function(response) {
              $.each(response.rows, function(index, row) {
             
                    if(row.sequence == order){        
                      status('','<ul><li> Selected sequence number already in used</li></ul>','alert-danger', '#form-notice');
                    }         
              });
            }, 'json');
          }
          

       });

    $(document).ready(function() {
    $('#category_type_one').click(function() {
        if($("#category_type_one").is(":checked")){
             $("#row-buy_and_sell_status").val("2");

        }else{
             $("#row-buy_and_sell_status").val("1");
        } 
        
       });
        $('#category_type_two').click(function() {
          if($("#category_type_two").is(":checked")){
             $("#row-biddable_status").val("2");
           }else{
             $("#row-biddable_status").val("1");
           } 
        });
        $('#category_home_option').click(function() {
          if($("#category_home_option").is(":checked")){
             $("#row-home").val("2");
           }else{
            $("#row-home").val("1");
           } 
        });  
    });
    $(".content-wrapper").on("click", ".btn-expand", function() {
      $button = $(this);
      // $button.prop('disabled', true);
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
       
        if ($loading){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        console.log($id);
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return;
        }
        $selectedID = $id;
        
        // $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $(".loading-pane").removeClass("hide");
        $loading = true;

        $.post("{{ url('admin/get-subcategories') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="10" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="expand-table row-'+$selectedID +'" style="display:none;">' + response + '</div></td></tr>');
          $(".row-"+$selectedID).slideDown();
          // console.log($selectedID);
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
          $(".loading-pane").addClass("hide");
        })

 }); 

   $loadingSub = false;
   $selectedIDSub =0;
 $(".content-wrapper").on("click", ".btn-expand-subcat", function() {
      $button = $(this);
      // $button.prop('disabled', true);
      $(".btn-expand-subcat").html('<i class="fa fa-minus"></i>');
      $(".btn-expand-subcat").html('<i class="fa fa-plus"></i>');
       
        if ($loadingSub){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedIDSub;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedIDSub) {
          $selectedIDSub = 0;
          return;
        }
        $selectedIDSub = $id;
        
        // $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $(".loading-pane").removeClass("hide");
        $loadingSub = true;

        $.post("{{ url('admin/get-subcategories-two') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="10" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="expand-table row-'+$selectedIDSub +'" style="display:none;">' + response + '</div></td></tr>');
          $(".row-"+$selectedIDSub).slideDown();
          // console.log($selectedID);
          $button.html('<i class="fa fa-minus"></i>');
          $loadingSub = false;
          $(".loading-pane").addClass("hide");
        })

 }); 
  $(".content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        console.log(id);
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Delete Category', 'Are you sure you want to delete this category</strong>?', "{{ url('admin/category/delete') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Enable Category', 'Are you sure you want to enable this category</strong>?', "{{ url('admin/category/enable') }}",id);
  });  
  $(".content-wrapper").on("click", ".btn-disable", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Disable Category', 'Are you sure you want to disable this category</strong>?', "{{ url('admin/category/disable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable-sub", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Disable Category', 'Are you sure you want to disable this category</strong>?', "{{ url('admin/subcategory/disable') }}",id);
  }); 
  $(".content-wrapper").on("click", ".btn-enable-sub", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Disable Category', 'Are you sure you want to disable this category</strong>?', "{{ url('admin/subcategory/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-delete-sub", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Delete Category', 'Are you sure you want to delete this category</strong>?', "{{ url('admin/subcategory/delete') }}",id);
  });
  $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
  }); 
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-plus"></i> Add</button>
        <!-- <button type="button" class="btn btn-warning btn-md btn-attr pull-right btn-phrase"><i class="fa fa-list"></i> Phrases</button>         -->
      </div>
    </div>
     <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                                <option value="">All</option>
                                @foreach($countries as $row)
                                  <option value="{{$row->countryCode}}">{{$row->countryName}} [{{$row->countryCode}}]</option>
                                @endforeach
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="category-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="sequence" id="row-sort"><i class="fa"></i> Display Order</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="name" id="row-sort"><i class="fa"></i> Name</div></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="country_code" id="row-sort"><i class="fa"></i> Country</div></center></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="buy_and_sell_status" id="row-sort"><i class="fa"></i> Auction</div></center></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="biddable_status" id="row-sort"><i class="fa"></i> Buy and Sell</div></center></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="home" id="row-sort"><i class="fa fa-caret-up"></i> Home</div></center></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($rows as $row)
            <tr data-id="{{$row->id}}" class="{{ ($row->status == 2 ? 'disabledColor':'') }}">
                <td><a class="btn btn-xs btn-table btn-expand" data-id="{{ $row->id }}"><i class="fa fa-plus"></i></a><small>{{$row->sequence }}</small></td>
                <td><small>{{$row->name}}</small></td>
                <td><small><center>{{$row->country_code}}</center></small></td>
                <td><small><center>{{$row->biddable_status == 1 ? 'No':'Yes'}}</center></small></td>
                <td><small><center>{{$row->buy_and_sell_status == 1 ? 'No':'Yes'}}</center></small></td>
                <td><small><center>{{$row->home == 1 ? 'No':'Yes'}}</center></small></td>
                <td>
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>
                  @if($row->status == 1 )
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>
                  @else 
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>
                  @endif
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-subcat"><i class="fa fa-plus"></i></button>
                </td> 
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
@stop
@include('admin.category-management.form')