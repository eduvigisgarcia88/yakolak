<!-- modal add user -->
  <div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/category/custom-attribute/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i></h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Country Code</label>
                  <div class="col-sm-9 col-xs-7">
                   <select class="form-control disable-view" id="row-country_code" name="country_code">
                          <option class="hide" value="">Select</option>
                          @foreach($countries as $row)
                            <option value="{{$row->countryCode}}">{{$row->countryName}} [{{$row->countryCode}}]</option>
                          @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group category-pane">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Category</label>
                  <div class="col-sm-9 col-xs-7">
                    <select class="form-control disable-view" id="row-attri_id" name="attri_id">

                    </select>
                  </div>
                </div>
                <div class="form-group subcategory-pane hide">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color"></label>
                  <div class="col-sm-9 col-xs-7 disable-view" id="sub_attri_id_container">
                  </div>
                </div>

                <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Order No</label>
                  <div class="col-sm-9 col-xs-7">
                    <input type="number" name="sequence_no" class="form-control disable-view" style="cursor:default !important;" id="row-sequence_no" maxlength="30" placeholder="Enter order no..">
                  </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Attribute Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="name" class="form-control disable-view" id="row-name" maxlength="30" placeholder="Enter attribute name...">
                </div>
                </div>
                <div class="form-group" id="attri_type_pane">
                    <label for="row-attri_type" class="col-sm-3 col-xs-5 control-label font-color">Attribute Type</label>
                <div class="col-sm-9 col-xs-7">
                     <select class="form-control disable-view" id="row-attri_type" name="attri_type">
                          <option class="hide">Select</option>
                          <!-- <option value="1">CHECKBOX</option> -->
                          <option value="2">Textbox</option>  
                          <option value="3">Dropdown</option> 
                     </select>
                </div>
                </div>
                <div class="form-group attribute_dropdown hide">
                  <div class="col-lg-12">
                    <div class="row" id="attri_value_pane">
                    <label for="row-code" class="col-sm-3 control-label"><button type="button"ss value="" class="disable-view btn-add-attribute-value " style="background-color:transparent;border:0;color:#5da4ee;"> <i class="fa fa-plus Add"> </i></button></label>
                      <div class="table-responsive">
                      <table id="attribute-value-table" class="table">
                          <tbody id="attribute-value-table-body">
                              <tr>
                                  <td class="col-xs-12">
                                      <div class="col-xs-12 input-group">
                                          <input type="text" name="attribute_value_dropdown[]" class="form-control borderzero col-lg-12 col-md-12 col-sm-12 disable-view" maxlength="30">
                                      </div>
                                  </td>
                                  <td class="hide-sta text-right">
                                      <input type="hidden" name="attribute_value_id[]">
                                      <input type="hidden" name="attribute_status[]" id="attribute_status">
                                      <button type="button" style="background-color:transparent;border:0;color:red;" class="hide-view btn-del-attribute-value" title="Remove"> <i class="fa fa-close"> </i></button>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
                      </div>
                  </div>
                </div>
                </div>
               <!--  <div class="form-group attribute_textbox hide">
                    <label for="row-attribute_value_textbox" class="col-sm-3 col-xs-5 control-label font-color">Attribute Value</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="attribute_value_textbox" class="form-control disable-view" id="row-attribute_value_textbox" maxlength="30">
                </div>
                </div> -->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
