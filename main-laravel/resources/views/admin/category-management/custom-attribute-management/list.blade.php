@extends('layout.master')
@section('scripts')

<script>
   $loading = false;
   $selectedID =0;
   $loadingSub = false;
   $selectedSubID =0;
   $_token = '{{ csrf_token() }}';
  function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, country: $country ,status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();
        $.each(response.rows.data, function(index, row) {
              body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' +
                    '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.name + '</small></td>' + 
                    '<td class="text-right"><small>' + row.country_code.toUpperCase() + '</small></td></tr>';
                    
                    // '<td><small>' + (row.status == 1 ? 'Enabled':'Disabled') + '</small></td>' +
                    // '<td class="rightalign">'+
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                    //   (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>') +
                    // '</td></tr>';
            });
           
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
    function search(){
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
     
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, country: $country ,status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
             body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' +
                    '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.name + '</small></td>' + 
                    '<td class="text-right"><small>' + row.country_code.toUpperCase() + '</small></td></tr>';
                    
                    // '<td><small>' + (row.status == 1 ? 'Enabled':'Disabled') + '</small></td>' +
                    // '<td class="rightalign">'+
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                    //   (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>') +
                    // '</td></tr>';


            });
           
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
    function maxAds(){
      refresh();
    }
     $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
       $.each(response.rows.data, function(index, row) {
                body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' +
                    '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.name + '</small></td>' + 
                    '<td class="text-right"><small>' + row.country_code.toUpperCase() + '</small></td></tr>';
                    
                    // '<td><small>' + (row.status == 1 ? 'Enabled':'Disabled') + '</small></td>' +
                    // '<td class="rightalign">'+
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                    //   (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>') +
                    // '</td></tr>';
         });
        
        $("#modal-form").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

      }
    });
     $("#category-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#category-table").find("i").removeClass('fa-caret-up');
          $("#category-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
           if($order == "asc"){
              $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
           }else{
              $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
           } 
          
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' +
                    '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>' + row.name + '</small></td>' + 
                    '<td class="text-right"><small>' + row.country_code.toUpperCase() + '</small></td></tr>';
                    
                    // '<td><small>' + (row.status == 1 ? 'Enabled':'Disabled') + '</small></td>' +
                    // '<td class="rightalign">'+
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                    //    '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                    //   (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>') +
                    // '</td></tr>';


            });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
            
          }else{
              $('#row-order').val("asc");
          }
       
      }, 'json');
  

    });
    $(".content-wrapper").on("click", ".btn-edit-cat-attri", function() {
        var id = $(this).parent().parent().data('cat-attri-id');
        var btn = $(this);
        console.log(id);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                          $("#attri_type_pane").addClass("hide");  
                      });
                      $("#modal-form").find(".disable-view").removeAttr("readonly","readonly");
                      $("#modal-form").find(".disable-view").removeAttr("disabled","disabled"); 
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });


   $(".content-wrapper").on("click", ".btn-view-cat-attri", function() {
        var id = $(this).parent().parent().data('cat-attri-id');
        var btn = $(this);
        console.log(id);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";

        // Remove selected photo input.

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                         $("#attri_type_pane").addClass("hide");    
                      });
                      $("#modal-form").find(".disable-view").attr("readonly","readonly");
                      $("#modal-form").find(".disable-view").attr("disabled","disabled"); 
                      $(".category-pane").removeClass("hide");
                      $("#row-name").removeAttr('disabled','disabled');
                      $("#row-sequence").removeAttr('disabled','disabled');
                      $("#category_type_one").removeAttr('disabled','disabled');
                      $("#category_type_two").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-custom_attributes").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
                      // show form
                      $("#modal-form").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
$(".content-wrapper").on("click", ".btn-view-subcat-attri", function() {
            var id = $(this).parent().parent().data('sub-cat-attri-id');
            var btn = $(this);
            console.log(id);
            // reset all form fields
            $("#modal-form").find('form').trigger("refresh");
            $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
            $("#row-id").val("");
            $_token = "{{ csrf_token() }}";

            // Remove selected photo input.

            btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
                
                  $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                      
                      if(!response.error) {
                      // set form title
                          $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                          // output form data
                          $.each(response, function(index, value) {
                              var field = $("#row-" + index);
                              // field exists, therefore populate
                              if(field.length > 0) {
                                  field.val(value);
                               
                              }
                                $("#attri_type_pane").addClass("hide");    
                          });
                          $("#modal-form").find(".disable-view").attr("readonly","readonly");
                          $("#modal-form").find(".disable-view").attr("disabled","disabled");  
                          $(".category-pane").removeClass("hide");
                          $("#row-name").removeAttr('disabled','disabled');
                          $("#row-sequence").removeAttr('disabled','disabled');
                          $("#category_type_one").removeAttr('disabled','disabled');
                          $("#category_type_two").removeAttr('disabled','disabled');
                          $("#row-description").removeAttr('disabled','disabled');
                          $("#row-custom_attributes").removeAttr('disabled','disabled');
                          $("#button-save").removeClass("hide");
                          // show form
                          $("#modal-form").modal('show');
                      } else {
                          status(false, response.error, 'alert-danger');
                      }

                      btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                  }, 'json');
            
          });
    $(".content-wrapper").on("click", ".btn-edit-subcat-attri", function() {
            var id = $(this).parent().parent().data('sub-cat-attri-id');
            var btn = $(this);
            console.log(id);
            // reset all form fields
            $("#modal-form").find('form').trigger("refresh");
            $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
            $("#row-id").val("");
            $_token = "{{ csrf_token() }}";

            // Remove selected photo input.

            btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
                
                  $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                      
                      if(!response.error) {
                      // set form title
                          $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Category ['+response.name+']</strong>');

                          // output form data
                          $.each(response, function(index, value) {
                              var field = $("#row-" + index);
                              // field exists, therefore populate
                              if(field.length > 0) {
                                  field.val(value);
                               
                              }
                                $("#attri_type_pane").addClass("hide");    
                          });
                          $("#modal-form").find(".disable-view").removeAttr("readonly","readonly");
                          $("#modal-form").find(".disable-view").removeAttr("disabled","disabled");    
                          $(".category-pane").removeClass("hide");
                          $("#row-name").removeAttr('disabled','disabled');
                          $("#row-sequence").removeAttr('disabled','disabled');
                          $("#category_type_one").removeAttr('disabled','disabled');
                          $("#category_type_two").removeAttr('disabled','disabled');
                          $("#row-description").removeAttr('disabled','disabled');
                          $("#row-custom_attributes").removeAttr('disabled','disabled');
                          $("#button-save").removeClass("hide");
                          // show form
                          $("#modal-form").modal('show');
                      } else {
                          status(false, response.error, 'alert-danger');
                      }

                      btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                  }, 'json');
            
         });
        $(".content-wrapper").on("click", ".btn-view", function() {
             var id = $(this).parent().parent().data('id');
                var btn = $(this);
                // reset all form fields
                $("#modal-form").find('form').trigger("refresh");
                $("#attribute-value-table-body").html("");
                $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
                $("#row-id").val("");
                $("#row-attri_id").html('');
                $_token = "{{ csrf_token() }}";
                btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
                    
                      $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                          if(!response.error) {
                          // set form title
                              $(".modal-title").html('<i class="fa fa-eye"></i> <strong>View Custom Attribute ['+response.data.name+']</strong>');
                              // output form data
                              $.each(response, function(index, value) {
                                  var field = $("#row-" + index);
                                  // field exists, therefore populate
                                  if(field.length > 0) {
                                      field.val(value);
                                    
                                  }  
                              });
                              $(".subcategory-pane").removeClass("hide");
                              $('#sub_attri_id_container').html(response.rows);
                              $("#row-attri_type").val(response.data.attribute_type);
                              if($("#row-attri_type").val() == 1){
                                $(".attribute_dropdown").removeClass("hide");
                                $(".attribute_textbox").addClass("hide");
                              }else if($("#row-attri_type").val() == 2){
                                $(".attribute_textbox").removeClass("hide");
                                $(".attribute_dropdown").addClass("hide");
                              }else {
                                $(".attribute_textbox").addClass("hide");
                                $(".attribute_dropdown").removeClass("hide");
                              }
                              $("#attribute-value-table-body").html(response.attribute_values);
                              $("#row-country_code").val(response.country_code);
                              $("#row-attri_id").html(response.categories); 
                           
                              $("#row-name").val(response.data.name)
                              $("#row-sequence_no").val(response.data.sequence_no);
                              $(".category-pane").removeClass("hide");
                              $("#modal-form").find("form").find(".disable-view").attr("disabled","disabled");
                           
                              $("#button-save").addClass("hide");
                              // show form
                              $("#modal-form").modal('show');
                          } else {
                              status(false, response.error, 'alert-danger');
                          }
                          $('#row-sub_attri_id').multiselect();
                          $(".subcategory-pane").find("button").attr("disabled","disabled");
                          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                      }, 'json');
        });
        $(".content-wrapper").on("click", ".btn-edit", function() {
                var id = $(this).parent().parent().data('id');
                var btn = $(this);
                // reset all form fields
                $("#modal-form").find('form').trigger("refresh");
                $("#attribute-value-table-body").html("");
                $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
                $("#row-id").val("");
                $("#row-attri_id").html('');
                $_token = "{{ csrf_token() }}";

                // Remove selected photo input.

                btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
                    
                      $.post("{{ url('admin/category/custom-attribute/manipulate') }}", { id: id, _token: $_token }, function(response) {
                          if(!response.error) {
                          // set form title
                              console.log(response.categories);
                              $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Attribute ['+response.data.name+']</strong>');
                              // output form data
                              $.each(response, function(index, value) {
                                  var field = $("#row-" + index);
                                  // field exists, therefore populate
                                  if(field.length > 0) {
                                      field.val(value);
                                    
                                  }  
                              });
                              if(response.rows == undefined){
                                 $(".subcategory-pane").addClass("hide");
                              }else{
                                 $(".subcategory-pane").removeClass("hide");
                              }
                              $('#sub_attri_id_container').html(response.rows);
                              $("#row-attri_type").val(response.data.attribute_type);
                              $('#attri_value_pane').removeClass("hide");
                              if($("#row-attri_type").val() == 1){
                                $(".attribute_dropdown").removeClass("hide");
                                $(".attribute_textbox").addClass("hide");
                              }else if($("#row-attri_type").val() == 2){
                                $(".attribute_textbox").removeClass("hide");
                                $(".attribute_dropdown").addClass("hide");
                              }else {
                                $(".attribute_textbox").addClass("hide");
                                $(".attribute_dropdown").removeClass("hide");
                              }
                              $("#attribute-value-table-body").html(response.attribute_values);
                              $("#row-country_code").val(response.country_code);
                              $("#row-attri_id").html(response.categories);
                              $("#row-sequence_no").val(response.data.sequence_no);
                              $("#row-name").val(response.data.name);
                              $("#row-id").val(response.data.id);
                              $("#modal-form").find("form").find(".disable-view").removeAttr("disabled","disabled");
                              $(".category-pane").removeClass("hide");
                              $("#row-name").removeAttr('disabled','disabled');
                              $("#row-sequence").removeAttr('disabled','disabled');
                              $("#category_type_one").removeAttr('disabled','disabled');
                              $("#category_type_two").removeAttr('disabled','disabled');
                              $("#row-description").removeAttr('disabled','disabled');
                              $("#row-custom_attributes").removeAttr('disabled','disabled');
                              $("#button-save").removeClass("hide");
                              // show form
                              $("#modal-form").modal('show');
                          } else {
                              status(false, response.error, 'alert-danger');
                          }
                          $(".subcategory-pane").find("button").removeAttr("disabled","disabled");
                           $('#row-sub_attri_id').multiselect({
                            maxHeight: 400,
                            enableCaseInsensitiveFiltering: true,
                            selectAllValue: 'multiselect-all',
                            selectAllText: 'Select All',
                            includeSelectAllOption: true,
                            numberDisplayed: 2
                          });
                          btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
                      }, 'json');
                
      });
      $(".content-wrapper").on("click", ".btn-add", function() {
           $("#attri_type_pane").removeClass("hide");
           $("#modal-form").find("form").trigger("reset");
           $("#modal-form").find(".disable-view").removeAttr("readonly","readonly");
           $("#modal-form").find(".disable-view").removeAttr("disabled","disabled");    
           $(".modal-title").html('<i class="fa fa-plus"></i><strong> Add Custom Attribute</strong>');
           $("#modal-form").find("form")[0].reset();
           $("#modal-form").find("#row-attri_id").html("<option value='' selected disabled='disabled'>Must select country first to populate</option>");
           $("#modal-form").find("#row-id").val("");
           $("#modal-form").find('#attri_value_pane').addClass("hide");
           $(".subcategory-pane").addClass("hide");
           $("#modal-form").modal('show');
     });
     $(".btn-add-attribute-value").click(function() {
        var table = $("#attribute-value-table tbody");
        var row = table.find("tr:first");
        var newrow = row.clone();
        newrow.appendTo(table);
        table.find("select[name='attribute_value[]']:last").focus();
        var last_tr = table.find("tr:last");
        last_tr.find('input').val("");
    });
    $(".content-wrapper").on("click", ".btn-expand", function() {
      $button = $(this);
      // $button.prop('disabled', true);
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
       
        if ($loading){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return;
        }
        $selectedID = $id;
        
        // $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $(".loading-pane").removeClass("hide");
        $loading = true;

        $.post("{{ url('admin/category/get-custom-attributes') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="9" style="padding: 0px 0px 0px 0px !important;background-color:#f7f7f7;text-indent:30px;"><div class="expand-table row-'+$selectedID +'">' + response + '</div></td></tr>');
          $(".row-"+$selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
          $(".loading-pane").addClass("hide");
        })

 }); 
 $(".content-wrapper").on("click", ".btn-expand-sub", function() {
      $button = $(this);
      // $button.prop('disabled', true);
      $(".btn-expand-sub").html('<i class="fa fa-minus"></i>');
      $(".btn-expand-sub").html('<i class="fa fa-plus"></i>');
       
        if ($loadingSub){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedSubID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedSubID) {
          $selectedSubID = 0;
          return;
        }
        $selectedSubID = $id;
        
        // $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $(".loading-pane").removeClass("hide");
        $loadingSub = true;

        $.post("{{ url('admin/subcategory/get-custom-attributes') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="9" style="padding: 0px 0px 0px 0px !important;background-color:#f7f7f7;text-indent:30px;"><div class="expand-table row-'+$selectedSubID +'">' + response + '</div></td></tr>');
          $(".row-"+$selectedSubID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loadingSub = false;
          $(".loading-pane").addClass("hide");
        })

 }); 
   $(document).on('click', '.btn-del-attribute-value', function() {
        var table = $("#attribute-value-table tbody");
        var row = $(this).parent().parent();
        var rowcount = table.find("tr").length;

        if(rowcount > 1) {
            row.remove();
            rowcount--;
        } else {
            row.find(":input").val("");
        }
    });
    $("#row-attri_type").on("change", function() {
      if($("#row-attri_type").val() == "1"){
         $(".attribute_dropdown").removeClass("hide");
         $(".attribute_textbox").addClass("hide");
         $("#attri_value_pane").removeClass("hide");
      }else if($("#row-attri_type").val() == "2"){
         $(".attribute_textbox").removeClass("hide");
         $(".attribute_dropdown").addClass("hide");
         $("#attri_value_pane").removeClass("hide");
      }else if($("#row-attri_type").val() == "3"){
         $(".attribute_dropdown").removeClass("hide");
         $(".attribute_textbox").addClass("hide");
         $("#attri_value_pane").removeClass("hide");
      }
   });
   $("#attribute-value-table-body").on("click", ".btn-del-attribute-value", function() {
      $(this).closest("#attribute_status").val("0");
   });
   $("#row-country_code").on("change", function() {
      var id = $(this).val();
      var attri = $("#row-attri_id");
      attri.html(" ");
        if ($("#row-country_code").val()) {
          $.post("{{ url('admin/category/custom-attribute/fetch') }}", { id: id, _token: $_token }, function(response) {
            attri.html(response);
                }, 'json');
       }
   });
    $("#row-attri_id").on("change", function() { 
        var id = $("#row-attri_id ").val();
        var subcategory   = $("#sub_attri_id_container");
        $(".subcategory-pane").removeClass("hide");
        subcategory.html(" ");
        if ($("#row-attri_id").val()) {
          $.post("{{ url('admin/category/custom-attribute/fetch/subcategories') }}", { id: id, _token: $_token }, function(response) {
            subcategory.append(response);
             $('#row-sub_attri_id').multiselect({
              maxHeight: 400,
              enableCaseInsensitiveFiltering: true,
              selectAllValue: 'multiselect-all',
              selectAllText: 'Select All',
              includeSelectAllOption: true,
              numberDisplayed: 2
            });
            $('#row-sub_attri_id').multiselect();
          }, 'json');
       }
    });

  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Delete Custom Attribute', 'Are you sure you want to delete this attribute ?', "{{ url('admin/category/custom-attribute/delete') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Enable Custom Attribute', 'Are you sure you want to enable this attribute ?', "{{ url('admin/category/custom-attribute/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Disable Custom Attribute', 'Are you sure you want to disable this attribute ?', "{{ url('admin/category/custom-attribute/disable') }}",id);
  });
   $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
   }); 
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-danger btn-md btn-attr pull-right"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>

    </div>
    <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                                <option value="">All</option>
                                @foreach($countries as $row)
                                  <option value="{{$row->countryCode}}">{{$row->countryName}} [{{$row->countryCode}}]</option>
                                @endforeach
                      </select>
                  </div>
             </div>
            <!--   <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                      <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Enabled</option>
                                <option value="2">Disabled</option>
                      </select>
                  </div>
             </div> -->
    </div>
    </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="category-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.name" id="row-sort"><i class="fa"></i> Group</div></th>
    <!--               <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="sub_attributes.name" id="row-sort"><i class="fa"></i> Attribute Name</div></th> -->
                  <th class="text-right"><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.country_code" id="row-sort"><i class="fa fa-caret-up"></i> Country</div></th>
              <!--     <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="sub_attributes.status" id="row-sort"><i class="fa"></i> Status</div></th> -->
                 <!--  <th class="text-right"><small>Tools</small></th> -->
                </tr>
              </thead>
            <tbody id="rows">
             @foreach($rows as $row)
              <tr data-id="{{$row->id}}" class="{{$row->status == 2 ? 'disabledColor':''}}">
                <td><small><a class="btn btn-xs btn-table btn-expand" data-id="{{$row->id}}"><i class="fa fa-plus"></i></a>{{ $row->name }}</small></td>
                <td colspan="2" class="text-right"><small>{{ strtoupper($row->country_code) }}</small></td>
             <!--    <td><small>{{ $row->status == 1 ? 'Enabled':'Disabled' }}</small></td> -->
                <!--  <td>
                   <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                   <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>
                   <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>
                  @if($row->status == 1)
                   <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>
                  @else 
                   <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>
                  @endif
                </td> -->
              </tr>
            @endforeach
            </tbody>
       </table>
     </div>
      <input type="hidden" id="row-page" value="1">
       <input type="hidden" id="row-order" value="">
         <div class="row paddingFooter marginFooter">
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
            {!!$pages!!}
          </div>
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
            <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="100">100</option>
                <option value="200">200</option>
            </select>
          </div>
        </div>
  </div>
</div>
@stop
@include('admin.category-management.custom-attribute-management.form')