<!-- modal add user -->
  <div class="modal fade" id="modal-add-category" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/category/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD CATEGORY</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
               <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Country</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-country_code" name="country_code">
                        @foreach($countries as $row)
                          <option value="{{$row->countryCode}}">{{$row->countryName}}</option>
                        @endforeach
                  </select>
                </div>
                </div>
             <!--    <div class="form-group category-pane hide">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Category Type</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-cat_id" name="cat_id">
                   </select>
                </div>
                </div> -->
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="name" class="form-control" id="row-name" maxlength="30" placeholder="Enter your name...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Display Order</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="sequence" class="form-control" id="row-sequence" maxlength="30" placeholder="Enter your sequence...">
                </div>
                </div>
                 <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-6 control-label font-color">Category Option</label>
                <div class="col-sm-3 col-xs-4">
                    <label class="checkbox-inline"><input type="checkbox" name="category_type_one" id="category_type_one">Buy and Sell</label>
                </div>
                <div class="col-sm-3 col-xs-4">
                    <label class="checkbox-inline"><input type="checkbox" name="category_type_two" id="category_type_two">Biddable</label>
                </div>
                <div class="col-sm-3 col-xs-4">
                    <label class="checkbox-inline"><input type="checkbox" name="category_home_option" id="category_home_option">Home</label>
                </div>   
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="description" id ="row-description" placeholder="Enter your category description"></textarea>
                </div>
                </div> 
             <!--    <div class="form-group">
                    <label for="row-custom_attributes" class="col-sm-3 col-xs-5 control-label font-color">Custom Attributes</label>
                <div class="col-sm-9 col-xs-7">
                    <select class="form-control" id="row-custom_attributes" name="custom_attributes">
                      <option class="hide">Select</option>
                      @foreach($custom_attri as $row)
                      <option value="{{$row->id}}">{{$row->name}}</option>
                      @endforeach
                    </select>
                </div>
                </div>  -->

               <!--  <!-- <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>-->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="buy_and_sell_status" id="row-buy_and_sell_status" value="1">
                <input type="hidden" name="biddable_status" id="row-biddable_status" value="1">
                <input type="hidden" name="home" id="row-home" value="1">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
        <!-- modal add sub category -->
  <div class="modal fade" id="modal-sub-category" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/subcategory/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD SUB CATEGORY</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Parent Category</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control edit-view" id="row-cat_id" name="cat_id">
                        <option class="hide">Select Category</option>
                      @foreach($categories as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                      @endforeach
                  </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                    <input type="text" name="name" class="form-control" id="row-name" maxlength="30" placeholder="Enter your name...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="description" id ="row-description" placeholder="Enter your category description"></textarea>
                </div>
                </div>
               <!--  <!-- <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>-->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="cat_id" id="row-form-cat-id">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
         <div class="modal fade" id="modal-sub-category-two" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/subcategorytwo/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD SUB CATEGORY</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
      <!--           <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Parent Category</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control edit-view" id="row-cat_id" name="cat_id">
                        <option class="hide">Select Category</option>
                      @foreach($subcategories as $row)
                        <option value="{{$row->unique_id}}">{{$row->name}}</option>
                      @endforeach
                  </select>
                </div>
                </div> -->
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                    <input type="text" name="name" class="form-control" id="row-name" maxlength="30" placeholder="Enter your name...">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="description" id ="row-description" placeholder="Enter your category description"></textarea>
                </div>
                </div>
             <!--    <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Description</label>
                <div class="col-sm-9 col-xs-7">
                  <textarea class="form-control" rows="3" name="description" id ="row-description" placeholder="Enter your category description"></textarea>
                </div>
                </div> -->
               <!--  <!-- <div class="form-group">
                    <label for="row-email" class="col-sm-3 col-xs-5 control-label font-color">Email</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="email" class="form-control" id="row-email" maxlength="30" placeholder="Enter your email address...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password" class="col-sm-3 col-xs-5 control-label font-color">Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password" class="form-control" id="row-password" maxlength="30" placeholder="Enter your password...">
                </div>
                </div>
                <div class="form-group hide-view">
                    <label for="row-password_confirmation" class="col-sm-3 col-xs-5 control-label font-color">Confirm Password</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="password" name="password_confirmation" class="form-control" id="row-password_confirmation" maxlength="30" placeholder="Confirm your password">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-mobile" class="col-sm-3 col-xs-5 control-label font-color">Contact Number</label>
                <div class="col-sm-9 col-xs-7">
                  <input type="text" name="mobile" class="form-control" id="row-mobile" maxlength="30" placeholder="Enter your contact number">
                </div>
                </div>-->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="cat_id" id="row-form-cat-id">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>