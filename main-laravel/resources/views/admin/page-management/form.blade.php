    <div class="modal fade" id="modal-add-phrase" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/localization/phrase/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-country_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add a Phrase</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="phrase_name" class="form-control" id="row-localization_phrase_name" maxlength="30">
                </div>
                </div>
               <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Value</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="phrase_value" class="form-control" id="row-localization_phrase_value" maxlength="30">
                </div>
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
       

  