@extends('layout.master')
@section('scripts')
<script>
 $_token = '{{ csrf_token() }}';
 function refresh(){


   var loading = $("#modal-pages").find(".loading-pane");
   loading.removeClass("hide");
   var url = "{{ url('/') }}";
   var country_id = $("#row-filter_status").val();
   var container = $("#pages_container");
   
    $.post("{{ url('admin/pages/refresh') }}", { country_id:country_id, _token: $_token }, function(response) {
            // container.html('');
            // container.html(response.rows);
            $("#pages_container").find('[data-id=' + country_id + ']').removeClass("hide");
    }, 'json'); 
            $("#pages_container").find(".panel-collapse").removeClass("in");
    loading.addClass("hide");
    location.reload();
  }
  
  $(document).ready(function() {
      $('.summernote').summernote({
      toolbar: [
      // [groupName, [list of button]]
       ['style', ['bold', 'italic', 'underline', 'clear']],
        ['fontname', ['fontname', 'fontsize', 'color']],
        
        ['para', ['ul', 'ol', 'paragraph']],
        ['height', ['height', 'table']],
        ['link', ['link', 'picture', 'video']],
        ['fullscreen', ['fullscreen', 'codeview', 'help']],
      ]
      });
      var country_id = $("#row-filter_status").val();
      $("#pages_container").find('[data-id=' + country_id + ']').removeClass("hide");
  });

  function pagePerCountry(){
      var country_id = $("#row-filter_status").val();
      $("#pages_container").find(".panel-group").addClass("hide");
      $("#pages_container").find('[data-id=' + country_id + ']').removeClass("hide");
  }
  
  $("#modal-pages").find("form#help-center").submit(function(e) {
      var form = $("#modal-pages").find("form");
      var loading = $("#modal-pages").find("#load-form");
      $("#modal-pages").find("#load-form").removeClass("hide");
      var notice = $("#modal-pages").find("#form-notice");
      // stop form from submitting
      e.preventDefault();
      loading.removeClass('hide');
      // push form data
    $.ajax({
      type: "post",
      url: form.attr('action'),
      data: form.serialize(),
      dataType: "json",
      success: function(response) {
        if(response.error) {
          var errors = '<ul>';

          $.each(response.error, function(index, value) {
            errors += '<li>' + value + '</li>';
            
          });

          errors += '</ul>';

          status('Please correct the following:', errors, 'alert-danger', notice);
        }  else {

         $("#modal-pages").modal('hide');
          status(response.title, response.body, 'alert-success');

          refresh();
        }

        loading.addClass('hide');

      }
  });
});
  
  @if(session()->get('page_update'))
  $(document).ready(function(){
    status('', '{{session()->get('page_update')}}', 'alert-success');
  });
  
  @endif
  
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <!-- <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add-localization"><i class="fa fa-location-arrow"></i> Create</button> -->
        <!-- <button type="button" class="btn btn-warning btn-md btn-attr pull-right btn-phrase"><i class="fa fa-list"></i> Phrases</button>         -->
      </div>
    </div>
     <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 noPaddingLeft">
           
           </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 noPaddingRight">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_status" onchange="pagePerCountry()">
                                @foreach($countries as $row)
                                <option value="{{$row->id}}">{{$row->countryName}}</option>
                                @endforeach
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12" id="modal-pages">
            <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div> 
            <div id="pages_container"> 
           @foreach($rows as $row)
             <div class="panel-group hide" data-id="{{$row->country_id}}" id="accordion">
               {!! Form::open(array('url' => 'admin/pages/save', 'role' => 'form', 'class' => 'form-horizontal', 'files' => 'true')) !!}
                <div id="form-notice"></div>
                <div class="panel panel-default" style="margin-bottom: 10px;">
                  <div class="panel-heading">
                   <a data-toggle="collapse" data-parent="#accordion" href="#{{$row->id}}">
                    <h4 class="panel-title">
                      {{$row->name}}<i class="fa fa-chevron-down pull-right" aria-hidden="true"></i>
                    </h4>
                   </a> 
                  </div>
                  <div id="{{$row->id}}" class="panel-collapse collapse">
                    <div class="panel-body">
                          <input type="hidden" id="row-id" name="id" value="{{$row->id}}"> 
                          <textarea class="form-control summernote" id="row-content" name="content">{{$row->content}}</textarea>
                          <button class="btn btn-success btn-submit hide-view pull-right" type="submit" style="margin-top: 10px;"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                    </div>
                 </div>
              {!! Form::close() !!}
             </div>
           </div>
           @endforeach
        
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">

@stop
