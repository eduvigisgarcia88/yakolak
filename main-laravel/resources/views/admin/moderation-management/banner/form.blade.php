<!-- modal add user -->
  <div class="modal fade" id="modal-moderation-banner" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/moderation/banner/change-status', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-moderation-banner', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD PROMOTION</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                    <img src="#" id="row-photo" height="100" width="100%">
<!--                     <button type="button" class="btn btn-primary pull-right hide-view btn-change"><i class="fa fa-camera"></i> Change Photo</button> -->
                    <button type="button" class="btn btn-primary pull-right btn-cancel">Cancel</button>
                    <div class="uploader-pane">
                        <div class='change'>
                            <input name='photo' id='row-photo' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo'>
                        </div>
                     </div>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Title</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="title" class="form-control hide-view" id="row-title" maxlength="30">
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Placement</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="title" class="form-control hide-view" id="row-placement" maxlength="30">
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Placement Plan</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="title" class="form-control hide-view" id="row-placement-plan" maxlength="30">
                </div>               
                </div>
                <div class="form-group category-pane">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Category</label>
                   <div class="col-sm-9 col-xs-7">
                   <select class="form-control hide-view" id="row-category" name="category">
                          <option class="hide">Select</option>
                          @foreach($categories as $row)
                          <option value="{{$row->id}}">{{$row->name}}</option>
                          @endforeach
                  </select>
                </div>
                </div>
                <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Country</label>
                   <div class="col-sm-9 col-xs-7">
                    <select class="form-control hide-view" id="row-country_id" name="country">
                      <option class="hide">Select</option>
                      @foreach($countries as $row)
                        <option value="{{$row->id}}">{{$row->countryName}}</option>
                      @endforeach
                     </select>
                </div>
                </div>
          <!--       <div class="form-group">
                  <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">City</label>
                   <div class="col-sm-9 col-xs-7">
                    <select class="form-control" id="row-city" name="city">
                    
                     </select>
                </div>
                </div> -->

                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Start Date</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="duration" class="form-control hide-view" id="row-start_date" maxlength="30">
                </div>
                </div>
                  <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">End Date</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="duration" class="form-control hide-view" id="row-expiration" maxlength="30">
                </div>
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
              <!--   <button type="button" class="btn btn-success" data-id="" data-title="" id="btn-approve"><i class="fa fa-check"></i> Approve</button>
                <button type="button" class="btn btn-danger" data-id="" data-title="" id="btn-reject"><i class="fa fa-times"></i> Reject</button>
 -->
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  