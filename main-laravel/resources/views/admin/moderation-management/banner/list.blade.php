@extends('layout.master')
@section('scripts')
<script>
   $_token = '{{ csrf_token() }}';
   $(".content-wrapper").on("click", ".btn-view", function() {
   	  var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-moderation-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $("#row-city").html("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/moderation/banner/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Change Banner Status </strong>');
                      console.log($("#modal-moderation-banner").find("form").find("#btn-approve").data('id',response.id));
                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          if(field.length > 0) {
                              field.val(value);
                          }
                          if(index == "image"){
                            $("#row-photo").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+value);
                          }
                         
                          if(index == "get_banner_placement"){
                          	   $.each(value, function(pindex, pvalue) {
                          	   		if(pindex == "name"){
                          	   			$("#row-placement").val(pvalue);
                          	   		}
                          	   		
                          	   });
                          }
                          if(index == "get_banner_placement_plan"){
                          	   $.each(value, function(ppindex, ppvalue) {
                          	   			$("#row-placement-plan").val("$ "+value.price+" / "+value.period+" months");
                          	   });
                          }
                      });
                      $("#button-save").removeClass("hide");
                      $("#modal-moderation-banner").find('form').find(".hide-view").attr("disabled","disabled");
                      $("#modal-moderation-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');

   });

    $(".content-wrapper").on("click", ".btn-approve", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $("#modal-moderation-banner").modal("hide");
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Banner Status', 'Are you sure you want to approve ' + title + '</strong>?', "{{ url('admin/moderation/banner/approve') }}",id);
  	});
    $(".content-wrapper").on("click", ".btn-reject", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $("#modal-moderation-banner").modal("hide");
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Banner Status', 'Are you sure you want to reject ' + title + '</strong>?', "{{ url('admin/moderation/banner/reject') }}",id);
  	});
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
       <!--  <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>     -->
      </div>
    </div>
    <div class="table-responsive topPaddingC col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table">
            <thead>
                <tr>
                  <th><small>Title</small></th>
                  <th><small>Placement<small></th>
                  <th><small>Placement Plan<small></th>
                  <th><small>Start Date<small></th>
                  <th><small>End Date<small></th>
                  <th class="text-right"><small>Status</small></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($user_banners as $row)
            <tr data-id="{{$row->id}}" data-title="{{$row->title}}">
              	 <td>{{ $row->title }}</td>
                 <td>{{ $row->getBannerPlacement->name}}</td>
                 <td>${{ $row->getBannerPlacementPlan->price }} / {{$row->getBannerPlacementPlan->period}} months</td>
                 <td>{{ $row->start_date }}</td>
                 <td>{{ $row->expiration }}</td>
                 <td class="text-right">{{ $row->banner_status==1 ? 'Pending':'' }}{{ $row->banner_status==2 ? 'Approved':''}} {{$row->banner_status==3 ? 'Rejected':'' }}</td>
                <td>

                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject {{$row->banner_status == 3 ? 'hide':''}}" data-toggle="tooltip" title="Reject"><i class="fa fa-times" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve {{$row->banner_status == 2 ? 'hide':''}}" data-toggle="tooltip" title="Approve"><i class="fa fa-check" aria-hidden="true"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye" aria-hidden="true"></i></button>
                </td> 
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.moderation-management.banner.form')