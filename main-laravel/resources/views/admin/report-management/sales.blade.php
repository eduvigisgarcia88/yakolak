@extends('layout.master')
@section('scripts') 
<script>
$_token = '{{csrf_token()}}';
  
	$( document ).ready(function() {
		$('.dateFrom').datetimepicker({format: 'MM/DD/YYYY'}).on('dp.change', function (e) { date_range_refresh(); });
		$('.dateTo').datetimepicker({format: 'MM/DD/YYYY'}).on('dp.change', function (e) { date_range_refresh(); });
	});

   function refresh(){
		var loading = $('#load-ads_form');
		loading.removeClass('hide');
		var table = $('#rows');
		var body = "";

			$.post("{{ $refresh_route }}", { search: $('#row-search').val(), name: $("#row-name").val(), page: $("#row-page").val(), sort: $('#row-name').val(), per: $('#row-per').val(), date_to: $('#dateTo').val(), date_from: $('#dateFrom').val(), order: $('#row-order').val(), _token: $_token }, function(response) {
        
             if($('#indicator').val() == 1){
               $('#row-order').val((response.order == 'asc' ? 'desc' : 'asc'));
             }      
                
			        $.each(response.rows.data, function(index, row) {
			        body += '<tr data-id="' + row.id + '">' + 
							'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+row.invoice_no+'</small></td>'+             
							'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+row.alias+'</small></td>'+
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+row.email+'</small></td>'+
							'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+row.transaction_type+'</small></td>'+
							'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.extended_details == null ? 'N/A' : row.extended_details)+'</small></td>'+
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+row.cost+' '+row.currency+'</small></td>'+
							'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" class="text-right"><small>'+row.created_at+'</small></td>'+
							'</tr>';
			        });
			        table.html("");
			        table.html(body);
			        $('#row-pages').html(response.pages);
			        loading.addClass("hide");
        $('#indicator').val(0);
			}, 'json');
		}
  
	$(".content-wrapper").on('click', '.pagination a', function (event) {
	event.preventDefault();
		if ( $(this).attr('href') != '#' ) {
			$("html, body").animate({ scrollTop: 0 }, "fast");
			$("#row-page").val($(this).html());
			refresh();
		}
	});
  
  $(".content-wrapper").on('click', '.sort_by', function (event) {
	event.preventDefault();
      $("#row-name").val($(this).attr('data-sort'));
      $('#indicator').val(1);
			refresh();
	});
  
  $('#row-search').keydown(function (event) {
      var keypressed = event.keyCode || event.which;
      if (keypressed == 13) {
          refresh();
      }
  });
  
  function date_range_refresh() {
    if ($('#dateFrom').val() && $('#dateTo').val()) {
      refresh();
    }
  }
	
	$("#download_pdf").click(function(){
		if ($('#dateFrom').val() && $('#dateTo').val()) {
			var base_url = "{{url('pdfreportsales')}}";
			var	from = $(document).find('#dateFrom').val().toString();
			var	to = $(document).find('#dateTo').val().toString();
			var	get_param = '?from='+from.replace(/\//g, '-')+'&to='+to.replace(/\//g, '-');
			var	endpoint = base_url+get_param;
			
			window.open(endpoint, '_blank');
      
    } else {
			status('', 'Please select a date range', 'alert-danger');
		}
	});
  
</script>
@stop
@section('content')
<div class="content-wrapper">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3 class="noMargin"><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button id="download_pdf" type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-file-pdf-o"></i> Download as PDF</button>
      </div>
       <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
              <div class="input-group borderZero">
                <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                <input type="text" class="form-control borderZero" id="row-search">
              </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class='input-group date dateFrom'>
                                <span class="input-group-addon borderZero" id="basic-addon1">From</span>
                                <input type='text' class="form-control" id="dateFrom"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
             </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class='input-group date dateTo'>
                                <span class="input-group-addon borderZero" id="basic-addon1">To</span>
                                <input type='text' class="form-control" id="dateTo"/>
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                        </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-condensed backend-generic-table" id="reportsManagement">
          <thead>
            <tr>
               <th class="sort_by" data-sort="invoice_no"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> Invoice No.</div></th>
               <th class="sort_by" data-sort="alias"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> User</div></th>
              <th class="sort_by" data-sort="email"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> Email</div></th>
               <th class="sort_by" data-sort="transaction_type"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> Type</div></th>
               <th class="sort_by" data-sort="extended_details"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> Details</div></th>
              <th class="sort_by" data-sort="cost"><div data-toggle="tooltip" title="Click to Sort" data-placement="right"><i class="fa"></i> Cost</div></th>
               <th class="text-right sort_by" data-sort="created_at"><div data-toggle="tooltip" title="Click to Sort" data-placement="left"><i class="fa "></i> Created At</div></th>
            </tr>
          </thead>
          <tbody id="rows" class="table-hover">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}" class="{{($row->status == 2 ? 'disabledColor' : '') }}">
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->invoice_no}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->alias}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->email}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->transaction_type}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->extended_details ?: 'N/A'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->cost.' '.$row->currency}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;" class="text-right"><small>{{$row->created_at}}</small></td>
              </tr>
            @endforeach
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
     <input type="hidden" id="row-name" value="">
     <input type="hidden" id="indicator" value="">
       <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="refresh()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
@stop
