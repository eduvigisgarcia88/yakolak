@extends('layout.master')
@section('scripts')
<script type="text/javascript">
	google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month','Registration'],
          ['{{Carbon\Carbon::today()->subMonths(3)->format('F Y')}}',  {{$user_reg_count_three}}],
          ['{{Carbon\Carbon::today()->subMonths(2)->format('F Y')}}',  {{$user_reg_count_two}}],
          ['{{Carbon\Carbon::today()->subMonths(1)->format('F Y')}}',  {{$user_reg_count_one}}],
          ['{{Carbon\Carbon::today()->format('F Y')}}',  {{$user_reg_count_today}}]
        ]);

        var options = {
          title: 'User Registration (Past 30 Days)',
          legend: {position: 'none', maxLines: 3},
          chartArea: {width: '75%', height: '75%'},
          // hAxis: {title: 'Year',  titleTextStyle: {color: '#666'}},
          vAxis: {maxValue: {{$user_reg_max == 0 ? 5 : $user_reg_max}}, minValue: {{$user_reg_min}}, format: "0", gridlines: { count: 5 },}
        };

        var chart = new google.visualization.AreaChart(document.getElementById('userRegistration-chart'));
        chart.draw(data, options);
      }

      google.charts.setOnLoadCallback(drawChart2);

      function drawChart2() {
        var data2 = google.visualization.arrayToDataTable([
          ['Month','Ads'],
          ['{{Carbon\Carbon::today()->subMonths(3)->format('F Y')}}',  {{$ads_count_three}}],
          ['{{Carbon\Carbon::today()->subMonths(2)->format('F Y')}}',  {{$ads_count_two}}],
          ['{{Carbon\Carbon::today()->subMonths(1)->format('F Y')}}',  {{$ads_count_one}}],
          ['{{Carbon\Carbon::today()->format('F Y')}}',  {{$ads_count_today}}]
        ]);

        var options2 = {
          title: 'Ads (Past 30 Days)',
          legend: {position: 'none', maxLines: 3},
          chartArea: {width: '75%', height: '75%'},
          colors: ['red'],
          // hAxis: {title: 'Year',  titleTextStyle: {color: '#666'}},
          vAxis: {maxValue: {{$ads_max == 0 ? 5 : $ads_max}}, minValue: {{$ads_min}}, format: "0", gridlines: { count: 5 },}
        };

        var chart2 = new google.visualization.AreaChart(document.getElementById('ads-chart'));
        chart2.draw(data2, options2);
      }

      $(window).resize(function(){
				drawChart();
				drawChart2();
			});
</script>
@stop
@section('content')
<div class="content-wrapper">
	<div class="container-fluid bgGray">
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12 bgWhite bottomMarginC borderBottom">
				<h3 class="borderbottomLight bottomPaddingC"><i class="fa fa-tachometer"></i> {{$title}}</h3>
				<div class="row">
		<div class="col-sm-6">
		<div id="userRegistration-chart" class="chart"></div>
		</div>
		<div class="col-sm-6">
		<div id="ads-chart" class="chart"></div>
		</div>
		</div>
			</div>
		</div>
		<div class="col-sm-12">
		<div class="row">
		<div class="col-sm-6">
		<div class="panel panel-default borderZero removeBorder">
		<div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Recent Search</span>
      </div>
		  <div class="panel-body">
		  	@foreach($recent_search as $row)
		  	<p class="borderbottomLight bottomPadding"> {{ $row->keyword }} </p>
		  	@endforeach
		  </div>
		</div>
		</div>
		<div class="col-sm-6">
		<div class="panel panel-default borderZero removeBorder">
		<div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Latest Comments</span>
      </div>
		  <div class="panel-body">
		  	@foreach($latest_comments_per as $row)
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock borderbottomLight">
              <div class="commenterImage">
                <a href="{{ url('/').'/'.$row->alias }}"><img src="{{ url('uploads').'/'.$row->photo }}" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"></a>
              </div>
              <div class="statisticcommentText">
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="panelTitleB bottomPadding ellipsis">{{ $row->title }}</div></a>
                <p class="normalText ellipsis"><a class="" style="color:#333;" href="{{ url('/').'/'.($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}">by: <span class="">{{ $row->name }}</span></a></p>
              </div>
            </div>
          </div>
          @endforeach
		  </div>
		</div>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-6">
		<div class="panel panel-default borderZero removeBorder">
		<div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">List of Top Ads</span>
      </div>
		  <div class="panel-body nobottomPadding">
		  	@foreach($top_ads as $row)
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock borderbottomLight">
              <div class="commenterImage">
                <a href="{{ url('/').'/'.$row->alias }}"><img src="{{ url('uploads').'/ads/thumbnail/'.$row->photo }}" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"></a>
              </div>
              <div class="statisticcommentText">
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="panelTitleB bottomPadding ellipsis">{{ $row->title }}<span class="pull-right normalText grayText">Number of Visits: {{ $row->ad_visits }}</span></div></a>
                <p class="normalText ellipsis"><a class="" style="color:#333;" href="{{ url('/').'/'.($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}">by: <span class="">{{ $row->username }}</span></a></p>
              </div>
            </div>
          </div>
          @endforeach
		  </div>
		  </div>
		</div>
				<div class="col-sm-6">
		<div class="panel panel-default borderZero removeBorder">
		<div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">List of Top Bids</span>
      </div>
		  <div class="panel-body nobottomPadding">
		  	@foreach($top_bid as $row)
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock borderbottomLight">
              <div class="commenterImage">
                <a href="{{ url('/').'/'.$row->alias }}"><img src="{{ url('uploads').'/ads/thumbnail/'.$row->photo }}" class="avatar" alt="user profile image" style="width: 35px; height: 35px;"></a>
              </div>
              <div class="statisticcommentText">
                <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"><div class="panelTitleB bottomPadding ellipsis">{{ $row->title }}<span class="pull-right normalText grayText">Number of Visits: {{ $row->ad_visits }}</span></div></a>
                <p class="normalText ellipsis"><a class="" style="color:#333;" href="{{ url('/').'/'.($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias }}">by: <span class="">{{ $row->username }}</span></a></p>
              </div>
            </div>
          </div>
          @endforeach
		  </div>
		</div>
		</div>
		</div>
		</div>
		<!-- <div class="table-responsive">
			<table class="table table-hover">
			    <thead>
			      <tr>
			        <th>Firstname</th>
			        <th>Lastname</th>
			        <th>Email</th>
			        <th>Tools</th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
			        <td>John</td>
			        <td>Doe</td>
			        <td>john@example.com</td>
			        <td>
			        	<button type="button" class="btn btn-danger btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
			       		<button type="button" class="btn btn-primary btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
			       		<button type="button" class="btn btn-primary btn-sm btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
			   		</td>		
			      </tr>
		 	 </table>
		 </div> -->
	</div>
</div>
@stop