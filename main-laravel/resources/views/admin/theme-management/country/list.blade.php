@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  function refresh(){
    $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
        $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
              '<td><small>'+ row.style_name +'</small></td>'+
              '<td><small>'+ row.style_path +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
 
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '</tr>';      
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
  function search(){
    refresh();
  }
  function maxAds(){
    refresh();
  }
  $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
              '<td><small>'+ row.style_name +'</small></td>'+
              '<td><small>'+ row.style_path +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
 
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '</tr>';     
        });
        
        $("#modal-add-theme-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

      }
  });
  $("#theme-country-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#theme-country-table").find("i").removeClass('fa-caret-up');
          $("#theme-country-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
           if($order == "asc"){
              $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
           }else{
              $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
           } 
          
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {

          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
              '<td><small>'+ row.style_name +'</small></td>'+
              '<td><small>'+ row.style_path +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
 
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '</tr>';     
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
            
          }else{
              $('#row-order').val("asc");
          }
       
      }, 'json');
  

  });

  $(".content-wrapper").on('click', '.btn-add', function () {
      $("#modal-add-theme-country").find("form").trigger("reset");
      $("#modal-add-theme-country").find("form").find("#row-id").val("");
      $("#modal-add-theme-country").find(".alert").remove();
      $("#modal-add-theme-country").find("form").find(".modal-title").html('<i class="fa fa-plus"></i> Add Theme</h4>');
      $("#modal-add-theme-country").modal("show");
  });
  $(".content-wrapper").on('click', '.btn-edit', function () {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-theme-country").find('form').trigger("refresh");
        $("#modal-add-theme-country").find(".alert").remove();
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/settings/theme/country/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Theme ['+response.name+']</strong>');
                      // output form data
                      $.each(response, function(index, value) {
        
                        var field = $("#row-" + index);
                          // field exists, therefore populate   
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                      // show form
                      $("#modal-add-theme-country").modal('show');
                      $("#modal-add-theme-country").find('form').find("#row-id").val(response.id);
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
        }, 'json');
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Theme', 'Are you sure you want to enable ?', "{{ url('admin/settings/theme/country/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Theme', 'Are you sure you want to disable ?', "{{ url('admin/settings/theme/country/disable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Theme', 'Are you sure you want to delete ?', "{{ url('admin/settings/theme/country/remove') }}",id);
  });
  $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
   });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-css3"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-css3"></i> Add</button>    
      </div>
    </div>
     <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 noPaddingLeft">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 noPaddingRight">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                      <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Enabled</option>
                                <option value="2">Disabled</option>
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="theme-country-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="status" id="row-sort"><i class="fa"></i> Status</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="name" id="row-sort"><i class="fa"></i> Name</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="style_name" id="row-sort"><i class="fa"></i> StyleSheet</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="style_path" id="row-sort"><i class="fa"></i> StyleSheet Path</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created Date</div></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($rows as $row)
            <tr data-id="{{$row->id}}" class="{{ ($row->status == 2 ? 'disabledColor':'') }}">
                <td><small>{{$row->status == 1 ? 'Enabled':''}}{{$row->status == 2 ? 'Disabled':''}}</small></td>
                <td><small>{{$row->name}}</small></td>
                <td><small>{{$row->style_name}}</small></td>
                <td><small>{{$row->style_path}}</small></td>
                <td><small>{{$row->created_at}}</small></td>
                <td>
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>
                    @if($row->status == 1)
                      <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>
                    @else
                      <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>
                    @endif
                  
                </td> 
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.theme-management.country.form')