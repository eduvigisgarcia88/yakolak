<div class="modal fade hide" id="modal-form_ad_management" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="ad-management-load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><i class="fa fa-plus"></i> Add User Plan</h4>
        </div>
         
            <div id="form-ad_management_notice" class=""></div>
            {!! Form::open(array('url' => 'admin/ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-ad_management_save', 'files' => true)) !!}
            <div class="modal-body"> 
{{--                  <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#basic-info" id="basic-tab">Basic Information</a></li>
                  <li><a data-toggle="tab" href="#additional-info" id="additional-tab" class="auction-tab {{ ($row->ads_type_id != 2 ?'hide':'') }}">Additional Information</a></li>
                </ul> --}}
               <div class="">
                  <div id="basic-info" class="tab-pane fade in active">
              <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
          <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="form-group">
                   <label for="row-name" class="col-sm-3 col-xs-3 font-color">Ads Type</label>
                      <div class="col-sm-9 col-xs-9">
                             <select type="text" class="form-control borderZero readonly-view" id="row-ads_type_id" name="ads_type_id">
                                  @foreach ($ad_types as $ad_type)
                                  <option value="{{ $ad_type->id}}" {{($ad_type->id==$row->ads_type_id ? 'selected' : '')}}>{{ $ad_type->name }}</option>
                                  @endforeach
                             </select> 
                      </div>
                </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12">
                <div class="form-group">
                   <label for="row-name" class="col-sm-3 col-xs-3 font-color">Parent Category</label>
                        <div class="col-sm-9 col-xs-9">
                               <select type="text" class="form-control borderZero readonly-view" id="row-parent_category_id" name="parent_category">
                              </select> 
                        </div>
                </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12 category-container">
                <div class="form-group">
                       <label for="row-name" class="col-sm-3 col-xs-3 font-color">Category</label>
                            <div class="col-sm-9 col-xs-9">
                                 <select class="form-control borderZero readonly-view" id="row-category_id" name="category">
                                 </select>
                            </div>
                 </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12 subcategory-container">
                <div class="form-group">
                       <label for="row-name" class="col-sm-3 col-xs-3 font-color">Sub Category</label>
                            <div class="col-sm-9 col-xs-9">
                                 <select class="form-control borderZero readonly-view" id="row-sub_category_id" name="subcategory">
                                 </select>
                            </div>
                 </div>
          </div>
          <div class="col-lg-12 col-md-12 col-xs-12">
              <div class="form-group">
                <label for="row-name" class="col-sm-3 col-xs-3 font-color">Title</label>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                         <input type="text" class="form-control borderZero readonly-view" id="row-title" name="title">
                    </div>
              </div>
          </div>
                 <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-3 font-color">Description</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                        <textarea class="form-control borderZero readonly-view" id="row-description" name="description" rows="10" cols="50"></textarea>
                       </div>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-3 font-color">Price</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                              <input class="form-control borderZero readonly-view" type="text" id="row-price"  name="price" value=''>
                       </div>
                   </div>
                </div>
             <div class="col-lg-12 col-md-12 col-xs-12">
             <div class="form-group">
               <label for="row-name" class="col-sm-3 col-xs-3 font-color">Photos</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                           <div class="thumbnails">
                             <div id="list-ad-pictures">
                            </div>
                            <input style="opacity:0; display:none;" class="photo_upload" type="file" name="photo">
                       </div>
                     </div>
                </div>
            </div>
           <div class="col-lg-12 col-md-12 col-xs-12">
               <h5 class="inputTitle borderbottomLight bottomPadding" id="custom-attribute-container">Custom Attribute</h5>
              <div id="custom-attributes-pane">
              </div>
           </div>
           <div class="col-lg-12 col-md-12 col-xs-12">
               <h5 class="inputTitle borderbottomLight bottomPadding" id="custom-attribute-container">Other Information</h5>
              <div class="form-group">
               <label for="row-name" class="col-sm-3 col-xs-3 font-color">Country</label>
                  <div class="col-sm-9">
                    <select class="form-control borderZero readonly-view" id="row-country_id" name="country">
                      <option class="hide">Select</option>
                      @foreach($countries as $country)
                         <option value="{{ $country->id}}" {{ ($row->country_id == $country->id ? 'selected' : 'none') }}>{{ $country->countryName }} {{$row->country_id}}</option>                 
                      @endforeach
                    </select>
                  </div>
                </div>
            </div>
          <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="form-group">
               <label for="row-name" class="col-sm-3 col-xs-3 font-color">City or State</label>
                <div class="col-md-9 col-sm-9 col-xs-9">
                <select class="form-control borderZero readonly-view" id="row-city_id" name="city">
                </select>

              </div>
              </div>
            </div>
                 <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-3 font-color">Address</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                              <input class="form-control borderZero readonly-view" type="text" id="row-address"  name="address" value=''>
                       </div>
                   </div>
                </div>
                    <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-3 font-color">Youtube</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="embed" name="youtube" class="form-control borderZero readonly-view" id="row-youtube">
                       </div>
                   </div>
                </div>
             </div>
             </div>
        </div>

               <div class="hide" id="auction-type-pane">
                    <h5 class="inputTitle borderbottomLight">Auction Information </h5>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-bid_limit_amount" name="bid_limit_amount">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-bid_start_amount" name="bid_start_amount">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Minimum Allowed Bid</h5>
                      <div class="col-sm-9">
                        <input type="number" class="form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid" placeholder="">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                      <div class="col-sm-9">
                        <div class="col-sm-6 noleftPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8">
                              <input type="number" class="form-control borderZero" id="row-bid_duration_start" name="bid_duration_start" placeholder="">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 norightPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8"> 
                              <select id="row-bid_duration_dropdown_id" name="bid_duration_dropdown_id" class="form-control borderZero">
                                <option class="hide" selected>Select</option>
                                <option value="3" selected="">Months</option>
                                <option value="2" selected="">Weeks</option>
                                <option value="1" selected="">Days</option>
                              </select>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>
        </div>
        </div>
        <div class="modal-footer">
        <input type="hidden" name="id" id="row-id" value="">
        <input type="hidden" name="cat_id" id="row-cat_id" value="">
        <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
        <input type="hidden" name="no_preview" id="no_preview" value="">
        <input type="hidden" id="ad_status_no" value="">
        <span class="read_remove">
        <button type="button" class="btn pull-left btn-enable hide"><i class="fa fa-unlock"></i> Enable</button>
        <button type="button" class="btn pull-left btn-disable hide"><i class="fa fa-lock"></i> Disable</button>
        <button type="button" class="btn pull-left btn-blocked hide"><i class="fa fa-shield"></i> Block</button>
        <button type="button" class="btn pull-left btn-unblocked hide"><i class="fa fa-shirtsinbulk"></i> Unblock</button>
        <button type="button" class="btn pull-left btn-flag hide"><i class="fa fa-flag"></i> Flag</button>
        <button type="button" class="btn pull-left btn-unflag hide"><i class="fa fa-flag-o"></i> Unflag</button>
        <button type="button" class="btn pull-left btn-delete-ads"><i class="fa fa-trash-o"></i> Delete</button>
        </span>
        <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
        <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
  
      </div>
    </div>
  </div>



    <div class="modal fade" id="modal-ads-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'admin/ads/delete', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
      <div class="modal-header modal-delete">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Ads</h4>
      </div>
      <div class="modal-body">
        <div id="form-notice"></div>
        <div class="form-group">
        <div class="col-lg-12">
          Do you want to delete? 
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="id" id="row-id" value="">
        <input type="hidden" name="title" id="row-title" value="">
        <button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
        <button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

<div class="modal fade" id="modal-form-ad-settings" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-mid">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-cog"></i> Ad Settings</h4>
        </div>
          <div id="form-ad_settings_notice"></div>      
          {!! Form::open(array('url' => 'admin/ads/management-settings/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-ad_settings_save_form', 'files' => true)) !!}
            <div class="modal-body"> 
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                  <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-4 col-xs-4 font-color">Basic Ad will expire after</label>
                       <div class="col-sm-4 col-xs-4">
                         <input type="text" class="form-control" id="row-basic_ad_expiration_start" name="basic_ad_expiration_start" value="">
                       </div>
                       <div class="col-sm-4 col-xs-4">
                         <select name="basic_ad_expiration_end" id="row-basic_ad_expiration_end" class="form-control">
                            <option value="hours" selected>hours</option>
                            <option value="days">days</option>
                            <option value="months">months</option>
                            <option value="years">years</option>
                            <option value="never">never</option>
                         </select>
                       </div>
                   </div>
                   <div class="form-group">
                    <label for="row-name" class="col-sm-4 col-xs-4 font-color">Auction Timer Increment</label>
                       <div class="col-sm-4 col-xs-4">
                         <input type="text" class="form-control" id="row-bid_adding_time_start" name="bid_adding_time_start" value="">
                       </div>
                       <div class="col-sm-4 col-xs-4">
                         <select name="bid_adding_time_end" id="row-bid_adding_time_end" class="form-control">
                            <option value="minutes" selected>minutes</option>
                            <option value="hours">hours</option>
                            <option value="days">days</option>
                            <option value="months">months</option>
                            <option value="years">years</option>
                         </select>
                       </div>
                   </div>
               <!--    <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 font-color">Bidder auto increment value</label>
                       <div class="col-sm-4 col-xs-4">
                         <input type="text" class="form-control" id="row-bid_inc_value" name="bid_inc_value" value="">
                       </div>
                   </div> -->
                  <div class="form-group">
                    <label for="row-name" class="col-sm-4 col-xs-4 font-color">Notify vendor on expiring Bid</label>
                       <div class="col-sm-4 col-xs-4">
                         <input type="text" class="form-control" id="row-expiring_auction_noti_time_start" name="expiring_auction_noti_time_start" value="">
                       </div>
                       <div class="col-sm-4 col-xs-4">
                         <select name="expiring_auction_noti_time_end" id="row-expiring_auction_noti_time_end" class="form-control">
                            <option value="minutes" selected>minutes</option>
                            <option value="hours">hours</option>
                            <option value="days">days</option>
                            <option value="months">months</option>
                            <option value="years">years</option>
                         </select>
                       </div>
                   </div>
                </div>

   </div>
   </div>
   </div>
        <div class="modal-footer">

          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>
  <!-- modal add spam and bots -->
  <div class="modal fade" id="modal-spam-and-bot" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/ads/spam-and-bots/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-spam-and-bot', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD SPAM AND BOT KEYWORD</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Keyword</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="email[]" class="borderZero inputBox fullSize restricted_keywords">
                </div>               
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success btn-hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  