
@extends('layout.master')
@section('scripts')
<script type="text/javascript">
function refresh() {
  $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order,status:$status, search: $search,country:$country, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
         $.each(response.rows.data, function(index, row) {

          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.feature == 1  ? 'featuredColor' : '')+''+ (row.feature == 3  ? 'blockedColor' : '')+''+ (row.feature == 4  ? 'unapprovedColor' : '')+'">' + 
              '<td><small>'+row.transaction_no+'</small></td>'+
              '<td><small><center>'+(row.payment_method == 1 ? 'Rewards':'Paid')+'</center></td>'+
              '<td><small>' + (row.feature == '1'?'Approved':'') + (row.feature == '2'?'Pending':'')+(row.feature == '3'?'Decline':'')+(row.feature == '4'?'UnApproved':'')+ '</small></td>' +
              '<td><small>'+(row.ads_type_id == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.user_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cat_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.duration +' '+row.metric_name+'</small></td>' +
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cost +' '+(row.payment_method == 1 ? 'Points':'USD')+'</small></td>' +
              '<td><small>' + row.feature_created_at  + '</small></td>' +
              '<td>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature == 3  ? 'disabled':'')+'><i class="fa fa-times"></i></button>'+
                // (row.status != '4'? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam"><i class="fa fa-exclamation"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unmark-spam"><i class="fa fa-check"></i></button>') +

                // (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa '+ (row.status == '1'  ? 'fa-adjust' : 'fa-check')+'"></i></button>':'') +
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature != 2  ? 'disabled':'')+'><i class="fa fa-ban"></i></button>'+
                (row.feature != 1  ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" '+ ([2, 3].includes(parseInt(row.feature)) ? '' : 'disabled') +'><i class="fa fa-check"></i></button>':
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" '+ (row.feature != 1 ? 'disabled' : '') +'><i class="fa fa-close"></i></button>') +
 
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +       
              '</td></tr>';
        });
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }



    function search() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

    $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

      $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.feature == 1  ? 'featuredColor' : '')+''+ (row.feature == 3  ? 'blockedColor' : '')+''+ (row.feature == 4  ? 'unapprovedColor' : '')+'">' + 
              '<td><small>'+row.transaction_no+'</small></td>'+
              '<td><small><center>'+(row.payment_method == 1 ? 'Rewards':'Paid')+'</center></td>'+
              '<td><small>' + (row.feature == '1'?'Approved':'') + (row.feature == '2'?'Pending':'')+(row.feature == '3'?'Decline':'')+(row.feature == '4'?'UnApproved':'')+ '</small></td>' +
              '<td><small>'+(row.ads_type_id == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.user_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cat_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.duration +' '+row.metric_name+'</small></td>' +
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cost +' '+(row.payment_method == 1 ? 'Points':'USD')+'</small></td>' +
              '<td><small>' + row.feature_created_at  + '</small></td>' +
              '<td>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature == 3  ? 'disabled':'')+'><i class="fa fa-times"></i></button>'+
                // (row.status != '4'? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam"><i class="fa fa-exclamation"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unmark-spam"><i class="fa fa-check"></i></button>') +

                // (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa '+ (row.status == '1'  ? 'fa-adjust' : 'fa-check')+'"></i></button>':'') +
                
                  '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature != 2  ? 'disabled':'')+'><i class="fa fa-ban"></i></button>'+
                (row.feature != 1  ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" '+ ([2, 3].includes(parseInt(row.feature)) ? '' : 'disabled') +'><i class="fa fa-check"></i></button>':
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" '+ (row.feature != 1 ? 'disabled' : '') +'><i class="fa fa-close"></i></button>') +
 
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +       
              '</td></tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
    }
     function maxAds() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

      $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.feature == 1  ? 'featuredColor' : '')+''+ (row.feature == 3  ? 'blockedColor' : '')+''+ (row.feature == 4  ? 'unapprovedColor' : '')+'">' + 
              '<td><small>'+row.transaction_no+'</small></td>'+
              '<td><small><center>'+(row.payment_method == 1 ? 'Rewards':'Paid')+'</center></td>'+
              '<td><small>' + (row.feature == '1'?'Approved':'') + (row.feature == '2'?'Pending':'')+(row.feature == '3'?'Decline':'')+(row.feature == '4'?'UnApproved':'')+ '</small></td>' +
              '<td><small>'+(row.ads_type_id == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.user_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cat_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.duration +' '+row.metric_name+'</small></td>' +
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cost +' '+(row.payment_method == 1 ? 'Points':'USD')+'</small></td>' +
              '<td><small>' + row.feature_created_at  + '</small></td>' +
              '<td>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature == 3  ? 'disabled':'')+'><i class="fa fa-times"></i></button>'+
                // (row.status != '4'? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam"><i class="fa fa-exclamation"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unmark-spam"><i class="fa fa-check"></i></button>') +

                // (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa '+ (row.status == '1'  ? 'fa-adjust' : 'fa-check')+'"></i></button>':'') +
                
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature != 2  ? 'disabled':'')+'><i class="fa fa-ban"></i></button>'+
                (row.feature != 1  ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" '+ ([2, 3].includes(parseInt(row.feature)) ? '' : 'disabled') +'><i class="fa fa-check"></i></button>':
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" '+ (row.feature != 1 ? 'disabled' : '') +'><i class="fa fa-close"></i></button>') +
 
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +       
              '</td></tr>';
        });
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
    }
     $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

      $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.feature == 1  ? 'featuredColor' : '')+''+ (row.feature == 3  ? 'blockedColor' : '')+''+ (row.feature == 4  ? 'unapprovedColor' : '')+'">' + 
              '<td><small>'+row.transaction_no+'</small></td>'+
              '<td><small><center>'+(row.payment_method == 1 ? 'Rewards':'Paid')+'</center></td>'+
              '<td><small>' + (row.feature == '1'?'Approved':'') + (row.feature == '2'?'Pending':'')+(row.feature == '3'?'Decline':'')+(row.feature == '4'?'UnApproved':'')+ '</small></td>' +
              '<td><small>'+(row.ads_type_id == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.user_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cat_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.duration +' '+row.metric_name+'</small></td>' +
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cost +' '+(row.payment_method == 1 ? 'Points':'USD')+'</small></td>' +
              '<td><small>' + row.feature_created_at  + '</small></td>' +
              '<td>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature == 3  ? 'disabled':'')+'><i class="fa fa-times"></i></button>'+
                // (row.status != '4'? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam"><i class="fa fa-exclamation"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unmark-spam"><i class="fa fa-check"></i></button>') +

                // (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa '+ (row.status == '1'  ? 'fa-adjust' : 'fa-check')+'"></i></button>':'') +
                
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature != 2  ? 'disabled':'')+'><i class="fa fa-ban"></i></button>'+
                (row.feature != 1  ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" '+ ([2, 3].includes(parseInt(row.feature)) ? '' : 'disabled') +'><i class="fa fa-check"></i></button>':
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" '+ (row.feature != 1 ? 'disabled' : '') +'><i class="fa fa-close"></i></button>') +
 
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +       
              '</td></tr>';
        });      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
      }
    });
    $("#userManagement").on("click", "#row-sort", function() {
          $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#userManagement").find("i").removeClass('fa-caret-up');
          $("#userManagement").find("i").removeClass('fa-caret-down');
          $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");

          $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
          table.html("");
          console.log(response);
          var body = "";

           $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.feature == 1  ? 'featuredColor' : '')+''+ (row.feature == 3  ? 'blockedColor' : '')+''+ (row.feature == 4  ? 'unapprovedColor' : '')+'">' + 
              '<td><small>'+row.transaction_no+'</small></td>'+
              '<td><small><center>'+(row.payment_method == 1 ? 'Rewards':'Paid')+'</center></td>'+
              '<td><small>' + (row.feature == '1'?'Approved':'') + (row.feature == '2'?'Pending':'')+(row.feature == '3'?'Decline':'')+(row.feature == '4'?'UnApproved':'')+ '</small></td>' +
              '<td><small>'+(row.ads_type_id == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.user_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cat_name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.duration +' '+row.metric_name+'</small></td>' +
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.cost +' '+(row.payment_method == 1 ? 'Points':'USD')+'</small></td>' +
              '<td><small>' + row.feature_created_at  + '</small></td>' +
              '<td>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature == 3  ? 'disabled':'')+'><i class="fa fa-times"></i></button>'+
                // (row.status != '4'? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-mark-spam"><i class="fa fa-exclamation"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unmark-spam"><i class="fa fa-check"></i></button>') +

                // (row.status != '2'?'<button type="button" class="btn btn-default btn-sm btn-attr pull-right '+ (row.status == '1'  ? 'btn-disable' : 'btn-enable')+'" data-toggle="modal" data-target="#"><i class="fa '+ (row.status == '1'  ? 'fa-adjust' : 'fa-check')+'"></i></button>':'') +
                
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" '+(row.feature != 2  ? 'disabled':'')+'><i class="fa fa-ban"></i></button>'+
                (row.feature != 1  ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" '+ ([2, 3].includes(parseInt(row.feature)) ? '' : 'disabled') +'><i class="fa fa-check"></i></button>':
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" '+ (row.feature != 1 ? 'disabled' : '') +'><i class="fa fa-close"></i></button>') +
 
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +       
              '</td></tr>';
        });
      
            
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

          if(response.order == "asc"){
              $("#row-order").val("desc");
              $(this).find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          }else{
              $('#row-order').val("asc");
              $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          }

            if($order == "asc"){
            if($sort != "created_at"){
              $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');
            }else{
              $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
         }else{
            if($sort != "created_at"){
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
             }else{
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down'); 
            }
          
           
         } 

          }, 'json');
    });

    $(".content-wrapper").on("click", ".btn-add", function() {
           $_token = $("#row-token").val();
           $("#modal-form-add").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add Advertisement</strong>');
           $("#modal-form-add").modal('show');
        
    });
 $(".content-wrapper").on("click", ".btn-edit", function() {
      $("#modal-form_ad_management").removeClass("hide");
      $("#form-ad_management_notice").addClass("hide");
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
            
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                 if(index== "parent_category"){
                   $("#row-parent_category_id").html(response.parent_category);
                 }
                 if(index=="category_id"){ 
                    if(response.category == null){
                       $(".category-container").addClass("hide");
                    }else{
                       $(".category-container").removeClass("hide");
                       $("#row-category_id").html(response.category); 
                    } 
                 }
                 if(index=="sub_category_id"){
                      console.log(response.sub_category);
                    if(response.sub_category == null){
                       $(".subcategory-container").addClass("hide");
                    }else{
                       $(".subcategory-container").removeClass("hide");
                       $("#row-sub_category_id").html(response.sub_category); 
                    } 
                 }

              });
   
        var cat_id = $("#row-cat_id").val();
        var country_id = $("#row-country_id").val();
        // var subcategory = $("#row-sub_category_id");
        // var category = $("#row-category_id");
        var cusAttr = $("#custom-attributes-pane");
        var fetchcity = $("#row-city_id");

      
        // if ($("#row-ads_type_id").val()) {
        // $_token = $("#row-token").val();
        //   subcategory.html("");
        //   $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
        //     subcategory.html(response);
        //   }, 'json');
        //   }
        // if ($("#row-ads_type_id").val()) {
        // $_token = $("#row-token").val();
        //    category.html("");
        //   $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
        //     category.html(response);
        //   }, 'json');
        // }
        if ($("#row-country_id").val()) {
         $_token = $("#row-token").val();
        fetchcity.html("");
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            fetchcity.html(response);
          }, 'json');
        }
     
       if ($("#row-ads_type_id").val()) {
        $_token = $("#row-token").val();
          cusAttr.html("");
          $.post("{{ url('admin/ads/fetch-custom-attributes-enabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttr.html(response);
          }, 'json');
        }
                      $("#button-save").removeClass("hide");
                      $("#row-ads_type_id").removeAttr('disabled','disabled');
                      $("#row-category_id").removeAttr('disabled','disabled');
                      $("#row-parent_category_id").removeAttr('disabled','disabled');
                      $("#row-sub_category_id").removeAttr('disabled','disabled');
                      $("#row-title").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-countryName").removeAttr('disabled','disabled');
                      $("#row-city").removeAttr('disabled','disabled');
                      $("#row-custom_attr1").removeAttr('disabled','disabled');
                      $("#row-custom_attr2").removeAttr('disabled','disabled');
                      $("#row-address").removeAttr('disabled','disabled');
                      $("#row-youtube").removeAttr('disabled','disabled');
                      $("#row-price").removeAttr('disabled','disabled');
                      $("#row-country_id").removeAttr('disabled','disabled');
                      $("#row-city_id").removeAttr('disabled','disabled');
                      $("#row-bid_limit_amount").removeAttr('disabled','disabled');
                      $("#row-bid_start_amount").removeAttr('disabled','disabled');
                      $("#row-minimum_allowed_bid").removeAttr('disabled','disabled');
                      $("#row-bid_duration_start").removeAttr('disabled','disabled');
                      $("#row-value").removeAttr('disabled','disabled');


              // show form
              $("#modal-form_ad_management").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          
            if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }    
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");      
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      }); 
    });





$(".content-wrapper").on("click", ".btn-view", function() {
  $_token = $("#row-token").val();
      $("#form-ad_management_notice").addClass("hide");  
      $("#modal-form_ad_management").removeClass("hide");  
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');

      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                    if(field.length > 0) {
                      field.val(value);
                    }
                     if(index== "parent_category"){
                       $("#row-parent_category_id").html(response.parent_category);
                     }
                     if(index=="category_id"){ 
                        if(response.category == null){
                           $(".category-container").addClass("hide");
                        }else{
                           $(".category-container").removeClass("hide");
                           $("#row-category_id").html(response.category); 
                        } 
                     }
                     if(index=="sub_category_id"){
                          console.log(response.sub_category);
                        if(response.sub_category == null){
                           $(".subcategory-container").addClass("hide");
                        }else{
                           $(".subcategory-container").removeClass("hide");
                           $("#row-sub_category_id").html(response.sub_category); 
                        } 
                     }
                  });

            var cat_id = $("#row-cat_id").val();
            var country_id = $("#row-country_id").val();
            var subcategory = $("#row-sub_category_id");
            var category = $("#row-category_id");
            var cusAttr = $("#custom-attributes-pane");
            var fetchcity = $("#row-city_id");

            if ($("#row-ads_type_id").val()) {
            $_token = $("#row-token").val();
              cusAttr.html("");
              $.post("{{ url('admin/ads/fetch-custom-attributes-disabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
                cusAttr.html(response);
              }, 'json');

            }
            // if ($("#row-ads_type_id").val()) {
            // $_token = $("#row-token").val();
            //   subcategory.html("");
            //   $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            //     subcategory.html(response);
            //   }, 'json');
            //   }
            // if ($("#row-ads_type_id").val()) {
            // $_token = $("#row-token").val();
            //    category.html("");
            //   $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
            //     category.html(response);
            //   }, 'json');
            // }
            if ($("#row-country_id").val()) {
             $_token = $("#row-token").val();
            fetchcity.html("");
              $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
                fetchcity.html(response);
              }, 'json');
            }
            var adPictureslist = $("#list-ad-pictures");      
              adPictureslist.html("");
                $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
                  adPictureslist.html(response);
                }, 'json');

                      // $("#row-ads_type_id").attr('disabled','disabled');
                      // $("#row-category_id").attr('disabled','disabled');
                      // $("#row-parent_category_id").attr('disabled','disabled');
                      // $("#row-sub_category_id").attr('disabled','disabled');
                      // $("#row-title").attr('disabled','disabled');
                      // $("#row-description").attr('disabled','disabled');
                      // $("#row-countryName").attr('disabled','disabled');
                      // $("#row-city").attr('disabled','disabled');
                      // $("#row-address").attr('disabled','disabled');
                      // $("#row-youtube").attr('disabled','disabled');
        
                      // $("#row-price").attr('disabled','disabled');
                      // $("#row-country_id").attr('disabled','disabled');
                      // $("#row-city_id").attr('disabled','disabled');
                      // $("#row-bid_limit_amount").attr('disabled','disabled');
                      // $("#row-bid_start_amount").attr('disabled','disabled');
                      // $("#row-minimum_allowed_bid").attr('disabled','disabled');
                      // $("#row-bid_duration_start").attr('disabled','disabled');
                      // $("#row-value").attr('disabled','disabled');
                      // $("#row-bid_duration_dropdown_id").attr('disabled','disabled');
                      $("#modal-form_ad_management").find(".readonly-view").attr("disabled","disabled");
                      $("#modal-form_ad_management").find(".readonly-view").css("cursor","default");
                      $("#button-save").addClass("hide");
                      var remove_btn = setInterval(function()
                      {
                        if($('.file_remove').hasClass('hide') && $('.file_ups').hasClass('hide'))
                        {
                          clearInterval(remove_btn);
                        }
                        else
                        {
                          $('.file_remove').addClass('hide');
                          $('.file_ups').addClass('hide');
                        }
                      }, 500);
                  

              // show form
              $("#modal-form_ad_management").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }

             if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }  
                
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });





 $("#client-country").on("change", function() { 
    $("#client-city").removeAttr('disabled','disabled');
  });  


   $(".content-wrapper").on("click", ".btn-delete-ads", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Delete Feature Request', 'Are you sure you want to delete ' + title + ' feature request</strong>?', "{{ url('admin/ads/delete-feature') }}",id);
  });


  $(".content-wrapper").on("click", ".btn-mark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Mark as Spam', 'Are you sure you want to mark as spam ' + title + '</strong>?', "{{ url('admin/ads/mark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-unmark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnMark as Spam', 'Are you sure you want to unmark as spam ' + title + '</strong>?', "{{ url('admin/ads/unmark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-decline", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Decline Ad', 'Are you sure you want to reject  ' + title + '</strong>?', "{{ url('admin/ads/reject-feature') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-approved", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approve Ad', 'Are you sure you want to approve ' + title + '</strong>?', "{{ url('admin/ads/approve-feature') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-unapproved", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnApprove Ad', 'Are you sure you want to unapprove ' + title + '</strong>?', "{{ url('admin/ads/unapprove-feature') }}",id);
  });


 $("#row-ads_type_id").on("change", function() { 
        $_token = $("#row-token").val();
        var ads_id = $("#row-id").val();
        var id = $("#row-ads_type_id").val();
        $("#auction-type-pane").removeClass("hide");
        var getcategory = $("#row-category_id"); 
                if (id == 2) {
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#auction-type-pane").addClass("hide");
        }
        // var auction = $("#auction-type-pane");
        // auction.html(" ");
        // if ($("#row-ads_type_id").val()) {
        //   $.post("{{ url('admin/auction-type') }}", { id: id, _token: $_token }, function(response) {
        //     auction.html(response);
        //   }, 'json');
          getcategory.html("");
          $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: id, ads_id: ads_id, _token: $_token }, function(response) {
            getcategory.html(response);
          }, 'json');
             
  });  

       $("#row-country_id").on("change", function() { 
        $("#row-city_id").addClass("hide");
           $("#row-city_id2").removeClass("hide");
       });  

    $("#row-country_id").on("change", function() { 
  $_token = $("#row-token").val();

        $("#row-city_id").removeClass("hide");
        var country_id = $("#row-country_id").val();  
        var city = $("#row-city_id");
        city.html("");
        if ($("#row-country_id").val()) {
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            city.html(response);
          }, 'json');

        }
        $("#row-city_id2").remove();
       });  

     $("#row-value").on("click", function() { 
     $("#row-value").addClass("hide");
     $("#row-bid_duration_dropdown_id").removeClass("hide");
    });            




    $(".content-wrapper").on("click", ".btn-settings", function() {
  $_token = $("#row-token").val();

      var settings_id = $(this).data('settings_id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-settings_id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-cog");
      $.post("{{ url('admin/ads/management-settings/edit') }}", { settings_id: settings_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-cog"></i><strong> Ad Settings</strong>');
              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                      $("#row-basic_ad_expiration_time").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
              // show form
              $("#modal-form-ad-settings").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-cog");
      }, 'json');
    });

 // $('[data-countdown]').each(function() {
 //   var $this = $(this), finalDate = $(this).data('countdown');
 //   $this.countdown(finalDate, function(event) {
 //   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
 //   });

 // });

  @foreach($rows as $row)
         $('#{{$row->id}}').countdown('{{$row->ad_expiration}}', function(event) {
   var $this = $(this).html(event.strftime(''
     + ' W: <span>%w</span> -'
     + ' D: <span>%d</span> -'
     + ' H: <span>%H</span> -'
     + ' M: <span>%M</span> -'
     + ' S: <span>%S</span>'));
 });
 @endforeach


     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");
        var ads_id = $("#ads_id").val();
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      });


         $("#row-category_id").on("change", function() { 
  $_token = $("#row-token").val();
          
        var cat_id = $("#row-category_id").val();
        var ads_id = $("#row-id").val();
        $("#custom-attribute-container").removeClass("hide");
        $("#ads-subcategory").removeClass("hide");
        $("#ads-sub_category_id").addClass("hide");
        $("#custom-attr-container").remove();
        $_token = "{{ csrf_token() }}";
        var cusAttrs = $("#custom-attributes-pane");
        cusAttrs.html(" ");
        var subcategory = $("#row-sub_category_id");
        if ($("#row-category_id").val()) {
          $.post("{{ url('admin/ads/get-custom-attributes') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttrs.html(response);
        }, 'json');
          subcategory.html("");
          $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            subcategory.html(response);
        }, 'json');

       }    
  });
   $(".content-wrapper").on("click", ".btn-featured-settings", function() {
       $_token = $("#row-token").val();
       $("#modal-form-featured-settings").find('form').trigger("reset");
       $("#modal-form-featured-settings").modal('show');
    });       
    $("#row-price").change(function() {                  
        $('#row-bid_limit_amount').val(this.value);                  
    });
     $("#row-featured_duration").change(function() {   
        var id = $(this).val();
        $("#featured_panel").removeClass("hide");               
        $.post("{{ url('admin/ads/featured-ad/settings') }}", { id: id, _token: $_token }, function(response) { 
            $("#row-featured_points").val(response.points);
            $("#row-featured_price").val(response.price);
            $("#modal-form-featured-settings").find("#row-id").val(response.id);
        }, 'json');                
    });
   $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });
   $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
 });
</script>
@stop
@section('content')
  <!-- include jquery -->
 <div class="content-wrapper">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <h3 class="noMargin"><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-featured-settings"><i class="fa fa-wrench"></i> Featured Settings</button>
     <!--    <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>    
       --></div>
          <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="input-group borderZero">
                <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                <input type="text" class="form-control borderZero" id="row-search">
                </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                          <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                    <option value="">All</option>
                                    <option value="1">Approved</option>
                                    <option value="2">Pending</option>
                                    <option value="3">Decline</option>
                                    <!-- <option value="4">UnApproved</option> -->
                          </select>
                      </div>
                 </div>
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                     <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                               <option value="">All</option>
                            @foreach($countries as $row)
                               <option value="{{strToLower($row->countryCode)}}">{{$row->countryName}}</option>
                            @endforeach
                      </select>
                      </div>
                 </div>
              
            </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="userManagement">
          <thead>
            <tr>
              <!-- <th>user</th> -->
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_feature_transaction_history.transaction_no" id="row-sort"><i class="fa"></i> Transaction No</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_feature_transaction_history.payment_method" id="row-sort"><center><i class="fa"></i> Transaction Type</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.feature" id="row-sort"><i class="fa"></i> Status</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.ads_type_id" id="row-sort"><i class="fa"></i> Type</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.user_id" id="row-sort"><i class="fa"></i> User</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.title" id="row-sort"><i class="fa"></i> Title</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.country_code" id="row-sort"><center><i class="fa"></i> Country Code</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.parent_category" id="row-sort"><i class="fa"></i> Category</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.parent_category" id="row-sort"><i class="fa"></i> Duration</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.parent_category" id="row-sort"><i class="fa"></i> Cost</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.created_at" id="row-sort"><i class="fa fa-caret-up"></i> Create Date</div></th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows" class="table-hover">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}" data-title="{{ $row->title }}" data-ads_type_id="{{ $row->ads_type_id }}" data-cat_id="{{ $row->category_id }}" class="{{($row->feature == 4 ? 'unapprovedColor' : '') }}{{($row->feature == 3 ? 'blockedColor' : '') }}{{($row->feature == 1 ? 'featuredColor' : '') }}">
              <td><small>{{$row->transaction_no}}</small></td>
              <td><small><center>{{$row->payment_method  == 1 ? 'Rewards':'Paid'}}</center></small></td>
              <td><small>
                  @if ($row->feature == '1')
                      Approved
                  @elseif ($row->feature == '2')
                      Pending
                  @elseif ($row->feature == '3')
                      Decline
                  @elseif ($row->feature == '4')
                      UnApproved  
                  @endif
                  </small>
                </td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->ads_type_id == 1 ? 'Ad Listings':'Auction'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->user_name}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->title}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>{{strtoUpper($row->country_code)}}</center></small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->cat_name}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->duration.' '.$row->metric_name}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->cost}} {{$row->payment_method == 1 ? 'Points':'USD'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->feature_created_at}}</small></td>
                <td>
                <!--  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" {{$row->feature == 3 ? 'disabled':''}}><i class="fa fa-close"></i></button> -->

                <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>

                <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-decline" {{($row->feature != 2  ? 'disabled':'')}} data-toggle="tooltip" title="Decline"><i class="fa fa-ban"></i></button>

                @if($row->feature !='1')
                   <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approved" data-toggle="tooltip" title="Approve" {{( !in_array($row->feature, [2, 3]) ? 'disabled' : '')}}><i class="fa fa-check"></i></button>
                @else
                   <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unapproved" data-toggle="tooltip" title="Unapprove" {{($row->feature != 1 ? 'disabled' : '')}}><i class="fa fa-close"></i></button>
                @endif 
                
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                </td> 
              </tr>
            @endforeach
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}}" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
@stop
@include('admin.ads-management.featured-ads.form')