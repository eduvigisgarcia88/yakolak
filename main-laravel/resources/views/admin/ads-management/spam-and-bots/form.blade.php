<!-- modal add user -->
  <div class="modal fade" id="modal-spam-and-bot" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/ads/spam-and-bots/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-spam-and-bot', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD SPAM AND BOT KEYWORD</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Keyword</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="restricted_word" class="form-control hide-view" id="row-restricted_word" maxlength="30">
                </div>               
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success btn-hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
    <div class="modal fade" id="modal-recaptcha-settings" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/settings/recaptcha/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-recaptcha', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-wrench"></i> Recaptcha Settings </h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
               @foreach($site_keys as $row)
                 <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Site Key</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="site_key" class="form-control hide-view" id="row-site_key" value="{{$row->site_key}}">
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Secret Key</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="secret_key" class="form-control hide-view" id="row-secret_key" value="{{$row->secret_key}}">
                </div>               
                </div>
               @endforeach
               
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success btn-hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  