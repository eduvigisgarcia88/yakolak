
@extends('layout.master')
@section('scripts')
<script type="text/javascript">
  $_token = '{{ csrf_token() }}';


function refresh() {
      // $_token = $("#row-token").val();
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
      
 
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
  function search() {
        // $_token = $("#row-token").val();
        $('.loading-pane').removeClass('hide');
        $page = $("#row-page").val();
        $order = $("#row-order").val();
        $sort = $("#row-sort").val();
        $search = $("#row-search").val();
        $status = $("#row-filter_status").val();
        $per = $("#row-per").val();

        var loading = $(".loading-pane");
        var table = $("#rows");
        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
          // clear
          table.html("");
          var body = "";
            $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
        
   
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");

        }, 'json');
    }




   function maxAds() {
      $("#row-page").val(1);
      // $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
        table.html(body);

      // update links
         $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
   function search() {
      $("#row-page").val(1);
      // $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
          $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
        table.html(body);

      // update links
         $("#modal-form").find('form').trigger("reset");
        table.html(body);
        // $.each(response.rows.data, function(index, row) {
        //     $("#my-checkbox-"+row.id).bootstrapSwitch();
        // });
        // $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
   $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
      }
    });
    $("#userManagement").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#userManagement").find("i").removeClass('fa-caret-up');
          $("#userManagement").find("i").removeClass('fa-caret-down');
          $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");

        
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2  ? 'disabledColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.restricted_word + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +

              '<td>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               (row.status == 1 ?  '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>': '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          // if(response.order == "asc"){
          //     $("#row-order").val("desc");
          //     $(this).find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          // }else{
          //     $('#row-order').val("asc");
          //     $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          // }

            if($order == "asc"){
            if($sort != "created_at"){
              $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');
            }else{
              $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
         }else{
            if($sort != "created_at"){
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
             }else{
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down'); 
            }
          }
       
      }, 'json');
  

    });

    $(".content-wrapper").on("click", ".btn-add", function() {
           $_token = $("#row-token").val();
           $("#modal-spam-and-bot").modal("show");
           $("#modal-spam-and-bot").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>ADD SPAM AND BOT KEYWORD</strong>');
           $("#modal-form-add").modal('show');
        
    });
    $(".content-wrapper").on("click", ".btn-recaptcha-settings", function() {
           $("#modal-recaptcha-settings").modal("show");
    });

 $(".content-wrapper").on("click", ".btn-edit", function() {
      $_token = $("#row-token").val();
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var btn = $(this);
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/spam-and-bots/edit') }}", { id: id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.restricted_word + '</strong>');
              // output form data
            
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  console.log(index);

                  if(field.length > 0) {
                    field.val(value);
                  }
        
              });

          // show form
             $("#modal-spam-and-bot").modal('show');
             $("#modal-spam-and-bot").find("form").find(".hide-view").removeAttr("disabled","disabled");
             $("#modal-spam-and-bot").find("form").find(".btn-hide-view").removeClass("hide");
          } else {
              status(false, response.error, 'alert-danger');
          }
          
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

    });





$(".content-wrapper").on("click", ".btn-view", function() {
      $_token = $("#row-token").val();
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var btn = $(this);

      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/spam-and-bots/edit') }}", { id: id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-eye"></i> View <strong>' + response.restricted_word + '</strong>');
              // output form data
            
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  console.log(index);

                  if(field.length > 0) {
                    field.val(value);
                  }
        
              });

          // show form
             $("#modal-spam-and-bot").find("form").find(".hide-view").attr("disabled","disabled");
             $("#modal-spam-and-bot").find("form").find(".btn-hide-view").addClass("hide");
             $("#modal-spam-and-bot").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });

   $(".content-wrapper").on("click", ".btn-delete-ads", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Delete Ad', 'Are you sure you want to delete ' + title + '</strong>?', "{{ url('admin/ads/delete') }}",id);
  });


  $(".content-wrapper").on("click", ".btn-mark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Mark as Spam', 'Are you sure you want to mark as spam ' + title + '</strong>?', "{{ url('admin/ads/mark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-unmark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnMark as Spam', 'Are you sure you want to unmark as spam ' + title + '</strong>?', "{{ url('admin/ads/unmark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Keyword', 'Are you sure you want to delete  ' + title + '</strong>?', "{{ url('admin/ads/spam-and-bots/delete') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable', 'Are you sure you want to enable this?', "{{ url('admin/ads/spam-and-bots/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable', 'Are you sure you want to disable this?', "{{ url('admin/ads/spam-and-bots/disable') }}",id);
  });
   $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });
    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
   });
</script>
@stop
@section('content')
  <!-- include jquery -->
 <div class="content-wrapper">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3 class="noMargin"><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button> 
        <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-recaptcha-settings"><i class="fa fa-wrench"></i>  Recaptcha Settings</button>   
      </div>
       <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                      <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Enabled</option>
                                <option value="2">Disabled</option>
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-condensed backend-generic-table" id="userManagement">
          <thead>
            <tr>
              <!-- <th>user</th> -->
               <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="restricted_word" id="row-sort"><i class="fa"></i> Restricted Word</div></th>
               <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created At</div></th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows" class="table-hover">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}" class="{{($row->status == 2 ? 'disabledColor' : '') }}">
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->restricted_word}}</small></td>             
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->created_at}}</small></td>
                <td>
                <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>
                @if($row->status == 1)
                     <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable" data-toggle="tooltip" title="Disable"><i class="fa fa-lock"></i></button>
                  @else
                      <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" data-toggle="tooltip" title="Unlock"><i class="fa fa-unlock"></i></button>
                @endif
                
                  <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads {{$row->status == 3 ? 'hide':''}}"><i class="fa fa-trash-o"></i></button> -->
                  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                </td> 
              </tr>
            @endforeach
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
          </select>
        </div>
      </div>
@stop
@include('admin.ads-management.spam-and-bots.form')