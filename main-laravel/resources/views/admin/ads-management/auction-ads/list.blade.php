@extends('layout.master')
@section('scripts')
<script type="text/javascript">

function refresh() {
  $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();
      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, country:$country , per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
        $.each(response.rows.data, function(index, row) {
          console.log(response);
         body += '<tr data-status_no="' + row.status + '" data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 2  ? 'blockedColor' : '')+(row.status == 4  ? 'disabledColor' : '')+(row.status == 3  ? 'disabledColor' : '')+(row.status == 6  ? 'flaggedColor' : '')+'">' + 
              '<td><small>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Blocked':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Disabled':'')+(row.status == '6'?'Flagged':'')+'</small></td>' +
              '<td><small>'+(row.feature == 1 ? 'Feature':'Regular')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.catName + '</small></td>' +
              '<td><small><span data-countdown="'+row.ad_expiration+'"></span></small></td>' +
              '<td><small>' + (row.ad_bidder == null ? '0': Math.floor(row.ad_bidder)) + '/'+(row.bid_limit_amount == null ? '0':row.bid_limit_amount)+' USD</small></td>' +
              '<td><small>' + row.created_at  + '</small></td>' +
              '<td>'+
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '4'  ? 'btn-unmark-spam' : 'btn-mark-spam')+'"><i class="fa '+ (row.status == '4'  ? 'fa-envelope' : 'fa-envelope-o')+'"></i></button>' +
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads '+(row.status == 7 ? 'hide':'')+'"><i class="fa fa-trash-o"></i></button>'+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '2'  ? 'btn-unblocked' : 'btn-blocked')+'"><i class="fa '+ (row.status == '2'  ? 'fa-shirtsinbulk ' : 'fa-shield ')+'"></i></button>' +
              
              (row.status == 3 || row.status == 4? '<button type="button" class="btn btn-default btn-xs pull-right btn-enable" '+(row.user_status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-disable" '+(row.status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>') +

              (row.status == 6 ? '<button type="button" class="btn btn-default btn-xs pull-right btn-unflag" '+(row.user_status == 2 ? 'disabled':'')+'><i class="fa fa-flag-o"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-flag" '+(row.status == 2 ? 'disabled':'')+'><i class="fa fa-flag"></i></button>') +
              
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable '+(row.status == 3 || row.status == 4 ? '':'')+'"><i class="fa fa-unlock"></i></button>'+

              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>'+   
               // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '6'  ? 'btn-unflag' : 'btn-flag')+'"><i class="fa '+ (row.status == '6'  ? 'fa-flag' : 'fa-flag-o')+'"></i></button>'+

                
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });

        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $('#row-pages').html(response.pages);
        $("#row-page").val(response.rows.current_page);
        $("#row-order").val(response.order);
        loading.addClass("hide");
       $('[data-countdown]').each(function() {
         var $this = $(this), finalDate = $(this).data('countdown');
         $this.countdown(finalDate, function(event) {
         $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
         });
      });
      }, 'json');
    
  }

    function search() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, country:$country , per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
          console.log(response);
         body += '<tr data-status_no="' + row.status + '" data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 2  ? 'blockedColor' : '')+(row.status == 4  ? 'disabledColor' : '')+(row.status == 3  ? 'disabledColor' : '')+(row.status == 6  ? 'flaggedColor' : '')+'">' + 
              '<td><small>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Blocked':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Disabled':'')+(row.status == '6'?'Flagged':'')+'</small></td>' +
              '<td><small>'+(row.feature == 1 ? 'Feature':'Regular')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.catName + '</small></td>' +
              '<td><small><span data-countdown="'+row.ad_expiration+'"></span></small></td>' +
              '<td><small>' + (row.ad_bidder == null ? '0': Math.floor(row.ad_bidder)) + '/'+(row.bid_limit_amount == null ? '0':row.bid_limit_amount)+' USD</small></td>' +
              '<td><small>' + row.created_at  + '</small></td>' +
              '<td>'+
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '4'  ? 'btn-unmark-spam' : 'btn-mark-spam')+'"><i class="fa '+ (row.status == '4'  ? 'fa-envelope' : 'fa-envelope-o')+'"></i></button>' +
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads '+(row.status == 7 ? 'hide':'')+'"><i class="fa fa-trash-o"></i></button>'+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '2'  ? 'btn-unblocked' : 'btn-blocked')+'"><i class="fa '+ (row.status == '2'  ? 'fa-shirtsinbulk ' : 'fa-shield ')+'"></i></button>' +
              
              (row.status == 3 || row.status == 4? '<button type="button" class="btn btn-default btn-xs pull-right btn-enable" '+(row.user_status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-disable" '+(row.status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>') +

              (row.status == 6 ? '<button type="button" class="btn btn-default btn-xs pull-right btn-unflag" '+(row.user_status == 2 ? 'disabled':'')+'><i class="fa fa-flag-o"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-flag" '+(row.status == 2 ? 'disabled':'')+'><i class="fa fa-flag"></i></button>') +
              
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable '+(row.status == 3 || row.status == 4 ? '':'')+'"><i class="fa fa-unlock"></i></button>'+

              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>'+   
               // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '6'  ? 'btn-unflag' : 'btn-flag')+'"><i class="fa '+ (row.status == '6'  ? 'fa-flag' : 'fa-flag-o')+'"></i></button>'+

                
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      $("#row-page").val(response.rows.current_page);
      $("#row-order").val(response.order);
      loading.addClass("hide");
      $('[data-countdown]').each(function() {
       var $this = $(this), finalDate = $(this).data('countdown');
       $this.countdown(finalDate, function(event) {
       $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
       });
      });
      }, 'json');
      
    }
     function maxAds() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

     $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, country:$country , per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
          console.log(response);
         body += '<tr data-status_no="' + row.status + '" data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 2  ? 'blockedColor' : '')+(row.status == 4  ? 'disabledColor' : '')+(row.status == 3  ? 'disabledColor' : '')+(row.status == 6  ? 'flaggedColor' : '')+'">' + 
              '<td><small>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Blocked':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Disabled':'')+(row.status == '6'?'Flagged':'')+'</small></td>' +
              '<td><small>'+(row.feature == 1 ? 'Feature':'Regular')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.catName + '</small></td>' +
              '<td><small><span data-countdown="'+row.ad_expiration+'"></span></small></td>' +
              '<td><small>' + (row.ad_bidder == null ? '0': Math.floor(row.ad_bidder)) + '/'+(row.bid_limit_amount == null ? '0':row.bid_limit_amount)+' USD</small></td>' +
              '<td><small>' + row.created_at  + '</small></td>' +
              '<td>'+
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '4'  ? 'btn-unmark-spam' : 'btn-mark-spam')+'"><i class="fa '+ (row.status == '4'  ? 'fa-envelope' : 'fa-envelope-o')+'"></i></button>' +
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads '+(row.status == 7 ? 'hide':'')+'"><i class="fa fa-trash-o"></i></button>'+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '2'  ? 'btn-unblocked' : 'btn-blocked')+'"><i class="fa '+ (row.status == '2'  ? 'fa-shirtsinbulk ' : 'fa-shield ')+'"></i></button>' +
              
              (row.status == 3 || row.status == 4? '<button type="button" class="btn btn-default btn-xs pull-right btn-enable" '+(row.user_status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-disable" '+(row.status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>') +

              (row.status == 6 ? '<button type="button" class="btn btn-default btn-xs pull-right btn-unflag" '+(row.user_status == 2 ? 'disabled':'')+'><i class="fa fa-flag-o"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-flag" '+(row.status == 2 ? 'disabled':'')+'><i class="fa fa-flag"></i></button>') +
              
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable '+(row.status == 3 || row.status == 4 ? '':'')+'"><i class="fa fa-unlock"></i></button>'+

              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>'+   
               // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '6'  ? 'btn-unflag' : 'btn-flag')+'"><i class="fa '+ (row.status == '6'  ? 'fa-flag' : 'fa-flag-o')+'"></i></button>'+

                
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      $("#row-page").val(response.rows.current_page);
      $("#row-order").val(response.order);
      loading.addClass("hide");
       $('[data-countdown]').each(function() {
       var $this = $(this), finalDate = $(this).data('countdown');
       $this.countdown(finalDate, function(event) {
       $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
        });
     });
      }, 'json');
     
    }

     $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, country:$country , per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

           $.each(response.rows.data, function(index, row) {
          console.log(response);
         body += '<tr data-status_no="' + row.status + '" data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 2  ? 'blockedColor' : '')+(row.status == 4  ? 'disabledColor' : '')+(row.status == 3  ? 'disabledColor' : '')+(row.status == 6  ? 'flaggedColor' : '')+'">' + 
              '<td><small>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Blocked':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Disabled':'')+(row.status == '6'?'Flagged':'')+'</small></td>' +
              '<td><small>'+(row.feature == 1 ? 'Feature':'Regular')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.name + '</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
              '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.catName + '</small></td>' +
              '<td><small><span data-countdown="'+row.ad_expiration+'"></span></small></td>' +
              '<td><small>' + (row.ad_bidder == null ? '0': Math.floor(row.ad_bidder)) + '/'+(row.bid_limit_amount == null ? '0':row.bid_limit_amount)+' USD</small></td>' +
              '<td><small>' + row.created_at  + '</small></td>' +
              '<td>'+
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '4'  ? 'btn-unmark-spam' : 'btn-mark-spam')+'"><i class="fa '+ (row.status == '4'  ? 'fa-envelope' : 'fa-envelope-o')+'"></i></button>' +
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads '+(row.status == 7 ? 'hide':'')+'"><i class="fa fa-trash-o"></i></button>'+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '2'  ? 'btn-unblocked' : 'btn-blocked')+'"><i class="fa '+ (row.status == '2'  ? 'fa-shirtsinbulk ' : 'fa-shield ')+'"></i></button>' +
              
              (row.status == 3 || row.status == 4? '<button type="button" class="btn btn-default btn-xs pull-right btn-enable" '+(row.user_status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-disable" '+(row.status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>') +

              (row.status == 6 ? '<button type="button" class="btn btn-default btn-xs pull-right btn-unflag" '+(row.user_status == 2 ? 'disabled':'')+'><i class="fa fa-flag-o"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-flag" '+(row.status == 2 ? 'disabled':'')+'><i class="fa fa-flag"></i></button>') +
              
              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable '+(row.status == 3 || row.status == 4 ? '':'')+'"><i class="fa fa-unlock"></i></button>'+

              // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>'+   
               // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '6'  ? 'btn-unflag' : 'btn-flag')+'"><i class="fa '+ (row.status == '6'  ? 'fa-flag' : 'fa-flag-o')+'"></i></button>'+

                
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
              '</td></tr>';
        });
      
      table.html(body);
      $('#row-pages').html(response.pages);
      $("#row-page").val(response.rows.current_page);
      $("#row-order").val(response.order);
      loading.addClass("hide");
      $('[data-countdown]').each(function() {
         var $this = $(this), finalDate = $(this).data('countdown');
         $this.countdown(finalDate, function(event) {
         $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
         });
      });
      }, 'json');
      }
    });
    $("#userManagement").on("click", "#row-sort", function() {
          $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $("#userManagement").find("i").removeClass('fa-caret-up');
          $("#userManagement").find("i").removeClass('fa-caret-down');
          $(this).find("i").addClass('fa-caret-down');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();

          var loading = $(".loading-pane");
          var table = $("#rows");

          $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, country:$country , per: $per, _token: $_token }, function(response) {
            table.html("");
            console.log(response);
            var body = "";

                $.each(response.rows.data, function(index, row) {
                console.log(response);
               body += '<tr data-status_no="' + row.status + '" data-id="' + row.id + '"  data-title="' + row.title + '" data-ads_type_id="' + row.ads_type_id + '" class="'+ (row.status == 2  ? 'blockedColor' : '')+(row.status == 4  ? 'disabledColor' : '')+(row.status == 3  ? 'disabledColor' : '')+(row.status == 6  ? 'flaggedColor' : '')+'">' + 
                    '<td><small>' + (row.status == '1'?'Active':'') + (row.status == '2'?'Blocked':'')+(row.status == '3'?'Disabled':'')+(row.status == '4'?'Disabled':'')+(row.status == '6'?'Flagged':'')+'</small></td>' +
                    '<td><small>'+(row.feature == 1 ? 'Feature':'Regular')+'</small></td>' +
                    '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.name + '</small></td>' +
                    '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.title + '</small></td>' +
                    '<td><small><center>' + row.country_code.toUpperCase() + '</center></small></td>' +
                    '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.catName + '</small></td>' +
                    '<td><small><span data-countdown="'+row.ad_expiration+'"></span></small></td>' +
                    '<td><small>' + (row.ad_bidder == null ? '0': Math.floor(row.ad_bidder)) + '/'+(row.bid_limit_amount == null ? '0':row.bid_limit_amount)+' USD</small></td>' +
                    '<td><small>' + row.created_at  + '</small></td>' +
                    '<td>'+
                    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '4'  ? 'btn-unmark-spam' : 'btn-mark-spam')+'"><i class="fa '+ (row.status == '4'  ? 'fa-envelope' : 'fa-envelope-o')+'"></i></button>' +
                    '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads '+(row.status == 7 ? 'hide':'')+'"><i class="fa fa-trash-o"></i></button>'+
                    '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '2'  ? 'btn-unblocked' : 'btn-blocked')+'"><i class="fa '+ (row.status == '2'  ? 'fa-shirtsinbulk ' : 'fa-shield ')+'"></i></button>' +
                    
                    (row.status == 3 || row.status == 4? '<button type="button" class="btn btn-default btn-xs pull-right btn-enable" '+(row.user_status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-disable" '+(row.status == 2 ? 'disabled':'')+' data-toggle="modal" data-target="#"><i class="fa fa-lock"></i></button>') +

                    (row.status == 6 ? '<button type="button" class="btn btn-default btn-xs pull-right btn-unflag" '+(row.user_status == 2 ? 'disabled':'')+'><i class="fa fa-flag-o"></i></button>':'<button type="button" class="btn btn-default btn-xs pull-right btn-flag" '+(row.status == 2 ? 'disabled':'')+'><i class="fa fa-flag"></i></button>') +
                    
                    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable '+(row.status == 3 || row.status == 4 ? '':'')+'"><i class="fa fa-unlock"></i></button>'+

                    // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>'+   
                     // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right '+ (row.status == '6'  ? 'btn-unflag' : 'btn-flag')+'"><i class="fa '+ (row.status == '6'  ? 'fa-flag' : 'fa-flag-o')+'"></i></button>'+

                      
                      '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                      '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +     
                    '</td></tr>';
              });
      
          
          table.html(body);
          $(".th-sort").find('i').removeAttr('class');
          $('#row-pages').html(response.pages);
          $("#row-page").val(response.rows.current_page);
          $("#row-order").val(response.order);
          loading.addClass("hide");
          $('[data-countdown]').each(function() {
           var $this = $(this), finalDate = $(this).data('countdown');
           $this.countdown(finalDate, function(event) {
           $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
           });

         });
         if(response.order == "asc"){
            $("#row-order").val("desc");
            $(this).html("<i class='fa fa-caret-down'></i>");
        }else{
            $('#row-order').val("asc");
            $(this).html("<i class='fa fa-caret-up'></i>");
        } 

          }, 'json');
          if($order == "asc"){
            if($sort != "created_at"){
              $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');
            }else{
              $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
         }else{
            if($sort != "created_at"){
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
             }else{
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down'); 
            }
           
         }
        
    });


    $(".content-wrapper").on("click", ".btn-add", function() {
           $_token = $("#row-token").val();
           $("#modal-form-add").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add Advertisement</strong>');
           $("#modal-form-add").modal('show');
        
    });


 $(".content-wrapper").on("click", ".btn-edit", function() {
      $("#modal-form_ad_management").removeClass("hide");
      $("#form-ad_management_notice").addClass("hide");
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');
      var status = $(this).parent().parent().data('status_no');
      console.log(status+' status');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {
        $('#ad_status_no').val(status);
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
            
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                 if(index== "parent_category"){
                   $("#row-parent_category_id").html(response.parent_category);
                 }
                 if(index=="category_id"){ 
                    if(response.category == null){
                       $(".category-container").addClass("hide");
                    }else{
                       $(".category-container").removeClass("hide");
                       $("#row-category_id").html(response.category); 
                    } 
                 }
                 if(index=="sub_category_id"){
                      console.log(response.sub_category);
                    if(response.sub_category == null){
                       $(".subcategory-container").addClass("hide");
                    }else{
                       $(".subcategory-container").removeClass("hide");
                       $("#row-sub_category_id").html(response.sub_category); 
                    } 
                 }

              });
   
        var cat_id = $("#row-cat_id").val();
        var country_id = $("#row-country_id").val();
        // var subcategory = $("#row-sub_category_id");
        // var category = $("#row-category_id");
        var cusAttr = $("#custom-attributes-pane");
        var fetchcity = $("#row-city_id");

      
        // if ($("#row-ads_type_id").val()) {
        // $_token = $("#row-token").val();
        //   subcategory.html("");
        //   $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
        //     subcategory.html(response);
        //   }, 'json');
        //   }
        // if ($("#row-ads_type_id").val()) {
        // $_token = $("#row-token").val();
        //    category.html("");
        //   $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
        //     category.html(response);
        //   }, 'json');
        // }
        if ($("#row-country_id").val()) {
         $_token = $("#row-token").val();
        fetchcity.html("");
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            fetchcity.html(response);
          }, 'json');
        }
     
       if ($("#row-ads_type_id").val()) {
        $_token = $("#row-token").val();
          cusAttr.html("");
          $.post("{{ url('admin/ads/fetch-custom-attributes-enabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttr.html(response);
          }, 'json');
        }
                      $("#button-save").removeClass("hide");
                      $("#row-ads_type_id").removeAttr('disabled','disabled');
                      $("#row-category_id").removeAttr('disabled','disabled');
                      $("#row-parent_category_id").removeAttr('disabled','disabled');
                      $("#row-sub_category_id").removeAttr('disabled','disabled');
                      $("#row-title").removeAttr('disabled','disabled');
                      $("#row-description").removeAttr('disabled','disabled');
                      $("#row-countryName").removeAttr('disabled','disabled');
                      $("#row-city").removeAttr('disabled','disabled');
                      $("#row-custom_attr1").removeAttr('disabled','disabled');
                      $("#row-custom_attr2").removeAttr('disabled','disabled');
                      $("#row-address").removeAttr('disabled','disabled');
                      $("#row-youtube").removeAttr('disabled','disabled');
                      $("#row-price").removeAttr('disabled','disabled');
                      $("#row-country_id").removeAttr('disabled','disabled');
                      $("#row-city_id").removeAttr('disabled','disabled');
                      $("#row-bid_limit_amount").removeAttr('disabled','disabled');
                      $("#row-bid_start_amount").removeAttr('disabled','disabled');
                      $("#row-minimum_allowed_bid").removeAttr('disabled','disabled');
                      $("#row-bid_duration_start").removeAttr('disabled','disabled');
                      $("#row-value").removeAttr('disabled','disabled');


              // show form
              $("#modal-form_ad_management").modal('show');
              $('#modal-form_ad_management').find('.read_remove').show();
          } else {
              status(false, response.error, 'alert-danger');
          }
          
            if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }    
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");      
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      }); 
    });





$(".content-wrapper").on("click", ".btn-view", function() {
  $_token = $("#row-token").val();
      $("#form-ad_management_notice").addClass("hide");  
      $("#modal-form_ad_management").removeClass("hide");  
      var ads_id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var ads_type_id = $(this).parent().parent().data('ads_type_id');

      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-ads_id").val("ads_id");
      $("#row-ads_type_id").val("ads_type_id");

      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/edit') }}", { ads_id: ads_id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + title + '</strong>');
              // output form data
           $.each(response.row, function(index, value) {
                  var field = $("#row-" + index);

                    if(field.length > 0) {
                      field.val(value);
                    }
                     if(index== "parent_category"){
                       $("#row-parent_category_id").html(response.parent_category);
                     }
                     if(index=="category_id"){ 
                        if(response.category == null){
                           $(".category-container").addClass("hide");
                        }else{
                           $(".category-container").removeClass("hide");
                           $("#row-category_id").html(response.category); 
                        } 
                     }
                     if(index=="sub_category_id"){
                          console.log(response.sub_category);
                        if(response.sub_category == null){
                           $(".subcategory-container").addClass("hide");
                        }else{
                           $(".subcategory-container").removeClass("hide");
                           $("#row-sub_category_id").html(response.sub_category); 
                        } 
                     }
                  });

            var cat_id = $("#row-cat_id").val();
            var country_id = $("#row-country_id").val();
            var subcategory = $("#row-sub_category_id");
            var category = $("#row-category_id");
            var cusAttr = $("#custom-attributes-pane");
            var fetchcity = $("#row-city_id");

            if ($("#row-ads_type_id").val()) {
            $_token = $("#row-token").val();
              cusAttr.html("");
              $.post("{{ url('admin/ads/fetch-custom-attributes-disabled') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
                cusAttr.html(response);
              }, 'json');

            }
            // if ($("#row-ads_type_id").val()) {
            // $_token = $("#row-token").val();
            //   subcategory.html("");
            //   $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            //     subcategory.html(response);
            //   }, 'json');
            //   }
            // if ($("#row-ads_type_id").val()) {
            // $_token = $("#row-token").val();
            //    category.html("");
            //   $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: ads_type_id, ads_id: ads_id, _token: $_token }, function(response) {
            //     category.html(response);
            //   }, 'json');
            // }
            if ($("#row-country_id").val()) {
             $_token = $("#row-token").val();
            fetchcity.html("");
              $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
                fetchcity.html(response);
              }, 'json');
            }
            var adPictureslist = $("#list-ad-pictures");      
              adPictureslist.html("");
                $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
                  adPictureslist.html(response);
                }, 'json');

                      // $("#row-ads_type_id").attr('disabled','disabled');
                      // $("#row-category_id").attr('disabled','disabled');
                      // $("#row-parent_category_id").attr('disabled','disabled');
                      // $("#row-sub_category_id").attr('disabled','disabled');
                      // $("#row-title").attr('disabled','disabled');
                      // $("#row-description").attr('disabled','disabled');
                      // $("#row-countryName").attr('disabled','disabled');
                      // $("#row-city").attr('disabled','disabled');
                      // $("#row-address").attr('disabled','disabled');
                      // $("#row-youtube").attr('disabled','disabled');
        
                      // $("#row-price").attr('disabled','disabled');
                      // $("#row-country_id").attr('disabled','disabled');
                      // $("#row-city_id").attr('disabled','disabled');
                      // $("#row-bid_limit_amount").attr('disabled','disabled');
                      // $("#row-bid_start_amount").attr('disabled','disabled');
                      // $("#row-minimum_allowed_bid").attr('disabled','disabled');
                      // $("#row-bid_duration_start").attr('disabled','disabled');
                      // $("#row-value").attr('disabled','disabled');
                      // $("#row-bid_duration_dropdown_id").attr('disabled','disabled');
                      $("#modal-form_ad_management").find(".readonly-view").attr("disabled","disabled");
                      $("#modal-form_ad_management").find(".readonly-view").css("cursor","default");
                      $("#button-save").addClass("hide");
                      var remove_btn = setInterval(function()
                      {
                        if($('.file_remove').hasClass('hide') && $('.file_ups').hasClass('hide'))
                        {
                          clearInterval(remove_btn);
                        }
                        else
                        {
                          $('.file_remove').addClass('hide');
                          $('.file_ups').addClass('hide');
                        }
                      }, 500);
                  

              // show form
              $("#modal-form_ad_management").modal('show');
              $('#modal-form_ad_management').find('.read_remove').hide();
          } else {
              status(false, response.error, 'alert-danger');
          }

             if (ads_type_id == 2) {
                      $("#auction-type-pane").removeClass("hide");
              } else {
                      $("#auction-type-pane").addClass("hide");

              }  
                
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });





 $("#client-country").on("change", function() { 
    $("#client-city").removeAttr('disabled','disabled');
  });  


   $(".content-wrapper").on("click", ".btn-delete-ads", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Delete Ad', 'Are you sure you want to delete ' + title + '</strong>?', "{{ url('admin/ads/delete') }}",id);
  });


  $(".content-wrapper").on("click", ".btn-mark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Mark as Spam', 'Are you sure you want to mark as spam ' + title + '</strong>?', "{{ url('admin/ads/mark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-unmark-spam", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnMark as Spam', 'Are you sure you want to unmark as spam ' + title + '</strong>?', "{{ url('admin/ads/unmark-spam') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-unblocked", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnBlock Ad', 'Are you sure you want to unblock ' + title + '</strong>?', "{{ url('admin/ads/unblock') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-blocked", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Block Ad', 'Are you sure you want to block ' + title + '</strong>?', "{{ url('admin/ads/block') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Ad', 'Are you sure you want to disable ' + title + '</strong>?', "{{ url('admin/ads/disable') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Ad', 'Are you sure you want to enable ' + title + '</strong>?', "{{ url('admin/ads/enable') }}",id);
  });
    $(".content-wrapper").on("click", ".btn-flag", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Flag Ad', 'Are you sure you want to flag ' + title + '</strong>?', "{{ url('admin/ads/flag') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-unflag", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnFlag Ad', 'Are you sure you want to unflag ' + title + '</strong>?', "{{ url('admin/ads/unflag') }}",id);
  });

 $("#row-ads_type_id").on("change", function() { 
        $_token = $("#row-token").val();
        var ads_id = $("#row-id").val();
        var id = $("#row-ads_type_id").val();
        $("#auction-type-pane").removeClass("hide");
        var getcategory = $("#row-category_id"); 
                if (id == 2) {
            $("#auction-type-pane").removeClass("hide");
        } else {
            $("#auction-type-pane").addClass("hide");
        }
        // var auction = $("#auction-type-pane");
        // auction.html(" ");
        // if ($("#row-ads_type_id").val()) {
        //   $.post("{{ url('admin/auction-type') }}", { id: id, _token: $_token }, function(response) {
        //     auction.html(response);
        //   }, 'json');
          getcategory.html("");
          $.post("{{ url('admin/ads/get-category') }}", { ads_type_id: id, ads_id: ads_id, _token: $_token }, function(response) {
            getcategory.html(response);
          }, 'json');
             
  });  

       $("#row-country_id").on("change", function() { 
        $("#row-city_id").addClass("hide");
           $("#row-city_id2").removeClass("hide");
       });  

    $("#row-country_id").on("change", function() { 
  $_token = $("#row-token").val();

        $("#row-city_id").removeClass("hide");
        var country_id = $("#row-country_id").val();  
        var city = $("#row-city_id");
        city.html("");
        if ($("#row-country_id").val()) {
          $.post("{{ url('admin/ads/get-city') }}", { country_id: country_id, _token: $_token }, function(response) {
            city.html(response);
          }, 'json');

        }
        $("#row-city_id2").remove();
       });  

     $("#row-value").on("click", function() { 
     $("#row-value").addClass("hide");
     $("#row-bid_duration_dropdown_id").removeClass("hide");
    });            




    $(".content-wrapper").on("click", ".btn-settings", function() {
  $_token = $("#row-token").val();

      var settings_id = $(this).data('settings_id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-settings_id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-cog");
      $.post("{{ url('admin/ads/management-settings/edit') }}", { settings_id: settings_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-cog"></i><strong> Ad Settings</strong>');
              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                      $("#row-basic_ad_expiration_time").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");
              // show form
              $("#modal-form-ad-settings").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-cog");
      }, 'json');
    });

 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('D: %D - H: %H - M: %M - S: %S'));
   });

 });

 //  @foreach($rows as $row)
 //   $('#{{$row->id}}').countdown('{{$row->ad_expiration}}', function(event) {
 //   var $this = $(this).html(event.strftime(''
 //     + ' W: <span>%w</span> -'
 //     + ' D: <span>%d</span> -'
 //     + ' H: <span>%H</span> -'
 //     + ' M: <span>%M</span> -'
 //     + ' S: <span>%S</span>'));
 // });
 // @endforeach


     $(function () {
  $_token = $("#row-token").val();

        var adPictureslist = $("#list-ad-pictures");
        var ads_id = $("#ads_id").val();
        adPictureslist.html("");
          $.post("{{ url('admin/ads/list-pictures') }}", { ads_id: ads_id, _token: $_token }, function(response) {
            adPictureslist.html(response);
          }, 'json');
      });


         $("#row-category_id").on("change", function() { 
  $_token = $("#row-token").val();
          
        var cat_id = $("#row-category_id").val();
        var ads_id = $("#row-id").val();
        $("#custom-attribute-container").removeClass("hide");
        $("#ads-subcategory").removeClass("hide");
        $("#ads-sub_category_id").addClass("hide");
        $("#custom-attr-container").remove();
        $_token = "{{ csrf_token() }}";
        var cusAttrs = $("#custom-attributes-pane");
        cusAttrs.html(" ");
        var subcategory = $("#row-sub_category_id");
        if ($("#row-category_id").val()) {
          $.post("{{ url('admin/ads/get-custom-attributes') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            cusAttrs.html(response);
        }, 'json');
          subcategory.html("");
          $.post("{{ url('admin/ads/get-sub-category') }}", { ads_id: ads_id, cat_id: cat_id, _token: $_token }, function(response) {
            subcategory.html(response);
        }, 'json');

       }    
  });

    $("#row-price").change(function() {                  
        $('#row-bid_limit_amount').val(this.value);                  
    });
   $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });

    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });
    $("#form-ad_management_save").on("click",'.btn-enable',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('UnBlock Ad', '<strong> Are you sure you want to enable this ad</strong>?', "{{ url('admin/ads/enable') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-disable',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Disable Ad', 'Are you sure you want to disable this ad</strong>?', "{{ url('admin/ads/disable') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-blocked',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Disable Ad', 'Are you sure you want to block this ad</strong>?', "{{ url('admin/ads/block') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-unblocked',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Disable Ad', 'Are you sure you want to unblock this ad</strong>?', "{{ url('admin/ads/unblock') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-flag',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Disable Ad', 'Are you sure you want to flag this ad</strong>?', "{{ url('admin/ads/flag') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-unflag',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Disable Ad', 'Are you sure you want to unflag this ad</strong>?', "{{ url('admin/ads/unflag') }}",id);
   });
    $("#form-ad_management_save").on("click",'.btn-delete-ads',function(){
        var id = $("#row-id").val();
        $("#modal-form_ad_management").modal("hide");
        dialog('Delete Ad', 'Are you sure you want to delete this ad</strong>?', "{{ url('admin/ads/delete') }}",id);
   });


    $('#modal-form_ad_management').on('shown.bs.modal', function() {
      var status = $('#ad_status_no').val();
      console.log(status+' updated');
      if(status == 1)
      {
        // $(this).find('.btn-enable').removeClass('hide');
        $(this).find('.btn-disable').removeClass('hide');
        $(this).find('.btn-flag').removeClass('hide');
        // $(this).find('.btn-unflag').removeClass('hide');
        $(this).find('.btn-blocked').removeClass('hide');
        // $(this).find('.btn-unblocked').removeClass('hide');
        $(this).find('.btn-delete-ads').removeClass('hide');
      }
      else if(status == 3)
      {
        // $(this).find('.btn-enable').removeClass('hide');
        $(this).find('.btn-flag').removeClass('hide');
        // $(this).find('.btn-unflag').removeClass('hide');
        $(this).find('.btn-blocked').removeClass('hide');
        // $(this).find('.btn-unblocked').removeClass('hide');
        $(this).find('.btn-delete-ads').removeClass('hide');
      }
      else if(status == 4)
      {
        // $(this).find('.btn-enable').removeClass('hide');
        // $(this).find('.btn-flag').removeClass('hide');
        $(this).find('.btn-enable').removeClass('hide');
        $(this).find('.btn-blocked').removeClass('hide');
        $(this).find('.btn-flag').removeClass('hide');
        // $(this).find('.btn-unblocked').removeClass('hide');
        $(this).find('.btn-delete-ads').removeClass('hide');
      }
      else if(status == 2)
      {
        $(this).find('.btn-unblocked').removeClass('hide');
        $(this).find('.btn-delete-ads').removeClass('hide');
      }
      else if(status == 6)
      {
        $(this).find('.btn-unflag').removeClass('hide');
        $(this).find('.btn-blocked').removeClass('hide');
      }
      else
      {
        $(this).find('.btn-enable').addClass('hide');
        $(this).find('.btn-disable').addClass('hide');
        $(this).find('.btn-flag').addClass('hide');
        $(this).find('.btn-unflag').addClass('hide');
        $(this).find('.btn-blocked').addClass('hide');
        $(this).find('.btn-unblocked').addClass('hide');
        $(this).find('.btn-delete-ads').addClass('hide');
      }
    })
    $('#modal-form_ad_management').on('hidden.bs.modal', function() {
      var status = $('#ad_status_no').val();
      // if(status == 1){
        $(this).find('.btn-enable').addClass('hide');
        $(this).find('.btn-disable').addClass('hide');
        $(this).find('.btn-flag').addClass('hide');
        $(this).find('.btn-unflag').addClass('hide');
        $(this).find('.btn-blocked').addClass('hide');
        $(this).find('.btn-unblocked').addClass('hide');
        $(this).find('.btn-delete-ads').removeClass('hide');
      // }
    })

    $('#form-ad_management_save').on('click','button.file_ups', function(){
      console.log('hahaha');
      $('.photo_upload').click();
    });

  function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#row-photo').attr('src', e.target.result);
          $('#no_preview').val(0);
      }
      reader.readAsDataURL(input.files[0]);
  }
}

$(".photo_upload").change(function(){
    readURL(this);
});

$('#form-ad_management_save').on('click','button.file_remove', function(){
    $('#row-photo').attr('src','https://www.attrum.com/Images/nopreview.png');
    $('#no_preview').val(1);
});
</script>
@stop
@section('content')
  <!-- include jquery -->
 <div class="content-wrapper">
   <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <h3 class="noMargin"><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <!-- <button type="button" class="btn btn-md btn-attr pull-right btn-settings" data-settings_id="{{$ad_management_settings->id}}"><i class="fa fa-cog"></i> Settings</button>    -->
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
  <!--       <button type="button" class="btn btn-warning btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-shield"></i> Block</button> -->
     <!--    <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>    
       --></div>
          <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="input-group borderZero">
                <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                <input type="text" class="form-control borderZero" id="row-search">
                </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Usertype</span>
                          <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                    <option value="">All</option>
                                    <option value="1">Active</option>
                                    <option value="2">Blocked</option>
                                    <option value="3">Disabled</option>
                                    <option value="6">Flagged</option>
                          </select>
                      </div>
                 </div>
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                     <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                             <option value="">All</option> 
                            @foreach($countries as $row)
                               <option value="{{strToLower($row->countryCode)}}">{{$row->countryName}}</option>
                            @endforeach
                      </select>
                      </div>
                 </div>
              
            </div>
        </div>
    <div class="col-xs-12 noPAdding">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="userManagement">
          <thead>
            <tr>
              <!-- <th>user</th> -->
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.status" id="row-sort"><i class="fa"></i> Status</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.feature" id="row-sort"><i class="fa"></i> Type</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="users.name" id="row-sort"><i class="fa"></i> User</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.title" id="row-sort"><i class="fa"></i> Title</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.country_code" id="row-sort"><center><i class="fa"></i> Country Code</center></div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="parent_category" id="row-sort"><i class="fa"></i> Category</div></th>
               <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_expiration" id="row-sort"><i class="fa"></i> Time Remaining</div></th>
                <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="ad_bidder" id="row-sort"><i class="fa"></i> Max Bids</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.created_at" id="row-sort"><i class="fa fa-caret-up"></i> Create Date</div></th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows" class="table-hover">
            @foreach($rows as $row)
               <tr data-status_no="{{$row->status}}" data-id="{{$row->id}}" data-title="{{ $row->title }}" data-ads_type_id="{{ $row->ads_type_id }}" data-cat_id="{{ $row->category_id }}" class="{{($row->status == 3 ? 'disabledColor' : '') }} {{($row->status == 2 ? 'blockedColor' : '') }}{{($row->status == 4 ? 'disabledColor' : '') }}{{($row->status == 6 ? 'flaggedColor' : '') }}">
              <td><small>
                   @if ($row->status == '1')
                      Active
                  @elseif ($row->status == '2')
                      Blocked
                  @elseif ($row->status == '3')
                      Disabled 
                  @elseif($row->status == '4')
                      Disabled
                  @elseif($row->status == '6')
                      Flagged
                  @endif
                </small>
                </td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->feature == 1 ? 'Featured':'Regular'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->name}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->title}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>{{strtoUpper($row->country_code)}}</center></small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->catName}}</small></td>
                <td><small><span data-countdown="{{$row->ad_expiration}}"></span></small></td>
                <td><small>{{(empty($row->ad_bidder) ? '0':floor($row->ad_bidder))}}/{{(empty($row->bid_limit_amount) ? '0':$row->bid_limit_amount)}} USD</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->created_at}}</small></td>
                <td>
               <!--  @if($row->status == 5)
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unmark-spam"><i class="fa fa-envelope"></i></button>
                @else
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-mark-spam"><i class="fa fa-envelope-o"></i></button>
                @endif -->
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads {{$row->status == 7 ? 'hide':''}}" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button>
               @if($row->status == '2')
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unblocked" data-toggle="tooltip" title="Unblock"><i class="fa fa-shirtsinbulk"></i></button>
               @else 
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-blocked"  data-toggle="tooltip" title="Block"><i class="fa fa-shield"></i></button>
               @endif 

               @if($row->status == 3 || $row->status == 4)
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"  data-toggle="tooltip" title="Enable" {{$row->user_status == 2 ? 'disabled':''}}><i class="fa fa-unlock"></i></button>
               @else
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"  data-toggle="tooltip" title="Disable" {{$row->status == 2 ? 'disabled':''}}><i class="fa fa-lock"></i></button>
               @endif

                @if($row->status == '6')
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unflag"   data-toggle="tooltip" title="Unflag" {{$row->user_status == 2 ? 'disabled':''}}><i class="fa fa-flag-o"></i></button>
                @else
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-flag"  data-toggle="tooltip" title="Flag" {{$row->status == 2 ? 'disabled':''}}><i class="fa fa-flag"></i></button>
                @endif
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"  data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"  data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                </td> 
              </tr>
            @endforeach
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
@stop
@include('admin.ads-management.regular-ads.form')