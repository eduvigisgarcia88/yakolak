@extends('layout.master')
@section('scripts')
<script type="text/javascript">
$loading = false;
$selectedID = 0;
   $_token = '{{ csrf_token() }}';
function refresh() {
      $_token = $("#row-token").val();
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $status = $("#row-filter_status").val();
      $listing_type = $("#row-filter_listing_type").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
         $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class="'+ (row.status == 4  ? 'blockedColor' : '')+''+ (row.status == 3  ? 'unapprovedColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'}}'+row.country_code+'/'+row.ad_type_slug+'/'+row.parent_category_slug+'/'+row.category_slug+'/'+row.slug+'" target = "_blank">'+row.title+'</a></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+ ( row.status == 2 ? 'Pending Approval':'') + ( row.status == 1 ? 'Approved':'')+( row.status == 3 ? 'Rejected':'')+( row.status == 4 ? 'Blocked':'')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.listing_type == 1 ? 'Comment':'Review')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.ad_type == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.username + '</small></td>' +
              (row.rate == null ?'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>':'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_'+row.id+'" ></td>') +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +
              // '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>'+row.CommentReply + '</center></small></td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+

               
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject" '+(row.status == 4 ? 'disabled':'')+''+(row.status == 1 ? 'disabled':'')+'><i class="fa fa-close"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.status == 4 ? 'disabled':'')+'><i class="fa fa-check"></i></button>')+

                (row.status == 4  ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' + 
              '</td></tr>';
        });
      
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");
         $.each(response.rows.data, function(index, row) {
            $("#comment_rating_"+row.id).rateYo({
                 starWidth: "15px",
                 rating: (row.rate == null ? 0:row.rate),
                 readOnly: true,
                 ratedFill: "#5da4ec",
                 normalFill: "#cccccc",
            });
       });
      }, 'json');
  }



    function search() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $listing_type = $("#row-filter_listing_type").val();
      $ad_type = $("#row-filter_ad_type").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class="'+ (row.status == 4  ? 'blockedColor' : '')+''+ (row.status == 3  ? 'unapprovedColor' : '')+'">' + 
               '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'}}'+row.country_code+'/'+row.ad_type_slug+'/'+row.parent_category_slug+'/'+row.category_slug+'/'+row.slug+'" target = "_blank">'+row.title+'</a></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+ ( row.status == 2 ? 'Pending Approval':'') + ( row.status == 1 ? 'Approved':'')+( row.status == 3 ? 'Rejected':'')+( row.status == 4 ? 'Blocked':'')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.listing_type == 1 ? 'Comment':'Review')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.ad_type == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.username + '</small></td>' +
               (row.rate == null ?'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>':'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_'+row.id+'" ></td>') +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +
              // '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>' + row.CommentReply + '</center></small></td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+

             
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject" '+(row.status == 4 ? 'disabled':'')+''+(row.status == 1 ? 'disabled':'')+'><i class="fa fa-close"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.status == 4 ? 'disabled':'')+'><i class="fa fa-check"></i></button>')+


                (row.status == 4  ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' + 
              '</td></tr>';
        });
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
        $.each(response.rows.data, function(index, row) {
            $("#comment_rating_"+row.id).rateYo({
                 starWidth: "15px",
                 rating: (row.rate == null ? 0:row.rate),
                 readOnly: true,
                 ratedFill: "#5da4ec",
                 normalFill: "#cccccc",
            });
       });
      }, 'json');
    
    }
     function maxAds() {
      $_token = $("#row-token").val();

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $listing_type = $("#row-filter_listing_type").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {     
       table.html("");
      console.log(response);
      var body = "";
       $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+ (row.status == 4  ? 'blockedColor' : '')+''+ (row.status == 3  ? 'unapprovedColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'}}'+row.country_code+'/'+row.ad_type_slug+'/'+row.parent_category_slug+'/'+row.category_slug+'/'+row.slug+'" target = "_blank">'+row.title+'</a></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+ ( row.status == 2 ? 'Pending Approval':'') + ( row.status == 1 ? 'Approved':'')+( row.status == 3 ? 'Rejected':'')+( row.status == 4 ? 'Blocked':'')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.listing_type == 1 ? 'Comment':'Review')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.ad_type == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.username + '</small></td>' +
               (row.rate == null ?'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>':'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_'+row.id+'" ></td>') +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +
              // '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>' + row.CommentReply + '</center></small></td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+

              
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject" '+(row.status == 4 ? 'disabled':'')+''+(row.status == 1 ? 'disabled':'')+'><i class="fa fa-close"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.status == 4 ? 'disabled':'')+'><i class="fa fa-check"></i></button>')+


                (row.status == 4  ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' + 
              '</td></tr>';
        });
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
        $.each(response.rows.data, function(index, row) {
            $("#comment_rating_"+row.id).rateYo({
                 starWidth: "15px",
                 rating: (row.rate == null ? 0:row.rate),
                 readOnly: true,
                 ratedFill: "#5da4ec",
                 normalFill: "#cccccc",
            });
       });
      }, 'json');
    
    }
     $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $listing_type = $("#row-filter_listing_type").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {    
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+ (row.status == 4  ? 'blockedColor' : '')+''+ (row.status == 3  ? 'unapprovedColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'}}'+row.country_code+'/'+row.ad_type_slug+'/'+row.parent_category_slug+'/'+row.category_slug+'/'+row.slug+'" target = "_blank">'+row.title+'</a></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+ ( row.status == 2 ? 'Pending Approval':'') + ( row.status == 1 ? 'Approved':'')+( row.status == 3 ? 'Rejected':'')+( row.status == 4 ? 'Blocked':'')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.listing_type == 1 ? 'Comment':'Review')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.ad_type == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.username + '</small></td>' +
               (row.rate == null ?'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>':'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_'+row.id+'" ></td>') +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +
              // '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>' + row.CommentReply + '</center></small></td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+

             
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject" '+(row.status == 4 ? 'disabled':'')+''+(row.status == 1 ? 'disabled':'')+'><i class="fa fa-close"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.status == 4 ? 'disabled':'')+'><i class="fa fa-check"></i></button>')+


                (row.status == 4  ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' + 
              '</td></tr>';
        });
      
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
        $.each(response.rows.data, function(index, row) {
            $("#comment_rating_"+row.id).rateYo({
                 starWidth: "15px",
                 rating: (row.rate == null ? 0:row.rate),
                 readOnly: true,
                 ratedFill: "#5da4ec",
                 normalFill: "#cccccc",
            });
       });
      }, 'json');
    
      }
    });
   $("#userManagement").on("click", "#row-sort", function() {
          $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $ad_type = $("#row-filter_ad_type").val();
          $search_tool = $("#row-filter_search_tool").val();
          $listing_type = $("#row-filter_listing_type").val();
          $status = $("#row-filter_status").val();
          $per = $("#row-per").val();
          $("#userManagement").find("i").removeClass('fa-caret-up');
          $("#userManagement").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
          if($order == "asc"){
            $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
         }else{
            $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
          }
        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {    
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class="'+ (row.status == 4  ? 'blockedColor' : '')+''+ (row.status == 3  ? 'unapprovedColor' : '')+'">' + 
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'}}'+row.country_code+'/'+row.ad_type_slug+'/'+row.parent_category_slug+'/'+row.category_slug+'/'+row.slug+'" target = "_blank">'+row.title+'</a></small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+ ( row.status == 2 ? 'Pending Approval':'') + ( row.status == 1 ? 'Approved':'')+( row.status == 3 ? 'Rejected':'')+( row.status == 4 ? 'Blocked':'')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.listing_type == 1 ? 'Comment':'Review')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>'+(row.ad_type == 1 ? 'Ad Listings':'Auction')+'</small></td>' +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.username + '</small></td>' +
              (row.rate == null ?'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>':'<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_'+row.id+'" ></td>') +
              '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>' + row.created_at + '</small></td>' +
              // '<td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><center>' + row.CommentReply + '</center></small></td>' +
              '<td>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+

                
               
                (row.status == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reject" '+(row.status == 4 ? 'disabled':'')+''+(row.status == 1 ? 'disabled':'')+'><i class="fa fa-close"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.status == 4 ? 'disabled':'')+'><i class="fa fa-check"></i></button>')+


                (row.status == 4  ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' + 
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
            $.each(response.rows.data, function(index, row) {
            $("#comment_rating_"+row.id).rateYo({
                 starWidth: "15px",
                 rating: (row.rate == null ? 0:row.rate),
                 readOnly: true,
                 ratedFill: "#5da4ec",
                 normalFill: "#cccccc",
            });
           });
         
         

        
          if(response.order == "asc"){
              $("#row-order").val("desc");
              //  $("#userManagement").find("i").find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          }else{
              $('#row-order').val("asc");
              // $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          }
      }, 'json');
  

    });

    $(".content-wrapper").on("click", ".btn-add", function() {
           $_token = $("#row-token").val();
           $("#modal-spam-and-bot").modal("show");
           $("#modal-spam-and-bot").find('form').trigger("reset");
           $(".uploader-pane").removeClass("hide");
           $("#row-ad_type").removeAttr('disabled','disabled');
           $("#row-title").removeAttr('disabled','disabled');
           $("#row-category").removeAttr('disabled','disabled');
           $("#row-description").removeAttr('disabled','disabled');
           $("#button-save").removeClass("hide");
           $(".img-responsive").addClass("hide");
           $(".modal-title").html('<i class="fa fa-plus"></i><strong>ADD SPAM AND BOT KEYWORD</strong>');
           $("#modal-form-add").modal('show');
        
    });
    @foreach($rows as $row)
      $("#comment_rating_{{$row->id}}").rateYo({
       starWidth: "15px",
           rating: {{ ($row->rate != null ? $row->rate:0) }},
           readOnly: true,
           ratedFill: "#5da4ec",
           normalFill: "#cccccc",
      });
    @endforeach
 $(".content-wrapper").on("click", ".btn-edit", function() {
      $_token = $("#row-token").val();
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var btn = $(this);
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/spam-and-bots/edit') }}", { id: id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.restricted_word + '</strong>');
              // output form data
            
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  console.log(index);

                  if(field.length > 0) {
                    field.val(value);
                  }
        
              });

          // show form
             $("#modal-spam-and-bot").modal('show');
             $("#modal-spam-and-bot").find("form").find(".hide-view").removeAttr("disabled","disabled");
             $("#modal-spam-and-bot").find("form").find(".btn-hide-view").removeClass("hide");
          } else {
              status(false, response.error, 'alert-danger');
          }
          
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');

    });





$(".content-wrapper").on("click", ".btn-view", function() {
      $_token = $("#row-token").val();
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var btn = $(this);

      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/comments-and-reviews/view') }}", { id: id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          console.log(response);
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-eye"></i> View <strong>' + response.id + '</strong>');
              // output form data
            
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  console.log(index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                  if(index == "ad_id"){
                    $("#row-title").val(response.get_comment_ad.title);
                  }

                  if(index == "status"){
                    if(value == '1'){
                       $("#modal-comments-and-reviews").find("#row-status").val("Approved");
                       $("#modal-comments-and-reviews").find(".btn-approve").addClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-reject").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-unblocked").addClass("hide");
                    }else if(value == 2){
                       $("#modal-comments-and-reviews").find("#row-status").val("Pending Approval");
                       $("#modal-comments-and-reviews").find(".btn-approve").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-reject").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-unblocked").addClass("hide");
                    }else if(value == 4){
                       $("#modal-comments-and-reviews").find("#row-status").val("Blocked");
                       $("#modal-comments-and-reviews").find(".btn-approve").addClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-reject").addClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-unblocked").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-blocked").addClass("hide");
                    }else if(value == 3){
                       $("#modal-comments-and-reviews").find("#row-status").val("Rejected");
                       $("#modal-comments-and-reviews").find(".btn-approve").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-reject").addClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews").find(".btn-unblocked").addClass("hide");
                    }
                   
                  }
              });

          // show form
             $("#modal-comments-and-reviews").find("form").find(".hide-view").attr("disabled","disabled");
             $("#modal-comments-and-reviews").find("form").find(".btn-hide-view").addClass("hide");
             $("#modal-comments-and-reviews").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });
  $(".content-wrapper").on("click", ".btn-view-reply", function() {
      $_token = $("#row-token").val();
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      var btn = $(this);

      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/ads/comments-and-reviews/reply/view') }}", { id: id, _token: $_token }, function(response) {   
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          console.log(response);
          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-eye"></i> View <strong>' + response.id + '</strong>');
              // output form data
            
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);
                  console.log(index);

                  if(field.length > 0) {
                    field.val(value);
                  }
                  if(value =="content"){
                    $("#row-content").val(response.getCommentReplyUser.name);
                  }

                  if(index == "status"){
                    if(value == '1'){
                       $("#modal-comments-and-reviews-reply").find("#row-status").val("Approved");
                       $("#modal-comments-and-reviews-reply").find(".btn-approve").addClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-reject").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-unblocked").addClass("hide");
                    }else if(value == 2){
                       $("#modal-comments-and-reviews-reply").find("#row-status").val("Pending Approval");
                       $("#modal-comments-and-reviews-reply").find(".btn-approve").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-reject").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-unblocked").addClass("hide");
                    }else if(value == 4){
                       $("#modal-comments-and-reviews-reply").find("#row-status").val("Blocked");
                       $("#modal-comments-and-reviews-reply").find(".btn-approve").addClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-reject").addClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-unblocked").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-blocked").addClass("hide");
                    }else if(value == 3){
                       $("#modal-comments-and-reviews-reply").find("#row-status").val("Rejected");
                       $("#modal-comments-and-reviews-reply").find(".btn-approve").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-reject").addClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-blocked").removeClass("hide");
                       $("#modal-comments-and-reviews-reply").find(".btn-unblocked").addClass("hide");
                    }
                   
                  }
              });

          // show form
             $("#modal-comments-and-reviews-reply").find("form").find(".hide-view").attr("disabled","disabled");
             $("#modal-comments-and-reviews-reply").find("form").find(".btn-hide-view").addClass("hide");
             $("#modal-comments-and-reviews-reply").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });
  //  $(".content-wrapper").on("click", ".btn-delete-ads", function() {
  //     var id = $(this).parent().parent().data('id');
  //     var title = $(this).parent().parent().data('title');
  //     $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
  //     dialog('Delete Ad', 'Are you sure you want to delete ' + title + '</strong>?', "{{ url('admin/ads/delete') }}",id);
  // });


  // $(".content-wrapper").on("click", ".btn-mark-spam", function() {
  //     var id = $(this).parent().parent().data('id');
  //     var title = $(this).parent().parent().data('title');
  //     $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
  //     dialog('Mark as Spam', 'Are you sure you want to mark as spam ' + title + '</strong>?', "{{ url('admin/ads/mark-spam') }}",id);
  // });

  // $(".content-wrapper").on("click", ".btn-unmark-spam", function() {
  //     var id = $(this).parent().parent().data('id');
  //     var title = $(this).parent().parent().data('title');
  //     $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
  //     dialog('UnMark as Spam', 'Are you sure you want to unmark as spam ' + title + '</strong>?', "{{ url('admin/ads/unmark-spam') }}",id);
  // });
  //for comment
  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete', 'Are you sure you want to delete  this comment ?', "{{ url('admin/ads/comments-and-reviews/delete') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-blocked", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Block', 'Are you sure you want to block  this comment ?', "{{ url('admin/ads/comments-and-reviews/block-comment') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-unblocked", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnBlock', 'Are you sure you want to unblock  this comment ?', "{{ url('admin/ads/comments-and-reviews/unblock-comment') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-approve", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approval', 'Are you sure you want to approve  this comment ?', "{{ url('admin/ads/comments-and-reviews/approve-comment') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-reject", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Reject', 'Are you sure you want to reject this comment ?', "{{ url('admin/ads/comments-and-reviews/reject-comment') }}",id);
  });



  //for modal
  $("#modal-comments-and-reviews").on("click", ".btn-approve", function() {
      $("#modal-comments-and-reviews").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approval', 'Are you sure you want to approve this comment ?', "{{ url('admin/ads/comments-and-reviews/approve-comment') }}",id);
  });
  $("#modal-comments-and-reviews").on("click", ".btn-reject", function() {
      $("#modal-comments-and-reviews").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Reject', 'Are you sure you want to reject this comment ?', "{{ url('admin/ads/comments-and-reviews/reject-comment') }}",id);
  });
  $("#modal-comments-and-reviews").on("click", ".btn-blocked", function() {
      $("#modal-comments-and-reviews").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Block', 'Are you sure you want to block this comment ?', "{{ url('admin/ads/comments-and-reviews/block-comment') }}",id);
  });
  $("#modal-comments-and-reviews").on("click", ".btn-unblocked", function() {
      $("#modal-comments-and-reviews").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnBlock', 'Are you sure you want to unblock this comment ?', "{{ url('admin/ads/comments-and-reviews/unblock-comment') }}",id);
  });

  $("#modal-comments-and-reviews").on("click", ".btn-delete", function() {
      $("#modal-comments-and-reviews").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete', 'Are you sure you want to unblock this comment ?', "{{ url('admin/ads/comments-and-reviews/delete') }}",id);
  });


  ///for comment reply
   $(".content-wrapper").on("click", ".btn-delete-reply", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete', 'Are you sure you want to delete this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/delete') }}",id);
   });
   $(".content-wrapper").on("click", ".btn-approve-reply", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approve', 'Are you sure you want to approve this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/approve-comment') }}",id);
   });
   $(".content-wrapper").on("click", ".btn-reject-reply", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Reject', 'Are you sure you want to reject this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/reject-comment') }}",id);
   });
   $(".content-wrapper").on("click", ".btn-blocked-reply", function() {
      var id = $(this).parent().parent().data('id');
      console.log(id);
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Block', 'Are you sure you want to block this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/block-comment') }}",id);
   });
   $(".content-wrapper").on("click", ".btn-unblocked-reply", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Unblock', 'Are you sure you want to unblock this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/unblock-comment') }}",id);
   });
   //for modal reply
  $("#modal-comments-and-reviews-reply").on("click", ".btn-approve", function() {
      $("#modal-comments-and-reviews-reply").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approval', 'Are you sure you want to approve this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/approve-comment') }}",id);
  });
  $("#modal-comments-and-reviews-reply").on("click", ".btn-reject", function() {
      $("#modal-comments-and-reviews-reply").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Reject', 'Are you sure you want to reject this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/reject-comment') }}",id);
  });
  $("#modal-comments-and-reviews-reply").on("click", ".btn-blocked", function() {
      $("#modal-comments-and-reviews-reply").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Block', 'Are you sure you want to block this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/block-comment') }}",id);
  });
  $("#modal-comments-and-reviews-reply").on("click", ".btn-unblocked", function() {
      $("#modal-comments-and-reviews-reply").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('UnBlock', 'Are you sure you want to unblock this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/unblock-comment') }}",id);
  });

  $("#modal-comments-and-reviews-reply").on("click", ".btn-delete", function() {
      $("#modal-comments-and-reviews-reply").modal("hide");
      var id = $("#row-id").val();
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete', 'Are you sure you want to unblock this comment ?', "{{ url('admin/ads/comments-and-reviews/reply/delete') }}",id);
  });



   $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });
    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });
     $(".content-wrapper").on("click", ".btn-expand", function() {
      $button = $(this);
      $button.prop('disabled', true);
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
    
        if ($loading){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return;
        }
        $selectedID = $id;
         
        // $button.html('<i class="fa fa-spinner fa-spin"></i>');
        // $loading = true;
         $(".loading-pane").removeClass("hide");
        $.post("{{ url('admin/ads/get-comment-replies') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="10" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="expand-table row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');
        $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
           $(".loading-pane").addClass("hide");
          // $loading = false;
        }).complete(function(){ $button.prop('disabled', false); });

 });
</script>
@stop
@section('content')
  <!-- include jquery -->
 <div class="content-wrapper">
   <div class="container-fluid">
     <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3 class="noMargin"><i class="fa fa-buysellads"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-user-plus"></i> Add</button>  -->
        <!-- <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-recaptcha-settings"><i class="fa fa-wrench"></i>  Recaptcha Settings</button>    -->
      </div>

      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="input-group borderZero">
                <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                <input type="text" class="form-control borderZero" id="row-search">
                </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                      <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                          <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Approved</option>
                                <option value='2'>Pending Approval</option>
                                <option value='3'>Rejected</option>
                                <option value='4'>Blocked</option>
                           </select>
                      </div>
                 </div>
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                     <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Ad Type</span>
                      <select class="form-control borderZero" id="row-filter_ad_type" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Ad Listings</option>
                                <option value='2'>Auction</option>
                      </select>
                      </div>
                 </div>
                 <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                     <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Listing Type</span>
                      <select class="form-control borderZero" id="row-filter_listing_type" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Comment</option>
                                <option value='2'>Review</option>
                      </select>
                      </div>
                 </div>
            </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
           <div id="load-ads_form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="userManagement">
          <thead>
            <tr>
              <!-- <th>user</th> -->
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="advertisements.title" id="row-sort"><i class="fa"></i> Listing Title</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_ad_comments.status" id="row-sort"><i class="fa"></i> Status</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_ad_comments.listing_type" id="row-sort"><i class="fa"></i> Listing Type</div></th>
               <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_ad_comments.ad_type" id="row-sort"><i class="fa"></i> Type</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="users.username" id="row-sort"><i class="fa"></i> Username</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="rate" id="row-sort"><i class="fa"></i> Rating</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="created_at" id="row-sort"><i class="fa fa-caret-up"></i> Create Date</div></th>
            <!--   <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="CommentReply" id="row-sort"><i class="fa"></i> Pending Replies</div></center></th> -->
              <th class="text-right">Tools</th>
            </tr>
          </thead>
          <tbody id="rows" class="table-hover table-comments-and-reviews">
            @foreach($rows as $row)
              <tr data-id="{{$row->id}}" data-title= "{{$row->id}}" class="{{($row->status == 4 ? 'blockedColor' : '') }} {{$row->status == 3 ? 'unapprovedColor':''}}">
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small><button class="btn btn-xs btn-toggle btn-table btn-expand" data-id="{{ $row->id }}"><i class="fa fa-plus"></i></button><a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}" target="_blank">{{$row->title}}</a></small></td>  
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{($row->status == 2 ? 'Pending Approval':'')}}{{($row->status == 1 ? 'Approved':'')}}{{$row->status == 3 ? 'Rejected':''}}{{$row->status == 4 ? 'Blocked':''}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->listing_type == 1 ? 'Comment':'Review'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->ad_type == 1 ? 'Ad Listings':'Auction'}}</small></td>
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->username}}</small></td>
                @if($row->rate == null)
                     <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;"><small>&nbsp;&nbsp;&nbsp;&nbsp;N/A</small></td>
                @else
                     <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;border-top:0px !important;" id="comment_rating_{{$row->id}}"></td>
                @endif
           
                <td style=" max-width: 100px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;"><small>{{$row->created_at}}</small></td>
                <!-- <td><center>{{$row->CommentReply}}</center></td> -->
                <td>
                
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                  <!-- <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete-ads {{$row->status == 3 ? 'hide':''}}"><i class="fa fa-trash-o"></i></button> -->
                  @if($row->status == '1')
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-reject" {{$row->status == 4 ? 'disabled':''}} {{$row->status == 1 ? 'disabled':''}}><i class="fa fa-close"></i></button>
                  @else
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-approve" {{$row->status == 4 ? 'disabled':''}}><i class="fa fa-check"></i></button>
                  @endif
                  @if($row->status =='4')
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-unblocked"><i class="fa fa-shirtsinbulk "></i></button>
                  @else
                     <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-blocked"><i class="fa fa-shield"></i></button>
                  @endif
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>
                  <!-- <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button> -->
                </td> 
              </tr>
            @endforeach
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}}" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
@stop
@include('admin.ads-management.comments-and-reviews.form')