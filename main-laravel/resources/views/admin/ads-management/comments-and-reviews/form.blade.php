<!-- modal add user -->
  <div class="modal fade" id="modal-comments-and-reviews" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/ads/spam-and-bots/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-spam-and-bot', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i>View</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">AD Title</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="title" class="form-control hide-view" id="row-title" maxlength="30">
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Comment</label>
                <div class="col-sm-9 col-xs-7">
                   <textarea name="restricted_word" class="form-control hide-view" id="row-comment" maxlength="30">></textarea>
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Status</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="restricted_word" class="form-control hide-view" id="row-status" maxlength="30">
                </div>               
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
                <button type="button" class="btn btn-success btn-approve pull-left hide"><i class="fa fa-check"></i> Approved</button>
                <button type="button" class="btn btn-danger btn-reject pull-left hide"><i class="fa fa-close"></i> Reject</button>
                <button type="button" class="btn btn-warning btn-blocked pull-left hide"><i class="fa fa-shield"></i> Block</button>
                <button type="button" class="btn btn-warning btn-unblocked pull-left hide"><i class="fa fa-shirtsinbulk"></i> UnBlock</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success btn-hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>

              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
     <div class="modal fade" id="modal-comments-and-reviews-reply" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/ads/spam-and-bots/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-form-spam-and-bot', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i>View</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <!-- <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">AD Title</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="title" class="form-control hide-view" id="row-title" maxlength="30">
                </div>               
                </div> -->
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Comment</label>
                <div class="col-sm-9 col-xs-7">
                   <textarea name="restricted_word" class="form-control hide-view" id="row-content" maxlength="30">></textarea>
                </div>               
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 font-color">Status</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="restricted_word" class="form-control hide-view" id="row-status" maxlength="30">
                </div>               
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <input type="hidden" name="token" id="row-token" value="{{ csrf_token()}}">
                <button type="button" class="btn btn-success btn-approve pull-left hide"><i class="fa fa-check"></i> Approved</button>
                <button type="button" class="btn btn-danger btn-reject pull-left hide"><i class="fa fa-close"></i> Reject</button>
                <button type="button" class="btn btn-warning btn-blocked pull-left hide"><i class="fa fa-shield"></i> Block</button>
                <button type="button" class="btn btn-warning btn-unblocked pull-left hide"><i class="fa fa-shirtsinbulk"></i> UnBlock</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success btn-hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>

              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  