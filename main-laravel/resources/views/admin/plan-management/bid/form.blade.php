<!-- modal add country -->
  <div class="modal fade" id="modal-bid-points" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/plan/bid/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-bid_points', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"></h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-2 col-xs-5 control-label font-color">Bid Points</label>
                <div class="col-sm-10 col-xs-7">
                   <input type="text" name="points" class="form-control disable-view" id="row-points" maxlength="30">
                </div>
                </div>
               <div class="form-group">
                    <label for="row-name" class="col-sm-2 col-xs-5 control-label font-color">Amount</label>
                <div class="col-sm-10 col-xs-7">
                   <input type="text" name="price" class="form-control disable-view" id="row-price" maxlength="30">
                </div>
                </div>
              </div>
             

              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  <!-- modal form add discount -->
  <div class="modal fade" id="modal-bid-discount" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-wrench"></i> DISCOUNT OPTIONS</h4>
        </div>
            <div id="form-plan_price_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan/promotion-bid', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-add_plan_discount', 'files' => true)) !!}
            <div class="modal-body"> 
              <div class="row" style="padding:10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                @foreach($promotion_bid_discounts as $row)
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                       <div class="form-group">
                        <label for="row-name" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 control-label font-color">Period</label>
                           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 input-group">
                             <input type="number" min="1" class="form-control" id="row-period" name="period[{{$row->id}}]" value="{{$row->period}}">
                             <span class="input-group-addon" style="padding-left:10px !important"><i class="fa fa-sun-o"></i></span>
                           </div>
                       </div>
                    </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:30px;">
                       <div class="form-group">
                        <label for="row-name" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 control-label font-color">Discount</label>
                           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 input-group">
                             <input type="number" class="form-control" id="row-discount" name="discount[{{$row->id}}]" value="{{$row->discount}}">
                             <span class="input-group-addon" style="padding-left:10px !important"><i class="fa fa-percent"></i></span>
                           </div>
                       </div>
                    </div>
                @endforeach
               </div>
            </div>
          </div>
        <div class="modal-footer">
         <input type="hidden" name="plan_id" id="row-plan_id" value="">
         <input type="hidden" name="price_id" id="row-price_id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
  {!! Form::close() !!}
   
      </div>
    </div>
  </div>