@extends('layout.master')
@section('scripts')
<script>
$_token = "{{csrf_token()}}";
function refresh(){
      var table = $("#rows");
      $loading_expand = false;
      $expand_id = 0;
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
           
            body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
             '<td><small>'+ row.points +'</small></td>'+
             '<td><small>'+ row.price +'</small></td>'+
             '<td class="rightalign">'+  
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove" title="Remove"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 '</td></tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
}
function search(){
  refresh();
}
function max(){
  refresh();
}
 $("#bidManagement").on("click", "#row-sort", function() {
                $('.loading-pane').removeClass('hide');
                $page = $("#row-page").val();
                $order = $("#row-order").val();
                $sort =  $(this).data('sort');
                $search = $("#row-search").val();
                $status = $("#row-filter_status").val();
                $usertype_id = $("#row-filter_usertype_id").val();
                // $country = $("#row-filter_country").val();
                $per = $("#row-per").val();
                $("#bidManagement").find("i").removeClass('fa-caret-up');
                $("#bidManagement").find("i").removeClass('fa-caret-down');
                // $(this).find("i").addClass('fa-caret-down');
                var loading = $(".loading-pane");
                var table = $("#rows");
                 if($order == "asc"){
                    $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
                 }else{
                    $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
                 } 
                
            $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
              
              var body = "";
              // clear table
              table.html("");
               $.each(response.rows.data, function(index, row) { 
                  body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
                   '<td><small>'+ row.points +'</small></td>'+
                   '<td><small>'+ row.price +'</small></td>'+
                   '<td class="rightalign">'+  
                       '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove" title="Remove"><i class="fa fa-trash-o"></i></button>' +
                       '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                       '</td></tr>';
              });
                table.html(body);
                $('#row-pages').html(response.pages);
                loading.addClass("hide");
                console.log(response.order);
                if(response.order == "asc"){
                    $("#row-order").val("desc");
                  
                }else{
                    $('#row-order').val("asc");
                }
             
            }, 'json');
      });
      $(".content-wrapper").on('click', '.pagination a', function (event) {
            event.preventDefault();
            if ( $(this).attr('href') != '#' ) {

            $("html, body").animate({ scrollTop: 0 }, "fast");
            $("#row-page").val($(this).html());

            $('.loading-pane').removeClass('hide');
            $page = $("#row-page").val();
            $order = $("#row-order").val();
            $sort = $("#row-sort").val();
            $search = $("#row-search").val();
            $status = $("#row-filter_status").val();
            $usertype_id = $("#row-filter_usertype_id").val();
            $per = $("#row-per").val();

            var loading = $(".loading-pane");
            var table = $("#rows");
            $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
              // clear
              table.html("");
              
              var body = "";
              $.each(response.rows.data, function(index, row) {
                  body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
                       '<td><small>'+ row.points +'</small></td>'+
                       '<td><small>'+ row.price +'</small></td>'+
                       '<td class="rightalign">'+  
                           '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove" title="Remove"><i class="fa fa-trash-o"></i></button>' +
                           '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                           '</td></tr>';
              });
              table.html(body);

              $('#row-pages').html(response.pages);
              loading.addClass("hide");

            }, 'json');

            }
    });
$(".content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      $("#modal-bid-points").find(".alert").remove();
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan/bid/manipulate') }}", { id: id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Edit Bid Plan');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                   $("#modal-bid-points").find("form").find(".disable-view").removeAttr("disabled","disabled");
                      // $("#button-save").removeClass("hide");

              // show form

                   $("#modal-bid-points").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
});
$('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
});
$(".content-wrapper").on("click", ".btn-bid-discount", function() {
   $("#modal-bid-discount").modal("show");
});
$(".content-wrapper").on("click", ".btn-add", function() {
   $("#modal-bid-points").find(".alert").remove();
   $("#modal-bid-points").modal("show");
   $(".modal-title").html('<i class="fa fa-pencil"></i> Add Bid Plan');
   $("#modal-bid-points").find("#row-id").val("");
   $("#modal-bid-points").find("form").trigger("reset");
});
$(".content-wrapper").on("click", ".btn-remove", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Remove Bid Plan', 'Are you sure you want to remove this plan ?', "{{ url('admin/plan/bid/remove') }}",id);
});
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-usd"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
   <!--      <button type="button" class="btn btn-danger btn-md btn-attr pull-right" disabled><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>   
        <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-bid-discount"><i class="fa fa-wrench"></i> Discount Options</button> 
       
      </div>
      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 filters">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <!--  <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 filters">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                      <select class="form-control borderZero" id="row-filter_usertype_id" onchange="refresh()">
                               <option value="">All</option>
                               <option value="1">Enabled</option>
                               <option value="2">Disabled</option>
                      </select>
                  </div>
             </div> -->
        </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed backend-generic-table" id="bidManagement">
          <thead>
            <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="points" id="row-sort"><i class="fa"></i> Bid Points</div></th>
            <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="price" id="row-sort"><i class="fa fa-caret-up"></i> Amount</div></th>
            <th class="text-right">Tools</th>
          </thead>
            <tbody id="rows"> 
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-plan_name="{{ $row->plan_name }}" data-usertype_id="{{ $row->usertype_id }}" class="{{($row->status == 2 ? 'disabledColor' : '') }}">
              <td><small>{{ $row->points }}</small></td>
              <td><small>{{ $row->price }}</small></td>
              <td class="rightalign">
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove" title="Remove"><i class="fa fa-trash-o"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
              </td>
           </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="desc">
       <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="max()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.plan-management.bid.form')