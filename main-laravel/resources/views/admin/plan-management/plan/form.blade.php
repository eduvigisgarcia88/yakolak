<div class="modal fade" id="modal-form-add-plan" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-plus"></i> Add User Plan</h4>
        </div>
        <div id="form-user_plan_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-user_plan_save_form', 'files' => true)) !!}

            <div class="modal-body"> 
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                 <div class="col-lg-12 col-md-12 col-xs-12">
                <h4 style="padding-left: 30px;">Ad Information</h4>
                </div>
               <div class="col-lg-6 col-md-6 col-xs-6">
                <div class="form-group">
                        <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">UserType</label>
                <div class="col-sm-7 col-xs-7">
                       <select type="text" class="form-control disable-view" id="row-usertype_id" name="usertype_id">
                           @foreach ($usertype as $usertypes)
                            <option value="{{ $usertypes->id}}" {{($usertypes->id==$row->usertype_id ? 'selected' : '')}}>{{ $usertypes->type_name }}</option>
                           @endforeach
                      </select> 
                </div>
                </div>
            </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Plan Name</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-plan_name" name="plan_name">
                       </div>
                   </div>
                </div>
                    <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Ad</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-ads" name="ads">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Auction</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-auction" name="auction">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Image per Ad</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-img_per_ad" name="img_per_ad">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Image</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-img_total" name="img_total">
                       </div>
                   </div>
                </div>
                  <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Video</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-video_total" name="video_total">
                       </div>
                   </div>
                </div>
                       <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Total Ads Allowed</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-total_ads_allowed" name="total_ads_allowed">
                       </div>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12 modal-footer">
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                <h4 style="padding-left: 30px;">Bid Information</h4>
                </div>

                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Points</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-point" name="point">
                       </div>
                   </div>
                </div>
                   <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Bids</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-bid" name="bid">
                       </div>
                   </div>
                </div>
<!--                    <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Point Exchange</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-point_exchange" name="point_exchange">
                       </div>
                   </div>
                </div> -->
               
                  <div class="col-lg-12 col-md-12 col-xs-12 modal-footer">
                </div>
                  <div class="col-lg-12 col-md-12 col-xs-12">
                <h4 style="padding-left: 30px;">Other Information</h4>
                </div>
                 <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Reward Points</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-reward_point" name="reward_point">
                       </div>
                   </div>
                </div>
                    <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">SMS</label>
                       <div class="col-sm-7 col-xs-7">
                         <input type="text" class="form-control disable-view" id="row-sms" name="sms">
                       </div>
                   </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-6">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-5 col-xs-5 control-label font-color">Theme</label>
                       <div class="col-sm-7 col-xs-7">
                         <select name="theme" id="row-theme" class="form-control disable-view">
                          @foreach($plan_themes as $row)
                            <option value="{{$row->id}}">{{$row->name}}</option>
                          @endforeach
                         </select>
                       </div>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-sm-2 col-xs-2 control-label font-color">Description</label>
                       <div class="col-sm-10 col-xs-10">
                        <textarea class="form-control disable-view" name="description" id="row-description" rows="10" cols="20"></textarea>
                       </div>
                   </div>
                </div>
   </div>
   </div>
   </div>
        <div class="modal-footer">
         <input type="hidden" name="id" id="row-id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>
<!-- modal for add price -->
<div class="modal fade" id="modal-form-plan-price" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-plus"></i> Adding Price to</h4>
        </div>
            <div id="form-plan_price_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan/add-price', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-user_plan_price_save_form', 'files' => true)) !!}

            <div class="modal-body"> 
              <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB bottomMargin" style="padding-right:53px !important;padding-left:0px !important;">
                <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-lg-2 col-md-2 col-sm-6 col-xs-6 control-label font-color">Term</label>
                       <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                         <select name="term" id="row-term" class="form-control disable-view">
                            @foreach($plan_term as $row) 
                                <option value="{{$row->id}}">{{ucfirst($row->name)}}</option>
                            @endforeach
                         </select>
                       </div>
                   </div>
                </div>
                 <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-lg-2 col-md-2 col-sm-6 col-xs-6 control-label font-color">Duration</label>
                       <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                         <input type="number" class="form-control" id="row-duration" name="duration" min="0">
                       </div>
                   </div>
                </div>
               <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-lg-2 col-md-2 col-sm-6 col-xs-6 control-label font-color">Price</label>
                       <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                         <input type="number" class="form-control" id="row-price" name="price" min="0">
                       </div>
                   </div>
                </div>
                <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-lg-2 col-md-2 col-sm-6 col-xs-6 control-label font-color">Points Ex.</label>
                       <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                         <input type="number" class="form-control" id="row-price" name="points_exchange" min="0">
                       </div>
                   </div>
                </div>
                  <div class="col-lg-12 col-md-12 col-xs-12">
                   <div class="form-group">
                    <label for="row-name" class="col-lg-2 col-md-2 col-sm-6 col-xs-6 control-label font-color">Order No</label>
                       <div class="col-lg-10 col-md-10 col-sm-6 col-xs-6">
                         <input type="number" class="form-control" id="row-order_no" name="order_no" min="0">
                       </div>
                   </div>
                </div>
   </div>
   </div>
   </div>
        <div class="modal-footer">
         <input type="hidden" name="plan_id" id="row-plan_id" value="">
         <input type="hidden" name="price_id" id="row-price_id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>

<!-- modal form add discount -->
  <div class="modal fade" id="modal-form-add-discount" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
<!-- Modal -->
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
           <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>

        <div class="modal-header bg-warning">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="user-plan-title"><i class="fa fa-wrench"></i> DISCOUNT OPTIONS</h4>
        </div>
            <div id="form-plan_price_notice"></div>      
 {!! Form::open(array('url' => 'admin/plan/promotion-discount', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-add_plan_discount', 'files' => true)) !!}
            <div class="modal-body"> 
              <div class="row" style="padding:10px;">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topPaddingB noPadding bottomMargin">
                @foreach($promotion_plan_discounts as $row)
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                       <div class="form-group">
                        <label for="row-name" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 control-label font-color">Period</label>
                           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 input-group">
                             <input type="number" min="1" class="form-control" id="row-period" name="period[{{$row->id}}]" value="{{$row->period}}">
                             <span class="input-group-addon" style="padding-left:10px !important"><i class="fa fa-sun-o"></i></span>
                           </div>
                       </div>
                    </div>
                     <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="padding-right:30px;">
                       <div class="form-group">
                        <label for="row-name" class="col-lg-3 col-md-3 col-sm-12 col-xs-12 control-label font-color">Discount</label>
                           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 input-group">
                             <input type="number" class="form-control" id="row-discount" name="discount[{{$row->id}}]" value="{{$row->discount}}">
                             <span class="input-group-addon" style="padding-left:10px !important"><i class="fa fa-percent"></i></span>
                           </div>
                       </div>
                    </div>
                @endforeach
               </div>
            </div>
          </div>
        <div class="modal-footer">
         <input type="hidden" name="plan_id" id="row-plan_id" value="">
         <input type="hidden" name="price_id" id="row-price_id" value="">
          <button type="submit" class="btn btn-default btn-success" id="button-save"> <i class="fa fa-floppy-o"></i> Save</button>
          <button type="button" class="btn btn-default btn-danger" data-dismiss="modal"><i class="fa fa-times-circle-o"></i> Close</button>
        </div>
           {!! Form::close() !!}
   
      </div>
    </div>
  </div>
  <!-- modal form remove discount -->
  <div class="modal fade" id="modal-plan-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'admin/plan-features/remove', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-plan-delete_form', 'files' => true)) !!}
      <div class="modal-header modal-delete">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Delete Ads</h4>
      </div>
      <div class="modal-body">
        <div id="form-plan-notice"></div>
        <div class="form-group">
        <div class="col-lg-12">
          Do you want to delete? 
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="plan_id" id="row-plan_id" value="">
        <button type="submit" class="btn btn-submit btn-danger borderZero"><i class="fa fa-trash"></i> Yes</button>
        <button type="button" class="btn btn-info borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
