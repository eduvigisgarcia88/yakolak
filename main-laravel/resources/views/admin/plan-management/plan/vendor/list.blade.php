@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  $loading_expand = false;
  $expand_id = 0;

  // refresh the list
  function refresh() {
      var table = $("#rows");
      $loading_expand = false;
      $expand_id = 0;
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
           
            body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
             '<td><button class="btn btn-xs btn-table btn-expand" '+(row.plan_name == 'Free' ? 'disabled':'')+' style="background-color:transparent;color:#337ab7;"><i class="fa fa-plus"></i></button><small>'+ row.plan_name +'</small></td>'+
             '<td><small>'+ row.type_name +'</small></td>'+
             '<td><small>'+ (row.theme_name == null ? 'N/A':row.theme_name) +'</small></td>'+
             '<td><small><center>'+ row.ads +'</small></center></td>'+
             '<td><small><center>'+ row.auction +'</small</center></td>'+
             '<td class="rightalign">'+  
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add" '+(row.plan_name == 'Free' ? 'disabled':'')+'><i class="fa fa-plus"></i></button>' +
                 '</td></tr>';
        });
        
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
  function search(){
    refresh();
  }

      $("#planManagement").on("click", "#row-sort", function() {
                $('.loading-pane').removeClass('hide');
                $page = $("#row-page").val();
                $order = $("#row-order").val();
                $sort =  $(this).data('sort');
                $search = $("#row-search").val();
                $status = $("#row-filter_status").val();
                $usertype_id = $("#row-filter_usertype_id").val();
                // $country = $("#row-filter_country").val();
                $per = $("#row-per").val();
                $("#planManagement").find("i").removeClass('fa-caret-up');
                $("#planManagement").find("i").removeClass('fa-caret-down');
                // $(this).find("i").addClass('fa-caret-down');
                var loading = $(".loading-pane");
                var table = $("#rows");
                 if($order == "asc"){
                    $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
                 }else{
                    $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
                 } 
                
            $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
              
              var body = "";
              // clear table
              table.html("");
               $.each(response.rows.data, function(index, row) {
                body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
                   '<td><button class="btn btn-xs btn-table btn-expand" '+(row.plan_name == 'Free' ? 'disabled':'')+' style="background-color:transparent;color:#337ab7;"><i class="fa fa-plus"></i></button><small>'+ row.plan_name +'</small></td>'+
                   '<td><small>'+ row.type_name +'</small></td>'+
                   '<td><small>'+ (row.theme_name == null ? 'N/A':row.theme_name) +'</small></td>'+
                   '<td><small><center>'+ row.ads +'</small></center></td>'+
                   '<td><small><center>'+ row.auction +'</small></center></td>'+
                   '<td class="rightalign">'+  
                       '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                       '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add" '+(row.plan_name == 'Free' ? 'disabled':'')+'><i class="fa fa-plus"></i></button>' +
                       '</td></tr>';
              });
                table.html(body);
                $('#row-pages').html(response.pages);
                loading.addClass("hide");
               
                if(response.order == "asc"){
                    $("#row-order").val("desc");
                  
                }else{
                    $('#row-order').val("asc");
                }
             
            }, 'json');
      });
      $(".content-wrapper").on('click', '.pagination a', function (event) {
            event.preventDefault();
            if ( $(this).attr('href') != '#' ) {

            $("html, body").animate({ scrollTop: 0 }, "fast");
            $("#row-page").val($(this).html());

            $('.loading-pane').removeClass('hide');
            $page = $("#row-page").val();
            $order = $("#row-order").val();
            $sort = $("#row-sort").val();
            $search = $("#row-search").val();
            $status = $("#row-filter_status").val();
            $usertype_id = $("#row-filter_usertype_id").val();
            $per = $("#row-per").val();

            var loading = $(".loading-pane");
            var table = $("#rows");
            $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
              // clear
              table.html("");
              
              var body = "";
              $.each(response.rows.data, function(index, row) {
                body += '<tr data-id="' + row.id + '" data-plan_name="' + row.plan_name + '" data-usertype_id="' + row.usertype_id + '" class="'+ (row.status == 2  ? 'disabledColor' : '')+'">'+ 
             '<td><button class="btn btn-xs btn-table btn-expand" '+(row.plan_name == 'Free' ? 'disabled':'')+' style="background-color:transparent;color:#337ab7;"><i class="fa fa-plus"></i></button><small>'+ row.plan_name +'</small></td>'+
             '<td><small>'+ row.type_name +'</small></td>'+
             '<td><small>'+ (row.theme_name == null ? 'N/A':row.theme_name) +'</small></td>'+
             '<td><small><center>'+ row.ads +'</small></center></td>'+
             '<td><small><center>'+ row.auction +'</small></center></td>'+
             '<td class="rightalign">'+  
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add" '+(row.plan_name == 'Free' ? 'disabled':'')+'><i class="fa fa-plus"></i></button>' +
                 '</td></tr>';
              });
              
              $("#modal-add-country").find('form').trigger("reset");
              table.html(body);

              $('#row-pages').html(response.pages);
              loading.addClass("hide");

            }, 'json');

            }
    });
      $(".content-wrapper").on("click", ".btn-add", function() {
            $("#modal-form-add-plan").find('form').trigger("reset");
            $(".modal-title").html('<i class="fa fa-plus"></i><strong>Add User Plan</strong>');
            $("#modal-form-add-plan").find("form").find(".disable-view").removeAttr("disabled","disabled");
            $("#button-save").removeClass("hide");
            $("#modal-form-add-plan").find("#row-id").val("");  
            $("#modal-form-add-plan").modal('show');
    });


    $(".content-wrapper").on("click", ".btn-edit", function() {
      var id = $(this).parent().parent().data('id');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-id").val("");
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.plan_name + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }
              });
                      $("#modal-form-add-plan").find("form").find(".disable-view").removeAttr("disabled","disabled");
                      $("#button-save").removeClass("hide");

              // show form
              $("#modal-form-add-plan").modal('show');
          } else {
              status(false, response.error, 'alert-danger');
          }
          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });
   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form-add").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Viewing <strong>' + response.plan_name + '</strong>');
                  
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                      $("#modal-form-add-plan").find("form").find(".disable-view").attr("disabled","disabled");
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-form-add-plan").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

//DONT DELETE
   $("#usertype_id").on("change", function() { 
        var id = $("#usertype_id").val();
        var plan_id = $("#row-id").val();
        $_token = "{{ csrf_token() }}";
        var usertype = $("#user-type-pane");
        usertype.html("");
        if ($("#usertype_id").val()) {
          $.post("{{ url('user-types') }}", { id: id, plan_id: plan_id, _token: $_token }, function(response) {
            usertype.html(response);
          }, 'json');
       }
  });  


  $(".content-wrapper").on("click", ".btn-delete", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Plan', 'Are you sure you want to delete ' + plan_name + '</strong>?', "{{ url('admin/plan/delete') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      console.log(plan_name)
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Plan', 'Are you sure you want to disable ' + plan_name +'</strong>?', "{{ url('admin/plan/disable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      console.log(plan_name);
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Plan', 'Are you sure you want to enable ' + plan_name +'</strong>?', "{{ url('admin/plan/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-price-delete", function() {
       var id = $(this).parent().parent().data('price_id');
       var price = $(this).parent().parent().data('price');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Delete Price', 'Are you sure you want to delete ' + price + '</strong>?', "{{ url('admin/plan/price/delete') }}",id);
  }); 

    $(".content-wrapper").on("click", ".btn-add-price", function() {

      var plan_id = $(this).parent().parent().data('id');
      var plan_name = $(this).parent().parent().data('plan_name');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
       $("#row-plan_id").val(plan_id);
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");
      $.post("{{ url('admin/plan/price/manipulate') }}",{ plan_id: plan_id,plan_name: plan_name, _token: $_token}, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }
          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-plus"></i> Adding Price to <strong>' + plan_name + '</strong>');
              // output form data
                      $("#row-usertype_id").removeAttr('disabled','disabled');
                      $("#button-save").removeClass("hide");

              // show form
          } else {
              status(false, response.error, 'alert-danger');
          }
        $("#modal-form-plan-price").find(".btn-change").addClass("hide");
        $("#modal-form-plan-price").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled"); 
        $("#modal-form-plan-price").find('form').trigger("reset");
        $("#modal-form-plan-price").modal('show');

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });



    $(".content-wrapper").on("click", ".btn-expand", function() {
       $(".btn-expand").html('<i class="fa fa-minus"></i>');
       $(".btn-expand").html('<i class="fa fa-plus"></i>');
      $type_id = 0;
       var plan_id = $(this).parent().parent().data('id');
       console.log(plan_id);
      if ($loading_expand){
        return;
      }
      $tr = $(this).parent().parent();
      $id = '1';
      $_token = '{{ csrf_token() }}';
      $selected = '.row-expand-' + $expand_id;
        $($selected).slideUp(function () {
          $($selected).parent().parent().remove();
        });

      if ($id == $expand_id) {
        $expand_id = 0;
        return
      }
      $expand_id = $id;
      $button = $(this);
      $button.html('<i class="fa fa-spinner fa-spin"></i>');
      $loading_expand = true;

      $.post("{{ url('admin/plan/get-plan-price') }}", { id: $id, plan_id: plan_id, _token: $_token }, function(response) {

        if (!response.error) {
          $tr.after('<tr><td colspan="7" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="row-expand-1" style="display:none;">' + response + '</div></td></tr>');

          $('.row-expand-' + $expand_id).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading_expand = false;
        } else {
          status(false, response.error, 'alert-danger');
        }
      });
    });

 $(".btn-promotion-discount").on("click", function() { 

      $("#modal-form-add-discount").modal("show");
 });

 $(".content-wrapper").on("click", ".btn-price-edit", function() {
      var price_id = $(this).parent().parent().data('price_id');
      var plan_id = $(this).parent().parent().data('plan_id');
      var price = $(this).parent().parent().data('price');
      var btn = $(this);
      var url = "{{ url('/') }}";
      $("#loan-header").removeAttr('class').attr('class', 'modal-header modal-default');
      $("#row-price_id").val(price_id);
      $("#row-plan_id").val(plan_id);
      btn.find('i').addClass('fa-spinner fa-spin').removeClass("fa-pencil");

      $.post("{{ url('admin/plan/price/manipulate') }}", { price_id: price_id, plan_id: plan_id, _token: $_token }, function(response) {
          
          if (response.unauthorized) {
            window.location.href = '{{ url("login")}}';
          }

          if(!response.error) {
          // set form title
              $(".user-plan-title").html('<i class="fa fa-pencil"></i> Editing <strong>' + response.price + '</strong>');

              // output form data
              $.each(response, function(index, value) {
                  var field = $("#row-" + index);

                  if(field.length > 0) {
                        field.val(value);
                  }

              });
                $("#row-usertype_id").removeAttr('disabled','disabled');
                $("#row-plan_name").removeAttr('disabled','disabled');
                $("#button-save").removeClass("hide");
                  // show form
                $("#modal-form-plan-price").modal('show');
              
          } else {
              status(false, response.error, 'alert-danger');
          }

          btn.find('i').removeClass('fa-spinner fa-spin').addClass("fa-pencil");
      }, 'json');
    });
  $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
  });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-usd"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" disabled><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>  -->  
        <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-promotion-discount"><i class="fa fa-wrench"></i> Discount Options</button> 
       
      </div>
      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 filters">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
             <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 filters">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Usertype</span>
                      <select class="form-control borderZero" id="row-filter_usertype_id" onchange="refresh()">
                                  <option value="">All</option>
                                @foreach($usertype as $row)
                                  <option value="{{$row->id}}">{{$row->type_name}}</option>
                                @endforeach
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed backend-generic-table" id="planManagement">
          <thead>
            <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_plan_types.plan_name" id="row-sort"><i class="fa"></i> Plan</div></th>
            <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="usertypes.type_name" id="row-sort"><i class="fa"></i> Usertype</div></th>
            <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="theme_plans.name" id="row-sort"><i class="fa"></i> Theme</div></th>
            <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_plan_types.ads" id="row-sort"><i class="fa"></i> Ads</div></center></th>
            <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="user_plan_types.auction" id="row-sort"><i class="fa"></i> Bids</div></center></th>
            <th class="text-right">Tools</th>
          </thead>
            <tbody id="rows"> 
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-plan_name="{{ $row->plan_name }}" data-usertype_id="{{ $row->usertype_id }}" class="{{($row->status == 2 ? 'disabledColor' : '') }}">
              <td><button class="btn btn-xs btn-table btn-expand" style="background-color:transparent;color:#337ab7;" {{$row->plan_name == 'Free' ? 'disabled':''}}><i class="fa fa-plus"></i></button><small>{{ $row->plan_name }}</small></td>
              <td><small>{{ $row->type_name }}</small></td>
              <td><small>{{$row->theme_name == null ? 'N/A': $row->theme_name}}</small></td>
              <td><small><center>{{ $row->ads }}</small></center></td>
              <td><small><center>{{ $row->auction }}</small></center></td>
              <td class="rightalign">
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" title="Edit"><i class="fa fa-pencil"></i></button>
                 <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-price" title="Add" {{$row->plan_name =="Free" ? 'disabled':''}}><i class="fa fa-plus"></i></button>
              </td>
           </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.plan-management.plan.form')