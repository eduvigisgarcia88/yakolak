@extends('layout.master')
@section('scripts')
<script>
 $_token = '{{ csrf_token() }}';

 function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $("#row-page").val(1);
      $("#row-order").val('');
      $("#row-sort").val('');
      $("#row-search").val('');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { country:$country, page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";

        // clear datatable cache
        // $('#my-table').dataTable().fnClearTable();

        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" style="'+(row.status == 2 ? 'background-color:#D3D3D3;':'')+'">' + 
              '<td class="hide">' + row.status + '</td>' +
              // '<td><img src="{{ url('uploads') }}/' + row.photo + '?' + new Date().getTime() + '" height="30" width="30"/></td>' +
              '<td>'+ row.name +'</td>';
           	  body += '<td class="text-right">'+(row.status == 1 ? '<input data-id = "'+row.id+'" type="checkbox" name="my-checkbox-'+row.id+'" id="my-checkbox-'+row.id+'" checked>':'<input data-id = "'+row.id+'" type="checkbox" id="my-checkbox-'+row.id+'" name="my-checkbox">')+'</td>';
              // body +='<td class="rightalign">'+
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              //    // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' 
              // '</td>';
              body += '</tr>';      
        });
        
        $("#modal-promotion").find('form').trigger("reset");
        table.html(body);
        $.each(response.rows.data, function(index, row) {
          $("#my-checkbox-"+row.id).bootstrapSwitch();
           $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-'+row.id, function(event, state) {
            var id = $(this).data('id');
            var status = state;
            var stat = 0;
            if(status == true){
                stat = 1;
            }
            if(status == false){
                stat = 2;
            }
             $.post("{{ url('admin/city/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
              refresh();
            }, 'json');
         
          });
        });



        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }

 @foreach($rows as $row)
  $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
 
  $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-{{$row->id}}', function(event, state) {
      var id = $(this).data('id');
      var status = state;
      var stat = 0;
      if(status == true){
          stat = 1;
      }
      if(status == false){
          stat = 2;
      }
       $.post("{{ url('admin/city/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
        refresh();
      }, 'json');
   
    });
   @endforeach
  
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
       <!--  <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button> 
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>     -->
      </div>
      <div class="col-lg-12 col-sm-12 col-xs-12 pull-right">
        <div class="col-lg-6 col-sm-6 col-xs-6">
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-6">
         <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Country</label>
                  <div class="col-sm-9 col-xs-7">
                   <select class="form-control" id="row-filter_country" onchange="refresh()">
                            <option class="hide">Select a Country</option>
                          @foreach($country as $row)
                            <option value="{{$row->id}}">{{$row->countryName}}</option>
                          @endforeach
                    </select>
        </div>
      </div>
    </div>
    </div>
    <hr></hr>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table">
            <thead>
                <tr>
                  <th><small>Name</small></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
            <tr>
              <td class="text-center">Choose a Country first</td>
            </tr>
           @foreach($rows as $row)
            <tr data-id="{{$row->id}}" style="{{ ($row->status == 2 ? 'background-color:#D3D3D3':'') }}">
                <td>{{$row->name}}</td>
               	<td class="text-right">
             	  @if($row->status == 1)
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}" checked>
                 @else
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}"> 
                @endif
                </td>
               <!--  <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete-ads" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                </td>  -->
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.country-management.city-management.form')