@extends('layout.master')
@section('scripts')
<script>
 $_token = '{{ csrf_token() }}';
 $loading = false;
 $selectedID =0;
 function refresh() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        console.log(response);
        var body = "";
        $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.countryName +'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
              '<td><small>'+ (row.name == null ? 'Default': row.name) +'</small></td>'+
              '<td><small>'+ row.country_theme +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';

          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>'+'</td>'+
              '</tr>';      
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
     function maxAds() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.countryName +'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
              '<td><small>'+ (row.name == null ? 'Default': row.name) +'</small></td>'+
              '<td><small>'+ row.country_theme +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>'+'</td>'+
              '</tr>';      
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
     function search() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.countryName +'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
              '<td><small>'+ (row.name == null ? 'Default': row.name) +'</small></td>'+
              '<td><small>'+ row.country_theme +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>'+'</td>'+
              '</tr>';      
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
    $(".content-wrapper").on("click", ".btn-expand", function() {
      $button = $(this);
      $button.prop('disabled', true);
      $(".btn-expand").html('<i class="fa fa-minus"></i>');
      $(".btn-expand").html('<i class="fa fa-plus"></i>');
    
        if ($loading){
          return;
        }

        $tr = $(this).closest('tr');
        $id = $(this).data('id');
        //$code = $(this).data('code');
        //$_token = '{{ csrf_token() }}';

        $selected = '.row-' + $selectedID;
          $($selected).slideUp(function () {
            $button.prop('disabled', false);
            $($selected).parent().parent().remove();
          });

        if ($id == $selectedID) {
          $selectedID = 0;
          return;
        }
        $selectedID = $id;
        
        $button.html('<i class="fa fa-spinner fa-spin"></i>');
        $loading = true;

        $.post("{{ url('admin/dashboard/country/get-cities') }}", { id: $id, _token: $_token }, function(response) {
          $tr.after('<tr><td colspan="10" style="padding: 0px 10px 0px 10px !important;background-color:#f7f7f7;"><div class="expand-table row-' + $selectedID + '" style="display:none;">' + response + '</div></td></tr>');
        $('.row-' + $selectedID).slideDown();
          $button.html('<i class="fa fa-minus"></i>');
          $loading = false;
        }).complete(function(){ $button.prop('disabled', false); });

 });

 $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
         body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.countryName +'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
              '<td><small>'+ (row.name == null ? 'Default': row.name) +'</small></td>'+
              '<td><small>'+ row.country_theme +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>'+'</td>'+
              '</tr>';      
        });
        
        $("#modal-add-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

      }
    });
  $("#country-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#country-table").find("i").removeClass('fa-caret-up');
          $("#country-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
           if($order == "asc"){
              $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
           }else{
              $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
           } 
          
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '" class="'+(row.status == 2 ? 'disabledColor':'')+'">' + 
              '<td><a class="btn btn-xs btn-table btn-expand" data-id="'+row.id+'"><i class="fa fa-plus"></i></a><small>'+ (row.status == 1 ? 'Enabled':'Disabled' )+'</small></td>'+
              '<td><small>'+ row.countryName +'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
              '<td><small>'+ (row.name == null ? 'Default': row.name) +'</small></td>'+
              '<td><small>'+ row.country_theme +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>';
          body +='<td class="rightalign">'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
                // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
                (row.status==1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
                '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>'+'</td>'+
              '</tr>';      
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
            
          }else{
              $('#row-order').val("asc");
          }
       
      }, 'json');
  

    });
    $(".content-wrapper").on("click", ".btn-edit-city", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-city").find(".alert").remove();
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");

        $_token = "{{ csrf_token() }}";
        $("#row-photo").removeClass("hide");
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/country/city/edit') }}", { id: id, _token: $_token }, function(response) {
                console.log(response);
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit City ['+response.name+']</strong>');
                      // output form data
                      $.each(response, function(index, value) {
                        var field = $("#row-" + index);
                        $("#modal-add-city").find(".hide-view").removeClass("hide");
                        $("#modal-add-city").find(".disable-view").removeAttr("disabled",'disabled');
                          // field exists, therefore populate   
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                      // show form
                      $("#modal-add-city").find("#row-status").val(response.status);
                      $("#modal-add-city").find("#row-id").val(response.id);
                      $("#modal-add-city").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
   });
    $(".content-wrapper").on("click", ".btn-view-city", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        $("#modal-add-city").find(".alert").remove();
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        $("#row-photo").removeClass("hide");
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/country/city/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View City ['+response.name+']</strong>');
                      console.log(response);
                      // output form data
                      $.each(response, function(index, value) {
                        var field = $("#row-" + index);
                        $("#modal-add-city").find(".hide-view").addClass("hide");
                        $("#modal-add-city").find(".disable-view").attr("disabled",'disabled');
                          // field exists, therefore populate   
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                      // show form
                      $("#modal-add-city").find("#row-status").val(response.status);
                      $("#modal-add-city").find("#row-id").val(response.id);
                      $("#modal-add-city").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
   });
   $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        $("#row-photo").removeClass("hide");
        // User Id Disabled
        $("#modal-add-country").find(".alert").remove();
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/country/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Country ['+response.countryName+']</strong>');
                      console.log(response);
                      // output form data
                      $.each(response, function(index, value) {
                        var field = $("#row-" + index);
                        $("#modal-add-country").find(".hide-view").removeClass("hide");
                        $("#modal-add-country").find(".disable-view").removeAttr("disabled",'disabled');
                          // field exists, therefore populate   
                          if(field.length > 0) {
                              field.val(value);
                          }
                          if(index == "theme"){
                             $("#modal-add-country").find('form').find("#row-country_theme").val(response.theme);
                          }
                      });
                      // show form
                      $("#modal-add-country").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
    });
    $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-country").find(".alert").remove();
        $("#modal-form").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        $("#row-photo").removeClass("hide");
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/country/edit') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Country ['+response.countryName+']</strong>');
                      console.log(response);
                      // output form data
                      $.each(response, function(index, value) {
                        var field = $("#row-" + index);
                        $("#modal-add-country").find(".hide-view").addClass("hide");
                        $("#modal-add-country").find(".disable-view").attr("disabled",'disabled');
                          // field exists, therefore populate   
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                      // show form
                      $("#modal-add-country").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
   });

  $(".content-wrapper").on("click", ".btn-add", function() {
    $("#modal-add-country").modal("show");
    $("#modal-add-country").find(".alert").remove();
    $("#modal-add-country").find("form").trigger('reset');
    $("#modal-add-country").find(".hide-view").removeClass("hide");
    $("#modal-add-country").find(".disable-view").removeAttr("disabled",'disabled');
  });

  // $("#modal-add-country").on("change", "#row-localization", function() { 
  //       var id = $(this).val();
  //       $.post("{{ url('admin/get-theme') }}", { id: id, _token: $_token }, function(response) {
  //                 if(!response.error) {
  //                     $.each(response, function(index, value) {
  //                         if(response.theme == 1){
  //                            $("#row-theme").val("Default");
  //                         }
                         
  //                     });
  //                 } else {
  //                     status(false, response.error, 'alert-danger');
  //                 }
  //             }, 'json');
  // })
   $(".content-wrapper").on("click", ".btn-add-city", function() {
    var id = $(this).parent().parent().data('id');
    $("#modal-add-city").find("form").find('#row-country_id').val(id).attr("selected", "selected");
    $("#modal-add-city").modal("show");
    // $("#modal-add-city").find("form").trigger('reset');
    // $("#modal-add-city").find(".alert").remove();
    // $("#modal-add-city").find(".hide-view").removeClass("hide");
    // $("#modal-add-city").find(".disable-view").removeAttr("disabled",'disabled');
  });
  $(".content-wrapper").on("click", ".btn-delete", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Remove Country', 'Are you sure you want to delete this country</strong>?', "{{ url('admin/country/delete') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Enable Country', 'Are you sure you want to enable this country</strong>?', "{{ url('admin/country/enable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Disable Country', 'Are you sure you want to disable this country</strong>?', "{{ url('admin/country/disable') }}",id);
  });
   $(".content-wrapper").on("click", ".btn-delete-city", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Delete City', 'Are you sure you want to delete this city</strong>?', "{{ url('admin/country/city/delete') }}",id);
  });
   $(".content-wrapper").on("click", ".btn-enable-city", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Enable City', 'Are you sure you want to enable this city</strong>?', "{{ url('admin/country/city/enable') }}",id);
  });
    $(".content-wrapper").on("click", ".btn-disable-city", function() {
   var id = $(this).parent().parent().data('id');
   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
    dialog('Disable City', 'Are you sure you want to disable this city</strong>?', "{{ url('admin/country/city/disable') }}",id);
  });
  
  $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
 }); 
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-globe"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-globe"></i> Add</button>    
      </div>
    </div>
     <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 noPaddingLeft">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 noPaddingRight">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                      <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Enabled</option>
                                <option value="2">Disabled</option>
                      </select>
                  </div>
             </div>
        </div>
    </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="country-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.status" id="row-sort"><i class="fa"></i> Status</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.countryName" id="row-sort"><i class="fa"></i> Name</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.countryCode" id="row-sort"><i class="fa"></i> Code</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="localization.name" id="row-sort"><i class="fa"></i> Localization</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="theme_localization.name" id="row-sort"><i class="fa"></i> Theme</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created Date</div></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($rows as $row)
            <tr data-id="{{$row->id}}" class="{{ ($row->status == 2 ? 'disabledColor':'') }}">
                <td><a class="btn btn-xs btn-table btn-expand" data-id="{{ $row->id }}"><i class="fa fa-plus"></i></a><small>{{$row->status == 1 ? 'Enabled':''}}{{$row->status == 2 ? 'Disabled':''}}</small></td>
                <td><small>{{$row->countryName}}</small></td>
                <td><small>{{$row->countryCode}}</small></td>
                <td><small>{{$row->name == null ? 'Default':$row->name}}</small></td>
                <td><small>{{$row->country_theme }}</small></td>
                <td><small>{{$row->created_at}}</small></td>
                <td>
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                   <!--  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button> -->
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>
                    @if($row->status == 1)
                      <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>
                    @else
                      <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>
                    @endif
                    <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-add-city"><i class="fa fa-building"></i></button>
                </td> 
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row paddingFooter marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.country-management.form')