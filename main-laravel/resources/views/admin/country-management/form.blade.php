<!-- modal add country -->
  <div class="modal fade" id="modal-add-country" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/country/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-country_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add Country</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="country_name" class="form-control disable-view" id="row-countryName" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Code</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="text" name="country_code" class="form-control disable-view" id="row-countryCode" maxlength="2">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Currency</label>
                <div class="col-sm-9 col-xs-7">
                    <input type="text" name="country_currency" class="form-control disable-view" id="row-currency" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Localization</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control disable-view" id="row-localization" name="country_localization">
                      <option class="hide">Select</option>
                    @foreach($localization as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Theme</label>
                <div class="col-sm-9 col-xs-7">
                   <select class="form-control disable-view" id="row-country_theme" name="country_theme">
                      <option class="hide">Select</option>
                      @foreach($country_themes as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                      @endforeach
                  </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Status</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control disable-view" id="row-status" name="status">
                        <option value="1">Enabled</option>
                        <option value='2'>Disabled</option>
                  </select>
                </div>
                </div>
              </div>
             

              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>

        <!-- modal add city -->
  <div class="modal fade" id="modal-add-city" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/country/city/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-city_form')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> Add City</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Country</label>
                <div class="col-sm-9 col-xs-7">
                     <select class="form-control disable-view" id="row-country_id" name="country">
                          <option class="hide">Select</option>
                        @foreach($countries as $row)  
                          <option value="{{$row->id}}">{{$row->countryName}}</option>
                        @endforeach
                     </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="city_name" class="form-control disable-view" id="row-name" maxlength="30">
                </div>
                </div>
              <!--   <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Currency</label>
                <div class="col-sm-9 col-xs-7">
                    <input type="name" name="country_currency" class="form-control" id="row-currency" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Localization</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control" id="row-localization" name="country_localization">
                    @foreach($localization as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
                </div> -->
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Status</label>
                <div class="col-sm-9 col-xs-7">
                  <select class="form-control disable-view" id="row-status" name="status">
                        <option value="1">Enabled</option>
                        <option value='2'>Disabled</option>
                  </select>
                </div>
                </div>
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  
  