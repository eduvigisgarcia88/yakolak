<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Latest compiled and minified CSS -->
    <title>{{ $title }}</title>
    <!-- Bootstrap -->
    {!! HTML::style('/css/bootstrap.min.css') !!}

    <!-- Font Awesome -->
    {!! HTML::style('/plugins/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {!! HTML::style('/css/login-css.css') !!}
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="bg-blue">
    <div class="container v-centered">
      <div class="row">
        <div class="centered">
            <div id="form-login" class="panel panel-default">
                {!! Form::open(array('url' => 'admin/login', 'role' => 'form')) !!}
                @if(Session::has('notice'))
                <div class="alert alert-dismissable alert-{{ Session::get('notice.type') }}">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <i class="fa fa-fw fa-exclamation-triangle"></i>{{ Session::get('notice.msg') }}
                </div>
                @endif
                <div class="panel-logo">
                    <img src="{{ url()}}/img/yakolaklogo.png" style="height: 50px; padding-bottom: 10px;">
                </div>
                <div class="panel-heading">
                    <div class="form-group">
                        <label class="control-label">Login</label>
                        <input type="text" class="form-control" name="email" id="email" placeholder="Username">
                      @if(Session::has('notice'))
                       <sup class="sup-errors redText">{!! Session::get('notice.msg.username_required') !!}</sup>
                      @endif
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                     @if(Session::has('notice'))
                       <sup class="sup-errors redText">{!! Session::get('notice.msg.password_required') !!}</sup>
                     @endif
                    </div>
                    <div class="form-group text-center">
                     {!! app('captcha')->display(); !!}
<!--                       <div class="g-recaptcha" data-theme="light" data-sitekey="{{Session::get('siteKey')}}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div> -->
                      @if(Session::has('notice'))
                       <sup class="sup-errors redText">{!! Session::get('notice.msg.captcha_required') !!}</sup>
                      @endif
                    </div>
                    <div class="form-group auth-group">
                        <div class="checkbox">
{{--                             <label><span id="rememberme" style="font-size:15px;"><input style="height:15px; width:15px;" type="checkbox" name="remember">&nbsp;Remember me</span></label> --}}
                            <input type="checkbox" style="width:15px; height:15px; margin-top:4px;" id="remember_me" name="remember">
                            <label for="remember_me" style="font-size:15px; padding-left:20px;">Remember me</label>
                        </div>
                        <div class="text-center"><input class="hide" name="honeypot" value=""><button type="submit" class="borderZero btn btn-warning btn-responsive btn-login">Login</button></div>
                    </div>
                </div>
                <div class="panel-captcha">

                </div>
                <div class="panel-forgot">
                    <div class="panel-body padding-none">
                        <label><small>Forgot Password?</small></label><br>
                        <span><small>Contact admin or retrieve password on the link provided. <a class="a-forgot" href="#">Forgot Password</a></small></span>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
      </div>
  </div>
    </div>

    <!-- jQuery -->
    {!! HTML::script('/js/jquery.js') !!}

    <!-- Bootstrap -->
    {!! HTML::script('/js/bootstrap.min.js') !!}
    
  </body>
</html>