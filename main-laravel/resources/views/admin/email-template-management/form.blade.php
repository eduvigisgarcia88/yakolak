<!-- modal add user -->
  <div class="modal fade" id="modal-email-template" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/email-template/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD NEWSLETTER</h4>
              </div>
              <div class="modal-body">
                <div style="max-width:600px; overflow:hidden;" id="email-design">
                  
                </div>
<!--                <div id="form-notice"></div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Module Trigger</label>
                <div class="col-sm-9 col-xs-7">
                    <select class="form-control" id="row-module-trigger" name="module_trigger">
                          <option class="hide">Select</option>
                          @foreach($module_triggers as $row)
                              <option value="{{$row->id}}">{{$row->trigger_name}}</option>
                          @endforeach
                    </select>
                </div>
                </div>
                <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Name</label>
                <div class="col-sm-9 col-xs-7">
                   <input type="name" name="newsletter_name" class="form-control" id="row-newsletter_name" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Title</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="title" class="form-control" id="row-title" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Subject</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="subject" class="form-control" id="row-subject" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-description" class="col-sm-3 col-xs-5 control-label font-color">Body</label>
                <div class="col-sm-9 col-xs-7">
                    <textarea name="body" class="form-control summernote borderZero" id="row-body"></textarea>
                </div>
                </div> -->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  