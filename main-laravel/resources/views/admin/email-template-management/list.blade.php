@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';

  // refresh the list
  function refresh() {
      $("#row-page").val(1);
      // $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? "disabledColor" : '')+'">' + 
              '<td class="hide">' + row.status + '</small></td>' +
              '<td><small>'+ row.newsletter_name + '</small></td>' +
              '<td><small>'+ row.title +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>'+
              '<td class="rightalign">'+
               (row.newsletter_status == 1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              '<a href="{{url("admin/email-template")}}'+'/'+row.id + '/edit"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>'+
               '</td>'+
              '</tr>';
         
        });
        table.html(body);

      // update links
        $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $.each(response.rows.data, function(index, row) {
            $("#my-checkbox-"+row.id).bootstrapSwitch();
        });
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
  function maxAds() {
      $("#row-page").val(1);
      // $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
        $.each(response.rows.data, function(index, row) {
           console.log(response);
             body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? "disabledColor" : '')+'">' + 
              '<td class="hide">' + row.status + '</small></td>' +
              '<td><small>'+ row.newsletter_name + '</small></td>' +
              '<td><small>'+ row.title +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>'+
              '<td class="rightalign">'+
               (row.newsletter_status == 1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              '<a href="{{url("admin/email-template")}}'+'/'+row.id + '/edit"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>'+
               '</td>'+
              '</tr>';
         
        });
        table.html(body);

      // update links
         $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $.each(response.rows.data, function(index, row) {
            $("#my-checkbox-"+row.id).bootstrapSwitch();
        });
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
   function search() {
      $("#row-page").val(1);
      // $(".th-sort").find('i').removeAttr('class');

      $per = $("#row-per").val();
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      var url = "{{ url('/') }}";
      loading.removeClass('hide');
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
        $.each(response.rows.data, function(index, row) {
           console.log(response);
            body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? "disabledColor" : '')+'">' + 
              '<td class="hide">' + row.status + '</small></td>' +
              '<td><small>'+ row.newsletter_name + '</small></td>' +
              '<td><small>'+ row.title +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>'+
              '<td class="rightalign">'+
               (row.newsletter_status == 1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              '<a href="{{url("admin/email-template")}}'+'/'+row.id + '/edit"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>'+
               '</td>'+
              '</tr>';
         
        });
        table.html(body);

      // update links
         $("#modal-form").find('form').trigger("reset");
        table.html(body);
        $.each(response.rows.data, function(index, row) {
            $("#my-checkbox-"+row.id).bootstrapSwitch();
        });
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
  }
   $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status : $status,country:$country, per: $per, _token: $_token }, function(response) {
      table.html("");
      console.log(response);
      var body = "";

        $.each(response.rows.data, function(index, row) {
           
            body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? "disabledColor" : '')+'">' + 
              '<td class="hide">' + row.status + '</small></td>' +
              '<td><small>'+ row.newsletter_name + '</small></td>' +
              '<td><small>'+ row.title +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>'+
              '<td class="rightalign">'+
               (row.newsletter_status == 1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              '<a href="{{url("admin/email-template")}}'+'/'+row.id + '/edit"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>'+
               '</td>'+
              '</tr>';
         
        });
       $.each(response.rows.data, function(index, row) {
         $("#my-checkbox-"+row.id).bootstrapSwitch();
       });
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
      }
    });
    $("#email-template-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#email-template-table").find("i").removeClass('fa-caret-up');
          $("#email-template-table").find("i").removeClass('fa-caret-down');
          $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");

        
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
        $.each(response.rows.data, function(index, row) {
           console.log(response);
             body += '<tr data-id="' + row.id + '" style="'+(row.popup_status == 2 ? 'opacity:0.5;':'')+'" class="'+ (row.newsletter_status == 2  ? "disabledColor" : '')+'">' + 
              '<td class="hide">' + row.status + '</small></td>' +
              '<td><small>'+ row.newsletter_name + '</small></td>' +
              '<td><small>'+ row.title +'</small></td>'+
              '<td><small>'+ row.created_at +'</small></td>'+
              '<td class="rightalign">'+
               (row.newsletter_status == 1 ? '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>':'<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>')+
              '<button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
              '<a href="{{url("admin/email-template")}}'+'/'+row.id + '/edit"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button></a>'+
               '</td>'+
              '</tr>';
         
        });
          $.each(response.rows.data, function(index, row) {
            $("#my-checkbox-"+row.id).bootstrapSwitch();
          });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
              $(this).find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          }else{
              $('#row-order').val("asc");
              $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          }

            if($order == "asc"){
            if($sort != "created_at"){
              $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down');
            }else{
              $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
            }
         }else{
            if($sort != "created_at"){
                $(this).find("i").addClass('fa-caret-down').removeClass('fa-caret-up');
             }else{
                $(this).find("i").addClass('fa-caret-up').removeClass('fa-caret-down'); 
            }
          }
       
      }, 'json');
  

    });
   $(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var body = $(this).data('body');
     
        console.log(body);
        var btn = $(this);
        // reset all form fields
        
        $(".uploader-pane").addClass("hide");
        $("#modal-form-plan-rate").find('form').trigger("refresh");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // Remove selected photo input.
        $('.change').find('.fileinput-remove').click();
        $(".change").addClass('hide');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/email-template/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
              $(".modal-title").html('<i class="fa fa-pencil"></i> Viewing <strong>' + response.newsletter_name + '</strong>');
                  
                      // output form data
                      $.each(response, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          
                          if(field.length > 0) {
                              field.val(value);
                            
                          }
                      });
                    
                       $('#email-design').html(response.body);                    
                       $("#row-newsletter_name").attr('disabled','disabled');
                       $("#row-title").attr('disabled','disabled');
                       $("#row-created_at").attr('disabled','disabled');
                       $("#row-discount").attr('disabled','disabled');
                       $("#row-user-type").attr('disabled','disabled');
                      $("#button-save").addClass("hide");
                      // show form
                      $("#modal-email-template").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });

  @foreach($rows as $row)
  $("#my-checkbox-{{$row->id}}").bootstrapSwitch();
 
  $(".content-wrapper").on('switchChange.bootstrapSwitch','#my-checkbox-{{$row->id}}', function(event, state) {

      var id = $(this).data('id');
      var status = state;
      var stat = 0;
      if(status == true){
          stat = 1;
      }
      if(status == false){
          stat = 2;
      }
       $.post("{{ url('admin/email-template/change-status') }}", { id: id, stat:stat, _token: $_token }, function(response) {
        refresh();
      }, 'json');
   
    });

  $(".content-wrapper").on("click", ".btn-add", function() {
    $("#modal-email-template").modal("show");
  });
   @endforeach
   $(document).ready(function() {
      $('.summernote').summernote({
      toolbar: [
    // [groupName, [list of button]]
   ['style', ['bold', 'italic', 'underline', 'clear']],
    ['fontname', ['fontname', 'fontsize', 'color']],
    
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height', 'table']],
    ['link', ['link', 'picture', 'video']],
    ['fullscreen', ['fullscreen', 'codeview', 'help']],
  ]
      });
    });
   $(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
   });
   $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
   });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Disable Template', 'Are you sure you want to disable this ?', "{{ url('admin/email-template/disable') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      var title = $(this).parent().parent().data('title');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-warning");
      dialog('Enable Template', 'Are you sure you want to enable this?', "{{ url('admin/email-template/enable') }}",id);
  });
</script> 
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12 paddingBottomA">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
<!--         <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#" disabled><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
         <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                <div class="input-group borderZero">
                <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
                <input type="text" class="form-control borderZero" id="row-search">
                </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="input-group borderZero">
                      <span class="input-group-addon borderZero" id="basic-addon1">Status</span>
                          <select class="form-control borderZero" id="row-filter_status" onchange="refresh()">
                                    <option value="">All</option>
                                    <option value="1">Enabled</option>
                                    <option value="2">Disabled</option>
                                    <!-- <option value="4">UnApproved</option> -->
                          </select>
                      </div>
                 </div>
            </div>
    </div>
    <div class="table-responsive col-xs-12 noPAdding">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>

        <table class="table table-condensed noMarginBottom backend-generic-table" id="email-template-table">
          <thead>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="newsletter_name" id="row-sort"><i class="fa"></i> Name</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="title" id="row-sort"><i class="fa"></i> Title</div></th>
              <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created At</div></th>
              <th class="text-right">Tools</th>
            </tr>
          </thead>
            <tbody id="rows">
           
           @foreach($rows as $row)
           <tr data-id="{{ $row->id }}" data-name="{{ $row->newsletter_name }}" class="{{($row->newsletter_status == 2 ? 'disabledColor' : '') }}">
             <td><small>{{$row->newsletter_name}}</small></td>
              <td><small>{{$row->title}}</small></td>
              <td><small>{{$row->created_at}}</small></td>
               <td class="rightalign">
                @if($row->newsletter_status == 1)
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-disable" data-toggle="tooltip" title="Disable"><i class="fa fa-lock"></i></button>
                @else
                  <button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-enable" data-toggle="tooltip" title="Enable"><i class="fa fa-unlock"></i></button>
                @endif
                <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button>            
                 <a href="{{ url('admin/email-template') . '/' . $row->id . '/edit'}}"><button type="button" class="btn btn-default btn-xs btn-attr pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button></a>  
                
              </td>
            <!--   <td class="rightalign">
                     @if($row->newsletter_status == 1)
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}" checked>
                    @else
                      <input data-id = "{{$row->id}}" type="checkbox" name="my-checkbox" id="my-checkbox-{{$row->id}}"> 
                    @endif
              </td>        -->
           </tr>
           @endforeach
          </tbody>
       </table>
     </div>
    <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}}" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.email-template-management.form')