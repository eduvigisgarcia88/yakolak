@extends('layout.master')
@section('scripts')

<script>
//CSRF Token Setup
$.ajaxSetup({
  headers: {
    'X-CSRF-Token': "{{ csrf_token() }}"
  }
});
</script>

<script>
//VIEW
$(document).on('click', '.btn-view', function(){
  var btn = $(this);
  var id = $(this).parent().parent().data('id');
  btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass('fa-pencil').parent().prop('disabled',true);
  $('#api_id_block').removeClass('hide');
  $('.modal-title').text('View API Key / Secret');
  $.ajax({
    type: 'POST',
    data: {id: id},
    url: '{{ url('admin/dashboard/collect/config') }}',
    success: function(response)
    {
      btn.find('i').addClass('fa-pencil').removeClass('fa-circle-o-notch fa-spin').parent().prop('disabled',false);
      $('#row-vanity').val(response.vanity);
      $('#row-description').val(response.description);
      $('#modal-vanity-edit').find('.modal-footer').hide('fast');
      $('#row-vanity, #row-description').prop('readonly',true).css('cursor', 'text');
      $('#modal-vanity-edit').modal('toggle');
    }
  });
});

//EDIT
$(document).on('click', '.btn-edit', function(){
  var btn = $(this);
  var id = $(this).parent().parent().data('id');
  $('#row-vanity, #row-description').prop('readonly',false).css('cursor', 'text');
  btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass('fa-pencil').parent().prop('disabled',true);
  $('.modal-title').text('Edit API Key / Secret');
  $('#modal-vanity-edit').find('.modal-footer').show('fast');
  $('#row-api_secret, #row-api_key, #row-api_name').prop('readonly',false).css('cursor', 'auto');
  $.ajax({
    type: 'POST',
    data: {id: id},
    url: '{{ url('admin/dashboard/collect/config') }}',
    success: function(response)
    {
      btn.find('i').addClass('fa-pencil').removeClass('fa-circle-o-notch fa-spin').parent().prop('disabled',false);
      $('#row-vanity').val(response.vanity);
      $('#row-description').val(response.description);
      $('#row-vanity-id').val(response.id);
      $('#row-vanity-indicator').val(0);
      $('#modal-vanity-edit').modal('toggle');
    }
  });
});

//ADD
$(document).on('click', '.btn-api_set', function(){
  $('.modal-title').text('API Configurations');
  $('#modal-api-edit').modal('toggle');
});

$(document).on('click', '.btn-add', function(){
  $('#row-vanity-indicator').val(1);
  $('.modal-title').text('Blacklist');
  $('#modal-vanity-edit').modal('toggle');
  $('#vanity-edit-form').trigger('reset');
});

$(document).on('click', '.btn-delete', function(){
  var id = $(this).parent().parent().data('id');
  dialog('Remove Blacklist', 'Are you sure you want to delete this vanity URL</strong>?', "{{ url('delete/vanity/url') }}",id);
});



//FORM SUBMIT
$('#api-edit-form').submit(function(e){
  $('div#api_load_div').removeClass('hide');
  $('#button-api-save').prop('disabled', true);
  e.preventDefault();
  $.ajax({
    type: 'POST',
    data: new FormData(this),
    processData: false,
    contentType: false,
    url: '{{ url('admin/dashboard/edit/config') }}',
    success: function(response)
    {
      $('div#api_load_div').addClass('hide');
      $('#button-api-save').prop('disabled', false);
      if(response.code == 1)
      {
        status('Success', response.message, 'alert-success');
        $('#modal-api-edit').modal('toggle');
      }
      else
      {
        status('Error', response.message, 'alert-danger');
        $('#modal-api-edit').modal('toggle');
      }
    }
  });
});

$('#vanity-edit-form').submit(function(e){
  $('div.loading-pane').removeClass('hide');
  e.preventDefault();
  $.ajax({
    type: 'POST',
    data: new FormData(this),
    processData: false,
    contentType: false,
    url: '{{ url('admin/dashboard/reserve/vanity') }}',
    beforeSend: function(){
      $('div.validation-messages').remove();
    },
    success: function(response)
    {
      $('div.loading-pane').addClass('hide');
      if(response.code == 0)
      {
        $('#form-notice-vanity').prepend("<div class='validation-messages alert alert-danger alert-dismissible' style='border-radius:0px;'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a> </div>");
        $.each(response.errors, function(i, v){
          $('div.validation-messages').append('<p class="errors_p">'+v+'</p>');
        });
      }
      else if(response.code == 1)
      {
        status('Success', response.message, 'alert-success');
        refresh();
        $('#modal-vanity-edit').modal('toggle');
      }
      else
      {
        status('Error', response.message, 'alert-danger');
        refresh();
        $('#modal-vanity-edit').modal('toggle');
      }
    }
  });
});

$('#contact-edit-form').submit(function(e){
  $('#button-contact-save').prop('disabled', true);
  $('div#contact_load_div').removeClass('hide');
  e.preventDefault();
  $.ajax({
    type: 'POST',
    data: new FormData(this),
    processData: false,
    contentType: false,
    url: '{{ url('admin/dashboard/edit/contact') }}',
    beforeSend: function(){
      $('sup').text('');
    },
    success: function(response)
    {
      $('#button-contact-save').prop('disabled', false);
      $('div#contact_load_div').addClass('hide');
      if(response.code == 1)
      {
        status('Success', response.message, 'alert-success');
      }
      else
      {
        $.each(response.errors, function(i, v){
          var error_id = v.split('|')[0];
          var error_msg = v.split('|')[1];
          $('#'+error_id).text(error_msg);
        });
      }
    }
  });
});

</script>

<script>
//REFRESH
function refresh() {
  $('#table_load_div').removeClass('hide');
  var loading = $("#table_load_div");
  var table = $("#rows");
  $('#refresh_button').find('i').addClass('fa-spin').parent().prop('disabled',true);
  $.post('{{ url('admin/settings/configuration') }}', function(response){
    console.log(response);
    table.html("");
    var body = "";
    $.each(response.vanity, function(index, row) {
    body += '<tr data-id="' + row.id + '">'+
            '<td>'+ row.vanity +'</td>'+
            '<td>'+ row.description +'</td>'+
            '<td>'+ row.created_at +'</td>'+
            '<td>'+
               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>'+
               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>'+
               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>'+
            '</td></tr>';
    });
    $("#modal-form").find('form').trigger("reset");
    table.html(body);
    $(".th-sort").find('i').removeAttr('class');
    $('#row-pages').html(response.pages);
    loading.addClass("hide");
    $('#refresh_button').find('i').removeClass('fa-spin').parent().prop('disabled',false);
  }, 'json');
}
</script>

@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
{!! Form::open(array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'contact-edit-form')) !!}
      <div class="col-lg-12 col-sm-12 col-xs-12 noPadding borderbottomLight bottomPaddingC bottomMarginC">
        <h3><i class="fa fa-book"></i> Contact Details <span class="pull-right"><button class="btn btn-success hide-view" id="button-contact-save" type="submit"><i class="fa fa-save"></i> SAVE CHANGES</button></span></h3>
      </div>
      <div class="col-sm-12">
      <div id="contact_load_div" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
      </div>
        <div id="form-notice"></div>
        @foreach($contact_details as $row)
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Contact Email</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-email" name="email" type="name" value="{{$row->email}}">
            <sup id="error-email" class="sup-errors redText"></sup>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Google Map (Search keyword)</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-address" name="address" type="name" value="{{$row->address}}">
            <sup id="error-address" class="sup-errors redText"></sup>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Address</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-address_line" name="address_line" type="name" value="{{$row->address_line}}">
            <sup id="error-address_line" class="sup-errors redText"></sup>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">City</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-city" name="city" type="name" value="{{$row->city}}">
            <sup id="error-city" class="sup-errors redText"></sup>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Zip Code</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-zip" name="zip" type="name" value="{{$row->zip}}">
            <sup id="error-zip" class="sup-errors redText"></sup>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Telephone</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-tel" name="tel" type="name" value="{{$row->tel}}">
            <sup id="error-tel" class="sup-errors redText"></sup>
          </div>
        </div>
        
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Description</label>
          <div class="col-sm-9 col-xs-7">
            <textarea class="form-control" id="row-description" name="description" form="contact-edit-form">{{$row->description}}</textarea>
            <sup id="error-description" class="sup-errors redText"></sup>
          </div>
        </div>
        <hr class="remove_hr">
        <input id="row-id" name="id" type="hidden" value="{{$row->id}}">
        @endforeach
      </div>
      {!! Form::close() !!}


{!! Form::open(array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'api-edit-form')) !!}
      <div class="col-lg-12 col-sm-12 col-xs-12 noPadding borderbottomLight bottomPaddingC bottomMarginC">
        <h3><i class="fa fa-book"></i> API Configurations <span class="pull-right"><button class="btn btn-success hide-view" id="button-api-save" type="submit"><i class="fa fa-save"></i> SAVE CHANGES</button></span></h3>
      </div>
      <div class="col-sm-12 ">
      <div id="api_load_div" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
      </div>
        <div id="form-notice"></div>
        @foreach($apis as $row)
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">API Name</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-api_name" name="api_name[]" type="name" value="{{$row->name}}">
          </div>
        </div>
        <div class="form-group" id="api_id_block">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">API ID</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-api_id" name="api_id[]" value="{{$row->api_id}}" placeholder="This is optional.">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">API Secret</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-api_secret" name="api_secret[]" type="name" value="{{$row->api_secret}}">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">API Key</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-api_key" name="api_key[]" type="name" value="{{$row->api_key}}">
          </div>
        </div>
        <hr class="remove_hr">
        <input id="row-id" name="id[]" type="hidden" value="{{$row->id}}">
        @endforeach
      </div>
      {!! Form::close() !!}

    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> Blacklist</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12 topPaddingB">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" id="refresh_button" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>
        {{-- <button type="button" class="btn btn-primary btn-md btn-attr pull-right btn-api_set"><i class="fa fa-wrench"></i> API Settings</button> --}}
      </div>
    </div>
    <div class="table-responsive topPaddingC col-xs-12">
      <div id="table_load_div" class="loading-pane hide">
        <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
      </div>
      <table class="table table-condensed noMarginBottom backend-generic-table">
            <thead>
                <tr>
                  <th><small>Value</small></th>
                  <th><small>Description</small></th>
                  <th><small>Created Date</small></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($vanity as $row)
            <tr data-id="{{$row->id}}">
                <td>{{$row->vanity}}</td>
                <td>{{$row->description}}</td>
                <td>{{$row->created_at}}</td>
                <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="modal" data-target="#"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="modal" data-target="#"><i class="fa fa-pencil"></i></button>
                </td>
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
  </div>
</div>
@stop
@include('admin.api-management.form')