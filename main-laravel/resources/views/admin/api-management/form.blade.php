<div aria-hidden="true" aria-labelledby="form-title" class="modal fade" data-backdrop="static" id="modal-vanity-edit" role="dialog" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="loading-pane hide" id="load-form">
        <div>
          <i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i>
        </div>
      </div>
      {!! Form::open(array('role' => 'form', 'class' => 'form-horizontal', 'id' => 'vanity-edit-form')) !!}
      <div class="modal-header modal-warning">
        <button aria-hidden="true" class="close" data-dismiss="modal" type="button">&times;</button>
        <h4 class="modal-title">...</h4>
      </div>
      <div class="modal-body">
        <div id="form-notice-vanity"></div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Value</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-vanity" name="vanity" type="name">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 col-xs-5 font-color" for="row-name">Description</label>
          <div class="col-sm-9 col-xs-7">
            <input class="form-control" id="row-description" name="description">
          </div>
        </div>
        <input id="row-vanity-indicator" name="vanity-indicator" type="hidden">
        <input id="row-vanity-id" name="id" type="hidden">
      </div>
      <div class="modal-footer" style="margin-bottom: -15px;">
        <button class="btn btn-danger" data-dismiss="modal" id="button-close" type="button"><i class="fa fa-times"></i> Close</button>
        <button class="btn btn-success hide-view" id="button-save" type="submit"><i class="fa fa-save"></i> Save changes</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>