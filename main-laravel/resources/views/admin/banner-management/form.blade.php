<!-- modal add user -->
<div class="modal fade" id="modal-form-purchase-banner" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title panelTitle">PURCHASE ADVERTISEMENT</h4>
          </div>
      <div class="modal-body nobottomPadding">
          <div id="banner-load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
          </div>
          <div id="form-banner_notice"></div>
      {!! Form::open(array('url' => 'banner/payment/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form_banner', 'files' => true)) !!}
           <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Order No</h5>
            <div class="col-sm-9">
              <input type="text" class="form-control borderZero" readonly id="row-order_no" name="order_no">
            </div>
          </div>
          <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Country</h5>
            <div class="col-sm-9">
              <select name="country_id" id="row-country_id" class="form-control borderZero disable-view">
                @foreach ($countries as $row)
                  <option value="{{$row->id}}"> {{ $row->countryName }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Page</h5>
            <div class="col-sm-9">
            <select class="form-control borderZero disable-view" name="page" id="row-page">
              <option class="hide">Select:</option>
              @foreach($banner_placement_location as $row)
                <option value="{{$row->id}}">{{$row->name}}</option>
              @endforeach
            </select>
            </div>
          </div>
          <div class="form-group hide" id="banner-category-pane">
            <h5 class="normalText col-sm-3" for="email">Category</h5>
            <div class="col-sm-9">
            <select type="text" class="form-control borderZero disable-view" id="ads-category"  name="category">
            
                  </select>
            </div>
          </div>
          <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Placement</h5>
            <div class="col-sm-9">
              <select name="banner_placement_id" id="row-banner_placement_id" class="form-control borderZero disable-view">
                  <option value="" selected> Select:</option>
              </select>
              <sub class="redText topmarginveryLight hide" id="error-placement-slot">* No availble slot for this placement</sub>
            </div>
          </div>
          <div class="form-group hide" id="banner_price_pane">
            <h5 class="normalText col-sm-3" for="email">Duration</h5>
            <div class="col-sm-9">
                   <input type="number" class="form-control borderZero disable-view col-xs-12" style="width:200px;display:inline;" placeholder="Duration" name="duration" id="row-duration">
                     <select id="placement-price" style="width:218px;display:inline;" class="form-control borderZero disable-view placement_amount col-xs-12">
                     </select>
            </div>
          </div>
        <!--   <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Duration</h5>
            <div class="col-sm-9">
               
            </div>
          </div> -->
          <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Total</h5>
            <div class="col-sm-9">
               <input type="number" class="form-control borderZero" readonly="readonly" style="background-color:none !important;" id="row-total_cost">
            </div>
          </div>
          <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Target URL</h5>
            <div class="col-sm-9">
               <input type="url" class="form-control borderZero disable-view" name="target_url" id="row-target_url">
               <input type="hidden" class="form-control borderZero" name="amount" id="row-placement_actual_amount">
                 <input type="hidden" class="form-control borderZero" name="metric_duration" id="row-placement_actual_duration">
            </div>
          </div>
          
      <!--    <div class="row borderBottomDarkLight blockBottom">                  
               </div>
              <div class="form-group">
            <h5 class="normalText col-sm-3" for="email">Referral Link</h5>
            <div class="col-sm-9">
              <input type="url" class="form-control borderZero disable-view" name="link" id="row-link" placeholder="">
             
            </div>
          </div> -->
      <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"> Image (Desktop)</h5>
               <div class="col-sm-9">
                <div id="preview">
            <img id="row-img-desktop" class="hide bottomPadding img-responsive" src="#" alt="your image" />
        </div>
    <!--    <div id="preview" class="center visible-xs">
         <img id="row-img-mobile" class="bottomPadding noMargin img-responsive" src="#" alt="your image">
        </div> -->
        <button type="button" class="btn blueButton btn-sm borderZero disable-view" id="change-photo-desktop" style="width:100%">Change Photo</button>
        <div class="uploader-pane">
            <div class="change">
          <div class="input-group input-group">
            <input type="text" name="file_name_desktop" id="row-file_name_desktop" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
            <span class="input-group-btn">
              <label class="btn-file btn blueButton borderZero cursor">
              Browse <input name="photo_desktop" type="file" id="row-photo-desktop" data-id="desktop" class="photo btn photoName borderZero form-control borderZero cursor"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*" value="Upload a Photo">
              </label> 
            </span>
            <span class="input-group-btn">
              <button data-id="desktop" type="button" class="btn removeImage redButton borderZero leftMargin"><span style="padding: 1px;">Delete</span></button>
            </span>
          </div>
           <sup class="sup-errors redText"></sup>
        </div>
        </div>
              </div>
            </div>
            <div class="form-group">
              <h5 class="col-sm-3 normalText" for="register4-email"> Image (Mobile)</h5>
               <div class="col-sm-9">
                <div id="preview">
            <img id="row-img-mobile" class="hide bottomPadding img-responsive" src="#" alt="your image" />
        </div>
        <button type="button" class="btn blueButton btn-sm borderZero disable-view" id="change-photo-mobile" style="width:100%">Change Photo</button>
        <div class="uploader-pane">
            <div class="change">
          <div class="input-group input-group">
            <input type="text" name="file_name_mobile" id="row-file_name_mobile" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
            <span class="input-group-btn">
              <label class="btn-file btn blueButton borderZero cursor">
              Browse <input name="photo_mobile" type="file" id="row-photo-mobile" data-id="mobile" class="photo btn photoName borderZero form-control borderZero cursor"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*"  value="Upload a Photo">
              </label> 
            </span>
            <span class="input-group-btn">
              <button data-id="mobile" type="button" class="btn removeImage redButton borderZero leftMargin"><span style="padding: 1px;">Delete</span></button>
            </span>
          </div>
           <sup class="sup-errors redText"></sup>
        </div>
        </div>
              </div>
            </div>
        </div>
      <div class="modal-footer borderTopDarkB">
        <input type="hidden" name="status" id="row-status"> 
        <input type="hidden" name="id" id="row-id"> 
        <button type="submit" class="btn btn-sm blueButton borderZero noMargin" id="btnPurchaseBanner">Purchase</button>
        <button type="button" class="btn btn-sm redButton borderZero hide" data-dismiss="modal" id="btnClose"><i class="fa fa-times"></i> Close</button>
      </div>
      {!! Form::close() !!} 

    </div>
  </div>
</div>
  <div class="modal" id="modal-reject" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
<div id="load-form" class="loading-pane hide">
  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
</div>
{!! Form::open(array('url' => 'admin/banner/decline', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'reject-banner', 'files' => true)) !!}
<div class="modal-header modalTitleBar">
  <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
  <h4 class="modal-title modal-title-bidder panelTitle">Reject Banner</h4>
</div>
<div class="modal-body">
  <div id="validation-errors">
  </div>
  
  <div class="col-lg-12">
<!--   <h4 class=""><span class="leftPadding bigText">Are you sure you want to reject this banner?</span></h4> -->
  <p>Reason for rejecting banner?<span><input type="text" placeholder="" class="form-control borderZero topMargin" name="reason" id="reason" required="required"></span></p>
  </div>
</div>
<div class="clearfix"></div>
  <div class="modal-footer">
    <input type="hidden" name="id" id="banner_id" value="">
    <button type="submit" class="btn btn-default borderZero">Yes</button>
    <button type="button" class="btn btn-default borderZero" data-dismiss="modal">No</button>
  </div>
{!! Form::close() !!}
</div>
</div>
</div>