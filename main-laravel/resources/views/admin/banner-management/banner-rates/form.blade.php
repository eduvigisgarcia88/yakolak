<!-- modal add user -->
  <div class="modal fade" id="modal-add-banner" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div id="load-form" class="loading-pane hide">
                  <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
                </div>
          {!! Form::open(array('url' => 'admin/banner/rate/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-banner_form', 'files' => 'true')) !!}
              <div class="modal-header modal-warning">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-plus"></i> ADD BANNER</h4>
              </div>
              <div class="modal-body">
               <div id="form-notice"></div>
               <!--  <div class="form-group">
                    <label for="row-name" class="col-sm-3 col-xs-5 control-label font-color">Photo</label>
                <div class="col-sm-9 col-xs-7">
                    <img src="#" id="row-photo" height="100" width="100%">
                    <button type="button" class="btn btn-primary pull-right hide-view btn-change"><i class="fa fa-camera"></i> Change Photo</button>
                    <button type="button" class="btn btn-primary pull-right btn-cancel">Cancel</button>
                    <div class="uploader-pane">
                        <div class='change'>
                            <input name='photo' id='row-photo_upload' type='file' class='file borderZero file_photo' data-show-upload='false' placeholder='Upload a photo'>
                        </div>
                     </div>
                </div>
                </div> -->
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Placement Name</label>
                <div class="col-sm-8 col-xs-7">
                     <input type="text" name="placement_name" class="form-control edit-view" id="row-banner_placement_name" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Location</label>
                <div class="col-sm-8 col-xs-7">
                  <select class="form-control edit-view" id="row-location_id" name="location_id">
                    <option class="hide">Select</option>
                    @foreach($banner_placement_pages as $row)
                        <option value="{{$row->id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
                </div>
                </div>
               <!--  <div class="form-group usertype-pane">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Usertype</label>
                <div class="col-sm-8 col-xs-7">
                  <select class="form-control" id="row-usertype_id" name="usertype_id">
                    <option class="hide">Select</option>
                    @foreach($usertype as $row)
                        <option value="{{$row->id}}">{{$row->type_name}}</option>
                    @endforeach
                  </select>
                </div>
                </div> -->
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Country</label>
                <div class="col-sm-8 col-xs-7">
                       <select class="form-control edit-view" id="row-country_id" name="country">
                          <option class="hide">Select</option>
                          @foreach($countries as $row)
                              <option value="{{$row->id}}">{{$row->countryName}}</option>
                          @endforeach
                        </select>
                </div>
                </div>
                <div class="form-group hide" id="category-pane">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Category</label>
                <div class="col-sm-8 col-xs-7">
                      <select class="form-control edit-view" id="row-category_id" name="category_id">

                      </select>
                </div>
                </div>
                 <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Daily Cost</label>
                <div class="col-sm-8 col-xs-7">
                     <input type="number" name="daily_cost_paid" class="form-control" id="row-daily_cost_paid" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Monthly Cost</label>
                <div class="col-sm-8 col-xs-7">
                     <input type="number" name="monthly_cost_paid" class="form-control" id="row-monthly_cost_paid" maxlength="30">
                </div>
                </div>
                <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Yearly Cost</label>
                <div class="col-sm-8 col-xs-7">
                     <input type="number" name="yearly_cost_paid" class="form-control" id="row-yearly_cost_paid" maxlength="30">
                </div>
                </div>
               <!--  <div class="form-group">
                    <label for="row-sequence" class="col-sm-4 col-xs-5 control-label font-color">Period</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="period" class="form-control" id="row-period" maxlength="30">
                </div>
                </div> -->
                <!-- <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Discount Duration(month)</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="duration" class="form-control" id="row-duration" maxlength="30">
                </div>
                </div> -->
                <!-- <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Price</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="price" class="form-control" id="row-price" maxlength="30">
                </div>
                </div> -->
                <!-- <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Discount</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="discount" class="form-control" id="row-discount" maxlength="30">
                </div>
                </div> -->
                <!-- <div class="form-group">
                    <label for="row-sequence" class="col-sm-3 col-xs-5 control-label font-color">Points</label>
                <div class="col-sm-9 col-xs-7">
                     <input type="name" name="points" class="form-control" id="row-points" maxlength="30">
                </div>
                </div> -->
              </div> 
              <div class="modal-footer">
                <input type="hidden" name="id" id="row-id">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="button-close"><i class="fa fa-times"></i> Close</button>
                <button type="submit" class="btn btn-success hide-view" id="button-save"><i class="fa fa-save"></i> Save changes</button>
              </div>
          {!! Form::close() !!}
            </div>
          </div>
        </div>
  