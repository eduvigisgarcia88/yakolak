@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  function refresh() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        console.log(response);
        var body = "";
        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small><center>'+row.daily_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.monthly_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.yearly_cost_paid +'</center></small></td>'+
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
 function search() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
        body += '<tr data-id="' + row.id + '">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small><center>'+row.daily_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.monthly_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.yearly_cost_paid +'</center></small></td>'+
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
   function maxAds() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
        body += '<tr data-id="' + row.id + '">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small><center>'+row.daily_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.monthly_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.yearly_cost_paid +'</center></small></td>'+
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
 $("#banner-rates-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $ad_type = $("#row-filter_ad_type").val();
          $search_tool = $("#row-filter_search_tool").val();
          $listing_type = $("#row-filter_listing_type").val();
          $status = $("#row-filter_status").val();
          $per = $("#row-per").val();
          $("#banner-rates-table").find("i").removeClass('fa-caret-up');
          $("#banner-rates-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
          if($order == "asc"){
            $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
         }else{
            $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
          }
        $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {    
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {
          body += '<tr data-id="' + row.id + '">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small><center>'+row.daily_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.monthly_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.yearly_cost_paid +'</center></small></td>'+
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");        
          if(response.order == "asc"){
              $("#row-order").val("desc");
              //  $("#userManagement").find("i").find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          }else{
              $('#row-order').val("asc");
              // $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          }
      }, 'json');
  

    });
  $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $listing_type = $("#row-filter_listing_type").val();
      $country = $("#row-filter_country").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, country: $country,search: $search, status:$status,ad_type:$ad_type,listing_type:$listing_type,per: $per, _token: $_token }, function(response) {    
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
           body += '<tr data-id="' + row.id + '">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small><center>'+row.daily_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.monthly_cost_paid +'</center></small></td>'+
              '<td><small><center>'+row.yearly_cost_paid +'</center></small></td>'+
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
      }
    });
  $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
  
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Banner Rate</strong>');

                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                          // if(index == "id"){
                          //     console.log(value);
                          //     $("#modal-add-banner").find('form').find("#row-id").val(id);   
                          // }
                        if(index == "location_id"){
                         if(value != 1){
                           $("#modal-add-banner").find('form').find("#category-pane").removeClass("hide");    
                           $("#modal-add-banner").find('form').find("#row-category_id").html(response.country);    
                         }else{
                           $("#modal-add-banner").find('form').find("#category-pane").addClass("hide");    
                         }
                        }

                      });
                       
                     
                      $("#button-save").removeClass("hide");
                      $("#modal-add-banner").find('form').find(".edit-view").prop("disabled","disabled");    
                      $("#modal-add-banner").find('form').find(".hide-view").removeClass("hide");
                      // $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
$(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      
                       $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Banner Rate </strong>');
                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                            console.log(index +"="+value);
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                        
                      });
                      $("#button-save").addClass("hide");
                      $("#modal-add-banner").find('form').find(".hide-view").addClass("hide");
                      $("#modal-add-banner").find('form').find(":input").not('#button-close').not('.close').attr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-add", function() {
        $(".uploader-pane").removeClass("hide");
        $(".usertype-pane").removeClass("hide");
        $("#modal-add-banner").find(".btn-change").addClass("hide");
        $("#row-id").val("");
        $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled"); 
        $("#modal-add-banner").find('form').trigger("reset");
        $("#modal-add-banner").modal('show');
        $("#row-placement_plan").html(""); 
        $("#row-photo").addClass('hide');
        $("#button-save").removeClass("hide");
    });

    $(".content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Delete Banner', 'Are you sure you want to delete this banner rate</strong>?', "{{ url('admin/banner/rate/delete') }}",id);
    }); 
     $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <!-- <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>     -->
      </div>
       <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
<!--                                 <option value="">All</option> -->
                                @foreach($countries as $row)
                                   <option value="{{$row->id}}">{{$row->countryName}}[{{$row->countryCode}}]</option>
                                @endforeach
                      </select>
                  </div>
             </div>
        </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="banner-rates-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_plans.banner_placement_name" id="row-sort"><i class="fa"></i> Placement Name</div></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countryCode" id="row-sort"><i class="fa"></i> Country</div></center></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_location.name" id="row-sort"><i class="fa"></i> Location</div></th>
<!--                   <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.name" id="row-sort"><i class="fa"></i> Category</div></th> -->
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_plans.daily_cost_paid" id="row-sort"><i class="fa"></i> Daily Cost</div></center></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_plans.monthly_cost_paid" id="row-sort"><i class="fa"></i> Monthly Cost</div></center></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_plans.yearly_cost_paid" id="row-sort"><i class="fa"></i> Yearly Cost</div></center></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="country_banner_placement_plans.created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created Date</div></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">       
            @foreach($rows as $row)
                <tr data-id = "{{$row->id}}">
                  <td><small>{{$row->banner_placement_name}}</small></td>
                  <td><small><center>{{$row->countryCode}}</center></small></td>
                  <td><small>{{$row->name}}</small></td>
<!--                   <td><small>{{$row->categoryName == null ? 'N/A':$row->categoryName}}</small></td> -->
                  <td><small><center>{{$row->daily_cost_paid}}</center></small></td>
                  <td><small><center>{{$row->monthly_cost_paid}}</center></small></td>
                  <td><small><center>{{$row->yearly_cost_paid}}</center></small></td>
                  <td><small>{{$row->created_at}}</small></td>
                  <td>
                   <!--  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button> -->
                   <!--  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button> -->
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                  </td> 
                </tr>
            @endforeach
            </tbody>
       </table>
     </div>
    <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter paddingFooter ">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
   </div>
  </div>
</div>
@stop
@include('admin.banner-management.banner-rates.form')