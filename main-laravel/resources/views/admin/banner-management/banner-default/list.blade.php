@extends('layout.master')
@section('scripts')
<script>
  $_token = '{{ csrf_token() }}';
  function refresh() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $category = $("#row-filter_category").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { category:$category,usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class="'+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small>'+ (row.reserve_status == 1 ? 'Paid': 'Reserved') +'</small></td>'+ 
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+

               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
               (row.reserve_status == 2 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve"><i class="fa fa-bookmark-o"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve"><i class="fa fa-bookmark"></i></button>')+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
 function search() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $category = $("#row-filter_category").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { category:$category,usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
             body += '<tr data-id="' + row.id + '" class="'+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small>'+ (row.reserve_status == 1 ? 'Paid': 'Reserved') +'</small></td>'+ 
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+

               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
               (row.reserve_status == 2 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve"><i class="fa fa-bookmark-o"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve"><i class="fa fa-bookmark"></i></button>')+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
   function maxAds() {
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $country = $("#row-filter_country").val();
      $category = $("#row-filter_category").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { category:$category,usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {
             body += '<tr data-id="' + row.id + '" class="'+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small>'+ (row.reserve_status == 1 ? 'Paid': 'Reserved') +'</small></td>'+ 
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+

               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
               (row.reserve_status == 2 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve"><i class="fa fa-bookmark-o"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve"><i class="fa fa-bookmark"></i></button>')+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
        table.html(body);
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    }
 $("#banner-rates-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $ad_type = $("#row-filter_ad_type").val();
          $search_tool = $("#row-filter_search_tool").val();
          $listing_type = $("#row-filter_listing_type").val();
          $usertype_id = $("#row-filter_usertype_id").val();
          $status = $("#row-filter_status").val();
          $category = $("#row-filter_category").val();
          $per = $("#row-per").val();
          $("#banner-rates-table").find("i").removeClass('fa-caret-up');
          $("#banner-rates-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
          if($order == "asc"){
            $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
         }else{
            $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
          }
        $.post("{{ $refresh_route }}", { category:$category,usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {   
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {

               body += '<tr data-id="' + row.id + '" class="'+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small>'+ (row.reserve_status == 1 ? 'Paid': 'Reserved') +'</small></td>'+ 
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+

               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
               (row.reserve_status == 2 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve"><i class="fa fa-bookmark-o"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve"><i class="fa fa-bookmark"></i></button>')+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");        
          if(response.order == "asc"){
              $("#row-order").val("desc");
              //  $("#userManagement").find("i").find("i").removeClass("fa fa-caret-up").addClass("fa fa-caret-down");
          }else{
              $('#row-order').val("asc");
              // $(this).find("i").removeClass("fa fa-caret-down").addClass("fa fa-caret-up");
          }
      }, 'json');
  

    });
  $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $ad_type = $("#row-filter_ad_type").val();
      $listing_type = $("#row-filter_listing_type").val();
      $country = $("#row-filter_country").val();
      $status = $("#row-filter_status").val();
      $usertype_id = $("#row-filter_usertype_id").val();
      $per = $("#row-per").val();
      $category = $("#row-filter_category").val();
      var loading = $(".loading-pane");
      var table = $("#rows");

      $.post("{{ $refresh_route }}", { category:$category,usertype_id: $usertype_id,page: $page, country:$country ,sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) { 
      table.html("");
      console.log(response);
      var body = "";

          $.each(response.rows.data, function(index, row) {
                body += '<tr data-id="' + row.id + '" class="'+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.banner_placement_name +'</small></td>'+
              '<td><small><center>'+ row.countryCode +'</center></small></td>'+
              '<td><small>'+ row.name +'</small></td>'+
//               '<td><small>'+ (row.categoryName == null ? 'N/A': row.categoryName) +'</small></td>'+
              '<td><small>'+ (row.reserve_status == 1 ? 'Paid': 'Reserved') +'</small></td>'+ 
              '<td><small>'+row.created_at +'</small></td>'+
              '<td class="rightalign">'+

               '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit"><i class="fa fa-pencil"></i></button>' +
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete"><i class="fa fa-trash-o"></i></button>' +
               (row.reserve_status == 2 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve"><i class="fa fa-bookmark-o"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve"><i class="fa fa-bookmark"></i></button>')+
                 // '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-add-subcat"><i class="fa fa-subway"></i></button>' +
              '</td></tr>';
        });
      
      
      table.html(body);
      $('#row-pages').html(response.pages);
      loading.addClass("hide");
      }, 'json');
    
      }
    });
  $(".content-wrapper").on("click", ".btn-edit", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner-default/edit') }}", { id: id, _token: $_token }, function(response) {
                    console.log(response);
                  if(!response.error) {
                  // set form title
                      $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>Edit Default Banner</strong>');

                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                          // if(index == "id"){
                          //     console.log(value);
                          //     $("#modal-add-banner").find('form').find("#row-id").val(id);
                            if(index == "image_type"){
                                if(value == "home_premium_A"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'main'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "home_premium_B"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'main'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "home_premium_C"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'main'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "home_main_A"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'main'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "home_main_B"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'main'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub1_A"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub1'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub1_B"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub1'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub1_C"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub1'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub1_premium_1"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub1'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub1_premium_2"){
                                     $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub1'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub2_A"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub2'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub2_B"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub2'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub2_C"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub2'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub2_premium_1"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub2'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "sub2_premium_2"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'home_banners'.'/'.'sub2'.'/'}}"+response.rows.image_desktop);
                                }else if(value == "browse_top"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);
                                }else if(value == "browse_bottom"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);
                                }else if(value == "browse_side_A"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);
                                }else if(value == "browse_side_B"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);
                                }else if(value == "listings_side"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);
                                }else if(value == "listings_content"){
                                    $("#modal-add-banner").find("form").find("#row-photo").attr("src","{{url('uploads').'/'.'banner'.'/'.'advertisement'.'/desktop/'}}"+response.rows.image_desktop);

                                }
                            }

                        if(index == "category_id"){
                         if(value != null){
                           $("#modal-add-banner").find('form').find("#category-pane").removeClass("hide");    
                           $("#modal-add-banner").find('form').find("#row-category_id").html(response.country);    
                         }else{
                           $("#modal-add-banner").find('form').find("#category-pane").addClass("hide");    
                         }
                        }

                      });
                       
                     
                      $("#button-save").removeClass("hide");
                      $("#modal-add-banner").find('form').find(".edit-view").prop("disabled","disabled");    
                      $("#modal-add-banner").find('form').find(".hide-view").removeClass("hide");
                      // $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
$(".content-wrapper").on("click", ".btn-view", function() {
        var id = $(this).parent().parent().data('id');
        var btn = $(this);
        // reset all form fields
        $("#modal-add-banner").find('form').trigger("reset");
        $(".modal-header").removeAttr("class").addClass("modal-header tbheader");
        $(".uploader-pane").addClass("hide");
        $("#row-photo").removeClass('hide');
        $(".usertype-pane").addClass("hide");
        $(".btn-cancel").addClass("hide");
        $("#row-placement_plan").html("");
        $("#row-id").val("");
        $_token = "{{ csrf_token() }}";
        // User Id Disabled
        $('#modal-form').find("input[name='system_id']").attr('disabled', 'disabled');

        btn.find('i').addClass('fa-circle-o-notch fa-spin').removeClass("fa-pencil");
            
              $.post("{{ url('admin/banner/plan/manipulate') }}", { id: id, _token: $_token }, function(response) {
                  if(!response.error) {
                  // set form title
                      
                       $(".modal-title").html('<i class="fa fa-pencil"></i> <strong>View Banner Rate </strong>');
                      // output form data
                      $.each(response.rows, function(index, value) {
                          var field = $("#row-" + index);
                          // field exists, therefore populate
                            console.log(index +"="+value);
                          if(field.length > 0) {
                              field.val(value);
                              
                          }
                        
                      });
                      $("#button-save").addClass("hide");
                      $("#modal-add-banner").find('form').find(".hide-view").addClass("hide");
                      $("#modal-add-banner").find('form').find(":input").not('#button-close').not('.close').attr("disabled","disabled");
                      // show form
                      $("#modal-add-banner").modal('show');
                  } else {
                      status(false, response.error, 'alert-danger');
                  }

                  btn.find('i').removeClass('fa-circle-o-notch fa-spin').addClass("fa-pencil");
              }, 'json');
        
      });
      $(".content-wrapper").on("click", ".btn-add", function() {
        $(".uploader-pane").removeClass("hide");
        $(".usertype-pane").removeClass("hide");
        $("#modal-add-banner").find(".btn-change").addClass("hide");
        $("#row-id").val("");
        $("#modal-add-banner").find('form').find(":input").not('#button-close').removeAttr("disabled","disabled"); 
        $("#modal-add-banner").find('form').trigger("reset");
        $("#modal-add-banner").modal('show');
        $("#row-placement_plan").html(""); 
        $("#row-photo").addClass('hide');
        $("#button-save").removeClass("hide");
    });

    $(".content-wrapper").on("click", ".btn-delete", function() {
        var id = $(this).parent().parent().data('id');
        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Delete Banner', 'Are you sure you want to delete this banner rate</strong>?', "{{ url('admin/banner/rate/delete') }}",id);
    }); 
     $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });
    $(".btn-change").click(function(){
        $("#row-photo").addClass("hide");
        $(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
        $(".btn-cancel").removeClass("hide");
         
    });
    $(".btn-cancel").click(function(){
        $("#row-photo").removeClass("hide");
        $(".uploader-pane").addClass("hide");
        $(this).addClass("hide");
        $(".btn-change").removeClass("hide");
    });
    $(".content-wrapper").on("click", ".btn-reserve", function() {
       var id = $(this).parent().parent().data('id');
       $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('Reserve Placement', 'Are you sure you want to reserve this placement</strong>?', "{{ url('admin/banner/placement/reserve') }}",id);
    });
    $(".content-wrapper").on("click", ".btn-unreserve", function() {
       var id = $(this).parent().parent().data('id');
       $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
        dialog('UnReserve Placement', 'Are you sure you want to unreserve this placement</strong>?', "{{ url('admin/banner/placement/unreserve') }}",id);
    });
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <!-- <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>     -->
      </div>
       <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
<!--                                 <option value="">All</option> -->
                                @foreach($countries as $row)
                                   <option value="{{$row->id}}">{{$row->countryName}}&nbsp;[{{$row->countryCode}}]</option>
                                @endforeach
                      </select>
                  </div>
             </div>
<!--               <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Category</span>
                      <select class="form-control borderZero" id="row-filter_category" onchange="refresh()">
                                <option value="">All</option>
                                @foreach($categories as $row)
                                   <option value="{{$row->name}}">{{$row->name}}</option>
                                @endforeach
                      </select>
                  </div>
             </div> -->
        </div>
    <div class="table-responsive col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="banner-rates-table">
            <thead>
                <tr>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_plans.banner_placement_name" id="row-sort"><i class="fa"></i> Placement Name</div></th>
                  <th><center><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countryCode" id="row-sort"><i class="fa"></i> Country</div></center></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_location.name" id="row-sort"><i class="fa"></i> Location</div></th>
<!--                   <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.name" id="row-sort"><i class="fa"></i> Category</div></th> -->
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_defaults.reserve_status" id="row-sort"><i class="fa"></i> Type</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_defaults.created_at" id="row-sort"><i class="fa fa-caret-up"></i> Created Date</div></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">       
            @foreach($rows as $row)
                <tr data-id = "{{$row->id}}" class="{{$row->reserve_status == 2 ? 'reservedColor':''}}">
                  <td><small>{{$row->banner_placement_name}}</small></td>
                  <td><small><center>{{$row->countryCode}}</center></small></td>
                  <td><small>{{$row->name}}</small></td>
<!--                   <td><small>{{$row->categoryName == null ? 'N/A':$row->categoryName}}</small></td> -->
                  <td><small>{{$row->reserve_status == 1 ? 'Paid':'Reserved'}}</small></td>
                  <td><small>{{$row->created_at}}</small></td>
                  <td>
                   <!--  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-delete" data-toggle="tooltip" title="Delete"><i class="fa fa-trash-o"></i></button> -->
                   <!--  <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></button> -->
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></button>
                    @if($row->reserve_status == 1)
                          <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-reserve" data-toggle="tooltip" title="Reserve"><i class="fa fa-bookmark"></i></button>
                    @else
                        <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-unreserve" data-toggle="tooltip" title="UnReserve"><i class="fa fa-bookmark-o"></i></button>
                    @endif
                 
                  </td> 
                </tr>
            @endforeach
            </tbody>
       </table>
     </div>
    <input type="hidden" id="row-page" value="">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter paddingFooter ">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
   </div>
  </div>
</div>
@stop
@include('admin.banner-management.banner-default.form')