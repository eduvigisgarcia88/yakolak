@extends('layout.master')
@section('scripts')

<script>
   $_token = '{{ csrf_token() }}';
  function refresh() {
      
      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $s = $("#row-sa").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $country = $("#row-filter_country").val();
      $category = $("#row-filter_category").val();
      $status = $("#row-filter_status").val();
      $banner_status = $("#row-filter_banner_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per,country:$country,banner_status:$banner_status,category:$category ,_token: $_token }, function(response) {
        // clear
        table.html("");
        var body = "";
        $.each(response.rows.data, function(index, row) {
            body += '<tr data-id="' + row.id + '" class="'+(row.banner_status == 6 ? 'disabledColor':'')+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.order_no + '</small></td>' +
              '<td><small>'+ (row.banner_status == 1 ? 'Pending':'')+(row.banner_status == 2 ? 'Approved':'')+(row.banner_status == 3 ? 'Declined':'')+(row.banner_status == 4 ? 'Active':'')+(row.banner_status == 6 ? 'Disabled':'')+'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
//               '<td><small>'+ (row.category_name == null ? 'N/A': row.category_name) +'</small></td>'+
              '<td><small>'+ row.placement_name +'</small></td>'+
              '<td><small>'+ row.amount +'</small></td>'+
              '<td><small>'+ row.duration +' '+(row.metric_duration == "d" ? 'days':'')+(row.metric_duration == "m" ? 'months':'')+(row.metric_duration == "y" ? 'years':'')+'</small></td>'+
              '<td><small>'+ row.total_cost +'</small></td>'+
              '<td><small>'+ (row.reserve_status  == 2 ? 'Reserved':'Paid') +'</small></td>'+
              '<td class="rightalign">'+

                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view-banner"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit-banner"><i class="fa fa-pencil"></i></button>' +
                 (row.banner_status == 6 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" '+(row.banner_status == 3 ? 'disabled':'')+'><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable" '+(row.banner_status == 3 ? 'disabled':'')+' '+(row.banner_status == 1 ? 'disabled':'')+' ><i class="fa fa-lock"></i></button>')+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-decline" '+(row.banner_status == 2 ? 'disabled':'')+(row.banner_status == 3 ? 'disabled':'')+(row.banner_status == 6 ? 'disabled':'')+'><i class="fa fa-close"></i></button>'+
                 (row.banner_status  == 1 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" '+(row.banner_status==6 ? 'disabled':'')+'><i class="fa fa-check"></i></button>':'')+ 
              '</td></tr>';
        });
        $("#modal-form").find('form').trigger("reset");

        table.html(body);
        $(".th-sort").find('i').removeAttr('class');
        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');
    } 
    function search(){
      refresh();
    }
    function maxAds(){
      refresh();
    }    
    function maxAds(){
      refresh();
    }    
    $('#row-search').keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
          search();
      }
    });
    $(".content-wrapper").on("click", ".btn-edit-banner", function() {
         var id = $(this).parent().parent().data("id");
         $('#modal-form-purchase-banner').find("form").find(".alert").remove();
         $.post("{{ url('user/dashboard/get-banner-info') }}", { id: id, _token: $_token }, function(response) {
            $.each(response.row, function(index, value) {
              var field = $("#row-" + index);
                if(field.length > 0) {
                    field.val(value);
                }
                $("#modal-form-purchase-banner").find("#row-img").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+value+'?'+$.now());
                $("#modal-form-purchase-banner").find(".uploader-pane").addClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").attr("src","{{url('uploads/banner/advertisement/mobile').'/'}}"+response.row.image_mobile+'?'+$.now());
                $("#modal-form-purchase-banner").find("#row-img-desktop").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+response.row.image_desktop+'?'+$.now());
                $("#modal-form-purchase-banner").find("#change-photo-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#change-photo-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#preview").removeClass("hide");

                if(index == "id"){
                    $("#modal-form-purchase-banner").find("#row-id").val(value);
                }
                if(index=="amount"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val(value);
                    $("#modal-form-purchase-banner").find("#placement-price").val(value);
                }
                if(index=="metric_duration"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_duration").val(value);
                }
                if(response.page_location == "1"){
                    $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide");
                }else{
                    $("#modal-form-purchase-banner").find("#banner-category-pane").removeClass("hide");
                    $("#modal-form-purchase-banner").find("#ads-category").html(response.categoriesContainer);
                }
                $("#modal-form-purchase-banner").find("#row-title").val(response.row.title);
                $("#banner_price_pane").removeClass("hide");
                $("#modal-form-purchase-banner").find("#placement-price").html(response.banner_placement_price_container);
                $("#row-banner_placement_id").html(response.placementContainer);
                $('#modal-form-purchase-banner').find(".disable-view").removeAttr('disabled','disabled');
                $('#modal-form-purchase-banner').find(".alert").remove();
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").removeClass("hide");
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").text("Save");
                $('#modal-form-purchase-banner').find("#btnClose").addClass("hide");
                $('#modal-form-purchase-banner').find("#row-photo").removeAttr("required");
                $('#modal-form-purchase-banner').find("form").find("#btnClose").addClass("hide");
                $("#modal-form-purchase-banner").find("#row-status").val("");
                $("#modal-form-purchase-banner").modal("show");
            });
      }, 'json');

    });
    $(".content-wrapper").on("click", ".btn-view-banner", function() {
         var id = $(this).parent().parent().data("id");
         $('#modal-form-purchase-banner').find("form").find(".alert").remove();
         $.post("{{ url('user/dashboard/get-banner-info') }}", { id: id, _token: $_token }, function(response) {
            $.each(response.row, function(index, value) {
              var field = $("#row-" + index);
                if(field.length > 0) {
                    field.val(value);
                }
                $("#modal-form-purchase-banner").find("#row-img").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+value+'?'+$.now());
                $("#modal-form-purchase-banner").find(".uploader-pane").addClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-desktop").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-img-mobile").attr("src","{{url('uploads/banner/advertisement/mobile').'/'}}"+response.row.image_mobile+'?'+$.now());
                $("#modal-form-purchase-banner").find("#row-img-desktop").attr("src","{{url('uploads/banner/advertisement/desktop').'/'}}"+response.row.image_desktop+'?'+$.now());
                $("#modal-form-purchase-banner").find("#change-photo-desktop").addClass("hide");
                $("#modal-form-purchase-banner").find("#change-photo-mobile").addClass("hide");
                $("#modal-form-purchase-banner").find("#preview").removeClass("hide");

                if(index == "id"){
                    $("#modal-form-purchase-banner").find("#row-id").val(value);
                }
                if(index=="amount"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val(value);
                    $("#modal-form-purchase-banner").find("#placement-price").val(value);
                }
                if(index=="metric_duration"){
                    $("#modal-form-purchase-banner").find("#row-placement_actual_duration").val(value);
                }
                if(response.page_location == "1"){
                    $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide");
                }else{
                    $("#modal-form-purchase-banner").find("#banner-category-pane").removeClass("hide");
                    $("#modal-form-purchase-banner").find("#ads-category").html(response.categoriesContainer);
                }
                $("#modal-form-purchase-banner").find("#row-title").val(response.row.title);
                $("#banner_price_pane").removeClass("hide");
                $("#modal-form-purchase-banner").find("#placement-price").html(response.banner_placement_price_container);
                $("#row-banner_placement_id").html(response.placementContainer);
                $('#modal-form-purchase-banner').find("form").find(".disable-view").attr('disabled','disabled');
                $('#modal-form-purchase-banner').find(".alert").remove();
                $('#modal-form-purchase-banner').find("#btnPurchaseBanner").addClass("hide");
                $('#modal-form-purchase-banner').find("#btnClose").removeClass("hide");
                $("#modal-form-purchase-banner").find("#row-status").val("");
                $("#modal-form-purchase-banner").modal("show");

            });
      }, 'json');
    });
    $("#banner-advertisement-table").on("click", "#row-sort", function() {
          // $_token = $("#row-token").val();
          $('.loading-pane').removeClass('hide');
          $page = $("#row-page").val();
          $order = $("#row-order").val();
          $sort =  $(this).data('sort');
          $search = $("#row-search").val();
          $status = $("#row-filter_status").val();
          $country = $("#row-filter_country").val();
          $per = $("#row-per").val();
          $("#banner-advertisement-table").find("i").removeClass('fa-caret-up');
          $("#banner-advertisement-table").find("i").removeClass('fa-caret-down');
          // $(this).find("i").addClass('fa-caret-down');
          var loading = $(".loading-pane");
          var table = $("#rows");
           if($order == "asc"){
              $(this).find('i').addClass('fa-caret-up').removeClass('fa-caret-down');
           }else{
              $(this).find('i').addClass('fa-caret-down').removeClass('fa-caret-up');
           } 
          
       $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search,status:$status, per: $per, _token: $_token }, function(response) {
        
        var body = "";
        // clear table
        table.html("");
         $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '" class="'+(row.banner_status == 6 ? 'disabledColor':'')+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.order_no + '</small></td>' +
              '<td><small>'+ (row.banner_status == 1 ? 'Pending':'')+(row.banner_status == 2 ? 'Approved':'')+(row.banner_status == 3 ? 'Declined':'')+(row.banner_status == 4 ? 'Active':'')+(row.banner_status == 6 ? 'Disabled':'')+'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
//               '<td><small>'+ (row.category_name == null ? 'N/A': row.category_name) +'</small></td>'+
              '<td><small>'+ row.placement_name +'</small></td>'+
              '<td><small>'+ row.amount +'</small></td>'+
              '<td><small>'+ row.duration +' '+(row.metric_duration == "d" ? 'days':'')+(row.metric_duration == "m" ? 'months':'')+(row.metric_duration == "y" ? 'years':'')+'</small></td>'+
              '<td><small>'+ row.total_cost +'</small></td>'+
              '<td><small>'+ (row.reserve_status  == 2 ? 'Reserved':'Paid') +'</small></td>'+
              '<td class="rightalign">'+

                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view-banner"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit-banner"><i class="fa fa-pencil"></i></button>' +
                 (row.banner_status == 6 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>')+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-decline"><i class="fa fa-close"></i></button>'+
                 (row.banner_status  == 1 || row.banner_status == 6 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve"><i class="fa fa-check"></i></button>':'')+ 
              '</td></tr>';
        });
          table.html(body);
          $('#row-pages').html(response.pages);
          loading.addClass("hide");
         
          if(response.order == "asc"){
              $("#row-order").val("desc");
            
          }else{
              $('#row-order').val("asc");
          }
       
      }, 'json');
  

  });
  $(".content-wrapper").on('click', '.pagination a', function (event) {
      event.preventDefault();
      if ( $(this).attr('href') != '#' ) {

      $("html, body").animate({ scrollTop: 0 }, "fast");
      $("#row-page").val($(this).html());

      $('.loading-pane').removeClass('hide');
      $page = $("#row-page").val();
      $order = $("#row-order").val();
      $sort = $("#row-sort").val();
      $search = $("#row-search").val();
      $status = $("#row-filter_status").val();
      $per = $("#row-per").val();

      var loading = $(".loading-pane");
      var table = $("#rows");
      $.post("{{ $refresh_route }}", { page: $page, sort: $sort, order: $order, search: $search, status: $status, per: $per, _token: $_token }, function(response) {
        // clear
        table.html("");
        
        var body = "";
        $.each(response.rows.data, function(index, row) {

            body += '<tr data-id="' + row.id + '" class="'+(row.banner_status == 6 ? 'disabledColor':'')+(row.reserve_status == 2 ? 'reservedColor':'')+'">' + 
              '<td><small>'+ row.order_no + '</small></td>' +
              '<td><small>'+ (row.banner_status == 1 ? 'Pending':'')+(row.banner_status == 2 ? 'Approved':'')+(row.banner_status == 3 ? 'Declined':'')+(row.banner_status == 4 ? 'Active':'')+(row.banner_status == 6 ? 'Disabled':'')+'</small></td>'+
              '<td><small>'+ row.countryCode +'</small></td>'+
//               '<td><small>'+ (row.category_name == null ? 'N/A': row.category_name) +'</small></td>'+
              '<td><small>'+ row.placement_name +'</small></td>'+
              '<td><small>'+ row.amount +'</small></td>'+
              '<td><small>'+ row.duration +' '+(row.metric_duration == "d" ? 'days':'')+(row.metric_duration == "m" ? 'months':'')+(row.metric_duration == "y" ? 'years':'')+'</small></td>'+
              '<td><small>'+ row.total_cost +'</small></td>'+
              '<td><small>'+ (row.reserve_status  == 2 ? 'Reserved':'Paid') +'</small></td>'+
              '<td class="rightalign">'+

                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove"><i class="fa fa-trash-o"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view-banner"><i class="fa fa-eye"></i></button>' +
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit-banner"><i class="fa fa-pencil"></i></button>' +
                 (row.banner_status == 6 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable"><i class="fa fa-unlock"></i></button>':'<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable"><i class="fa fa-lock"></i></button>')+
                 '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-decline"><i class="fa fa-close"></i></button>'+
                 (row.banner_status  == 1 || row.banner_status == 6 ? '<button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve"><i class="fa fa-check"></i></button>':'')+ 
              '</td></tr>';
        });
        
        $("#modal-add-theme-country").find('form').trigger("reset");
        table.html(body);

        $('#row-pages').html(response.pages);
        loading.addClass("hide");

      }, 'json');

      }
  });
    $(".content-wrapper").on("click", ".btn-add", function() {
        var generated =Math.random().toString(36).substr(2, 9);
        $("#modal-form-purchase-banner").find("#change-photo-desktop").addClass("hide");
        $("#modal-form-purchase-banner").find("#change-photo-mobile").addClass("hide");
        $("#modal-form-purchase-banner").find("#row-img-desktop").addClass("hide");
        $("#modal-form-purchase-banner").find("#row-img-mobile").addClass("hide");
        $("#modal-form-purchase-banner").find("#row-img-label-desktop").addClass("hide");
        $("#modal-form-purchase-banner").find("#row-img-label-mobile").addClass("hide");
        $("#modal-form-purchase-banner").find(".uploader-pane").removeClass("hide");
        $('#modal-form-purchase-banner').find("form").find(".alert").remove();
        $("#modal-form-purchase-banner").find("form")[0].reset();
        $("#modal-form-purchase-banner").find(".alert").remove();
        $("#modal-form-purchase-banner").find("#row-placement_actual_amount").val('');  
        $("#modal-form-purchase-banner").find("#banner_price_pane").addClass("hide"); 
        $("#modal-form-purchase-banner").find("#banner-category-pane").addClass("hide"); 
        $("#modal-form-purchase-banner").find("#row-id").val("");
        $('#modal-form-purchase-banner').find("form").find(".disable-view").removeAttr('disabled','disabled');
        $('#modal-form-purchase-banner').find(".disable-view").removeAttr('disabled','disabled');
        $('#modal-form-purchase-banner').find("#btnPurchaseBanner").removeClass("hide");
        $('#modal-form-purchase-banner').find("#btnClose").addClass("hide");
        $('#modal-form-purchase-banner').find("#btnPurchaseBanner").text("Purchase");
        $("#modal-form-purchase-banner").find("#row-order_no").val(generated);
        $("#modal-form-purchase-banner").find("#row-status").val("7");
        $("#modal-form-purchase-banner").modal("show");

    });
    $("#modal-form-purchase-banner").on("change","#row-page",function(){
     var id = $("#row-page").val();
     if(id == 1){
        $("#banner-category-pane").addClass("hide");
     }else{
        $("#banner-category-pane").removeClass("hide");
     }
     var country_id = $("#row-country_id").val();
     var containerPlacement = $("#row-banner_placement_id");
 
     var containerCategory = $("#ads-category");
         containerPlacement.html(" ");
         containerCategory.html(" ");
       if ($(this).val()) {
          $.post("{{ url('get-banner-placements') }}", { id: id,country_id:country_id, _token: $_token }, function(response) {
            containerPlacement.append(response.banner_placement_container);
            containerCategory.append(response.categories);
          }, 'json');
      }
    });
    $("#modal-form-purchase-banner").on("change","#row-banner_placement_id",function(){
        var id = $("#row-page").val();
        var containerPlacementPrice = $("#placement-price");
          containerPlacementPrice.html("");
           if ($(this).val()) {
              $.post("{{ url('get-banner-placements-price') }}", { id: id, _token: $_token }, function(response) {
                containerPlacementPrice.append(response.banner_placement_price_container);
                $("#banner_price_pane").removeClass("hide");
              }, 'json');
          }
    });
    $(".btn-change").click(function(){
      $("#row-photo").addClass("hide");
      $(".uploader-pane").removeClass("hide");
      $(this).addClass("hide");
      $(".btn-cancel").removeClass("hide");
       
    });
    $(".btn-cancel").click(function(){
      $("#row-photo").removeClass("hide");
      $(".uploader-pane").addClass("hide");
      $(this).addClass("hide");
      $(".btn-change").removeClass("hide");
    });
   $("#row-placement").on("change", function() { 
        var id = $("#row-placement").val();
        $_token = "{{ csrf_token() }}";
        var banner_placement_plan   = $("#row-placement_plan");
        banner_placement_plan.html(" ");
        if ($("#row-placement").val()) {
          $.post("{{ url('admin/get-banner-plans') }}", { id: id, _token: $_token }, function(response) {
            banner_placement_plan.append(response);
          }, 'json');
       }
  });
  $("#modal-form-purchase-banner").on("click", "#change-photo-mobile", function() {
        $(this).parent().parent().find(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
    });
    $("#modal-form-purchase-banner").on("click", "#change-photo-desktop", function() {
        $(this).parent().parent().find(".uploader-pane").removeClass("hide");
        $(this).addClass("hide");
    });
    $("#modal-form-purchase-banner").on("change", "#row-duration", function() {

        var duration = $(this).val();
        var amount = $("#placement-price").val();
        var price = $(".placement_amount").val();
        var total_amount = 0;
        var duration_metric = $(".placement_amount").find(':selected').data('metric');
        $("#row-placement_actual_amount").val(price);
        $("#row-placement_actual_duration").val(duration_metric);
        total_amount = parseFloat(duration) * parseFloat(amount);
        $("#row-total_cost").val(total_amount);

    });
    $("#modal-form-purchase-banner").on("change", ".placement_amount", function() {

       var price = $(this).val();
       var duration_metric = $(this).find(':selected').data('metric');
       var duration = $("#row-duration").val();
       var total_amount = 0;
       $("#row-placement_actual_amount").val(price);
       $("#row-placement_actual_duration").val(duration_metric);
      
       total_amount = parseFloat(duration) * parseFloat(price);
       $("#row-total_cost").val(total_amount);

    });
     function readURL(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
       
        reader.onload = function (e) {
            if(id =="desktop"){
               $("#row-img-desktop").attr('src', e.target.result);
               $("#row-img-desktop").removeClass('hide');
              
            }else{
               $("#row-img-mobile").attr('src', e.target.result);
               $("#row-img-mobile").removeClass('hide');
              
            }
        }

        reader.readAsDataURL(input.files[0]);
    }
}

  $("#modal-form-purchase-banner").on("change", ".photoName", function() {
    var id = $(this).data('id');
    readURL(this, id);
     var filenameDesktop = "";
     var filenameMobile = "";
     if(id =="desktop"){
        filenameDesktop = $(this).val().split("\\").pop();
        $("#row-file_name_desktop").val(filenameDesktop);
     }else{
         filenameMobile = $(this).val().split("\\").pop();
        $("#row-file_name_mobile").val(filenameMobile);
     }
  });

  $(".uploader-pane").on("click", ".removeImage", function() { 
      var id = $(this).data('id');
      if(id=="desktop"){
        $("#row-img-desktop").removeAttr("src");
        $("#row-img-desktop").addClass("hide");
        $("#row-file_name_desktop").val("");
      }else{
        $("#row-img-mobile").removeAttr("src");
        $("#row-img-mobile").addClass("hide");
        $("#row-file_name_mobile").val("");
      }
  });   
  $(".content-wrapper").on("click", ".btn-remove", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Remove Banner', 'Are you sure you want to remove this banner</strong>?', "{{ url('admin/banner/remove') }}",id);
  });

  $(".content-wrapper").on("click", ".btn-approve", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Approve Banner', 'Are you sure you want to approve this banner</strong>?', "{{ url('admin/banner/approve') }}",id);
  });
  $(".content-wrapper").on("click", ".btn-decline", function() {
      var id = $(this).parent().parent().data('id');
//       $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
//       dialog('Decline Banner', 'Are you sure you want to decline this banner</strong>?', "{{ url('admin/banner/decline')}}",id);
       $('#banner_id').val(id);
       $('#modal-reject').modal('show');
  });
  $(".content-wrapper").on("click", ".btn-reserve", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Reserve Banner', 'Are you sure you want to reserve this banner</strong>?', "{{ url('admin/banner/reserve')}}",id);
  });
  $(".content-wrapper").on("click", ".btn-enable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Enable Banner', 'Are you sure you want to enable this banner</strong>?', "{{ url('admin/banner/enable')}}",id);
  });
  $(".content-wrapper").on("click", ".btn-disable", function() {
      var id = $(this).parent().parent().data('id');
      $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      dialog('Disable Banner', 'Are you sure you want to disable this banner</strong>?', "{{ url('admin/banner/disable')}}",id);
  });
  
  $('#reject-banner').submit(function(e) {
    e.preventDefault();
    $.post($(this).attr('action'), {id: $('#banner_id').val(), reason: $('#reason').val(), _token: $_token}, function(response) {
      if (response.code == 1) {
        status('Success', 'Banner rejected', 'alert-success');
      } else {
        status('Error', 'Error', 'alert-success');
      }
      refresh();
      $('#modal-reject').modal('hide');
    });
  });
  
</script>
@stop
@section('content')
<div class="content-wrapper">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <h3><i class="fa fa-book"></i> {{$title}}</h3>
      </div>
      <div class="col-lg-6 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-success btn-md btn-attr pull-right" onclick="refresh()"><i class="fa fa-refresh"></i> Refresh</button>   
        <!-- <button type="button" class="btn btn-danger btn-md btn-attr pull-right" data-toggle="modal" data-target="#"><i class="fa fa-trash-o"></i> Remove</button>  -->
        <button type="button" class="btn btn-info btn-md btn-attr pull-right btn-add"><i class="fa fa-book"></i> Add</button>    
      </div>
    </div>
    <div class="col-lg-12 col-sm-12 col-xs-12 pull-right paddingAll">
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="input-group borderZero">
            <span class="input-group-addon borderZero" id="basic-addon1">Search</span>
            <input type="text" class="form-control borderZero" id="row-search">
            </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Country</span>
                      <select class="form-control borderZero" id="row-filter_country" onchange="refresh()">
                                <option value="">All</option>
                               @foreach($countries as $row)
                                  <option value="{{$row->id}}">{{$row->countryName}} [{{$row->countryCode}}]</option>
                               @endforeach
                      </select>
                  </div>
             </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                  <div class="input-group borderZero">
                  <span class="input-group-addon borderZero" id="basic-addon1">Type</span>
                      <select class="form-control borderZero" id="row-filter_banner_status" onchange="refresh()">
                                <option value="">All</option>
                                <option value="1">Paid</option>
                                <option value="2">Reserved</option>
                      </select>
                  </div>
             </div>
        </div>
    <div class="table-responsive topPaddingC col-xs-12">
      <div class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
      <table class="table table-condensed noMarginBottom backend-generic-table" id="banner-advertisement-table">
            <thead>
                <tr>

                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="order_no" id="row-sort"><i class="fa"></i> Transaction No</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_status" id="row-sort"><i class="fa"></i> Status</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.countryCode" id="row-sort"><i class="fa"></i> Country Code</div></th>
<!--                   <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="countries.countryCode" id="row-sort"><i class="fa"></i> City</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="categories.name" id="row-sort"><i class="fa"></i> Category</div></th> -->
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_placement_location.name" id="row-sort"><i class="fa"></i> Placement</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="amount" id="row-sort"><i class="fa"></i> Placement Cost</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_advertisement_subscriptions.duration" id="row-sort"><i class="fa"></i> Duration</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="banner_advertisement_subscriptions.total_cost" id="row-sort"><i class="fa"></i> Total</div></th>
                  <th><div data-toggle="tooltip" title="Click to Sort" data-placement="right" data-sort="reserve_status" id="row-sort"><i class="fa"></i> Type</div></th>
                  <th class="text-right"><small>Tools</small></th>
                </tr>
              </thead>
            <tbody id="rows">
           @foreach($rows as $row)
            <tr data-id="{{$row->id}}" class="{{$row->banner_status == 6 ? 'disabledColor':''}}{{$row->reserve_status == 2 ? 'reservedColor':''}}">
                <td><small>{{$row->order_no}}</small></td>
                <td><small>{{$row->banner_status==1 ? 'Pending':'' }}{{$row->banner_status==2 ? 'Approved':'' }}{{$row->banner_status==3 ? 'Declined':'' }}{{$row->banner_status==4 ? 'Active':'' }}{{$row->banner_status==6 ? 'Disabled':'' }}</small></td>
                <td><small>{{$row->countryCode}}</small></td>
<!--                 <td><small>{{$row->category_name == null ? 'N/A': $row->category_name}}</small></td> -->
<!--                 <td><small>{{$row->category_name == null ? 'N/A': $row->category_name}}</small></td> -->
                <td><small>{{$row->placement_name}}</small></td>
                <td><small>{{$row->amount}}</small></td>
                <td><small>{{$row->duration}} {{ $row->metric_duration == "d" ? 'days':''}}{{ $row->metric_duration == "m" ? 'months':''}}{{ $row->metric_duration == "y" ? 'years':''}}</small></td>
                <td><small>{{$row->total_cost}}</small></td>
                <td><small>{{$row->reserve_status == 2 ? 'Reserved':'Paid'}}</small></td>
                <td>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-remove"><i class="fa fa-trash-o"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-view-banner"><i class="fa fa-eye"></i></button>
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-edit-banner"><i class="fa fa-pencil"></i></button>

                @if($row->banner_status == 6)
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-enable" {{$row->banner_status == 3 ? 'disabled':''}}><i class="fa fa-unlock"></i></button>
                @else
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-disable" {{$row->banner_status == 3 ? 'disabled':''}} {{$row->banner_status == 1 ? 'disabled':''}}><i class="fa fa-lock"></i></button>
                @endif

                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-decline" {{$row->banner_status == 3 ? 'disabled':''}} {{$row->banner_status == 6 ? 'disabled':''}}{{$row->banner_status == 2 ? 'disabled':''}}><i class="fa fa-close"></i></button>
                @if($row->banner_status == 1)
                    <button type="button" class="btn btn-default btn-sm btn-attr pull-right btn-approve" {{$row->banner_status == 6 ? 'disabled':''}}><i class="fa fa-check"></i></button>
                @endif
                    
                </td> 
            </tr>
           @endforeach
          </tbody>
       </table>
     </div>
     <input type="hidden" id="row-page" value="1">
     <input type="hidden" id="row-order" value="">
       <div class="row marginFooter">
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="row-pages">
          {!!$pages!!}
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="padding-top:18px">
          <select class="form-control {{count($rows) < 10 ? 'hide':''}} borderZero" id="row-per" onchange="maxAds()">
              <option value="10">10</option>
              <option value="20">20</option>
              <option value="30">30</option>
              <option value="100">100</option>
              <option value="200">200</option>
          </select>
        </div>
      </div>
  </div>
</div>
@stop
@include('admin.banner-management.form')