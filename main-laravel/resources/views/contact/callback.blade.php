@extends('layout.frontend')
@section('scripts')
<?php
session_start();
    $accesstoken = '';
    $client_id = env('GMAIL_CLIENT_ID');
    $client_secret = env('GMAIL_SECRET_KEY');
    $redirect_uri = url('referral/callback');
    $simple_api_key = 'AIzaSyC4IVTUetKHLYjv7nMfUU7rH-0lIaawIj4';
    $max_results = 500;
    $auth_code = $_GET["code"];
    function curl_file_get_contents($url) {
        $curl = curl_init();
        $userAgent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; .NET CLR 1.1.4322)';
        curl_setopt($curl, CURLOPT_URL, $url);   //The URL to fetch. This can also be set when initializing a session with curl_init().
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);    //TRUE to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);   //The number of seconds to wait while trying to connect.    
        curl_setopt($curl, CURLOPT_USERAGENT, $userAgent); //The contents of the "User-Agent: " header to be used in a HTTP request.
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, TRUE);  //To follow any "Location: " header that the server sends as part of the HTTP header.
        curl_setopt($curl, CURLOPT_AUTOREFERER, TRUE); //To automatically set the Referer: field in requests where it follows a Location: redirect.
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);   //The maximum number of seconds to allow cURL functions to execute.
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); //To stop cURL from verifying the peer's certificate.
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $contents = curl_exec($curl);
        curl_close($curl);
        return $contents;
        }

        $fields = array(
        'code' => urlencode($auth_code),
        'client_id' => urlencode($client_id),
        'client_secret' => urlencode($client_secret),
        'redirect_uri' => urlencode($redirect_uri),
        'grant_type' => urlencode('authorization_code')
        );
        $post = '';
        foreach ($fields as $key => $value) {
                 $post .= $key . '=' . $value . '&';
        }
         $post = rtrim($post, '&');

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
        curl_setopt($curl, CURLOPT_POST, 5);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $result = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($result);
        if (isset($response->access_token)) {
            $accesstoken = $response->access_token;
            $_SESSION['access_token'] = $response->access_token;
        }
        if (isset($_GET['code'])) {
          $accesstoken = $_SESSION['access_token'];
        }
        if (isset($_REQUEST['logout'])) {
            unset($_SESSION['access_token']);
        }
        $url = 'https://www.google.com/m8/feeds/contacts/default/full?max-results=' . $max_results . '&oauth_token=' . $accesstoken;
        $xmlresponse = curl_file_get_contents($url);

        if ((strlen(stristr($xmlresponse, 'Authorization required')) > 0) && (strlen(stristr($xmlresponse, 'Error ')) > 0)) {
            echo "<h2>OOPS !! Something went wrong. Please try reloading the page.</h2>";
            exit();
        }
        $xml = new SimpleXMLElement($xmlresponse);
        $xml->registerXPathNamespace('gd', 'http://schemas.google.com/g/2005/Atom');

        $result = $xml->xpath('//gd:email');

  ?>
<script type="text/javascript">
   $(".btn-import").on("click", function() { 
   $(".btn-import").addClass("hide");
   $(".btn-back").removeClass("hide");
  });

</script>
@stop
@section('content')
<div data-spy="scroll" data-target=".navbar" data-offset="50" id="content-wrapper">
<div class="container-fluid">
  <div class="container bannerJumbutron nobottomMargin">

</div>
<div class="container-fluid bgWhite">
  <div class="container">
        <div class="col-sm-12 noPadding">
    <div class="borderZero">
    <h1 class="blueText text-center">Save Contact List
        <hr>
    </h1> 
  </div>
</div>
</div>
</div>
<div class="container-fluid bgWhite">
    <div class="container">
   </div>
</div>


<div class="text-center" id="form-import_contact" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
      {!! Form::open(array('url' => 'save/import', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-save_import', 'files' => true)) !!}
      <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <div class="">
        <div id="form-import_contact_notice"></div>

        <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <select name="email[]" id="example-filterBehavior" multiple="multiple" class="form-control input-md borderzero  multiselect" style="width: 100%;display: inline !important;">
                  @foreach ($result as $title) 
               <option value="{{$title->attributes()->address}}" selected>{{$title->attributes()->address}}</option>
                @endforeach
              </select>
        </div>
        </div>
      </div>
        <button type="submit" class="btn blueButton borderZero noMargin btn-import">Save Contacts</button>
      {!! Form::close() !!}
        <a href="{{url('/').'/dashboard'}}"><button type="submit" class="hide btn redButton borderZero noMargin btn-back">Back to Dashboad</button></a>
    </div>
  </div>
</div>


<div class="container-fluid bgWhite">
  <div class="container">
        <div class="col-sm-12 noPadding">
    <div class="borderZero">
    <h1 class="blueText text-center">
        <br>
        <br>
        <br>
        <br>
        <br>
   
    </h1> 
  </div>
</div>
</div>
</div>
</div>
<div class="container-fluid bgGray topPaddingC">

</div>
</div>
</div>
@stop



