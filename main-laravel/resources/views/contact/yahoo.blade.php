@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
   $(".btn-import").on("click", function() { 
   $(".btn-import").addClass("hide");
   $(".btn-back").removeClass("hide");
  });

</script>
@stop
@section('content')
<div data-spy="scroll" data-target=".navbar" data-offset="50" id="content-wrapper">
<div class="container-fluid">
  <div class="container bannerJumbutron nobottomMargin">

</div>
<div class="container-fluid bgWhite">
  <div class="container">
        <div class="col-sm-12 noPadding">
    <div class="borderZero">
    <h1 class="blueText text-center">Save Contact List
        <hr>
    </h1> 
  </div>
</div>
</div>
</div>
<div class="container-fluid bgWhite">
    <div class="container">
   </div>
</div>


<div class="text-center" id="form-import_contact" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
      {!! Form::open(array('url' => 'save/import', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'form-save_import', 'files' => true)) !!}
      <div id="load-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      <div class="">
        <div id="form-import_contact_notice"></div>

        <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <select name="email[]" id="example-filterBehavior" multiple="multiple" class="form-control input-md borderzero  multiselect" style="width: 100%;display: inline !important;">
                  @foreach ((array) $result as $email) 
               <option value="{{$email}}" selected>{{$email}}</option>
                @endforeach
              </select>
        </div>
        </div>
      </div>
        <button type="submit" class="btn blueButton borderZero noMargin btn-import">Save Contacts</button>
      {!! Form::close() !!}
        <a href="{{url('/').'/dashboard'}}"><button type="submit" class="hide btn redButton borderZero noMargin btn-back">Back to Dashboad</button></a>
    </div>
  </div>
</div>


<div class="container-fluid bgWhite">
  <div class="container">
        <div class="col-sm-12 noPadding">
    <div class="borderZero">
    <h1 class="blueText text-center">
        <br>
        <br>
        <br>
        <br>
        <br>
   
    </h1> 
  </div>
</div>
</div>
</div>
</div>
<div class="container-fluid bgGray topPaddingC">

</div>
</div>
</div>
@stop