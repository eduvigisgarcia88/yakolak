

<div class="modal fade" id="modal-plan-subscription" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'subscription/upgrade', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_subscription_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Upgrade Plan</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to Upgrade your Subscription? 
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="user_id" id="row-id" value="">
				<input type="hidden" name="plan_id" id="row-plan_id" value="">
				<button type="submit" class="btn btn-success borderZero modal-plan-subscription" name="months_subscribed" id="row-months" value="6"><i class="fa fa-check"></i> 6 MONTHS</button>
				<button type="submit" class="btn btn-success borderZero modal-plan-subscription" name="months_subscribed" id="row-months" value="12"><i class="fa fa-check"></i> 12 MONTHS</button>
				
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>


