@extends('layout.frontend')
@section('scripts')
@stop

@section('content')
<div class="modal fade" id="btn-login" role="dialog">
  <div class="modal-dialog modal-md">
    <div class="modal-content borderzero">
      <div class="modal-header modal-warning">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Message</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal push-5-t" action="{{ url('/login') }}" method="post">
             <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="form-group">
          <label class="col-xs-12" for="register4-email">Email</label>
          <div class="col-xs-12">
          <div class="input-group">
          <input class="form-control borderzero" type="email" id="row-email" name="email" placeholder="Enter your email..">
          <span class="input-group-addon borderzero"><i class="fa fa-envelope-o"></i></span>
          </div>
          </div>
          </div>
          <div class="form-group">
          <label class="col-xs-12" for="register4-password">Password</label>
          <div class="col-xs-12">
          <div class="input-group">
          <input class="form-control borderzero" type="password" id="register4-password" name="password" placeholder="Enter your password..">
          <span class="input-group-addon borderzero"><i class="fa fa-asterisk"></i></span>
          </div>
          </div>
          </div>
         
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default borderzero" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-warning borderzero">Login</button>

        </div>
      {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>

<div class="header navbar-fixed-top">
  <div class="container">
    <div class="row">
      <div class="col-xs-4 col-sm-2 col-md-3 col-lg-3 logoPaddingTop">
       <a href="{{ url('/') }}"> <img id="logo" class="img-responsive" src="../img/yakolak.png" alt="Chania"></a>
      </div>
      <div class="col-xs-8 visible-xs">
        <button class=" pull-right btn btn-yak borderzero" data-toggle="offcanvas" type="button"><i class="fa fa-bars"></i></button> 
        <button type="button" class=" pull-right btn btn-yak borderzero dropdown-toggle" data-toggle="dropdown"><i class="fa fa-circle-o-notch"></i></button>
        <ul class="dropdown-menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
    <div class="col-sm-10 col-md-7 col-lg-7 col-md-offset-2 col-lg-offset-2 hidden-xs">
      <div class="row col-lg-12 pull-right">
      <div class="col-sm-10 col-md-10 col-lg-10">
        <div class="row">
        <div class="input-group">
        <input type="text" class="form-control borderzero" placeholder="Search for...">
        <span class="input-group-btn">
        <button class="btn btn-primary borderzero" type="button"><i class="fa fa-search"></i> Search</button>
        </span>
        </div><!-- /input-group -->
        </div>
      </div>
      <div class="row pull-right col-sm-2 col-md-2 col-lg-2">
        <button class="btn btn-danger borderzero pull-right" type="button">Post Ads</button>
      </div>
    </div>
      <div class="col-sm-12 col-md-12 pull-right" style="padding-top: 5px; ">
        <div class="row">
          <ul class="pull-right nav navbar-nav">
            <li class="active"><a href="#">Buy <span class="sr-only">(current)</span></a></li>
            <li><a href="#">Sell</a></li>
            <li><a href="#">Bid</a></li>
            <li><a href="#">Categories</a></li>
            <li><a href="#">Plans</a></li>
            <li><a href="#">Support</a></li>
            <li class="divider-vertical"></li>
            @if (Auth::guest())
            <li><a href="" class="link" data-toggle="modal" data-target="#btn-login">Login</a></li>
            @else
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->name }} <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
            <li><a href="{{ url('admin') }}">Dashboard</a></li>
            <li><a href="{{ url('#') }}">Profile</a></li>
            <li><a href="{{ url('#') }}">Settigs</a></li>
            <li><a href="{{ url('/logout') }}">Logout</a></li>
            </ul>
            </li>
            @endif
            <li class="divider-vertical"></li>
            <li>
            <select class="form-control languageSelection borderzero">
            <option>EN</option>
            <option>DE</option>
            <option>FR</option>
            <option>IT</option>
            </select>
            </li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  </div>
</div>
  
<div class="row-offcanvas row-offcanvas-left">


<section id="midBody" class="col-lg-12">
<div class="row">
<div class="container">

<br>
<br>
<div class="form-register">
<div class="panel panel-default borderzero">
<div class="panel-heading form-signin-heading">Sign Up</div>
<div class="panel-body">
{!! Form::open(array('url'=>'user/register','method'=>'POST', 'id'=>'myform')) !!}
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
<div class="form-group">
<label class="col-xs-12" for="register4-account">Name</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="text" id="register4-account" name="account" placeholder="Company's name..">
</div>
</div>
</div>
<div class="form-group">
<label class="col-xs-12" for="register4-account">Company</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="text" id="register4-account" name="account" placeholder="Company's name..">
<span class="input-group-addon borderzero"><i class="fa fa-cog"></i></span>
</div>
</div>
</div>
<div class="form-group">
<label class="col-xs-12" for="register4-email">Email</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="email" id="register4-email" name="email" value="{{ old('email') }}" placeholder="Enter your email..">
<span class="input-group-addon borderzero"><i class="fa fa-envelope-o"></i></span>
</div>
</div>
</div>
<div class="form-group">
<label class="col-xs-12" for="register4-password">Password</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="password" id="register4-password" name="password" placeholder="Enter your password..">
<span class="input-group-addon borderzero"><i class="fa fa-asterisk"></i></span>
</div>
</div>
</div>
<div class="form-group">
<label class="col-xs-12" for="register4-password2">Confirm Password</label>
<div class="col-xs-12">
<div class="input-group">
<input class="form-control borderzero" type="password" id="register4-password2" name="password_confirmation" placeholder="Enter your password..">
<span class="input-group-addon borderzero"><i class="fa fa-asterisk"></i></span>
</div>
</div>
</div>

<div class="form-group">
<div class="col-xs-12">
<input class="btn btn-sm btn-success borderzero" type="submit" value="submit">
</div>
</div>
</form>
</div>
</div>
    </div>
    </div>
<br>

</div>
</div>
</section>

<section id="listing">
  <div class="container">
  <div class="col-xs-6 col-lg-3">
    <h4>Ad Listing Statistic</h4>
    <hr>
    <p>New Listing in last 24 hours</p>
    <h4 class="lbl">48,670</h4>
    <p>No. of comments</p>
    <h4 class="lbl">10,001</h4>
    <p>Premium listings</p>
    <h4 class="lbl">300</h4>
    <p>Total Ad Listings</p>
    <h4 class="lbl">48,670</h4>
  </div>
  <div class="col-xs-6 col-lg-3">
    <h4>Bid Listing Statistic</h4>
    <hr>
    <p>New Bidding in last 24 hours</p>
    <h4 class="lbl">48,670</h4>
    <p>Biddings placed</p>
    <h4 class="lbl">10,001</h4>
    <p>Finished Bidding</p>
    <h4 class="lbl">300</h4>
    <p>Total Bidding</p>
    <h4 class="lbl">48,670</h4>
  </div>
  <div class="col-xs-6 col-lg-3">
    <h4>Top Members</h4>
    <hr>
    <p>New Listing in last 24 hours</p>
    <h4 class="lbl">48,670</h4>
    <p>No. of comments</p>
    <h4 class="lbl">10,001</h4>
    <p>Premium listings</p>
    <h4 class="lbl">300</h4>
    <p>Total Ad Listings</p>
    <h4 class="lbl">48,670</h4>
  </div>
  <div class="col-xs-6 col-lg-3">
    <h4>Top Reviews</h4>
    <hr>
    <p>New Listing in last 24 hours</p>
    <h4 class="lbl">48,670</h4>
    <p>No. of comments</p>
    <h4 class="lbl">10,001</h4>
    <p>Premium listings</p>
    <h4 class="lbl">300</h4>
    <p>Total Ad Listings</p>
    <h4 class="lbl">48,670</h4>
  </div>
</div>
  <div class="container">
    <hr>
  <div class="col-xs-6 col-lg-4">
    <h4>CONTACT INFORMATION</h4>
    <p>Facebook</p>
    <p>Twitter</p>
    <p>Google Plus</p>
    <br>
    <h4>OTHER URLS</h4>
    <p>PLAN PRICES</p>
    <p>FAQ</p>
    <p>SUPPORT PAGE</p>
  </div>
  <div class="col-xs-6 col-lg-4">
    <h4>WANT TO SELL?</h4>
    <p>Thank you for showing your interest in listing your Ads at yakolak.com</p>
    <p>To quickly add an Ad, please click the button below.</p>
    <button type="button" class="btn btn-sm btn-danger borderzero">LIST A AD FOR FREE</button>
    <br>
    <br>
    <p>Want to do this offline? To list your deal, please send us your deal's information and pictures along with the details to listing@akolak.com</p>
  </div>
  <div class="col-xs-12 col-lg-4">
    <h4>SUBSCRIBE TO NEWSLETTER</h4>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="usr" placeholder="EMAIL ADDRESS">
    </div>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="usr" placeholder="FIRST NAME">
    </div>
    <div class="form-group">
      <input type="text" class="form-control borderzero" id="usr" placeholder="LAST NAME">
    </div>
    <div class="form-group">
      <button type="button" class="btn btn-sm btn-primary borderzero">SUBSCRIBE</button>
    </div>
  </div>
  </div>
</section>

<section id="footer">
<footer class="container">
  <p><span class="pull-left"><button type="button" class="btn btn-xs borderzero btn-facebook"><i class="fa fa-facebook-official"></i> Like</button><button type="button" class="btn-facebook btn btn-xs borderzero">Share</button></span><span class="pull-right"><i class="fa fa-copyright"></i> 2015 yakolak.com. All rights reserved</span></p>
</footer>
</section>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('[data-toggle=offcanvas]').click(function() {
    $('#cssmenu').toggleClass('active');
  });
});
</script>
</body>
</html>
@stop
