@extends('layout.frontend')
@section('scripts')
<script>
 $_token = "{{ csrf_token() }}";
$(function () {

  @foreach($adslatestbids as $row)
    $("#ads_latest_bids_{{$row->id}}").rateYo({
         starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbids_panel2 as $row)
    $("#ads_latest_bids_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads as $row)
    $("#latest_ads_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads_panel2 as $row)
    $("#latest_ads_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbidsmobile as $row)
    $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($latestadsmobile as $row)
    $("#latest_ads_mobile_{{$row->id}}").rateYo({
         starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbidsmobile as $row)
    $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
         starWidth: "12px",
         rating: {{ ($row->average_rate != null ? $row->average_rate:0) }},
         ratedFill: "#5da4ec",
         readOnly: true,
    });
  @endforeach
});

 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   var ads_id = $(this).data('ads_id');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('D: %D - H: %H - M: %M'));
   });

 });


 $('#flyout_menu').each(function() {
            $.get("{{ url('user/plan/expiring') }}", { _token: $_token}, function(response) {
                append(response);   
    }, 'json');

 });
  $('#flyout_menu').each(function() {
            $.get("{{ url('user/plan/expired') }}", { _token: $_token}, function(response) {
                append(response);   
    }, 'json');

 });
   @if(Auth::check())
            $("#latest-ads-wrapper").on("click", "#btn-user-view", function() {
                var ad_id = $(this).data("id"); 
                var user_id = "{{Auth::user()->id}}";
                var post_url = "{{ url('user/save/ad/view') }}";
                $.post(post_url, { user_id: user_id,ad_id:ad_id,_token: $_token}, function(response) { 
                }, 'json'); 
             });
    @endif


</script>
@stop
@section('content')
<!-- main page start -->
<div class="container-fluid noPadding bgWhite borderBottom">
<div class="container bannerJumbutron">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
  <div class="col-lg-3 col-md-3 col-md-3 hidden-xs hidden-sm">
    <h4 class="listTitle leftPaddingB">CATEGORIES</h4>
<div id='flyout_menu'>
<ul class="main">
  @foreach($categories as $row)
    <li class=""><a href="{{url('category/list').'/'.$row->id}}" class="grayText">{{$row->name}}<i class="fa fa-angle-right pull-right"></i></a></li>
  @endforeach
    <li class=""><a href="{{url('category/list')}}" class="redText">All Categories <i class="fa fa-angle-right pull-right"></i></a></li>
  <!-- <li class=""><a href="{{url('category/list')}}" class="grayText">Live Auctions <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Cars and Vehicles <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Clothing <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Events <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Jobs <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Real Estate <i class="fa fa-angle-right pull-right"></i></a>
  <li class=""><a href="{{url('category/list')}}" class="grayText">Services <i class="fa fa-angle-right pull-right"></i></a>-->
  
</ul>
</div>
<!--     <div id="sidebar">
    <ul class="nav nav-pills nav-stacked">
      <li class=""><a href="#" class="listCategory">Buy and Sell <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="subMenu">
        <li><a href="#" class="">Page 1-1</a></li>
        <li><a href="#" class="listCategory whiteText">Page 1-2</a></li>
        <li><a href="#" class="listCategory whiteText">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Bidding <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Cars and Vehicles <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Clothings <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Events <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Jobs <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Real Estate <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategory">Services <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
      <li class="dropdown"><a href="#" class="listCategoryAll textRed">All Categories <i class="fa fa-angle-right pull-right"></i></a>
      <ul class="dropdown-menu subMenu">
        <li><a href="#" class="listCategory">Page 1-1</a></li>
        <li><a href="#" class="listCategory">Page 1-2</a></li>
        <li><a href="#" class="listCategory">Page 1-3</a></li> 
      </ul>
      </li>
    </ul>
  </div> -->
  </div>
  
  <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 noPadding">
    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 norightPadding rightPaddingSm bottomPadding">
      <div id="carouselMain" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMain" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMain" data-slide-to="1"></li>
    <li data-target="#carouselMain" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainA.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainB.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    <div class="item">
      <div class="fill" style="background: url({{ url('img').'/'}}Banner-MainC.jpg); background-repeat: no-repeat; background-size: cover; height: 340px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        advertising
      </div> -->
    </div>
  </div>

  <!-- Controls -->
  <!-- <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
     <!--  <a>
        <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; height: 400px; width: auto;">
        </div>
      </a> -->
    </div>

    
    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 noPadding topPaddingSm">
      <div class="col-sm-6  col-md-12 col-sm-6 col-xs-12 bottomPadding ">
<div id="carouselMiniA" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMiniA" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMiniA" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniA-1.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniA-2.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
  </div>

  <!-- Controls -->
 <!--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
        <!-- <div class="" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;"> -->
        </div>
      <div class="col-sm-6 col-md-12 col-sm-6 col-xs-12 notoppaddingSm topPadding">
        <div id="carouselMiniB" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMiniB" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMiniB" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniB-1.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
       <div class="" style="background: url({{ url('img').'/'}}Banner-MiniB-2.jpg); background-repeat: no-repeat; background-size: cover; height: 165px; width: auto;">
        </div>
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
  </div>

  <!-- Controls -->
<!--   <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
       <!--  <div class="" style="background: url({{ url('img').'/'}}6.jpg); background-repeat: no-repeat; background-size: cover; height: 200px; width: auto;"> -->
        </div>
      </div>
      <div class="clearfix visible-md-block visible-lg-block"></div>
    </div>
  </div>
</div>
</div>
<!-- main page end -->

<!-- latest bids start -->
<div class="container-fluid bgGray">
<div class="container blockMargin noPadding">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
      <span class="pull-right"><a href="{{url('category/list')}}" class="redText">View More</a></span>
    </div>

    
    <div class="col-sm-12 col-xs-12 blockBottom noPadding">
    <div class="panel-body noPadding">
      @foreach ($adslatestbidsmobile as $row)
      <div class="panel panel-default removeBorder borderZero bottomMarginLight">
        <div class="panel-body noPadding">
         <a href="{{ url('/ads/view') . '/' . $row->id }}"> <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div></a>

        <div class="rightPadding nobottomPadding lineHeight">
          <div class="mediumText noPadding topPadding bottomPadding">
                <div class="visible-sm"><?php echo substr($row->title, 0, 37); 
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
                <div class="visible-xs"><?php echo substr($row->title, 0, 17);
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
          <div class="normalText redText bottomPadding">
                 <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
          </div>
          </div>
          <div class="normalText redText bottomPadding">
          <div data-countdown="{{$row->ad_expiration}}"></div>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="mediumText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <div>
              @if($row->average_rate != null)
               <span id="ads_latest_bids_mobile_{{$row->id}}" class="blueText rightMargin pull-left"></span>
               <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
             @else
               <span class="blueText rightMargin">No Rating</span>
               <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
              @endif 
           <!--  <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span> -->
          </div>
        </div>
        </div>
      </div>
      @endforeach
    </div>
    </div>
  </div>
</div>

<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs" >
    <div class="panel panel-default  removeBorder borderZero borderBottom">
      <div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
        <span class="pull-right"><a href="{{url('category/list')}}" class="redText">View More</a></span>
      </div>
      <div class="panel-body">
        <div id="bidsCarousel" class="carousel slide carousel-fade" data-ride="carousel">


          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">
        
            <div class="item active">
            <div class="col-lg-12 col-md-12">
              @foreach ($adslatestbids as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight">
                <div class="sideMargin borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                    </div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right">
                           @if($row->average_rate != null)
                             <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                           @else
                             <span class="blueText rightMargin">No Rating</span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                           @endif 
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">       
                  <div data-countdown="{{$row->ad_expiration}}" data-currenttime="{{ $current_time}}" data-ads_id="{{ $row->get_ads_id }}"></div>
                
                </div>
              </div>
                @endforeach
            </div>
            </div>
             
             <div class="item">
            <div class="col-lg-12 col-md-12">
            @foreach ($adslatestbids_panel2 as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight">
                <div class="sideMargin borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 37); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 27);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                    </div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">
                          @if($row->average_rate != null)
                             <span id="ads_latest_bids_panel2{{$row->id}}" class="blueText rightMargin"></span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                          @else
                             <span class="blueText rightMargin">No Rating</span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  <div data-countdown="{{$row->ad_expiration}}"></div>
                </div>
              </div>
              @endforeach     
            </div>         
            </div>
          </div>

          <!-- Left and right controls -->
          <a class="left carousel-control" href="#bidsCarousel" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; left:-3px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#bidsCarousel" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:100px; right:-3px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
  </div>
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default bottomMarginLight removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Ads</span> 
      <span class="pull-right"><a href="{{url('category/list')}}" class="redText">View More</a></span>
    </div>
     
     <div class="col-sm-12 col-xs-12 noPadding">
      <div class="panel-body noPadding">
     @foreach ($latestadsmobile as $row)
        <div class="panel panel-default borderZero removeBorder bottomMarginLight">
          <div class="panel-body noPadding">
              <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding lineHeight">
          <div class="mediumText noPadding topPadding bottomPadding">
                <div class="visible-sm"><?php echo substr($row->title, 0, 37); 
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
                <div class="visible-xs"><?php echo substr($row->title, 0, 17);
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
          </div>
          <div class="normalText redText bottomPadding">
                 <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="mediumText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <div class="mediumText bottomPadding">
             @if($row->average_rate != null)
                <span id="latest_ads_mobile_{{$row->id}}" class="blueText rightMargin pull-left"></span>
                <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
             @else
                <span class="blueText rightMargin">No Rating</span>
                <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
             @endif 
           <!--  <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span> -->
          </div>
        </div>
          </div>
        </div>
         @endforeach
    </div>
    </div>
  </div>
<!--   <div class="panel panel-default marginbottomHeavy removeBorder borderZero">
    <div class="panel-body noPadding">
      <div class="col-sm-12 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            by Dell Distributor
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>listingsa
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div> -->
</div>
<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
    <div class="panel panel-default removeBorder borderZero noMargin">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our members latest listings now!</span>
        <span class="pull-right"><a href="{{url('category/list')}}" class="redText">View More</a></span>
      </div>
      <div class="panel-body">
        <div id="adsCarousel" class="carousel slide carousel-fade" data-ride="carousel">
          <!-- Indicators -->

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox" id="latest-ads-wrapper">
            
            <div class="item active">
            <div class="col-lg-12 col-md-12">
              @foreach ($latestads as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight">
                <div class="sideMargin bottomMarginB borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}" id="btn-user-view" data-id="{{$row->id}}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                  </div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
                          @else
                             <span class="blueText rightMargin">No Rating</span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                </div>
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
              </div>
                @endforeach
            </div>
            </div>


           <div class="item">
            <div class="col-lg-12 col-md-12">
            @foreach ($latestads_panel2 as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight">
                <div class="sideMargin bottomMarginB  borderBottom">
                  <a href="{{ url('/ads/view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;pubpubl">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                    </div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{ url('/user/vendor') . '/' . $row->user_id }}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                      <span class="pull-right ">
                         @if($row->average_rate != null)
                             <span id="latest_ads_panel2_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                          @else
                             <span class="blueText rightMargin">No Rating</span>
                             <span class="lightgrayText pull-right">27 <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                </div>
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
              </div>
              @endforeach     
            </div>         
            </div>
      </div>











          <!-- Left and right controls -->
          <a class="left carousel-control" href="#adsCarousel" role="button" data-slide="prev" style="background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; left:-3px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#adsCarousel" role="button" data-slide="next" style="background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; right:-3px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
          
        </div>
      </div>
    </div>
      </div>
    </div>
    </div>
  </div>


<div class="modal-backdrop fade in"></div>

<!-- latest end -->
@stop
@section('content')

@stop
