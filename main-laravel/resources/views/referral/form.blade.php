<div class="modal fade" id="modal-watchlist" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Add this on your watchlists?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>




<div class="modal fade" id="modal-post_invalid" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
		<!-- 	{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			 --><div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					You've reached the allowed limit of ads, Do you want to upgrade your subscription?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit watchButton borderZero"><i class="fa fa-check"></i> Upgrade</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			<!-- {!! Form::close() !!} -->
		</div>
	</div>
</div>




<div class="modal fade" id="modal-watchlist-remove" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'remove-ad-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-remove_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Remove Watchlist</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Remove Ad from your Watchlist?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="watchitem_id" id="row-watchitem_id">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>





<div class="modal fade" id="modal-bidding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form_bidder" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ad/add/bidders', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_bidder_form', 'files' => true)) !!}
			<div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title-bidder">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-bidder_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Are you sure you want to bid on this ad?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero"><i class="fa fa-check"></i> Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>




@if(Route::current()->getPath() == 'ads/view/{id}')
<div id="refresh-bidders-list">
</div>
@endif
