@if(Request::route()->getName() == "SEO_URL")
@if(Auth::check())
@if(Auth::user()->id == $getads_info->user_id)
<div class="modal fade" id="modal-feature-ads" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
			{!! Form::open(array('url' => 'ad/feature', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Feature Ad</h4>
	        </div>
			<div class="modal-body">
			 <div id="feature_ad_points">
				<div id="load-form-feature" class="loading-pane hide">
	    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
	  			</div>
				<div id="form-notice"></div>
				<p>Do you want to make this a feature ad?</p>
				<p>Duration:</p>
				<div class="row" style="padding-bottom: 12px;">
					<div class="col-md-6 col-xs-6">
						<input class="form-control borderZero" type="number" name="duration" id="row-featured_duration" onkeyup="computeDuration()">
					</div>
					<div class="col-md-6 col-xs-6" style="padding-left:0;">
					<select class="form-control borderZero" id="row-featured_duration_metric" name="duration_metric" onchange="computeDuration()">
						@foreach($price_matrix_global as $row)
							<option value="{{$row->id}}">{{$row->name}}</option>
						@endforeach
					</select>
					</div>
				</div>
				<p>Payment Method</p>
				<p>
				   <select class="form-control borderZero" id="row-payment_method" name="payment_method" onchange="computeDuration()">
						<option value="1">Reward Points</option>
						<option value="2">Paypal</option>
					</select>
				</p>
				<p id="total-point-label">Cost: <span id="total-point-cost">0</span> <span id="payment_unit"></span> <span class="redText normalText hide" id="feature-error">Insufficient Points</span><span id="current_points" class="pull-right"> You have {{Auth::user()->points}} points available.</span></p>
		<!-- 	    <p></p> -->
				<input type="hidden" name="id" id="row-id" value="{{$getads_info->id}}">
				<input type="hidden" name="title" id="row-title" value="{{$getads_info->title}}">
				<input type="hidden" name="cost_points" id="row-cost_points" value="">
		    	<div class="modal-footer" id="buttons_for_reward_points" style="padding-bottom:0px;padding-right:0px;">
				 <span>
					<button type="button" class="btn btn-submit blueButton borderZero" disabled="disabled" id="btnFeature">Submit</button>
					<button type="button" class="btn redButton borderZero" data-dismiss="modal">Close</button>
				</span>
               {!! Form::close() !!}
        		</div>
        	</div>
        	</div>
        	<div class="modal-footer hide" id="buttons_for_paypal" style="margin-top: -16px; margin-bottom: 0px;">
        		<span>
				  <form class="noMargin" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="paypalForm" style="padding-top: 1px;">
                        <input type="hidden" name="cmd" value="_xclick">
                        <input type="hidden" name="hosted_button_id" value="{{$paypal_credentials->api_secret}}">
                        <INPUT TYPE="hidden" NAME="return" id="return_url" value="{{ url() }}">
                        <input type="hidden" name="business" id="element-to-hide" value="{{$paypal_credentials->api_key}}">
                        <input type="hidden" name="item_name" id="paypal-item-name">
                        <input type="hidden" name="item_number" id="paypal-item-number">
                        <input type="hidden" name="cm" value="">
                        <input type="hidden" name="currency_code" value="USD">
                        <input type="hidden" name="amount" id="paypal-amount">
                        <div class="form-group-sm noMargin">
<!--                         <label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding "></label> -->
                        <!-- <div class="input-group col-md-9 col-sm-12 col-xs-12 "> -->
                        <input type="submit" value="Add To Cart" style="padding:7px 15px !important;" name="submit" title="PayPal - The safer, easier way to pay online!" class="paypal_btn btn btn-submit blueButton borderZero" id="btnPaypalFeature">
                        <button type="button" class="btn redButton borderZero" data-dismiss="modal">Close</button>
<!--                         <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1"> -->

                        <!-- </div> -->
                        </div>
                    </form>
                </span>
        	</div>
		
		</div>
	</div>
</div>
@endif
@endif
@endif
	


<div class="modal fade" id="modal-watchlist" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title modal-title-bidder panelTitle">Watchlist</h4>
          </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Add this on your watchlists?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit blueButton borderZero">Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>




<div class="modal fade" id="modal-post_invalid" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
		<!-- 	{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			 --><div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					You've reached the allowed limit of ads, Do you want to upgrade your subscription?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<a role="button" href="{{url('dashboard').'#subscription'}}" type="submit" class="btn btn-submit watchButton borderZero"><i class="fa fa-check"></i> Upgrade</a>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			<!-- {!! Form::close() !!} -->
		</div>
	</div>
</div>




<div class="modal fade" id="modal-watchlist-remove" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'remove-ad-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-remove_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Remove Watchlist</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group nobottomMargin">
				<div class="col-lg-12">
					Remove Ad from your Watchlist?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="watchitem_id" id="row-watchitem_id">
				<button type="submit" class="btn btn-submit blueButton borderZero">Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal">No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

  @if(Request::route()->getName() == "SEO_URL")
<div class="modal fade" id="modal-bidding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		<div id="load-form_bidder" class="loading-pane hide">
			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
		</div>
		{!! Form::open(array('url' => 'ad/add/bidders', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_bidder_form', 'files' => true)) !!}
		<div class="modal-header modalTitleBar">
			<button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
			<h4 class="modal-title modal-title-bidder panelTitle">Bid</h4>
		</div>
		<div class="modal-body">
			<!--<div id="form-notice"></div>-->
			<div id="validation-errors">
			</div>
			
			<div class="col-lg-12">
			<h4 class="minbid"><span class="leftPadding bigText"></span></h4>
			<p>Please enter your bid amount (starting from {{number_format($getads_info->minimum_allowed_bid)}} {{$getads_info->currency}}):<span><input type="text" placeholder="" class="form-control borderZero topMargin" name="bidvalue" required="required" id="bid-amount-modal"></span></p>
			</div>
		</div>
		<div class="clearfix"></div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<button type="submit" class="btn btn-submit redButton borderZero"><i class="fa fa-gavel"></i> Bid</button>
				<button type="button" class="btn blueButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>
@endif
<!-- <div class="modal fade" id="modal-bidding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form_bidder" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ad/add/bidders', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_bidder_form', 'files' => true)) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title-bidder">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-bidder_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<button type="submit" class="btn btn-submit redButton borderZero"><i class="fa fa-gavel"></i> Bid</button>
				<button type="button" class="btn blueButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> -->
@if(Request::url() != (url('user/post/ads/6789') || url('post/ads') || url('post/auctions')))
	@if(count($promotion_popups) != 0)
		 @foreach($promotion_popups as $row)
			<div class="modal fade" id="modal-promotion-popup" role="dialog">
			  <div class="modal-dialog modal-md" id="modal-login_form">
			    <div class="modal-content removeBorder loginSize borderZero">
			      <div class="modal-header modalTitleBar">
			        <button type="button" class="close closeButton btn-hide-promotion" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title panelTitle">{{$row->name}}</h4>
			        </div>
			        <div class="modal-body">
			        	<img src="{{url('uploads').'/'.$row->photo}}" width="100%" height="100%" class="img-responsive">
			        </div>
			         <div class="modal-footer bordertopLight borderBottom">
			          <span class="pull-right">
			            <button type="button" class="btn blueButton borderZero btn-hide-promotion" data-dismiss="modal">Hide</button>
			          </span>
			        </div>
			      </div>
			    </div>
			  </div>
		  @endforeach
	@endif
@endif

