@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
var currentInnerHtml;
var element = new Image();
var elementWithHiddenContent = document.querySelector("#element-to-hide");
var innerHtml = elementWithHiddenContent.innerHTML;

element.__defineGetter__("id", function() {
    currentInnerHtml = "";
});


$('#row-payment-type').change(function() {
	var type= $(this).val();
	if(type == "points"){
		$("#paypal-pane").addClass("hide");
		$("#points-pane").removeClass("hide");
	}else{
		$("#paypal-pane").removeClass("hide");
		$("#points-pane").addClass("hide");
	}
});
  
  $('#points_submitter').click(function() {
    $('#redeem_by_points').submit();
  });
  
</script>
@stop
@section('content')
<div class="container-fluid bgGray">
	<div class="container bannerJumbutron">
		<div class="col-md-12">
	

				<div class="panel panel-default removeBorder borderZero borderBottom">
				<div class="panel-heading panelTitleBarLight"><span class="mediumText grayText"><strong>PAYMENT INFORMATION</strong></span></div>
				<div class="panel-body">
				<div class="col-md-6">
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Transaction ID.</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<input name="country" value="{{$trans_id_number}}" placeholder="" class="form-control borderZero inputBox fullSize" type="text">
								</div>
							</div>
						</div>
						<div class="form-group bottomMargin">
							<label class="col-md-3 col-sm-12 col-xs-12 normalText inputLabel noPadding ">Pay Thru</label>  
							<div class="inputGroupContainer">
								<div class="input-group col-md-9 col-sm-12 col-xs-12 ">
									<!-- <input name="username" placeholder="Input" value="Paypal Payment" class="form-control borderZero inputBox fullSize" type="text"> -->
									<select name="payment_type" id="row-payment-type" class="form-control borderZero inputBox fullSize">
										<option class="hide">Select</option>
										<option value="paypal">Paypal</option>
										<option value="points">Reward Points</option>
									</select>
								</div>
							</div>
						</div>			
					</div>
					<div class="col-md-6 hide" id="paypal-pane">
						<div class="form-group bottomMargin">{!! $form !!}</div>
					</div>
					<div class="col-md-6 hide" id="points-pane">
						<div class="form-group bottomMargin">{!! $form_points !!}</div>
					</div>
				</div>
		</div>
	</div>
</div>
@stop

