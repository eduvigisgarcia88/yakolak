-- phpMyAdmin SQL Dump
-- version 4.3.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 11, 2016 at 02:12 PM
-- Server version: 5.5.43-MariaDB
-- PHP Version: 5.5.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yakolak`
--

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_advertisements`
--

CREATE TABLE IF NOT EXISTS `yakolak_advertisements` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `yakolak_advertisements`
--

INSERT INTO `yakolak_advertisements` (`id`, `user_id`, `category`, `title`, `description`, `price`, `status`, `created_at`, `updated_at`) VALUES
(40, 5, 'Bidding', 'aweawe', 'waewaeaw', 0, 1, '2016-02-05 01:58:34', '2016-02-05 01:58:34'),
(41, 5, 'Bidding', 'wqewqe', 'wqewq', 0, 1, '2016-02-05 02:02:03', '2016-02-05 02:02:03'),
(42, 5, 'Bidding', '324324', 'awewa', 0, 1, '2016-02-05 02:02:47', '2016-02-05 02:02:47'),
(43, 5, 'Buy and Sell', '', 'awdwad', 0, 1, '2016-02-05 02:03:11', '2016-02-05 02:03:11'),
(44, 5, 'Buy and Sell', 'waewae', 'awdwad', 0, 1, '2016-02-05 02:03:14', '2016-02-05 02:03:14'),
(45, 5, 'Buy and Sell', 'Samsung Tv', 'qweqwewqe', 0, 1, '2016-02-05 02:15:01', '2016-02-05 02:15:01'),
(46, 5, 'Buy and Sell', 'Luxury Bed', 'Luxury Bed', 0, 1, '2016-02-05 02:16:02', '2016-02-05 02:16:02'),
(47, 5, 'Buy and Sell', 'Toyota', 'Toyota', 0, 1, '2016-02-05 02:16:35', '2016-02-05 02:16:35'),
(48, 5, 'Jobs', 'wewae', 'awdaw', 0, 1, '2016-02-05 02:21:21', '2016-02-05 02:21:21'),
(49, 5, 'Buy and Sell', 'qwewqeqwe', 'wqewqew', 0, 1, '2016-02-05 02:21:48', '2016-02-05 02:21:48'),
(50, 5, 'Buy and Sell', 'wqewqeqwe', 'weqewq', 0, 1, '2016-02-05 02:30:36', '2016-02-05 02:30:36'),
(51, 5, 'Buy and Sell', 'wadawd', 'awdawd', 0, 1, '2016-02-05 02:30:55', '2016-02-05 02:30:55'),
(52, 5, 'Buy and Sell', '1312321', 'aweawe', 0, 1, '2016-02-05 02:35:17', '2016-02-05 02:35:17'),
(53, 5, 'Bidding', 'waewae', 'awewa', 0, 1, '2016-02-05 03:02:38', '2016-02-05 03:02:38'),
(54, 5, '', '', '', 0, 1, '2016-02-05 03:04:00', '2016-02-05 03:04:00'),
(55, 5, 'Buy and Sell', 'wqewqe', 'wewae', 0, 1, '2016-02-05 03:04:06', '2016-02-05 03:04:06'),
(56, 5, 'Buy and Sell', 'waeawe', 'awdaw', 0, 1, '2016-02-05 03:14:00', '2016-02-05 03:14:00'),
(57, 1, 'Buy and Sell', '111', '1111', 0, 1, '2016-02-10 22:08:38', '2016-02-10 22:08:38'),
(58, 1, 'Buy and Sell', '1231231', '21312312', 0, 1, '2016-02-10 22:09:26', '2016-02-10 22:09:26'),
(59, 1, 'Buy and Sell', 'eaweaw', '213123', 0, 1, '2016-02-10 22:10:55', '2016-02-10 22:10:55'),
(60, 1, 'Buy and Sell', '213123', '213213213231', 0, 1, '2016-02-10 22:12:15', '2016-02-10 22:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_advertisements_photo`
--

CREATE TABLE IF NOT EXISTS `yakolak_advertisements_photo` (
`id` int(11) NOT NULL,
  `ads_id` int(11) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `primary` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_advertisements_photo`
--

INSERT INTO `yakolak_advertisements_photo` (`id`, `ads_id`, `photo`, `primary`, `created_at`, `updated_at`) VALUES
(1, 31, 'the-division.jpg', 0, '2016-02-05 05:48:16', '2016-02-04 21:48:16'),
(2, 31, 'the-divisionPH.jpg', 0, '2016-02-05 05:48:18', '2016-02-04 21:48:18'),
(3, 31, 'xero.jpg', 0, '2016-02-05 05:48:18', '2016-02-04 21:48:18'),
(4, 32, '50 - Copy.jpg', 0, '2016-02-05 09:38:59', '2016-02-05 01:38:59'),
(5, 33, '53.jpg', 0, '2016-02-05 09:39:22', '2016-02-05 01:39:22'),
(6, 33, '55.jpg', 0, '2016-02-05 09:39:23', '2016-02-05 01:39:23'),
(7, 38, '55.jpg', 1, '2016-02-05 09:55:49', '2016-02-05 01:55:49'),
(8, 39, '48.jpg', 1, '2016-02-05 09:56:09', '2016-02-05 01:56:09'),
(9, 39, '50 - Copy.jpg', 0, '2016-02-05 09:56:09', '2016-02-05 01:56:09'),
(10, 40, '48.jpg', 1, '2016-02-05 09:58:34', '2016-02-05 01:58:34'),
(11, 40, '50 - Copy.jpg', 0, '2016-02-05 09:58:34', '2016-02-05 01:58:34'),
(12, 41, '52.jpg', 1, '2016-02-05 10:02:04', '2016-02-05 02:02:04'),
(13, 41, '27399.jpg', 0, '2016-02-05 10:02:05', '2016-02-05 02:02:05'),
(14, 41, '65948.jpg', 0, '2016-02-05 10:02:05', '2016-02-05 02:02:05'),
(15, 42, '47.jpg', 1, '2016-02-05 10:02:48', '2016-02-05 02:02:48'),
(16, 42, '48.jpg', 0, '2016-02-05 10:02:48', '2016-02-05 02:02:48'),
(17, 42, '55.jpg', 0, '2016-02-05 10:02:49', '2016-02-05 02:02:49'),
(18, 44, '50 - Copy.jpg', 1, '2016-02-05 10:03:15', '2016-02-05 02:03:15'),
(19, 44, '50.jpg', 0, '2016-02-05 10:03:15', '2016-02-05 02:03:15'),
(20, 45, '57.jpg', 1, '2016-02-05 10:15:01', '2016-02-05 02:15:01'),
(21, 46, '55.jpg', 1, '2016-02-05 10:16:03', '2016-02-05 02:16:03'),
(22, 47, '52.jpg', 1, '2016-02-05 10:16:37', '2016-02-05 02:16:37'),
(23, 47, '27399.jpg', 0, '2016-02-05 10:16:37', '2016-02-05 02:16:37'),
(24, 48, '7 - Copy.jpg', 1, '2016-02-05 10:21:21', '2016-02-05 02:21:21'),
(25, 48, '9.jpg', 0, '2016-02-05 10:21:21', '2016-02-05 02:21:21'),
(26, 48, '12 - Copy (2).jpg', 0, '2016-02-05 10:21:21', '2016-02-05 02:21:21'),
(27, 49, '25.jpg', 1, '2016-02-05 10:21:48', '2016-02-05 02:21:48'),
(28, 49, '29.jpg', 0, '2016-02-05 10:21:48', '2016-02-05 02:21:48'),
(29, 49, '34.jpg', 0, '2016-02-05 10:21:48', '2016-02-05 02:21:48'),
(30, 49, '36.jpg', 0, '2016-02-05 10:21:49', '2016-02-05 02:21:49'),
(31, 50, '4.jpg', 1, '2016-02-05 10:30:36', '2016-02-05 02:30:36'),
(32, 50, '5 - Copy.jpg', 0, '2016-02-05 10:30:36', '2016-02-05 02:30:36'),
(33, 51, '15.jpg', 1, '2016-02-05 10:30:55', '2016-02-05 02:30:55'),
(34, 52, '3 - Copy.jpg', 1, '2016-02-05 10:35:17', '2016-02-05 02:35:17'),
(35, 52, '4.jpg', 0, '2016-02-05 10:35:17', '2016-02-05 02:35:17'),
(36, 52, '8.jpg', 0, '2016-02-05 10:35:17', '2016-02-05 02:35:17'),
(37, 53, '43.jpg', 1, '2016-02-05 11:02:38', '2016-02-05 03:02:38'),
(38, 53, '44.jpg', 0, '2016-02-05 11:02:38', '2016-02-05 03:02:38'),
(39, 53, '45.jpg', 0, '2016-02-05 11:02:38', '2016-02-05 03:02:38'),
(40, 55, '7.jpg', 1, '2016-02-05 11:04:06', '2016-02-05 03:04:06'),
(41, 55, '15.jpg', 0, '2016-02-05 11:04:06', '2016-02-05 03:04:06'),
(42, 55, '28.jpg', 0, '2016-02-05 11:04:06', '2016-02-05 03:04:06'),
(43, 55, '29.jpg', 0, '2016-02-05 11:04:06', '2016-02-05 03:04:06'),
(44, 56, '2.jpg', 1, '2016-02-05 11:14:00', '2016-02-05 03:14:00'),
(45, 56, '4.jpg', 0, '2016-02-05 11:14:00', '2016-02-05 03:14:00'),
(46, 56, '5 - Copy.jpg', 0, '2016-02-05 11:14:00', '2016-02-05 03:14:00'),
(47, 56, '6.jpg', 0, '2016-02-05 11:14:01', '2016-02-05 03:14:01'),
(48, 56, '7.jpg', 0, '2016-02-05 11:14:01', '2016-02-05 03:14:01'),
(49, 56, '8.jpg', 0, '2016-02-05 11:14:01', '2016-02-05 03:14:01'),
(50, 58, '6.jpg', 1, '2016-02-11 06:09:28', '2016-02-10 22:09:28'),
(51, 58, 'Mario_png.png', 0, '2016-02-11 06:09:31', '2016-02-10 22:09:31'),
(52, 59, '1.jpg', 1, '2016-02-11 06:10:55', '2016-02-10 22:10:55'),
(53, 59, '6.jpg', 0, '2016-02-11 06:10:56', '2016-02-10 22:10:56'),
(54, 60, 'Mario_png.png', 1, '2016-02-11 06:12:19', '2016-02-10 22:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_categories`
--

CREATE TABLE IF NOT EXISTS `yakolak_categories` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` longtext NOT NULL,
  `description` text NOT NULL,
  `sequence` int(11) NOT NULL,
  `biddable_status` int(11) NOT NULL DEFAULT '1',
  `buy_and_sell_status` int(11) NOT NULL DEFAULT '1',
  `status` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_categories`
--

INSERT INTO `yakolak_categories` (`id`, `user_id`, `name`, `description`, `sequence`, `biddable_status`, `buy_and_sell_status`, `status`, `created_at`, `updated_at`) VALUES
(6, 0, 'bags', 'sdsdd', 3, 1, 0, 1, '2016-01-05 07:10:04', '2016-01-12 21:33:21'),
(7, 0, 'jeans', 'dsdasd', 2, 2, 2, 1, '2016-01-05 07:11:37', '2016-01-12 21:33:12'),
(11, 0, 'Caps', 'sdsd', 1, 2, 1, 1, '2016-01-11 05:53:19', '2016-01-12 21:33:04'),
(12, 0, 'TShirts', 'dsdsd', 0, 1, 0, 2, '2016-01-11 06:01:13', '2016-01-11 07:21:28'),
(22, 0, 'Sample', 'dsdass', 4, 2, 2, 1, '2016-01-11 11:00:48', '2016-01-12 02:55:45');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_category_custom_attributes`
--

CREATE TABLE IF NOT EXISTS `yakolak_category_custom_attributes` (
`id` int(11) NOT NULL,
  `name` varchar(10000) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_category_custom_attributes`
--

INSERT INTO `yakolak_category_custom_attributes` (`id`, `name`, `cat_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Cars and Vehicles', 0, 1, '0000-00-00 00:00:00', '2016-01-11 10:10:26'),
(2, 'Clothing', 0, 1, '0000-00-00 00:00:00', '2016-01-11 10:10:26'),
(3, 'Events', 0, 1, '0000-00-00 00:00:00', '2016-01-11 10:10:48'),
(4, 'Jobs', 0, 1, '0000-00-00 00:00:00', '2016-01-11 10:10:48'),
(5, 'Real Estate', 0, 1, '0000-00-00 00:00:00', '2016-01-11 10:11:03');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_countries`
--

CREATE TABLE IF NOT EXISTS `yakolak_countries` (
`id` int(5) NOT NULL,
  `countryCode` char(2) NOT NULL DEFAULT '',
  `countryName` varchar(45) NOT NULL DEFAULT ''
) ENGINE=MyISAM AUTO_INCREMENT=251 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_countries`
--

INSERT INTO `yakolak_countries` (`id`, `countryCode`, `countryName`) VALUES
(1, 'AD', 'Andorra'),
(2, 'AE', 'United Arab Emirates'),
(3, 'AF', 'Afghanistan'),
(4, 'AG', 'Antigua and Barbuda'),
(5, 'AI', 'Anguilla'),
(6, 'AL', 'Albania'),
(7, 'AM', 'Armenia'),
(8, 'AO', 'Angola'),
(9, 'AQ', 'Antarctica'),
(10, 'AR', 'Argentina'),
(11, 'AS', 'American Samoa'),
(12, 'AT', 'Austria'),
(13, 'AU', 'Australia'),
(14, 'AW', 'Aruba'),
(15, 'AX', 'Åland'),
(16, 'AZ', 'Azerbaijan'),
(17, 'BA', 'Bosnia and Herzegovina'),
(18, 'BB', 'Barbados'),
(19, 'BD', 'Bangladesh'),
(20, 'BE', 'Belgium'),
(21, 'BF', 'Burkina Faso'),
(22, 'BG', 'Bulgaria'),
(23, 'BH', 'Bahrain'),
(24, 'BI', 'Burundi'),
(25, 'BJ', 'Benin'),
(26, 'BL', 'Saint Barthélemy'),
(27, 'BM', 'Bermuda'),
(28, 'BN', 'Brunei'),
(29, 'BO', 'Bolivia'),
(30, 'BQ', 'Bonaire'),
(31, 'BR', 'Brazil'),
(32, 'BS', 'Bahamas'),
(33, 'BT', 'Bhutan'),
(34, 'BV', 'Bouvet Island'),
(35, 'BW', 'Botswana'),
(36, 'BY', 'Belarus'),
(37, 'BZ', 'Belize'),
(38, 'CA', 'Canada'),
(39, 'CC', 'Cocos [Keeling] Islands'),
(40, 'CD', 'Democratic Republic of the Congo'),
(41, 'CF', 'Central African Republic'),
(42, 'CG', 'Republic of the Congo'),
(43, 'CH', 'Switzerland'),
(44, 'CI', 'Ivory Coast'),
(45, 'CK', 'Cook Islands'),
(46, 'CL', 'Chile'),
(47, 'CM', 'Cameroon'),
(48, 'CN', 'China'),
(49, 'CO', 'Colombia'),
(50, 'CR', 'Costa Rica'),
(51, 'CU', 'Cuba'),
(52, 'CV', 'Cape Verde'),
(53, 'CW', 'Curacao'),
(54, 'CX', 'Christmas Island'),
(55, 'CY', 'Cyprus'),
(56, 'CZ', 'Czech Republic'),
(57, 'DE', 'Germany'),
(58, 'DJ', 'Djibouti'),
(59, 'DK', 'Denmark'),
(60, 'DM', 'Dominica'),
(61, 'DO', 'Dominican Republic'),
(62, 'DZ', 'Algeria'),
(63, 'EC', 'Ecuador'),
(64, 'EE', 'Estonia'),
(65, 'EG', 'Egypt'),
(66, 'EH', 'Western Sahara'),
(67, 'ER', 'Eritrea'),
(68, 'ES', 'Spain'),
(69, 'ET', 'Ethiopia'),
(70, 'FI', 'Finland'),
(71, 'FJ', 'Fiji'),
(72, 'FK', 'Falkland Islands'),
(73, 'FM', 'Micronesia'),
(74, 'FO', 'Faroe Islands'),
(75, 'FR', 'France'),
(76, 'GA', 'Gabon'),
(77, 'GB', 'United Kingdom'),
(78, 'GD', 'Grenada'),
(79, 'GE', 'Georgia'),
(80, 'GF', 'French Guiana'),
(81, 'GG', 'Guernsey'),
(82, 'GH', 'Ghana'),
(83, 'GI', 'Gibraltar'),
(84, 'GL', 'Greenland'),
(85, 'GM', 'Gambia'),
(86, 'GN', 'Guinea'),
(87, 'GP', 'Guadeloupe'),
(88, 'GQ', 'Equatorial Guinea'),
(89, 'GR', 'Greece'),
(90, 'GS', 'South Georgia and the South Sandwich Islands'),
(91, 'GT', 'Guatemala'),
(92, 'GU', 'Guam'),
(93, 'GW', 'Guinea-Bissau'),
(94, 'GY', 'Guyana'),
(95, 'HK', 'Hong Kong'),
(96, 'HM', 'Heard Island and McDonald Islands'),
(97, 'HN', 'Honduras'),
(98, 'HR', 'Croatia'),
(99, 'HT', 'Haiti'),
(100, 'HU', 'Hungary'),
(101, 'ID', 'Indonesia'),
(102, 'IE', 'Ireland'),
(103, 'IL', 'Israel'),
(104, 'IM', 'Isle of Man'),
(105, 'IN', 'India'),
(106, 'IO', 'British Indian Ocean Territory'),
(107, 'IQ', 'Iraq'),
(108, 'IR', 'Iran'),
(109, 'IS', 'Iceland'),
(110, 'IT', 'Italy'),
(111, 'JE', 'Jersey'),
(112, 'JM', 'Jamaica'),
(113, 'JO', 'Jordan'),
(114, 'JP', 'Japan'),
(115, 'KE', 'Kenya'),
(116, 'KG', 'Kyrgyzstan'),
(117, 'KH', 'Cambodia'),
(118, 'KI', 'Kiribati'),
(119, 'KM', 'Comoros'),
(120, 'KN', 'Saint Kitts and Nevis'),
(121, 'KP', 'North Korea'),
(122, 'KR', 'South Korea'),
(123, 'KW', 'Kuwait'),
(124, 'KY', 'Cayman Islands'),
(125, 'KZ', 'Kazakhstan'),
(126, 'LA', 'Laos'),
(127, 'LB', 'Lebanon'),
(128, 'LC', 'Saint Lucia'),
(129, 'LI', 'Liechtenstein'),
(130, 'LK', 'Sri Lanka'),
(131, 'LR', 'Liberia'),
(132, 'LS', 'Lesotho'),
(133, 'LT', 'Lithuania'),
(134, 'LU', 'Luxembourg'),
(135, 'LV', 'Latvia'),
(136, 'LY', 'Libya'),
(137, 'MA', 'Morocco'),
(138, 'MC', 'Monaco'),
(139, 'MD', 'Moldova'),
(140, 'ME', 'Montenegro'),
(141, 'MF', 'Saint Martin'),
(142, 'MG', 'Madagascar'),
(143, 'MH', 'Marshall Islands'),
(144, 'MK', 'Macedonia'),
(145, 'ML', 'Mali'),
(146, 'MM', 'Myanmar [Burma]'),
(147, 'MN', 'Mongolia'),
(148, 'MO', 'Macao'),
(149, 'MP', 'Northern Mariana Islands'),
(150, 'MQ', 'Martinique'),
(151, 'MR', 'Mauritania'),
(152, 'MS', 'Montserrat'),
(153, 'MT', 'Malta'),
(154, 'MU', 'Mauritius'),
(155, 'MV', 'Maldives'),
(156, 'MW', 'Malawi'),
(157, 'MX', 'Mexico'),
(158, 'MY', 'Malaysia'),
(159, 'MZ', 'Mozambique'),
(160, 'NA', 'Namibia'),
(161, 'NC', 'New Caledonia'),
(162, 'NE', 'Niger'),
(163, 'NF', 'Norfolk Island'),
(164, 'NG', 'Nigeria'),
(165, 'NI', 'Nicaragua'),
(166, 'NL', 'Netherlands'),
(167, 'NO', 'Norway'),
(168, 'NP', 'Nepal'),
(169, 'NR', 'Nauru'),
(170, 'NU', 'Niue'),
(171, 'NZ', 'New Zealand'),
(172, 'OM', 'Oman'),
(173, 'PA', 'Panama'),
(174, 'PE', 'Peru'),
(175, 'PF', 'French Polynesia'),
(176, 'PG', 'Papua New Guinea'),
(177, 'PH', 'Philippines'),
(178, 'PK', 'Pakistan'),
(179, 'PL', 'Poland'),
(180, 'PM', 'Saint Pierre and Miquelon'),
(181, 'PN', 'Pitcairn Islands'),
(182, 'PR', 'Puerto Rico'),
(183, 'PS', 'Palestine'),
(184, 'PT', 'Portugal'),
(185, 'PW', 'Palau'),
(186, 'PY', 'Paraguay'),
(187, 'QA', 'Qatar'),
(188, 'RE', 'Réunion'),
(189, 'RO', 'Romania'),
(190, 'RS', 'Serbia'),
(191, 'RU', 'Russia'),
(192, 'RW', 'Rwanda'),
(193, 'SA', 'Saudi Arabia'),
(194, 'SB', 'Solomon Islands'),
(195, 'SC', 'Seychelles'),
(196, 'SD', 'Sudan'),
(197, 'SE', 'Sweden'),
(198, 'SG', 'Singapore'),
(199, 'SH', 'Saint Helena'),
(200, 'SI', 'Slovenia'),
(201, 'SJ', 'Svalbard and Jan Mayen'),
(202, 'SK', 'Slovakia'),
(203, 'SL', 'Sierra Leone'),
(204, 'SM', 'San Marino'),
(205, 'SN', 'Senegal'),
(206, 'SO', 'Somalia'),
(207, 'SR', 'Suriname'),
(208, 'SS', 'South Sudan'),
(209, 'ST', 'São Tomé and Príncipe'),
(210, 'SV', 'El Salvador'),
(211, 'SX', 'Sint Maarten'),
(212, 'SY', 'Syria'),
(213, 'SZ', 'Swaziland'),
(214, 'TC', 'Turks and Caicos Islands'),
(215, 'TD', 'Chad'),
(216, 'TF', 'French Southern Territories'),
(217, 'TG', 'Togo'),
(218, 'TH', 'Thailand'),
(219, 'TJ', 'Tajikistan'),
(220, 'TK', 'Tokelau'),
(221, 'TL', 'East Timor'),
(222, 'TM', 'Turkmenistan'),
(223, 'TN', 'Tunisia'),
(224, 'TO', 'Tonga'),
(225, 'TR', 'Turkey'),
(226, 'TT', 'Trinidad and Tobago'),
(227, 'TV', 'Tuvalu'),
(228, 'TW', 'Taiwan'),
(229, 'TZ', 'Tanzania'),
(230, 'UA', 'Ukraine'),
(231, 'UG', 'Uganda'),
(232, 'UM', 'U.S. Minor Outlying Islands'),
(233, 'US', 'United States'),
(234, 'UY', 'Uruguay'),
(235, 'UZ', 'Uzbekistan'),
(236, 'VA', 'Vatican City'),
(237, 'VC', 'Saint Vincent and the Grenadines'),
(238, 'VE', 'Venezuela'),
(239, 'VG', 'British Virgin Islands'),
(240, 'VI', 'U.S. Virgin Islands'),
(241, 'VN', 'Vietnam'),
(242, 'VU', 'Vanuatu'),
(243, 'WF', 'Wallis and Futuna'),
(244, 'WS', 'Samoa'),
(245, 'XK', 'Kosovo'),
(246, 'YE', 'Yemen'),
(247, 'YT', 'Mayotte'),
(248, 'ZA', 'South Africa'),
(249, 'ZM', 'Zambia'),
(250, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_sub_attributes`
--

CREATE TABLE IF NOT EXISTS `yakolak_sub_attributes` (
`id` int(11) NOT NULL,
  `attri_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_sub_attributes`
--

INSERT INTO `yakolak_sub_attributes` (`id`, `attri_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Brand', 1, '2016-01-19 09:46:15', '2016-02-02 10:13:01'),
(2, 2, 'Brand', 1, '2016-01-19 09:58:56', '2016-02-02 10:13:19'),
(3, 5, 'Type', 1, '2016-01-19 10:00:32', '2016-02-02 10:14:36'),
(4, 3, 'Type', 1, '2016-01-19 11:31:50', '2016-02-02 10:13:42'),
(5, 3, 'Date', 1, '2016-01-19 11:31:59', '2016-02-02 10:13:46'),
(6, 4, 'Type', 1, '2016-01-19 11:32:11', '2016-02-02 10:13:58'),
(7, 2, 'Color', 1, '2016-01-19 11:32:38', '2016-02-02 10:13:28'),
(8, 1, 'Color', 1, '2016-01-19 11:33:00', '2016-02-02 10:13:10'),
(9, 3, 'Location', 1, '2016-01-19 11:33:18', '2016-02-02 10:14:04');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_sub_categories`
--

CREATE TABLE IF NOT EXISTS `yakolak_sub_categories` (
`id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(1000) NOT NULL,
  `sequence` int(11) NOT NULL,
  `biddable_status` int(11) NOT NULL DEFAULT '1',
  `buy_and_sell_status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_sub_categories`
--

INSERT INTO `yakolak_sub_categories` (`id`, `cat_id`, `name`, `sequence`, `biddable_status`, `buy_and_sell_status`, `created_at`, `updated_at`) VALUES
(1, 7, 'Anta', 1, 1, 1, '2016-01-06 07:10:44', '2016-01-12 06:27:20'),
(2, 7, 'adidas', 2, 1, 1, '2016-01-06 07:11:51', '2016-01-11 05:31:39'),
(3, 7, 'nike', 3, 1, 1, '2016-01-06 07:11:51', '2016-01-11 05:31:43'),
(4, 6, 'black', 1, 1, 1, '2016-01-06 07:53:10', '2016-01-08 08:12:41'),
(5, 6, 'brown', 2, 1, 1, '2016-01-06 07:53:10', '2016-01-08 08:12:47'),
(7, 6, 'Fila', 5, 1, 1, '2016-01-11 05:34:10', '2016-01-10 21:34:10'),
(8, 6, 'Vhitton', 6, 1, 1, '2016-01-11 05:52:10', '2016-01-10 21:52:10'),
(13, 0, 'Buy and Sell', 1, 1, 1, '2016-01-11 10:35:22', '2016-01-11 02:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_users`
--

CREATE TABLE IF NOT EXISTS `yakolak_users` (
`id` int(11) unsigned NOT NULL,
  `code` varchar(1000) CHARACTER SET utf8 DEFAULT NULL,
  `photo` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `name` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `email` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `mobile` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `country` int(11) NOT NULL,
  `city` int(11) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `office_hours` varchar(1000) NOT NULL,
  `website` varchar(100) NOT NULL,
  `username` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `password` varchar(1000) CHARACTER SET utf8 NOT NULL,
  `usertype_id` int(11) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yakolak_users`
--

INSERT INTO `yakolak_users` (`id`, `code`, `photo`, `name`, `email`, `mobile`, `telephone`, `country`, `city`, `address`, `office_hours`, `website`, `username`, `password`, `usertype_id`, `gender`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'CEO-0001', 'default_image.png', 'CEO', 'ceo@legacy.com', '12345678', '', 0, 0, '', '', '', 'ceo@legacy.com', '$2y$10$MW18a47sC5Yoqp6cVme.4.I0XKvKYpNAa2dM87K3Sa1lvw2W7ZwL.', 1, '', 'yHrVfUQrY5KgSK34pdjxjfkGDBsNC7LaxJifrhS3bJkaq5U0MT5vHsjpIaLw', 1, '2015-11-02 00:00:00', '2016-02-10 22:09:33'),
(2, 'IT-0001', '2.jpg', 'IT Users', 'itusers@legacy.com', '1234567', '', 0, 0, '', '', '', 'itusers@legacy.com', '$2y$10$qdKtJnkjOJkE2AhuD4E71uPA6Va8CIc4ZRWQhx0YZtODB3LEdYwT6', 2, '', 'Xgjn9R4EqRFhcNyVkmhwbbQHi8KAOwTKWNSbCh54kAwCgtvMyTb3Et7Ju9YI', 1, '2015-11-02 05:39:24', '2015-12-17 02:48:04'),
(3, 'ADMIN-0001', 'default_image.jpg', 'Admin Accounts', 'admin_accounts@legacy.com', '1234567', '', 0, 0, '', '', '', 'admin_accounts@legacy.com', '$2y$10$j.D2Cu0.wFu98mNZfChZOO/9k2jaVt1nzhcf3BE1RaZF3lKX1pb.C', 3, '', 'hGw5ef1xGFFeBMtJ9dqiPuysFEChl90iv4OwBQR7Hld4vErrDXPA5D3xmhAq', 1, '2015-11-02 05:44:06', '2015-12-17 01:39:15'),
(4, 'ADMIN-0002', 'default_image.jpg', 'Admin Assistant', 'admin_assistant@legacy.com', '1234567', '', 0, 0, '', '', '', 'admin_assistant@legacy.com', '$2y$10$XKE6pxd4w75dx35RdQbdCumopfu75IPXC5iywyfCfJnC2cNpVPAiy', 4, '', NULL, 1, '2015-11-02 05:44:45', '2015-11-02 05:47:14'),
(5, 'ACC-0001', '5.jpg', 'Accounting', 'accounting@legacy.com', '1234567', '', 0, 0, '', '', '', 'accounting@legacy.com', '$2y$10$k22s5BI9bcbE1WBSF60QYeKf2p2LijfE3qtXYgC80DuRN.5DtNmvu', 5, '', 'Q2nevhwtpHBdHYf5hxmFF9cwYTtQpsmk7AqrrSCq0ELznBJbBqeCOdsmwnYv', 1, '2015-11-02 05:45:24', '2016-02-10 18:58:35'),
(6, 'ACC-0002', '6.jpg', 'Accounting Assistant', 'accounting_assistant@legacy.co', '1234567', '', 0, 0, '', '', '', 'accounting_assistant@legacy.co', '$2y$10$SXAv/u4rkBzvZpDaYK3TIOfuw9r4y6HtmxyZQWdN6g7GsnSTd2VGq', 6, '', 'Mz6HnURMGsh3awGXqvl1XIQ34mYXmQqergx8nimhXGKkIm2Ga4xi8uB5pyfR', 1, '2015-11-02 05:46:10', '2015-12-27 21:28:13'),
(7, 'SALES-0001', '7.jpg', 'Sales Assistant', 'sales_assistant@legacy.com', '1234567', '', 0, 0, '', '', '', 'sales_assistant@legacy.com', '$2y$10$GarMf.Po9OmOuo3.CRY8ceSMY.rZxls79D.veazbg1jgYKjUu32vO', 7, '', NULL, 1, '2015-11-02 05:46:51', '2015-12-08 02:44:36'),
(8, '69000007', 'default_image.jpg', 'Alvin Nathan', 'alvin.nathan@advisors.aviva-asia.com', '94878277', '', 0, 0, '', '', '', 'alvin.nathan@advisors.aviva-asia.com', '$2y$10$Pa6Ptbv8nKvy9dK.GUArYOTKA4VMetLgPyPKCxr/PrQps9hJkTaQO', 8, 'm', 'q1xHJqGYL9vDG6xlaIgRm2SUtxGpKnZ2pLRr5n53eWsfpmA4vZwZndYtidZX', 1, '2015-11-02 05:53:59', '2015-12-21 02:41:42'),
(10, '69000023', 'default_image.jpg', 'K Joshi', 'kjoshi@legacy.com', '2232322333', '', 0, 0, '', '', '', 'kjoshi@legacy.com', '$2y$10$j.TnXS2pUVSGhty5dIVxOemFNcAMQktw79vlPep9BNimEmE3B4SMq', 8, 'm', 'f8IUkFq9oSZwyVuTjSWmqgwbg9QzdpDStVmil56t23nVSq0tbhh8NmHJPlhD', 1, '2015-11-02 06:36:10', '2015-12-17 02:24:14'),
(11, '69000024', 'default_image.jpg', 'Terrence Lee', 'terrencelee@legacy.com', '92310230', '', 0, 0, '', '', '', 'terrencelee@legacy.com', '$2y$10$5EPoXokDO8O00htRNkV.zuC3JJ3aWXxX/VCXNhHwusBot2S5x.ESG', 8, 'm', NULL, 1, '2015-11-02 06:39:54', '2015-11-01 22:39:54'),
(12, '69000026', 'default_image.jpg', 'Rachel Lorin', 'rachellorin@yahoo.com', '96868906', '', 0, 0, '', '', '', 'rachellorin@yahoo.com', '$2y$10$4JMHTrL.Q3aveqCghupZmO.Ph1JwozDG7kcmDU8LxKg6aG1dVGsE6', 8, 'f', 'L2bnAOtvWNGj4AAykUNh9dxrlQhijBpQ4vK4Onz0U8z86EyYgAioZvPu27gY', 1, '2015-11-02 06:41:34', '2015-12-02 01:04:48'),
(13, '69000025', 'default_image.jpg', 'Gideon Lee', 'gideonlee@legacy.com', '94502885', '', 0, 0, '', '', '', 'gideonlee@legacy.com', '$2y$10$Vd2beTBOihLNvblR04nrJ.1sUTNIwXVYA/h9v4mVo/mciunr2jSc6', 8, 'm', 'bH5Yha1JhTvuxNNloLogLU3Ejk6pLClyHkGOtBzPFAOB985HOU3SY58bNtgc', 1, '2015-11-02 06:48:14', '2015-11-02 23:51:02'),
(14, '69000078', 'default_image.jpg', 'Ivan Chew', 'ivanchew@yahoo.com', '91712766', '', 0, 0, '', '', '', 'ivanchew@yahoo.com', '$2y$10$QoNsH0yklXE6L7oSnE0E8.H4.0KaVGUzwV4oNoq3PGTNOTgxD13mW', 8, 'm', 'WQA7JZxJiZNvFiSQ1G15tYSM7NPJc90IlSVGPgBOiuKSz3mqBSf2m1J7T9sE', 1, '2015-11-02 06:50:23', '2015-12-17 01:26:32'),
(15, '69000121', 'default_image.jpg', 'Mervyn Chew', 'mervynchew@legacy.com', '98482872', '', 0, 0, '', '', '', 'mervynchew@legacy.com', '$2y$10$7pKsVOSfrjeyM1MD08d7Furm5b7P4Dh9M8FSDI0G5g1z7kngIW7JK', 8, 'm', 'RUZ4suqRS1ExeAc55NuHBYK81zJUBRms6zoKIawopBz7ymuhGYSgVZmbcZWc', 1, '2015-11-02 06:51:49', '2015-12-18 00:08:24'),
(16, '69000147', 'default_image.jpg', 'Louis Lim', 'louislim@legacy.com', '96817362', '', 0, 0, '', '', '', 'louislim@legacy.com', '$2y$10$nqEr8TrhMc2/cggTsEOTOeFB8bsMbeQF3zs2lP1m28OfQoooO0mdC', 8, 'm', '8rLZlMvmV84l6ukbTKGjGOJ8wS1cng7BdAT6tqmofIkUcVQblZ8OZlxiv5TQ', 1, '2015-11-02 06:53:10', '2015-12-17 01:40:26'),
(17, '69000130', 'default_image.jpg', 'Tan Teck Neng', 'tanteckneng@legacy.com', '91852076', '', 0, 0, '', '', '', 'tanteckneng@legacy.com', '$2y$10$Z6taBCf7dWyphJtnDBJICubur868I3LLAOscfafskY3YriKAnrROO', 8, 'm', 'XmLpzKQfNAe99rnTRy3e16hFzVlWqUnOn5j4FWnfmCCxqnkFFJNm02FKLazE', 1, '2015-11-02 06:55:19', '2015-12-11 00:52:33'),
(18, '69000131', 'default_image.jpg', 'Wilson Chan', 'wilsonchan@legacy.com', '93886269', '', 0, 0, '', '', '', 'wilsonchan@legacy.com', '$2y$10$1Dp4CZqsGKDt.WZtffl8meghtI4P05phIZabYlBOvd.OJlKy.EsRe', 8, 'm', 'blZGaWbGjuBBNeBGQZlyRHq67igG24QOhWwxfM34uLjxTWflDTFGTet28hzs', 1, '2015-11-02 06:56:47', '2015-11-02 23:52:45'),
(19, '69000133', 'default_image.jpg', 'Chris Ang', 'chrisang@legacy.com', '98778902', '', 0, 0, '', '', '', 'chrisang@legacy.com', '$2y$10$uCRgFY6iGWTqMzjX.bNHkucBL40eXI1Wi3cRrHhXyoBpLVdr65GNC', 8, 'm', NULL, 1, '2015-11-02 06:58:03', '2015-11-24 07:04:35'),
(20, '69000203', 'default_image.jpg', 'Vivian Chua', 'vivianchua@legacy.com', '97864820', '', 0, 0, '', '', '', 'vivianchua@legacy.com', '$2y$10$dBQUGzM8K.XWqMcgWs/Sx..6h1MLlhLwbEqH9xS1n5YV8d3gDVuQe', 8, 'm', NULL, 1, '2015-11-02 06:59:28', '2015-11-24 07:04:30'),
(21, '69000027', 'default_image.jpg', 'MARNI BTE RAHMAT', 'marni.rahmat@gmail.com', '3222322', '', 0, 0, '', '', '', 'marni.rahmat@gmail.com', '$2y$10$x41Fmx8mJkAkcFycdTuJsu0fsDPfNkV47n5kfHu2vzC0xC5V12836', 8, 'm', 'OaNKBQTcCxSVhUwUHmULilzGqATao38d3BV2NvjE5DoTF6ZFdmQliw71m81P', 1, '2015-11-03 08:44:12', '2015-12-17 01:41:46'),
(22, '69000028', 'default_image.jpg', 'NGO ENG HUAT', 'ngoenghuat@legacy.com', '90936996', '', 0, 0, '', '', '', 'ngoenghuat@legacy.com', '$2y$10$4Zc.kQIzOwEZXcCtEF5oM.Lvusnok3TP7SQiD1R8I.37pT05v4P3u', 8, 'm', 'QZIAReXIoJPsYCB9WXNNCjGh4HN5pk3x69l00wYtp3YAoVJH6iYmEls2JjYX', 1, '2015-11-03 08:46:11', '2015-12-17 02:04:27'),
(23, '2323223', 'default_image.jpg', 'sample', 'sample@yahoo.com', '233223223', '', 0, 0, '', '', '', 'sample@yahoo.com', '$2y$10$UJrEsEfrp0LH.36Tkx3DLuNFk06Zwx6dhCnDMfFsdw6/8JT453mb2', 8, 'm', NULL, 2, '2015-11-04 08:09:41', '2015-11-04 00:12:57'),
(24, '312312231', 'default_image.jpg', 'sample', 'sample@yahoo.com.ph', '3213212', '', 0, 0, '', '', '', 'sample@yahoo.com.ph', '$2y$10$DRdGtiy.wsU37IsXpWyrW.JssKMjE/wPxQv0MN4./DSLX8b42F6Kq', 8, 'f', NULL, 2, '2015-11-04 08:14:36', '2015-11-04 00:15:05'),
(25, '3123221', 'default_image.jpg', 'sample', 'sample@yahoo.com.phh', '23123212', '', 0, 0, '', '', '', 'sample@yahoo.com.phh', '$2y$10$p53LsowXtGJukTl0ZtUYOecBA7tTYWhPCB6rMhJ4OXuz.9CBfz.J2', 8, 'f', NULL, 2, '2015-11-04 08:18:57', '2015-11-04 00:19:32'),
(26, '23321', 'default_image.jpg', 'sample', 'sample23232@yahoo.com', '2312312312', '', 0, 0, '', '', '', 'sample23232@yahoo.com', '$2y$10$txTAXYX6TZeNdOTbUyUihexEEico6lpLWfO8/QUI1qm8iE1RYF4IG', 8, 'f', NULL, 2, '2015-11-04 09:00:37', '2015-11-04 01:20:02'),
(27, '33233', 'default_image.jpg', 'sample', 'sampl5555e@yahoo.com', '312322', '', 0, 0, '', '', '', 'sampl5555e@yahoo.com', '$2y$10$UN9kZFm8AiH3CuxcYfGhuekXw1uqFqY90A82YqRoGvNoCKQPzdvqi', 8, 'm', NULL, 2, '2015-11-05 06:59:23', '2015-11-18 09:38:48'),
(45, '23233123', 'default_image.jpg', 'sample_supervisor_no_code', 'sample_supervisor_no_code@yahoo.com', '32322', '', 0, 0, '', '', '', 'sample_supervisor_no_code@yahoo.com', '$2y$10$yfe7XRLF/9JKLXNGJEf32eoqPp0SLjy7yG3lfG5i0vMdvJJjNp0Ti', 8, 'm', NULL, 2, '2015-11-12 10:09:40', '2015-11-17 02:05:27'),
(46, '32332', 'default_image.jpg', 'sample_advisor', 'sample_advisor@yahoo.com', '3223232', '', 0, 0, '', '', '', 'sample_advisor@yahoo.com', '$2y$10$r7W.X69M.OmDiT65aVMneeOoQfR2NJnn5vPFBgoq0OMPrv.n3EnLW', 8, 'f', NULL, 2, '2015-11-12 10:19:15', '2015-11-17 02:05:27'),
(47, 'ddd', 'default_image.jpg', 'dd', 'ddd@yahoo.com', '323232', '', 0, 0, '', '', '', 'ddd@yahoo.com', '$2y$10$/XSjQF4HxNz1BUPF7P/JUev6fhFVGvfA8D.O342broB2YkPoT/fAm', 8, 'f', NULL, 2, '2015-11-19 07:06:14', '2015-11-19 07:42:03'),
(48, '32323', 'default_image.jpg', '3232', '23232@yahoo.com', '32232', '', 0, 0, '', '', '', '23232@yahoo.com', '$2y$10$oSKh.p1PSJ88WPgLXsxgVua30VHAnDNZem.Sbcxldpjg7UajmHVE2', 8, 'f', NULL, 2, '2015-11-19 07:07:35', '2015-11-19 07:42:00'),
(49, '43543', 'default_image.jpg', '543543', 'ceoi@legacy.com', '54354354', '', 0, 0, '', '', '', 'ceoi@legacy.com', '$2y$10$WsrNj.gvZULMB7r1s4/HK./9D6EEBTugda/QzTMfp23XpNAA9M6s6', 8, 'm', NULL, 1, '2015-11-26 08:19:14', '2015-11-26 00:19:14'),
(50, '12345768', 'default_image.jpg', 'Doodz Garcia', 'edu@com.com', '1234567', '', 0, 0, '', '', '', 'edu@com.com', '$2y$10$Ih0iAlAfmc6yHYAF9/ODVe68UMDnXft78lXLqaz7H9NoLiT5S6R.a', 8, 'm', NULL, 1, '2015-11-26 08:26:53', '2015-11-26 00:26:53'),
(51, '23112', 'default_image.jpg', 'sdas', 'adDs@yahoo.com', '32323223', '', 0, 0, '', '', '', 'adDs@yahoo.com', '$2y$10$rGI0JhLipoXT/pCeCq3ufuBEG8qzRynrwm8LERWq11ag.z4GyZ186', 8, 'f', NULL, 1, '2015-11-26 08:29:47', '2015-11-26 00:29:47'),
(52, '3223133', 'default_image.jpg', 'sample', 'sample@yahoo.cmddd', '2332132', '', 0, 0, '', '', '', 'sample@yahoo.cmddd', '$2y$10$Cm14LgynfOxrNfmvo.MdKum0Yomwu4KsE1ksTZ2NO7hNrrQPv7mu.', 8, 'm', NULL, 1, '2015-11-17 08:31:00', '2015-11-17 00:31:00'),
(53, '1234567', 'default_image.jpg', 'Doodz', 'doodz@gmail.com', '23456', '', 0, 0, '', '', '', 'doodz@gmail.com', '$2y$10$tPBkyExEGMX.778ONGWE/OtQaLgBooMZWh8pfSvWRpp6i7pE3MIz2', 8, 'm', NULL, 1, '2015-11-26 09:20:23', '2015-11-26 01:20:23'),
(54, 'Aga', 'default_image.jpg', 'dd', 'dddddd@yahoo.com', '22323', '', 0, 0, '', '', '', 'dddddd@yahoo.com', '$2y$10$Zfb01EJrJoI29GTsZpnc3uXCi4HevMlZSvIZ2SajrRDkM49DokPs6', 8, 'm', NULL, 1, '2015-11-26 09:32:59', '2015-11-26 01:32:59'),
(55, 'dsdsd', 'default_image.jpg', 'ddd', 'sdsadad@yahoo.com', '22323', '', 0, 0, '', '', '', 'sdsadad@yahoo.com', '$2y$10$35UKTXqmS.5mnXqpZHTEpOGzG/4q3kpq/6THEzfSVH.XIj1k7Nnw6', 8, 'm', NULL, 2, '2015-11-26 09:37:05', '2015-11-26 02:31:41'),
(56, '2321312', 'default_image.jpg', '3232', 'sample44444444@yahoo.com', '3323232', '', 0, 0, '', '', '', 'sample44444444@yahoo.com', '$2y$10$NyvNLTI4.Qeg0Wg2Oew4R.BcrNfScH/1ASVmqPTBhvRZSX9ijWYlu', 8, 'm', NULL, 2, '2015-11-26 09:38:53', '2015-11-26 02:31:41'),
(57, '4234', 'default_image.jpg', 'lean', 'ertge4@fsfs.com', '54354354', '', 0, 0, '', '', '', 'ertge4@fsfs.com', '$2y$10$0oHLWJimVAm0u92WIgw/Lu1c9tc4/tuKKSLfw8yA6G5oqHE.CcAiS', 8, 'm', NULL, 2, '2015-11-26 09:42:07', '2015-11-26 02:31:41'),
(58, '222222', 'default_image.jpg', 'lean', 'weee@legacy.com', '54354354', '', 0, 0, '', '', '', 'weee@legacy.com', '$2y$10$lHLIwCFLwhSf3NUeH0QZpObWB.U1rA68UTIZmDuI0uOSvCFbY2hVq', 8, 'm', NULL, 1, '2015-12-01 09:05:11', '2015-12-01 01:05:11'),
(59, '43244234', 'default_image.jpg', 'dsa', '22@legacy.com', '4324324', '', 0, 0, '', '', '', '22@legacy.com', '$2y$10$Pn91/HWw0VDpb421XVDi5uyVdyfrgW.ihZBqmDQawpjPksAkfnMj2', 8, 'm', NULL, 1, '2015-12-01 09:09:38', '2015-12-01 01:09:38'),
(60, '4324234', 'default_image.jpg', '432432', '423432@dad.com', 'rewrwerew', '', 0, 0, '', '', '', '423432@dad.com', '$2y$10$.tIFXj5TYQRKOzCsCvyph.7.I3TMut/eVDcdV0GnOf7RU0IgJstZu', 8, 'f', NULL, 1, '2015-12-01 09:11:56', '2015-12-01 01:11:56'),
(61, 'sample', 'default_image.jpg', 'sample', 'sample323223233@yahoo.com', '23232', '', 0, 0, '', '', '', 'sample323223233@yahoo.com', '$2y$10$4lgUrcKpxzm3WafZ8VMoVek/SM.1QOH8dR5bVqMXl4SpqXuenYlBy', 8, 'f', NULL, 1, '2015-12-01 09:23:25', '2015-12-01 01:23:25'),
(62, '31232', 'default_image.jpg', 'sample', 'samlpldddddd@yahoo.com', '333', '', 0, 0, '', '', '', 'samlpldddddd@yahoo.com', '$2y$10$CptSj4N788Nzw.txJE/eSu2OYXk3xRhg5CfBOXIYg2B89NYNbE2Xa', 8, 'm', NULL, 1, '2015-12-01 09:27:10', '2015-12-01 01:27:10'),
(63, '23323223', 'default_image.jpg', 'sample', 'samplesdadsad@yahoo.com', '323232', '', 0, 0, '', '', '', 'samplesdadsad@yahoo.com', '$2y$10$nxpHBY08Vsp.o7pgfN2Uh.z.Lmx/zof4Xv8LhjwrSIeaiGfsQqKaa', 8, 'f', NULL, 1, '2015-12-01 09:27:59', '2015-12-01 01:27:59'),
(64, '3332323232', 'default_image.jpg', 'sddsd', 'partner@yahoo.com', '23232', '', 0, 0, '', '', '', 'partner@yahoo.com', '$2y$10$F.QtqsGiFnxF4Y3yxfF/K.retXM5L4AM4J02tZy5HFO0WMXsZ8wGy', 8, 'f', 'i0QQpfQj11pTBAMdenAl0bDYr2RsYEokDHYHZ1mRpwoG636NXdK6Mdgqc4Qh', 1, '2015-12-01 09:31:09', '2015-12-09 01:15:20'),
(65, '32321321', '65.jpg', 'sample partner', 'sample_partner@yahoo.com', '23232233232', '', 0, 0, '', '', '', 'sample_partner@yahoo.com', '$2y$10$g88N.DQYR3t9nvrY1GsxIO.MrmDm2JXMRG6EfB.4HdYEWySp//JxS', 8, 'm', 'mfg1tTuVlD3ijQYRCiTMKfIIKcN5qnVSYoVbZb5XdDnLdUk7DGKr38rWZbQE', 1, '2015-12-02 07:28:14', '2015-12-04 02:12:44'),
(66, '23213', '66.jpg', 'dsds', 'dadsdasd@yahoo.com', '323232', '', 0, 0, '', '', '', 'dadsdasd@yahoo.com', '$2y$10$6PDOGozZzN0GAHGXLO/8D.WZQhTlMXfNFrgq4Q.PiA6vuyjqcpC96', 8, 'm', NULL, 1, '2015-12-04 03:38:16', '2015-12-04 01:31:11'),
(67, '2323', '67.jpg', 'sds', 'dsdsd@yahoo.com', '4444', '', 0, 0, '', '', '', 'dsdsd@yahoo.com', '$2y$10$2E.3wgDh8u5NyhRdYL4dv.X1AOhxxmTqXwvU2GlxPpbjPs8s26fKi', 8, 'f', NULL, 1, '2015-12-07 09:16:48', '2015-12-08 01:58:12'),
(68, '23322323555', '68.jpg', 'sample', 'sample232322@yahoo.com', '2323232', '', 0, 0, '', '', '', 'sample232322@yahoo.com', '$2y$10$.APkHXHM8efs3t858yjH5uaf8YRDbovi5mVRHHbkWP326Aiw0Anb.', 3, '', NULL, 1, '2015-12-08 03:49:47', '2015-12-07 22:50:25'),
(69, '3232356565', 'default_image.jpg', '2332', 'sample232324444@yahoo.com', '2223232', '', 0, 0, '', '', '', 'sample232324444@yahoo.com', '$2y$10$WPujfRXkCSP8TFAmYQwCpuBM8qcw7GZWUCdrs2Xt61TOiMI.M1lbS', 8, 'm', NULL, 1, '2015-12-09 06:46:04', '2015-12-08 22:46:04'),
(70, '32322323232322323', 'default_image.jpg', 'sample', 'sample23223232323@yahoo.com', '232232323', '', 0, 0, '', '', '', 'sample23223232323@yahoo.com', '$2y$10$.tmlK1w.vr/.oAgWnts2buXIE7yqV1JsiorSjSzN1xYSYkAWTm4hK', 8, 'm', NULL, 1, '2015-12-09 06:48:17', '2015-12-08 22:48:17'),
(71, '232322365656565', 'default_image.jpg', 'sample', 'sample3232323@yahoo.com', '233323', '', 0, 0, '', '', '', 'sample3232323@yahoo.com', '$2y$10$A//U4mDsWNe.j.8lrLzAGOSfbrVbs2dtE2SbK4pOb3lcil/dP9MM6', 8, 'm', NULL, 1, '2015-12-09 06:50:31', '2015-12-08 22:50:31'),
(72, '2232322322322332', 'default_image.jpg', 'sample', 'sample23232223233223@yahoo.com', '2322233232', '', 0, 0, '', '', '', 'sample23232223233223@yahoo.com', '$2y$10$eLQzC38VRtC.Ny4xk2Mul.AOAM/r.J5v3RbAewP3nsxXgw9K0SIKK', 8, 'm', NULL, 1, '2015-12-09 06:52:57', '2015-12-08 22:52:57'),
(73, '232232232323', 'default_image.jpg', 'dsdsdsds', '2112323322323323@yahoo.com', '2322322323', '', 0, 0, '', '', '', '2112323322323323@yahoo.com', '$2y$10$3BEIPy4yS045KtGaUOEzwuIOeQfLFZ3u7FRcbtYhCOsiRpb45SMt.', 8, 'f', NULL, 1, '2015-12-09 06:54:39', '2015-12-08 22:54:39'),
(74, '232232323232', 'default_image.jpg', '2322332223', '232323232323@yahoo.com', '323232323', '', 0, 0, '', '', '', '232323232323@yahoo.com', '$2y$10$.A04Khl5.9aDnJgdTPuBkON4MGSpmZjO3TOwPg0nZMO2EPOS2gl8a', 8, 'f', NULL, 1, '2015-12-09 06:55:42', '2015-12-08 22:55:42'),
(75, '222322322', 'default_image.jpg', 'sample with group', 'samplegroup@yahoo.com', '2322332', '', 0, 0, '', '', '', 'samplegroup@yahoo.com', '$2y$10$qPv7Qrp/SQYzJXTGcgkhJ.U0l2Za3rcbVAqdKZRobnUcp4HhhEQKe', 8, 'm', NULL, 1, '2015-12-09 07:06:46', '2015-12-08 23:06:46'),
(76, '2232323233232', 'default_image.jpg', '2323223223', '23232233232232@yahoo.com', '44444', '', 0, 0, '', '', '', '23232233232232@yahoo.com', '$2y$10$cw/G5XZMBlJrHeJUj0whtelyoHfrDlttFH6Ajh7Dia9.gniwWIP.S', 8, 'm', NULL, 1, '2015-12-09 08:19:28', '2015-12-09 00:19:28'),
(77, '23321323321', 'default_image.jpg', '23123123', '3123123@yahoo.com', '2322323', '', 0, 0, '', '', '', '3123123@yahoo.com', '$2y$10$Vk2kZgnXKC5c16n7h6sviO2rJqAsekq2G7LZ593Y9j.JYFnZu2PoW', 8, 'm', NULL, 1, '2015-12-09 08:23:18', '2015-12-09 00:23:18'),
(78, '23232', 'default_image.jpg', '2331', '312312@yahoo.com', '2323', '', 0, 0, '', '', '', '312312@yahoo.com', '$2y$10$mdTGqfig8qPwZdl7lk0NFuLRAnBoMnqDUVv3XyjfNRN9WlOEPcGaC', 8, 'f', NULL, 1, '2015-12-15 07:51:18', '2015-12-14 23:51:19'),
(79, NULL, '', '', '444@444.444', '', '', 0, 0, '', '', '', '', 'password', 0, '', NULL, 1, '2015-12-22 06:45:30', '2015-12-21 22:45:30'),
(80, NULL, '', '', '444@444.444', '', '', 0, 0, '', '', '', '', 'password', 0, '', NULL, 1, '2015-12-22 06:46:58', '2015-12-21 22:46:58'),
(81, NULL, '', '', 'sample@gmail.com', '', '', 0, 0, '', '', '', '', 'sample', 0, '', NULL, 1, '2015-12-22 06:55:59', '2015-12-21 22:55:59'),
(82, NULL, '', '', 'tejerogerald1990@gmail.com', 'tejerogerald1990@gmail.com', '', 0, 0, '', '', '', 'tejerogerald1990@gmail.com', 'password', 0, '', NULL, 1, '2015-12-22 07:01:06', '2015-12-22 07:03:18'),
(83, NULL, '', '', 'farfar@gmail.com', '', '', 0, 0, '', '', '', 'farfar@gmail.com', '$2y$10$TKG4uQkVbJu9kfLMq5/79u3JgNAoeqjpOPsNukVvEvvwaOb8kELWC', 0, '', NULL, 1, '2015-12-22 07:07:21', '2015-12-21 23:07:21'),
(84, NULL, '', '', 'ddddddddddd@add.com', 'ddddddddddd@add.com', '', 0, 0, '', '', '', 'ddddddddddd@add.com', '$2y$10$eF6a98d9z/unqZpWslWZPudpAxaUwTkohxZePLF34oNLgp4EyeHLK', 0, '', NULL, 1, '2015-12-22 08:00:24', '2015-12-22 08:01:39'),
(85, NULL, '', 'Gerald', 'Sample21@gmail.com', '', '', 0, 0, '', '', '', 'Sample21@gmail.com', '$2y$10$NfVMuAydCkdySknQR5Hrku0R1vo.JnuvNWzLdBUZp2/WSIibHxdI.', 0, '', 'dnpq3dB16lGjl3cxO7fwo1dmLGdjumaLWd88VIRETenNxbZM9hLGqIBWqJ2g', 1, '2015-12-22 08:04:43', '2015-12-22 00:07:05'),
(86, NULL, '', '', '', '', '', 0, 0, '', '', '', '', '$2y$10$NQj1z6nVRGKLnySBvYI93e58EqFvq1AzovMmE8ARU6EJAWX3G0nUW', 0, '', 'klo26st84E4hCXJ0yg7OEJykJ63IKf7GHUnDpxkM', 1, '2015-12-22 08:10:03', '2015-12-22 00:10:03'),
(87, NULL, '', 'Gerald', 'samplesample@gmail.com', '', '', 0, 0, '', '', '', 'samplesample@gmail.com', '$2y$10$5GzueU3WA/.Z4ccISLZdeeLvAkzMoH..YY593rHBeG1JF.oFXnVFC', 0, '', 'wGs3J5y0pZVVbjxEan8Nvd2rvdW4GYrdPGsax17Ng33UnEgmzQW5qPfvdDNP', 1, '2015-12-23 03:33:47', '2015-12-22 22:43:56'),
(88, NULL, '', '', 'gggggggg@ggggg.gggg', '', '', 0, 0, '', '', '', 'gggggggg@ggggg.gggg', '$2y$10$WAPUwxOJK12E9C.9E3DrtepGJ/1YwmGYCpkHRyWclSFfwfotfI61W', 0, '', 'G8LUJdDxX9CoY91Yhm6blCh4iW150alTSlIPhwIn', 1, '2015-12-23 06:53:57', '2015-12-22 22:53:57'),
(89, NULL, '', '', 'reterter@efwe.werwer', '', '', 0, 0, '', '', '', 'reterter@efwe.werwer', '$2y$10$lc8.xPOFG4QYboJuP3Pg2eGdrlIzxO5w1ED3z635FHTy8APjDxOGK', 0, '', 'G8LUJdDxX9CoY91Yhm6blCh4iW150alTSlIPhwIn', 1, '2015-12-23 10:23:54', '2015-12-23 02:23:54'),
(90, NULL, '', '', 'reterter@efwe.werwer', '', '', 0, 0, '', '', '', 'reterter@efwe.werwer', '$2y$10$48rXbeTB85tFquYkylWok.OG6TiB4bpGapU/.bA41RGmCl97S0/gG', 0, '', 'G8LUJdDxX9CoY91Yhm6blCh4iW150alTSlIPhwIn', 1, '2015-12-23 10:24:10', '2015-12-23 02:24:10'),
(91, NULL, '', '', 'accounting_assistant@lsss.ee', '', '', 0, 0, '', '', '', 'accounting_assistant@lsss.ee', '$2y$10$SPeMJQFACGUTJdiFVUE2OOsTXEioXIFZKjsYGRY0/eps3JXfWPwBK', 0, '', 'Jsx1J0ebcKC2mvD1Ir7y5LtGoo8zr3OYIcstVfem', 1, '2016-01-04 07:05:45', '2016-01-03 23:05:45'),
(92, NULL, '', '23232', '32313@yahoo.com', '2323223', '', 0, 0, '', '', '', '32313@yahoo.com', '$2y$10$YT7jl8YTcyGcm1slU0Na7.C63BZhKf63OvRVLj4Yfwf02Kfx4hqEm', 0, '', NULL, 1, '2016-01-04 07:05:49', '2016-01-03 23:05:49'),
(93, NULL, '', '23232', '231321@yahoo.com', '23223232', '', 0, 0, '', '', '', '231321@yahoo.com', '$2y$10$BUk227OclMyqSOuUbKZUherMStddYIewCq1AUUGfZJz8znrpUVnEe', 0, '', NULL, 1, '2016-01-04 07:06:31', '2016-01-03 23:06:31'),
(94, NULL, '', '', 'ddddddddddd@ddddddddd.com', '', '', 0, 0, '', '', '', 'ddddddddddd@ddddddddd.com', '$2y$10$Nzwcqx0xM15VRcy4qpHAN.Er6OFWQtyBoq6tBERxrGrZUFsmIF6q6', 0, '', 'Jsx1J0ebcKC2mvD1Ir7y5LtGoo8zr3OYIcstVfem', 1, '2016-01-04 07:07:02', '2016-01-03 23:07:02'),
(95, NULL, '', '2323', '2313131@yahoo.com', '2322332', '', 0, 0, '', '', '', '2313131@yahoo.com', '$2y$10$KdNaj3lkAucxqLJcjTKcFu0L8HyYbMSfozxnYSuy4QndFWCzKSkhu', 0, '', NULL, 1, '2016-01-04 07:07:09', '2016-01-03 23:07:09'),
(96, NULL, '', '', 'lorem@gmaial.com', '', '', 0, 0, '', '', '', 'lorem@gmaial.com', '$2y$10$MXf2kePqAoWIxK/39olzSuPoRX.mPzqgctB0xAVt80E2yF0/bwtt.', 0, '', 'Jsx1J0ebcKC2mvD1Ir7y5LtGoo8zr3OYIcstVfem', 1, '2016-01-04 07:10:40', '2016-01-03 23:10:40'),
(97, NULL, '', '2323', '2313131@yahoo.com656', '2322332', '', 0, 0, '', '', '', '2313131@yahoo.com656', '$2y$10$qsdK7ir1s8UJ4Vgc1Vyzku.vepQSV7B74ICyf.Llq.kCglfOnFIby', 0, '', NULL, 1, '2016-01-04 07:44:11', '2016-01-03 23:44:11'),
(98, NULL, '', 'sample', 'sample@yahoo.com333', '32312321312', '', 0, 0, '', '', '', 'sample@yahoo.com333', '$2y$10$AnOIF7yurDB5GN8TSagdgOdPllY4Hx4P9lfUwWucbSrFRw2I.BcSy', 0, '', NULL, 1, '2016-01-04 07:44:37', '2016-01-03 23:44:37'),
(99, NULL, '', '232322323', '2323232@yahoo.com', '2323232223', '', 0, 0, '', '', '', '2323232@yahoo.com', '$2y$10$mZ8CerJ2wqzK1sTu7KbSxOUqsPWlpmdpSc3/vuC.5hwP8htJXJYsS', 0, '', NULL, 1, '2016-01-04 07:45:56', '2016-01-03 23:45:56'),
(100, NULL, '', '232323313', '2131213123@yahoo.com', '2323323232', '', 0, 0, '', '', '', '2131213123@yahoo.com', '$2y$10$ZTisqIr/ovJQOaPiHtRWdO79T8iamAhI4Gm2qJIstpSwAd2gzc7jm', 0, '', NULL, 1, '2016-01-04 07:48:20', '2016-01-03 23:48:20'),
(101, NULL, '', 'sample_yakO', 'sample_yakO@yahoo.com', '22131223231', '', 0, 0, '', '', '', 'sample_yakO@yahoo.com', '$2y$10$R8zYmUOuo.N3HmTuChDQj.WpllsSd2iinJDU7pFUbVdsYN6CerUeO', 0, '', NULL, 1, '2016-01-04 07:55:37', '2016-01-03 23:55:37'),
(102, NULL, '', 'sample_yakO33333', 'sample_yakO@yahoo.com33333', '22131223231', '', 0, 0, '', '', '', 'sample_yakO@yahoo.com33333', '$2y$10$sKsm0OB9R8Qmm7CUPHzLN.NwkLR4UXEIpRSE7uY8/fSbLbS45iOc2', 0, '', NULL, 1, '2016-01-04 07:57:00', '2016-01-03 23:57:00'),
(103, NULL, '', 'sample_yakO33333', 'sample_yakO@yahoo.com333334444', '22131223231', '', 0, 0, '', '', '', 'sample_yakO@yahoo.com333334444', '$2y$10$mPiKx5hoD6rjOWluMF1Q9u.gRvDTsor1Gfl8yFGK2WPTSrQQIOT/W', 0, '', NULL, 1, '2016-01-04 07:57:36', '2016-01-03 23:57:36'),
(104, NULL, '', 'sample_yakO', 'sample_yakO@yahoo.com222', '22131223231', '', 0, 0, '', '', '', 'sample_yakO@yahoo.com222', '$2y$10$Ot/nf33i0566a828cjkhVOFXfoc5ePH6zp62dKGkIk4.MlzwkfMNq', 0, '', NULL, 1, '2016-01-04 07:58:54', '2016-01-03 23:58:54'),
(105, NULL, '', '23232444', '213213444444@yahoo.com4444', '13123121312', '', 0, 0, '', '', '', '213213444444@yahoo.com4444', '$2y$10$P/KmaLqXKPyjbYPbiJjvVewGzLB0Pj0snhvX07vGFYKsMKKoOtS8q', 0, '', NULL, 1, '2016-01-04 08:02:39', '2016-01-04 00:02:39'),
(106, NULL, '', 'dasdassdas', 'sadsadassdsasdasd@yahoo.com', '2312331232311', '', 0, 0, '', '', '', 'sadsadassdsasdasd@yahoo.com', '$2y$10$4F3Mjz5Og8sSBE3fBaB11u8XFCgjORn90mw4xJ7G/o233T7OKHkJq', 0, '', NULL, 1, '2016-01-04 08:25:59', '2016-01-04 00:25:59'),
(107, NULL, '', 'dasdassdas4', 'sadsadassdsasdasd@yahoo.com4', '2312331232311', '', 0, 0, '', '', '', 'sadsadassdsasdasd@yahoo.com4', '$2y$10$rCYdtVapwN2URf/5UWeiEe492aOGxqRs7iE.X2fvsY2ibYfkryNnm', 0, '', NULL, 1, '2016-01-04 08:26:10', '2016-01-04 00:26:10'),
(108, NULL, '', '33231312344', '12321321@yahoo.com44', '231231223113', '', 0, 0, '', '', '', '12321321@yahoo.com44', '$2y$10$fiJ/gSdpmqhBljSyNvZuO.mNqc1zi2DlyP/fcnRdBKbEJsvuaBokK', 0, '', NULL, 1, '2016-01-04 08:28:43', '2016-01-04 00:28:43'),
(109, NULL, '', '32123', '232322323@yahoo.com', '2321321323', '', 0, 0, '', '', '', '232322323@yahoo.com', '$2y$10$3Z8kjIbGyzpNPbjZxCbmWOSL.9TWCl6FaY3G9qivmCD1t10lkgyGS', 0, '', NULL, 1, '2016-01-04 09:01:00', '2016-01-04 01:01:00'),
(110, NULL, '', 'yuck luck', 'yuck_luck@yahoo.com', '321321312321', '', 0, 0, '', '', '', 'yuck_luck@yahoo.com', '$2y$10$y3VZw6Xdse6GJre3fP4mR.WaUO65dNHJsBr.shk/Q8L5vL2zjgUtW', 2, '', NULL, 1, '2016-01-04 09:26:08', '2016-01-04 01:26:08'),
(111, NULL, '', 'o luck', 'oluck@yahoo.com', '23212312312', '', 0, 0, '', '', '', 'oluck@yahoo.com', '$2y$10$/OGqS6FkwY6t5KRUav3/yuzvyVrQz6eFOQTPeGEZB6opMhvH.eSy2', 1, '', NULL, 1, '2016-01-04 09:27:16', '2016-01-04 01:27:16'),
(112, NULL, '', 'sample_yak', 'sample_yak@yahoo.com', '231232123', '', 0, 0, '', '', '', 'sample_yak@yahoo.com', '$2y$10$kdj15RoqAO/TVHNxqqLHGemqTQohxUik47bzwRlwQevNrX0gRu0qO', 2, '', NULL, 1, '2016-01-04 09:28:38', '2016-01-04 01:28:38'),
(113, NULL, '', '233123333', '2312322@yahoo.com', '223131321', '', 0, 0, '', '', '', '2312322@yahoo.com', '$2y$10$MGJBHtiMRqvXvIfwWl/0XOTZIcYW8UVTrkpjXH33.EmOLKHOj2fAq', 2, '', NULL, 1, '2016-01-04 09:35:09', '2016-01-04 01:35:09'),
(114, NULL, '', 'sample_luck', 'sample_luck@yahoo.com', '2213123', '', 0, 0, '', '', '', 'sample_luck@yahoo.com', '$2y$10$Y0rddTTI7YKH/COxHEYAPOwVs/oLOMyDJDcwhRJuJmSe6/dnW.nMW', 1, '', NULL, 1, '2016-01-04 09:35:44', '2016-01-04 01:35:44'),
(115, NULL, '', 'doodz', 'doodz@yahoo.com', '231321312', '', 0, 0, '', '', '', 'doodz@yahoo.com', '$2y$10$A9Ec7w7MK0l8ZZNfVZpuNuy/XUNlnQZ8Ygj8MC7Q0Bswh1G74nneW', 2, '', NULL, 1, '2016-01-04 09:36:21', '2016-01-04 01:36:21'),
(116, NULL, '', 'dssd', 'dsadas@yahoo.com', '213212321332', '', 0, 0, '', '', '', 'dsadas@yahoo.com', '$2y$10$S8.oEe6i1KZgk/DCXVEHL..fCbtdOrIsjRm2C6/ieE.Wyq1RzD2J6', 2, '', NULL, 1, '2016-01-04 09:52:13', '2016-01-04 01:52:13'),
(117, NULL, '', 'mark', 'mark@yahoo.com', '231321123', '', 0, 0, '', '', '', 'mark@yahoo.com', '$2y$10$15r.LYmeIGFI88xEH1n/euMjqrrSiotZeSIIgo6NGD1w/gbFmO2EW', 1, '', NULL, 1, '2016-01-04 10:00:09', '2016-01-04 02:00:09'),
(118, NULL, '', '123132311444', '32131231231@yahoo.com', '2232322232', '', 0, 0, '', '', '', '32131231231@yahoo.com', '$2y$10$0dvxbaNxVxo2ICH0aRLPHumh39AqyFY5luCJrHpzGiKyBTHpfjLQK', 2, '', NULL, 1, '2016-01-05 03:36:30', '2016-01-04 22:26:10'),
(119, NULL, '', 'sdsad', 'dsada@yahoo.com', '232322332', '', 0, 0, '', '', '', 'dsada@yahoo.com', '$2y$10$7b1mhxMCl5QDSNt7SIYegeEP4HlmdqRYnRbkLipjrA5eHNfhqs.F.', 1, '', NULL, 1, '2016-01-05 06:35:10', '2016-01-04 22:35:10'),
(120, NULL, '', '31232321', '32312312231@yahoo.com', '222323232', '', 0, 0, '', '', '', '32312312231@yahoo.com', '$2y$10$iyPGLvRNOmtkR3smHweiwuBmvL4azUPBP2T0/xmhBzoYwfdeZFo0e', 2, '', NULL, 1, '2016-01-05 06:54:14', '2016-01-04 22:54:14'),
(121, NULL, '', '232123', '32123213@yahoo.com', '32323232', '', 0, 0, '', '', '', '32123213@yahoo.com', '$2y$10$gh/WuriXgAjkZfbOMw34AOLivCqbob1UOnbhE27a2m.Atf1dALVbm', 1, '', NULL, 1, '2016-01-05 06:57:58', '2016-01-04 22:57:58'),
(122, NULL, '', '23232', '212312@yahoo.com', '2132322', '', 0, 0, '', '', '', '212312@yahoo.com', '$2y$10$SRhm1.6wxQcQzW7ts1GXPuvsNxwpRFx4U7i0xfoyYc0YJ6R16W63q', 1, '', NULL, 1, '2016-01-05 06:59:15', '2016-01-04 22:59:15'),
(123, NULL, '', '3123223231', '21312312323132@yahoo.com', '213231', '', 0, 0, '', '', '', '21312312323132@yahoo.com', '$2y$10$XLpb/jr3eA41JFPGbwzY/emqfY9Fl0RoAZT.EXcLIn/Y6lbmaDwQy', 2, '', NULL, 1, '2016-01-05 07:01:26', '2016-01-04 23:01:26'),
(124, NULL, '', 'awdawdwa', 'dddd@rge.ret', 'ttttt', '', 0, 0, '', '', '', 'dddd@rge.ret', '$2y$10$jG.ueXmzNg8Vwg3cIsXJTeQxItIvrEFL0m6wxOgpfYp0Ox8/0NDkq', 2, '', NULL, 1, '2016-02-03 05:34:01', '2016-02-02 21:34:01'),
(125, NULL, '', 'Gerald', 'tejero@gmail.com', '0923121', '', 0, 0, '', '', '', 'tejero@gmail.com', '$2y$10$k6AH9DVMwYZZyh4ln0340O2eZD8BhQLbxZs3aKYL5kIsjuPzq4Pqa', 1, '', NULL, 1, '2016-02-03 06:14:51', '2016-02-02 22:14:51'),
(126, NULL, '', 'awdawdwa', 'awdwa@wqdqwd.qwewqe', '12312', '', 0, 0, '', '', '', 'awdwa@wqdqwd.qwewqe', '$2y$10$fio3m4wYzgWYSYPUNSJ5muL2g2WMmVA22JCz.oexKqXYadYBL3aL2', 1, '', NULL, 1, '2016-02-03 06:33:00', '2016-02-02 22:33:00'),
(127, NULL, '', 'awdwad', 'awddwa@erg.erre', '12312321', '', 0, 0, '', '', '', 'awddwa@erg.erre', '$2y$10$EllLVwzMyqCblsE0U595ae7hH1dgrpAhxD2HnlDkgKdNi9mUfYlfe', 1, '', NULL, 1, '2016-02-03 06:37:58', '2016-02-02 22:37:58'),
(128, NULL, '', 'awdawd', 'ddddddddddd@add.com2', '123123', '', 0, 0, '', '', '', 'ddddddddddd@add.com2', '$2y$10$UWVl/L.gX00HcGMVSmtgPezArvVGCLyq/Uo3TW/7NouzZ/G2T3l3q', 2, '', NULL, 1, '2016-02-03 06:41:11', '2016-02-02 22:41:11'),
(129, NULL, '', 'wadadw', 'aaw@efwe.sdr', '123123', '', 0, 0, '', '', '', 'aaw@efwe.sdr', '$2y$10$6vqkI5tvkK6u/qlYkqv3ROATtrIW.wBs9SQm7HWoF57lgDkiUcyNi', 1, '', NULL, 1, '2016-02-03 06:43:57', '2016-02-02 22:43:57'),
(130, NULL, '', 'wqewe', 'wqewq@4rege.rere', '123213', '', 0, 0, '', '', '', 'wqewq@4rege.rere', '$2y$10$ZRjKOyYTryYRSFlnTzNEvOY8vINIx97EPPH1Rh4mCyB.Zk.RW.ehK', 1, '', NULL, 1, '2016-02-03 06:46:05', '2016-02-02 22:46:05'),
(131, NULL, '', 'awdwad', 'awddaw@rg.rr', '12312', '', 0, 0, '', '', '', 'awddaw@rg.rr', '$2y$10$QcN8w9EJREXrg8QfVgUe/.Gz6/HMwO3LMrki16holMVD/eTHNBcSq', 1, '', NULL, 1, '2016-02-03 06:50:45', '2016-02-02 22:50:45'),
(132, NULL, '', 'awdwad', 'awddaw@rg.rr', '12312', '', 0, 0, '', '', '', 'awddaw@rg.rr', '$2y$10$n77UBg8n9tMrx3iOL.rZxeFE9i3Z.q15AR8YQDU.mRwNsz4DXZoiW', 1, '', NULL, 1, '2016-02-03 06:50:45', '2016-02-02 22:50:45'),
(133, NULL, '', 'wadwadwad', 'ada@refgerh.retf22', '12312', '', 0, 0, '', '', '', 'ada@refgerh.retf22', '$2y$10$Zwn6bD1o.ODCIT029gsy0OroAOk9j24LGr/Ph6QvsSIScq1tlH4aK', 1, '', NULL, 1, '2016-02-03 06:51:16', '2016-02-02 22:51:16'),
(134, NULL, '', 'wadwadwad', 'ada@refgerh.retf22', '12312', '', 0, 0, '', '', '', 'ada@refgerh.retf22', '$2y$10$egsyLiUGh.b70B4PF1aj4O0h1WjE2fP9yKK5w9ZyL5KC.tMUoUznG', 1, '', NULL, 1, '2016-02-03 06:51:16', '2016-02-02 22:51:16'),
(135, NULL, '', 'awdawdawd', 'ddddddddddd@dddddd22ddd.com', '21321', '', 0, 0, '', '', '', 'ddddddddddd@dddddd22ddd.com', '$2y$10$QHDFAf/6hBALwYInritp4eSODThAE9mvfefzzoKUhullrX0hWxbxO', 1, '', NULL, 1, '2016-02-03 06:52:05', '2016-02-02 22:52:05'),
(136, NULL, '', 'awdawdawd', 'ddddddddddd@dddddd22ddd.com', '21321', '', 0, 0, '', '', '', 'ddddddddddd@dddddd22ddd.com', '$2y$10$b3UOWk8bocEK97IPeobSe.Yp/lzxmAJApmXCkIww6NAI2hoZjantC', 1, '', NULL, 1, '2016-02-03 06:52:05', '2016-02-02 22:52:05'),
(137, NULL, '', 'awdwad', 'awdawd@efe.regewqeqwewqe', '1234', '', 0, 0, '', '', '', 'awdawd@efe.regewqeqwewqe', '$2y$10$y425qVvhzMHjzqWHdJrkc.MaLSqdTwuEUvhGe4Svb36mcFnFFJAbG', 1, '', NULL, 1, '2016-02-03 06:54:55', '2016-02-02 22:54:55'),
(138, NULL, '', 'awdwad', 'awdawd@efe.regewqeqwewqe', '1234', '', 0, 0, '', '', '', 'awdawd@efe.regewqeqwewqe', '$2y$10$SZL1RQ3njhGPr0FT.SQuHuRig2UM6hdk7O0tbSX.7ql9QWPBajoWO', 1, '', NULL, 1, '2016-02-03 06:54:55', '2016-02-02 22:54:55'),
(139, NULL, '', 'awdwad', 'wadawd@rgeg.ert', '12312321', '', 0, 0, '', '', '', 'wadawd@rgeg.ert', '$2y$10$Ow8bvrb0ujhdArv8GqB4pOARy0Ojm3zjRt67Tb9nXcsDi1oJn3yaC', 1, '', NULL, 1, '2016-02-03 06:58:08', '2016-02-02 22:58:08'),
(140, NULL, '', 'awdwad', 'wadawd@rgeg.ert', '12312321', '', 0, 0, '', '', '', 'wadawd@rgeg.ert', '$2y$10$FdcKR.HfXap0u9gGGr1hnuxwFvWsC78MpcLbgMVDY8B/Gw5hX6CzC', 1, '', NULL, 1, '2016-02-03 06:58:09', '2016-02-02 22:58:09'),
(141, NULL, '', 'awdawdawd', 'awdadw@efwe.sdf', '123123', '', 0, 0, '', '', '', 'awdadw@efwe.sdf', '$2y$10$SJ9XC7HzD2UrtHCQwqpQL.zPJL/unCDxMi.ktz9clEqeOb3b0OYke', 1, '', NULL, 1, '2016-02-03 07:01:02', '2016-02-02 23:01:02'),
(142, NULL, '', 'awdawdawd', 'awdadw@efwe.sdf', '123123', '', 0, 0, '', '', '', 'awdadw@efwe.sdf', '$2y$10$skPbUXXarFvn2IStfjJWv.VgkKvFZeB54wDm/4mRYEokl9ClX7gIy', 1, '', NULL, 1, '2016-02-03 07:01:02', '2016-02-02 23:01:02'),
(143, NULL, '', 'dwadwd', 'awdawdaw@wed.wqqw', '12312', '', 0, 0, '', '', '', 'awdawdaw@wed.wqqw', '$2y$10$BK.WrjgPfOIreqFmdKt4L.56WTHgjWcA3JbYbwQjkKeeFtBYuCWzG', 1, '', NULL, 1, '2016-02-03 07:06:24', '2016-02-02 23:06:24'),
(144, NULL, '', 'dwadwd', 'awdawdaw@wed.wqqw', '12312', '', 0, 0, '', '', '', 'awdawdaw@wed.wqqw', '$2y$10$FzlrWh0a.Px59FjJhyHQXuyVupm/9HG.Zo.C1Igqn0cLMhD707xY.', 1, '', NULL, 1, '2016-02-03 07:06:24', '2016-02-02 23:06:24'),
(145, NULL, '', 'password', 'qwdqwd@ewfwe.werwe', '12312', '', 0, 0, '', '', '', 'qwdqwd@ewfwe.werwe', '$2y$10$Tzi6upCd.9Yhh4I09Bc6aOSEE9YSulAFlals9cYRCgZFskN1G4Nq.', 1, '', NULL, 1, '2016-02-03 07:10:10', '2016-02-02 23:10:10'),
(146, NULL, '', 'password', 'qwdqwd@ewfwe.werwe', '12312', '', 0, 0, '', '', '', 'qwdqwd@ewfwe.werwe', '$2y$10$GbkXxMZDXjqIqSWSAvT6re/qEKubIcZKInPlgURvZ8I.bc1mcRCVm', 1, '', NULL, 1, '2016-02-03 07:10:10', '2016-02-02 23:10:10'),
(147, NULL, '', 'awdawdawd', 'awda@ewf.wefwef', '1234234', '', 0, 0, '', '', '', 'awda@ewf.wefwef', '$2y$10$dmmv4p0f8AkTLX7gN4j48.KyCo0xMj/H5TP2S6O2LsASR.KbShWsi', 1, '', NULL, 1, '2016-02-04 03:34:25', '2016-02-03 19:34:25'),
(148, NULL, '', 'awdawdwad', 'awdawd@efref.ertertrt', '12312321', '', 0, 0, '', '', '', 'awdawd@efref.ertertrt', '$2y$10$1RKAGn8O/kbJ3gT6diMhBON03DokRuecw5yIIPrQAZkRWx4O/jK5y', 1, '', NULL, 1, '2016-02-04 03:36:18', '2016-02-03 19:36:18'),
(149, NULL, '', 'awdawdwad', 'awdawd@efref.ertertrt', '12312321', '', 0, 0, '', '', '', 'awdawd@efref.ertertrt', '$2y$10$yg8a1RKwxguGo1QXagHBt.VdxX4uhh3WE95iLejRUd2ZsqC10Mvvi', 1, '', NULL, 1, '2016-02-04 03:36:18', '2016-02-03 19:36:18'),
(150, NULL, '', 'wefewf', 'ewfwef@rtgerg.erter', '213421321', '', 0, 0, '', '', '', 'ewfwef@rtgerg.erter', '$2y$10$SfAQJhIXVsvT72VRUK8nXOGa/gBFuFHLKeg1ZFWKhv98SmEvRQ62C', 1, '', NULL, 1, '2016-02-04 03:36:52', '2016-02-03 19:36:52'),
(151, NULL, '', 'wefewf', 'ewfwef@rtgerg.erter', '213421321', '', 0, 0, '', '', '', 'ewfwef@rtgerg.erter', '$2y$10$eciCeV2auc7DoFDOYIccYeKLKkOkiJNWu9jEYa3X0nqe8IDjSOhqO', 1, '', NULL, 1, '2016-02-04 03:36:52', '2016-02-03 19:36:52'),
(152, NULL, '', 'ewrewr', 'werwer@wed.wedwer', 'wadawd', '', 0, 0, '', '', '', 'werwer@wed.wedwer', '$2y$10$pqBbay2LMDktQwxJx3foVuDgj4nvxvZK6j76RM/HuZmEp2phMCQAm', 1, '', NULL, 1, '2016-02-04 03:38:11', '2016-02-03 19:38:11'),
(153, NULL, '', 'ewrewr', 'werwer@wed.wedwer', 'wadawd', '', 0, 0, '', '', '', 'werwer@wed.wedwer', '$2y$10$4aj0S1rxrY.MB0ABMBk4outRrhaEyL9FBAfXuBTPUdW2qppQ29IqK', 1, '', NULL, 1, '2016-02-04 03:38:11', '2016-02-03 19:38:11'),
(154, NULL, '', 'sadasd', 'dsadasd@yahoo.com', '232323', '', 0, 0, '', '', '', 'dsadasd@yahoo.com', '$2y$10$ZnAeEUZlWSkV.vfH0iZh3OcPep9SverOqrjFPjzPkPx61Cu1S6NE6', 1, '', NULL, 1, '2016-02-04 03:41:27', '2016-02-03 19:41:27'),
(155, NULL, '', 'sadasd', 'dsadasd@yahoo.com', '232323', '', 0, 0, '', '', '', 'dsadasd@yahoo.com', '$2y$10$.W/zXEvq19ehhQ7UNVd4NuZzBkeJkGIvB.0Fqs3DN/PMM4XX25xsy', 1, '', NULL, 1, '2016-02-04 03:41:27', '2016-02-03 19:41:27'),
(156, NULL, '', 'sadsad', 'dsadsad@yahoo.com', '21323232', '', 0, 0, '', '', '', 'dsadsad@yahoo.com', '$2y$10$6P0twpnU7yoYYwBa1Xt9fOOWcnslJkFhPaDN/gsY4XTcKcH5uuuNG', 0, '', NULL, 1, '2016-02-04 03:42:04', '2016-02-03 19:42:04'),
(157, NULL, '', 'sadsad', 'dsadsad@yahoo.com', '21323232', '', 0, 0, '', '', '', 'dsadsad@yahoo.com', '$2y$10$xhTkwMWRTp866aHtla9PT.SS3TU5Hz8T2E9fPUA50S61tgEBsZ.2a', 0, '', NULL, 1, '2016-02-04 03:42:04', '2016-02-03 19:42:04'),
(158, NULL, '', 'dasdsad', 'dsadas4444d@yahoo.com', '23213232', '', 0, 0, '', '', '', 'dsadas4444d@yahoo.com', '$2y$10$Jr7kv1Em8lRLX9E/uynmdOfuAAMh3lfCnVdOdcSt57C4s2/KjKvKq', 1, '', NULL, 1, '2016-02-04 03:44:07', '2016-02-03 19:44:07'),
(159, NULL, '', 'dasdsad', 'dsadas4444d@yahoo.com', '23213232', '', 0, 0, '', '', '', 'dsadas4444d@yahoo.com', '$2y$10$ETe3ryHY.scFumrlEyuQJOoe0QmRURubzen7MoXsWUEryVZtdF8ty', 1, '', NULL, 1, '2016-02-04 03:44:08', '2016-02-03 19:44:08'),
(160, NULL, '', 'sasadasdasd', 'dsadsad44@yahoo.com', '232323', '', 0, 0, '', '', '', 'dsadsad44@yahoo.com', '$2y$10$r7QhOg3fWYzUb0OTjRdjueI69HUex9.PekPyf3K232jCfJasjB5mK', 1, '', NULL, 1, '2016-02-04 03:45:49', '2016-02-03 19:45:49'),
(161, NULL, '', 'sasadasdasd', 'dsadsad44@yahoo.com', '232323', '', 0, 0, '', '', '', 'dsadsad44@yahoo.com', '$2y$10$H.hNroZ8vOxc90eMjGoOX.vpXSX31y1.ZrDu.T0HPok8bq5ZCiNAC', 1, '', NULL, 1, '2016-02-04 03:45:49', '2016-02-03 19:45:49'),
(162, NULL, '', 'sasadasda', 'dadsadsadsdD@yahoo.com', '2323232', '', 0, 0, '', '', '', 'dadsadsadsdD@yahoo.com', '$2y$10$mD8hYgAZFyw7f7xRez3icehY8ayDB1KDZB3OtOiBVSyOh/6WbKp7q', 1, '', NULL, 1, '2016-02-04 03:48:16', '2016-02-03 19:48:16'),
(163, NULL, '', 'sasadasda', 'dadsadsadsdD@yahoo.com', '2323232', '', 0, 0, '', '', '', 'dadsadsadsdD@yahoo.com', '$2y$10$dEfoaaOhDlVeL1oMZflrkeqfO2OwZwc3MNREYK/YHOMPX7vpxQ0pC', 1, '', NULL, 1, '2016-02-04 03:48:16', '2016-02-03 19:48:16'),
(164, NULL, '', 'sample', 'sampl444e@yahoo.com', '232323', '', 0, 0, '', '', '', 'sampl444e@yahoo.com', '$2y$10$i7sRS2C8XR18eOcqbYFEyeWSZU83m/kmh0FtHvVbM2aTffYfWUhYG', 1, '', NULL, 1, '2016-02-04 03:51:12', '2016-02-03 19:51:12'),
(165, NULL, '', 'awdawdawd', 'wadd@erge.rft', '12321321', '', 0, 0, '', '', '', 'wadd@erge.rft', '$2y$10$8pFS3SuW.Up9tiqrYC2nieLP0Y7elfYEg6qGcAFqUfe1PlKjmTLoW', 1, '', NULL, 1, '2016-02-04 03:56:57', '2016-02-03 19:56:57'),
(166, NULL, '', 'awdawdawd', 'wadd@erge.rft', '12321321', '', 0, 0, '', '', '', 'wadd@erge.rft', '$2y$10$2NJGrmSrO7yKm4.NDTkFV.ZPO5o9TNcL7Sa2rwMxounTreztMU0im', 1, '', NULL, 1, '2016-02-04 03:56:57', '2016-02-03 19:56:57'),
(167, NULL, '', 'awdawdawd', 'w21312add@erge.rft', '12321321', '', 0, 0, '', '', '', 'w21312add@erge.rft', '$2y$10$qAZvXXJacZsudj4loyiZyuUwDCPMUl4ANGcQGQiWAYeVZ3PFKT766', 1, '', NULL, 1, '2016-02-04 03:57:13', '2016-02-03 19:57:13'),
(168, NULL, '', 'Gearad', 'ewrdwef@ewfwe.werwer', '123123', '', 0, 0, '', '', '', 'ewrdwef@ewfwe.werwer', '$2y$10$ejlN7zpp41cmIGFfrg1S/O1qs0yONA1DJKWqHkxxPc5djSv4aBchi', 1, '', NULL, 1, '2016-02-04 05:20:12', '2016-02-03 21:20:12'),
(169, NULL, '', 'Gearad', 'ewrdwef@ewfwe.werwer', '123123', '', 0, 0, '', '', '', 'ewrdwef@ewfwe.werwer', '$2y$10$4LINCHp27C/TdQoeeEK6j.bx7bHx.jv.ovOX19OnSX1pmY.oaNL82', 1, '', NULL, 1, '2016-02-04 05:20:12', '2016-02-03 21:20:12'),
(170, NULL, '', 'efefe', 'ewrwer@efwe.werwer', '123123', '', 0, 0, '', '', '', 'ewrwer@efwe.werwer', '$2y$10$du4RMeNenXUeYRjTfuuvwu4xx9p41pZwnB2LE0uroUNAPmmkiB1i.', 1, '', NULL, 1, '2016-02-04 05:20:50', '2016-02-03 21:20:50'),
(171, NULL, '', 'efefe', 'ewrwer@efwe.werwer', '123123', '', 0, 0, '', '', '', 'ewrwer@efwe.werwer', '$2y$10$m3bH46hJCfgLzh/t.w13T..UpEzOrMokCRGGARCPf7UIlQN0FI4NG', 1, '', NULL, 1, '2016-02-04 05:20:50', '2016-02-03 21:20:50'),
(172, NULL, '', 'awdawdawdawd', 'awdawd@efwe.ewrewr', '213213', '', 0, 0, '', '', '', 'awdawd@efwe.ewrewr', '$2y$10$n51i/r/GFZxVdK2kEYNpd..aXq38jJqoYieKjbcls6BPq1ngT4I5O', 1, '', NULL, 1, '2016-02-04 05:21:32', '2016-02-03 21:21:32'),
(173, NULL, '', 'awdawdawdawd', 'awdawd@efwe.ewrewr', '213213', '', 0, 0, '', '', '', 'awdawd@efwe.ewrewr', '$2y$10$htVEO8LstuxK5KC.HtcmJ.Y0ws5XG2Acd2OjQTI3paNXTBbpC5Ofe', 1, '', NULL, 1, '2016-02-04 05:21:32', '2016-02-03 21:21:32'),
(174, NULL, '', 'awewaewa', 'waewea@erfgew.rwetew', '12312321', '', 0, 0, '', '', '', 'waewea@erfgew.rwetew', '$2y$10$pJgIg1VtPKFf1RvlXd8K7Ow2.EoXvxpsuuSneyFv977p219gVW8uG', 1, '', NULL, 1, '2016-02-04 05:21:57', '2016-02-03 21:21:57'),
(175, NULL, '', 'awewaewa', 'waewea@erfgew.rwetew', '12312321', '', 0, 0, '', '', '', 'waewea@erfgew.rwetew', '$2y$10$B4xVpMbYt0FjMb6lzel.fOlMqrYd5MnQoTDVlLCIEsajXWsmFY6Oq', 1, '', NULL, 1, '2016-02-04 05:21:57', '2016-02-03 21:21:57'),
(176, NULL, '', 'awdwadawdwa', 'awdadw@efew.ert', '12312', '', 0, 0, '', '', '', 'awdadw@efew.ert', '$2y$10$31woWIux6avK3WftfM.g8e71EsFwnlgXtwc2I9rZW/3aO1GUlGnLq', 1, '', NULL, 1, '2016-02-04 09:42:07', '2016-02-04 01:42:07'),
(177, NULL, '', 'wqewqe', 'qwewq@RGE.ERTER', '23324', '', 0, 0, '', '', '', 'qwewq@RGE.ERTER', '$2y$10$RnenoidQsiM3u8evSQ7eD.cmCEb2Te6pMpTKnd89iKASj5lYKwkPS', 1, '', NULL, 1, '2016-02-04 09:45:02', '2016-02-04 01:45:02'),
(178, NULL, '', 'Frf', 'alvin.nathan@yahoo.com', 'rtytr', '', 0, 0, '', '', '', 'alvin.nathan@yahoo.com', '$2y$10$HUyyjX56KQ20ozlbwoWEseZwnx4E9ZXCbTglg6f282I1K2mTO/RPG', 1, '', NULL, 1, '2016-02-04 10:51:54', '2016-02-04 02:51:54'),
(179, NULL, '', 'awdawdawd', 'wadwda@edfwe.werwe', '43253', '', 0, 0, '', '', '', 'wadwda@edfwe.werwe', '$2y$10$f.kKZvIew5WvhzaLoya8ruBpk4srNnWtOJaMAV9Emx0/mV5krieau', 1, '', NULL, 1, '2016-02-04 10:52:35', '2016-02-04 02:52:35'),
(180, NULL, '', 'awdawdawd', 'wa5ytydwda@edfwe.werwe', '43253', '', 0, 0, '', '', '', 'wa5ytydwda@edfwe.werwe', '$2y$10$KISfl6dIbq/RdhSVlWnmM.CM0K2QxSMjy7.4rhSYV15Q9LG.uZ4r6', 1, '', NULL, 1, '2016-02-04 10:53:17', '2016-02-04 02:53:17'),
(181, NULL, '', 'sample sample', 'sample90@yahoo.com', '232232323', '', 0, 0, '', '', '', 'sample90@yahoo.com', '$2y$10$D7jULIe4dphznSnYun3U8OXMkU8qCwZJLvSWzxUZI9wTFrqkrC1uG', 1, '', NULL, 1, '2016-02-05 09:33:19', '2016-02-05 01:33:19'),
(182, NULL, '', 'test test', 'test@yahoo.com', '2323232232', '', 0, 0, '', '', '', 'test@yahoo.com', '$2y$10$8KiRAMzcDXdafkvDgcEvFuFBSdEE6N7F185oAOJyjqNQ56Ary.n72', 1, '', NULL, 1, '2016-02-05 09:37:51', '2016-02-05 01:37:51'),
(183, NULL, '', 'sample test', 'sampletest@yahoo.com', '22323232', '', 0, 0, '', '', '', 'sampletest@yahoo.com', '$2y$10$fwb/2XWtDnpYzq0yDvD6LOdZ4d1F1SRpMGShZc/QgQ/39CoT4slhS', 1, '', NULL, 1, '2016-02-05 09:39:24', '2016-02-05 01:39:24'),
(184, NULL, '', 'sample test sample', 'sampletest@yahoo.com.ph', '44232323', '', 0, 0, '', '', '', 'sampletest@yahoo.com.ph', '$2y$10$sSGhyi1NICa6Rr6eRvBNRO.a2WuCLbBxgNnjva4e9jMINC9T/pEYe', 1, '', NULL, 1, '2016-02-05 09:41:11', '2016-02-05 01:41:11'),
(185, NULL, '', 'xample', 'xample@yahoo.com', '23223', '', 0, 0, '', '', '', 'xample@yahoo.com', '$2y$10$Xx/umNEEq5CtetgRlVl/YuMznsXDBVHPVhkqX.N3lv7.suEu7J2zy', 1, '', NULL, 1, '2016-02-05 09:42:45', '2016-02-05 01:42:45'),
(186, NULL, '', 'samplesample', 'samplesample@yahoo.com', '232322323232', '', 0, 0, '', '', '', 'samplesample@yahoo.com', '$2y$10$xKxicBi5o7165Pz6uE9fgekEESaTf97agX6BHTahVuAP3KMwx3B4G', 1, '', NULL, 1, '2016-02-05 09:53:44', '2016-02-05 01:53:44'),
(187, NULL, '', 'qwerty', 'qwerty@yahoo.com', '2323223232', '', 0, 0, '', '', '', 'qwerty@yahoo.com', '$2y$10$c3MbDrDuKgr9oxZYErcekO6zvv/HfE26LNGUt.yESFW8Rg9Q1lscO', 1, '', NULL, 1, '2016-02-05 09:54:58', '2016-02-05 01:54:58'),
(188, NULL, '', '22323322332', '2323223322323@yahoo.com', '22322323232', '', 0, 0, '', '', '', '2323223322323@yahoo.com', '$2y$10$G1amASbT43XL6t8mOOS85.Hh8W2bu4EV12oA2ZXujVl.kTsFKUlOe', 1, '', NULL, 1, '2016-02-05 10:01:11', '2016-02-05 02:01:11'),
(189, NULL, '', 'sdaasdd', 'sdaasdssdasdD@yahoo.com', '23232232232', '', 0, 0, '', '', '', 'sdaasdssdasdD@yahoo.com', '$2y$10$alFDHwGOUxzhLyk6AK2Sre0u7S.WER53RCLwFRdbAcqZW8Znek3Om', 1, '', NULL, 1, '2016-02-05 10:03:01', '2016-02-05 02:03:01'),
(190, NULL, '', '232322322', '2323223244443@yahoo.com', '23232232232', '', 0, 0, '', '', '', '2323223244443@yahoo.com', '$2y$10$iIlWraANpNAAv1nBNSeE6.6rougOFYUPegYZs9GDxwnMqdXCeKn8i', 1, '', NULL, 1, '2016-02-05 10:05:24', '2016-02-05 02:05:24'),
(191, NULL, '', '232322323232', '22322322323@yahoo.com', '22322323', '', 0, 0, '', '', '', '22322322323@yahoo.com', '$2y$10$LbFcQOXa20FAZl4ROrqsZePaUoTovAkND2lPA.PRtrwcDVmEuotUW', 1, '', NULL, 1, '2016-02-05 10:06:55', '2016-02-05 02:06:55'),
(192, NULL, '', '232323', '232322322232@yahoo.com', '232323223', '', 0, 0, '', '', '', '232322322232@yahoo.com', '$2y$10$k8BOoEs7f2aHKT2Wiawgx.kU6UMLujez87Jt.t4FJUVx1freVIUie', 1, '', NULL, 1, '2016-02-05 10:12:44', '2016-02-05 02:12:44'),
(193, NULL, '', '23232232223232', '2323232232322323@yahoo.com', '232322323', '', 0, 0, '', '', '', '2323232232322323@yahoo.com', '$2y$10$mS1cxmTgcW54q3yq4XMwuO8tuVTgvlqkOHSy1oTYwDRM87WzJJkpK', 1, '', NULL, 1, '2016-02-05 10:14:26', '2016-02-05 02:14:26'),
(194, NULL, '', '232323222223', '2232323323323@yahoo.com', '3232322323', '', 0, 0, '', '', '', '2232323323323@yahoo.com', '$2y$10$XPLEMM8yVDeON.s00LFl1OqjziHOGVSzxs9mYXpaHDfHMvIVXMnOm', 1, '', NULL, 1, '2016-02-05 10:15:27', '2016-02-05 02:15:27'),
(195, NULL, '', '3232232323', '23233222323232@yahoo.com', '232323232', '', 0, 0, '', '', '', '23233222323232@yahoo.com', '$2y$10$OM2eX4WU8uKMNQBr6m4/qOERcaClj8VfA6OEMjJKtdL/0GY2Cl8CO', 1, '', NULL, 1, '2016-02-05 10:16:16', '2016-02-05 02:16:16'),
(196, NULL, '', '23232232323', '23232322232323@yahoo.com', '2332232323', '', 0, 0, '', '', '', '23232322232323@yahoo.com', '$2y$10$0qO1Zkkl6oo6GmuaVmUlAO9izbnA6iiFURGi.nvaM9RQRdTU.Fksa', 1, '', NULL, 1, '2016-02-05 10:17:31', '2016-02-05 02:17:31'),
(197, NULL, '', '2323223232232232', '23223232322323@yahoo.com', '232322223', '', 0, 0, '', '', '', '23223232322323@yahoo.com', '$2y$10$w8Tc.K1YjhVOEumaYRDJ2uK2c3EE9K8QIKvY.yZf9gw55qIZnSqTK', 1, '', NULL, 1, '2016-02-05 10:19:42', '2016-02-05 02:19:42'),
(198, NULL, '', '32323223', '32322323223@yahoo.com', '23232232323', '', 0, 0, '', '', '', '32322323223@yahoo.com', '$2y$10$1S2B1/.Y4zYbpjUIekvJ7OX2BV1wUQlQxA.lLPPK3tcUJQ9nJq5qe', 1, '', NULL, 1, '2016-02-05 10:21:50', '2016-02-05 02:21:50'),
(199, NULL, '', '32321223123', '23123123231312312@yahoo.com', '2323223232323', '', 0, 0, '', '', '', '23123123231312312@yahoo.com', '$2y$10$KBDEA3CuyXfiEHpAm35o/.FSeQZ5aR2TbuJTKyzsklDiW.a6AOIbi', 1, '', NULL, 1, '2016-02-05 10:27:50', '2016-02-05 02:27:50'),
(200, NULL, '', '3232223223', '2323232232344442323@yahoo.com', '32322323', '', 0, 0, '', '', '', '2323232232344442323@yahoo.com', '$2y$10$cdM34XnWZnrBxXRBmz2AduVnNJZmAGznyOc3czu/hQoouFVvmv8hy', 1, '', NULL, 1, '2016-02-05 10:29:23', '2016-02-05 02:29:23'),
(201, NULL, '', '2323222323', '2323223232@yahoo.com', '32323223', '', 0, 0, '', '', '', '2323223232@yahoo.com', '$2y$10$H2hE8Scxg5v5Au87Y.8Lgu4OzrY.rFDBVDLS801ZFxHZLBp/xMhsq', 1, '', NULL, 1, '2016-02-05 10:35:06', '2016-02-05 02:35:06'),
(202, NULL, '', '323223222232323', '2323223232232@yahoo.com', '232323223322', '', 0, 0, '', '', '', '2323223232232@yahoo.com', '$2y$10$wmPqaW4Xddwl7nKcw/oAKeLPbfpZKq9125bAyZz0ZYEuAFRBkjaHC', 1, '', NULL, 1, '2016-02-05 10:42:22', '2016-02-05 02:42:22'),
(203, NULL, '', 'dsa', 'c54353eo@legacy.com', '000000', '', 0, 0, '', '', '', 'c54353eo@legacy.com', '$2y$10$GVb1rICendq3JyFdigufduw3IfXqkgupXtMunRZovvFnUoZks..zW', 1, '', NULL, 1, '2016-02-05 10:43:22', '2016-02-05 02:43:22'),
(204, NULL, '', '2323223232322323232', '2323232232322323232323@yahoo.c', '23232223232', '', 0, 0, '', '', '', '2323232232322323232323@yahoo.c', '$2y$10$Y4J.h2kdJYjcE5PNB3JhY.5v4pJiJeSAx6UsCflsbs/LUe5KSdgae', 1, '', NULL, 1, '2016-02-05 10:44:22', '2016-02-05 02:44:22'),
(205, NULL, '', '2323222232', '323232232323@yahoo.com', '232323223223', '', 0, 0, '', '', '', '323232232323@yahoo.com', '$2y$10$jSOPP2AE8SunfaWnsvXafeCGDl3A6H.cQDjRKWNx/iIA9VUNKtvue', 1, '', NULL, 1, '2016-02-05 10:47:23', '2016-02-05 02:47:23'),
(206, NULL, '', '23223223', '232323222323232@yahoo.com', '32323232', '', 0, 0, '', '', '', '232323222323232@yahoo.com', '$2y$10$5/4lp4gKUN0/w0/p/2zbueQLJgSJcljIbb4QEH4QXE0rKRmzRuB0K', 1, '', NULL, 1, '2016-02-05 10:48:16', '2016-02-05 02:48:16'),
(207, NULL, '', '23232232', '2322323223223@yahoo.com', '232323232', '', 0, 0, '', '', '', '2322323223223@yahoo.com', '$2y$10$xCLFEWrma03wlwo3UhRIcu2chTd2jntNUtX6hIDwCM7twQ71quqSO', 1, '', NULL, 1, '2016-02-05 10:49:13', '2016-02-05 02:49:13'),
(208, NULL, '', '23232232232', '2323232232323@yahoo.com', '232323223', '', 0, 0, '', '', '', '2323232232323@yahoo.com', '$2y$10$pgH4V/QHeVYKplFf423v.Oc0hGzUTKCwLY3zP9B2p2TuRMTafhGfu', 1, '', NULL, 1, '2016-02-05 10:52:37', '2016-02-05 02:52:37'),
(209, NULL, '', '22322323232', '22232322232@yahoo.com', '23232232332', '', 0, 0, '', '', '', '22232322232@yahoo.com', '$2y$10$S/YLPGQEoQkbnWRQFgu5lu0NNldyg84n2MhdehuG.ywNK4m2QL2ku', 1, '', NULL, 1, '2016-02-05 10:54:36', '2016-02-05 02:54:36'),
(210, NULL, '', '2323223223', '2323232221321@yahoo.com', '32322232332', '', 0, 0, '', '', '', '2323232221321@yahoo.com', '$2y$10$jusowdaebE0z10aK66G6JOdCi5hra7t2FfMBlb6ZLjk9DyfcShsKq', 1, '', NULL, 1, '2016-02-05 10:56:16', '2016-02-05 02:56:16'),
(211, NULL, '', '2323223223', '223232232@yahoo.com', '2323223', '', 0, 0, '', '', '', '223232232@yahoo.com', '$2y$10$Jzxr7r4Tdhdg0MFfPosYoeAXWgJa9BHhXPY9AYyyiygNXBhfrOVa.', 1, '', NULL, 1, '2016-02-05 10:59:22', '2016-02-05 02:59:22'),
(212, NULL, '', '223222322332', '232322323244432@yahoo.com', '3232322', '', 0, 0, '', '', '', '232322323244432@yahoo.com', '$2y$10$wvEXhQBX9JU/CLOfdB5sD.SNqEjXKHdTGs8Oh3nCF6voSw2Q5JjOW', 1, '', NULL, 1, '2016-02-05 11:00:08', '2016-02-05 03:00:08'),
(213, NULL, '', '223222322332', '232322323244432@yahoo.com', '3232322', '', 0, 0, '', '', '', '232322323244432@yahoo.com', '$2y$10$aQszQvggd5edpFxKOajdq.8n.hlHvSeLN8vGvmfuSEFq8xmwlI9JK', 1, '', NULL, 1, '2016-02-05 11:00:08', '2016-02-05 03:00:08'),
(214, NULL, '', '23232232323', '32322323232@yahoo.com', '2323223232', '', 0, 0, '', '', '', '32322323232@yahoo.com', '$2y$10$QhvtM1l.LNL8.DNlxI4AQOQvWsOcSk08YDiKhhvjd5G0TlEUGbTne', 1, '', NULL, 1, '2016-02-05 11:09:22', '2016-02-05 03:09:22'),
(215, NULL, '215.jpg', '2323223232322323232', '3232322323@yahoo.com', '232322332', '', 0, 0, '', '', '', '3232322323@yahoo.com', '$2y$10$cr3.0gy3pzOLeQ0slLJz0O0LQnG33eNNggl9SzGQSdGf3951NiCbO', 0, '', NULL, 1, '2016-02-05 11:09:58', '2016-02-05 03:09:58'),
(216, NULL, '', 'awewaewa', 'aweaew@efdew.ewrwer', '23142', '', 0, 0, '', '', '', 'aweaew@efdew.ewrwer', '$2y$10$4Hpt5XmwD4Phhi8SilhoZea45txK0fyN7CZ90WI0Qub4cYw71EDrm', 1, '', NULL, 1, '2016-02-10 03:57:01', '2016-02-10 09:57:01'),
(217, NULL, '', 'awdwadawdwa', 'awdwa@wdw.werwe', '32432432', '', 0, 0, '', '', '', 'awdwa@wdw.werwe', '$2y$10$mX7xoMTjBAVuYWaGBakBseXtEEi5vkC0FG5PBWeXQLohl5yflF96S', 0, '', NULL, 1, '2016-02-10 03:58:16', '2016-02-10 09:58:16'),
(218, NULL, '', 'awewaewa', 'aweawe@ewfdwe.ewrewr', '4324324', '', 0, 0, '', '', '', 'aweawe@ewfdwe.ewrewr', '$2y$10$CHp6ojlf/IycFOAd10cpW.sI5rRG8DrvKkUaLJrr8t0z7SIJcGX0C', 1, '', NULL, 1, '2016-02-10 03:59:18', '2016-02-10 09:59:18'),
(219, NULL, '', 'wadwadwadawd', 'wadaw@3wr3.com', '32432', '', 0, 0, '', '', '', 'wadaw@3wr3.com', '$2y$10$TiZW76/I2OuZOz99ho.ui.qWtf1xjdtmC.0oKJOHviluKpo4Kjfmi', 1, '', NULL, 1, '2016-02-10 05:19:26', '2016-02-10 11:19:26');

-- --------------------------------------------------------

--
-- Table structure for table `yakolak_usertypes`
--

CREATE TABLE IF NOT EXISTS `yakolak_usertypes` (
`id` int(11) NOT NULL,
  `type_name` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `yakolak_usertypes`
--

INSERT INTO `yakolak_usertypes` (`id`, `type_name`) VALUES
(1, 'vendor'),
(2, 'system'),
(3, 'customer'),
(4, 'company\r\n');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `yakolak_advertisements`
--
ALTER TABLE `yakolak_advertisements`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_advertisements_photo`
--
ALTER TABLE `yakolak_advertisements_photo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_categories`
--
ALTER TABLE `yakolak_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_category_custom_attributes`
--
ALTER TABLE `yakolak_category_custom_attributes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_countries`
--
ALTER TABLE `yakolak_countries`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_sub_attributes`
--
ALTER TABLE `yakolak_sub_attributes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_sub_categories`
--
ALTER TABLE `yakolak_sub_categories`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_users`
--
ALTER TABLE `yakolak_users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `yakolak_usertypes`
--
ALTER TABLE `yakolak_usertypes`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `yakolak_advertisements`
--
ALTER TABLE `yakolak_advertisements`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `yakolak_advertisements_photo`
--
ALTER TABLE `yakolak_advertisements_photo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `yakolak_categories`
--
ALTER TABLE `yakolak_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `yakolak_category_custom_attributes`
--
ALTER TABLE `yakolak_category_custom_attributes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `yakolak_countries`
--
ALTER TABLE `yakolak_countries`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=251;
--
-- AUTO_INCREMENT for table `yakolak_sub_attributes`
--
ALTER TABLE `yakolak_sub_attributes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `yakolak_sub_categories`
--
ALTER TABLE `yakolak_sub_categories`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `yakolak_users`
--
ALTER TABLE `yakolak_users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=220;
--
-- AUTO_INCREMENT for table `yakolak_usertypes`
--
ALTER TABLE `yakolak_usertypes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
