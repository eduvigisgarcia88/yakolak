<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
  
    'google' => [
        'client_id' =>  '648140357257-7g0oprl1rrpb3u11u9jl1ocfmm3c982e.apps.googleusercontent.com',
        'client_secret' => 'rXvRTIalLy2Y92PIOhK6YK2m',
        'redirect' => 'https://yakolak.xerolabs.co/google/login-callback'

    ],
  
    'facebook' => [
        'client_id' => '2034041853508919',
        'client_secret' => 'f7f9606c763daa7a54515d8a0a5b955a',
        'redirect' => 'https://yakolak.xerolabs.co/facebook/login-callback'
    ],
  
    'twitter' => [
        'client_id' => 'twFY9Bmtu1tCruanIdtAAeaII',
        'client_secret' => 'xRiz4OYQvM0A1IRS3M1myl15epA234wQKyFA9J5Xa6SpuKxtvl',
        'redirect' => 'https://yakolak.xerolabs.co/twitter/login-callback'
    ],

];
