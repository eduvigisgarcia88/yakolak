<?php

return [

	/*
	|--------------------------------------------------------------------------
	| oAuth Config
	|--------------------------------------------------------------------------
	*/

	/**
	 * Storage
	 */
	'storage' => '\\OAuth\\Common\\Storage\\Session',

	/**
	 * Consumers
	 */
	'consumers' => [

		'Facebook' => [
			'client_id'     => '',
			'client_secret' => '',
			'scope'         => [],
		],
		'Google' => [
		    'client_id'     => env('122135541275-rbmhguphudgjbeh530itr5hg25vo3puu.apps.googleusercontent.com'),
		    'client_secret' => env('rXMZ4jYyCODRuBWtu18W9A_5'),
		    'scope'         => ['userinfo_email', 'userinfo_profile', 'https://www.google.com/m8/feeds/'],
		],

	]

];