<?php

return array(


    'pdf' => array(
        'enabled' => true,
        'binary'  => '/home/xerolabs/htmlpdf/wkhtmltopdf',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),
    'image' => array(
        'enabled' => true,
        'binary'  => '/home/xerolabs/htmlpdf/wkhtmltoimage',
        'timeout' => false,
        'options' => array(),
        'env'     => array(),
    ),


);
