@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
             window.location.href = "";   
        loading.addClass('hide');
    }
 // $("#ads-category").on("change", function() { 
 //        var id = $("#ads-category").val();
 //        $("#custom-attribute-container").removeClass("hide");
 //        $_token = "{{ csrf_token() }}";
 //        var customAtt = $("#custom-attributes-pane");
 //        customAtt.html(" ");
 //        // var subcategory = $("#ads-subcategory");
 //        if ($("#ads-category").val()) {
 //          $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
 //            customAtt.html(response.rows);
 //          }, 'json');
 //          // subcategory.html("");
 //          // $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
 //          //   subcategory.html(response);
 //          // }, 'json');

 //         var parent_category = $("#ads-parent_category");
 //          $.post("{{ url('get-parent-category') }}", { id: id, _token: $_token }, function(response) {
 //            parent_category.val(response.parent_cat_id);
 //          }, 'json')
 //       }    
 //  });
 $("#ads-type").on("change", function() { 
        var id = $("#ads-type").val();
        $("#category-pane").addClass("hide");
        $("#subcategory-pane").addClass("hide");
        $("#subcategorythree-pane").addClass("hide");
        if (id == 1) {
            $("#category-ads-pane").removeClass("hide");
            $("#category-auction-pane").addClass("hide");
            $("#auction-type-pane").addClass("hide");
        } else {
            $("#category-ads-pane").addClass("hide");
            $("#category-auction-pane").removeClass("hide");
            $("#auction-type-pane").removeClass("hide");
        }
        $("#ads-main-category").html("");
        $("#ads-category-auction").html("");
        $.post("{{ url('get-cat-type-for-post') }}", { id: id, _token: $_token }, function(response) {
          if(response.cat_type == "undefined"){
            $("#ads-main-category").html('<option value="all" readOnly>All Categories</option>');
            $("#ads-category-auction").html('<option value="all" readOnly>All Categories</option>');
          }else{
            $("#ads-main-category").html(response.cat_type);
            $("#ads-category-auction").html(response.cat_type);
          } 
       }, 'json');
  });  
 $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html("");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
  });
  $("#ads-category").on("change", function() { 
          var id = $("#ads-category").val();
           $("#custom-attribute-container").removeClass("hide");
          if(id=="Y2F0LTI4" || id == "Y2F0LTI5"){
            $("#price-range-container-title").addClass("hide");
            $('#price-range-container').addClass("hide");
          }else{
            $("#price-range-container-title").removeClass("hide");
            $('#price-range-container').removeClass("hide");
          }
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          var subcategory = $("#ads-subcategory");
          if(id){
            $('#custom-attributes-pane').addClass('hide');
            function stick(){
              $.post("{{ url('custom-attributes') }}", { id: ($('#ads-main-category').val() != 'Select' ? $('#ads-main-category').val() : $('#ads-category-auction').val()), _token: $_token }, function(response) {
             if(response.rows.length > 0){
               custom_attri.prepend(response.rows);
              }
            }, 'json').complete(function(){
                $('#custom-attributes-pane').removeClass('hide');
              });
            }
            
            
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                $("#custom-attribute-pane-title").addClass("hide");
                $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                    custom_attri.html(response.rows);
              }     
            }, 'json').complete(stick);
            
            
              subcategory.html("");
            if(id !="all"){
              $.post("{{ url('get-sub-category-for-post') }}", { id: id, _token: $_token }, function(response) {
                if(response == "undefined"){
                  $("#subcategory-pane").addClass("hide");
                  $("#subcategorythree-pane").addClass("hide");
                }else{
                  $("#subcategory-pane").removeClass("hide");
                   subcategory.html(response);
                }
               
              }, 'json');
            }
         }    
    });
 $("#ads-subcategory").on("change", function() { 
      var id = $("#ads-subcategory").val();
      $("#custom-attribute-container").removeClass("hide");
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          if(id){
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                  $("#custom-attribute-pane-title").addClass("hide");
                  $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                  custom_attri.html(response.rows);
              }     
          }, 'json');
          }
            var container = $("#ads-subSubcategory");
          $.post("{{ url('get-sub-category-three-for-post') }}", { id: id, _token: $_token }, function(response) {
            if(response == "undefined"){
               $("#subcategorythree-pane").addClass("hide");
            }else{
               $("#subcategorythree-pane").removeClass("hide");
               container.html(response);
            }
          }, 'json');
   });
   $("#ads-main-category").on("change", function() {
        var id = $("#ads-main-category").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
                    $("#ads-subcategory").addClass("hide");
                  }else{
                    $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  } 
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
//                     $("#custom-attribute-pane-title").addClass("hide");
                    $("#custom-attribute-container").addClass("hide");
                }else{
//                     $("#custom-attribute-pane-title").removeClass("hide");
                    $("#custom-attribute-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }     
            }, 'json');
       }
   });
 $("#ads-category-auction").on("change", function() {
        var id = $("#ads-category-auction").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
                    $("#ads-subcategory").addClass("hide");
                  }else{
                    $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  } 
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
                    // $("#custom-attribute-pane-title").addClass("hide");
                    // $("#custom-attribute-pane-container").addClass("hide");
                    $("#custom-attribute-container").addClass("hide");
                }else{
                    // $("#custom-attribute-pane-title").removeClass("hide");
                    // $("#custom-attribute-pane-container").removeClass("hide");
                    $("#custom-attribute-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }     
            }, 'json');
       }
   });  
$("#ad-post-content-wrapper").on("click", ".btn-post_not_allowed", function() {
    $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
    $("#modal-form").find('form').trigger("reset");
    $(".modal-title").html('<strong>Limit Reached </strong>');
    $("#modal-post_invalid").modal('show');    
});


function limit_reached(){
  $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
  $("#modal-form").find('form').trigger("reset");
  $(".modal-title").html('<strong>Limit Reached </strong>');
  $("#modal-post_invalid").modal('show'); 
}

    $("#client-country").on("change", function() { 
    $("#client-city2").attr('disabled', 'disabled');
    $("#client-city2").addClass("hide");
    $("#client-city").removeClass("hide");
}); 
                                    
    $("#ads-price").change(function() {                  
        $('#row-bid_limit_amount').val(this.value);
    });
$(".uploader-pane").on("change", ".photoName", function() {
// $('.photoName').change(function() {         
  var id = $(this).data('id');
  var filename = "";
  filename = $(this).val().split("\\").pop();
   $("#file_name_"+id).val(filename);
   $("#preview_"+id);
   $("#row-img_"+id).attr("src");
   $("#preview_"+id).removeClass("hide");
});

function readURL(input, id) {
    // var id = $(this).data('id');
    // console.log(id);
    // console.log(input);
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#row-img_" + id).attr('src', e.target.result);
            $("#row-img_" + id).removeClass('hide');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".photoName").change(function(){
    var id = $(this).data('id');
    readURL(this, id);
});

$(".uploader-pane").on("click", ".removeImage", function() { 
    var id = $(this).data('id');
    $("#preview_"+id).addClass("hide");
    $("#file_name_"+id).val("");
    $("#row-img_"+id).removeAttr("src");
    $(".photo"+id).val("");
});
// ('[data-countdown]').each(function() {
//                var $this = $(this), finalDate = $(this).data('countdown');
//                var ads_id = $(this).data('ads_id');
//                $this.countdown(finalDate, function(event) {
//                $this.html(event.strftime('%DD-%HH-%MM-%SS'));
//                });
//              });

  var count_this_down = $('div#count_me_down').data('countdown');
  console.log(count_this_down);
  $("div#count_me_down").countdown(count_this_down, function(event) {
    $(this).text(event.strftime('%DD-%HH-%MM-%SS')).addClass('ellipsis');
    $(this).attr('title',event.strftime('%DD-%HH-%MM-%SS'));
  });

  // $("select#ads-type").val(1).text('Ad Listings');
  $(document).ready(function(){
    @if(isset($change_to_auction))
    $('select#ads-type option[value=2]').attr('selected','selected').change();
    @else
    $('select#ads-type option[value=1]').attr('selected','selected').change();
    @endif

    var default_country_id = '{{$get_user_country}}';
    if(default_country_id){
      if($('#client-country').children('[value="'+default_country_id+'"]').length > 0)
      {
       $('#client-country').val(default_country_id).change(); 
      }
    }


  });

</script>
@stop
@section('content')
<div class="container-fluid bgGray noPadding borderBottom" id="ad-post-content-wrapper">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 noPadding">
      <div class="col-lg-9 col-md-9 col-sm-7 col-xs-12 sideBlock">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Create Ad </span>
          </div>
          <div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
            <div id="ads-notice"></div>
           {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_ads', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-type" name="ad_type">
                    <option class="hide">Select</option>
                    @foreach ($ads_types as $ads_type)
                      <option value="{{ $ads_type->id }}">{{ $ads_type->name }}</option>
                    @endforeach
                </select> 
              </div>
            </div>
            <div class="form-group" id="category-ads-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Parent Category</h5>
              <div class="col-sm-9 error-category">
               <select type="text" class="bottomMargin form-control borderZero" id="ads-main-category" name="main_category">
                  <!--   <option class="hide">Select</option> -->
                  <!--  @foreach($category as $row)
                      <option value="{{$row->unique_id}}">{{$row->name}}</option>
                  @endforeach -->
               </select>
               <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="category-auction-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Parent Category</h5>
              <div class="col-sm-9">
                 <select class="bottomMargin form-control borderZero" id="ads-category-auction" name="main_category_auction">
                    <option class="hide">Select</option>
                    @foreach($preloaded_categories as $row)
                      <option value="{{$row->unique_id}}">{{$row->name}}</option>
                    @endforeach
                  </select>
              </div>
            </div>
            <div class="form-group hide" id="category-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
                  <select type="text" class="bottomMargin form-control borderZero" id="ads-category" name="category">
                  </select>
              </div>
            </div>
            <div class="form-group hide" id="subcategory-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category (1)</h5>
              <div class="col-sm-9">
                   <select type="text" class="bottomMargin form-control borderZero" id="ads-subcategory" name="subcategory_one">
                   </select>
              </div>
            </div>
            <div class="form-group hide" id="subcategorythree-pane"> 
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category (2)</h5>
              <div class="col-sm-9">
                  <select class="bottomMargin form-control borderZero" id="ads-subSubcategory" name="subcategory_two">
                  </select>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title </h5>
              <div class="col-sm-9 error-title">
                <input type="text" name="title" class="bottomMargin borderZero form-control borderzero" id="ads-title" maxlength="60">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="price-pane">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9 error-price">
                <input class="bottomMargin form-control borderZero" type="number" id="ads-price" name="price">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="event-date-pane">
               <h5 class="col-sm-3 normalText" for="event-date">Event Date</h5>
              <div class="col-sm-9 error-price">
                <div class="input-group date error-price" id="date-event-date">
                        <input type="text" name="date_of_birth" class="bottomMargin form-control borderZero" id="ads-date_of_birth" value="">
                        <span class="input-group-addon borderZero">
                          <span><i class="fa fa-calendar"></i></span>
                       </span>
                </div>
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="salary-pane">
               <h5 class="col-sm-3 normalText" for="salary">Salary</h5>
              <div class="col-sm-9 error-price">
                <input class="bottomMargin form-control borderZero" type="number" id="ads-salary" name="salary">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description 
               </h5>
              <div class="col-sm-9 error-description">
                <textarea  class="bottomMargin form-control borderZero" id="ads-description" rows="10"  name="description"></textarea>
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
<!-- <div class="col-sm-9 col-sm-offset-3">
<img id="blah" src="#" style="width:400px;">
</div> -->
<?php $i= $get_user_plan_types->img_per_ad - 0; 
             for ($x = 1; $x <= $i; $x++) {
                 echo '
<div class="form-group">
<h5 class="col-sm-3 normalText" for="register4-email">'; if ($x == 1) { echo 'Ad Photos ('.$get_user_plan_types->img_per_ad.')'; } echo '</h5>
  <div class="col-sm-9">
  <div id="preview_'.$x.'">
  <img id="row-img_' . $x . '" class="hide bottomPadding img-responsive" src="#" alt="your image" />
  </div>
  <div class="uploader-pane">
      <div class="change">
    <div class="input-group input-group-sm error-photo'.$x.' error-photo-type'.$x.'">
      <input type="text" name="file_name" id="file_name_'.$x.'" class="form-control borderZero unclickable" placeholder="" aria-describedby="sizing-addon2">
      <span class="input-group-btn">
        <label class="btn-file btn blueButton borderZero btn-sm cursor">
        Browse <input name="photo'.$x.'" type="file" id="row-photo[1]" data-id="'.$x.'" class="photo'.$x.' btn photoName borderZero btn-sm form-control borderZero cursor"  multiple="true" data-show-upload="false" placeholder="Upload a photo..." accept="image/*" ';
                 echo 'value="Upload a Photo">&nbsp;
        </label> 
      </span>
      <span class="input-group-btn">
        <button data-id="'.$x.'" type="button" class="btn removeImage redButton borderZero btn-sm leftMargin"><span style="padding: 1px;">Delete</span></button>
      </span>
    </div>
     <sup class="topPadding sup-errors redText"></sup>
  </div>
  </div>
  </div>
</div>
';
              }
              ?>  
                  <div class="hide" id="auction-type-pane">
                    <h5 class="inputTitle borderbottomLight">Auction Information </h5>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
                      <div class="col-sm-9 error-bid_limit_amount">
                        <input type="number" class="bottomMargin form-control borderZero" id="row-bid_limit_amount" name="bid_limit_amount">
                        <sup class="sup-errors redText"></sup>
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
                      <div class="col-sm-9 error-bid_start_amount">
                        <input type="number" class="bottomMargin form-control borderZero" id="row-bid_start_amount" name="bid_start_amount">
                        <sup class="sup-errors redText"></sup>
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
                      <div class="col-sm-9 error-minimum_allowed_bid">
                        <input type="number" class="bottomMargin form-control borderZero" id="row-minimum_allowed_bid" name="minimum_allowed_bid" placeholder="">
                        <sup class="sup-errors redText"></sup>
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                      <div class="col-sm-9">
                        <div class="col-sm-6 noleftPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8 error-bid_duration_start">
                              <input type="number" class="bottomMargin form-control borderZero" id="row-bid_duration_start" name="bid_duration_start" placeholder="">
                              <sup class="sup-errors redText"></sup>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 norightPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8 error-bid_duration_dropdown_id"> 
                              <select id="row-bid_duration_dropdown_id" name="bid_duration_dropdown_id" class="bottomMargin form-control borderZero inputBox fullSize">
                                <option value="" class="hide" selected>Select:</option>
                                 @foreach ($bid_duration_dropwdown as $value)
                                    <option value="{{$value->id}}">{{$value->value}}</option>
                                 @endforeach
                              </select>
                              <sup class="sup-errors redText"></sup>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <h5 class="inputTitle borderbottomLight bottomPadding hide" id="custom-attribute-container">Custom Attribute</h5>
              <div id="custom-attributes-pane">
              </div>

              <h5 class="inputTitle borderbottomLight bottomPadding">Additional Information </h5>
              <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">Country</h5>
              <div class="col-sm-9 error-country">
                <select class="bottomMargin form-control borderZero" id="client-country" name="country">
                  <option value="" class="hide">Select</option>
                  @foreach($countries as $row)
                    <option value="{{$row->id}}" {{($get_user_country==$row->id ? 'selected' : '')}}>{{$row->countryName}}</option>
                  @endforeach
                </select>
                <sup class="sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
              <h5 class="normalText col-sm-3" for="email">City or State</h5>
              <div class="col-sm-9 error-city">
                <select class="bottomMargin form-control borderZero hide" id="client-city" name="city">
                </select>
                <sup class="sup-errors redText"></sup>


                   <select class="bottomMargin form-control borderZero" id="client-city2" name="city2">
                   <option class="hide">Select</option>
                   @foreach($city as $row)
                    <option value="{{$row->id}}" {{($get_user_city==$row->id ? 'selected' : '')}}>{{$row->name}}</option>
                  @endforeach
                    </select>
              </div>
            </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Address</h5>
                        <div class="col-sm-9">
                              <input class="bottomMargin form-control borderZero" type="text" id="#"  name="address" value='{{ $get_user_address }}'>
                        </div>
                      </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Youtube</h5>
                        <div class="col-sm-9 error-youtube">
                            <input type="text" name="youtube" class="bottomMargin form-control borderZero">
                            <sup class="sup-errors redText"></sup>
                        </div>
                      </div>
     
            <div class="bordertopLight topPaddingB">
              <input type="hidden" name="rem_img_upload" value="<?php echo $i; ?>">
                    <?php echo $post_ad_btn;?>
            </div>
          {!! Form::close() !!}


          </div>
        </div>
        </div>
        </div>
      </div>

 <div class="col-lg-3 col-md-3 col-sm-5 col-xs-12 noPadding">  
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span id="ad_avail" data-rem="{{$get_listing_available}}" class="pull-right"><?php echo($get_listing_available); ?></span></div>
            <hr>
            <div class=""><i class="fa fa-gavel" aria-hidden="true"></i> Auction Ads Available <span id="bid_avail" data-rem="{{$get_auction_ads_available}}" class="pull-right"><?php echo($get_auction_ads_available); ?></span></div>
          </div>
        </div>
          
      <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
{{--           <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div> --}}
          <img src="{{ URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg':$banner_placement_right->image}}" style="height:390px; width:100%;">

       </div>
      </div>

<span class="{{count($featured_ads_per) == 0 ? 'hide' : ''}}">
 <div class="panel panel-default removeBorder bottomMarginB borderZero">
      </div>
      <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        @if($featured_ads_per->isEmpty())
        <p class="text-center">No available featured ads for your country</p>
        @endif
            @foreach ($featured_ads_per as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding ellipsis">
                <a href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}"> 
                  {{-- <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImageB">
                  <img class="adImageB" alt="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 of '.$featured_ad->parent_category_slug.' '.$featured_ad->title}}" title="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 '. $featured_ad->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_ad->photo}}" style="object-fit: cover; overflow: hidden;" />
                </div>

              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a>
                    <div class="normalText bottomPadding bottomMargin ellipsis">
                          <a class="normalText lightgrayText" href="{{ url($featured_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
             </span>

             
             @if($featured_auctions->isEmpty())
             @else
             <div class="panel-heading panelTitleBarLightB">
                <span class="panelRedTitle">FEATURED AUCTIONS</span>
            </div>
            @foreach ($featured_auctions as $featured_auction)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding ellipsis">
                <a href="{{url('/').'/'.$featured_auction->country_code.'/'.$featured_auction->ad_type_slug.'/'.$featured_auction->parent_category_slug.'/'.$featured_auction->category_slug.'/'.$featured_auction->slug}}"> 
                  {{-- <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_auction->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImageB">
                  <img class="adImageB" alt="{{$featured_auction->ad_type_slug.' '.$featured_auction->photo.' 1 of '.$featured_auction->parent_category_slug.' '.$featured_auction->title}}" title="{{$featured_auction->ad_type_slug.' '.$featured_auction->photo.' 1 '. $featured_auction->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_auction->photo}}" style="object-fit: cover; overflow: hidden;" />
                </div>
                
                
              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_auction->country_code.'/'.$featured_auction->ad_type_slug.'/'.$featured_auction->parent_category_slug.'/'.$featured_auction->category_slug.'/'.$featured_auction->slug}}">{{ $featured_auction->title }}</a>
                    <div class="normalText bottomMargin ellipsis">
                          <a class="normalText lightgrayText" href="{{ url($featured_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_auction->alias }}">by {{$featured_auction->name}}</a>
                    </div>
                    <div class="normalText redText" id="count_me_down" title="" data-countdown="{{$featured_auction->ad_expiration}}"></div>
                    <div class="mediumText blueText"><strong>{{ number_format($featured_auction->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_auction->city || $featured_auction->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_auction->city }} @if ($featured_auction->city),@endif {{ $featured_auction->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>
                     
                    @if(isset($featured_ad))
                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                   
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
                    @endif
            </div>
              </div>
             </div>
             @endforeach
             @endif

    </div>

  </div>

</div>

@include('ads.form')
@stop