<div class="modal fade" id="modal-watchlist" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title modal-title-bidder panelTitle">Watchlist</h4>
          </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Add this on your watchlists?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit blueButton borderZero">Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>




<div class="modal fade" id="modal-post_invalid" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
		<!-- 	{!! Form::open(array('url' => 'add-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_form', 'files' => true)) !!}
			 --><div class="modal-header modal-success">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					You've reached the allowed limit of ads, Do you want to upgrade your subscription?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="id" id="row-id" value="">
				<a role="button" href="{{url('dashboard').'#subscription'}}" type="submit" class="btn btn-submit watchButton borderZero"><i class="fa fa-check"></i> Upgrade</a>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			<!-- {!! Form::close() !!} -->
		</div>
	</div>
</div>




<div class="modal fade" id="modal-watchlist-remove" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'remove-ad-watchlist', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-remove_watchlist_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Remove Watchlist</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group nobottomMargin">
				<div class="col-lg-12">
					Remove Ad from your Watchlist?
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="watchitem_id" id="row-watchitem_id">
				<button type="submit" class="btn btn-submit blueButton borderZero">Yes</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal">No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>

<div class="modal fade" id="modal-bidding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		<div id="load-form_bidder" class="loading-pane hide">
			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
		</div>
		{!! Form::open(array('url' => 'ad/add/bidders', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_bidder_form', 'files' => true)) !!}
		<div class="modal-header modalTitleBar">
			<button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
			<h4 class="modal-title modal-title-bidder panelTitle">Bid</h4>
		</div>
		<div class="modal-body">
			<!--<div id="form-notice"></div>-->
			<div id="validation-errors">
			</div>
			
			<div class="col-lg-12">
			<p class="minbid"><span class="leftPadding normalText"></span></p>
			<p>Enter your Bid Amount:<span><input step=".1" type="number" placeholder="" class="suggestedbid form-control borderZero topMargin" name="bidvalue" required="required"></span></p>
			</div>
		</div>
		<div class="clearfix"></div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<button type="submit" class="btn btn-submit redButton borderZero"><i class="fa fa-gavel"></i> Bid</button>
				<button type="button" class="btn blueButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
		{!! Form::close() !!}
		</div>
	</div>
</div>

<!-- <div class="modal fade" id="modal-bidding" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form_bidder" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ad/add/bidders', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-save_bidder_form', 'files' => true)) !!}
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title-bidder">Delete Ads</h4>
			</div>
			<div class="modal-body">
				<div id="form-bidder_notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
				</div>
				</div>
			</div>
			<div class="modal-footer">
				<input type="hidden" name="ads_id" id="row-ads_id" value="">
				<button type="submit" class="btn btn-submit redButton borderZero"><i class="fa fa-gavel"></i> Bid</button>
				<button type="button" class="btn blueButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> -->
@if(Request::url() != (url('user/post/ads/6789') || url('post/ads') || url('post/auctions')))
	@if(count($promotion_popups) != 0)
		 @foreach($promotion_popups as $row)
			<div class="modal fade" id="modal-promotion-popup" role="dialog">
			  <div class="modal-dialog modal-md" id="modal-login_form">
			    <div class="modal-content removeBorder loginSize borderZero">
			      <div class="modal-header modalTitleBar">
			        <button type="button" class="close closeButton btn-hide-promotion" data-dismiss="modal">&times;</button>
			          <h4 class="modal-title panelTitle">{{$row->name}}</h4>
			        </div>
			        <div class="modal-body">
			        	<img src="{{url('uploads').'/'.$row->photo}}" width="100%" height="100%" class="img-responsive">
			        </div>
			         <div class="modal-footer bordertopLight borderBottom">
			          <span class="pull-right">
			            <button type="button" class="btn blueButton borderZero btn-hide-promotion" data-dismiss="modal">Hide</button>
			          </span>
			        </div>
			      </div>
			    </div>
			  </div>
		  @endforeach
	@endif
@endif

@if(Request::route()->getName() == "SEO_URL")
<div id="refresh-bidders-list">
</div>

<!-- Modal -->
<div id="adsFeature" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content borderZero">
      <div class="modal-header modalTitleBar">
        <button type="button" class="close closeButton" data-dismiss="modal">×</button>
          <h4 class="modal-title panelTitle">Featured</h4>
        </div>
      <div class="modal-body">
        <p>Are you sure you want to featured this Ads?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn blueButton borderZero" data-dismiss="modal">Yes</button>
      </div>
    </div>

  </div>
</div>
@endif
