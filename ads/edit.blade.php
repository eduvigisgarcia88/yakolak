@extends('layout.frontend')
@section('scripts')
<script type="text/javascript">
    $_token = "{{ csrf_token() }}";
    function refresh() {
        var loading = $("#load-form");
        $('#modal-form').find('form').trigger('reset');
          location.reload();  
        loading.addClass('hide');

    } 
   $("#btn-change_photo").on("click", function() { 
        $(".uploader-pane").removeClass("hide");
        $("#btn-cancel").removeClass("hide");
        $(".photo-pane").addClass("hide");
        $("#btn-change_photo").addClass("hide");
    });
    $("#btn-cancel").on("click", function() { 
        $(".uploader-pane").addClass("hide"); 
        $(".uploader-pane2").removeClass("hide");
        $(".uploader-pane3").removeClass("hide");
        $(".uploader-pane4").removeClass("hide");
         $("#btn-cancel").addClass("hide");
        $("#btn-change_photo").removeClass("hide");
    });

  $("#ads-type").on("change", function() { 
        var id = $("#ads-type").val();
        $("#category-pane").addClass("hide");
        $("#subcategory-pane").addClass("hide");
        $("#subcategorythree-pane").addClass("hide");
        if (id == 1) {
            $("#category-ads-pane").removeClass("hide");
            $("#category-auction-pane").addClass("hide");
            $("#auction-type-pane").addClass("hide");
        } else {
            $("#category-ads-pane").addClass("hide");
            $("#category-auction-pane").removeClass("hide");
            $("#auction-type-pane").removeClass("hide");
        }
        $("#ads-main-category").html("");
        $("#ads-category-auction").html("");
        $.post("{{ url('get-cat-type-for-post') }}", { id: id, _token: $_token }, function(response) {
          console.log(response);
          if(id == 1){
              if(response.cat_type == "undefined"){
                $("#ads-main-category").html('<option value="all" readOnly>All</option>');  
              }else{
                $("#ads-main-category").html(response.cat_type);     
              } 
          }else{
              if(response.cat_type == "undefined"){
                $("#ads-category-auction").html('<option value="all" readOnly>All</option>');  
              }else{
                $("#ads-category-auction").html(response.cat_type);     
              } 
          }
          
       }, 'json');
  });  
   $("#ads-category").on("change", function() { 
          var id = $("#ads-category").val();
           $("#custom-attribute-container").removeClass("hide");
          if(id=="Y2F0LTI4" || id == "Y2F0LTI5"){
            $("#price-range-container-title").addClass("hide");
            $('#price-range-container').addClass("hide");
          }else{
            $("#price-range-container-title").removeClass("hide");
            $('#price-range-container').removeClass("hide");
          }
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          var subcategory = $("#ads-subcategory");
          if(id){
            
            $('#custom-attributes-pane').addClass('hide');
            function stick(){
              $.post("{{ url('custom-attributes') }}", { id: ($('#ads-main-category').val() != 'Select' ? $('#ads-main-category').val() : $('#ads-category-auction').val()), _token: $_token }, function(response) {
             if(response.rows.length > 0){
               custom_attri.prepend(response.rows);
              }
            }, 'json').complete(function(){
                $('#custom-attributes-pane').removeClass('hide');
              });
            }
            
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                $("#custom-attribute-pane-title").addClass("hide");
                $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                    custom_attri.html(response.rows);
              }     
            }, 'json').complete(stick); 

              subcategory.html("");
            if(id !="all"){
              $.post("{{ url('get-sub-category-for-post') }}", { id: id, _token: $_token }, function(response) {
                if(response == "undefined"){
                  $("#subcategory-pane").addClass("hide");
                  $("#subcategorythree-pane").addClass("hide");
                }else{
                  $("#subcategory-pane").removeClass("hide");
                   subcategory.html(response);
                }
               
              }, 'json');
            }
         }    
    });
 $("#ads-subcategory").on("change", function() { 
      var id = $("#ads-subcategory").val();
      $("#custom-attribute-container").removeClass("hide");
          var custom_attri = $("#custom-attributes-pane");
          custom_attri.html("");
          if(id){
            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
             if(response.rows == ""){
                  $("#custom-attribute-pane-title").addClass("hide");
                  $("#custom-attribute-pane-container").addClass("hide");
              }else{
                  $("#custom-attribute-pane-title").removeClass("hide");
                  $("#custom-attribute-pane-container").removeClass("hide");
                  custom_attri.html(response.rows);
              }     
          }, 'json');
          }
            var container = $("#ads-subSubcategory");
          $.post("{{ url('get-sub-category-three-for-post') }}", { id: id, _token: $_token }, function(response) {
            if(response == "undefined"){
               $("#subcategorythree-pane").addClass("hide");
            }else{
               $("#subcategorythree-pane").removeClass("hide");
               container.html(response);
            }
          }, 'json');
   });
   $("#ads-main-category").on("change", function() {
        var id = $("#ads-main-category").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
                    $("#ads-subcategory").addClass("hide");
                  }else{
                    $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  } 
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
                    $("#custom-attribute-pane-title").addClass("hide");
                    $("#custom-attribute-pane-container").addClass("hide");
                }else{
                    $("#custom-attribute-pane-title").removeClass("hide");
                    $("#custom-attribute-pane-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }     
            }, 'json');
       }
   });
 $("#ads-category-auction").on("change", function() {
        var id = $("#ads-category-auction").val();
        $("#ads-subcategory").html("");
        if(id == "Y2F0LTI5"){ // for jobs
          $("#salary-pane").removeClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").addClass("hide");
        }else if(id == "Y2F0LTI4"){ // for events
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").removeClass("hide");
          $("#price-pane").addClass("hide");
        }else{ // for ads
          $("#salary-pane").addClass("hide");
          $("#event-date-pane").addClass("hide");
          $("#price-pane").removeClass("hide");
        }
        var container = $("#ads-category");
        container.html('');
        $("#category-pane").removeClass("hide");
         if($("#ads-main-category")){
           $.post("{{ url('get-main-subcategory-for-post') }}", { id: id, _token: $_token }, function(response) {
                  if(response == ""){
                    $("#ads-subcategory").addClass("hide");
                  }else{
                    $("#ads-subcategory").removeClass("hide");
                     container.html(response);
                  } 
           }, 'json'); 
            var custom_attri = $("#custom-attributes-pane");
            custom_attri.html("");
              $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
               if(response.rows == ""){
                    $("#custom-attribute-pane-title").addClass("hide");
                    $("#custom-attribute-pane-container").addClass("hide");
                }else{
                    $("#custom-attribute-pane-title").removeClass("hide");
                    $("#custom-attribute-pane-container").removeClass("hide");
                  custom_attri.html(response.rows);
                }     
            }, 'json');
       }
   });  

   $(".btn-change-profile-pic").on("click", function() { 
        $(".photo-upload").removeClass("hide");
        $(".button-pane").addClass("hide");
        $(".file-preview-frame").addClass("hide");
        $(".profile-photo-pane").addClass("hide");
     });
     $(".btn-cancel").on("click", function() { 
        $(".photo-upload").addClass("hide");
        $(".button-pane").removeClass("hide");
        $("#profile-photo").addClass("hide");
        $(".profile-photo-pane").removeClass("hide");
     });

        @foreach($ads_photos as $row)

        $(".remove-image_{{$row->id}}").on("click", function() { 
        $("#photo_{{$row->id}}").addClass("hide");
        // console.log(photo_{{$row->id}});
        $("#delete-photo_{{$row->id}}").removeAttr('disabled','disabled');
        $(".uploader-pane_{{$row->id}}").removeClass("hide");
        $("#placeholder_{{$row->id}}").removeAttr('placeholder');
        $("#placeholder_{{$row->id}}").val("");
        $("#photo_{{$row->id}}").addClass("hide");
        $("#preview_{{$row->id}}").addClass("hide");
        });

        $(".uploader-pane").on("change", "#photo_upload_{{$row->id}}", function() {
          var id = $(this).data('id');
          // console.log(id);
        filename = $(this).val().split("\\").pop();
        $("#placeholder_{{$row->id}}").val(filename);
        // $("#preview_{{$row->id}}")
        });


        function readURL(input,id) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#preview_"+id).attr('src', e.target.result);
            $("#preview_"+id).removeClass("hide");
        }

        reader.readAsDataURL(input.files[0]);
    }
}

 $(".uploader-pane").on("change", "#photo_upload_{{$row->id}}", function() {
    var id = $(this).data('id');
    console.log(id);
    readURL(this,id);
});
        @endforeach

  //  $("#ads-category_id").on("change", function() { 
  //       var id = $("#ads-category_id").val();
  //       $("#custom-attribute-container").removeClass("hide");
  //       $("#ads-subcategory").removeClass("hide");
  //       $("#ads-sub_category_id").addClass("hide");
  //       $("#custom-attr-container").remove();
  //       $_token = "{{ csrf_token() }}";
  //       var city = $("#custom-attributes-pane");
  //       city.html(" ");
  //       var subcategory = $("#ads-subcategory");
  //       if ($("#ads-category_id").val()) {
  //         $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
  //           city.html(response);
  //         }, 'json');
  //         subcategory.html("");
  //         $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
  //           subcategory.html(response);
  //         }, 'json');

  //      }    
  // });


  $("#ads-price").change(function() {                  
        $('#ads-bid_limit_amount').val(this.value);                  
    });

 // $("#ads-category").on("change", function() { 
 //        var id = $("#ads-category").val();
 //        $("#custom-attribute-container").removeClass("hide");
 //        $_token = "{{ csrf_token() }}";
 //        var customAtt = $("#custom-attributes-pane");
 //        customAtt.html(" ");
 //        var subcategory = $("#ads-subcategory");
 //        if ($("#ads-category").val()) {
 //          $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
 //            customAtt.html(response);
 //          }, 'json');
 //          subcategory.html("");
 //          $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
 //            subcategory.html(response);
 //          }, 'json');

 //       }    
 //  });
   $("#client-country").on("change", function() { 
        var id = $("#client-country").val();
        $_token = "{{ csrf_token() }}";
        var city = $("#client-city");
        city.html(" ");
        if ($("#client-country").val()) {
          $.post("{{ url('get-city') }}", { id: id, _token: $_token }, function(response) {
            city.append(response.rows);
          }, 'json');
       }
  });
    //  $("#ads-category").on("change", function() { 
    //       var id = $("#ads-category").val();
    //        $("#custom-attribute-container").removeClass("hide");
    //       if(id=="Y2F0LTI4" || id == "Y2F0LTI5"){
    //         $("#price-range-container-title").addClass("hide");
    //         $('#price-range-container').addClass("hide");
    //       }else{
    //         $("#price-range-container-title").removeClass("hide");
    //         $('#price-range-container').removeClass("hide");
    //       }
    //       var custom_attri = $("#custom-attributes-pane");
    //       custom_attri.html("");
    //       var subcategory = $("#ads-subcategory");
    //       if(id){
    //         $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
    //          if(response.rows == ""){
    //             $("#custom-attribute-pane-title").addClass("hide");
    //             $("#custom-attribute-pane-container").addClass("hide");
    //           }else{
    //               $("#custom-attribute-pane-title").removeClass("hide");
    //               $("#custom-attribute-pane-container").removeClass("hide");
    //             custom_attri.html(response.rows);
    //           }     
    //         }, 'json');

    //           subcategory.html("");
    //         if(id !="all"){
    //           $.post("{{ url('get-sub-category') }}", { id: id, _token: $_token }, function(response) {
    //             subcategory.html(response);
    //           }, 'json');
    //         }
    //      }    
    // });
 // $("#ads-subcategory").on("change", function() { 
 //        var id = $("#ads-subcategory").val();
 //      $("#custom-attribute-container").removeClass("hide");
 //          var custom_attri = $("#custom-attributes-pane");
 //          custom_attri.html("");
 //          if(id){
 //            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
 //             if(response.rows == ""){
 //                $("#custom-attribute-pane-title").addClass("hide");
 //                $("#custom-attribute-pane-container").addClass("hide");
 //              }else{
 //                  $("#custom-attribute-pane-title").removeClass("hide");
 //                  $("#custom-attribute-pane-container").removeClass("hide");
 //                custom_attri.html(response.rows);
 //              }     
 //          }, 'json');
 //          }
 //   });
   // $("#ads-main-category").on("change", function() {
   //      var id = $("#ads-main-category").val();
   //        $("#ads-subcategory").html("");
   //      var container = $("#ads-category");
   //       if($("#ads-main-category")){
   //         $.post("{{ url('get-main-subcategory') }}", { id: id, _token: $_token }, function(response) {
   //                if(response == ""){
   //                  $("#ads-subcategory").addClass("hide");
   //                }else{
   //                  $("#ads-subcategory").removeClass("hide");
   //                  container.html(response);
   //                }
                  
   //         }, 'json'); 
   //          var custom_attri = $("#custom-attributes-pane");
   //          custom_attri.html("");
   //            $.post("{{ url('custom-attributes') }}", { id: id, _token: $_token }, function(response) {
   //             if(response.rows == ""){
   //                $("#custom-attribute-pane-title").addClass("hide");
   //                $("#custom-attribute-pane-container").addClass("hide");
   //              }else{
   //                  $("#custom-attribute-pane-title").removeClass("hide");
   //                  $("#custom-attribute-pane-container").removeClass("hide");
   //                custom_attri.html(response.rows);
   //              }     
   //          }, 'json');
   //         }

   //  });
</script>
@stop
@section('content')
<div class="container-fluid bgGray borderBottom">
  <div class="container bannerJumbutron">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
      <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 noPadding">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLight">
            <span class="panelTitle">Edit Ad </span><span class="redText panelTitle pull-right">{{$ads->title}}</span>
          </div>
          <div class="panel-body">
         <div id="ads-save">
        <div class="form-ads">
            <div id="load-form" class="loading-pane hide">
              <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
            </div>
           
           {!! Form::open(array('url' => 'ads/save', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'save_ads', 'files' => true)) !!}
           <h5 class="inputTitle borderbottomLight bottomPadding">Basic Information</h5>
            <div id="ads-notice"></div>

            <div class="form-group">
            <h5 class="col-sm-3 normalText" for="ad_type">Ad Type</h5>
              <div class="col-sm-9">
                  <select type="text" class="form-control borderZero" id="ads-type" name="ad_type">
                @foreach ($get_ad_types as $get_ad_type) 
                  <option value="{{ $get_ad_type->id}}" {{ ($ads->ads_type_id == $get_ad_type->id ? 'selected' : 'none') }}>{{ $get_ad_type->name }}</option>                 
                @endforeach
                </select> 
              </div>
            </div>              
            <!-- <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title</h5>
              <div class="col-sm-9">
                <input type="text" name="title" class="borderZero form-control borderzero" id="row-title" maxlength="30" value="{{ $ads->title }}">

              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9">
                <input class="form-control borderZero" type="text" id="ads-price" name="price" placeholder="" value="{{ $ads->price }}">
              </div>
            </div> -->
            <div class="form-group {{$ads->ads_type_id  == '1' ? '':'hide'}}" id="category-ads-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Parent Category</h5>
              <div class="col-sm-9 error-category">
               <select type="text" class="bottomMargin form-control borderZero" id="ads-main-category" name="main_category">
                 {!!$parent_category_html!!}
               </select>
               <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group {{$ads->ads_type_id  == '2' ? '':'hide'}}" id="category-auction-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Parent Category</h5>
              <div class="col-sm-9">
                 <select class="bottomMargin form-control borderZero" id="ads-category-auction" name="main_category_auction">
                 {!!$parent_category_html!!}
                  </select>
              </div>
            </div>
            <div class="form-group {{$ads->category_id  == null ? 'hide':''}}" id="category-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Category</h5>
              <div class="col-sm-9">
                  <select type="text" class="bottomMargin form-control borderZero" id="ads-category" name="category">
                  {!!$category_html!!}
                  </select>
              </div>
            </div>
            <div class="form-group {{$ads->sub_category_id  == null ? 'hide':''}}" id="subcategory-pane">
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category (1)</h5>
              <div class="col-sm-9">
                   <select type="text" class="bottomMargin form-control borderZero" id="ads-subcategory" name="subcategory_one">
                  {!!$subcategory_html!!}
                   </select>
              </div>
            </div>
            <div class="form-group {{$ads->sub_category_second_id  == null ? 'hide':''}}" id="subcategorythree-pane"> 
               <h5 class="col-sm-3 normalText" for="register4-email">Sub Category (2)</h5>
              <div class="col-sm-9">
                  <select class="bottomMargin form-control borderZero" id="ads-subSubcategory" name="subcategory_two">
                   {!!$subcategorytwo_html!!}
                  </select>
              </div>
            </div>
             <div class="form-group">
               <h5 class="col-sm-3 normalText" for="title">Title </h5>
              <div class="col-sm-9 error-title">
                <input type="text" name="title" class="borderZero bottomMargin form-control borderzero" value="{{$ads->title}}" id="ads-title" maxlength="60">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="price-pane">
               <h5 class="col-sm-3 normalText" for="price">Price</h5>
              <div class="col-sm-9 error-price">
                <input class="bottomMargin form-control borderZero" type="number" id="ads-price" name="price" value="{{$ads->price}}">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="event-date-pane">
               <h5 class="col-sm-3 normalText" for="event-date">Event Date</h5>
              <div class="col-sm-9 error-price">
                <div class="input-group date error-price" id="date-event-date">
                        <input type="text" name="date_of_birth" class="bottomMargin form-control borderZero" id="ads-date_of_birth" value="">
                        <span class="input-group-addon borderZero">
                          <span><i class="fa fa-calendar"></i></span>
                       </span>
                </div>
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group hide" id="salary-pane">
               <h5 class="col-sm-3 normalText" for="salary">Salary</h5>
              <div class="col-sm-9 error-price">
                <input class="bottomMargin form-control borderZero" type="number" id="ads-salary" name="salary">
                <sup class="topPadding sup-errors redText"></sup>
              </div>
            </div>
            <div class="form-group">
               <h5 class="col-sm-3 normalText" for="register4-email">Description</h5>
              <div class="col-sm-9">
                <textarea  class="bottomMargin form-control borderZero" id="ads-description" rows="10"  name="description">{{ $ads->description }}</textarea>
              </div>
            </div>
   <?php      
   $i = 0; 



  foreach($ads_photos as $row) {
  echo ' <div class="form-group nobottomMargin">
      <h5 class="col-sm-3 normalText" for="register4-email">'.($row->primary == 1 ? '':'').'</h5>
              <div class="col-sm-9">
                 <div id="load-form" class="loading-pane hide">
                           <div><i class="fa fa-inverse fa-spinner fa-spin fa-3x centered"></i></div>
                   </div>';
                  if($row->photo != null){
                 echo '<img src ="'.url('uploads/ads').'/'.$row->photo.'" class="img-responsive" id="photo_'.$row->id.'" width="100%">
                <input type="hidden" name="delete_photo'.$row->id.'" value="'.$row->id.'" id="delete-photo_'.$row->id.'" disabled>';
                  } 
                  echo'  <div class="minPadding noleftPadding norightPadding">
                  <div id="#previewBlock">
                  <img id="preview_' . $row->id . '" class="hide bottomPadding img-responsive"  alt="your image" />
                  </div>
                  <div class="uploader-pane">
      <div class="change">
                      <div class="input-group input-group-sm">
      <input type="text" name="file_name" id="placeholder_'.$row->id.'" class="bottomMargin form-control borderZero unclickable" placeholder="'.($row->photo).'"  aria-describedby="sizing-addon2">
      <span class="input-group-btn uploader-pane_'.$row->id.' '.($row->photo != null ? 'hide':'').'">
        <label class="btn-file btn blueButton borderZero btn-sm cursor">
        Browse <input name="photo'.$row->id.'" id="photo_upload_'.$row->id.'" type="file" title="aw" data-id="'.$row->id.'" class="btn photoName borderZero btn-sm bottomMargin form-control borderZero cursor" data-show-upload="false" placeholder="remove"  accept="image/*">
        </label> 
      </span>
      <span class="input-group-btn leftPadding">
      <input type="hidden" name="id" value="{{$row->id}}">
        <button class="btn redButton borderZero btn-sm remove-image_'.$row->id.' pull-right" type="button"><span style="padding: 1px;">Delete</span></button>
      </span>
    </div>
    </div>
    </div>
    </div>
        </div>
     </div>';
  echo ' <input type="hidden" name="rem_img_upload" value="'.$i.'">';   
}
  ?>       
   
                 <div class="{{ ($ads->ads_type_id == 1 ? 'hide':'') }}" id="auction-type-pane">
                    <h5 class="inputTitle borderbottomLight">Auction Information </h5>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Limit Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="bottomMargin form-control borderZero" id="ads-bid_limit_amount" name="bid_limit_amount" value="{{$ads->bid_limit_amount}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Bid Start Amount</h5>
                      <div class="col-sm-9">
                        <input type="number" class="bottomMargin form-control borderZero" id="ads-bid_start_amount" name="bid_start_amount" value="{{$ads->bid_start_amount}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email">Minimmum Allowed Bid</h5>
                      <div class="col-sm-9">
                        <input type="number" class="bottomMargin form-control borderZero" id="ads-minimum_allowed_bid" name="minimum_allowed_bid" placeholder="" value="{{$ads->minimum_allowed_bid}}">
                      </div>
                    </div>
                    <div class="form-group">
                      <h5 class="col-sm-3 normalText" for="register4-email"></h5>
                      <div class="col-sm-9">
                        <div class="col-sm-6 noleftPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8">
                              <input type="number" class="bottomMargin form-control borderZero" id="ads-bid_duration_start" name="bid_duration_start" placeholder="" value="{{$ads->bid_duration_start}}">
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 norightPadding">
                          <div class="form-group">
                            <h5 class="col-sm-4 normalText" for="register4-email">Bid Duration</h5>
                            <div class="col-sm-8"> 
                              <select id="ads-bid_duration_dropdown_id" name="bid_duration_dropdown_id" class="bottomMargin form-control borderZero">
                                <option value="" class="hide">Select:</option>
                           @foreach ($bid_duration_dropwdown as $value)
                              <option value="{{$value->id}}" {{($ads->bid_duration_dropdown_id==$value->id?'selected':'')}}>{{$value->value}}</option>
                           @endforeach
                              </select>
                            </div>
                          </div>
                          </div>
                        </div>
                      </div>
                    </div>

            <div class="{{ ($custom_attributes == "" ? 'hide':'') }}" id="custom-attr-container">
               <h5 class="inputTitle borderbottomLight bottomPadding" id="custom-attribute-container">Custom Attribute</h5>
              <div id="custom-attributes-pane">
                 <?php echo $custom_attributes;?>
              </div>
                  
            </div>    
                
<!--               <h5 class="inputTitle borderbottomLight bottomPadding hide" id="custom-attribute-container">Custom Attribute</h5>
                  <div id="custom-attributes-pane">
              </div> -->
                <h5 class="inputTitle borderbottomLight bottomPadding">Additional Information </h5>
                        <div class="form-group">
                  <h5 class="normalText col-sm-3" for="email">Country</h5>
                  <div class="col-sm-9">
                    <select class="bottomMargin form-control borderZero" id="client-country" name="country">
                      <option value="" class="hide">Select:</option>
                      @foreach($countries as $row)
                         <option value="{{ $row->id}}" {{ ($ads->country_id == $row->id ? 'selected' : 'none') }}>{{ $row->countryName }}</option>                 
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <h5 class="normalText col-sm-3" for="email">City or State</h5>
                  <div class="col-sm-9">
                    <select class="bottomMargin form-control borderZero" id="client-city" name="city">
                      <option value="" class="hide">Select:</option>
                        @foreach($city as $row)
                             <option value="{{ $row->id}}" {{ ($ads->city_id == $row->id ? 'selected' : 'none') }}>{{ $row->name }}</option>                 
                        @endforeach
                    </select>
                  </div>
                </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Address</h5>
                        <div class="col-sm-9">
                              <input class="bottomMargin form-control borderZero" type="text" id="#"  name="address" value='{{$ads->address}}'>
                        </div>
                      </div>
                      <div class="form-group">
                         <h5 class="col-sm-3 normalText" for="register4-email">Youtube</h5>
                        <div class="col-sm-9">
                            <input type="embed" name="youtube" class="bottomMargin form-control borderZero" value='<?php echo($ads->youtube) ?>'>
                        </div>
                      </div>

            <div class="bordertopLight topPaddingB">
              <input type="hidden" name="id" value='<?php echo($ads->id) ?>'>
              <input type="submit" class="btn blueButton borderZero noMargin pull-right" value="Update Ads">
            </div>
           
          {!! Form::close() !!}
          </div>
        </div>
        </div>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 noPadding">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel panel-default removeBorder borderZero borderBottom ">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">USER AD STATS </span>
          </div>
          <div class="panel-body normalText">
            <div class=""><i class="fa fa-th"></i> Listings Available <span class="pull-right"><?php echo($get_listing_available); ?></span></div>
            <hr>
            <div class=""><i class="fa fa-th"></i> Auction Ads Available <span class="pull-right"><?php echo($get_auction_ads_available); ?></span></div>
          </div>
        </div>
          
        <div class="panel panel-default removeBorder borderZero">
          <div class="panel-heading panelTitleBarLightB">
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
          <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 390px; width: auto;">
          </div>
       </div>
      </div>
        <div class="{{count($featured_ads_per) == 0 ? 'hide' : ''}}">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
            @foreach ($featured_ads_per as $featured_ad)
              <div class="panel panel-default bottommarginveryLight removeBorder borderZero">
              <div class="panel-body noPadding">
                <a href="{{ url('/ads/view') . '/' . $featured_ad->id }}"> 
                  {{-- <div class="adImageB" style="background: url({{ url('uploads/ads/thumbnail').'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImageB">
                  <img class="adImageB" alt="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 of '.$featured_ad->parent_category_slug.' '.$featured_ad->title}}" title="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 '. $featured_ad->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_ad->photo}}" style="object-fit: cover; overflow: hidden;" />
                </div>
                
              </a>
              <div class="rightPadding nobottomPadding lineHeight">
          <a class="mediumText grayText topPadding" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a>
                    <div class="normalText bottomPadding ellipsis">
                          <a class="normalText lightgrayText" href="{{ url($featured_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                      
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left rightMargin"></span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="blueText rightMargin">No Rating</span>
                         <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
             </div>
       <!--  <div class="panel panel-default bottomMarginLight removeBorder borderZero">
        <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        <div class="panel-body adspanelPadding">
        <div class="row">
          <div class="col-md-5 col-sm-5 col-xs-5">
            <div class="fill" style="background: url(http://192.168.254.14/yakolak/public/img/sample.jpg); background-position: center center; background-repeat: no-repeat; background-size: cover; height: 120px; width: auto;">
          </div>
          </div>
          <div class="col-md-7 col-sm-7 col-xs-7">
            <div class="mediumText grayText">Featured Ads 2</div>
            <div class="normalText lightgrayText">by Dell Distributor</div>
            <div class="mediumText blueText"><strong>1,000,000 USD</strong></div>
            <div class="mediumText blueText">
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star"></i>
              <i class="fa fa-star-half-empty"></i>
              <span class="lightgrayText mediumText pull-right rightMargin">27 <i class="fa fa-comment"></i></span>
            </div>
          </div>
        </div>
        </div>
       </div> -->
          
        </div>
      </div>
    </div>
  </div>
</div>

@stop
