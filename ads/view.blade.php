
@extends('layout.frontend')
@section('scripts')
<script src="https://apis.google.com/js/platform.js" async defer></script>
<script type="text/javascript">
$_token = "{{ csrf_token() }}";

function refresh(){
	 var id = "{{ $ads_id }}";
	 var loading = $(".loading-pane");
	 loading.removeClass("hide");
 	 var url = "{{ url('/') }}";
	 $.post("{{ url('ads/fetch-info/'.$ads_id) }}", { id: id, _token: $_token }, function(response) {
	 	$.each(response.rows, function(index, value) {
	 		//console.log(index+"="+value);
	 		var field = $("#rows-" + index);

 			if(field.length > 0) {
                  field.val(value);
              }
             if(index=="name"){
             	 $("#profile-name").text(value)
             }
             if(index=="photo"){
             	console.log(value);
             	 $("#profile-photo").attr('src', url + '/uploads/' + value + '?' + new Date().getTime());
             }       
          }); 
      }, 'json');
	 loading.addClass("hide");
	}
	$('#thumbCarousel').carousel({
		pause: true,
		interval: false
	});
	$('#adsCarousel').carousel({
		pause: true,
		interval: false
	});


 function refreshBidders(){
        var ads_id = $("#ads_id").val();
        var bidder = $("#refresh-bidders");
			$("#countdown").load("{{ url('/') }}"+" #countdown>*","");
        if ($("#ads_id").val()) {
        	$.post("{{ url('ad/bidders/refresh') }}", { ads_id:ads_id, _token: $_token }, function(response) {
            bidder.html(response);
          }, 'json'); 

        };
		$("#bidders").addClass('hide','hide');  
		 refreshBidderList();   
 	}


$("#content-wrapper").on("click", ".btn-watchlist", function() {
	var id = $(this).data('id');
	console.log(id);
	var title = $(this).data('title');
		$("#row-id").val("");
		$("#row-id").val(id);
		$("#row-title").val(title);
		$("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
		$("#modal-form").find('form').trigger("reset");
		$(".modal-title").html('<strong>Watchlists '+ title +' </strong>', title);
		$("#modal-watchlist").modal('show');    
});

$(function () {
	$('#comment-pane').find("#rateYo").rateYo({
	 starWidth: "15px",
	 fullStar: true,
	 ratedFill: "#5da4ec",
	 normalFill: "#cccccc",
	 {{$check_rated_ad != null ? 'readOnly: true':''}}
	});
	$('#comment-pane-mobile').find("#rateYo").rateYo({
	 starWidth: "15px",
	 fullStar: true,
	 ratedFill: "#5da4ec",
	 normalFill: "#cccccc",
	  {{$check_rated_ad != null ? 'readOnly: true':''}}
	});
	$("#averageRate").rateYo({
	 starWidth: "15px",
	 rating: {{floor($get_average_rate)}},
	 ratedFill: "#5da4ec",
	 normalFill: "#cccccc",
	 readOnly: true,
	});
	$("#averageRateForNonMobile").rateYo({
	 starWidth: "15px",
	 rating: {{floor($get_average_rate)}},
	 ratedFill: "#5da4ec",
	 normalFill: "#cccccc",
	 readOnly: true,
	});
	@foreach($featured_ads as $row)
		$("#featured_ads_{{$row->id}}").rateYo({
		 starWidth: "14px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
		});
	@endforeach
	@foreach($vendor_ads as $row)
	    $("#vendor_ads_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
	});
	@endforeach
	@foreach($related_ads as $row)
	    $("#related_ads_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
	});
	@endforeach
	@foreach($related_ads_mobile as $row)
	    $("#related_ads_mobile_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
	});
	@endforeach
	@foreach($related_adset2 as $row)
	    $("#related_adset2_{{$row->id}}").rateYo({
		 starWidth: "15px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         readOnly: true,
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
	});
	@endforeach
	$('#comment-pane-mobile').find("#rateYo").css('cursor','default');
	$('#comment-pane').find("#rateYo").css('cursor','default');

	var starWidth = $('#comment-pane-mobile').find("#rateYo").rateYo("option", "starWidth"); //returns 40px
		$('#comment-pane-mobile').find("#rateYo").rateYo("option", "starWidth", "15px"); //returns a jQuery Element
		$('#comment-pane-mobile').find("#rateYo").rateYo()
		.on("rateyo.set", function (e, data) {
		var rating = data.rating;
		console.log(rating);
		if(rating == 0){
		$('#comment-pane-mobile').find(".btn-rate_product").attr('disabled','disabled');
		}else{
		$('#comment-pane-mobile').find(".btn-rate_product").removeAttr('disabled','disabled');
		}
	});

	var starWidth = $('#comment-pane').find("#rateYo").rateYo("option", "starWidth"); //returns 40px
		$('#comment-pane').find("#rateYo").rateYo("option", "starWidth", "15px"); //returns a jQuery Element
		$('#comment-pane').find("#rateYo").rateYo()
		.on("rateyo.set", function (e, data) {
		var rating = data.rating;
		console.log(rating);
		if(rating == 0){
		$('#comment-pane').find(".btn-rate_product").attr('disabled','disabled');
		}else{
		$('#comment-pane').find(".btn-rate_product").removeAttr('disabled','disabled');
		}
	});


});

$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});

$(document).ready(function(){
	
	setTimeout(function(){
	  $("#modal-pop_up_message").modal("show");
    }, 5000);
 	// setTimeout(function(){
  //         $('#modal-pop_up_message').modal('hide')
  //       }, {{$modal_related_popups_duration}}000);

	$(".btn-rate_product").attr('disabled','disabled');
	$(".btn-save_comment").attr('disabled','disabled');
	$(".submitReply ").attr('disabled','disabled');

    // $('').scrollspy({target: ".rateButton ", offset: 50});

  //   $("#btn-top_rate_product").on('click', function(event) {

  // 		$("html, body").animate({ scrollTop: $(".btn-rate_product").offset().top}, 1500);
 	// });   
});

$('#comment-pane').find("#review-product-pane").on("click", ".btn-rate_product", function() {	
	
		var $rateYo = $('#comment-pane').find("#rateYo").rateYo();
		var review = $("#comment-pane").find("#user-ad-comment").val();
	if(review == ""){
	       $("#comment-notice").removeClass("hide");
	}else {
	   var id = {{ $ads_id }};
	   var rate = $rateYo.rateYo("rating");
	   $("#comment-notice").addClass("hide");
	   $(".modal-header").removeAttr("class").addClass("modal-header modalTitleBar");
		dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Rate Ad</strong></h4>', 'Are you sure you want to rate this ad?', "{{ url('ad/rate') }}",id,rate,review);
	}
});

$('#comment-pane-mobile').find("#review-product-pane").on("click", ".btn-rate_product", function(e) {	
	var $rateYo = $('#comment-pane-mobile').find("#rateYo").rateYo();
	var review = $("#comment-pane-mobile").find("#user-ad-comment").val();
	if(review == ""){
	       $("#comment-notice").removeClass("hide");
	}else {
	   var id = {{ $ads_id }};
	   var rate = $rateYo.rateYo("rating");
	   $("#comment-notice").addClass("hide");
	   $(".modal-header").removeAttr("class").addClass("modal-header modalTitleBar");
		dialog('<h4 class="modal-title modal-title-bidder panelTitle"><strong>Rate Ad</strong></h4>', 'Are you sure you want to rate this ad?', "{{ url('ad/rate') }}",id,rate,review);
	}
	
});
$("#comment-pane").on("keyup", "#user-ad-comment", function() {
	var comment = $(this).val();
		if((jQuery.trim( comment )).length==0 ){
			$("#comment-pane").find(".btn-save_comment").attr('disabled','disabled');
		}else{
			$("#comment-pane").find(".btn-save_comment").removeAttr('disabled','disabled');
		}	
});

$("#comment-pane-mobile").on("keyup", "#user-ad-comment", function() {
	
	var comment = $(this).val();
	  if((jQuery.trim( comment )).length==0 ){
		$("#comment-pane-mobile").find(".btn-save_comment").attr('disabled','disabled');
		}else{
		$("#comment-pane-mobile").find(".btn-save_comment").removeAttr('disabled','disabled');
		}	
});

$(".comments-container").on("keyup", "#reply-message", function() {  
	var reply_message = $(this).parent().parent().parent().parent().parent().find('#reply-message').val();
		if(reply_message == ""){
		$(this).parent().parent().parent().parent().parent().find('.submitReply').attr('disabled','disabled');
		}else{
		$(this).parent().parent().parent().parent().parent().find('.submitReply').removeAttr('disabled','disabled');
		} 
});

$(".comments-container").on("keyup", "#main_comment_reply", function() {  
	var main_reply = $(this).parent().parent().parent().find('#main_comment_reply').val();
		if(main_reply == ""){
		$(this).parent().parent().parent().parent().find('.parentSubmit').attr('disabled','disabled');
		}else{
		$(this).parent().parent().parent().parent().find('.parentSubmit').removeAttr('disabled','disabled');
		} 
});

$(".comments-container").on("click", "#show", function() {
$(this).parent().parent().parent().find('.inputReply').toggleClass("hidden");
// var get_name  = $(this).data('name');
// 	if(get_name != undefined){
// var parse_name = get_name+' :';
// 	$(this).parent().parent().parent().parent().find('#main_comment_reply').val(parse_name);
// 	}else{
// 	parse_name = "";
// 	}
});
@if(Auth::check())
var acc_stat = "{{Auth::user()->status}}";
var has_pending = "{{$has_pending_comments}}";
var has_pending_replies = "{{$has_pending_comments_replies}}";
if(acc_stat == 2 || acc_stat == 3){
	$('#show').prop('disabled',true);
	$('textarea#user-ad-comment').attr('disabled','disabled').css({'cursor' : 'default','background-color' : '#f3f3f3'});
}
if(has_pending == 1 || has_pending_replies == 1 )
{
	$('div#has_pending').append('<div class="alert alert-danger text-center" style="border: -1px solid #ffcccc; border-radius: 0; background-color: #fcf3f3; margin-left: 15px; margin-right: 15px;"><span id="resend_email" class="grayText" style="font-size:14px;">Your comment or review is pending for approval, once its approved there will no longer be any restriction.</span></div>');
}

@endif
$(".comments-container").on("click", "#show_reply_box", function() {
$(this).parent().parent().parent().parent().parent().find('.inputReplyBox').toggleClass("hidden");
var get_name  = $(this).data('name');
	if(get_name != undefined){
	var parse_name = get_name+' :';
	$(this).parent().parent().parent().parent().parent().find('#reply-message').val(parse_name);
	}else{
	parse_name = "";
	}
});


     $("#content-wrapper").on("click", ".btn-remove-watchlist", function() {
    	  var watchitem_id = $(this).data('watchitem_id');
    	  var ad_id = $(this).data('ad_id');
    	  var user_id = $(this).data('user_id');
    	  var title = $(this).data('title');
    	  $("#row-watchitem_id").val(watchitem_id);
    	  $("#row-title").val(title);
		  $("#row-ad_id").val(ad_id);
		  $("#row-user_id").val(user_id);
		
		  $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
          $("#modal-form").find('form').trigger("reset");
          $(".modal-title").html('<strong>Remove Watchlist '+ title +' </strong>', title);
          $("#modal-watchlist-remove").modal('show');    
    });

    // $('#user-ad-comment').on("change", function(e){
    //      var regex = /(<([^>]+)>)/ig;
    //      var message = $('#user-ad-comment').val();
  		//  var validated = message.replace(regex, "");
  		//  if(validated == ""){
  		//  	 $('#user-ad-comment').val("");
  		//  	 $(".btn-save_comment").attr('disabled','disabled');	
  		//  }else{
  		//  	 $('#user-ad-comment').val(validated);
  		//  }
    // });

 //    $(function(){
 
	//  $('#user-ad-comment').keyup(function()	{
	// 		var yourInput = $(this).val();
	// 		re = /[`~!@#$%^&*()_|+\-=?;:'",<>\{\}\[\]\\\/]/gi;
	// 		var isSplChar = re.test(yourInput);
	// 		if(isSplChar)
	// 		{
	// 			var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
	// 			$(this).val(no_spl_char);
	// 		}
	// 	});
 
	// });
	  $(function(){
        $("#addClass").click(function () {
          $('#qnimate').addClass('popup-box-on');
            });
          
            $("#removeClass").click(function () {
              $('#qnimate').removeClass('popup-box-on');
            });
  })


  $("#content-wrapper").on("click", ".btn-add-biding", function() {
     var ads_id = $(this).data('id');
     console.log(ads_id);
     var title = $(this).data('title');
     $("#row-ads_id").val(ads_id);
     $("#row-title").val(title);
	 $("#client-header").removeAttr('class').attr('class', 'modal-header panelTitle');
     $("#modal-form").find('form').trigger("reset");
     $(".modal-title-bidder").html('<strong>Bid on '+ title +' </strong>', title);
     $("#modal-bidding").modal('show'); 
     $("#modal-list-bidders").find('form').trigger("reset");

    });


 $('[data-countdown]').each(function() {
	  var $this = $(this), finalDate = $(this).data('countdown');
	   $this.countdown(finalDate, function(event) {
	   $this.html(event.strftime('%DD-%HH-%MM-%SS'));
	   });
 });


$(function() {
       $.post("{{ url('auction/expired') }}", { _token: $_token}, function(response) {
        ads_id.append(response);   
    }, 'json');
});

$(function() {
       $.post("{{ url('basic-ad/expired') }}", { _token: $_token}, function(response) {
         basic_ads_id.append(response);   
    }, 'json');
});
$(function() {
       $.post("{{ url('auction/expiring') }}", { _token: $_token}, function(response) {
         basic_ads_id.append(response);   
    }, 'json');
});


 jQuery(document).ready(function() {
     $("time.timeago").timeago();
   });



  //  $(function() {
  //       var bidderlist = $("#refresh-bidders-list");
  //       var ads_id = $("#ads_id").val();
  //       bidderlist.html("");
		// $_token = "{{ csrf_token() }}";
  //         $.post("{{ url('ad/bidders-list/refresh') }}", { ads_id: ads_id, _token: $_token }, function(response) {
  //           bidderlist.html(response);
  //         }, 'json');   
         
  // 	});

  //    $(function () {
  //       var bidderlistRefresh = $("#refresh-bidders-list");
  //       var ads_id = $("#ads_id").val();
  //       bidderlistRefresh.html("");
		// $_token = "{{ csrf_token() }}";
  //         $.post("{{ url('ad/bidders-list/refresh') }}", { ads_id: ads_id, _token: $_token }, function(response) {
  //           bidderlistRefresh.html(response);
  //         }, 'json');
  //     });

     
     $('#btn-top_rate_product').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top
	        }, 1000);
    }
    }); 


    $('#btn-comment').on('click', function(event) {

	    var target = $(this.getAttribute('href'));
	    if( target.length ) {
	        event.preventDefault();
	        $('html, body').stop().animate({
	            scrollTop: target.offset().top
	        }, 1000);
    }

});
    // Putting comma every 3 digits to minimum allowed bid and suggested bid - START {
    /*Suggested bid*/
    var auction_type = "{{$getads_info->ads_type_id}}";
    if(auction_type == 2){
	    var arraychecks = "{{$get_all_bidders}}";
	    if(arraychecks == "[]"){
	    	var suggestedbid = "{{$getads_info->bid_start_amount+50}}";
	    }

	    else{
	    	var suggestedbid = "{{$getads_info->minimum_allowed_bid+50}}";
	    }
	    if(/.00/i.test(suggestedbid)){
	    	var suggested_bid = parseInt(suggestedbid);
	    	var tostringsug = suggested_bid.toString();
	    	var suggestedwithcomma = tostringsug.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	    	$('input.suggestedbid').attr('placeholder','Suggested bid is '+suggestedwithcomma+' USD');
	    }
	    else{
	    	$('input.suggestedbid').attr('placeholder','Suggested bid is '+suggestedbid+' USD');
	    }
	    /*Minimum allowed bid*/
	    var arraycheck = "{{$get_all_bidders}}";
	    if(arraycheck == "[]"){
	    	var minbid = "{{$getads_info->bid_start_amount}}";
	    }
	    else{
	    	var minbid = "{{$getads_info->minimum_allowed_bid}}";
	    }
	    var parsed = parseInt(minbid);
 	    if(isNaN(parsed)){
	    	$('.minbid').text('0 USD minimum bid allowed');
	    }
	    else{
		    if(/.00/i.test(minbid)){
		    	var toint = parseInt(minbid);
		    	var minimum_bid_decimal_removed = toint;
		    	var tostringmin = minimum_bid_decimal_removed.toString();
		    	var minbidwithcomma = tostringmin.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		    	$('.minbid').text(minbidwithcomma+' USD minimum bid allowed');
		    }
		    else{
		    	var toint = parseFloat(minbid);
		    	var plusone = toint.toFixed(2);
		    	$('.minbid').text(plusone+' USD minimum bid allowed');
		    }
	    }
    }
    // Putting comma every 3 digits to minimum allowed bid and suggested bid - END }
    //Youtube URL to iFrame - START {
    var testtt = "{{$getads_info->video}}";
    var youtube_url = '<?php echo "$getads_info->youtube"; ?>';
	var regExp = /^.*(youtu\.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
	var match = youtube_url.match(regExp);
	if (match && match[2].length == 11) {
		if(/youtube/i.test(youtube_url)){
	  		$('iframe#yt').attr("src","https://youtube.com/embed/"+match[2]);
	  	}
	  	else if(/youtu.be/i.test(youtube_url)){
	  		$('iframe#yt').attr("src","https://youtube.com/embed/"+match[2]);
	  	}
	  	else{
	  		$('div.yt').remove();
	  	}
	}
	else {
	  $('div.yt').remove();
	}
	var facebookNA = $('span.facebookNA').text();
	var twitterNA = $('span.twitterNA').text();
	var websiteNA = $('span.websiteNA').text();
	if(facebookNA == 'N/A'){
		$('div.fbNA').remove();
	}
	if(twitterNA == 'N/A'){
		$('div.tcoNA').remove();
	}
	if(websiteNA == 'N/A'){
		$('div.webNA').remove();
	}
	if(websiteNA && twitterNA && facebookNA == 'N/A'){
		$('div.socialPanel').remove();
	}
	@if(count($promotion_popups) != 0)
	    window.setInterval( function() {
	  		 $("#modal-promotion-popup").modal("show");
		}, 3000)
	@endif
	$("#modal-promotion-popup").on("click", ".btn-hide-promotion", function() { 
	  	 $("#modal-promotion-popup").remove();
	});


	// $(".btn-delete-ads").click(function() {
 //    	 var id = $(this).data('id');
 //    	 var title = $(this).data('title');
 //    // 	 console.log(id);
 //    // 	 $("input#row-id").val("");
 //  		//  $("input#row-id").val(id);
 //  		//  $("#row-title").val(title);
 //  		//   console.log($("#row-title").val());
 //  		// $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
 //    //     $("#modal-form").find('form').trigger("reset");
 //    //     $(".modal-title").html('<strong>Delete Ad '+ title +' </strong>', title);
 //    //     $("#modal-ads-delete").modal('show'); 
 //        $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
 //      dialog('Delete Ad', 'Are you sure you want to delete this ad</strong>?', "{{ url('profile/photo/remove') }}", id); 
 //    });

	$(".btn-delete-ads").click(function() {
    	 var id = $(this).data('id');
    	 var title = $(this).data('title');
    	 console.log(id);
    // 	 console.log(id);
    // 	 $("input#row-id").val("");
  		//  $("input#row-id").val(id);
  		//  $("#row-title").val(title);
  		// //   console.log($("#row-title").val());
  		// // $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
    //     $("#modal-form").find('form').trigger("reset");
    //     $(".modal-title").html('<strong>Delete Ad '+ title +' </strong>', title);
    //     $("#modal-ads-delete").modal('show'); 
      //   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      dialog('Delete Ad', 'Are you sure you want to delete this ad</strong>?', "{{ url('ads/remove') }}", id); 
    });

    $(".btn-disable-ads-alert-list").click(function() {
    	 var id = $(this).data('id');
    	 var title = $(this).data('title');
    	 console.log(id);
    // 	 console.log(id);
    // 	 $("input#row-id").val("");
  		//  $("input#row-id").val(id);
  		//  $("#row-title").val(title);
  		// //   console.log($("#row-title").val());
  		// // $(".modal-header").removeAttr('class').attr('class', 'modal-header modal-default');
    //     $("#modal-form").find('form').trigger("reset");
    //     $(".modal-title").html('<strong>Delete Ad '+ title +' </strong>', title);
    //     $("#modal-ads-delete").modal('show'); 
      //   $(".modal-header").removeAttr("class").addClass("modal-header modal-info");

      dialog('Disable Ad', 'Are you sure you want to disable this ad</strong>?', "{{ url('ads/disable') }}", id); 
    });

   $("#btn-feature").click(function() {
	    var id = $(this).data('feature-id');
	    var title = $(this).data('ad-title');
	    $('#modal-feature-ads').find("#row-id").val(id);
	    $('#modal-feature-ads').find("#row-title").val(title);
	    $('#modal-feature-ads').modal("show"); 
      // $(".modal-header").removeAttr("class").addClass("modal-header modal-info");
      // dialog('Feature Ad', 'Make this a feature ad</strong>?', "{{ url('ad/feature') }}", id);
  });
  //  $(".btn-disable-ads-alert-list").click(function() {
  //     console.log(id);
  //       var id = $(this).data('id');
  //       var title = $(this).data('title');
  //       $("#row-disable-id").val("");
  //       $("#row-disable-id").val(id);
  //       $("#row-title").val(title);
  //       $("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
  //       $("#modal-form").find('form').trigger("reset");
  //       $(".modal-title").html('<strong>Disable Ad '+ title +' </strong>', title);
  //       $("#modal-ads-disable-list").modal('show');    
  // });

//     $("#content-wrapper").on("click", ".btn-delete-ads", function() {
// 	var id = $(this).data('id');
// 	console.log(id);
	// var title = $(this).data('title');
	// 	$("#row-id").val("");
	// 	$("#row-id").val(id);
	// 	$("#row-title").val(title);
	// 	$("#client-header").removeAttr('class').attr('class', 'modal-header modal-default');
	// 	$("#modal-form").find('form').trigger("reset");
	// 	$(".modal-title").html('<strong>Watchlists '+ title +' </strong>', title);
	// 	$("#modal-ads-delete").modal('show');    
// });
</script>
@stop
@section('content')
<div class="container-fluid bgGray noPadding" id="ad-view-container" data-spy="scroll" data-target=".navbar" data-offset="50">
	<div class="container bannerJumbutron">
		<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
		<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
			<ol class="breadcrumb normalText noMargin noBg borderZero ">
			 {!!$breadcrumb_container!!}
			</ol>
		</div>

<div class="col-md-9 col-sm-7 col-xs-12">
	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
	
	<div class="visible-xs">
		<div class="panel panel-default bottommarginveryLight removeBorder borderZero">
		<div class="panel-body minPaddingB">
			<div class="blueText xlargeText" style="display:inline;"><strong class="ellipsis">{{ $getads_info->title }}</strong>@if(Auth::check())
              @if(!(Auth::user()->id == $getads_info->user_id))
                  @if($watch_item)
                    <span class="right"><a data-watchitem_id="{{ $watch_item->id }}" data-ad_id="{{ $getads_info->ads_id }}" data-user_id="{{ $getads_info->user_id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-remove-watchlist blueText"><i class="fa fa-bookmark blueText cursor" aria-hidden="true"></i></a></span>              
                  @else
                    <span class="right"><a data-id="{{ $getads_info->id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-watchlist lightgrayText"><i class="fa fa-bookmark cursor" aria-hidden="true"></i></a></span>               
                  @endif             
              @endif
              @else
             <span class="right"><a href="#" class="btn-login lightgrayText" data-toggle="modal" data-target="#modal-login"><i class="fa fa-bookmark cursor" data-title="{{ $getads_info->title }}" href="#" data-target="#modal-login" aria-hidden="true"></i></a></span>                
              @endif</div>
			<div class="xlargeText grayText"><strong>{{ number_format($getads_info->price) }} USD</strong></div>
		</div>
	</div>
	<div class="panel panel-default bglightGray removeBorder borderZero bottommarginveryLight">
		<div class="panel-body minPaddingB">

			<div class="mediumText grayTextB">Ad Rating:
				<span class="inlineBlock">
				@if($get_average_rate != 0)
				<span id="averageRateForNonMobile" class="mediumText adRating"></span>
				@else
				<span class="mediumText grayTextB"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
				@endif
				</span>
				<span class="mediumText grayTextB pull-right"> {{count($getads_info->getAdvertisementComments)}} <i class="fa fa-comment" aria-hidden="true"></i></span>
			</div>
		</div>
	</div>
	</div>

		<div class="panel panel-default removeBorder bottomMarginLight borderZero {{$thumbnails->get(0)->photo == 'nopreview.png' ? 'hide' : ''}}">
			<div id="adsCarousel" class="carousel slide carousel-fade" data-ride="carousel">

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
					<div class="item active">
						<img class="img-responsive data-img" data-img="{{ URL::route('uploads', array(), false).'/ads'.'/'.$primary_photos->photo }}" src="{{ URL::route('uploads', array(), false).'/ads'.'/'.$primary_photos->photo }}" alt="{{$getads_info->ad_type_slug.' '.$primary_photos->photo.' 1 of '.$getads_info->category_slug.' '.$getads_info->title}}" style="max-width: 100; max-height: 100%; display: block; -webkit-transition: all 1s ease, all 1s ease;" title="{{$getads_info->ad_type_slug.' '.$primary_photos->photo.' '.$getads_info->title}}">
					</div>
  					@foreach ($ads_photos as $key => $rows)
					<div class="item">
						<img class="img-responsive" src="{{ URL::route('uploads', array(), false).'/ads'.'/'.$rows->photo}}" alt="{{$getads_info->ad_type_slug.' '.$rows->photo.' '.($key+1).' of '.$getads_info->category_slug.' '.$getads_info->title}}" style="max-width: 100; max-height: 100%; display: block; -webkit-transition: all 1s ease, all 1s ease;" title="{{$getads_info->ad_type_slug.' '.$rows->photo.' '.$getads_info->title}}">
						<!-- <div class="" style="background: url({{ URL::route('uploads', array(), false).'/ads'.'/'.$rows->photo}}); background-position: center center; background-repeat: no-repeat; background-size: 100% 100%; min-height: 500px;width: 100%;">
						</div> -->
					</div>
					@endforeach
				</div>
			</div>
<div class="panel-body bgWhite bordertopLightB {{$thumbnails->get(0)->photo == 'nopreview.png' ? 'hide' : ''}}">
<div class="col-md-12 col-sm-12 col-xs-12">
	<ul class="horizontal-slide">
	  <?php $i=0; ?>
	@foreach ($thumbnails as $key => $rows)
	<li class="col-md-2 col-sm-4 col-xs-4 nobottomPadding" style="padding-top: 7px; padding-right:0px; padding-left:0px;">
	<div data-target="#adsCarousel" data-slide-to="{{ $i++ }}" class="" style="height: 80px; width: auto;">
	<img title="{{$getads_info->ad_type_slug.' '.$rows->photo.' '.$getads_info->title}}" alt="{{$getads_info->ad_type_slug.' '.$rows->photo.' '.($key+1).' of '.$getads_info->category_slug.' '.$getads_info->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$rows->photo}}" style="zoom: 2; display: block; margin: auto; height: auto; max-height: 100%; width: auto; max-width: 100%; overflow-x: hidden; overflow-y: hidden" />
	</div>
	</li>  
	@endforeach
	</ul>
	<a class="left carousel-control" href="#adsCarousel" role="button" data-slide="prev" style="width: 5px; background:none; text-align:left; color:#7eacda; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:20px; left:-10px;">
		<span class="fa fa-angle-left" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#adsCarousel" role="button" data-slide="next" style="width: 5px; background:none; text-align:right; color:#7eacda; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:20px; right:0px;">
		<span class="fa fa-angle-right" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	</a>
</div>
</div>
</div>
</div>
<div class="clearfix"></div>
<div class="col-sm-12 noPadding">
	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<div class="panelTitle">Description</div>
				  	<p class="topPadding normalText text-capitalize data-description noMargin" style="word-break: break-word;" data-description="{{ $getads_info->description }}">{!! str_replace('  ', ' &nbsp;', nl2br(htmlentities($getads_info->description))); !!}</p>
				  </div>
				</div>
	<div class="panel panel-default borderZero removeBorder bottomMarginLight">
				  <div class="panel-body noPaddingXs">
				  	<a href=""><img class="fill hidden-xs" src="{{URL::route('uploads', array(), false)}}/banner/advertisement/desktop/default_top.jpg" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
				  	<img class="fill visible-xs" src="{{URL::route('uploads', array(), false)}}/banner/advertisement/mobile/default_top.jpg" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;">
				  	</a>
				  </div>
				  <div class="panel-footer bgWhite">
				  	<span class="panelRedTitle">ADVERTISEMENT</span>
						<span class="pull-right normalText"><a class="redText" href="">view rates</a></span>
				 	</div>
				</div>
<div class="visible-xs">
	<div class="panel panel-default removeBorder borderZero bottommarginveryLight bgWhite">
		<div class="panel-body minPaddingB">
		<!-- 					  	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
		<div class="panel-body topPaddingB bgGray"> -->
			<div class="col-xs-12 bottomPadding noPadding">
				<div class="profileBlock">
				<a class="panelRedTitle" href="{{ url('/') . '/' . $getads_info->alias}}">
					<div class="profileImage">
						<img src="{{ URL::route('uploads', array(), false).'/'.$getads_info->user_photo}}" class="avatar" alt="user profile image" style="width: 85px; height: 85px;">
					</div>
				</a>
				<div class="profileText">
				<a class="panelRedTitle" href="{{ url('/') . '/' . $getads_info->alias }}">
				<div class="mediumText grayText"><strong>{{ $getads_info->name }}</strong></div></a>
				@if ($getads_info->ads_type_id != 2)
{{-- 				<div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->email }}"><i class="fa fa-envelope"></i> {{ $getads_info->email }}</div>
 --}}				@if ($getads_info->telephone)
				<div class="normalText grayText {{ $getads_info->telephone == '' ? 'hide':'' }}" data-placement="left" data-toggle="tooltip" title="{{ $getads_info->telephone }}"><i class="fa fa-phone"></i> {{ $getads_info->telephone}}</div>
				@else
				<div class="normalText grayText {{ $getads_info->mobile == '' ? 'hide':'' }}" data-placement="left" data-toggle="tooltip" title="{{ $getads_info->mobile }}"><i class="fa fa-phone"></i> {{ $getads_info->mobile }}</div>
				@endif
				@else 

{{-- 				<div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->email }}"><i class="fa fa-envelope"></i> {{ $getads_info->email }}</div>
 --}}				@if ($getads_info->telephone)
				<div class="normalText grayText {{ $getads_info->telephone == '' ? 'hide': ''}}" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->telephone }}"><i class="fa fa-phone"></i> {{ $getads_info->telephone }}</div>
				@else 
				<div class="normalText grayText {{ $getads_info->mobile == '' ? 'hide':'' }}" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->mobile }}"><i class="fa fa-phone"></i> {{ $getads_info->mobile}}</div>
				@endif

				@endif
				@if ($getads_info->city || $getads_info->countryName)
				<div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->city }}, {{ $getads_info->countryName }}"><i class="fa fa-map-marker"></i> {{ $getads_info->city }}
				@if ($getads_info->city) 
				, 
				@endif
				{{ $getads_info->countryName }}</div>
				@endif
				</div>
				</div>
			</div>
		@if($rows->description == "")
		@else
		<div class="col-md-12 col-sm-12 col-xs-12 noPadding normalText topPaddingB">
		<article class="bottomMarginB">
		<?php echo $rows->description; ?>
		</article>
		</div>
		@endif

		<!-- 							  </div>
		</div> -->

		</div>
	</div>
{{-- <div class="panel panel-default bglightGray removeBorder borderZero noMargin socialPanel">
<div class="panel-body minPaddingB">		
<div class="lineHeightA">
<div class="normalText grayText fbNA"><i class="fa fa-globe"></i><span class="websiteNA leftPadding">{{$getads_info->website == "" ? 'N/A' : $getads_info->website}}</span></div>
<div class="normalText grayText tcoNA"><span class="blueText"><i class="fa fa-facebook-square"></i></span><span class="leftPadding facebookNA">{{ $getads_info->facebook_account == ""  ? 'N/A': $getads_info->facebook_account }}</span></div>
<div class="normalText grayText webNA"><span class="blueText"><i class="fa fa-twitter"></i></span><span class="leftPadding twitterNA">{{ $getads_info->twitter_account == "" ? 'N/A': $getads_info->twitter_account}}</span></div>
</div>



</div>
</div> --}}

<div class="panel panel-default bglightGray removeBorder borderZero bottomMarginLight">
<div class="panel-body noPadding">
<div class="">
@if(Auth::check())
@if(!(Auth::user()->id == $getads_info->user_id))
<div class="col-md-12 noPadding">
<button data-target = "#modal-send-msg" data-toggle="modal" class="btn fullSize mediumText blueButton borderZero">
<i class="fa fa-paper-plane"></i> 
Message {{$getads_info->name }}
{{ $getads_info->online_status == "1" ? '(online)':'(offline)' }}
</button>
</div>				  
@else
<!-- 		<div class="col-md-12 noPadding">
<p class="alert alert-info borderZero">It's your own listing, you can't contact the publisher.</p>
</div> -->
@endif
@else
<div class="col-md-12 noPadding">
<button  data-target = "#modal-login" data-toggle="modal" class="btn fullSize mediumText blueButton borderZero">
<i class="fa fa-paper-plane"></i>
Message {{$getads_info->name }}
{{ $getads_info->online_status == "1" ? '(online)':'(offline)' }}
</button>
</div>				  
@endif
</div>
</div>
</div>
</div>
<span class="visible-xs">
	<div class="panel panel-default removeBorder borderZero topMargin bottomMargin {{$getads_info->ads_type_id == '1' ? 'hide':''}}">
		<div class="panel-body minPaddingB lineHeight">
			<strong><div class="norightPadding normalWeight xlargeText redText text-center" data-countdown="{{$getads_info->ad_expiration}}"></div></strong>
			@if($get_all_bidders->isEmpty())
			<div class="normalText grayText text-center">{{$getads_info->bid_start_amount+1}} USD minimum allowed bid.</div>
			@else
			<div class="normalText grayText text-center">{{$getads_info->minimum_allowed_bid+1}} USD minimum allowed bid.</div>
			@endif
		</div>
	</div>
	@if($getads_info->ads_type_id != 2)
	<div class="">
        <div class="">
    @else
	<div class="panel panel-default removeBorder bottomMarginLight borderZero">
        <div class="panel-body minPaddingB lineHeight">
        	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
        		<span class="mediumText grayText text-uppercase"><strong>Current Bids</strong></span>
        	</div>
        	@endif
        	@foreach($get_all_bidders as $currentbids)
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <a href="{{url('/').'/'.$currentbids->alias}}"><img src="{{URL::route('uploads', array(), false).'/'.$currentbids->photo}}" class="avatar" alt="user profile image" style="width: 52px; height: 52px;"></a>
              </div>
              <div class="commentText">
                <div class="panelTitleB bottomPadding"><a style="color:#5da4ec" href="{{url('/').'/'.$currentbids->alias}}">{{$currentbids->username}}</a><span class="smallText topPadding lightgrayText pull-right">{{ date("m/j/Y", strtotime($currentbids->created_at)) }}</span></div>
                <p class="normalText">Placed {{number_format($currentbids->bid, 2, '.', ',')}} USD</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
      <div class="panel panel-default bglightGray removeBorder borderZero bottomMargin {{$getads_info->ads_type_id == '1' ? 'hide':''}}">
	<div class="panel-body noPadding">
		<div class="">
		@if (Auth::check())
		@if (Auth::user()->id != $getads_info->user_id)
			@if($getads_info->status == 3)
		<button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-trophy"></i> You won the bidding</button>
			@else
		<button class="btn fullSize mediumText redButton borderZero btn-add-biding" {{Auth::user()->status == 2 || Auth::user()->status == 3 ? 'disabled':''}} data-id="{{$getads_info->id}}" data-title="{{$getads_info->title}}"><i class="fa fa-gavel"></i> Place Bid</button>
			@endif
		@endif
		@else
			@if($getads_info->status == 3)
		<button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-gavel"></i> Auction Ended</button>
			@else
		<button class="btn fullSize mediumText redButton borderZero" class="btn-login" data-toggle="modal" data-target="#modal-login"><i class="fa fa-gavel"></i> Place Bid</button>
			@endif
		@endif
		</div>
	</div>
</div>
  </span>

  				@if($custom_attributes)
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				 <div class="panelTitle">Custom Attributes</div>
				  <div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  	<div class="row">
						<?php if($custom_attributes == ""){ echo "<div class='col-lg-12 col-md-12 col-sm-12 topPadding'>Not Specified</div>";}else{ echo $custom_attributes;}?>
				  	</div>
				  </div>
				</div>
				</div>
				@endif

				<div class="hidden-xs">
				@if($getads_info->youtube)
				<div class="panel panel-default bottomMarginLight removeBorder borderZero yt">
				  <div class="panel-body">
				  	<span class="panelTitle">Video</span>
				  	<div class="topMarginB embed-responsive embed-responsive-16by9">
					  <p class="embed-responsive-item" src="..."><iframe id="yt" src="" allowfullscreen></iframe></p>
					</div>
				  </div>
				</div>
				@endif
				</div>
				<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">
				  	<div class="panelTitle">Contact Information</div>
				  	<div class="topPadding">
				@if($getads_info->ads_type_id != 2)  	
				  	@if($getads_info->address)
				  	<p class="normalText nobottomMargin topPadding grayText"><strong>Address:</strong><span class="text-capitalize normalText topPadding grayText"> {{ $getads_info->address }}, {{$getads_info->city}}, {{$getads_info->countryName}}</span></p>
				  	@endif
				  	@if($getads_info->telephone)
				  	<p class="normalText nobottomMargin topPadding grayText"><strong>Phone No.:</strong><span class="text-capitalize normalText topPadding grayText"> {{ $getads_info->telephone }}</span></p>
				  	@endif
				  	@if($getads_info->email)
				  	<p class="normalText nobottomMargin topPadding grayText"><strong>Email Address:</strong><span class="normalText topPadding grayText"> {{ $getads_info->email }}</span></p>
				  	@endif
				  	@if($getads_info->office_hours)
				  	<p class="normalText nobottomMargin topPadding grayText"><strong>Office Hours:</strong><span> {{ $getads_info->office_hours }}</span></p>
				  	@endif

				  	@if ($getads_info->facebook_account || $getads_info->twitter_account)
				  	<div class="panelTitle topPaddingB">Social Media</div>
				  	@endif
				  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
				  		@if($getads_info->facebook_account)
				  		<div class="col-md-4 col-sm-4 col-xs-6 noleftPadding"><p class="nobottomMargin rightMargin normalText topPadding grayText"><i class="fa fa-facebook-square"></i> {{ $getads_info->facebook_account }}</p></div>
				  		@endif
				  		@if($getads_info->twitter_account)
				  		<div class="col-md-4 col-sm-4 col-xs-6 noleftPadding"><p class="nobottomMargin rightMargin normalText topPadding grayText"><i class="fa fa-twitter"></i> {{ $getads_info->twitter_account }}</p></div>
						@endif
				  </div>
				@else
					 @if($get_winner_bidder)
					 	@if($get_winner_bidder->user_id == Auth::user()->id)
							@if($getads_info->address)
						  	<p class="normalText nobottomMargin topPadding grayText"><strong>Address:</strong><span class="text-capitalize normalText topPadding grayText"> {{ $getads_info->address }}</span></p>
						  	@endif
						  	@if($getads_info->telephone)
						  	<p class="normalText nobottomMargin topPadding grayText"><strong>Phone No.:</strong><span class="normalText topPadding grayText"> {{ $getads_info->telephone }}</span></p>
						  	@endif
						  	@if($getads_info->email)
						  	<p class="normalText nobottomMargin topPadding grayText"><strong>Email Address:</strong><span class="normalText topPadding grayText"> {{ $getads_info->email }}</span></p>
						  	@endif
						  	@if($getads_info->office_hours)
						  	<div class="mediumText grayText"><strong>Office Hours:</strong><span> {{ $getads_info->office_hours }}</span></div>
						  	@endif

						  	@if ($getads_info->facebook_account || $getads_info->twitter_account)
						  	<div class="panelTitle topPaddingB bottomPaddingB">Social Media</div>
						  	@endif
						  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
						  		@if($getads_info->facebook_account)
						  		<div class="col-md-4 col-sm-4 col-xs-6 noleftPadding"><span class="rightMargin mediumText topPadding grayText"><i class="fa fa-facebook-square"></i> {{ $getads_info->facebook_account }}</span></div>
						  		@endif
						  		@if($getads_info->twitter_account)
						  		<div class="col-md-4 col-sm-4 col-xs-6 noleftPadding"><span class="rightMargin mediumText topPadding grayText"><i class="fa fa-twitter"></i> {{ $getads_info->twitter_account }}</span></div>
								@endif
						  </div>
						@else	
							<p class="noMargin alert alert-yakolak grayText grayBorder borderZero">Contact information is hidden. It only shows to the Bid Winner.</p>
						@endif
					@else
							<p class="noMargin alert alert-yakolak grayText grayBorder borderZero">Contact information is hidden. It only shows to the Bid Winner.</p>

					@endif	
				@endif
			</div>
				</div>
			</div>
			<div class="visible-xs">
				@if($getads_info->youtube)
				<div class="panel panel-default bottomMarginLight removeBorder borderZero yt">
				  <div class="panel-body">
				  	<span class="panelTitle">Video</span>
				  	<div class="topMarginB embed-responsive embed-responsive-16by9">
					  <p class="embed-responsive-item" src="..."><iframe id="yt" src="" allowfullscreen></iframe></p>
					</div>
				  </div>
				</div>
				@endif
				</div>
			<div class="panel panel-default bottomMarginLight removeBorder borderZero">
				  <div class="panel-body">

<!-- 			  	<iframe width="100%" height="400px"frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDuGKZMGH7Fo4H8SWyvNJWYelH5brT4bTg&q='{{ $getads_info->city }}'+'{{ $getads_info->countryName }}'" allowfullscreen></iframe> -->

			  	@if($getads_info->address)
			  	<iframe width="100%" height="400px "frameborder="0" style="border:0" src="https://www.google.com/maps?q={{ $getads_info->address }},{{ $getads_info->city }},{{ $getads_info->countryName }}&output=embed" allowfullscreen></iframe>
					
			  	@else
			  	<iframe width="100%" height="400px "frameborder="0" style="border:0" src="https://www.google.com/maps?q={{ $getads_info->city }},{{ $getads_info->countryName }}&output=embed" allowfullscreen></iframe>
					
			  	@endif
				</div>
			</div>
			<div class="panel panel-default removeBorder nobottomPadding bottomMarginLight borderZero">
				  <div class="panel-body">
				  	<ul class="list-inline noMargin">
						  <li class="col-md-4 col-xs-4"><a href="http://www.facebook.com/sharer.php?u=<?php echo urlencode(Request::url()); ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=500');return false;" class="btn btn-block borderZero btn-facebook"><span class="mediumText"><i class="mediumText fa fa-facebook-official"></i></span ><span class="leftPadding hidden-xs normalText text-uppercase">Facebook</span></a></li>

						  <li class="col-md-4 col-xs-4"><a href="https://plus.google.com/share?url={{Request::url()}}" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=500');return false;" class="btn btn-block borderZero btn-google-plus"><i class="mediumText fa fa-google-plus"></i> <span class="leftPadding hidden-xs normalText text-uppercase">Google</span></a></li>

						  <li class="col-md-4 col-xs-4"><a target="_blank" href="https://twitter.com/share?text=Check this out on Yakolak&url={{Request::url()}}&hashtags=Yakolak" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=450,width=500');return false;" class="btn btn-block borderZero btn-twitter"><i class="mediumText fa fa-twitter"></i> <span class="leftPadding hidden-xs normalText text-uppercase">Twitter</span></a></li>

						  <!-- <li class="col-sm-3"><a class="btn btn-block borderZero btn-instagram"><i class="mediumText fa fa-instagram"></i> <span class="leftPadding hidden-xs normalText text-uppercase">Instagram</span></a></li> -->
<!-- 							<li><a class="btn btn-block btn-linkedin borderRadius"><i class="mediumText fa fa-linkedin"></i> <span class="leftPadding hidden-xs hidden-sm normalText text-uppercase">Linkedin</span></a></li>
							<li><a clasdes="btn btn-block btn-tumblr borderRadius"><i class="mediumText fa fa-tumblr"></i> <span class="leftPadding hidden-xs hidden-sm normalText text-uppercase">Tumblr</span></a></li>
							<li><a class="btn btn-block btn-reddit borderRadius"><i class="mediumText fa fa-reddit-alien"></i> <span class="leftPadding hidden-xs hidden-sm normalText text-uppercase">Reddit</span></a></li> -->
						</ul>
<!-- 				  <div class="col-sm-12 noPadding">
				  	<div class="col-sm-3 topPadding">
				  		<a class="btn btn-block btn-social btn-facebook borderZero"><i class="fa fa-facebook"></i> Facebook</a>
				  	</div>
				  	<div class="col-sm-3 topPadding">
				  		<a class="btn btn-block btn-social btn-google-plus borderZero"><i class="fa fa-google-plus"></i> Google</a>
				  	</div>
				  	<div class="col-sm-3 topPadding">
				  		<a class="btn btn-block btn-social btn-twitter borderZero"><i class="fa fa-twitter"></i> Twitter</a>
				  	</div>
				  	<div class="col-sm-3 topPadding">
				  		<a class="btn btn-block btn-social btn-instagram borderZero"><i class="fa fa-instagram"></i> Instagram</a>
				  	</div>
				  </div> -->
<!-- 			  	<ul class="list-inline nobottomMargin">
				<li><a class="btn btn-block btn-social btn-facebook borderZero"><i class="fa fa-facebook"></i> Facebook</a></li>
				<li><a class="btn btn-block btn-social btn-google-plus borderZero"><i class="fa fa-google-plus"></i> Google</a></li>
				<li><a class="btn btn-block btn-social btn-twitter borderZero"><i class="fa fa-twitter"></i> Twitter</a></li>
          		<li><a class="btn btn-block btn-social btn-instagram borderZero"><i class="fa fa-instagram"></i> Instagram</a></li>
				</ul> -->
				</div>
			</div>

			@if(Auth::check())
				@if(Auth::user()->id == $getads_info->user_id)
				<div class="panel panel-default removeBorder borderZero bottomMarginLight ">
					 <div class="panel-body">
						<div class="panelTitle">Tools</div>
						<div class="col-sm-12 noPadding">
						<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 topPadding">
						<a type="button" href="{{ url('ads/edit').'/'.$getads_info->id}}" class="btn btn-default borderZero fullSize">Edit</a>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 topPadding">
						<button type="button" class="btn btn-default borderZero fullSize" data-toggle="modal" data-target="#adsFeature">Feature</button>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 topPadding">
						<button type="button"  data-id="{{$getads_info->id}}" class="btn btn-default borderZero fullSize btn-disable-ads-alert-list">Disable</button>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 topPadding">
						<button type="button" data-id="{{$getads_info->id}}" data-title="{{$getads_info->title}}" class="btn-delete-ads btn btn-default borderZero fullSize">Delete</button>
						</div>
						</div>
						</div>
					</div>
				</div>
				@endif
			@endif
		</div>
</div>
			<div class="col-md-3 col-sm-5 col-xs-12 noPadding sideBlock" id="content-wrapper">
				<div class="col-md-12 col-sm-12 col-xs-12 noPadding">
					<div class="hidden-xs bottomMarginB">
					<div class="panel panel-default bottommarginveryLight removeBorder borderZero">
					  <div class="panel-body minPaddingB">
					  	<div class="blueText xlargeText" style="display:inline;"><strong class="data-title" data-title="{{$getads_info->title}}">{{ $getads_info->title }}</strong>@if(Auth::check())
              @if(!(Auth::user()->id == $getads_info->user_id))
                  @if($watch_item)
                    <span class="right"><a data-watchitem_id="{{ $watch_item->id }}" data-ad_id="{{ $getads_info->ads_id }}" data-user_id="{{ $getads_info->user_id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-remove-watchlist blueText"><i class="fa fa-bookmark blueText cursor" aria-hidden="true"></i></a></span>              
                  @else
                    <span class="right"><a data-id="{{ $getads_info->id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-watchlist lightgrayText"><i class="fa fa-bookmark cursor" aria-hidden="true"></i></a></span>               
                  @endif             
              @endif
              @else
             <span class="right"><a href="#" class="btn-login lightgrayText" data-toggle="modal" data-target="#modal-login"><i class="fa fa-bookmark cursor" data-title="{{ $getads_info->title }}" href="#" data-target="#modal-login" aria-hidden="true"></i></a></span>                
              @endif</div>
					  	<div class="xlargeText grayText"><strong>{{ number_format($getads_info->price) }} USD</strong></div>
<!-- 					  	<div class="mediumText lightgrayText">Ad Rating
					  	@if($get_average_rate != 0)
					  	<span id="averageRate" class="mediumText adRating pull-right"></span>
					  	@else
					  	<span class="mediumText lightgrayText pull-right">N/A</span>
					  	@endif
					  	</div> -->
<!-- 					  	@if(Auth::check())
					  	@if(!(Auth::user()->id == $getads_info->user_id))
				 		 <div class="row topPaddingB">
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><button href ="#rate_product" class="btn fullSize normalText rateButton borderZero" id="btn-top_rate_product"><i class="fa fa-star rightMargin"></i>Rate Product</button></div>
					  			@if($watch_item)
					  				<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><a data-watchitem_id="{{ $watch_item->id }}" data-ad_id="{{ $getads_info->ads_id }}" data-user_id="{{ $getads_info->user_id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-remove-watchlist"><button class="btn fullSize normalText watchButton borderZero"><i class="fa fa-eye rightMargin"></i> Remove Watch Item</button></a></div>
					  			@else
					  				<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><a data-id="{{ $getads_info->id }}" data-title="{{ $getads_info->title }}" href="#" data-toggle="modal" class="btn-watchlist"><button class="btn fullSize normalText watchButton borderZero"><i class="fa fa-eye rightMargin"></i> Watch Item</button></a></div>
					  			@endif
					  		</div>
					  		<div class="col-md-12 noPadding"><button href="#user-ad-comment" id="btn-comment" class="btn fullSize normalText blueButton borderZero"><i class="fa fa-comments"></i> Leave Comment</button></div>	  	  					
					    @endif
					  	@else
					   <div class="row topPaddingB">
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding"><button  class="btn fullSize normalText rateButton borderZero"><i class="fa fa-star rightMargin"></i>Rate Product</button></div>
					  		<div class="col-md-12 col-sm-6 col-xs-12 bottomPadding "><a href="#" class="btn-login" data-toggle="modal" data-target="#modal-login"><button class="btn fullSize normalText watchButton borderZero" data-title="{{ $getads_info->title }}" href="#" data-target="#modal-login"><i class="fa fa-eye rightMargin"></i> Watch Item</button></a></div>
					  		
					  		</div>
					  		<div class="col-md-12 noPadding"><button class="btn fullSize normalText blueButton borderZero"><i class="fa fa-comments rightMargin"></i> Leave Comment</button></div>	  	  					
					   	@endif -->
				  		
<!-- 					  	@if($getads_info->ads_type_id == 2)
  		     			<div class="col-md-12 blockTop noPadding">
								<div class="panel panel-default bottomMarginLight removeBorder borderZero">
								  <div class="panel-body lineHeightC bgGray"> 	
				                 <div id="load-form_bidder" class="loading-pane hide">
				                      <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
				                  </div> -->
								  		<!--ad timer -->
<!--               						   <div class="col-lg-12 col-md-12 col-xs-12 noPadding">

							         	<span class="norightPadding normalText redText" data-countdown="{{$getads_info->ad_expiration}}"></span> <span class="normalText redText">Left</span>
							          <span class="leftPadding normalText pull-right leftMargin">Bid cost: {{$getads_info->minimum_allowed_bid}}</span>
							      </div>

							          <div id="load-form_bidder" class="loading-pane hide">
							        		<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
									  </div>
									  <div id="refresh-bidders">
										</div>
									  <div id="bidders">
						  			<?php  $i = 1; ?>
								   	@foreach ($get_bidders as $bidders)
								   		@if($i == 1)
              						   <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   			<div class="mediumText grayText">
								   				<strong>
								   					<span class="col-lg-1 noPadding"> <div style="display:none;">{{$i++}}</div><img src="{{ url('/').'/uploads/'.$bidders->photo}}"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText grayText" href="{{ url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name }}">{{$bidders->name}}</a> </span><span class="noPadding pull-right">{{number_format($bidders->bid)}} pts.</span>
								   				</strong>
								   			</div>
								   		</div>
								   			@if($getads_info->ad_expiration <= $current_time)
									  			@if($getads_info->ads_type_id == '2' && $getads_info->status == '3')
              						  				 <div class="col-lg-12 col-md-12 col-xs-12 noPadding">
									  					<div class="blueText mediumText noPadding noMargin" style="text-align: center;">Bid Winner</div>
									  				</div>	
									  			@endif
									  		@endif
								   		@else
									  		@if($getads_info->status != '3')
              						   			<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   					<div class="normalText {{($getads_info->status == 3?'lightgrayText':'grayText')}}">
								   						<span class="col-lg-1 noPadding"><div style="display:none;">{{$i++}}</div> <img src="{{ url('/').'/uploads/'.$bidders->photo}}"  class="img-circle noPadding" alt="Cinque Terre" width="20" height="20"/> </span><span class="col-lg-7 noPadding">&nbsp;&nbsp;<a class="mediumText {{($getads_info->status == 3?'lightgrayText':'grayText')}}" href="{{ url('/user/vendor') . '/' . $getads_info->user_id .'/'. $bidders->name }}">{{$bidders->name}}</a> </span><span class="noPadding pull-right">{{number_format($bidders->points)}} pts.</span>
								   					</div>
								   				</div>
								   			@endif
								   		@endif

									@endforeach	
									@if(count($get_all_bidders) > 3)
								   		    <a href="#" class="news-expand pull-left normalText" data-toggle="modal" data-target="#modal-list-bidders" aria-controls="rmjs-1">View More</a>
								   	@elseif (count($get_all_bidders) == 0)
              						   			<div class="col-lg-12 col-md-12 col-xs-12 noPadding">
								   						<center>No Bidders yet</center>
								   				</div>
								   	@else 

								   	@endif	    
									</div>
								  </div>
								</div>
										<input type="hidden" name="ads_id" id="ads_id" value="{{$getads_info->id}}">
									@if (Auth::check())
										@if (Auth::user()->id != $getads_info->user_id) 
										<button class="btn fullSize mediumText blueButton borderZero btn-add-biding" data-id="{{$getads_info->id}}" data-title="{{$getads_info->title}}"><i class="fa fa-money"></i> Bid</button>
						 	 			@endif
						 	 		@else
										<button class="btn fullSize mediumText blueButton borderZero" class="btn-login" data-toggle="modal" data-target="#modal-login"><i class="fa fa-money"></i> Bid</button>	
						 	 		@endif
						 	 	</div>
					  	
					  	@else
						 
					  	@endif -->

					  </div>
				</div>
				
				<div class="panel panel-default bglightGray removeBorder borderZero bottommarginveryLight">
				  <div class="panel-body minPaddingB">

				  	<div class="mediumText grayTextB">Ad Rating:
				  		<span class="inlineBlock">
				  	@if($get_average_rate != 0)
				  	<span id="averageRate" class="mediumText adRating"></span>
				  	@else
				  	<span class="mediumText grayTextB"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
				  	@endif
				  </span>
				  	<span class="mediumText grayTextB pull-right"> {{count($getads_info->getAdvertisementComments)}} <i class="fa fa-comment" aria-hidden="true"></i></span>
				  	</div>
				  </div>
				</div>
					<div class="panel panel-default removeBorder borderZero bottommarginveryLight bgWhite">
						  <div class="panel-body minPaddingB">
<!-- 					  	<div class="panel panel-default bottomMarginLight removeBorder borderZero">
							  <div class="panel-body topPaddingB bgGray"> -->
							  	<div class="col-xs-12 bottomPadding noPadding">
							  		<div class="profileBlock">
							  			<a class="panelRedTitle" href="{{ url($getads_info->usertype_id == 1 ? 'vendor' : 'company') . '/' . $getads_info->alias }}">
				                      <div class="profileImage">
				                        <img src="{{ URL::route('uploads', array(), false).'/'.$getads_info->user_photo}}" class="avatar" alt="user profile image" style="width: 85px; height: 85px;">
				                      </div>
				                 		 </a>
				                      <div class="profileText">
							  			<a class="panelRedTitle" href="{{ url($getads_info->usertype_id == 1 ? 'vendor' : 'company') . '/' . $getads_info->alias }}">
				                        <div class="mediumText grayText"><strong>{{ $getads_info->name }}</strong></div></a>
				                   	 @if ($getads_info->ads_type_id != 2)
{{-- 							  			<div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->email }}"><i class="fa fa-envelope"></i> {{ $getads_info->email }}</div> --}}
					  	         		@if ($getads_info->telephone)
							  			    <div class="normalText grayText {{ $getads_info->telephone == '' ?  'hide':'' }}" data-placement="left" data-toggle="tooltip" title="{{ $getads_info->telephone }} "><i class="fa fa-phone"></i> {{ $getads_info->telephone }}</div>
							  			@else
							  			    <div class="normalText grayText {{ $getads_info->mobile == '' ?  'hide':'' }}" data-placement="left" data-toggle="tooltip" title="{{ $getads_info->mobile }}"><i class="fa fa-phone"></i> {{ $getads_info->mobile }}</div>
							  			@endif
							  		@else 

												{{-- <div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->email }}"><i class="fa fa-envelope"></i> {{ $getads_info->email }}</div> --}}
							  	         		@if ($getads_info->telephone)
								  			    <div class="normalText grayText {{ $getads_info->telephone == '' ?  'hide':'' }}" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->telephone}}"><i class="fa fa-phone"></i> {{ $getads_info->telephone }}</div>
								  				@else 
								  			    <div class="normalText grayText {{ $getads_info->mobile == '' ?  'hide':'' }}" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->mobile }}"><i class="fa fa-phone"></i> {{ $getads_info->mobile }}</div>
								  				@endif

							  		@endif
					  	         	    @if ($getads_info->city || $getads_info->countryName)
							  			  <div class="normalText grayText" data-placement="left"  data-toggle="tooltip" title="{{ $getads_info->city }}, {{ $getads_info->countryName }}"><i class="fa fa-map-marker"></i> {{ $getads_info->city }}
							  			@if ($getads_info->city) 
							  			   , 
							  			@endif
							  				{{ $getads_info->countryName }}</div>
							  			@endif
				                      </div>
				                    </div>
							  	</div>
							  	@if($rows->description == "")
							  	@else
							  	<div class="col-md-12 col-sm-12 col-xs-12 noPadding normalText topPaddingB description_remove">
							  		<article class="bottomMarginB grayText">
									{{ $getads_info->user_description }}
							  <!-- 		<?php $rows->description;?> -->
							  		</article>
							    </div>
							    @endif

<!-- 							  </div>
							</div> -->
				      
						  </div>
						</div>
				<div class="panel panel-default bglightGray removeBorder borderZero noMargin {{ $getads_info->website == "" && $getads_info->facebook_account == "" && $getads_info->twitter_account == ""? 'hide' : ''}}">
				  {{-- <div class="panel-body minPaddingB ellipsis">		
							  <div class="lineHeightA">
					  				<div class="normalText grayText ellipsis {{ $getads_info->website == "" ? 'hide' : ''}}"><i class="fa fa-globe"></i><span class="leftPadding">{{$getads_info->website}}</span></div>
					  				<div class="normalText grayText ellipsis {{ $getads_info->facebook_account == ""  ? 'hide': '' }}"><span class="blueText"><i class="fa fa-facebook-square"></i></span><span class="leftPadding">{{ $getads_info->facebook_account }}</span></div>
					  				<div class="normalText grayText ellipsis {{ $getads_info->twitter_account == "" ? 'hide': ''}}"><span class="blueText"><i class="fa fa-twitter"></i></span><span class="leftPadding">{{ $getads_info->twitter_account}}</span></div>
							  </div>

						
				 	
				  </div> --}}
				</div>

<div class="panel panel-default bglightGray removeBorder borderZero bottomMarginLight">
	<div class="panel-body noPadding">
		<div class="">
		@if(Auth::check())
		@if(!(Auth::user()->id == $getads_info->user_id))
		<div class="col-md-12 noPadding">
			<button data-target = "#modal-send-msg" data-toggle="modal" class="btn fullSize mediumText blueButton borderZero ellipsis">
				<i class="fa fa-paper-plane"></i> 
				Message {{$getads_info->name }}
				{{ $getads_info->online_status == "1" ? '(online)':'(offline)' }}
			</button>
		</div>				  
		@else
<!-- 		<div class="col-md-12 noPadding">
			<p class="alert alert-info borderZero">It's your own listing, you can't contact the publisher.</p>
		</div> -->
		@endif
		@else
		<div class="col-md-12 noPadding">
			<button  data-target = "#modal-login" data-toggle="modal" class="btn fullSize mediumText blueButton borderZero ellipsis">
				<i class="fa fa-paper-plane"></i>
				 Message {{$getads_info->name }}
				 {{ $getads_info->online_status == "1" ? '(online)':'(offline)' }}
			</button>
				</button>
			</div>				  
		@endif
		</div>
	</div>
</div>
</div>
<span class="hidden-xs">
	<div class="panel panel-default removeBorder borderZero topMarginB bottommarginveryLightB {{$getads_info->ads_type_id == '1' ? 'hide':''}}">
		<div class="panel-body minPaddingB lineHeight">
			<strong><div class="norightPadding normalWeight xlargeText redText text-center" data-countdown="{{$getads_info->ad_expiration}}"></div></strong>
			<div class="minbid normalText grayText text-center ellipsis"></div>
		</div>
	</div>
	<div class="panel panel-default removeBorder noMargin borderZero {{$getads_info->ads_type_id == '1' ? 'hide':''}}">
		@if($get_all_bidders->isEmpty())
        <div class="panel-body minPaddingB lineHeight currentBid hide">
        @else
        <div class="panel-body minPaddingB lineHeight currentBid">
        @endif
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
			@if($get_all_bidders->isEmpty())
	    	@else
        		 <span class="mediumText grayText text-uppercase"><strong>Current Bids</strong></span>
        	@endif
        	</div>
        	@foreach($get_all_bidders as $currentbids)
           <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 bottomPaddingB noPadding">
            <div class="commentBlock">
              <div class="commenterImage">
                <a class="ellipsis" href="{{url('/').'/'.$currentbids->alias}}"><img src="{{URL::route('uploads', array(), false).'/'.$currentbids->photo}}" class="avatar" alt="user profile image" style="width: 40px; height: 40px;"></a>
              </div>
              <div class="statisticcommentText">
                <div class="panelTitleB bottomPadding"><a style="color:#5da4ec;" href="{{url('/').'/'.$currentbids->alias}}">{{$currentbids->username}}</a><span class="smallText normalWeight topPadding lightgrayText pull-right ellipsis">{{ date("m/j/Y", strtotime($currentbids->created_at)) }}</span></div>
                <p style="margin-left:45px !important;" class="normalText ellipsis">Placed {{number_format($currentbids->bid, 2, '.', ',')}} USD</p>
              </div>
            </div>
          </div>
          @endforeach
        </div>
      </div>
  </span>
  <span class="hidden-xs">
      <div class="panel panel-default bglightGray removeBorder borderZero bottomMarginB {{$getads_info->ads_type_id == '1' ? 'hide':''}}">
	<div class="panel-body noPadding">
		<div class="">
		@if (Auth::check())
		@if (Auth::user()->id != $getads_info->user_id)
			@if($getads_info->status == 3)
		<button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-trophy"></i> You won the bidding</button>
			@else
		<button class="btn fullSize mediumText redButton borderZero btn-add-biding" {{Auth::user()->status == 2 ||Auth::user()->status == 3 ? 'disabled':''}} data-id="{{$getads_info->id}}" data-title="{{$getads_info->title}}"><i class="fa fa-gavel"></i> Place Bid</button>
			@endif
		@endif
		@else
			@if($getads_info->status == 3)
		<button class="btn fullSize mediumText redButton borderZero"><i class="fa fa-gavel"></i> Auction Ended</button>
			@else
		<button class="btn fullSize mediumText redButton borderZero" class="btn-login" data-toggle="modal" data-target="#modal-login"><i class="fa fa-gavel"></i> Place Bid</button>
			@endif
		@endif
		</div>
	</div>
</div>
</span>
      <div class="panel panel-default removeBorder bottomMarginB borderZero">
          <div class="panel-heading panelTitleBarLightB"> 
            <span class="panelRedTitle">ADVERTISEMENT</span>
          </div>
        <div class="panel-body noPadding">
        	<!-- <img class="fill" src="" style="background-repeat: no-repeat; background-size: cover; height: 120px; width: 100%;"> -->
{{--           <div class="fill" style="background: url({{ url('uploads/banner/advertisement/desktop').'/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg':$banner_placement_right->image}}); background-position: center center; background-repeat: no-repeat; background-size: initial; height: 390px; width: 100%;"> --}}
          <img src="{{URL::route('uploads', array(), false).'/banner/advertisement/desktop/'}}{{$banner_placement_right_status == 0 ? 'default_right.jpg': $banner_placement_right->image}}" style="height:390px; width:100%;"> 
          </div>
       </div>

      <div class="visible-xs">
 <div class="container-fluid bgWhite borderBottom bottomMarginLight ">
	<div class="container topPaddingD blockBottom">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 noPadding sideBlock">

		@if(Auth::check())
		@if(Auth::user()->status == 2)
		<div class="alert alert-danger text-center" style="border: -1px solid #ffcccc; border-radius: 0; background-color: #fcf3f3; margin-left: 15px; margin-right: 15px;">
	      <span id="resend_email" class="grayText" style="font-size:14px;">You must verify your account to be able to use this feature. <a id="resend_mail" class="grayText" href="#"><u>Resend verification.</u></a></span>
	    </div>
	    @endif
	    @endif
	    <div id="has_pending">
	    </div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panelTitle">Comments and reviews</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomPadding">
			<div class="panel panel-default borderZero removeBorder noMargin" id="comment-pane-mobile">
		 <div class="panel-body topPadding nobottomPadding" id="post-comment-pane">
					<?php echo $form;?>
				</div>
	   		</div>
   		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topBorder noPadding comments-container">
   				@if($comments == "")
   				<h4 class='text-center lightgrayText normalText borderBottom noComments'><span class='topPaddingB bottomPyyaddingB'>No current comments and reviews</span></h4>
   				@else
   				<?php echo $comments; ?>
   				@endif
	   </div>
  </div>
</div>
 </div>
</div>
</div>

 <div class="visible-xs bottomMarginB">
      <div class="panel panel-default removeBorder bottomMarginB borderZero">
      </div>
         <div class="panel-heading panelTitleBarLightB borderZero ellipsis {{count($vendor_ads) == 0 ? 'hide':''}}">
          <span class="panelRedTitle ellipsis">ADS BY <a class="panelRedTitle ellipsis" href="{{ url('/') . '/' . $getads_info->alias .'/'.$getads_info->name }}">{{ $getads_info->name}}</a></span>
        </div>
            @foreach ($vendor_ads as $vendor_ad)
              <div class="panel panel-default bottomMarginLight  removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{url('/').'/'.$vendor_ad->country_code.'/'.$vendor_ad->ad_type_slug.'/'.$vendor_ad->parent_category_slug.'/'.$vendor_ad->category_slug.'/'.$vendor_ad->slug}}">
              		<div class="adImageB" style="background: url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$vendor_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div>
          		</a>
	          	<div class="rightPadding topPadding nobottomPadding lineHeight">
	          	<div class="mediumText noPadding bottomPadding ellipsis cursor">
	          		<a class="grayText" href="{{url('/').'/'.$vendor_ad->country_code.'/'.$vendor_ad->ad_type_slug.'/'.$vendor_ad->parent_category_slug.'/'.$vendor_ad->category_slug.'/'.$vendor_ad->slug}}">{{ $vendor_ad->title }}</a>
	          	</div>
					<div class="normalText bottomPadding bottomMargin ellipsis">
	                        <a class="normalText lightgrayText" href="{{ url($vendor_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $vendor_ad->alias }}">by {{$vendor_ad->name}}</a>
	                  </div>
	                  <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($vendor_ad->price) }} USD</strong></div>
	                        <div class="normalText bottomPadding ellipsis">
	                         @if($vendor_ad->city || $vendor_ad->countryName)
	                          <i class="fa fa-map-marker rightMargin"></i>
	                          {{ $vendor_ad->city }} @if ($vendor_ad->city),@endif {{ $vendor_ad->countryName }}
	                         @else
	                          <i class="fa fa-map-marker rightMargin"></i> not available
	                         @endif
	                         </div>

	                          <?php $i=0;?>
                               @if(count($vendor_ad->getAdvertisementComments)>0)
                                  @foreach($vendor_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
	                  <div class="nobottomPadding minPadding adcaptionMargin">
	                  	
	                  	 @if($vendor_ad->average_rate != null)
	                       <span id="vendor_ads_{{$vendor_ad->id}}" class="blueText pull-left noPadding"></span>
	                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
	                     @else
	                       <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
	                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
	                     @endif	
	                  </div>
	        	</div>
              </div>
             </div>
             @endforeach
	</div>	
<span class="{{count($featured_ads_per) == 0 ? 'hide' : ''}} {{$getads_info->ads_type_id == 2 ? 'hide' : ''}}">
 <div class="panel panel-default removeBorder bottomMarginB borderZero">
      </div>
      <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">FEATURED ADS</span>
        </div>
        @if($featured_ads_per->isEmpty())
        <p class="text-center">No available featured ads for your country</p>
        @endif
            @foreach ($featured_ads_per as $featured_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding ellipsis">
                <a href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}"> 
                  {{-- <div class="adImageB" style="background: url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImageB">
                  <img class="adImageB" alt="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 of '.$featured_ad->parent_category_slug.' '.$featured_ad->title}}" title="{{$featured_ad->ad_type_slug.' '.$featured_ad->photo.' 1 '. $featured_ad->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_ad->photo}}" style="object-fit: cover; overflow: hidden;" />
                </div>

              </a>

                  



              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_ad->country_code.'/'.$featured_ad->ad_type_slug.'/'.$featured_ad->parent_category_slug.'/'.$featured_ad->category_slug.'/'.$featured_ad->slug}}">{{ $featured_ad->title }}</a>
                    <div class="normalText bottomPadding bottomMargin ellipsis">
                          <a class="normalText lightgrayText" href="{{ url($featured_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_ad->alias }}">by {{$featured_ad->name}}</a>
                    </div>
                    <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($featured_ad->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_ad->city || $featured_ad->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_ad->city }} @if ($featured_ad->city),@endif {{ $featured_ad->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>

                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
            </div>
              </div>
             </div>
             @endforeach
             </span>

@if($featured_auctions_global->isEmpty() or $getads_info->ads_type_id == 1)
             @else
             <div class="panel-heading panelTitleBarLightB">
                <span class="panelRedTitle">FEATURED AUCTIONS</span>
            </div>
            @foreach ($featured_auctions_global as $featured_auction)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding ellipsis">
                <a href="{{url('/').'/'.$featured_auction->country_code.'/'.$featured_auction->ad_type_slug.'/'.$featured_auction->parent_category_slug.'/'.$featured_auction->category_slug.'/'.$featured_auction->slug}}"> 
                  {{-- <div class="adImageB" style="background: url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_auction->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImageB">
                  <img class="adImageB" alt="{{$featured_auction->ad_type_slug.' '.$featured_auction->photo.' 1 of '.$featured_auction->parent_category_slug.' '.$featured_auction->title}}" title="{{$featured_auction->ad_type_slug.' '.$featured_auction->photo.' 1 '. $featured_auction->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$featured_auction->photo}}" style="object-fit: cover; overflow: hidden;" />
                </div>

              </a>
              <div class="rightPadding topPadding nobottomPadding lineHeight">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$featured_auction->country_code.'/'.$featured_auction->ad_type_slug.'/'.$featured_auction->parent_category_slug.'/'.$featured_auction->category_slug.'/'.$featured_auction->slug}}">{{ $featured_auction->title }}</a>
                    <div class="normalText bottomMargin ellipsis">
                          <a class="normalText lightgrayText" href="{{ url($featured_auction->usertype_id == 1 ? 'vendor' : 'company') . '/' . $featured_auction->alias }}">by {{$featured_auction->name}}</a>
                    </div>
                    <div class="normalText redText" id="count_me_down" title="" data-countdown="{{$featured_auction->ad_expiration}}"></div>
                    <div class="mediumText blueText"><strong>{{ number_format($featured_auction->price) }} USD</strong></div>
                          <div class="normalText bottomPadding ellipsis">
                           @if($featured_auction->city || $featured_auction->countryName)
                            <i class="fa fa-map-marker rightMargin"></i>
                            {{ $featured_auction->city }} @if ($featured_auction->city),@endif {{ $featured_auction->countryName }}
                           @else
                            <i class="fa fa-map-marker rightMargin"></i> not available
                           @endif
                           </div>
                  @if(isset($featured_ad))
                            <?php $i=0;?>
                               @if(count($featured_ad->getAdvertisementComments)>0)
                                  @foreach($featured_ad->getAdvertisementComments as $count_featured)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
                    <div class="minPadding adcaptionMargin">
                       @if($featured_ad->average_rate != null)
                         <span id="featured_ads_{{$featured_ad->id}}" class="blueText pull-left noPadding"></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @else
                         <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                         <span class="normalText lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                       @endif 
                    </div>
                    @endif
            </div>
              </div>
             </div>
             @endforeach
             @endif

             <div class="visible-xs">
             <div class="panel panel-default bottomMarginLight removeBorder borderZero">
			      <div class="panel-heading panelTitleBarLightB">
          <span class="panelRedTitle">Related Ads</span>  <span class="hidden-xs">| Check out other ads with the same criteria.</span>
			      </div> 
	<div class="col-xs-12 col-sm-12 visible-xs visible-sm noPadding blockBottom">
	@foreach($related_ads_mobile as $row)
		<div class="panel panel-default bottomMarginLight removeBorder borderZero">
			<div class="panel-body noPadding">
			<a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
{{-- 			<div class="adImageB" style="background:  url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
			</div> --}}

			<div style="overflow: hidden;" class="adImageB">
              <img class="adImageB" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" />
            </div>


			</a>
				<div class="rightPadding nobottomPadding lineHeight">
				<div class="mediumText grayText topPadding bottomPadding"><a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}"> {{ $row->title }}</a></div>
				<div class="normalText bottomPadding ellipsis"><a class="normalText lightgrayText">by {{ $row->name }}</a></div>
				<div class="bottomPadding mediumText blueText"><strong> {{ $row->price }} USD</strong></div>
					<div class="normalText bottomPadding">
						@if($row->city || $row->countryName)
						<i class="fa fa-map-marker rightMargin"></i>
						{{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
						@else
						<i class="fa fa-map-marker rightMargin"></i> not available
						@endif
					</div>
				<?php $i=0;?>
				@if(count($row->getAdvertisementComments)>0)
				@foreach($row->getAdvertisementComments as $count)
				<?php $i++;?>     
				@endforeach
				@endif
					<div class="nobottomPadding minPadding adcaptionMargin topPaddingB">
						@if($row->average_rate != null)
						<span id="related_ads_mobile_{{$row->id}}" class="blueText pull-left rightMargin"></span>
						<span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
						@else
						<span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
						<span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
						@endif	
					</div>
				</div>
			</div>
		</div>
	@endforeach
	</div>
</div>
</div>

       <div class="hidden-xs bottomMarginB">
      <div class="panel panel-default removeBorder bottomMarginB borderZero">
      </div>
         <div class="panel-heading panelTitleBarLightB borderZero ellipsis {{count($vendor_ads) == 0 ? 'hide':''}}">
          <span class="panelRedTitle ellipsis">ADS BY <a class="panelRedTitle ellipsis redText" href="{{ url($getads_info->usertype_id == 1 ? 'vendor' : 'company').'/'.$getads_info->alias}}">{{ $getads_info->name}}</a></span>
        </div>
            @foreach ($vendor_ads as $vendor_ad)
              <div class="panel panel-default bottomMarginLight removeBorder borderZero">
              <div class="panel-body noPadding">
              	<a href="{{url('/').'/'.$vendor_ad->country_code.'/'.$vendor_ad->ad_type_slug.'/'.$vendor_ad->parent_category_slug.'/'.$vendor_ad->category_slug.'/'.$vendor_ad->slug}}"> 
              		{{-- <div class="adImageB" style="background: url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$vendor_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          			</div> --}}
          			<div style="overflow: hidden;" class="adImageB">
		              <img class="adImageB" alt="{{$vendor_ad->ad_type_slug.' '.$vendor_ad->photo.' 1 of '.$vendor_ad->parent_category_slug.' '.$vendor_ad->title}}" title="{{$vendor_ad->ad_type_slug.' '.$vendor_ad->photo.' 1 '. $vendor_ad->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$vendor_ad->photo}}" style="object-fit: cover; overflow: hidden;" />
		            </div>

          		</a>
	          	<div class="rightPadding topPadding nobottomPadding lineHeight">
	          	<div class="mediumText noPadding bottomPadding ellipsis cursor">
	          		<a class="grayText" href="{{url('/').'/'.$vendor_ad->country_code.'/'.$vendor_ad->ad_type_slug.'/'.$vendor_ad->parent_category_slug.'/'.$vendor_ad->category_slug.'/'.$vendor_ad->slug}}">{{ $vendor_ad->title }}</a>
	          	</div>
					<div class="normalText bottomPadding bottomMargin ellipsis">
	                        <a class="normalText lightgrayText" href="{{ url($vendor_ad->usertype_id == 1 ? 'vendor' : 'company') . '/' . $vendor_ad->alias }}">by {{$vendor_ad->name}}</a>
	                  </div>
	                  <div class="mediumText topPadding bottomPadding blueText"><strong>{{ number_format($vendor_ad->price) }} USD</strong></div>
	                        <div class="normalText bottomPadding ellipsis">
	                         @if($vendor_ad->city || $vendor_ad->countryName)
	                          <i class="fa fa-map-marker rightMargin"></i>
	                          {{ $vendor_ad->city }} @if ($vendor_ad->city),@endif {{ $vendor_ad->countryName }}
	                         @else
	                          <i class="fa fa-map-marker rightMargin"></i> not available
	                         @endif
	                         </div>

	                          <?php $i=0;?>
                               @if(count($vendor_ad->getAdvertisementComments)>0)
                                  @foreach($vendor_ad->getAdvertisementComments as $count)
                                        <?php $i++;?>     
                                   @endforeach
                               @endif
	                  <div class="nobottomPadding minPadding adcaptionMargin">
	                  	 @if($vendor_ad->average_rate != null)
	                       <span id="vendor_ads_{{$vendor_ad->id}}" class="blueText pull-left noPadding"></span>
	                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
	                     @else
	                       <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
	                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
	                     @endif	
	                  </div>
	        	</div>
              </div>
             </div>
             @endforeach
	</div>	
	</div>		
</div>
</div>
 </div>
 </div>

<div class="hidden-xs">
 <div class="container-fluid bgWhite borderBottom">
	<div class="container topPaddingD blockBottom">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12 noPadding sideBlock">
		@if(Auth::check())
		@if(Auth::user()->status == 2)
		<div class="alert alert-danger text-center" style="border: -1px solid #ffcccc; border-radius: 0; background-color: #fcf3f3; margin-left: 15px; margin-right: 15px;">
	      <span id="resend_email" class="grayText" style="font-size:14px;">You must verify your account to be able to use this feature</span>
	    </div>
	    @endif
	    @endif
	    <div id="has_pending">
	    </div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panelTitle">Comments and reviews</div>
			</div>
			<div id="comment-notice" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center bottomPaddingB topPaddingB hide" style="background-color:#fcf3f3;size:14px;">Comment is required</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding bottomPadding">
			<div class="panel panel-default borderZero removeBorder noMargin" id="comment-pane">
		 <div class="panel-body topPadding nobottomPadding" id="post-comment-pane">

					<?php echo $form;?>
				</div>
	   		</div>
   		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 topBorder noPadding comments-container">
   				@if($comments == "")
   				<h4 class='text-center lightgrayText normalText noComments'><span class='topPaddingB bottomPyyaddingB'>No current comments and reviews</span></h4>
   				@else
   				<?php echo $comments; ?>
   				@endif
	   </div>
  </div>
</div>
 </div>
</div>
</div>
<div class="hidden-xs">
			<div class="container-fluid bgGray">
				<div class="container blockTop">
					<div class="col-md-12 col-sm-12 col-xs-12 noPadding sideBlock">
			 		<div class="panel panel-default  removeBorder borderZero">
			      <div class="panel-heading panelTitleBarLight">
			        <span class="panelTitle">Related Ads</span>  <span class="hidden-xs">| Check out other ads with the same criteria.</span>
			      </div> 
			      	
			      		<div class="panel-body">
			        <div id="relatedAds" class="carousel slide carousel-fade" data-ride="carousel">
			          <!-- Wrapper for slides -->
			          <div class="carousel-inner" role="listbox">
			            <div class="item active">
			            <div class="col-lg-12 col-md-12 col-sm-12">
			           @foreach ($related_ads  as $related_ad)
			              <div class="col-lg-3 col-md-3 col-sm-3 noPadding lineHeight">
			                <div class="sideMargin borderBottom">
			                <span class="label label-featured {{$related_ad->feature == 0 ? 'hide':''}}">FEATURED</span>
			                  <a href="{{url('/').'/'.$related_ad->country_code.'/'.$related_ad->ad_type_slug.'/'.$related_ad->parent_category_slug.'/'.$related_ad->category_slug.'/'.$related_ad->slug}}">
			                  	{{-- <div class="adImage" style="background:  url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$related_ad->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
			                		</div> --}}

			                	<div style="overflow: hidden;" class="adImage">
					              <img class="adImage" alt="{{$related_ad->ad_type_slug.' '.$related_ad->photo.' 1 of '.$related_ad->parent_category_slug.' '.$related_ad->title}}" title="{{$related_ad->ad_type_slug.' '.$related_ad->photo.' 1 '. $related_ad->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$related_ad->photo}}" style="object-fit: cover; overflow: hidden;" />
					            </div>

			                	</a>
			                  <div class="minPadding nobottomPadding">
			                  	<div class="row">
			                      <div class="col-sm-12 ellipsis cursor">
			                          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$related_ad->country_code.'/'.$related_ad->ad_type_slug.'/'.$related_ad->parent_category_slug.'/'.$related_ad->category_slug.'/'.$related_ad->slug}}">{{ $related_ad->title }}</a>
			                        </div>
			                      </div>
<!-- 			                    <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{ url('view') . '/' . $related_ad->id }}">{{ $related_ad->title }}</a> -->
			                    <div class="normalText grayText bottomPadding">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                     <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
			                    <!-- <div class="normalText bottomPadding">
			                         @if($related_ad->city || $related_ad->countryName)
			                          <i class="fa fa-map-marker rightMargin"></i>
			                          {{ $related_ad->city }} @if ($related_ad->city),@endif {{ $related_ad->countryName }}
			                         @else
			                          <i class="fa fa-map-marker rightMargin"></i> not available
			                         @endif
			                       <span class="pull-right ">
			                        <span class="blueText rightMargin">
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star-half-empty"></i>
			                        </span>
			                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
			                      </span>
			                    </div> -->
			                      <div class="normalText bottomPaddingB">
                      <div class="row">
                        <div class="col-sm-12 col-md-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                        <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-12 col-md-6">
                      <span class="pull-right hidden-sm">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                             <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                      </span>
                      <span class="visible-sm topPadding">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                             <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                      </span>
                    </div>
                  </div>
                    </div>
				                         <?php $i=0;?>
				                               @if(count($related_ad->getAdvertisementComments)>0)
				                                  @foreach($related_ad->getAdvertisementComments as $count)
				                                        <?php $i++;?>     
				                                   @endforeach
				                               @endif
			                  </div>
			                </div>
			              </div>
			              @endforeach
			             </div>
			            </div>

			            @if(count($related_adset2) != "0")
			            <div class="item">
			            <div class="col-lg-12 col-md-12">
			           @foreach ($related_adset2  as $row)
			              <div class="col-lg-3 col-md-3 noPadding lineHeight">
			                <div class="sideMargin borderBottom">
			                  <span class="label label-featured {{$row->feature == 0 ? 'hide':''}}">FEATURED</span>
			                  <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
			                  	{{-- <div class="adImage" style="background:  url({{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
			                		</div> --}}

			                	<div style="overflow: hidden;" class="adImage">
					              <img class="adImage" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" />
					            </div>

			                	</a>
			                  <div class="minPadding nobottomPadding">
			                    <div class="row">
			                      <div class="col-sm-12 ellipsis cursor">
			                          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">{{ $related_ad->title }}</a>
			                        </div>
			                      </div>
			                    <div class="normalText grayText bottomPadding">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                     <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
			                    <!-- <div class="normalText bottomPadding">
			                         @if($row->city || $row->countryName)
			                          <i class="fa fa-map-marker rightMargin"></i>
			                          {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
			                         @else
			                          <i class="fa fa-map-marker rightMargin"></i> not available
			                         @endif
			                       <span class="pull-right ">
			                        <span class="blueText rightMargin">
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star"></i>
			                        <i class="fa fa-star-half-empty"></i>
			                        </span>
			                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span>
			                      </span>
			                    </div> -->
			                    <div class="normalText bottomPaddingB">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                        <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-6">
                      <span class="pull-right">
                          @if($row->average_rate != null)
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="lightgrayText rightMargin"><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span><span class="glyphicon glyphicon-star" aria-hidden="true"></span></span>
                             <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif 
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                    </div>
				                         <?php $i=0;?>
				                               @if(count($row->getAdvertisementComments)>0)
				                                  @foreach($row->getAdvertisementComments as $count)
				                                        <?php $i++;?>     
				                                   @endforeach
				                               @endif
				                  <!-- <div class="minPadding adcaptionMargin">
				                  	 @if($row->average_rate != null)
				                       <span id="related_adset2_{{$row->id}}" class="blueText pull-left rightMargin"></span>
				                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
				                     @else
				                       <span class="blueText rightMargin">No Rating</span>
				                       <span class="lightgrayText pull-right normalText">{{$i}} <i class="fa fa-comment"></i></span>
				                     @endif	
				                  </div> -->
			                  </div>
			                </div>
			              </div>
			              @endforeach
			             </div>
			            </div>
			            @endif
			          </div>
			          <!-- Left and right controls -->
			          <a class="left carousel-control" href="#relatedAds" role="button" data-slide="prev" style="width: 10px; background:none; text-align:left; color:#7eacda; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:90px; left:0px;">
			            <span class="fa fa-angle-left" aria-hidden="true"></span>
			            <span class="sr-only">Previous</span>
			          </a>
			          <a class="right carousel-control" href="#relatedAds" role="button" data-slide="next" style="width: 10px; background:none; text-align:right; color:#7eacda; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:90px; right:0px;">
			            <span class="fa fa-angle-right" aria-hidden="true"></span>
			            <span class="sr-only">Next</span>
			          </a>
			          
			        </div>
			    </div>
			      </div>
			    </div>
				</div>
</div>
</div>
<?php echo $modal_related_popups;?>
       <div class="popup-box chat-popup" id="qnimate">
          <div class="popup-head">
        <div class="popup-head-left pull-left"> Send A Message </div>
            <div class="popup-head-right pull-right">
<!--             <div class="btn-group">
                      <button class="chat-header-button" data-toggle="dropdown" type="button" aria-expanded="false">
                     <i class="glyphicon glyphicon-cog"></i> </button>
                    <ul role="menu" class="dropdown-menu pull-right">
                    <li><a href="#">Media</a></li>
                    <li><a href="#">Block</a></li>
                    <li><a href="#">Clear Chat</a></li>
                    <li><a href="#">Email Chat</a></li>
                    </ul>
            </div> -->
            
            <button data-widget="remove" id="removeClass" class="chat-header-button pull-right" type="button"><i class="glyphicon glyphicon-remove"></i></button>
                      </div>
        </div>
      <div class="minPadding">
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <input class="topPadding fullSize" type="text" name="title" id="row-title" >
      <input class="topPadding" type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
      <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message"></textarea>
<!--       <div class="btn-footer">
      <button class="bg_none"><i class="glyphicon glyphicon-film"></i> </button>
      <button class="bg_none"><i class="glyphicon glyphicon-camera"></i> </button>
            <button class="bg_none"><i class="glyphicon glyphicon-paperclip"></i> </button>-->
<!--       <button class="bg_none pull-right"><i class="glyphicon glyphicon-send"></i> </button> -->
          <div class="clearfix bottomPadding">
        <div class="popup-messages-footer">
        <button id="" class="btn normalText redButton borderZero pull-right"><i class="fa fa-paper-plane"></i> Submit</button>
      </span>
        {!! Form::close() !!}
      </div>
    </div>
      </div>
    </div>
  <!--  <div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-message-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle"><i class="fa fa-plane"></i>Send Message</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<input class="fullSize" type="text" name="title" id="row-title" value="{{$getads_info->title.'(Inquiry)'}}">
				<textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
			</div>
			<div class="modal-footer">
				<input  type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
				<button type="submit" class="btn btn-submit borderZero btn-success"><i class="fa fa-plane"></i> Send</button>
				<button type="button" class="btn redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> -->
<div class="modal fade" id="modal-send-msg" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content borderZero">
        <div id="load-message-form" class="loading-pane hide">
          <div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
        </div>
      {!! Form::open(array('url' => 'user/send/message/vendor', 'role' => 'form', 'class' => 'form-horizontal', 'id' => 'modal-send-message', 'files' => 'true')) !!}
      <div class="modal-header modalTitleBar">
          <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
            <h4 class="modal-title panelTitle"><i class="fa fa-plane rightPadding"></i>Send Message</h4>
          </div>
      <div class="modal-body">
        <div id="form-notice"></div>
        <!-- <input class="fullSize" type="text" name="title" id="row-title" value="Inquiry ({{$rows->name}})">
        <textarea class="topMargin fullSize" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea> -->
  <div class="grayText">
    <label for="email">Title:</label>
    <input class="form-control borderZero inputBox" type="text" name="title" id="row-title" value="Inquiry ({{$getads_info->title}})">
  </div>
  <div class="grayText topPaddingB">
    <label for="pwd">Message:</label>
    <textarea class="form-control borderZero inputBox" id="status_message" placeholder="Type a message..." rows="10" cols="" name="message" required></textarea>
  </div>
      </div>
      <div class="modal-footer">
        <input  type="hidden" name="reciever_id" id="row-reciever_id" value="{{$getads_info->user_id}}">
        <button type="submit" class="btn blueButton borderZero"> Send</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>
</div>
</div>
<!-- </div> -->
<!-- <div class="modal fade" id="modal-ads-delete" tabindex="-1" role="dialog" aria-labelledBy="form-title" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content borderZero">
		    <div id="load-form" class="loading-pane hide">
    			<div><i class="fa fa-inverse fa-circle-o-notch fa-spin fa-3x centered"></i></div>
  			</div>
			{!! Form::open(array('url' => 'ads/remove', 'role' => 'form', 'class' => 'form-horizontal', 'style'=>'margin-bottom: 0', 'id' => 'modal-save_form', 'files' => true)) !!}
			<div class="modal-header modalTitleBar">
	        <button type="button" class="close closeButton" data-dismiss="modal">&times;</button>
	          <h4 class="modal-title panelTitle">Delete Ads</h4>
	        </div>
			<div class="modal-body">
				<div id="form-notice"></div>
				<div class="form-group">
				<div class="col-lg-12">
					Do you want to delete? 
				</div>
				</div>
			</div>
			<div class="modal-footer bordertopLight borderBottom">
				<input type="hidden" name="id" id="row-id" value="">
				<button type="submit" class="btn btn-submit rateButton borderZero" id="disableYes"><i class="fa fa-trash"></i> Yes</button>
				<button type="button" class="btn  redButton borderZero" data-dismiss="modal"><i class="fa fa-times"></i> No</button>
        	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div> -->
@include('ads.form')
@stop