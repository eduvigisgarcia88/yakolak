@extends('layout.frontend')
@section('scripts')
<script>
 $_token = "{{ csrf_token() }}";
$(function () {

  @foreach($adslatestbids as $row)
    $("#ads_latest_bids_{{$row->id}}").rateYo({
         starWidth: "12px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbids_panel2 as $row)
    $("#ads_latest_bids_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads as $row)
    $("#latest_ads_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestads_panel2 as $row)
    $("#latest_ads_panel2_{{$row->id}}").rateYo({
     starWidth: "12px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($latestadsmobile as $row)
    $("#latest_ads_mobile_{{$row->id}}").rateYo({
         starWidth: "14px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
  @foreach($adslatestbidsmobile as $row)
    $("#ads_latest_bids_mobile_{{$row->id}}").rateYo({
         starWidth: "14px",
         rating: {{ ($row->average_rate != null ? floor($row->average_rate):0) }},
         ratedFill: "#5da4ec",
         normalFill: "#cccccc",
         readOnly: true,
    });
  @endforeach
});

 $('[data-countdown]').each(function() {
   var $this = $(this), finalDate = $(this).data('countdown');
   var ads_id = $(this).data('ads_id');
   $this.countdown(finalDate, function(event) {
   $this.html(event.strftime('%DD-%HH-%MM-%SS'));
   });
 });


 // $('#flyout_menu').each(function() {
 //            $.get("{{ url('user/plan/expiring') }}", { _token: $_token}, function(response) {
 //                append(response);   
 //    }, 'json');

 // });
 //  $('#flyout_menu').each(function() {
 //            $.get("{{ url('user/plan/expired') }}", { _token: $_token}, function(response) {
 //                append(response);   
 //    }, 'json');

 // });
   @if(Auth::check())
            $("#latest-ads-wrapper").on("click", "#btn-user-view", function() {
                var ad_id = $(this).data("id"); 
                var user_id = "{{Auth::user()->id}}";
                var post_url = "{{ url('user/save/ad/view') }}";
                $.post(post_url, { user_id: user_id,ad_id:ad_id,_token: $_token}, function(response) { 
                }, 'json'); 
             });
    @endif

      
    $("#main-container").on("click", "#btn-view-more-bids", function() {
          var last_id = $("#latest-bids-container:last").children().last().data('id');
          var btn = $(this);
          btn.html("");
          btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
          var loading = $("#panel-bids").find(".loading-pane");
          // loading.removeClass("hide");
          var skip = $("#skip").val();
          var take = $("#take").val();
          var get_current_no_bids = $("#per_bids").val();
          var per = parseInt(get_current_no_bids) + 4;
          var no_of_results = $("#no_of_results").val();
          var container = $("#latest-bids-container");
          var mobile_container = $("#bids-mobile-container");
          var body = "";
          var mobile_body="";
          $.post("{{url('get-bids-pagination')}}", {per:per,_token: $_token}, function(response) {
                $.each(response.rows, function(index, value) {
                    console.log(response.rows);
                   var concat_country = value.city+' , '+value.countryName;
                    var len = concat_country.length;
                    if(len > 13){
                    var res = concat_country.substr(0,13);
                    var ellipsible_location = res+'...';
                    }else{
                      ellipsible_location = concat_country;
                    }
                    var len = value.name.length;
                    if(len > 13){
                    var res = value.name.substr(0,13);
                    var ellipsible_name = res+'...';
                    }else{
                      ellipsible_name= value.name;
                    }

                  body += '<div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="'+value.id+'">'+
                      '<div class="sideMargin borderBottom">'+
                      '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                        '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                      '</a>'+
                        '<div class="minPadding nobottomPadding">'+
                          '<div class="mediumText noPadding topPadding bottomPadding ellipsis cursor">'+
                                '<div class="visible-lg ellipsis">'+ value.title +
                               '</div>'+
                                '<div class="visible-md ellipsis">'+ value.title +
                                '</div>'+
                          '</div>'+
                          '<div class="normalText grayText bottomPadding bottomMargin ellipsis">'+ 
                           '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ellipsible_name+'</a>'+
                            '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' USD</strong>'+
                           '</span>'+
                          '</div>'+
                          '<div class="normalText bottomPadding">'+
                             '<i class="fa fa-map-marker rightMargin"></i> '+ellipsible_location+    
                            '<span class="pull-right">'+
                                   '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' +
                                   '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                            '</span>'+
                          '</div>'+
                        '</div>'+
                      '</div>'+
                      '<div class="sideMargin minPadding text-center nobottomPadding redText normalText bottomMarginC">'+       
                      '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.get_ads_id+'"></div>'+
                      '</div>'+
                      '</div>';

                   mobile_body +='<div class="panel panel-default removeBorder borderZero bottomMarginLight ">'+
                              '<div class="panel-body rightPaddingB noPadding">'+
                              '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                              '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                                '</a>'+
                              '<div class="rightPadding nobottomPadding lineHeight">'+
                               '<div class="mediumText topPadding noPadding bottomPadding">'+
                                '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                      '<div class="visible-sm">'+ value.title +
                                      '</div>'+
                                      '<div class="visible-xs">'+ value.title +
                                      '</div>'+
                                '</a>'+
                                '<div class="normalText redText bottomPadding ellipsis">'+
                                       '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+value.name+'</a>'+
                                '</div>'+
                                '</div>'+
                                '<div class="normalText redText bottomPadding">'+
                                '<div data-countdown="'+value.ad_expiration+'" data-currenttime="'+response.current_time+'" data-ads_id="'+value.get_ads_id+'"></div>'+
                                '</div>'+
                                '<div class="mediumText blueText bottomPadding">'+
                                  '<strong>'+(value.price).toLocaleString()+'USD</strong>'+
                                '</div>'+
                                '<div class="normalText bottomPadding">'+
                                  '<i class="fa fa-map-marker rightMargin"></i>'+ concat_country +
                                '</div>'+
                                '<div style="padding-top:8px;">'+
                                      '<span id="ads_latest_bids_mobile_'+value.id+'" class="pull-left blueText rightMargin noPadding"></span>' +
                                      '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                                '</div>'+
                              '</div>'+
                              '</div>'+
                            '</div>';

                    $("#no_of_results").val(response.no_of_results);

                  });
          
            $("#per_bids").val(per);

            var per_bids = $("#per_bids").val();
            var max_bids = $("#max_bids").val();
            var parse_per_bids = parseInt(per_bids);
            var parse_max_bids = parseInt(max_bids);
            if(parse_per_bids >= parse_max_bids){
              $("#btn-view-more-bids").remove();
            }

          
            container.html(body);
            mobile_container.html(mobile_body);
            btn.html("");
            btn.html("View More");
  
             $('[data-countdown]').each(function() {
               var $this = $(this), finalDate = $(this).data('countdown');
               var ads_id = $(this).data('ads_id');
               $this.countdown(finalDate, function(event) {
               $this.html(event.strftime('%DD-%HH-%MM-%SS'));
               });
             });
            $.each(response.rows, function(index, value) {      
               $("#ads_latest_bids_"+value.id).rateYo({
                   starWidth: "12px",
                   rating:(value.average_rate != null ? value.average_rate:0),
                   ratedFill: "#5da4ec",
                   normalFill: "#cccccc",
                   readOnly: true,
               });
            });
            $.each(response.rows, function(index, value) {      
               $("#ads_latest_bids_mobile_"+value.id).rateYo({
                   starWidth: "14px",
                   rating:(value.average_rate != null ? value.average_rate:0),
                   ratedFill: "#5da4ec",
                   normalFill: "#cccccc",
                   readOnly: true,
               });
            });

          }, 'json'); 
    });
   $("#main-container").on("click", "#btn-view-more-ads", function() {
            var last_id = $("#latest-ads-container:last").children().last().data('id');
            var loading = $("#panel-ads").find(".loading-pane");
            var get_current_no_ads= $("#per_ads").val();
            var per = parseInt(get_current_no_ads) + 4;
            // loading.removeClass("hide");
            var btn = $(this);
            btn.html("");
            btn.html("<i class='fa fa-spinner fa-spin fa-lg centered'></i>");
            var container = $("#latest-ads-container");
            var mobile_container = $("#ads-mobile-container");
            var body = "";
            var mobile_body ="";
            $.post("{{url('get-ads-pagination')}}", {per:per,_token: $_token}, function(response) {
                  
                  $.each(response.rows, function(index, value) {
                    var concat_country = value.city+' , '+value.countryName;
                    var len = concat_country.length;
                    if(len > 13){
                    var res = concat_country.substr(0,13);
                    var ellipsible_location = res+'...';
                    }else{
                      ellipsible_location = concat_country;
                    }
                    var len = value.name.length;
                    if(len > 13){
                    var res = value.name.substr(0,13);
                    var ellipsible_name = res+'...';
                    }else{
                      ellipsible_name= value.name;
                    }
                    console.log(response.rows);
                    // if(response.last_id == value.id){
                    //   $("#btn-view-more-ads").remove();
                    //   // $("#btn-view-more-ads").removeClass('blueButton ').addClass("btn-danger");
                    //   // $("#btn-view-more-ads").text("End of Record");

                    // }else{
                          
                    body += '<div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="'+value.id+'">'+
                        '<div class="sideMargin bottomMarginC borderBottom">'+
                        '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                          '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'" target="_blank"><div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                        '</a>'+
                          '<div class="minPadding nobottomPadding">'+
                            '<div class="mediumText noPadding topPadding bottomPadding ellipsis cursor">'+
                                  '<div class="visible-lg ellipsis">'+ value.title +
                                 '</div>'+
                                  '<div class="visible-md ellipsis">'+ value.title +
                                  '</div>'+
                            '</div>'+
                            '<div class="normalText grayText bottomPadding bottomMargin ellipsis">'+ 
                             '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+ ellipsible_name +'</a>'+
                              '<span class="pull-right"><strong class="blueText"> '+(value.price).toLocaleString()+' USD</strong>'+
                             '</span>'+
                            '</div>'+
                            '<div class="normalText bottomPadding">'+
                               '<i class="fa fa-map-marker rightMargin"></i> '+ellipsible_location+
                              '<span class="pull-right">'+
                                     '<span id="ads_latest_bids_'+value.id+'" class="pull-left blueText rightMargin"></span>' +
                                     '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                              '</span>'+
                            '</div>'+
                          '</div>'+
                        '</div>'+
                        '</div>';


                    mobile_body +='<div class="panel panel-default removeBorder borderZero bottomMarginLight ">'+
                              '<div class="panel-body rightPaddingB noPadding">'+
                              '<span class="label label-featured '+(value.feature == 1 ? '':'hide')+'">FEATURED</span>'+
                              '<a href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'"> <div style="overflow: hidden;" class="adImage"> <img alt="'+value.ad_type_slug+' '+value.photo+' 1 of '+value.parent_category_slug+' '+value.title+'" class="adImage" title="'+value.ad_type_slug+' '+value.photo+' 1 '+value.title+'" src="{{URL::route('uploads', array(), false).'/ads/thumbnail'.'/'}}'+value.photo+'" style="object-fit: cover; overflow: hidden;" /> </div>'+
                                '</a>'+
                              '<div class="rightPadding nobottomPadding lineHeight">'+
                               '<div class="mediumText topPadding noPadding bottomPadding">'+
                                '<a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url()}}/'+value.country_code+'/'+value.ad_type_slug+'/'+value.parent_category_slug+'/'+value.category_slug+'/'+value.slug+'">'+
                                      '<div class="visible-sm">'+ value.title +
                                      '</div>'+
                                      '<div class="visible-xs">'+ value.title +
                                      '</div>'+
                                '</a>'+
                                '<div class="normalText redText bottomPadding ellipsis">'+
                                       '<a class="normalText lightgrayText" href="{{ url('/').'/'}}'+(value.usertype_id == 1 ? 'vendor/' : 'company/')+''+value.alias+'"> by '+value.name+'</a>'+
                                '</div>'+
                                '</div>'+
                                '<div class="mediumText blueText bottomPadding">'+
                                  '<strong>'+(value.price).toLocaleString()+'USD</strong>'+
                                '</div>'+
                                '<div class="normalText bottomPadding">'+
                                  '<i class="fa fa-map-marker rightMargin"></i>'+ concat_country +
                                '</div>'+
                                '<div style="padding-top:8px;" class="topPaddingE">'+
                                      '<span id="latest_ads_mobile_'+value.id+'" class="pull-left blueText noPadding rightMargin"></span>' +
                                      '<span class="lightgrayText pull-right">'+value.get_advertisement_comments.length+' <i class="fa fa-comment"></i></span>'+
                                '</div>'+
                              '</div>'+
                              '</div>'+
                            '</div>';  
                    });
              $("#per_ads").val(per);
              var per_ads = $("#per_ads").val();
              var max_ads = $("#max_ads").val();
            
//               if(parseInt(per_ads) >= parseInt(max_ads)){
//                 $("#main-container").find("#btn-view-more-ads").remove();
//               }
              
              if(response.is_last == true){
                while($("#btn-view-more-ads").length > 0){
                  $(document).find("#btn-view-more-ads").remove();
                }
              }
              
              container.html(body);
              mobile_container.html(mobile_body);
               btn.html("");
               btn.html("View More");
             
                
               $('[data-countdown]').each(function() {
                 var $this = $(this), finalDate = $(this).data('countdown');
                 var ads_id = $(this).data('ads_id');
                 $this.countdown(finalDate, function(event) {
                 $this.html(event.strftime('%DD-%HH-%MM-%SS'));
                 });
               });
              $.each(response.rows, function(index, value) {      
                 $("#ads_latest_bids_"+value.id).rateYo({
                     starWidth: "12px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                 });
              });
              $.each(response.rows, function(index, value) {      
                 $("#latest_ads_mobile_"+value.id).rateYo({
                     starWidth: "14px",
                     rating:(value.average_rate != null ? value.average_rate:0),
                     ratedFill: "#5da4ec",
                     normalFill: "#cccccc",
                     readOnly: true,
                 });
              });
            }, 'json'); 
      });
</script>
@stop
@section('content')

<!-- main page start -->
<div class="container-fluid noPadding bgWhite borderBottom">
<div class="container bannerJumbutron">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding">
  <div class="col-lg-2 col-md-2 hidden-xs hidden-sm">
    <h4 class="listTitle leftPaddingB">CATEGORIES</h4>
<div id='flyout_menu'>
<ul class="main">
  <li class=""><a href="{{url('/').'/'.strtolower($country_code_session).'/'}}all-cities/buy-and-sell" class="grayText"><span class="rightMargin"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span> Buy and Sell<i class="fa fa-angle-right pull-right"></i></a></li>
  <li class=""><a href="{{url('/').'/'.strtolower($country_code_session).'/'}}all-cities/auctions" class="grayText"><span class="rightMargin"><i class="fa fa-gavel" aria-hidden="true"></i></span> Live Auctions<i class="fa fa-angle-right pull-right"></i></a></li>
  @foreach($categories as $row)
    <li class=""><a href="{{url('/').'/'.strtolower($country_code_session).'/'.'all-cities'.'/'.$row->slug}}" class="grayText"><span class="rightMargin"><?php echo $row->icon; ?></span> {{$row->name}}<i class="fa fa-angle-right pull-right"></i></a></li>
  @endforeach
    <li class=""><a href="{{url('/').'/'.strtolower($country_code_session).'/all-categories'}}" class="redText"><i class="fa fa-bars rightMargin" aria-hidden="true"></i>All Categories <i class="fa fa-angle-right pull-right"></i></a></li>
</ul>
</div>
  </div>
  <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 noPadding">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 norightPadding rightPaddingSm bottomPadding">
      <div id="carouselMain" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMain" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMain" data-slide-to="1"></li>
    <li data-target="#carouselMain" data-slide-to="2"></li>
    <li data-target="#carouselMain" data-slide-to="3"></li>
    <li data-target="#carouselMain" data-slide-to="4"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
<!--     <div class="item active">
        <img src="{{ URL::route('uploads', array(), false).'/home_banners/main'.'/'}}MainA.jpg">
    </div>
    <div class="item">
        <img src="{{ URL::route('uploads', array(), false).'/home_banners/main'.'/'}}MainB.jpg">
    </div> -->
        
          
   </div>
  </div>
</div>

    
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 noPadding topPaddingSm ">
      <div class="col-md-12 col-sm-6 col-xs-12 bottomPadding rightPaddingSMb">
<div id="carouselMiniA" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMiniA" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMiniA" data-slide-to="1"></li>
    <li data-target="#carouselMiniA" data-slide-to="2"></li>
    <li data-target="#carouselMiniA" data-slide-to="3"></li>
    <li data-target="#carouselMiniA" data-slide-to="4"></li>
  </ol>
  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">

<!-- {{--<div class="" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub1A.jpg); background-repeat: no-repeat; background-position:center; background-size:cover; height: 178px; width: auto;">
        </div> --}} -->

        <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub1A.jpg">
        <!-- <img src="{{ url('uploads') }}Sub1A.jpg"> -->

<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item" >
<!-- {{--        <div class="" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A.jpg); background-repeat: no-repeat; background-position:center; background-size:cover; height: 178px; width: auto;">
        </div> --}} -->
        <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A.jpg">
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    @foreach($get_banner_jumbo_top as $row)
    <div class="item" >
<!-- {{--        <div class="" style="background: url({{ url('uploads/banner/advertisement/desktop').'/'}}{{$row->image}}); background-position:center; background-size:cover; background-repeat: no-repeat; height: 178px; width: auto;">
        </div> --}} -->
        
        <img src="{{ url('uploads/banner/advertisement/desktop').'/'}}{{$row->image_desktop}}">
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    @endforeach
    @if(count($get_banner_jumbo_top)==1)
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub3A.jpg); background-repeat: no-repeat; background-size:cover; background-position:center;  height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub3A.jpg">
          </div>
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium1.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium1.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">.jpg">
          </div>
      @elseif(count($get_banner_jumbo_top)==2)
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium2.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium2.jpg">
          </div>
      @elseif(count($get_banner_jumbo_top)==0)
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub3A.jpg); background-size:cover; background-repeat: no-repeat; background-position:center;  height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub3A.jpg">
          </div>
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium1.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium1.jpg">
          </div>
          <div class="item" >
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium2.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub1'.'/'}}Sub2A-Premium2.jpg">
          </div>
      @endif
  </div>

  <!-- Controls -->
 <!--  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
        <!-- <div class="" style="background: url({{ url('img').'/'}}sample.jpg); background-repeat: no-repeat; height: 178px; width: auto;"> -->
        </div>
      <div class="col-sm-6 col-md-12 col-sm-6 col-xs-12 notoppaddingSm topPadding leftPaddingSMb">
        <div id="carouselMiniB" class="carousel slide carousel-fade" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carouselMiniB" data-slide-to="0" class="active"></li>
    <li data-target="#carouselMiniB" data-slide-to="1"></li>
    <li data-target="#carouselMiniB" data-slide-to="2"></li>
    <li data-target="#carouselMiniB" data-slide-to="3"></li>
    <li data-target="#carouselMiniB" data-slide-to="4"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
<!-- {{--        <div class="" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub1B.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
        </div> --}} -->
        <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub1B.jpg" style="min-height: 178px; width: auto;">
<!--       <div class="carousel-caption text-center">
        welcome
      </div> -->
    </div>
    <div class="item">
<!-- {{--        <div class="" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
        </div> --}} -->
        <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B.jpg" style="min-height: 178px; width: auto;">
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    @foreach($get_banner_jumbo_bottom as $row)
    <div class="item">
<!-- {{--        <div class="" style="background: url({{ url('uploads/banner/advertisement/desktop').'/'}}{{$row->image}}); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
        </div> --}} -->
        <img src="{{ URL::route('uploads', array(), false).'/banner/advertisement/desktop'.'/'}}{{$row->image_desktop}}" style="min-height: 178px; width: auto;">
<!--       <div class="carousel-caption text-center">
        plans
      </div> -->
    </div>
    @endforeach
    @if(count($get_banner_jumbo_bottom)==1)
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub3B.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub3B.jpg" style="min-height: 178px; width: auto;">
          </div>
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumA.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumA.jpg" style="min-height: 178px; width: auto;">
          </div>
      @elseif(count($get_banner_jumbo_bottom)==2)
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumB.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumB.jpg" style="min-height: 178px; width: auto;">
          </div>
      @elseif(count($get_banner_jumbo_bottom)==0)
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub3B.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub3B.jpg" style="min-height: 178px; width: auto;">
          </div>
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumA.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumA.jpg" style="min-height: 178px; width: auto;">
          </div>
          <div class="item">
<!-- {{--             <div class="fill" style="background: url({{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumB.jpg); background-size:cover; background-repeat: no-repeat; background-position:center; height: 178px; width: auto;">
              </div> --}} -->
              <img src="{{ URL::route('uploads', array(), false).'/home_banners/sub2'.'/'}}Sub2B-PremiumB.jpg" style="min-height: 178px; width: auto;">
          </div>
      @endif
  </div>

  <!-- Controls -->
<!--   <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> -->
</div>
       <!--  <div class="" style="background: url({{ url('img').'/'}}6.jpg); background-repeat: no-repeat; height: 200px; width: auto;"> -->
        </div>
      </div>
      <div class="clearfix visible-md-block visible-lg-block"></div>
    </div>
  </div>
</div>
</div>
  
<!-- main page end -->

<!-- latest bids start -->
<div class="container-fluid bgGray" id="main-container">
<div class="container blockMargin noPadding">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noPadding" >
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
    </div>
    <div class="col-sm-12 col-xs-12 blockBottom noPadding">
    <div class="panel-body noPadding" id="bids-mobile-container">
      @foreach ($adslatestbidsmobile as $row)
      <div class="panel panel-default removeBorder borderZero bottomMarginLight ">
        <div class="panel-body rightPaddingB rightPaddingXs noPadding">
        <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
         <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
           
          <div style="overflow: hidden;" class="adImage"> <img class="adImage" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" /> </div>

         </a>

        <div class="rightPadding nobottomPadding lineHeight">
          <div class="mediumText topPadding noPadding bottomPadding">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                <div class="visible-sm"><?php echo substr($row->title, 0, 37); 
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
                <div class="visible-xs"><?php echo substr($row->title, 0, 17);
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
            </a>
          <div class="normalText redText bottomPadding ellipsis">
                 <a class="normalText lightgrayText" href="{{ url($row->usertype_id == 1 ? 'vendor' : 'company') . '/' . $row->alias }}"> by {{ $row->name }}</a>
          </div>
          </div>
          <div class="normalText redText bottomPadding">
          <div data-countdown="{{$row->ad_expiration}}"></div>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="normalText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
          <?php $i=0;?>
                    @if(count($row->getAdvertisementComments)>0)
                       @foreach($row->getAdvertisementComments as $count)
                            <?php $i++;?>     
                       @endforeach   
                    @endif
<!--             <div style="padding-top:8px;">
                <span id="ads_latest_bids_mobile_{{$row->id}}" class="blueText noPadding pull-left"></span>
                <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
          </div>  -->


                   <?php $i=0;?>
                    @if(count($row->getAdvertisementComments)>0)
                       @foreach($row->getAdvertisementComments as $count)
                            <?php $i++;?>     
                       @endforeach   
                    @endif
          <div class="mediumText nobottomPadding topPaddingB">
                <span id="ads_latest_bids_mobile_{{$row->id}}" class="blueText noPadding pull-left"></span>
                <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                <div class="clearfix"></div>
          </div>
        </div>
        </div>
      </div>
      @endforeach
    </div>
    <div class="panel-footer bgWhite noPaddingXs text-center removeBorder">
      <button class="btn blueButton borderZero {{count($adslatestbids) == $max_bids_count  ? 'hide':''}}{{count($adslatestbids) == 0  ? 'hide':''}} {{count($adslatestbids) <= 4  ? 'hide':''}}" type="button" id="btn-view-more-bids" style="width:100%;"><i class=""></i>View More</button>
    </div>
    </div>
  </div>
</div>

<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs" >
    <div class="panel panel-default removeBorder borderZero borderBottom" id="panel-bids-main">
      <div class="panel-heading panelTitleBarLight">
        <span class="panelTitle">Latest Bids</span>  <span class="hidden-xs">| Check out our members latest bids and start bidding now!</span>
<!--         <span class="pull-right"><a href="{{url('category/list')}}" class="redText">View More</a></span> -->
      </div>

      <div class="panel-body nobottomPadding" id="panel-bids">
<!--             <div id="load-form" class="loading-pane hide">
                    <div><i class="fa fa-inverse fa-spinner fa-pulse fa-3x centered"></i></div>
            </div> -->
    <!--     <div id="bidsCarousel" class="carousel slide carousel-fade" data-ride="carousel"> -->
          <!-- Wrapper for slides -->
      <!--     <div class="carousel-inner" role="listbox"> -->
        
      <!--       <div class="item active"> -->
            <div class="col-lg-12 col-md-12" id="main-bid-container">
              <div id="latest-bids-container">
              @foreach ($adslatestbids as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="{{$row->get_ads_id}}" id="bid-container">
                <div class="sideMargin borderBottom">
                <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
                  <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}" target="_blank">
                  <div style="overflow: hidden;" class="adImage"> <img class="adImage" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" /> </div>
                </a>
                  <div class="minPadding nobottomPadding">
<!--                     <div class="mediumText noPadding topPadding bottomPadding"> -->
                      <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                    </a>
                    <div class="normalText grayText bottomPadding"> 
                    <div class="row">
                    <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}"> 
                     <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
                    <div class="normalText bottomPaddingB">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor rightPadding" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                               <?php $i=0;?>
                                  @if(count($row->getAdvertisementComments)>0)
                                      @foreach($row->getAdvertisementComments as $count)
                                           <?php $i++;?>     
                                      @endforeach   
                                  @endif
                                </div>
                                <div class="col-sm-6 noleftPadding">
                      <span class="pull-right">
                             <span id="ads_latest_bids_{{$row->id}}" class="pull-left blueText"></span>
                             <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>

                      </span>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
                <div class="sideMargin minPadding text-center nobottomPadding redText normalText bottomMarginC">       
                  <div data-countdown="{{$row->ad_expiration}}" data-currenttime="{{ $current_time}}" data-ads_id="{{ $row->get_ads_id }}"></div>
                </div>
              </div>
                @endforeach
              </div>
            </div>
       </div>
        <div class="text-center">
            <input type="hidden" id="per_bids" value="{{count($adslatestbids)}}">
            <input type="hidden" id="max_bids" value="{{$max_bids_count}}">
            <button class="btn blueButton borderZero {{count($adslatestbids) == $max_bids_count ? 'hide':''}} {{count($adslatestbids) == 0  ? 'hide':''}} {{count($adslatestbids) <= 4  ? 'hide':''}}" type="button" id="btn-view-more-bids" style="width:100%;"><i class=""></i>View More</button>
        </div>
    </div>
  </div>
<!-- Mobile View -->
<div class="col-xs-12 col-sm-12 visible-sm visible-xs" >
  <div class="panel panel-default bottomMarginLight removeBorder borderZero">
    <div class="panel-heading panelTitleBarLightB">
      <span class="panelTitle">Latest Ads</span> 
    </div>
     <div class="col-sm-12 col-xs-12 noPadding">
      <div class="panel-body noPadding" id="ads-mobile-container">
     @foreach ($latestadsmobile as $row)
        <div class="panel panel-default borderZero removeBorder bottomMarginLight ">
          <div class="panel-body rightPaddingB rightPaddingXs noPadding">
          <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
              <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}" id="btn-user-view" data-id="{{$row->id}}">

              {{-- <div class="adImage" style="overflow:hidden;"> --}}

              <div style="overflow: hidden;" class="adImage"> <img class="adImage" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" /> </div>

              {{-- </div> --}}
          </a>
        <div class="rightPadding nobottomPadding lineHeight">
          <div class="mediumText noPadding topPadding bottomPadding">
          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                <div class="visible-sm"><?php echo substr($row->title, 0, 37); 
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
                <div class="visible-xs"><?php echo substr($row->title, 0, 17);
                   if(strlen($row->title) >= 37){echo "..";} ?>
                </div>
          </a>
          </div>
          <div class="normalText redText bottomPadding ellipsis">
                 <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'.$row->alias}}"> by {{ $row->name }}</a>
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>{{ number_format($row->price) }} USD</strong>
          </div>
          <div class="normalText bottomPadding">
            @if($row->city || $row->countryName)
            <i class="fa fa-map-marker rightMargin"></i>
            {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
            @else
            <i class="fa fa-map-marker rightMargin"></i> not available
            @endif
          </div>
                   <?php $i=0;?>
                    @if(count($row->getAdvertisementComments)>0)
                       @foreach($row->getAdvertisementComments as $count)
                            <?php $i++;?>     
                       @endforeach   
                    @endif
          <div class="mediumText bottomPadding topPaddingE">
                <span id="latest_ads_mobile_{{$row->id}}" class="blueText noPadding pull-left"></span>
                <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
          </div>
        </div>
          </div>
        </div>
         @endforeach
         
    </div>
    <div class="panel-footer bgWhite text-center noPaddingXs removeBorder">
            <button class="btn blueButton borderZero {{count($latestads) == $max_ads_count  ? 'hide':''}} {{count($latestads) == 0  ? 'hide':''}} {{count($latestads) <= 4  ? 'hide':''}}" type="button" id="btn-view-more-ads" style="width:100%;"><i class=""></i>View More</button>
          </div>
    </div>
  </div>
<!--   <div class="panel panel-default marginbottomHeavy removeBorder borderZero">
    <div class="panel-body noPadding">
      <div class="col-sm-12 col-xs-12 noPadding">
        <div class="">
          <div class="adImage" style="background: url({{ url('img').'/'}}w.jpg); background-repeat: no-repeat; background-size: cover; background-position:center center;">
          </div>
        <div class="rightPadding nobottomPadding">
          <div class="mediumText noPadding topPadding bottomPadding">Ad Listing Title Here</div>
          <div class="normalText redText bottomPadding">
            by Dell Distributor
          </div>
          <div class="mediumText blueText bottomPadding">
            <strong>1,000,000 USD</strong>
          </div>
          <div class="mediumText bottomPadding">
          <i class="fa fa-map-marker"></i> 
            Jeddah, UAE
          </div>
          <div class="mediumText blueText">
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>
            <i class="fa fa-star"></i>listingsa
            <i class="fa fa-star-half-empty"></i>
            <span class="pull-right normalText lightgrayText">27<i class="fa fa-comment"></i></span>
          </div>
        </div>
      </div>
    </div>
    </div>
  </div> -->
</div>
<!-- Mobile View -->
  <div class="col-lg-12 col-md-12 hidden-sm hidden-xs">
    <div class="panel panel-default removeBorder borderZero noMargin" id="panel-ads-main">
      <div class="panel-heading panelTitleBar">
        <span class="panelTitle">Latest Ads</span> <span class="hidden-xs">| Check out our members latest listings now!</span>
      </div>
      <div class="panel-body nobottomPadding" id="panel-ads">
           <div id="load-form" class="loading-pane hide">
                    <div><i class="fa fa-inverse fa-spinner fa-pulse fa-3x centered"></i></div>
            </div>
       <!--  <div id="adsCarousel" class="carousel slide carousel-fade" data-ride="carousel"> -->
          <!-- Indicators -->

          <!-- Wrapper for slides -->
   <!--        <div class="carousel-inner" role="listbox" id="latest-ads-wrapper"> -->
            
     <!--        <div class="item active"> -->
            <div class="col-lg-12 col-md-12" id="main-ads-container">
              <div id="latest-ads-container">
              @foreach ($latestads as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight" data-id="{{$row->id}}">
                <div class="sideMargin bottomMarginC borderBottom">
                <span class="label label-featured {{$row->feature == 1 ? '':'hide'}}">FEATURED</span>
                  <a href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}" id="btn-user-view" data-id="{{$row->id}}">
                  
                {{-- <div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;">
                </div> --}}

                <div style="overflow: hidden;" class="adImage"> <img class="adImage" alt="{{$row->ad_type_slug.' '.$row->photo.' 1 of '.$row->parent_category_slug.' '.$row->title}}" title="{{$row->ad_type_slug.' '.$row->photo.' 1 '. $row->title}}" src="{{ URL::route('uploads', array(), false).'/ads/thumbnail'.'/'.$row->photo}}" style="object-fit: cover; overflow: hidden;" /> </div>

                </a>
                  <div class="minPadding nobottomPadding">
<!--                     <div class="mediumText noPadding topPadding bottomPadding"> -->
                    <div class="row">
                      <div class="col-sm-12 ellipsis cursor">
                          <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{url('/').'/'.$row->country_code.'/'.$row->ad_type_slug.'/'.$row->parent_category_slug.'/'.$row->category_slug.'/'.$row->slug}}">
                             <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                          </a>
                        </div>
                      </div>
<!--                       <a class="mediumText noPadding topPadding bottomPadding grayText" href="{{ url('view') . '/' . $row->id }}">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                  </a> -->
                    <div class="normalText grayText bottomPadding">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->name }}">
                     <a class="normalText lightgrayText" href="{{url($row->usertype_id == 1 ? 'vendor' : 'company').'/'. $row->alias}}"> by {{ $row->name }}</a>
                   </div>
                   <div class="col-sm-6">
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                  </div>
                    </div>
                    <div class="normalText bottomPaddingB">
                      <div class="row">
                        <div class="col-sm-6 ellipsis cursor" data-toggle="tooltip" title="{{ $row->countryName}}">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                        <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      </div>
                      <div class="col-sm-6">
                      <span class="pull-right">
                             <span id="latest_ads_{{$row->id}}" class="pull-left blueText rightMargin"></span>
                             <span class="lightgrayText">{{$i}} <i class="fa fa-comment"></i></span>

                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
                      </span>
                    </div>
                  </div>
                    </div>
                  </div>
                </div>
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
              </div>
                @endforeach
              </div>
            </div>
    <!--         </div> -->


         <!--   <div class="item">
            <div class="col-lg-12 col-md-12">
            @foreach ($latestads_panel2 as $row)
              <div class="col-lg-3 col-md-3 noPadding lineHeight">
                <div class="sideMargin bottomMarginB  borderBottom">
                  <a href="{{ url('view') . '/' . $row->id }}"><div class="adImage" style="background: url({{ url('uploads/ads/thumbnail').'/'.$row->photo}}); background-repeat: no-repeat; background-size: cover; background-position:center center;pubpubl">
                </div></a>
                  <div class="minPadding nobottomPadding">
                    <div class="mediumText noPadding topPadding bottomPadding">
                          <div class="visible-lg"><?php echo substr($row->title, 0, 35); 
                              if(strlen($row->title) >= 37){echo "..";} ?>
                         </div>
                          <div class="visible-md"><?php echo substr($row->title, 0, 25);
                              if(strlen($row->title) >= 37){echo "..";} ?>
                          </div>
                    </div>
                    <div class="normalText grayText bottomPadding bottomMargin">
                     <a class="normalText lightgrayText" href="{{URL::to('/'.$row->alias)}}"> by {{ $row->name }}</a>
                      <span class="pull-right"><strong class="blueText">{{ number_format($row->price) }} USD</strong>
                      </span>
                    </div>
                    <div class="normalText bottomPadding">
                       @if($row->city || $row->countryName)
                       <i class="fa fa-map-marker rightMargin"></i>
                       {{ $row->city }} @if ($row->city),@endif {{ $row->countryName }}
                       @else
                       <i class="fa fa-map-marker rightMargin"></i> not available
                       @endif
                       <?php $i=0;?>
                        @if(count($row->getAdvertisementComments)>0)
                           @foreach($row->getAdvertisementComments as $count)
                                <?php $i++;?>     
                           @endforeach   
                        @endif
                      <span class="pull-right ">
                         @if($row->average_rate != null)
                             <span id="latest_ads_panel2_{{$row->id}}" class="blueText pull-left rightMargin"></span>
                             <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @else
                             <span class="blueText rightMargin">No Rating</span>
                             <span class="lightgrayText pull-right">{{$i}} <i class="fa fa-comment"></i></span>
                          @endif  -->
                       <!--  <span class="blueText rightMargin">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-empty"></i>
                        </span>
                        <span class="lightgrayText">27 <i class="fa fa-comment"></i></span> -->
         <!--              </span>
                    </div>
                  </div>
                </div> -->
               <!--  <div class="sideMargin minPadding text-center nobottomPadding redText normalText">
                  D: 00 - H: 00 - M: 00
                </div> -->
          <!--     </div>
              @endforeach     
            </div>         
            </div> -->
  <!--     </div> -->











          <!-- Left and right controls -->
       <!--    <a class="left carousel-control" href="#adsCarousel" role="button" data-slide="prev" style="background:none; text-align:left; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; left:-3px;">
            <span class="fa fa-angle-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#adsCarousel" role="button" data-slide="next" style="background:none; text-align:right; color:#cccccc; text-shadow:none; font-size:36px; vertical-align:middle; font-weight:normal; opacity:1; top:227px; width: 10px; right:-3px;">
            <span class="fa fa-angle-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
           -->
       <!--  </div> -->
      </div>
      <div class="text-center">
          <input type="hidden" id="per_ads" value="{{count($latestads)}}">
          <input type="hidden" id="max_ads" value="{{$max_ads_count}}">
          <button class="btn blueButton borderZero {{count($latestads) == $max_ads_count  ? 'hide':''}} {{count($latestads) == 0  ? 'hide':''}} {{count($latestads) <= 4  ? 'hide':''}}" type="button" id="btn-view-more-ads" style="width:100%;">View More</button>

      </div>
    </div>
      </div>
    </div>
    </div>
  </div>
<!-- latest end -->
@stop